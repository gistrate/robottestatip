*** Settings ***
Suite Setup    setupSuiteEnv

#Libraries
Library  Utils.py
Library  Collections

#Resource files
Resource  RobotUtils.robot

*** Variables ***
#Command line variables:  ${USERNAME}, ${RESOURCEPOOL}

*** Test Cases ***
Single_Virtual_Port_Configuration

    AddNTOLocal  nto1  10.215.157.87

    #10Gb Card port definitions.
    &{P2A-04Def}=  Create Dictionary   name=np1   port=P2A-04   portMode=NETWORK  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-05Def}=  Create Dictionary   name=tp1   port=P2A-05   portMode=TOOL     connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P2A-04Def}
    AddPort  ${nto1}  ${P2A-05Def}

    #Virt Port local definitions.
    ${vp1Local}=  Catenate  {'enabled': True, 'local_ip': '1.1.1.1', 'subnet_mask': '255.255.255.0', 'default_gateway': None}

    AddVirtPort  ${tp1}  vp1  ${vp1Local}  1.1.1.100

    AddFilter  ${nto1}  f1    PASS_ALL

    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${stats}=  vp1.getPortStats
    log  Virt Port Stats - ${stats}

    sleep  10secs

    ${vpInfo}=  vp1.getVirtPortInfo
    log   Virtual Port Info - ${vpInfo}

    nto1.cleanupNVSDevices


Single_Virtual_Port_Configuration_with_Optional_Default_GW

    AddNTOLocal  nto1  10.215.157.87

    #10Gb Card port definitions.
    &{P2A-04Def}=  Create Dictionary   name=np1   port=P2A-04   portMode=NETWORK  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-05Def}=  Create Dictionary   name=tp1   port=P2A-05   portMode=TOOL     connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P2A-04Def}
    AddPort  ${nto1}  ${P2A-05Def}

    #Virt Port local definitions.
    ${vp1Local}=  Catenate  {'enabled': True, 'local_ip': '1.1.1.1', 'subnet_mask': '255.255.255.0', 'default_gateway': '1.1.1.254'}

    AddVirtPort  ${tp1}  vp1  ${vp1Local}  1.1.1.100

    AddFilter  ${nto1}  f1    PASS_ALL

    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    sleep  10secs

    ${vpInfo}=  vp1.getVirtPortInfo
    log   Virtual Port Info - ${vpInfo}

    nto1.cleanupNVSDevices


Single_Virtual_Port_Configuration_with_Manual_Cleanup

    AddNTOLocal  nto1  10.215.157.87

    #10Gb Card port definitions.
    &{P2A-04Def}=  Create Dictionary   name=np1   port=P2A-04   portMode=NETWORK  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-05Def}=  Create Dictionary   name=tp1   port=P2A-05   portMode=TOOL     connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P2A-04Def}
    AddPort  ${nto1}  ${P2A-05Def}

    #Virt Port local definitions.
    ${vp1Local}=  Catenate  {'enabled': True, 'local_ip': '1.1.1.1', 'subnet_mask': '255.255.255.0', 'default_gateway': '1.1.1.254'}

    AddVirtPort  ${tp1}  vp1  ${vp1Local}  1.1.1.100

    AddFilter  ${nto1}  f1    PASS_ALL

    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    sleep  10secs

    ${vpInfo}=  vp1.getVirtPortInfo
    log   Virtual Port Info - ${vpInfo}

    log   Disabling virt port  console=true
    vp1.disableVirtPort

    sleep  10secs

    log   Enabling virt port  console=true
    vp1.enableVirtPort

    sleep  10secs

    log   Disabling virt port  console=true
    vp1.disableVirtPort

    log   Deleting virt port  console=true
    vp1.deleteVirtPortById

    sleep  10secs

    log   Calling CleanupNVSDevices  console=true
    nto1.cleanupNVSDevices


Multiple_Virtual_Port_Configuration

    AddNTOLocal  nto1  10.215.157.87

    #10Gb Card port definitions.
    &{P2A-01Def}=  Create Dictionary   name=np1   port=P2A-01   portMode=NETWORK  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-02Def}=  Create Dictionary   name=tp1   port=P2A-02   portMode=TOOL     connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-03Def}=  Create Dictionary   name=np2   port=P2A-03   portMode=NETWORK  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-04Def}=  Create Dictionary   name=tp2   port=P2A-04   portMode=TOOL     connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P2A-01Def}
    AddPort  ${nto1}  ${P2A-02Def}
    AddPort  ${nto1}  ${P2A-03Def}
    AddPort  ${nto1}  ${P2A-04Def}

    #Virt Port local definitions.
    ${vp1Local}=  Catenate  {'enabled': True, 'local_ip': '1.1.1.1', 'subnet_mask': '255.255.255.0', 'default_gateway': '1.1.1.254'}
    ${vp2Local}=  Catenate  {'enabled': True, 'local_ip': '1.1.2.1', 'subnet_mask': '255.255.255.0', 'default_gateway': '1.1.2.254'}

    AddVirtPort  ${tp1}  vp1  ${vp1Local}  1.1.1.100
    AddVirtPort  ${tp2}  vp2  ${vp2Local}  1.1.2.200

    AddFilter  ${nto1}  f1    PASS_ALL
    AddFilter  ${nto1}  f2    PASS_ALL

    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}
    AddConn    ${nto1}  c2    ${np2}   ${f2}    ${vp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    sleep  10secs

    ${vpInfo}=  vp1.getVirtPortInfo
    log   Virtual Port Info - ${vpInfo}

    ${vpInfo}=  vp2.getVirtPortInfo
    log   Virtual Port Info - ${vpInfo}

    nto1.cleanupNVSDevices


Multiple_Virtual_Port_with_Single_Filter_Configuration

    AddNTOLocal  nto1  10.215.157.87

    #10Gb Card port definitions.
    &{P2A-01Def}=  Create Dictionary   name=np1   port=P2A-01   portMode=NETWORK  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-02Def}=  Create Dictionary   name=tp1   port=P2A-02   portMode=TOOL     connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-03Def}=  Create Dictionary   name=np2   port=P2A-03   portMode=NETWORK  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-04Def}=  Create Dictionary   name=tp2   port=P2A-04   portMode=TOOL     connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P2A-01Def}
    AddPort  ${nto1}  ${P2A-02Def}
    AddPort  ${nto1}  ${P2A-03Def}
    AddPort  ${nto1}  ${P2A-04Def}

    #Virt Port local definitions.
    ${vp1Local}=  Catenate  {'enabled': True, 'local_ip': '1.1.1.1', 'subnet_mask': '255.255.255.0', 'default_gateway': '1.1.1.254'}
    ${vp2Local}=  Catenate  {'enabled': True, 'local_ip': '1.1.2.1', 'subnet_mask': '255.255.255.0', 'default_gateway': '1.1.2.254'}

    AddVirtPort  ${tp1}  vp1  ${vp1Local}  1.1.1.100
    AddVirtPort  ${tp2}  vp2  ${vp2Local}  1.1.2.200

    AddFilter  ${nto1}  f1    PASS_ALL

    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}
    AddConn    ${nto1}  c2    ${np2}   ${f1}    ${vp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    sleep  10secs

    ${vpInfo}=  vp1.getVirtPortInfo
    log   Virtual Port Info - ${vpInfo}

    ${vpInfo}=  vp2.getVirtPortInfo
    log   Virtual Port Info - ${vpInfo}

    nto1.cleanupNVSDevices


Single_Virtual_Port_with_Multiple_Filters_Configuration

    AddNTOLocal  nto1  10.215.157.87

    #10Gb Card port definitions.
    &{P2A-01Def}=  Create Dictionary   name=np1   port=P2A-01   portMode=NETWORK  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-02Def}=  Create Dictionary   name=np2   port=P2A-02   portMode=NETWORK  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-03Def}=  Create Dictionary   name=tp1   port=P2A-03   portMode=TOOL     connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P2A-01Def}
    AddPort  ${nto1}  ${P2A-02Def}
    AddPort  ${nto1}  ${P2A-03Def}

    #Virt Port local definitions.
    ${vp1Local}=  Catenate  {'enabled': True, 'local_ip': '1.1.1.1', 'subnet_mask': '255.255.255.0', 'default_gateway': None}

    AddVirtPort  ${tp1}  vp1  ${vp1Local}  1.1.1.100

    AddFilter  ${nto1}  f1    PASS_ALL
    AddFilter  ${nto1}  f2    PASS_ALL

    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}
    AddConn    ${nto1}  c2    ${np2}   ${f2}    ${vp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    sleep  10secs

    ${vpInfo}=  vp1.getVirtPortInfo
    log   Virtual Port Info - ${vpInfo}

    nto1.cleanupNVSDevices


Single_Virtual_Port_Port_Group

    AddNTOLocal  nto1  10.215.157.87

    #10Gb Card port definitions.
    &{P2A-04Def}=  Create Dictionary   name=np1   port=P2A-04   portMode=NETWORK  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-05Def}=  Create Dictionary   name=tp1   port=P2A-05   portMode=TOOL     connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P2A-04Def}
    AddPort  ${nto1}  ${P2A-05Def}

    #Virt Port local definitions.
    ${vp1Local}=  Catenate  {'enabled': True, 'local_ip': '1.1.1.1', 'subnet_mask': '255.255.255.0', 'default_gateway': None}

    AddVirtPort  ${tp1}  vp1  ${vp1Local}  1.1.1.100

    #Port Group definitions.
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}

    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${vp1}

    AddFilter  ${nto1}  f1    PASS_ALL

    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tpg1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    sleep  10secs

    ${vpInfo}=  vp1.getVirtPortInfo
    log   Virtual Port Info - ${vpInfo}

    nto1.cleanupNVSDevices


Dual_Virt_Port_Port_Group

     AddNTOLocal  nto1  10.215.157.87

    #10Gb Card port definitions.
    &{P2A-01Def}=  Create Dictionary   name=np1   port=P2A-03   portMode=NETWORK  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-02Def}=  Create Dictionary   name=tp1   port=P2A-04   portMode=TOOL     connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-03Def}=  Create Dictionary   name=tp2   port=P2A-05   portMode=TOOL     connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P2A-01Def}
    AddPort  ${nto1}  ${P2A-02Def}
    AddPort  ${nto1}  ${P2A-03Def}

    #Virt Port local definitions.
    ${vp1Local}=  Catenate  {'enabled': True, 'local_ip': '1.1.1.1', 'subnet_mask': '255.255.255.0', 'default_gateway': None}
    ${vp2Local}=  Catenate  {'enabled': True, 'local_ip': '1.1.1.10', 'subnet_mask': '255.255.255.0', 'default_gateway': None}

    AddVirtPort  ${tp1}  vp1  ${vp1Local}  1.1.1.100
    AddVirtPort  ${tp2}  vp2  ${vp2Local}  1.1.1.101

    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    @{pgPortList}=  Create List  ${vp1}  ${vp2}

    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${pgPortList}

    AddFilter  ${nto1}  f1    PASS_ALL

    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tpg1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    sleep  10secs

    ${vpInfo}=  vp1.getVirtPortInfo
    log   Virtual Port Info - ${vpInfo}

    ${vpInfo}=  vp2.getVirtPortInfo
    log   Virtual Port Info - ${vpInfo}

    nto1.cleanupNVSDevices

*** Keywords ***
setupSuiteEnv
    log  USERNAME=${USERNAME}  console=true
    Set Global Variable  &{ixChassisDict}  &{EMPTY}

