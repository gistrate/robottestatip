*** Settings ***
Suite Setup     setupSuiteEnv
Suite Teardown  suiteCleanup
Test Teardown   testCleanup

#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-02.ann.is.keysight.com
Library  ./NTO/frwk/Utils.py
Library  Collections
Library  OperatingSystem

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  60
${noOfPacketsInStreams}=  100
${maxFrameLength}=  9216
${bwAllocation}=  10

*** Test Cases ***

AFM-VxLAN-STRIP-NP-TP-TC000791218a
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f1}  ${tp2}

    ${original_captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/Original.enc
    ${original_capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/Original.pcap

    ${np1stripped_captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/np1Stripped.enc
    ${np1stripped_capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/np1Stripped.pcap

    ${np1NoStrip_captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/np1NoStrip.enc
    ${np1NoStrip_capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/np1NoStrip.pcap

    ${np1CfpStrip_captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/np1StrippedCfp.enc
    ${np1CfpStrip_capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/np1StrippedCfp.pcap

    ${tp2Strip_captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tp2StrippedVxlan.enc
    ${tp2Strip_capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tp2StrippedVxlan.pcap

    ${noOfStreams}=  evaluate  4
    ${noOfPackets}=  evaluate    ${noOfStreams} * ${noOfPacketsInStreams}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #packet processing config
    ${pp1}=  Catenate  {'fabric_path_strip_settings': {'enabled':True}}
    ${pp2}=  Catenate  {'vxlan_strip_settings':{'enabled':True}}
    ${pp3}=  Catenate  {'fabric_path_strip_settings': {'enabled':False}}
    ${pp4}=  Catenate  {'vxlan_strip_settings':{'enabled':False}}

    #Stream definitions
    ${stream1}=  createStream  s57  CFP(src='00:00:aa:00:00:57',dst='00:00:bb:00:00:57',type=0x8903 ,ftag=10, ttl=3)/Ether(src='00:11:11:11:11:57',dst='00:22:22:22:22:57')/IP(src='11.11.11.57',dst='22.22.22.57')/UDP(sport=57571,dport=4789,chksum=0)/VXLAN(vni=16777215)/Ether(src='00:33:33:33:33:57',dst='00:44:44:44:44:57')/Dot1Q(vlan=3357,prio=100)/IPv6(src='3001:AAAA:BBBB:CCCC:DDDD:EEEE:FFF0:1157',dst='3ffe:1944:100:a:0:bc:2500:d057',nh=59)  payload=incr  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}

    ${stream2}=  createStream  s58  CFP(src='00:00:aa:00:00:58',dst='00:00:bb:00:00:58',type=0x8903,ftag=10, ttl=3)/Ether(src='00:11:11:11:11:58',dst='00:22:22:22:22:58')/IPv6(src='2001:1cd9:22:33ab:4:5555:6667:7758',dst='2ffe:1944:100c:a23d:2f04:bc21:2500:d058',nh=17)/UDP(sport=58581,dport=4789,chksum=0)/VXLAN(vni=16777215)/Ether(src='00:33:33:33:33:58',dst='00:44:44:44:44:58')/Dot1Q(vlan=3358,prio=100)/IPv6(src='3001:AAAA:BBBB:CCCC:DDDD:EEEE:FFF0:1158',dst='3ffe:1944:100:a:0:bc:2500:d058',nh=59)  payload=incr  frameSize=156  numFrames=${noOfPacketsInStreams}

    ${stream3}=  createStream  s59  CFP(src='00:00:aa:00:00:59',dst='00:00:bb:00:00:59',type=0x8903,ftag=10, ttl=3)/Ether(src='00:11:11:11:11:59',dst='00:22:22:22:22:59')/Dot1Q(vlan=459,prio=111)/IP(src='11.11.11.59',dst='22.22.22.59')/UDP(sport=59591,dport=4789,chksum=0)/VXLAN(vni=16777215)/Ether(src='00:33:33:33:33:59',dst='00:44:44:44:44:59')/Dot1Q(vlan=3359,prio=100)/IPv6(src='3001:AAAA:BBBB:CCCC:DDDD:EEEE:FFF0:1159',dst='3ffe:1944:100:a:0:bc:2500:d059',nh=59)  payload=incr  frameSize=256  numFrames=${noOfPacketsInStreams}

    ${stream4}=  createStream  s60  CFP(src='00:00:aa:00:00:60',dst='00:00:bb:00:00:60',type=0x8903,ftag=10, ttl=3)/Ether(src='00:11:11:11:11:60',dst='00:22:22:22:22:60')/Dot1Q(vlan=460,prio=111)/IPv6(src='2001:1cd9:22:33ab:4:5555:6667:7760',dst='2ffe:1944:100c:a23d:2f04:bc21:2500:d060',nh=17)/UDP(sport=60601,dport=4789,chksum=0)/VXLAN(vni=16777215)/Ether(src='00:33:33:33:33:60',dst='00:44:44:44:44:60')/Dot1Q(vlan=3360,prio=100)/IPv6(src='3001:AAAA:BBBB:CCCC:DDDD:EEEE:FFF0:1160',dst='3ffe:1944:100:a:0:bc:2500:d060',nh=59)  payload=incr  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}

    # Configure streams and port
    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts

    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback

    afm1.attachResource  ${np1}  ${bwAllocation}
    afm1.attachResource  ${tp2}  ${bwAllocation}

    log   Test 1 - Enable CFP and VXLAN Stripping on NP1
    np1.configurePacketProcessing  ${pp1}
    np1.configurePacketProcessing  ${pp2}

    # Start Capture, Run Traffic and Stop Capture
    np1Ixia.startIxCapture
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    np1Ixia.stopIxCapture
    tp1Ixia.stopIxCapture

    # Verify pcaps
    np1Ixia.exportIxFramesToFile  ${original_captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${original_captureFile}  ${original_capturePcap}

    tp1Ixia.exportIxFramesToFile  ${np1stripped_captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${np1stripped_captureFile}  ${np1stripped_capturePcap}
    tp1Ixia.scapyImportPcap  ${np1stripped_capturePcap}

    # Verify stats
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats
    ${tp1ActualStats} =  tp1.getPortStats  statName=all

    ${noOfBytesBeforeStripping}=  evaluate    18844 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    18532 * ${noOfPacketsInStreams}

    ${pktProcessStats} =  Catenate  {'vxlan_strip':${noOfPackets}, 'fabric_path_strip':${noOfPackets}}
    compareStatsOnNetwork   ${np1ActualStats}  rxPackets=${noOfPackets}   rxBytes=${noOfBytesBeforeStripping}  txBytes=${noOfBytesAfterStripping}  txPackets=${noOfPackets}  pktProcessStats=${pktProcessStats}
    compareStatsOnFilter  ${f1ActualStats}  rxPackets=${noOfPackets}   rxBytes=${noOfBytesAfterStripping}  txBytes=${noOfBytesAfterStripping}  txPackets=${noOfPackets}
    compareStatsOnTool   ${tp1ActualStats}  rxPackets=${noOfPackets}   txBytes=${noOfBytesAfterStripping}  txPackets=${noOfPackets}

    ${np1CfpStrippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${original_capturePcap}  verbose=${True}
    ${np1CfpAndVxLANStrippedFrames}=  np1.pktProc.emulate  ${pp2}  frames=${np1CfpStrippedFrames}  verbose=${True}
    compareFrames  ${np1CfpAndVxLANStrippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}

    log   Test 2 - Disable CFP and VXLAN Stripping on NP1
    np1.resetPortStats    
    tp1.resetPortStats    
    nto1.clearAllIxPorts    
    f1.resetFilterStats    
    np1.configurePacketProcessing  ${pp3}
    np1.configurePacketProcessing  ${pp4}

    # Start Capture, Run Traffic and Stop Capture
    np1Ixia.startIxCapture
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    np1Ixia.stopIxCapture
    tp1Ixia.stopIxCapture

    # Verify pcaps
    np1Ixia.scapyImportPcap  ${original_capturePcap}

    tp1Ixia.exportIxFramesToFile  ${np1NoStrip_captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${np1NoStrip_captureFile}  ${np1NoStrip_capturePcap}
    tp1Ixia.scapyImportPcap  ${np1NoStrip_capturePcap}

    # Verify stats
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats
    ${tp1ActualStats} =  tp1.getPortStats  statName=all

    ${originalNofBytes}=  evaluate    18844 * ${noOfPacketsInStreams}

    ${pktProcessStats} =  Catenate  {'vxlan_strip':0, 'fabric_path_strip':0}
    compareStatsOnNetwork   ${np1ActualStats}  rxPackets=${noOfPackets}   rxBytes=${originalNofBytes}  txBytes=${originalNofBytes}  txPackets=${noOfPackets}  pktProcessStats=${pktProcessStats}
    compareStatsOnFilter  ${f1ActualStats}  rxPackets=${noOfPackets}   rxBytes=${originalNofBytes}  txBytes=${originalNofBytes}  txPackets=${noOfPackets}
    compareStatsOnTool   ${tp1ActualStats}  rxPackets=${noOfPackets}   txBytes=${originalNofBytes}  txPackets=${noOfPackets}

    compareFrames  ${np1Ixia.recdFrames}  ${tp1Ixia.recdFrames}  ordered=${True}

    log   Test 3 - Disable CFP and VXLAN Stripping on NP1
    np1.resetPortStats    
    tp1.resetPortStats    
    f1.resetFilterStats    
    nto1.clearAllIxPorts    

    np1.configurePacketProcessing  ${pp1}
    tp2.configurePacketProcessing  ${pp2}

    # Start Capture, Run Traffic and Stop Capture
    np1Ixia.startIxCapture
    tp1Ixia.startIxCapture
    tp2Ixia.startIxCapture
    np1Ixia.runIxTraffic
    np1Ixia.stopIxCapture
    tp1Ixia.stopIxCapture
    tp2Ixia.stopIxCapture

    tp1Ixia.exportIxFramesToFile  ${np1CfpStrip_captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${np1CfpStrip_captureFile}  ${np1CfpStrip_capturePcap}
    tp1Ixia.scapyImportPcap  ${np1CfpStrip_capturePcap}

    tp2Ixia.exportIxFramesToFile  ${tp2Strip_captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${tp2Strip_captureFile}  ${tp2Strip_capturePcap}
    tp2Ixia.scapyImportPcap  ${tp2Strip_capturePcap}

    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${tp2ActualStats} =  tp2.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats

    #set expected statistics for port and filter
    ${noOfBytesAfterStrippingNp1}=  evaluate    18780 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStrippingTp2}=  evaluate    18532 * ${noOfPacketsInStreams}

    ${np1pktProcessStats} =  Catenate  {'vxlan_strip':0, 'fabric_path_strip':${noOfPackets}}
    ${tp2pktProcessStats} =  Catenate  {'vxlan_strip':${noOfPackets}, 'fabric_path_strip':0}

    compareStatsOnNetwork   ${np1ActualStats}  rxPackets=${noOfPackets}   rxBytes=${originalNofBytes}  txBytes=${noOfBytesAfterStrippingNp1}  txPackets=${noOfPackets}  pktProcessStats=${np1pktProcessStats}
    compareStatsOnFilter  ${f1ActualStats}  rxPackets=${noOfPackets}   rxBytes=${noOfBytesAfterStrippingNp1}  txBytes=${noOfBytesAfterStrippingNp1}  txPackets=${noOfPackets}
    compareStatsOnTool   ${tp1ActualStats}  rxPackets=${noOfPackets}   txBytes=${noOfBytesAfterStrippingNp1}  txPackets=${noOfPackets}
    compareStatsOnTool   ${tp2ActualStats}  rxPackets=${noOfPackets}   txBytes=${noOfBytesAfterStrippingTp2}  txPackets=${noOfPackets}  pktProcessStats=${tp2pktProcessStats}

    ${np1CfpStrippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${original_capturePcap}  verbose=${True}
    compareFrames  ${np1CfpStrippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}

    ${tp2CfpAndVxLANStrippedFrames}=  tp2.pktProc.emulate  ${pp2}  frames=${np1CfpStrippedFrames}  verbose=${True}
    compareFrames  ${tp2CfpAndVxLANStrippedFrames}  ${tp2Ixia.recdFrames}  ordered=${True}

    np1Ixia.disableInternalLoopback

AFM-VxLAN-STRIP-TP-TC000791224a
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f1}  ${tp2}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/Original.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/Original.pcap

    ${vlan_added_captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/Tagged.enc
    ${vlan_added_capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/Tagged.pcap

    ${vlan_vxlan_stripped_captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/Stripped.enc
    ${vlan_vxlan_stripped_capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/Stripped.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'std_port_tagging_settings': {'enabled': True, 'vlan_id': 777}}
    ${pp2}=  Catenate  {'vxlan_strip_settings': {'enabled': True}}
    ${pp3}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'egress_count': 2, 'strip_mode': 'EGRESS'}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:11:11:58',dst='00:22:22:22:22:58')/IPv6(src='2001:1cd9:22:33ab:4:5555:6667:7758',dst='2ffe:1944:100c:a23d:2f04:bc21:2500:d058')/UDP(sport=58581,dport=4789)/VXLAN(vni=16777215)/Ether(src='00:33:33:33:33:58',dst='00:44:44:44:44:58')/Dot1Q(vlan=3358,prio=100)/IPv6(src='3001:AAAA:BBBB:CCCC:DDDD:EEEE:FFF0:1158',dst='3ffe:1944:100:a:0:bc:2500:d058')  payload=incr  frameSize=156  numFrames=${noOfPacketsInStreams}
    ${stream2}=  createStream  s2  Ether(src='00:11:11:11:11:59',dst='00:22:22:22:22:59')/Dot1Q(vlan=459,prio=111)/IP(src='11.11.11.59',dst='22.22.22.59')/UDP(sport=59591,dport=4789)/VXLAN(vni=16777215)/Ether(src='00:33:33:33:33:59',dst='00:44:44:44:44:59')/Dot1Q(vlan=3359,prio=100)/IPv6(src='3001:AAAA:BBBB:CCCC:DDDD:EEEE:FFF0:1159',dst='3ffe:1944:100:a:0:bc:2500:d059')  payload=incr  frameSize=256  numFrames=${noOfPacketsInStreams}
    ${stream3}=  createStream  s3  Ether(src='00:11:11:11:11:60',dst='00:22:22:22:22:60')/Dot1Q(vlan=460,prio=111)/IPv6(src='2001:1cd9:22:33ab:4:5555:6667:7760',dst='2ffe:1944:100c:a23d:2f04:bc21:2500:d060')/UDP(sport=60601,dport=4789)/VXLAN(vni=16777215)/Ether(src='00:33:33:33:33:60',dst='00:44:44:44:44:60')/Dot1Q(vlan=3360,prio=100)/IPv6(src='3001:AAAA:BBBB:CCCC:DDDD:EEEE:FFF0:1160',dst='3ffe:1944:100:a:0:bc:2500:d060')  payload=incr  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${stream4}=  createStream  s4  Ether(src='00:11:11:11:11:57',dst='00:22:22:22:22:57')/IP(src='11.11.11.57',dst='22.22.22.57')/UDP(sport=57571,dport=4789)/VXLAN(vni=16777215)/Ether(src='00:33:33:33:33:57',dst='00:44:44:44:44:57')/Dot1Q(vlan=3357,prio=100)/IPv6(src='3001:AAAA:BBBB:CCCC:DDDD:EEEE:FFF0:1157',dst='3ffe:1944:100:a:0:bc:2500:d057')  payload=incr  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    afm1.attachResource  ${tp2}  ${bwAllocation}

    np1.configurePacketProcessing  ${pp1}
    tp2.configurePacketProcessing  ${pp2}
    tp2.configurePacketProcessing  ${pp3}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    np1Ixia.startIxCapture
    tp1Ixia.startIxCapture
    tp2Ixia.startIxCapture
    np1Ixia.runIxTraffic
    np1Ixia.stopIxCapture
    tp1Ixia.stopIxCapture
    tp2Ixia.stopIxCapture

    ${noOfStreams}=  evaluate  4
    ${noOfPackets}=  evaluate    ${noOfStreams} * ${noOfPacketsInStreams}
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${vlan_added_captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${vlan_added_captureFile}  ${vlan_added_capturePcap}
    tp1Ixia.scapyImportPcap  ${vlan_added_capturePcap}
    ${np1TaggedFrames}=  tp1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TaggedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}

    tp2Ixia.exportIxFramesToFile  ${vlan_vxlan_stripped_captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${vlan_vxlan_stripped_captureFile}  ${vlan_vxlan_stripped_capturePcap}
    tp2Ixia.scapyImportPcap  ${vlan_vxlan_stripped_capturePcap}

    log   Get statistics from the device
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${tp2ActualStats} =  tp2.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats

    #set expected statistics for port and filter
    ${originalNofBytes}=  evaluate    18844 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    18596 * ${noOfPacketsInStreams}
    ${noOfBytesAfterTagging}=  evaluate    18860 * ${noOfPacketsInStreams}

    ${tp2pktProcessStats} =  Catenate  {'vxlan_strip':${noOfPackets}}
    compareStatsOnNetwork   ${np1ActualStats}  rxPackets=${noOfPackets}   rxBytes=${originalNofBytes}  txBytes=${originalNofBytes}  txPackets=${noOfPackets}
    compareStatsOnFilter  ${f1ActualStats}  rxPackets=${noOfPackets}   rxBytes=${originalNofBytes}  txBytes=${originalNofBytes}  txPackets=${noOfPackets}
    compareStatsOnTool   ${tp1ActualStats}  rxPackets=${noOfPackets}   txBytes=${noOfBytesAfterTagging}  txPackets=${noOfPackets}
    compareStatsOnTool   ${tp2ActualStats}  rxPackets=${noOfPackets}   txBytes=${noOfBytesAfterStripping}  txPackets=${noOfPackets}  pktProcessStats=${tp2pktProcessStats}

    ${tp2VLANStrippedFrames}=  tp2.pktProc.emulate  ${pp3}  pcapFile=${vlan_added_capturePcap}   verbose=${True}
    ${tp2VxLANAndVLANStrippedFrames}=  tp2.pktProc.emulate  ${pp2}  frames=${tp2VLANStrippedFrames}  verbose=${True}
    compareFrames  ${tp2VxLANAndVLANStrippedFrames}  ${tp2Ixia.recdFrames}  ordered=${True}

*** Keywords ***
setupSuiteEnv
#    log  Release resource pool: ${RESOURCEPOOL}  console=true
#    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myRequestDef}=  Catenate  {'myPorts': {'type':'NTOPort',
    ...                                         'properties':{'ConnectionType':'standard'}},
    ...                          'myNTO': {'type': 'NTO'}}

    ${myMap}=  Catenate  {'myNTO': {'myPorts': ['np1', 'np2', 'tp1', 'tp2']}}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myReservation}=  requestResourceBlocking  ${RESOURCETIMEOUT}  ${RESOURCEPOOL}  ${myRequestDef}  requestMapParentBound=${myMap}
    Set Global Variable  ${myReservation}

    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  np2  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp2  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR    ${nto1}  afm1  L1-AFM

suiteCleanup
    releaseResources   ${myReservation['responseId']}

testCleanup
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts
    nto1.cleanupEphemeralNamespace
