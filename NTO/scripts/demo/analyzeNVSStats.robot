*** Settings ***
Suite Setup     setupSuiteEnv
Suite Teardown  suiteCleanup
Test Teardown   testCleanup

#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-02.ann.is.keysight.com
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  60

*** Test Cases ***
Single_Single_Single

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66", "00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2', 's3', 's4']}, 'Filter': {'f1': ['s1', 's3']}, 'Tool': {'tp1': ['s1', 's3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

Single_Single_Dual

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66", "00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f1}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2', 's3', 's4']}, 'Filter': {'f1': ['s1', 's3']}, 'Tool': {'tp1': ['s1', 's3'], 'tp2': ['s1', 's3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

Single_Dual_Single

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f2}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2', 's3', 's4']}, 'Filter': {'f1': ['s1'], 'f2': ['s3']}, 'Tool': {'tp1': ['s1', 's3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

Single_Dual_Dual

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f2}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2', 's3', 's4']}, 'Filter': {'f1': ['s1'], 'f2': ['s3']}, 'Tool': {'tp1': ['s1'], 'tp2': ['s3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

Dual_Single_Single

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66", "00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np2}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic
    np2Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2'], 'np2': ['s3', 's4']}, 'Filter': {'f1': ['s1', 's3']}, 'Tool': {'tp1': ['s1', 's3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

Dual_Single_Dual

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np2}  ${f1}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic
    np2Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2'], 'np2': ['s3', 's4']}, 'Filter': {'f1': ['s1']}, 'Tool': {'tp1': ['s1'], 'tp2': ['s1']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

Dual_Dual_Single

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np2}  ${f2}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic
    np2Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2'], 'np2': ['s3', 's4']}, 'Filter': {'f1': ['s1'], 'f2': ['s3']}, 'Tool': {'tp1': ['s1', 's3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

Dual_Dual_Dual

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np2}  ${f2}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic
    np2Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2'], 'np2': ['s3', 's4']}, 'Filter': {'f1': ['s1'], 'f2': ['s3']}, 'Tool': {'tp1': ['s1'], 'tp2': ['s3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

Dual_Mesh_Dual_Dual

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f2}  ${tp2}
    AddConn    ${nto1}  c3  ${np2}  ${f2}  ${tp2}
    AddConn    ${nto1}  c4  ${np2}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic
    np2Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2'], 'np2': ['s3', 's4']}, 'Filter': {'f1': ['s1'], 'f2': ['s3']}, 'Tool': {'tp1': ['s1'], 'tp2': ['s3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

Dual_Dual_Mesh_Dual

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f1}  ${tp2}
    AddConn    ${nto1}  c3  ${np2}  ${f2}  ${tp1}
    AddConn    ${nto1}  c4  ${np2}  ${f2}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic
    np2Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2'], 'np2': ['s3', 's4']}, 'Filter': {'f1': ['s1'], 'f2': ['s3']}, 'Tool': {'tp1': ['s1', 's3'], 'tp2': ['s1', 's3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

Dual_Mesh_Dual_Mesh_Dual

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f1}  ${tp2}
    AddConn    ${nto1}  c3  ${np1}  ${f2}  ${tp2}
    AddConn    ${nto1}  c4  ${np2}  ${f2}  ${tp1}
    AddConn    ${nto1}  c5  ${np2}  ${f2}  ${tp2}
    AddConn    ${nto1}  c6  ${np2}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic
    np2Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2'], 'np2': ['s3', 's4']}, 'Filter': {'f1': ['s1'], 'f2': ['s3']}, 'Tool': {'tp1': ['s1', 's3'], 'tp2': ['s1', 's3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

*** Keywords ***
setupSuiteEnv
#    log  Release resource pool: ${RESOURCEPOOL}  console=true
#    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

#    ${myRequestDef}=  Catenate  {'myPorts': {'type':'NTOPort',
#    ...                                         'properties':{'ConnectionType':'standard'}},
#    ...                          'myNTO': {'type': 'NTO'}}
#
#    ${myMap}=  Catenate  {'myNTO': {'myPorts': ['np1', 'np2', 'tp1', 'tp2']}}

#    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
#    ${myReservation}=  requestResourceBlocking  ${RESOURCETIMEOUT}  ${RESOURCEPOOL}  ${myRequestDef}  requestMapParentBound=${myMap}
#    Set Global Variable  ${myReservation}

#    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

#    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
#    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}
#    AddPort  ${nto1}  np2  NETWORK  ${myReservation['myNTO']['myPorts']}
#    AddPort  ${nto1}  tp2  TOOL     ${myReservation['myNTO']['myPorts']}

    ${myRequestDef}=  Catenate  {'myPorts': {'type':'NTOPort',
    ...                                      'properties':{'ConnectionType':'standard'}}}

    ${myMap}=  Catenate  {'myPorts': ['np1', 'np2', 'tp1', 'tp2']}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myReservation}=  requestResourceBlocking  ${RESOURCETIMEOUT}  ${RESOURCEPOOL}  ${myRequestDef}  requestMap=${myMap}
    Set Global Variable  ${myReservation}

    ${myNTOInfo}=  getResourceInfoById  ${myReservation['myPorts']['np1']['parent']}

    AddNTO  nto1  ${myNTOInfo['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myPorts']}
    AddPort  ${nto1}  np2  NETWORK  ${myReservation['myPorts']}
    AddPort  ${nto1}  tp2  TOOL     ${myReservation['myPorts']}

suiteCleanup
    releaseResources   ${myReservation['responseId']}

testCleanup
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts
    nto1.cleanupEphemeralNamespace