*** Settings ***
Suite Setup    setupSuiteEnv

#Libraries
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Test Cases ***
Snake_Test

    AddNTOLocal  nto1  10.218.164.212

   #40Gb Card P2A port definitions.
    &{P2A-01Def}=  Create Dictionary   name=P2A-01   port=P2A-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2A-02Def}=  Create Dictionary   name=P2A-02   port=P2A-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2A-03Def}=  Create Dictionary   name=P2A-03   port=P2A-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2A-04Def}=  Create Dictionary   name=P2A-04   port=P2A-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G


    AddPort  ${nto1}  ${P2A-01Def}
    AddPort  ${nto1}  ${P2A-02Def}
    AddPort  ${nto1}  ${P2A-03Def}
    AddPort  ${nto1}  ${P2A-04Def}


    #40Gb Card P2B port definitions.
    &{P2B-01Def}=  Create Dictionary   name=P2B-01   port=P2B-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2B-02Def}=  Create Dictionary   name=P2B-02   port=P2B-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2B-03Def}=  Create Dictionary   name=P2B-03   port=P2B-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2B-04Def}=  Create Dictionary   name=P2B-04   port=P2B-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G

    AddPort  ${nto1}  ${P2B-01Def}
    AddPort  ${nto1}  ${P2B-02Def}
    AddPort  ${nto1}  ${P2B-03Def}
    AddPort  ${nto1}  ${P2B-04Def}


    #10Gb Card P3 port definitions.
    &{P3-01Def}=  Create Dictionary   name=P3-01   port=P3-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-02Def}=  Create Dictionary   name=P3-02   port=P3-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-03Def}=  Create Dictionary   name=P3-03   port=P3-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-04Def}=  Create Dictionary   name=P3-04   port=P3-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-05Def}=  Create Dictionary   name=P3-05   port=P3-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-06Def}=  Create Dictionary   name=P3-06   port=P3-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-07Def}=  Create Dictionary   name=P3-07   port=P3-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-08Def}=  Create Dictionary   name=P3-08   port=P3-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-09Def}=  Create Dictionary   name=P3-09   port=P3-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-10Def}=  Create Dictionary   name=P3-10   port=P3-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-11Def}=  Create Dictionary   name=P3-11   port=P3-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-12Def}=  Create Dictionary   name=P3-12   port=P3-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-13Def}=  Create Dictionary   name=P3-13   port=P3-13   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-14Def}=  Create Dictionary   name=P3-14   port=P3-14   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-15Def}=  Create Dictionary   name=P3-15   port=P3-15   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-16Def}=  Create Dictionary   name=P3-16   port=P3-16   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-17Def}=  Create Dictionary   name=P3-17   port=P3-17   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-18Def}=  Create Dictionary   name=P3-18   port=P3-18   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-19Def}=  Create Dictionary   name=P3-19   port=P3-19   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-20Def}=  Create Dictionary   name=P3-20   port=P3-20   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-21Def}=  Create Dictionary   name=P3-21   port=P3-21   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-22Def}=  Create Dictionary   name=P3-22   port=P3-22   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-23Def}=  Create Dictionary   name=P3-23   port=P3-23   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-24Def}=  Create Dictionary   name=P3-24   port=P3-24   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-25Def}=  Create Dictionary   name=P3-25   port=P3-25   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-26Def}=  Create Dictionary   name=P3-26   port=P3-26   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-27Def}=  Create Dictionary   name=P3-27   port=P3-27   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-28Def}=  Create Dictionary   name=P3-28   port=P3-28   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-29Def}=  Create Dictionary   name=P3-29   port=P3-29   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-30Def}=  Create Dictionary   name=P3-30   port=P3-30   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-31Def}=  Create Dictionary   name=P3-31   port=P3-31   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-32Def}=  Create Dictionary   name=P3-32   port=P3-32   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-33Def}=  Create Dictionary   name=P3-33   port=P3-33   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-34Def}=  Create Dictionary   name=P3-34   port=P3-34   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-35Def}=  Create Dictionary   name=P3-35   port=P3-35   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-36Def}=  Create Dictionary   name=P3-36   port=P3-36   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-37Def}=  Create Dictionary   name=P3-37   port=P3-37   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-38Def}=  Create Dictionary   name=P3-38   port=P3-38   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-39Def}=  Create Dictionary   name=P3-39   port=P3-39   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-40Def}=  Create Dictionary   name=P3-40   port=P3-40   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-41Def}=  Create Dictionary   name=P3-41   port=P3-41   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-42Def}=  Create Dictionary   name=P3-42   port=P3-42   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-43Def}=  Create Dictionary   name=P3-43   port=P3-43   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-44Def}=  Create Dictionary   name=P3-44   port=P3-44   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-45Def}=  Create Dictionary   name=P3-45   port=P3-45   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-46Def}=  Create Dictionary   name=P3-46   port=P3-46   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-47Def}=  Create Dictionary   name=P3-47   port=P3-47   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-48Def}=  Create Dictionary   name=P3-48   port=P3-48   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P3-01Def}
    AddPort  ${nto1}  ${P3-02Def}
    AddPort  ${nto1}  ${P3-03Def}
    AddPort  ${nto1}  ${P3-04Def}
    AddPort  ${nto1}  ${P3-05Def}
    AddPort  ${nto1}  ${P3-06Def}
    AddPort  ${nto1}  ${P3-07Def}
    AddPort  ${nto1}  ${P3-08Def}
    AddPort  ${nto1}  ${P3-09Def}
    AddPort  ${nto1}  ${P3-10Def}
    AddPort  ${nto1}  ${P3-11Def}
    AddPort  ${nto1}  ${P3-12Def}
    AddPort  ${nto1}  ${P3-13Def}
    AddPort  ${nto1}  ${P3-14Def}
    AddPort  ${nto1}  ${P3-15Def}
    AddPort  ${nto1}  ${P3-16Def}
    AddPort  ${nto1}  ${P3-17Def}
    AddPort  ${nto1}  ${P3-18Def}
    AddPort  ${nto1}  ${P3-19Def}
    AddPort  ${nto1}  ${P3-20Def}
    AddPort  ${nto1}  ${P3-21Def}
    AddPort  ${nto1}  ${P3-22Def}
    AddPort  ${nto1}  ${P3-23Def}
    AddPort  ${nto1}  ${P3-24Def}
    AddPort  ${nto1}  ${P3-25Def}
    AddPort  ${nto1}  ${P3-26Def}
    AddPort  ${nto1}  ${P3-27Def}
    AddPort  ${nto1}  ${P3-28Def}
    AddPort  ${nto1}  ${P3-29Def}
    AddPort  ${nto1}  ${P3-30Def}
    AddPort  ${nto1}  ${P3-31Def}
    AddPort  ${nto1}  ${P3-32Def}
    AddPort  ${nto1}  ${P3-33Def}
    AddPort  ${nto1}  ${P3-34Def}
    AddPort  ${nto1}  ${P3-35Def}
    AddPort  ${nto1}  ${P3-36Def}
    AddPort  ${nto1}  ${P3-37Def}
    AddPort  ${nto1}  ${P3-38Def}
    AddPort  ${nto1}  ${P3-39Def}
    AddPort  ${nto1}  ${P3-40Def}
    AddPort  ${nto1}  ${P3-41Def}
    AddPort  ${nto1}  ${P3-42Def}
    AddPort  ${nto1}  ${P3-43Def}
    AddPort  ${nto1}  ${P3-44Def}
    AddPort  ${nto1}  ${P3-45Def}
    AddPort  ${nto1}  ${P3-46Def}
    AddPort  ${nto1}  ${P3-47Def}
    AddPort  ${nto1}  ${P3-48Def}

    #10Gb Card P4 port definitions.
    &{P4-01Def}=  Create Dictionary   name=P4-01   port=P4-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-02Def}=  Create Dictionary   name=P4-02   port=P4-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-03Def}=  Create Dictionary   name=P4-03   port=P4-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-04Def}=  Create Dictionary   name=P4-04   port=P4-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-05Def}=  Create Dictionary   name=P4-05   port=P4-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-06Def}=  Create Dictionary   name=P4-06   port=P4-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-07Def}=  Create Dictionary   name=P4-07   port=P4-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-08Def}=  Create Dictionary   name=P4-08   port=P4-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-09Def}=  Create Dictionary   name=P4-09   port=P4-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-10Def}=  Create Dictionary   name=P4-10   port=P4-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-11Def}=  Create Dictionary   name=P4-11   port=P4-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-12Def}=  Create Dictionary   name=P4-12   port=P4-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-13Def}=  Create Dictionary   name=P4-13   port=P4-13   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-14Def}=  Create Dictionary   name=P4-14   port=P4-14   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-15Def}=  Create Dictionary   name=P4-15   port=P4-15   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-16Def}=  Create Dictionary   name=P4-16   port=P4-16   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-17Def}=  Create Dictionary   name=P4-17   port=P4-17   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-18Def}=  Create Dictionary   name=P4-18   port=P4-18   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-19Def}=  Create Dictionary   name=P4-19   port=P4-19   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-20Def}=  Create Dictionary   name=P4-20   port=P4-20   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-21Def}=  Create Dictionary   name=P4-21   port=P4-21   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-22Def}=  Create Dictionary   name=P4-22   port=P4-22   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-23Def}=  Create Dictionary   name=P4-23   port=P4-23   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-24Def}=  Create Dictionary   name=P4-24   port=P4-24   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-25Def}=  Create Dictionary   name=P4-25   port=P4-25   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-26Def}=  Create Dictionary   name=P4-26   port=P4-26   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-27Def}=  Create Dictionary   name=P4-27   port=P4-27   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-28Def}=  Create Dictionary   name=P4-28   port=P4-28   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-29Def}=  Create Dictionary   name=P4-29   port=P4-29   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-30Def}=  Create Dictionary   name=P4-30   port=P4-30   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-31Def}=  Create Dictionary   name=P4-31   port=P4-31   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-32Def}=  Create Dictionary   name=P4-32   port=P4-32   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-33Def}=  Create Dictionary   name=P4-33   port=P4-33   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-34Def}=  Create Dictionary   name=P4-34   port=P4-34   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-35Def}=  Create Dictionary   name=P4-35   port=P4-35   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-36Def}=  Create Dictionary   name=P4-36   port=P4-36   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-37Def}=  Create Dictionary   name=P4-37   port=P4-37   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-38Def}=  Create Dictionary   name=P4-38   port=P4-38   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-39Def}=  Create Dictionary   name=P4-39   port=P4-39   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-40Def}=  Create Dictionary   name=P4-40   port=P4-40   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-41Def}=  Create Dictionary   name=P4-41   port=P4-41   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-42Def}=  Create Dictionary   name=P4-42   port=P4-42   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-43Def}=  Create Dictionary   name=P4-43   port=P4-43   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-44Def}=  Create Dictionary   name=P4-44   port=P4-44   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-45Def}=  Create Dictionary   name=P4-45   port=P4-45   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-46Def}=  Create Dictionary   name=P4-46   port=P4-46   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-47Def}=  Create Dictionary   name=P4-47   port=P4-47   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-48Def}=  Create Dictionary   name=P4-48   port=P4-48   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P4-01Def}
    AddPort  ${nto1}  ${P4-02Def}
    AddPort  ${nto1}  ${P4-03Def}
    AddPort  ${nto1}  ${P4-04Def}
    AddPort  ${nto1}  ${P4-05Def}
    AddPort  ${nto1}  ${P4-06Def}
    AddPort  ${nto1}  ${P4-07Def}
    AddPort  ${nto1}  ${P4-08Def}
    AddPort  ${nto1}  ${P4-09Def}
    AddPort  ${nto1}  ${P4-10Def}
    AddPort  ${nto1}  ${P4-11Def}
    AddPort  ${nto1}  ${P4-12Def}
    AddPort  ${nto1}  ${P4-13Def}
    AddPort  ${nto1}  ${P4-14Def}
    AddPort  ${nto1}  ${P4-15Def}
    AddPort  ${nto1}  ${P4-16Def}
    AddPort  ${nto1}  ${P4-17Def}
    AddPort  ${nto1}  ${P4-18Def}
    AddPort  ${nto1}  ${P4-19Def}
    AddPort  ${nto1}  ${P4-20Def}
    AddPort  ${nto1}  ${P4-21Def}
    AddPort  ${nto1}  ${P4-22Def}
    AddPort  ${nto1}  ${P4-23Def}
    AddPort  ${nto1}  ${P4-24Def}
    AddPort  ${nto1}  ${P4-25Def}
    AddPort  ${nto1}  ${P4-26Def}
    AddPort  ${nto1}  ${P4-27Def}
    AddPort  ${nto1}  ${P4-28Def}
    AddPort  ${nto1}  ${P4-29Def}
    AddPort  ${nto1}  ${P4-30Def}
    AddPort  ${nto1}  ${P4-31Def}
    AddPort  ${nto1}  ${P4-32Def}
    AddPort  ${nto1}  ${P4-33Def}
    AddPort  ${nto1}  ${P4-34Def}
    AddPort  ${nto1}  ${P4-35Def}
    AddPort  ${nto1}  ${P4-36Def}
    AddPort  ${nto1}  ${P4-37Def}
    AddPort  ${nto1}  ${P4-38Def}
    AddPort  ${nto1}  ${P4-39Def}
    AddPort  ${nto1}  ${P4-40Def}
    AddPort  ${nto1}  ${P4-41Def}
    AddPort  ${nto1}  ${P4-42Def}
    AddPort  ${nto1}  ${P4-43Def}
    AddPort  ${nto1}  ${P4-44Def}
    AddPort  ${nto1}  ${P4-45Def}
    AddPort  ${nto1}  ${P4-46Def}
    AddPort  ${nto1}  ${P4-47Def}
    AddPort  ${nto1}  ${P4-48Def}

    #100Gb Card P5 port definitions.
    &{P5-01Def}=  Create Dictionary   name=P5-01   port=P5-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P5-02Def}=  Create Dictionary   name=P5-02   port=P5-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P5-03Def}=  Create Dictionary   name=P5-03   port=P5-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P5-04Def}=  Create Dictionary   name=P5-04   port=P5-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P5-05Def}=  Create Dictionary   name=P5-05   port=P5-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P5-06Def}=  Create Dictionary   name=P5-06   port=P5-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P5-07Def}=  Create Dictionary   name=P5-07   port=P5-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P5-08Def}=  Create Dictionary   name=P5-08   port=P5-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P5-09Def}=  Create Dictionary   name=P5-09   port=P5-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P5-10Def}=  Create Dictionary   name=P5-10   port=P5-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P5-11Def}=  Create Dictionary   name=P5-11   port=P5-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P5-12Def}=  Create Dictionary   name=P5-12   port=P5-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
#
    AddPort  ${nto1}  ${P5-01Def}
    AddPort  ${nto1}  ${P5-02Def}
    AddPort  ${nto1}  ${P5-03Def}
    AddPort  ${nto1}  ${P5-04Def}
    AddPort  ${nto1}  ${P5-05Def}
    AddPort  ${nto1}  ${P5-06Def}
    AddPort  ${nto1}  ${P5-07Def}
    AddPort  ${nto1}  ${P5-08Def}
    AddPort  ${nto1}  ${P5-09Def}
    AddPort  ${nto1}  ${P5-10Def}
    AddPort  ${nto1}  ${P5-11Def}
    AddPort  ${nto1}  ${P5-12Def}


    #100Gb Card P6 port definitions.
    &{P6-01Def}=  Create Dictionary   name=P6-01   port=P6-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P6-02Def}=  Create Dictionary   name=P6-02   port=P6-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P6-03Def}=  Create Dictionary   name=P6-03   port=P6-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P6-04Def}=  Create Dictionary   name=P6-04   port=P6-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P6-05Def}=  Create Dictionary   name=P6-05   port=P6-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P6-06Def}=  Create Dictionary   name=P6-06   port=P6-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P6-07Def}=  Create Dictionary   name=P6-07   port=P6-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P6-08Def}=  Create Dictionary   name=P6-08   port=P6-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P6-09Def}=  Create Dictionary   name=P6-09   port=P6-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P6-10Def}=  Create Dictionary   name=P6-10   port=P6-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P6-11Def}=  Create Dictionary   name=P6-11   port=P6-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28
    &{P6-12Def}=  Create Dictionary   name=P6-12   port=P6-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP28

    AddPort  ${nto1}  ${P6-01Def}
    AddPort  ${nto1}  ${P6-02Def}
    AddPort  ${nto1}  ${P6-03Def}
    AddPort  ${nto1}  ${P6-04Def}
    AddPort  ${nto1}  ${P6-05Def}
    AddPort  ${nto1}  ${P6-06Def}
    AddPort  ${nto1}  ${P6-07Def}
    AddPort  ${nto1}  ${P6-08Def}
    AddPort  ${nto1}  ${P6-09Def}
    AddPort  ${nto1}  ${P6-10Def}
    AddPort  ${nto1}  ${P6-11Def}
    AddPort  ${nto1}  ${P6-12Def}


    #40Gb Card P1 port definitions.
    &{P1-01Def}=  Create Dictionary   name=P1-01   port=P1-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-02Def}=  Create Dictionary   name=P1-02   port=P1-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-03Def}=  Create Dictionary   name=P1-03   port=P1-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-04Def}=  Create Dictionary   name=P1-04   port=P1-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-05Def}=  Create Dictionary   name=P1-05   port=P1-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-06Def}=  Create Dictionary   name=P1-06   port=P1-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-07Def}=  Create Dictionary   name=P1-07   port=P1-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-08Def}=  Create Dictionary   name=P1-08   port=P1-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-09Def}=  Create Dictionary   name=P1-09   port=P1-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-10Def}=  Create Dictionary   name=P1-10   port=P1-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-11Def}=  Create Dictionary   name=P1-11   port=P1-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-12Def}=  Create Dictionary   name=P1-12   port=P1-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-13Def}=  Create Dictionary   name=P1-13   port=P1-13   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-14Def}=  Create Dictionary   name=P1-14   port=P1-14   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-15Def}=  Create Dictionary   name=P1-15   port=P1-15   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-16Def}=  Create Dictionary   name=P1-16   port=P1-16   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G

    AddPort  ${nto1}  ${P1-01Def}
    AddPort  ${nto1}  ${P1-02Def}
    AddPort  ${nto1}  ${P1-03Def}
    AddPort  ${nto1}  ${P1-04Def}
    AddPort  ${nto1}  ${P1-05Def}
    AddPort  ${nto1}  ${P1-06Def}
    AddPort  ${nto1}  ${P1-07Def}
    AddPort  ${nto1}  ${P1-08Def}
    AddPort  ${nto1}  ${P1-09Def}
    AddPort  ${nto1}  ${P1-10Def}
    AddPort  ${nto1}  ${P1-11Def}
    AddPort  ${nto1}  ${P1-12Def}
    AddPort  ${nto1}  ${P1-13Def}
    AddPort  ${nto1}  ${P1-14Def}
    AddPort  ${nto1}  ${P1-15Def}
    AddPort  ${nto1}  ${P1-16Def}

    &{P1-01IxiaDef}=  Create Dictionary  ixIPaddr=10.218.164.237  ixPort=1/7/18  phyMode=Fiber
    AddIxiaPort  ${P1-01}  ${P1-01IxiaDef}

    &{P3-01IxiaDef}=  Create Dictionary  ixIPaddr=10.218.164.237  ixPort=1/2/13  phyMode=Fiber
    AddIxiaPort  ${P3-01}  ${P3-01IxiaDef}

    &{P5-01IxiaDef}=  Create Dictionary  ixIPaddr=10.218.164.239  ixPort=1/8/1  phyMode=Fiber
    AddIxiaPort  ${P5-01}  ${P5-01IxiaDef}


    #40Gb Card P2 port definitions.
    &{P2-01Def}=  Create Dictionary   name=P2-01   port=P2-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2-02Def}=  Create Dictionary   name=P2-02   port=P2-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2-03Def}=  Create Dictionary   name=P2-03   port=P2-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2-04Def}=  Create Dictionary   name=P2-04   port=P2-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G

    AddPort  ${nto1}  ${P2-01Def}
    AddPort  ${nto1}  ${P2-02Def}
    AddPort  ${nto1}  ${P2-03Def}
    AddPort  ${nto1}  ${P2-04Def}

# Fiters for 40G cards in slots 1 & 2
    AddFilter  ${nto1}  f1    PASS_ALL
    AddFilter  ${nto1}  f2    PASS_ALL
    AddFilter  ${nto1}  f3    PASS_ALL
    AddFilter  ${nto1}  f4    PASS_ALL
    AddFilter  ${nto1}  f5    PASS_ALL
    AddFilter  ${nto1}  f6    PASS_ALL
    AddFilter  ${nto1}  f7    PASS_ALL
    AddFilter  ${nto1}  f8    PASS_ALL
    AddFilter  ${nto1}  f9    PASS_ALL
    AddFilter  ${nto1}  f10   PASS_ALL
    AddFilter  ${nto1}  f11   PASS_ALL
    AddFilter  ${nto1}  f12   PASS_ALL
    AddFilter  ${nto1}  f13   PASS_ALL
    AddFilter  ${nto1}  f14   PASS_ALL
    AddFilter  ${nto1}  f15   PASS_ALL
    AddFilter  ${nto1}  f16   PASS_ALL
    AddFilter  ${nto1}  f17   PASS_ALL
    AddFilter  ${nto1}  f18   PASS_ALL
    AddFilter  ${nto1}  f19   PASS_ALL
    AddFilter  ${nto1}  f20   PASS_ALL
    AddFilter  ${nto1}  f21   PASS_ALL
    AddFilter  ${nto1}  f22   PASS_ALL
    AddFilter  ${nto1}  f23   PASS_ALL
    AddFilter  ${nto1}  f24   PASS_ALL
    AddFilter  ${nto1}  f25   PASS_ALL
    AddFilter  ${nto1}  f26   PASS_ALL
    AddFilter  ${nto1}  f27   PASS_ALL
    AddFilter  ${nto1}  f28   PASS_ALL

# Filters for Gibson cards in slots 5 & 6
    AddFilter  ${nto1}  f29   PASS_ALL
    AddFilter  ${nto1}  f30   PASS_ALL
    AddFilter  ${nto1}  f31   PASS_ALL
    AddFilter  ${nto1}  f32   PASS_ALL
    AddFilter  ${nto1}  f33   PASS_ALL
    AddFilter  ${nto1}  f34   PASS_ALL
    AddFilter  ${nto1}  f35   PASS_ALL
    AddFilter  ${nto1}  f36   PASS_ALL
    AddFilter  ${nto1}  f37   PASS_ALL
    AddFilter  ${nto1}  f38   PASS_ALL
    AddFilter  ${nto1}  f39   PASS_ALL
    AddFilter  ${nto1}  f40   PASS_ALL
    AddFilter  ${nto1}  f41   PASS_ALL
    AddFilter  ${nto1}  f42   PASS_ALL
    AddFilter  ${nto1}  f43   PASS_ALL
    AddFilter  ${nto1}  f44   PASS_ALL
    AddFilter  ${nto1}  f45   PASS_ALL
    AddFilter  ${nto1}  f46   PASS_ALL
    AddFilter  ${nto1}  f47   PASS_ALL
    AddFilter  ${nto1}  f48   PASS_ALL
    AddFilter  ${nto1}  f49   PASS_ALL
    AddFilter  ${nto1}  f50   PASS_ALL
    AddFilter  ${nto1}  f51   PASS_ALL
    AddFilter  ${nto1}  f52   PASS_ALL

# Filters for 10G line cards in slots 3 & 4
   AddFilter  ${nto1}  f53   PASS_ALL
   AddFilter  ${nto1}  f54   PASS_ALL
   AddFilter  ${nto1}  f55   PASS_ALL
   AddFilter  ${nto1}  f56   PASS_ALL
   AddFilter  ${nto1}  f57   PASS_ALL
   AddFilter  ${nto1}  f58   PASS_ALL
   AddFilter  ${nto1}  f59   PASS_ALL
   AddFilter  ${nto1}  f60   PASS_ALL
   AddFilter  ${nto1}  f61   PASS_ALL
   AddFilter  ${nto1}  f62   PASS_ALL
   AddFilter  ${nto1}  f63   PASS_ALL
   AddFilter  ${nto1}  f64   PASS_ALL
   AddFilter  ${nto1}  f65   PASS_ALL
   AddFilter  ${nto1}  f66   PASS_ALL
   AddFilter  ${nto1}  f67   PASS_ALL
   AddFilter  ${nto1}  f68   PASS_ALL
   AddFilter  ${nto1}  f69   PASS_ALL
   AddFilter  ${nto1}  f70   PASS_ALL
   AddFilter  ${nto1}  f71   PASS_ALL
   AddFilter  ${nto1}  f72   PASS_ALL
   AddFilter  ${nto1}  f73   PASS_ALL
   AddFilter  ${nto1}  f74   PASS_ALL
   AddFilter  ${nto1}  f75   PASS_ALL
   AddFilter  ${nto1}  f76   PASS_ALL
   AddFilter  ${nto1}  f77   PASS_ALL
   AddFilter  ${nto1}  f78   PASS_ALL
   AddFilter  ${nto1}  f79   PASS_ALL
   AddFilter  ${nto1}  f80   PASS_ALL
   AddFilter  ${nto1}  f81   PASS_ALL
   AddFilter  ${nto1}  f82   PASS_ALL
   AddFilter  ${nto1}  f83   PASS_ALL
   AddFilter  ${nto1}  f84   PASS_ALL
   AddFilter  ${nto1}  f85   PASS_ALL
   AddFilter  ${nto1}  f86   PASS_ALL
   AddFilter  ${nto1}  f87   PASS_ALL
   AddFilter  ${nto1}  f88   PASS_ALL
   AddFilter  ${nto1}  f89   PASS_ALL
   AddFilter  ${nto1}  f90   PASS_ALL
   AddFilter  ${nto1}  f91   PASS_ALL
   AddFilter  ${nto1}  f92   PASS_ALL
   AddFilter  ${nto1}  f93   PASS_ALL
   AddFilter  ${nto1}  f94   PASS_ALL
   AddFilter  ${nto1}  f95   PASS_ALL
   AddFilter  ${nto1}  f96   PASS_ALL
   AddFilter  ${nto1}  f97   PASS_ALL
   AddFilter  ${nto1}  f98   PASS_ALL
   AddFilter  ${nto1}  f99   PASS_ALL
   AddFilter  ${nto1}  f100  PASS_ALL
   AddFilter  ${nto1}  f101  PASS_ALL
   AddFilter  ${nto1}  f102  PASS_ALL
   AddFilter  ${nto1}  f103  PASS_ALL
   AddFilter  ${nto1}  f104  PASS_ALL
   AddFilter  ${nto1}  f105  PASS_ALL
   AddFilter  ${nto1}  f106  PASS_ALL
   AddFilter  ${nto1}  f107  PASS_ALL
   AddFilter  ${nto1}  f108  PASS_ALL
   AddFilter  ${nto1}  f109  PASS_ALL
   AddFilter  ${nto1}  f110  PASS_ALL
   AddFilter  ${nto1}  f111  PASS_ALL
   AddFilter  ${nto1}  f112  PASS_ALL
   AddFilter  ${nto1}  f113  PASS_ALL
   AddFilter  ${nto1}  f114  PASS_ALL
   AddFilter  ${nto1}  f115  PASS_ALL
   AddFilter  ${nto1}  f116  PASS_ALL
   AddFilter  ${nto1}  f117  PASS_ALL
   AddFilter  ${nto1}  f118  PASS_ALL
   AddFilter  ${nto1}  f119  PASS_ALL
   AddFilter  ${nto1}  f120  PASS_ALL
   AddFilter  ${nto1}  f121  PASS_ALL
    AddFilter  ${nto1}  f122  PASS_ALL
    AddFilter  ${nto1}  f123  PASS_ALL
    AddFilter  ${nto1}  f124  PASS_ALL
    AddFilter  ${nto1}  f125  PASS_ALL
    AddFilter  ${nto1}  f126  PASS_ALL
    AddFilter  ${nto1}  f127  PASS_ALL
    AddFilter  ${nto1}  f128  PASS_ALL
    AddFilter  ${nto1}  f129  PASS_ALL
    AddFilter  ${nto1}  f130  PASS_ALL
    AddFilter  ${nto1}  f131  PASS_ALL
    AddFilter  ${nto1}  f132  PASS_ALL
    AddFilter  ${nto1}  f133  PASS_ALL
    AddFilter  ${nto1}  f134  PASS_ALL
    AddFilter  ${nto1}  f135  PASS_ALL
    AddFilter  ${nto1}  f136  PASS_ALL
    AddFilter  ${nto1}  f137  PASS_ALL
    AddFilter  ${nto1}  f138  PASS_ALL
    AddFilter  ${nto1}  f139  PASS_ALL
    AddFilter  ${nto1}  f140  PASS_ALL
    AddFilter  ${nto1}  f141  PASS_ALL
    AddFilter  ${nto1}  f142  PASS_ALL
    AddFilter  ${nto1}  f143  PASS_ALL
    AddFilter  ${nto1}  f144  PASS_ALL
    AddFilter  ${nto1}  f145  PASS_ALL
    AddFilter  ${nto1}  f146  PASS_ALL
    AddFilter  ${nto1}  f147  PASS_ALL
    AddFilter  ${nto1}  f148  PASS_ALL

# Extra Filters Not Needed for Gibson
#   AddFilter  ${nto1}  f149  PASS_ALL
#   AddFilter  ${nto1}  f150  PASS_ALL
#   AddFilter  ${nto1}  f151  PASS_ALL
#   AddFilter  ${nto1}  f152  PASS_ALL
#   AddFilter  ${nto1}  f153  PASS_ALL
#   AddFilter  ${nto1}  f154  PASS_ALL
#   AddFilter  ${nto1}  f155  PASS_ALL
#   AddFilter  ${nto1}  f156  PASS_ALL
#   AddFilter  ${nto1}  f157  PASS_ALL
#   AddFilter  ${nto1}  f158  PASS_ALL
#   AddFilter  ${nto1}  f159  PASS_ALL
#   AddFilter  ${nto1}  f160  PASS_ALL
#   AddFilter  ${nto1}  f161  PASS_ALL
#   AddFilter  ${nto1}  f162  PASS_ALL
#   AddFilter  ${nto1}  f163  PASS_ALL
#   AddFilter  ${nto1}  f164  PASS_ALL
#   AddFilter  ${nto1}  f165  PASS_ALL
#   AddFilter  ${nto1}  f166  PASS_ALL
#   AddFilter  ${nto1}  f167  PASS_ALL
#   AddFilter  ${nto1}  f168  PASS_ALL
#   AddFilter  ${nto1}  f169  PASS_ALL
#   AddFilter  ${nto1}  f170  PASS_ALL
#   AddFilter  ${nto1}  f171  PASS_ALL
#   AddFilter  ${nto1}  f172  PASS_ALL
#   AddFilter  ${nto1}  f173  PASS_ALL
#   AddFilter  ${nto1}  f174  PASS_ALL
#   AddFilter  ${nto1}  f175  PASS_ALL
#   AddFilter  ${nto1}  f176  PASS_ALL
#   AddFilter  ${nto1}  f177  PASS_ALL
#   AddFilter  ${nto1}  f178  PASS_ALL
#   AddFilter  ${nto1}  f179  PASS_ALL
#   AddFilter  ${nto1}  f180  PASS_ALL
#   AddFilter  ${nto1}  f181  PASS_ALL
#   AddFilter  ${nto1}  f182  PASS_ALL
#   AddFilter  ${nto1}  f183  PASS_ALL
#   AddFilter  ${nto1}  f184  PASS_ALL
#   AddFilter  ${nto1}  f185  PASS_ALL
#   AddFilter  ${nto1}  f186  PASS_ALL
#   AddFilter  ${nto1}  f187  PASS_ALL
#   AddFilter  ${nto1}  f188  PASS_ALL
#   AddFilter  ${nto1}  f189  PASS_ALL
#   AddFilter  ${nto1}  f190  PASS_ALL
#   AddFilter  ${nto1}  f191  PASS_ALL
#   AddFilter  ${nto1}  f192  PASS_ALL
#   AddFilter  ${nto1}  f193  PASS_ALL
#   AddFilter  ${nto1}  f194  PASS_ALL
#   AddFilter  ${nto1}  f195  PASS_ALL
#   AddFilter  ${nto1}  f196  PASS_ALL
#   AddFilter  ${nto1}  f197  PASS_ALL
#   AddFilter  ${nto1}  f198  PASS_ALL
#   AddFilter  ${nto1}  f199  PASS_ALL
#   AddFilter  ${nto1}  f200  PASS_ALL
#   AddFilter  ${nto1}  f201  PASS_ALL
#   AddFilter  ${nto1}  f202  PASS_ALL
#    AddFilter  ${nto1}  f203  PASS_ALL
#    AddFilter  ${nto1}  f204  PASS_ALL
#    AddFilter  ${nto1}  f205  PASS_ALL
#    AddFilter  ${nto1}  f206  PASS_ALL
#    AddFilter  ${nto1}  f207  PASS_ALL
#    AddFilter  ${nto1}  f208  PASS_ALL
#    AddFilter  ${nto1}  f209  PASS_ALL
#    AddFilter  ${nto1}  f210  PASS_ALL
#    AddFilter  ${nto1}  f211  PASS_ALL
#    AddFilter  ${nto1}  f212  PASS_ALL
#    AddFilter  ${nto1}  f213  PASS_ALL
#    AddFilter  ${nto1}  f214  PASS_ALL
#    AddFilter  ${nto1}  f215  PASS_ALL
#    AddFilter  ${nto1}  f216  PASS_ALL
#    AddFilter  ${nto1}  f217  PASS_ALL
#    AddFilter  ${nto1}  f218  PASS_ALL

# 40G port connections
    AddConn    ${nto1}  c1    ${P1-01}   ${f1}    ${P2-01}
    AddConn    ${nto1}  c2    ${P2-01}   ${f2}    ${P1-01}
#
    AddConn    ${nto1}  c3    ${P1-04}   ${f3}    ${P2-03}
    AddConn    ${nto1}  c4    ${P2-03}   ${f4}    ${P1-04}
#
    AddConn    ${nto1}  c5    ${P1-06}   ${f5}    ${P2A-01}
    AddConn    ${nto1}  c6    ${P2A-01}  ${f6}    ${P1-06}
#
    AddConn    ${nto1}  c7    ${P1-08}   ${f7}    ${P2A-03}
    AddConn    ${nto1}  c8    ${P2A-03}  ${f8}    ${P1-08}
#
    AddConn    ${nto1}  c9    ${P1-10}   ${f9}    ${P2B-01}
    AddConn    ${nto1}  c10   ${P2B-01}   ${f10}   ${P1-10}
#
    AddConn    ${nto1}  c11   ${P1-12}   ${f11}   ${P2B-03}
    AddConn    ${nto1}  c12   ${P2B-03}  ${f12}   ${P1-12}
#
    AddConn    ${nto1}  c13   ${P1-14}   ${f13}   ${P1-15}
    AddConn    ${nto1}  c14   ${P1-15}   ${f14}   ${P1-14}
#
    AddConn    ${nto1}  c15   ${P1-16}   ${f15}    ${P1-02}
    AddConn    ${nto1}  c16   ${P1-02}   ${f16}    ${P1-16}
#
    AddConn    ${nto1}  c17    ${P2-02}   ${f17}    ${P1-03}
    AddConn    ${nto1}  c18    ${P1-03}   ${f18}    ${P2-02}
#
    AddConn    ${nto1}  c19    ${P2-04}   ${f19}    ${P1-05}
    AddConn    ${nto1}  c20    ${P1-05}   ${f20}    ${P2-04}
#
    AddConn    ${nto1}  c21    ${P2A-02}   ${f21}    ${P1-07}
    AddConn    ${nto1}  c22    ${P1-07}  ${f22}    ${P2A-02}
#
    AddConn    ${nto1}  c23    ${P2A-04}   ${f23}    ${P1-09}
    AddConn    ${nto1}  c24    ${P1-09}  ${f24}    ${P2A-04}
#
    AddConn    ${nto1}  c25    ${P2B-02}   ${f25}    ${P1-11}
    AddConn    ${nto1}  c26   ${P1-11}   ${f26}   ${P2B-02}
#
    AddConn    ${nto1}  c27   ${P2B-04}   ${f27}   ${P1-13}
    AddConn    ${nto1}  c28   ${P1-13}  ${f28}   ${P2B-04}

# 100G port connections
    AddConn    ${nto1}  c29    ${P5-01}   ${f29}    ${P6-01}
    AddConn    ${nto1}  c30    ${P6-01}   ${f30}    ${P5-01}
#
    AddConn    ${nto1}  c31    ${P5-04}   ${f31}    ${P6-03}
    AddConn    ${nto1}  c32    ${P6-03}   ${f32}    ${P5-04}
#
    AddConn    ${nto1}  c33    ${P5-06}   ${f33}    ${P5-07}
    AddConn    ${nto1}  c34    ${P5-07}  ${f34}    ${P5-06}
#
    AddConn    ${nto1}  c35    ${P5-08}   ${f35}    ${P5-09}
    AddConn    ${nto1}  c36    ${P5-09}  ${f36}    ${P5-08}
#
    AddConn    ${nto1}  c37    ${P5-10}   ${f37}    ${P5-11}
    AddConn    ${nto1}  c38   ${P5-11}   ${f38}   ${P5-10}
#
    AddConn    ${nto1}  c39   ${P5-12}   ${f39}   ${P6-05}
    AddConn    ${nto1}  c40   ${P6-05}  ${f40}   ${P5-12}
#
    AddConn    ${nto1}  c41   ${P6-02}   ${f41}   ${P5-03}
    AddConn    ${nto1}  c42   ${P5-03}   ${f42}   ${P6-02}
#
    AddConn    ${nto1}  c43   ${P6-04}   ${f43}    ${P5-05}
    AddConn    ${nto1}  c44   ${P5-05}   ${f44}    ${P6-04}
#
    AddConn    ${nto1}  c45    ${P6-06}   ${f45}    ${P6-07}
    AddConn    ${nto1}  c46    ${P6-07}   ${f46}    ${P6-06}
#
    AddConn    ${nto1}  c47    ${P6-08}   ${f47}    ${P6-09}
    AddConn    ${nto1}  c48    ${P6-09}   ${f48}    ${P6-08}
#
    AddConn    ${nto1}  c49    ${P6-10}   ${f49}    ${P6-11}
    AddConn    ${nto1}  c50    ${P6-11}  ${f50}    ${P6-10}
#
    AddConn    ${nto1}  c51    ${P6-12}   ${f51}    ${P5-02}
    AddConn    ${nto1}  c52    ${P5-02}  ${f52}    ${P6-12}

# 10G port connections
    AddConn    ${nto1}  c53   ${P3-01}   ${f53}    ${P4-01}
    AddConn    ${nto1}  c54   ${P4-01}   ${f54}    ${P3-01}
#
    AddConn    ${nto1}  c55    ${P3-04}   ${f55}    ${P4-03}
    AddConn    ${nto1}  c56    ${P4-03}   ${f56}    ${P3-04}
#
    AddConn    ${nto1}  c57    ${P3-06}   ${f57}    ${P4-05}
    AddConn    ${nto1}  c58    ${P4-05}   ${f58}    ${P3-06}
#
    AddConn    ${nto1}  c59    ${P3-08}   ${f59}    ${P4-07}
    AddConn    ${nto1}  c60    ${P4-07}  ${f60}    ${P3-08}
#
    AddConn    ${nto1}  c61    ${P3-10}   ${f61}    ${P4-09}
    AddConn    ${nto1}  c62    ${P4-09}  ${f62}    ${P3-10}
#
    AddConn    ${nto1}  c63   ${P3-12}   ${f63}    ${P4-11}
    AddConn    ${nto1}  c64   ${P4-11}   ${f64}    ${P3-12}
#
    AddConn    ${nto1}  c65    ${P3-14}   ${f65}    ${P4-13}
    AddConn    ${nto1}  c66    ${P4-13}   ${f66}    ${P3-14}
#
    AddConn    ${nto1}  c67    ${P3-16}   ${f67}    ${P4-15}
    AddConn    ${nto1}  c68    ${P4-15}   ${f68}    ${P3-16}
#
    AddConn    ${nto1}  c69    ${P3-18}   ${f69}    ${P4-17}
    AddConn    ${nto1}  c70    ${P4-17}  ${f70}    ${P3-18}
#
    AddConn    ${nto1}  c71    ${P3-20}   ${f71}    ${P4-19}
    AddConn    ${nto1}  c72    ${P4-19}  ${f72}    ${P3-20}
#
    AddConn    ${nto1}  c73   ${P3-22}   ${f73}    ${P4-21}
    AddConn    ${nto1}  c74   ${P4-21}   ${f74}    ${P3-22}
#
    AddConn    ${nto1}  c75    ${P3-24}   ${f75}    ${P4-23}
    AddConn    ${nto1}  c76    ${P4-23}   ${f76}    ${P3-24}
#
    AddConn    ${nto1}  c77    ${P3-26}   ${f77}    ${P4-25}
    AddConn    ${nto1}  c78    ${P4-25}   ${f78}    ${P3-26}
#
    AddConn    ${nto1}  c79    ${P3-28}   ${f79}    ${P4-27}
    AddConn    ${nto1}  c80    ${P4-27}  ${f80}    ${P3-28}
#
    AddConn    ${nto1}  c81    ${P3-30}   ${f81}    ${P4-29}
    AddConn    ${nto1}  c82    ${P4-29}  ${f82}    ${P3-30}
#
    AddConn    ${nto1}  c83   ${P3-32}   ${f83}    ${P4-31}
    AddConn    ${nto1}  c84   ${P4-31}   ${f84}    ${P3-32}
#
    AddConn    ${nto1}  c85    ${P3-34}   ${f85}    ${P4-33}
    AddConn    ${nto1}  c86    ${P4-33}   ${f86}    ${P3-34}
#
    AddConn    ${nto1}  c87    ${P3-36}   ${f87}    ${P4-35}
    AddConn    ${nto1}  c88    ${P4-35}   ${f88}    ${P3-36}
#
    AddConn    ${nto1}  c89    ${P3-38}   ${f89}    ${P4-37}
    AddConn    ${nto1}  c90    ${P4-37}  ${f90}    ${P3-38}
#
    AddConn    ${nto1}  c91    ${P3-40}   ${f91}    ${P4-39}
    AddConn    ${nto1}  c92    ${P4-39}  ${f92}    ${P3-40}
#
    AddConn    ${nto1}  c93   ${P3-42}   ${f93}    ${P4-41}
    AddConn    ${nto1}  c94   ${P4-41}   ${f94}    ${P3-42}
#
    AddConn    ${nto1}  c95    ${P3-44}   ${f95}    ${P4-43}
    AddConn    ${nto1}  c96    ${P4-43}   ${f96}    ${P3-44}
#
    AddConn    ${nto1}  c97    ${P3-46}   ${f97}    ${P4-45}
    AddConn    ${nto1}  c98    ${P4-45}   ${f98}    ${P3-46}
#
    AddConn    ${nto1}  c99    ${P3-48}   ${f99}    ${P4-47}
    AddConn    ${nto1}  c100    ${P4-47}  ${f100}    ${P3-48}
#
    AddConn    ${nto1}  c101    ${P4-02}   ${f101}    ${P3-03}
    AddConn    ${nto1}  c102    ${P3-03}  ${f102}    ${P4-02}
#
    AddConn    ${nto1}  c103   ${P4-04}   ${f103}    ${P3-05}
    AddConn    ${nto1}  c104   ${P3-05}   ${f104}    ${P4-04}
#
    AddConn    ${nto1}  c105    ${P4-06}   ${f105}    ${P3-07}
    AddConn    ${nto1}  c106    ${P3-07}   ${f106}    ${P4-06}
#
    AddConn    ${nto1}  c107    ${P4-08}   ${f107}    ${P3-09}
    AddConn    ${nto1}  c108    ${P3-09}   ${f108}    ${P4-08}
#
    AddConn    ${nto1}  c109    ${P4-10}   ${f109}    ${P3-11}
    AddConn    ${nto1}  c110    ${P3-11}  ${f110}    ${P4-10}
#
    AddConn    ${nto1}  c111    ${P4-12}   ${f111}    ${P3-13}
    AddConn    ${nto1}  c112    ${P3-13}  ${f112}    ${P4-12}
#
    AddConn    ${nto1}  c113   ${P4-14}   ${f113}    ${P3-15}
    AddConn    ${nto1}  c114   ${P3-15}   ${f114}    ${P4-14}
#
    AddConn    ${nto1}  c115    ${P4-16}   ${f115}    ${P3-17}
    AddConn    ${nto1}  c116    ${P3-17}   ${f116}    ${P4-16}
#
    AddConn    ${nto1}  c117    ${P4-18}   ${f117}    ${P3-19}
    AddConn    ${nto1}  c118    ${P3-19}   ${f118}    ${P4-18}
#
    AddConn    ${nto1}  c119    ${P4-20}   ${f119}    ${P3-21}
    AddConn    ${nto1}  c120    ${P3-21}  ${f120}    ${P4-20}
#
    AddConn    ${nto1}  c121    ${P4-22}   ${f121}    ${P3-23}
    AddConn    ${nto1}  c122    ${P3-23}  ${f122}    ${P4-22}
#
    AddConn    ${nto1}  c123   ${P4-24}   ${f123}    ${P3-25}
    AddConn    ${nto1}  c124   ${P3-25}   ${f124}    ${P4-24}
#
    AddConn    ${nto1}  c125    ${P4-26}   ${f125}    ${P3-27}
    AddConn    ${nto1}  c126    ${P3-27}   ${f126}    ${P4-26}
#
    AddConn    ${nto1}  c127    ${P4-28}   ${f127}    ${P3-29}
    AddConn    ${nto1}  c128    ${P3-29}   ${f128}    ${P4-28}
#
    AddConn    ${nto1}  c129    ${P4-30}   ${f129}    ${P3-31}
    AddConn    ${nto1}  c130    ${P3-31}  ${f130}    ${P4-30}
#
    AddConn    ${nto1}  c131    ${P4-32}   ${f131}    ${P3-33}
    AddConn    ${nto1}  c132    ${P3-33}  ${f132}    ${P4-32}
#
    AddConn    ${nto1}  c133   ${P4-34}   ${f133}    ${P3-35}
    AddConn    ${nto1}  c134   ${P3-35}   ${f134}    ${P4-34}
#
    AddConn    ${nto1}  c135    ${P4-36}   ${f135}    ${P3-37}
    AddConn    ${nto1}  c136    ${P3-37}   ${f136}    ${P4-36}
#
    AddConn    ${nto1}  c137    ${P4-38}   ${f137}    ${P3-39}
    AddConn    ${nto1}  c138    ${P3-39}   ${f138}    ${P4-38}
#
    AddConn    ${nto1}  c139    ${P4-40}   ${f139}    ${P3-41}
    AddConn    ${nto1}  c140    ${P3-41}  ${f140}    ${P4-40}
#
    AddConn    ${nto1}  c141    ${P4-42}   ${f141}    ${P3-43}
    AddConn    ${nto1}  c142    ${P3-43}  ${f142}    ${P4-42}
#
    AddConn    ${nto1}  c143   ${P4-44}   ${f143}    ${P3-45}
    AddConn    ${nto1}  c144   ${P3-45}   ${f144}    ${P4-44}
#
    AddConn    ${nto1}  c145    ${P4-46}   ${f145}    ${P3-47}
    AddConn    ${nto1}  c146    ${P3-47}   ${f146}    ${P4-46}
#
    AddConn    ${nto1}  c147    ${P4-48}   ${f147}    ${P3-02}
    AddConn    ${nto1}  c148    ${P3-02}   ${f148}    ${P4-48}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:22:33:44:55')/IP()/TCP()  payload=rand  frameSize=300   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:66:77:88:99:11')/IP()/TCP()  payload=rand  frameSize=128   numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:20:30:40:50:60')/IP()/TCP()  payload=rand  frameSize=256   numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:02:03:04:05:06')/IP()/TCP()  payload=rand  frameSize=512   numFrames=100

    ${stream5}=  createStream  s5  Ether(src='00:11:22:33:44:55')/IP()/TCP()  payload=rand  frameSize=300   numFrames=100
    ${stream6}=  createStream  s6  Ether(src='00:66:77:88:99:11')/IP()/TCP()  payload=rand  frameSize=128   numFrames=100
    ${stream7}=  createStream  s7  Ether(src='00:20:30:40:50:60')/IP()/TCP()  payload=rand  frameSize=256   numFrames=100
    ${stream8}=  createStream  s8  Ether(src='00:02:03:04:05:06')/IP()/TCP()  payload=rand  frameSize=512   numFrames=100

    ${stream9}=  createStream  s9  Ether(src='00:11:22:33:44:55')/IP()/TCP()  payload=rand  frameSize=300   numFrames=100
    ${stream10}=  createStream  s10  Ether(src='00:66:77:88:99:11')/IP()/TCP()  payload=rand  frameSize=128   numFrames=100
    ${stream11}=  createStream  s11  Ether(src='00:20:30:40:50:60')/IP()/TCP()  payload=rand  frameSize=256   numFrames=100
    ${stream12}=  createStream  s12  Ether(src='00:02:03:04:05:06')/IP()/TCP()  payload=rand  frameSize=512   numFrames=100


    P1-01Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    P3-01Ixia.addIxStreams  ${stream5}  ${stream6}  ${stream7}  ${stream8}
    P5-01Ixia.addIxStreams  ${stream9}  ${stream10}  ${stream11}  ${stream12}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  continuous=True  percentPktRate=100.0

    Set Test Variable  ${maxIterations}  5
    :FOR  ${loop}  IN RANGE  0  ${maxIterations}
    \  Log  Traffic iteration ${loop} out of ${maxIterations} executing...  console=true
    \  P1-01Ixia.runIxContinuousTraffic
    \  P3-01Ixia.runIxContinuousTraffic
    \  P5-01Ixia.runIxContinuousTraffic
    \  Sleep  30 seconds
    \  P1-01Ixia.stopIxTraffic
    \  P3-01Ixia.stopIxTraffic
    \  P5-01Ixia.stopIxTraffic
    \  Sleep  3 seconds
    \  ${P1-01IxiaTxFrames}=  P1-01Ixia.getIxPortTxStats  statName=txFrames
    \  ${P1-01IxiaRxFrames}=  P1-01Ixia.getIxPortRxStats  statName=rxFrames
    \  ${P1-01RxFrames}=  P1-01.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  Should Be Equal As Integers  ${P1-01IxiaTxFrames}  ${P1-01IxiaRxFrames}
    \  ${P3-01IxiaTxFrames}=  P3-01Ixia.getIxPortTxStats  statName=txFrames
    \  ${P3-01IxiaRxFrames}=  P3-01Ixia.getIxPortRxStats  statName=rxFrames
    \  ${P3-01RxFrames}=  P3-01.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  Should Be Equal As Integers  ${P3-01IxiaTxFrames}  ${P3-01IxiaRxFrames}
    \  ${P5-01IxiaTxFrames}=  P5-01Ixia.getIxPortTxStats  statName=txFrames
    \  ${P5-01IxiaRxFrames}=  P5-01Ixia.getIxPortRxStats  statName=rxFrames
    \  ${P5-01RxFrames}=  P5-01.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \   Should Be Equal As Integers  ${P5-01IxiaTxFrames}  ${P5-01IxiaRxFrames}

#    nto1.cleanupNVSDevices
#    nto1.cleanupAllIxPorts

*** Keywords ***
setupSuiteEnv
    Set Global Variable  &{ixChassisDict}  &{EMPTY}

