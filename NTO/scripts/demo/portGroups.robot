*** Settings ***
Suite Setup     setupSuiteEnv
Suite Teardown  suiteCleanup

#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-02.ann.is.keysight.com
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  60

*** Test Cases ***
Single_NP_Port_Group
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  np2  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp2  TOOL     ${myReservation['myNTO']['myPorts']}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66", "00-22-33-44-55-67"]}}}

    #Port Group definitions.
    #portgroup (id pg1 type interconnect mode network)
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}

    AddFilter   ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    @{myStreams}=  Create List  s1  s2  s3  s4

    ${statsMap}=  Catenate  {'Network': {'npg1': ['s1', 's2', 's3', 's4'],
                  ...                    'np1' : ${myStreams}},
                  ...        'Filter' : {'f1'  : ['s1', 's3']},
                  ...        'Tool'   : {'tp1' : ['s1', 's3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Dual_NP_Port_Group
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  np2  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp2  TOOL     ${myReservation['myNTO']['myPorts']}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    @{pgPortList}=  Create List  ${np1}  ${np2}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${pgPortList}
    AddConn    ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${npg1}  ${f2}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic
    np2Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'npg1': ['s1', 's2', 's3', 's4'],
                  ...                    'np1' : ['s1', 's2'],
                  ...                    'np2' : ['s3', 's4']},
                  ...        'Filter' : {'f1': ['s1'],
                  ...                    'f2': ['s3']},
                  ...        'Tool'   : {'tp1': ['s1'], 'tp2': ['s3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Single_TP_Port_Group
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  np2  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp2  TOOL     ${myReservation['myNTO']['myPorts']}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66", "00-22-33-44-55-67"]}}}

    #Port Group definitions.
    #portgroup (id pg1 type interconnect mode network)
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}

    AddFilter   ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network' : {'np1'  : ['s1', 's2', 's3', 's4']},
                  ...        'Filter'  : {'f1'   : ['s1', 's3']},
                  ...        'Tool'    : {'tp1'  : ['s1', 's3'],
                  ...                     'tpg1' : ['s1', 's3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Dual_TP_Port_Group
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  np2  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp2  TOOL     ${myReservation['myNTO']['myPorts']}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    @{pgPortList}=  Create List  ${tp1}  ${tp2}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${pgPortList}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    AddConn    ${nto1}  c2  ${np2}  ${f2}  ${tpg1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions (NOTE: Difference in IP dst 4th octet for TP port group load balance default (IPv4 src/dst) setting.)
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP(src='1.1.1.2', dst='2.2.2.1')  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP(src='1.1.1.2', dst='2.2.2.1')  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP(src='1.1.1.2', dst='2.2.2.2')  payload=rand  frameSize=256  numFrames=200
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP(src='1.1.1.2', dst='2.2.2.2')  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic
    np2Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network' : {'np1'  : ['s1', 's2'], 'np2': ['s3', 's4']},
                  ...        'Filter'  : {'f1'   : ['s1'], 'f2': ['s3']},
                  ...        'Tool'    : {'tpg1' : ['s1', 's3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Dual_NP_And_Dual_TP_Port_Groups
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  np2  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp2  TOOL     ${myReservation['myNTO']['myPorts']}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    ${pg2}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    @{npPgList}=  Create List  ${np1}  ${np2}
    @{tpPgList}=  Create List  ${tp1}  ${tp2}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${npPgList}
    AddPortGrp  ${nto1}  tpg1  ${pg2}  ${tpPgList}
    AddConn    ${nto1}  c1  ${npg1}  ${f1}  ${tpg1}
    AddConn    ${nto1}  c2  ${npg1}  ${f2}  ${tpg1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions (NOTE: Difference in IP dst 4th octet for TP port group load balance default (IPv4 src/dst) setting.)
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP(src='1.1.1.2', dst='2.2.2.1')  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP(src='1.1.1.2', dst='2.2.2.1')  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP(src='1.1.1.2', dst='2.2.2.2')  payload=rand  frameSize=256  numFrames=200
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP(src='1.1.1.2', dst='2.2.2.2')  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic
    np2Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network' : {'np1'  : ['s1', 's2'],
                  ...                     'np2'  : ['s3', 's4'],
                  ...                     'npg1' : ['s1', 's2', 's3', 's4']},
                  ...        'Filter'  : {'f1'   : ['s1'],
                  ...                     'f2'   : ['s3']},
                  ...        'Tool'    : {'tpg1' : ['s1', 's3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

*** Keywords ***
setupSuiteEnv
#    log  Release resource pool: ${RESOURCEPOOL}  console=true
#    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myRequestDef}=  Catenate  {'myPorts': {'type':'NTOPort',
    ...                                         'properties':{'ConnectionType':'standard'}},
    ...                          'myNTO': {'type': 'NTO'}}

    ${myMap}=  Catenate  {'myNTO': {'myPorts': ['np1', 'np2', 'tp1', 'tp2']}}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myReservation}=  requestResourceBlocking  ${RESOURCETIMEOUT}  ${RESOURCEPOOL}  ${myRequestDef}  requestMapParentBound=${myMap}
    Set Global Variable  ${myReservation}

suiteCleanup
    releaseResources   ${myReservation['responseId']}
