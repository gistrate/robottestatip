*** Settings ***
Suite Setup    setupSuiteEnv

#Libraries
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  60

*** Test Cases ***
Sample_Dual_Port_And_Filter_Test
    AddNTO  nto1  10.36.168.11

    ${myPortsDef}=  Catenate  {'np1': {'properties': {'ID': 'P05',
    ...                                               'ConnectionType': 'standard',
    ...                                               'MediaType': 'SFP_PLUS_10G'},
    ...                                'lockAlso': {'myIxiaPort': {'type': 'IXIAPort',
    ...                                                            'IPAddress': '10.36.168.242',
    ...                                                            'properties': {'ID': '1/9/5',
    ...                                                                           'PHYMode': 'Fiber'}}}},
    ...                        'np2': {'properties': {'ID': 'P04',
    ...                                               'ConnectionType': 'standard',
    ...                                               'MediaType': 'SFP_PLUS_10G'},
    ...                                'lockAlso': {'myIxiaPort': {'type': 'IXIAPort',
    ...                                                            'IPAddress': '10.36.168.242',
    ...                                                            'properties': {'ID': '1/9/4',
    ...                                                                           'PHYMode': 'Fiber'}}}},
    ...                        'tp1': {'properties': {'ID': 'P01',
    ...                                               'ConnectionType': 'standard',
    ...                                               'MediaType': 'SFP_PLUS_10G'},
    ...                                'lockAlso': {'myIxiaPort': {'type': 'IXIAPort',
    ...                                                            'IPAddress': '10.36.168.242',
    ...                                                            'properties': {'ID': '1/9/1',
    ...                                                                           'PHYMode': 'Fiber'}}}},
    ...                        'tp2': {'properties': {'ID': 'P03',
    ...                                               'ConnectionType': 'standard',
    ...                                               'MediaType': 'SFP_PLUS_10G'},
    ...                                'lockAlso': {'myIxiaPort': {'type': 'IXIAPort',
    ...                                                            'IPAddress': '10.36.168.242',
    ...                                                            'properties': {'ID': '1/9/3',
    ...                                                                           'PHYMode': 'Fiber'}}}}}

    ${myPorts}=  catenateToDict  ${myPortsDef}

    AddPort  ${nto1}  np1  NETWORK  ${myPorts}
    AddPort  ${nto1}  tp1  TOOL     ${myPorts}
    AddPort  ${nto1}  np2  NETWORK  ${myPorts}
    AddPort  ${nto1}  tp2  TOOL     ${myPorts}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np2}  ${f2}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic
    np2Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2'], 'np2': ['s3', 's4']}, 'Filter': {'f1': ['s1'], 'f2': ['s3']}, 'Tool': {'tp1': ['s1'], 'tp2': ['s3']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts


*** Keywords ***
setupSuiteEnv
    Set Global Variable  &{ixChassisDict}  &{EMPTY}
