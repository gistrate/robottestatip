*** Settings ***
Suite Setup    setupSuiteEnv
Suite Teardown  suiteCleanup

#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-01
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  60

*** Test Cases ***
Continuous_Traffic_With_Looping

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np2}  ${f2}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66')/IP()  payload=rand  frameSize=128  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67')/IP()  payload=rand  frameSize=256  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65')/IP()  payload=rand  frameSize=512  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    np2Ixia.addIxStreams  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  continuous=True  percentPktRate=0.01

    Set Test Variable  ${maxIterations}  5
    :FOR  ${loop}  IN RANGE  0  5
    \  Log  Traffic iteration ${loop} out of ${maxIterations} executing...  console=true
    \  np1Ixia.runIxContinuousTraffic
    \  np2Ixia.runIxContinuousTraffic
    \  Sleep  5 seconds
    \  np1Ixia.stopIxTraffic
    \  np2Ixia.stopIxTraffic
    \  Sleep  3 seconds
    \  ${np1IxiaTxFrames}=  np1Ixia.getIxPortTxStats  statName=txFrames
    \  ${np2IxiaTxFrames}=  np2Ixia.getIxPortTxStats  statName=txFrames
    \  ${np1RxFrames}=  np1.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  ${np2RxFrames}=  np2.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  Should Be Equal As Integers  ${np1IxiaTxFrames}  ${np1RxFrames}
    \  Should Be Equal As Integers  ${np2IxiaTxFrames}  ${np2RxFrames}

    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

*** Keywords ***
setupSuiteEnv
 #   log  Release resource pool: ${RESOURCEPOOL}  console=true
 #   releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myHWList}=  Create List
    ...  standard np1 any NETWORK
    ...  standard tp1 any TOOL
    ...  standard np2 any NETWORK
    ...  standard tp2 any TOOL
    Set Global Variable  ${myHWList}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myResource}=  requestResourceBlocking  ${myHWList}  ${RESOURCEPOOL}  NTO  ${RESOURCETIMEOUT}
    Set Global Variable  ${myResource}

suiteCleanup
    releaseResources   ${myResource}
