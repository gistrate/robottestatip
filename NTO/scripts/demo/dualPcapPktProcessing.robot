*** Settings ***
Suite Setup     setupSuiteEnv
Suite Teardown  suiteCleanup

#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-01
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  60

*** Test Cases ***
Std_Vlan_Stripping

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/Vlan_stripping_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/Vlan_stripping_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/Vlan_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/Vlan_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'ingress_count': 1, 'strip_mode': 'INGRESS'}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:33',dst='00:60:89:47:48:22')/Dot1Q(vlan=777)/IP()/UDP(sport=1001,dport=2001)  payload=ones  frameSize=80  numFrames=1
    ${stream2}=  createStream  s2  Ether(src='00:73:31:93:18:55',dst='00:60:89:47:48:44')/Dot1Q(vlan=111)/Dot1Q(vlan=222)/IP()/UDP(sport=1002,dport=2003)  payload=incr  frameSize=128  numFrames=1
    ${stream3}=  createStream  s3  Ether(src='00:73:31:93:18:77',dst='00:60:89:47:48:66')/Dot1Q(vlan=3)/Dot1Q(vlan=4)/Dot1Q(vlan=5)/IP()/UDP(sport=1003,dport=2003)  payload=altones  frameSize=233  numFrames=1
    ${stream4}=  createStream  s4  Ether(src='00:73:31:93:18:99',dst='00:60:89:47:48:88')/IP()/TCP(sport=1004,dport=2004)  payload=zeros  frameSize=77  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    np1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Std_Port_Tagging

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/Port_tagging_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/Port_tagging_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/Port_tagging.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/Port_tagging.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

     ${pp1}=  Catenate  {'std_port_tagging_settings': {'enabled': True, 'vlan_id': 999}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/Dot1Q(vlan=777)/Dot1Q(vlan=733)/MPLS(s=0,label=63886)/MPLS(s=0,label=51866)/MPLS(s=0,label=15323)/MPLS(s=0,label=22006)/MPLS(s=0,label=75855)/MPLS(s=0,label=20912)/MPLS(s=0,label=82346)/MPLS(label=31349)/Ether(src='00:82:28:84:99:86',dst='00:88:16:38:34:24',type=0x86dd)/IPv6(src='8282:1793:9436:6647:3267:7344:8266:3034',dst='3747:7478:6801:5779:5979:4355:6014:3075')/TCP(sport=7853,dport=1220)  payload=incr  frameSize=200  numFrames=5
    ${stream2}=  createStream  s2  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/MPLS(label=51866)/Ether(src='B4:99:BA:00:00:40',dst='2C:30:33:01:80:C2')/IP()  payload=incr  frameSize=65  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    np1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Fabric_Path_Stripping

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/CFP_stripping_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/CFP_stripping_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/CFP_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/CFP_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'fabric_path_strip_settings': {'enabled': True}}

    #Stream definitions.
    ${stream1}=  createStream  s1  CFP(src='11:00:aa:00:00:01', dst='11:00:bb:00:00:01', type=0x8903, ftag=10, ttl=3)/Ether(src='22:55:55:55:55:02', dst='22:66:66:66:66:02')/Dot1Q(vlan=102)/IP(src='10.10.10.2', dst='20.20.20.2')  payload=rand  frameSize=100  numFrames=1
    ${stream2}=  createStream  s2  CFP(src='11:22:33:44:55:66', dst='11:22:33:44:55:77')/Ether(src='00:11:11:00:00:08',dst='00:22:22:00:00:08')/MPLS(s=0,label=10008)/MPLS(label=20008)/IPv6(src='1000::0008',dst='2000::0008')/TCP(sport=1056,dport=2008)  payload=ones  frameSize=128  numFrames=10

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    tp1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

GTP_Stripping

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/GTP_stripping_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/GTP_stripping_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/GTP_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/GTP_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'gtp_strip_settings': {'enabled': True}}

    #Stream definitions (Note: 51d -> 204 dscp value (binary with two zeros postpended for 6-bit field).
    ${stream1}=  createStream  s1  Ether(src='11:11:11:11:11:11',dst='22:22:22:22:22:22')/IP(src='10.10.10.10',dst='11.11.11.11',tos=204)/UDP(dport=2123)/GTPHeader(S=0,gtp_type=255,teid=0x1010101)/IP(src='20.20.20.20',dst='31.31.31.31')  payload=incr  frameSize=512  numFrames=10
    ${stream2}=  createStream  s2  Ether(src='11:11:11:11:11:11',dst='22:22:22:22:22:22')/IP(src='10.10.10.10',dst='11.11.11.11',tos=204)/UDP(dport=2152)/GTPHeader(S=0,gtp_type=255,teid=0x1010101)/IP(src='20.20.20.20',dst='31.31.31.31')  payload=incr  frameSize=512  numFrames=10
    ${stream3}=  createStream  s3  Ether(src='2c:21:72:65:c7:f0',dst='d8:24:bd:8a:28:f3')/Dot1Q(vlan=1458)/Dot1Q(vlan=24)/IP(src='172.17.33.118',dst='20.20.20.11',tos=204)/UDP(dport=2123)/GTPHeader(S=0,gtp_type=255,teid=0x5555)/IP(src='20.20.20.20',dst='31.31.31.31')/UDP(sport=35984, dport=19523)  payload=incr  frameSize=512  numFrames=10
    ${stream4}=  createStream  s4  Ether(src='2c:21:72:65:c7:f0',dst='d8:24:bd:8a:28:f3')/Dot1Q(vlan=1458)/Dot1Q(vlan=24)/IP(src='172.17.33.118',dst='20.20.20.11',tos=204)/UDP(dport=2152)/GTPHeader(S=0,gtp_type=255,teid=0x5555)/IP(src='20.20.20.20',dst='31.31.31.31')/UDP(sport=35984, dport=19523)  payload=incr  frameSize=512  numFrames=10

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    tp1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

MPLS_Stripping

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/MPLS_stripping_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/MPLS_stripping_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/MPLS_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/MPLS_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'mpls_strip_settings': {'enabled': True, 'service_type': 'L2_VPN_WITHOUT_CONTROL'}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/Dot1Q(vlan=777)/Dot1Q(vlan=733)/MPLS(s=0,label=63886)/MPLS(s=0,label=51866)/MPLS(s=0,label=15323)/MPLS(s=0,label=22006)/MPLS(s=0,label=75855)/MPLS(s=0,label=20912)/MPLS(s=0,label=82346)/MPLS(label=31349)/Ether(src='00:82:28:84:99:86',dst='00:88:16:38:34:24',type=0x86dd)/IPv6(src='8282:1793:9436:6647:3267:7344:8266:3034',dst='3747:7478:6801:5779:5979:4355:6014:3075')/TCP(sport=7853,dport=1220)  payload=incr  frameSize=200  numFrames=1
    ${stream2}=  createStream  s2  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/MPLS(label=51866)/Ether(src='B4:99:BA:00:00:40',dst='2C:30:33:01:80:C2')/IP()  payload=incr  frameSize=65  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    tp1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Anue_Trailer_Stripping

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/Anue_trailer_stripping_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/Anue_trailer_stripping_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/Anue_trailer_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/Anue_trailer_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'trailer_strip_settings': {'enabled': True}}

    #Stream definitions.
    ${payload}=  configurePayload  altones  100
    ${payload2}=  configurePayload  rand  40
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP(src='10.10.10.10',dst='11.11.11.11')/Raw(load=bytes(${payload}))/ANUE(type='origlen')  payload=None  frameSize=None  numFrames=1
    ${stream2}=  createStream  s2  Ether(src='00:72:35:96:19:56',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload2}))/ANUE(type='local')  payload=None  frameSize=None  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    tp1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

ANUE_TrailerOrigPktLen

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/AddTrailerOrigLen_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/AddTrailerOrigLen_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/AddTrailerOrigLen.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/AddTrailerOrigLen.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'packet_length_trailer_settings': {'enabled': True, 'adjust_length': False}}

    #Stream definitions.
    ${payload}=  configurePayload  incr  40
    ${payload2}=  configurePayload  rand  55
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload}))  payload=None  frameSize=None  numFrames=1
    ${stream2}=  createStream  s2  Ether(src='00:72:35:96:19:56',dst='00:60:89:47:48:36')/IPv6()/TCP()/Raw(load=bytes(${payload2}))  payload=None  frameSize=None  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    np1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

ANUE_AddTimeStamp

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/AddAnueTimeStamp_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/AddAnueTimeStamp_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/AddAnueTimeStamp.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/AddAnueTimeStamp.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'timestamp_settings': {'enabled': True}}

    #Stream definitions.
    ${payload}=  configurePayload  incr  40
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload}))  payload=None  frameSize=None  numFrames=1

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    np1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ignoreOffsetAndLen=[(76,8), (87,2)]  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Packet_Trimming_MAC

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'trim_settings': {'enabled': True, 'retained_bytes': 55, 'retained_headers': 'MAC'}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='11:11:11:11:11:88',dst='22:22:22:22:22:99')/MPLS(s=0,label=10008)/MPLS(label=20008)/IPv6(src='1000::0008',dst='2000::0008')/TCP(sport=1056,dport=2008)  payload=ones  frameSize=1400  numFrames=1

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    tp1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Packet_Trimming_MAC_VLAN

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'trim_settings': {'enabled': True, 'retained_bytes': 24, 'retained_headers': 'MAC_VLAN'}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:00:00:21',dst='00:22:22:00:00:21')/Dot1Q(id=121)/MPLS(label=10021)/IP(src='10.10.10.21',dst='20.20.20.21')/TCP(sport=1021,dport=2021)  payload=ones  frameSize=988  numFrames=1

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    tp1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Packet_Trimming_MAC_VLAN_MPLS

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'trim_settings': {'enabled': True, 'retained_bytes': 33, 'retained_headers': 'MAC_VLAN_MPLS'}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:00:00:21',dst='00:22:22:00:00:21')/Dot1Q(id=121)/MPLS(label=10021)/IP(src='10.10.10.21',dst='20.20.20.21')/TCP(sport=1021,dport=2021)  payload=ones  frameSize=501  numFrames=1

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    tp1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Packet_Trimming_MAC_VLAN_MPLS_L3

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'trim_settings': {'enabled': True, 'retained_bytes': 21, 'retained_headers': 'MAC_VLAN_MPLS_L3'}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:00:00:21',dst='00:22:22:00:00:21')/Dot1Q(id=121)/MPLS(label=10021)/IPv6(src='1000::0008',dst='2000::0008')/TCP(sport=1021,dport=2021)  payload=ones  frameSize=333  numFrames=1

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    tp1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

L2GRE_Stripping

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/L2GRE_stripping_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/L2GRE_stripping_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/L2GRE_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/L2GRE_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'l2gre_strip_settings': {'enabled': True}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:11:11:22',dst='00:22:22:22:22:22')/Dot1Q(vlan=2222)/MPLS(s=0,label=40095)/MPLS(s=0,label=30095)/MPLS(s=0,label=20095)/MPLS(label=10095)/IP(src='11.11.11.22',dst='22.22.22.22')/GRE(proto=0x6558)/Ether(src='00:33:33:33:33:22',dst='00:44:44:44:44:22')/Dot1Q(vlan=3322)/IP(src='33.33.33.22',dst='44.44.44.22',ttl=1,proto=61)  payload=incr  frameSize=1600  numFrames=10

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    tp1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

VXLAN_Stripping

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/VXLAN_stripping_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/VXLAN_stripping_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/VXLAN_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/VXLAN_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'vxlan_strip_settings': {'enabled': True}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:11:11:11',dst='00:22:22:22:22:11')/Dot1Q(vlan=1122)/IP(src='11.11.11.11',dst='22.22.22.11')/UDP(sport=4789,dport=4789)/VXLAN(vni=16777215)/Ether(src='00:33:33:33:33:33',dst='00:44:44:44:44:44')/Dot1Q(vlan=3344)/IP(src='33.33.33.33',dst='44.44.44.44')  payload=incr  frameSize=483  numFrames=10

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    tp1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

ERSPAN_Stripping

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/ERSPAN_stripping_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/ERSPAN_stripping_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/ERSPAN_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/ERSPAN_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'erspan_strip_settings': {'enabled': True, 'empty_header': False}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:11:11:07',dst='00:22:22:22:22:07')/IP(src='11.11.11.7',dst='22.22.22.7',tos=34)/GRE(key_present=0,seqnum_present=1,seqence_number=426295,proto=0x88be)/ERSPAN(version=1,truncated=1,unknown7=27272727,vlan=222,span_id=22)/Ether(src='00:33:33:33:33:07',dst='00:44:44:44:44:07')/IP(src='33.33.33.7',dst='44.44.44.7')/TCP(sport=7007,dport=80)  payload=incr  frameSize=1256  numFrames=10

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    tp1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Data_Masking_L2_Start

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/DataMasking_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/DataMasking_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/DataMasking.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/DataMasking.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'data_masking_settings': {'enabled': True, 'fill_value': 0xbb, 'length': 10, 'offset_layer': 'L2_START', 'offset_value': 1}}

    #Stream definitions.
    ${payload}=  configurePayload  incr  40
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload}))  payload=None  frameSize=None  numFrames=1
    ${stream2}=  createStream  s2  CFP(src='11:00:aa:00:00:01', dst='11:00:bb:00:00:01', type=0x8903, ftag=10, ttl=3)/Ether(src='22:55:55:55:55:02', dst='22:66:66:66:66:02')/Dot1Q(vlan=102)/IP(src='10.10.10.2', dst='20.20.20.2')  payload=rand  frameSize=100  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    np1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Data_Masking_L2_End

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/DataMasking_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/DataMasking_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/DataMasking.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/DataMasking.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'data_masking_settings': {'enabled': True, 'fill_value': 0xcc, 'length': 12, 'offset_layer': 'L2_END', 'offset_value': 0}}

    #Stream definitions.
    ${payload}=  configurePayload  incr  40
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload}))  payload=None  frameSize=None  numFrames=1
    ${stream2}=  createStream  s2  CFP(src='11:00:aa:00:00:01', dst='11:00:bb:00:00:01', type=0x8903, ftag=10, ttl=3)/Ether(src='22:55:55:55:55:02', dst='22:66:66:66:66:02')/Dot1Q(vlan=102)/IPv6()/TCP()  payload=rand  frameSize=100  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    np1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Data_Masking_L3_End

    AddNTO  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/DataMasking_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/DataMasking_orig.pcap

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/DataMasking.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/DataMasking.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'data_masking_settings': {'enabled': True, 'fill_value': 0xdd, 'length': 16, 'offset_layer': 'L3_END', 'offset_value': 2}}

    #Stream definitions.
    ${payload}=  configurePayload  incr  40
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload}))  payload=None  frameSize=None  numFrames=1
    ${stream2}=  createStream  s2  CFP(src='11:00:aa:00:00:01', dst='11:00:bb:00:00:01', type=0x8903, ftag=10, ttl=3)/Ether(src='22:55:55:55:55:02', dst='22:66:66:66:66:02')/Dot1Q(vlan=102)/IP()/TCP()  payload=rand  frameSize=100  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    np1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${True}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

*** Keywords ***
setupSuiteEnv
#    log  Release resource pool: ${RESOURCEPOOL}  console=true
#    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myHWList}=  Create List
    ...  standard np1 any NETWORK
    ...  standard tp1 any TOOL
    ...  standard tp2 any TOOL
    Set Global Variable  ${myHWList}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myResource}=  requestResourceBlocking  ${myHWList}  ${RESOURCEPOOL}  NTO  ${RESOURCETIMEOUT}
    Set Global Variable  ${myResource}

suiteCleanup
    releaseResources   ${myResource}
