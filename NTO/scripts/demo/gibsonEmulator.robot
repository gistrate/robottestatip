*** Settings ***
Suite Setup    setupSuiteEnv
Suite Teardown  suiteCleanup

#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-01
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  60

*** Test Cases ***
Single_Single_Single
    AddNTO  nto1  ${myResource}  9000
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66", "00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.cleanupNVSDevices

Single_Single_Dual
    AddNTO  nto1  ${myResource}  9000
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66", "00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f1}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.cleanupNVSDevices

Single_Dual_Single
    AddNTO  nto1  ${myResource}  9000
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f2}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.cleanupNVSDevices

Single_Dual_Dual
    AddNTO  nto1  ${myResource}  9000
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f2}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.cleanupNVSDevices

Dual_Single_Single
    AddNTO  nto1  ${myResource}  9000
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66", "00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np2}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.cleanupNVSDevices

Dual_Single_Dual
    AddNTO  nto1  ${myResource}  9000
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np2}  ${f1}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.cleanupNVSDevices

Dual_Dual_Single
    AddNTO  nto1  ${myResource}  9000
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np2}  ${f2}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.cleanupNVSDevices

Dual_Dual_Dual
    AddNTO  nto1  ${myResource}  9000
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np2}  ${f2}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.cleanupNVSDevices

Dual_Mesh_Dual_Dual
    AddNTO  nto1  ${myResource}  9000
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f2}  ${tp2}
    AddConn    ${nto1}  c3  ${np2}  ${f2}  ${tp2}
    AddConn    ${nto1}  c4  ${np2}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.cleanupNVSDevices

Dual_Dual_Mesh_Dual
    AddNTO  nto1  ${myResource}  9000
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f1}  ${tp2}
    AddConn    ${nto1}  c3  ${np2}  ${f2}  ${tp1}
    AddConn    ${nto1}  c4  ${np2}  ${f2}  ${tp2}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.cleanupNVSDevices

Dual_Mesh_Dual_Mesh_Dual
    AddNTO  nto1  ${myResource}  9000
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66"]}}}
    ${fdef2}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-67"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${fdef2}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f1}  ${tp2}
    AddConn    ${nto1}  c3  ${np1}  ${f2}  ${tp2}
    AddConn    ${nto1}  c4  ${np2}  ${f2}  ${tp1}
    AddConn    ${nto1}  c5  ${np2}  ${f2}  ${tp2}
    AddConn    ${nto1}  c6  ${np2}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.cleanupNVSDevices


AFMR_MPLS_Stripping_on_Single_NP
    AddNTO  nto1  ${myResource}  9000
    AddAFMR  ${nto1}  afm1  L1-AFM
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    #Filter definitions.
    ${fdef}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-22-33-44-55-66", "00-22-33-44-55-67"]}}}

    #Packet processing definitions.
    ${pp1}=  Catenate  {'mpls_strip_settings': {'enabled': True, 'service_type': 'L2_VPN_WITHOUT_CONTROL'}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${fdef}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    afm1.attachResource  ${np1}  100
    #np1.attachAFMResource  ${afm1}  25
    np1.configurePacketProcessing  ${pp1}

    Sleep  15s

    ${afm1LaneCfg}=  afm1.getLaneConfig
    Log  Here is the afm1 Lane Configuration - ${afm1LaneCfg}

    afm1.detachResource  ${np1}
    nto1.cleanupNVSDevices


*** Keywords ***
setupSuiteEnv
#    log  Release resource pool: ${RESOURCEPOOL}  console=true
#    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myHWList}=  Create List
    ...  standalone np1 QSFP28 NETWORK
    ...  standalone tp1 QFSP28 TOOL
    ...  standalone np2 QFSP28 NETWORK
    ...  standalone tp2 QFSP28 TOOL
    Set Global Variable  ${myHWList}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myResource}=  requestResourceBlocking  ${myHWList}  ${RESOURCEPOOL}  NTO  ${RESOURCETIMEOUT}
    Set Global Variable  ${myResource}

suiteCleanup
    releaseResources   ${myResource}
