*** Settings ***
Suite Setup     setupSuiteEnv
Suite Teardown  suiteCleanup

#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-02.ann.is.keysight.com
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  60

*** Test Cases ***
NP_Pass_All
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    np1.configurePortFilter  PASS_ALL

    ${pkt1}=  Set Variable  Ether(src='00:22:33:44:55:66', dst='00:02:00:00:00:ad') / IP()

    #Stream definitions.
    ${stream1}=  createStream  s1  ${pkt1}  payload=rand  frameSize=512  numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1024  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1518  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1048  numFrames=100
    ${stream5}=  createStream  s5  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  rand  1216  100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2', 's3', 's4', 's5']}, 'Filter': {'f1': ['s1', 's2', 's3', 's4', 's5']}, 'Tool': {'tp1': ['s1', 's2', 's3', 's4', 's5']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

NP_Deny_All
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    np1.configurePortFilter  DENY_ALL

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=512   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1024  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1518  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1048  numFrames=100
    ${stream5}=  createStream  s5  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1216  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': []}, 'Filter': {'f1': []}, 'Tool': {'tp1': []}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

NP_Pass_By_Criteria
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Filter definitions.
    ${fdef}=  Catenate  {'filter_criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["*-22-33-44-55-66"]}}}

    np1.configurePortFilter  PASS_BY_CRITERIA  ${fdef}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=512   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1024  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1518  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1048  numFrames=100
    ${stream5}=  createStream  s5  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1216  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1']}, 'Filter': {'f1': ['s1']}, 'Tool': {'tp1': ['s1']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

NP_Deny_By_Criteria
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Filter definitions.
    ${fdef}=  Catenate  {'filter_criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["*-22-33-44-55-66"]}}}

    np1.configurePortFilter  DENY_BY_CRITERIA  ${fdef}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=512   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1024  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1518  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1048  numFrames=100
    ${stream5}=  createStream  s5  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1216  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s2', 's3', 's4', 's5']}, 'Filter': {'f1': ['s2', 's3', 's4', 's5']}, 'Tool': {'tp1': ['s2', 's3', 's4', 's5']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

TP_Pass_All
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    tp1.configurePortFilter  PASS_ALL

    #Stream definitions.
    ${stream1}=  createstream  s1  Ether(src='00:22:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=512   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1024  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1518  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1048  numFrames=100
    ${stream5}=  createStream  s5  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1216  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2', 's3', 's4', 's5']}, 'Filter': {'f1': ['s1', 's2', 's3', 's4', 's5']}, 'Tool': {'tp1': ['s1', 's2', 's3', 's4', 's5']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

TP_Deny_All
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    tp1.configurePortFilter  DENY_ALL

    #Stream definitions.
    ${stream1}=  createstream  s1  Ether(src='00:22:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=512   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1024  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1518  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1048  numFrames=100
    ${stream5}=  createStream  s5  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1216  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2', 's3', 's4', 's5']}, 'Filter': {'f1': ['s1', 's2', 's3', 's4', 's5']}, 'Tool': {'tp1': []}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

TP_Pass_By_Criteria
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Filter definitions.
    ${fdef}=  Catenate  {'filter_criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["*-22-33-44-55-66"]}}}

    tp1.configurePortFilter  PASS_BY_CRITERIA  ${fdef}

    #Stream definitions.
    ${stream1}=  createstream  s1  Ether(src='00:22:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=512   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1024  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1518  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1048  numFrames=100
    ${stream5}=  createStream  s5  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1216  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2', 's3', 's4', 's5']}, 'Filter': {'f1': ['s1', 's2', 's3', 's4', 's5']}, 'Tool': {'tp1': ['s1']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

TP_Deny_By_Criteria
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Filter definitions.
    ${fdef}=  Catenate  {'filter_criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["*-22-33-44-55-66"]}}}

    tp1.configurePortFilter  DENY_BY_CRITERIA  ${fdef}

    #Stream definitions.
    ${stream1}=  createstream  s1  Ether(src='00:22:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=512   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:21:33:44:55:66', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1024  numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:22:33:44:55:67', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1518  numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1048  numFrames=100
    ${stream5}=  createStream  s5  Ether(src='00:22:33:44:55:65', dst='00:02:00:00:00:ad') / IP()  payload=rand  frameSize=1216  numFrames=100

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    np1Ixia.runIxTraffic

    ${statsMap}=  Catenate  {'Network': {'np1': ['s1', 's2', 's3', 's4', 's5']}, 'Filter': {'f1': ['s1', 's2', 's3', 's4', 's5']}, 'Tool': {'tp1': ['s2', 's3', 's4', 's5']}}

    nto1.stats.analyzeNVSStats  ${statsMap}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

*** Keywords ***
setupSuiteEnv
#    log  Release resource pool: ${RESOURCEPOOL}  console=true
#    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myRequestDef}=  Catenate  {'myPorts': {'type':'NTOPort',
    ...                                         'properties':{'ConnectionType':'standard'}},
    ...                          'myNTO': {'type': 'NTO'}}

    ${myMap}=  Catenate  {'myNTO': {'myPorts': ['np1', 'tp1']}}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myReservation}=  requestResourceBlocking  ${RESOURCETIMEOUT}  ${RESOURCEPOOL}  ${myRequestDef}  requestMapParentBound=${myMap}
    Set Global Variable  ${myReservation}

suiteCleanup
    releaseResources   ${myReservation['responseId']}
