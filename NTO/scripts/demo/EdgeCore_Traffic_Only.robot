*** Settings ***
Suite Setup    setupSuiteEnv

#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-01
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***

*** Test Cases ***
QSFP_100G_Traffic

    AddNTOLocal  nto1  10.218.164.87

    #QSFP28 100Gb port definitions.
    &{P17Def}=  Create Dictionary   name=P17   port=P17   portMode=BIDIRECTIONAL  connType=DAC  mediaType=QSFP28
    &{P18Def}=  Create Dictionary   name=P18   port=P18   portMode=BIDIRECTIONAL  connType=DAC  mediaType=QSFP28

    AddPort  ${nto1}  ${P17Def}
    AddPort  ${nto1}  ${P18Def}


    &{P17IxiaDef}=  Create Dictionary  ixIPaddr=10.218.164.239  ixPort=1/1/4  phyMode=Copper
    AddIxiaPort  ${P17}  ${P17IxiaDef}

    &{P18IxiaDef}=  Create Dictionary  ixIPaddr=10.218.164.239  ixPort=1/1/3  phyMode=Copper
    AddIxiaPort  ${P18}  ${P18IxiaDef}

    # Add Fiters
    AddFilter  ${nto1}  f1    PASS_ALL
    AddFilter  ${nto1}  f2    PASS_ALL

    AddConn    ${nto1}  c1    ${P17}   ${f1}    ${P18}
    AddConn    ${nto1}  c2    ${P18}   ${f2}    ${P17}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:22:33:44:55')/IP(dst="1.1.1.0")/TCP(seq=1)  payload=rand  frameSize=500   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:66:77:88:99:11')/IP(dst="1.0.1.0")/TCP()  payload=rand  frameSize=128   numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:20:30:40:50:60')/IP(dst="1.1.0.0")/TCP()  payload=rand  frameSize=256   numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:02:03:04:05:06')/IP(dst="1.0.1.1")/TCP()  payload=rand  frameSize=512   numFrames=100

    ${stream5}=  createStream  s5  Ether(src='00:11:22:33:44:55')/IP()/TCP()  payload=rand  frameSize=300   numFrames=100
    ${stream6}=  createStream  s6  Ether(src='00:66:77:88:99:11')/IP()/TCP()  payload=rand  frameSize=128   numFrames=100
    ${stream7}=  createStream  s7  Ether(src='00:20:30:40:50:60')/IP()/TCP()  payload=rand  frameSize=256   numFrames=100
    ${stream8}=  createStream  s8  Ether(src='00:02:03:04:05:06')/IP()/TCP()  payload=rand  frameSize=512   numFrames=100

    P17Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    P18Ixia.addIxStreams  ${stream5}  ${stream6}  ${stream7}  ${stream8}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  continuous=True  percentPktRate=100.0

    Set Test Variable  ${maxIterations}  1
    :FOR  ${loop}  IN RANGE  0  ${maxIterations}
    \  Log  Traffic iteration ${loop} out of ${maxIterations} executing...  console=true
    \  P17Ixia.runIxContinuousTraffic
    \  P18Ixia.runIxContinuousTraffic
    \  Sleep  30 seconds
    \  P17Ixia.stopIxTraffic
    \  P18Ixia.stopIxTraffic
    \  Sleep  3 seconds
    \  ${P17IxiaTxFrames}=  P17Ixia.getIxPortTxStats  statName=txFrames
    \  ${P17IxiaRxFrames}=  P17Ixia.getIxPortRxStats  statName=rxFrames
    \  ${P17RxFrames}=  P17.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  ${P18IxiaTxFrames}=  P18Ixia.getIxPortTxStats  statName=txFrames
    \  ${P18IxiaRxFrames}=  P18Ixia.getIxPortRxStats  statName=rxFrames
    \  ${P18RxFrames}=  P18.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  Should Not Be Equal As Integers  ${P17IxiaTxFrames}  0
    \  Should Not Be Equal As Integers  ${P18IxiaTxFrames}  0
    \  Should Be Equal As Integers  ${P17IxiaTxFrames}  ${P18IxiaRxFrames}
    \  Should Be Equal As Integers  ${P18IxiaTxFrames}  ${P17IxiaRxFrames}

    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts


QSFP_40G_Traffic

    AddNTOLocal  nto1  10.218.164.87

    #QSFP28 40Gb port definitions.
    &{P26Def}=  Create Dictionary   name=P26   port=P26   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P28Def}=  Create Dictionary   name=P28   port=P28   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G

    AddPort  ${nto1}  ${P26Def}
    AddPort  ${nto1}  ${P28Def}


    &{P26IxiaDef}=  Create Dictionary  ixIPaddr=10.218.164.239  ixPort=1/10/19  phyMode=Fiber
    AddIxiaPort  ${P26}  ${P26IxiaDef}

    &{P28IxiaDef}=  Create Dictionary  ixIPaddr=10.218.164.239  ixPort=1/10/20  phyMode=Fiber
    AddIxiaPort  ${P28}  ${P28IxiaDef}

    # Add Fiters
    AddFilter  ${nto1}  f1    PASS_ALL
    AddFilter  ${nto1}  f2    PASS_ALL

    AddConn    ${nto1}  c1    ${P26}   ${f1}    ${P28}
    AddConn    ${nto1}  c2    ${P28}   ${f2}    ${P26}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:22:33:44:55')/IP(dst="1.1.1.0")/TCP(seq=1)  payload=rand  frameSize=500   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:66:77:88:99:11')/IP(dst="1.0.1.0")/TCP()  payload=rand  frameSize=128   numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:20:30:40:50:60')/IP(dst="1.1.0.0")/TCP()  payload=rand  frameSize=256   numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:02:03:04:05:06')/IP(dst="1.0.1.1")/TCP()  payload=rand  frameSize=512   numFrames=100

    ${stream5}=  createStream  s5  Ether(src='00:11:22:33:44:55')/IP()/TCP()  payload=rand  frameSize=300   numFrames=100
    ${stream6}=  createStream  s6  Ether(src='00:66:77:88:99:11')/IP()/TCP()  payload=rand  frameSize=128   numFrames=100
    ${stream7}=  createStream  s7  Ether(src='00:20:30:40:50:60')/IP()/TCP()  payload=rand  frameSize=256   numFrames=100
    ${stream8}=  createStream  s8  Ether(src='00:02:03:04:05:06')/IP()/TCP()  payload=rand  frameSize=512   numFrames=100

    P26Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    P28Ixia.addIxStreams  ${stream5}  ${stream6}  ${stream7}  ${stream8}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  continuous=True  percentPktRate=100.0

    Set Test Variable  ${maxIterations}  1
    :FOR  ${loop}  IN RANGE  0  ${maxIterations}
    \  Log  Traffic iteration ${loop} out of ${maxIterations} executing...  console=true
    \  P26Ixia.runIxContinuousTraffic
    \  P28Ixia.runIxContinuousTraffic
    \  Sleep  30 seconds
    \  P26Ixia.stopIxTraffic
    \  P28Ixia.stopIxTraffic
    \  Sleep  3 seconds
    \  ${P26IxiaTxFrames}=  P26Ixia.getIxPortTxStats  statName=txFrames
    \  ${P26IxiaRxFrames}=  P26Ixia.getIxPortRxStats  statName=rxFrames
    \  ${P26RxFrames}=  P26.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  ${P28IxiaTxFrames}=  P28Ixia.getIxPortTxStats  statName=txFrames
    \  ${P28IxiaRxFrames}=  P28Ixia.getIxPortRxStats  statName=rxFrames
    \  ${P28RxFrames}=  P28.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  Should Not Be Equal As Integers  ${P26IxiaTxFrames}  0
    \  Should Not Be Equal As Integers  ${P28IxiaTxFrames}  0
    \  Should Be Equal As Integers  ${P26IxiaTxFrames}  ${P28IxiaRxFrames}
    \  Should Be Equal As Integers  ${P28IxiaTxFrames}  ${P26IxiaRxFrames}

    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts


QSFP_10G_Traffic

    AddNTOLocal  nto1  10.218.164.87

    #QSFP28 10Gb port definitions.
    &{P25-1Def}=  Create Dictionary   name=P25-1   port=P25-1   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P25-2Def}=  Create Dictionary   name=P25-2   port=P25-2   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P25-1Def}
    AddPort  ${nto1}  ${P25-2Def}


    &{P25-1IxiaDef}=  Create Dictionary  ixIPaddr=10.218.164.239  ixPort=1/10/1  phyMode=Fiber
    AddIxiaPort  ${P25-1}  ${P25-1IxiaDef}

    &{P25-2IxiaDef}=  Create Dictionary  ixIPaddr=10.218.164.239  ixPort=1/10/2  phyMode=Fiber
    AddIxiaPort  ${P25-2}  ${P25-2IxiaDef}

    # Add Fiters
    AddFilter  ${nto1}  f1    PASS_ALL
    AddFilter  ${nto1}  f2    PASS_ALL

    AddConn    ${nto1}  c1    ${P25-1}   ${f1}    ${P25-2}
    AddConn    ${nto1}  c2    ${P25-2}   ${f2}    ${P25-1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:22:33:44:55')/IP(dst="1.1.1.0")/TCP(seq=1)  payload=rand  frameSize=500   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:66:77:88:99:11')/IP(dst="1.0.1.0")/TCP()  payload=rand  frameSize=128   numFrames=100
    ${stream3}=  createStream  s3  Ether(src='00:20:30:40:50:60')/IP(dst="1.1.0.0")/TCP()  payload=rand  frameSize=256   numFrames=100
    ${stream4}=  createStream  s4  Ether(src='00:02:03:04:05:06')/IP(dst="1.0.1.1")/TCP()  payload=rand  frameSize=512   numFrames=100

    ${stream5}=  createStream  s5  Ether(src='00:11:22:33:44:55')/IP()/TCP()  payload=rand  frameSize=300   numFrames=100
    ${stream6}=  createStream  s6  Ether(src='00:66:77:88:99:11')/IP()/TCP()  payload=rand  frameSize=128   numFrames=100
    ${stream7}=  createStream  s7  Ether(src='00:20:30:40:50:60')/IP()/TCP()  payload=rand  frameSize=256   numFrames=100
    ${stream8}=  createStream  s8  Ether(src='00:02:03:04:05:06')/IP()/TCP()  payload=rand  frameSize=512   numFrames=100

    P25-1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    P25-2Ixia.addIxStreams  ${stream5}  ${stream6}  ${stream7}  ${stream8}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  continuous=True  percentPktRate=100.0

    Set Test Variable  ${maxIterations}  1
    :FOR  ${loop}  IN RANGE  0  ${maxIterations}
    \  Log  Traffic iteration ${loop} out of ${maxIterations} executing...  console=true
    \  P25-1Ixia.runIxContinuousTraffic
    \  P25-2Ixia.runIxContinuousTraffic
    \  Sleep  30 seconds
    \  P25-1Ixia.stopIxTraffic
    \  P25-2Ixia.stopIxTraffic
    \  Sleep  3 seconds
    \  ${P25-1IxiaTxFrames}=  P25-1Ixia.getIxPortTxStats  statName=txFrames
    \  ${P25-1IxiaRxFrames}=  P25-1Ixia.getIxPortRxStats  statName=rxFrames
    \  ${P25-1RxFrames}=  P25-1.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  ${P25-2IxiaTxFrames}=  P25-2Ixia.getIxPortTxStats  statName=txFrames
    \  ${P25-2IxiaRxFrames}=  P25-2Ixia.getIxPortRxStats  statName=rxFrames
    \  ${P25-2RxFrames}=  P25-2.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  Should Not Be Equal As Integers  ${P25-1IxiaTxFrames}  0
    \  Should Not Be Equal As Integers  ${P25-2IxiaTxFrames}  0
    \  Should Be Equal As Integers  ${P25-1IxiaTxFrames}  ${P25-2IxiaRxFrames}
    \  Should Be Equal As Integers  ${P25-2IxiaTxFrames}  ${P25-1IxiaRxFrames}

    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

*** Keywords ***
setupSuiteEnv
    Set Global Variable  &{ixChassisDict}  &{EMPTY}

