*** Settings ***
Suite Setup     setupSuiteEnv
Suite Teardown  suiteCleanup

#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-02.ann.is.keysight.com
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  60

*** Test Cases ***
AFMR_Fabric_Path_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/CFP_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/CFP_stripping.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'fabric_path_strip_settings': {'enabled': True}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  CFP(src='11:00:aa:00:00:01', dst='11:00:bb:00:00:01', type=0x8903, ftag=10, ttl=3)/Ether(src='22:55:55:55:55:02', dst='22:66:66:66:66:02')/Dot1Q(vlan=102)/IP(src='10.10.10.2', dst='20.20.20.2')  payload=rand  frameSize=100  numFrames=1
    ${stream2}=  createStream  s2  CFP(src='11:22:33:44:55:66', dst='11:22:33:44:55:77')/Ether(src='00:11:11:00:00:08',dst='00:22:22:00:00:08')/MPLS(s=0,label=10008)/MPLS(label=20008)/IPv6(src='1000::0008',dst='2000::0008')/TCP(sport=1056,dport=2008)  payload=ones  frameSize=128  numFrames=10

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_GTP_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/GTP_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/GTP_stripping.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'gtp_strip_settings': {'enabled': True}}
    #${pp1}=  Catenate  {'gtp_strip_settings': {'enabled': False}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions (Note: 51d -> 204 dscp value (binary with two zeros postpended for 6-bit field).
    ${stream1}=  createStream  s1  Ether(src='11:11:11:11:11:11',dst='22:22:22:22:22:22')/IP(src='10.10.10.10',dst='11.11.11.11',tos=204)/UDP(dport=2123)/GTPHeader(S=0,gtp_type=255,teid=0x1010101)/IP(src='20.20.20.20',dst='31.31.31.31')  payload=incr  frameSize=512  numFrames=10
    ${stream2}=  createStream  s2  Ether(src='11:11:11:11:11:11',dst='22:22:22:22:22:22')/IP(src='10.10.10.10',dst='11.11.11.11',tos=204)/UDP(dport=2152)/GTPHeader(S=0,gtp_type=255,teid=0x1010101)/IP(src='20.20.20.20',dst='31.31.31.31')  payload=incr  frameSize=512  numFrames=10
    ${stream3}=  createStream  s3  Ether(src='2c:21:72:65:c7:f0',dst='d8:24:bd:8a:28:f3')/Dot1Q(vlan=1458)/Dot1Q(vlan=24)/IP(src='172.17.33.118',dst='20.20.20.11',tos=204)/UDP(dport=2123)/GTPHeader(S=0,gtp_type=255,teid=0x5555)/IP(src='20.20.20.20',dst='31.31.31.31')/UDP(sport=35984, dport=19523)  payload=incr  frameSize=512  numFrames=10
    ${stream4}=  createStream  s4  Ether(src='2c:21:72:65:c7:f0',dst='d8:24:bd:8a:28:f3')/Dot1Q(vlan=1458)/Dot1Q(vlan=24)/IP(src='172.17.33.118',dst='20.20.20.11',tos=204)/UDP(dport=2152)/GTPHeader(S=0,gtp_type=255,teid=0x5555)/IP(src='20.20.20.20',dst='31.31.31.31')/UDP(sport=35984, dport=19523)  payload=incr  frameSize=512  numFrames=10

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_MPLS_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/MPLS_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/MPLS_stripping.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'mpls_strip_settings': {'enabled': True, 'service_type': 'L2_VPN_WITHOUT_CONTROL'}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/Dot1Q(vlan=777)/Dot1Q(vlan=733)/MPLS(s=0,label=63886)/MPLS(s=0,label=51866)/MPLS(s=0,label=15323)/MPLS(s=0,label=22006)/MPLS(s=0,label=75855)/MPLS(s=0,label=20912)/MPLS(s=0,label=82346)/MPLS(label=31349)/Ether(src='00:82:28:84:99:86',dst='00:88:16:38:34:24',type=0x86dd)/IPv6(src='8282:1793:9436:6647:3267:7344:8266:3034',dst='3747:7478:6801:5779:5979:4355:6014:3075')/TCP(sport=7853,dport=1220)  payload=incr  frameSize=200  numFrames=5

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_Anue_Trailer_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/Anue_trailer_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/Anue_trailer_stripping.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'trailer_strip_settings': {'enabled': True}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${payload}=  configurePayload  altones  100
    ${payload2}=  configurePayload  rand  55
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload}))/ANUE(type=1)  payload=None  frameSize=None  numFrames=1
    ${stream2}=  createStream  s2  Ether(src='00:71:31:93:20:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload2}))/ANUE(type=3)  payload=None  frameSize=None  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_AddTrailerOrigLen
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/AddTrailerOrigLen.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/AddTrailerOriglen.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'packet_length_trailer_settings': {'enabled': True, 'adjust_length': False}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${payload}=  configurePayload  incr  40
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload}))  payload=None  frameSize=None  numFrames=1

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_AddTimeStamp
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/AddAnueTimeStamp.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/AddAnueTimeStamp.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'timestamp_settings': {'enabled': True}}

    afm1.attachResource  ${np1}  10
    np1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${payload}=  configurePayload  incr  40
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload}))  payload=None  frameSize=None  numFrames=1

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ignoreOffsetAndLen=[(76,8), (87,2)]  ordered=${False}
    afm1.detachResource  ${np1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts


AFMR_Packet_Trimming_MAC
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'trim_settings': {'enabled': True, 'retained_bytes': 55, 'retained_headers': 'MAC'}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='11:11:11:11:11:88',dst='22:22:22:22:22:99')/MPLS(s=0,label=10008)/MPLS(label=20008)/IPv6(src='1000::0008',dst='2000::0008')/TCP(sport=1056,dport=2008)  payload=ones  frameSize=1400  numFrames=1

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_Packet_Trimming_MAC_VLAN
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'trim_settings': {'enabled': True, 'retained_bytes': 24, 'retained_headers': 'MAC_VLAN'}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:00:00:21',dst='00:22:22:00:00:21')/Dot1Q(id=121)/MPLS(label=10021)/IP(src='10.10.10.21',dst='20.20.20.21')/TCP(sport=1021,dport=2021)  payload=ones  frameSize=988  numFrames=1

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_Packet_Trimming_MAC_VLAN_MPLS
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'trim_settings': {'enabled': True, 'retained_bytes': 33, 'retained_headers': 'MAC_VLAN_MPLS'}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:00:00:21',dst='00:22:22:00:00:21')/Dot1Q(id=121)/MPLS(label=10021)/IP(src='10.10.10.21',dst='20.20.20.21')/TCP(sport=1021,dport=2021)  payload=ones  frameSize=501  numFrames=1

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_Packet_Trimming_MAC_VLAN_MPLS_L3
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/PktTrimming.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/PktTrimming.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'trim_settings': {'enabled': True, 'retained_bytes': 21, 'retained_headers': 'MAC_VLAN_MPLS_L3'}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:00:00:21',dst='00:22:22:00:00:21')/Dot1Q(id=121)/MPLS(label=10021)/IPv6(src='1000::0008',dst='2000::0008')/TCP(sport=1021,dport=2021)  payload=ones  frameSize=333  numFrames=1

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_L2GRE_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/L2GRE_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/L2GRE_stripping.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'l2gre_strip_settings': {'enabled': True}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:11:11:22',dst='00:22:22:22:22:22')/Dot1Q(vlan=2222)/MPLS(s=0,label=40095)/MPLS(s=0,label=30095)/MPLS(s=0,label=20095)/MPLS(label=10095)/IP(src='11.11.11.22',dst='22.22.22.22')/GRE(proto=0x6558)/Ether(src='00:33:33:33:33:22',dst='00:44:44:44:44:22')/Dot1Q(vlan=3322)/IP(src='33.33.33.22',dst='44.44.44.22',ttl=1,proto=61)  payload=incr  frameSize=1600  numFrames=10

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_VXLAN_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/VXLAN_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/VXLAN_stripping.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'vxlan_strip_settings': {'enabled': True}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:11:11:11',dst='00:22:22:22:22:11')/Dot1Q(vlan=1122)/IP(src='11.11.11.11',dst='22.22.22.11')/UDP(sport=4789,dport=4789)/VXLAN(vni=16777215)/Ether(src='00:33:33:33:33:33',dst='00:44:44:44:44:44')/Dot1Q(vlan=3344)/IP(src='33.33.33.33',dst='44.44.44.44')  payload=incr  frameSize=483  numFrames=10

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_ERSPAN_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/ERSPAN_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/ERSPAN_stripping.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'erspan_strip_settings': {'enabled': True, 'empty_header': False}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:11:11:07',dst='00:22:22:22:22:07')/IP(src='11.11.11.7',dst='22.22.22.7',tos=34)/GRE(key_present=0,seqnum_present=1,seqence_number=426295,proto=0x88be)/ERSPAN(version=1,truncated=1,unknown7=27272727,vlan=222,span_id=22)/Ether(src='00:33:33:33:33:07',dst='00:44:44:44:44:07')/IP(src='33.33.33.7',dst='44.44.44.7')/TCP(sport=7007,dport=80)  payload=incr  frameSize=1256  numFrames=10

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_PPPoE_Stripping
    [Tags]  incomplete

    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/PPPoE_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/PPPoE_stripping.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'pppoe_strip_settings': {'enabled': True}}

    afm1.attachResource  ${tp1}  25
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:11:11:48',dst='00:22:22:22:22:48')/PPPoE()/PPP()/IP(src='11.11.11.7',dst='22.22.22.7')  payload=incr  frameSize=125  numFrames=1
    ${stream2}=  createStream  s2  Ether(src='00:11:11:11:11:49',dst='00:22:22:22:22:49')/PPPoE()/PPP(proto=0x0023)  payload=incr  frameSize=114  numFrames=1
    ${stream3}=  createStream  s3  Ether(src='00:11:11:11:11:50',dst='00:22:22:22:22:50')/PPPoE()/PPP(proto=0x0057)/IPv6(src='1000::0008',dst='2000::0008')  payload=incr  frameSize=114  numFrames=1
    ${stream4}=  createStream  s4  Ether(src='00:11:11:11:11:51',dst='00:22:22:22:22:51')/PPPoE(code=0x09)  payload=incr  frameSize=85  numFrames=1
    ${stream5}=  createStream  s5  Ether(src='00:11:11:11:11:52',dst='00:22:22:22:22:52')/PPPoE(code=0x07)  payload=ones  frameSize=78  numFrames=1
    ${stream6}=  createStream  s6  Ether(src='00:11:11:11:11:53',dst='00:22:22:22:22:53')/PPPoE(code=0x19)  payload=ones  frameSize=74  numFrames=1
    ${stream7}=  createStream  s7  Ether(src='00:11:11:11:11:54',dst='00:22:22:22:22:54')/PPPoE(code=0x65)  payload=ones  frameSize=68  numFrames=1
    ${stream8}=  createStream  s8  Ether(src='00:11:11:11:11:55',dst='00:22:22:22:22:55')/PPPoE(code=0xa7)  payload=ones  frameSize=90  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}  ${stream6}  ${stream7}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_ETag_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/ETag_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/ETag_stripping.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'etag_strip_settings': {'enabled': True}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:11:11:02', dst='00:22:22:22:22:02')/ETag(pcp=1, iecid_base=31, ecid_base=31)/IP(src='10.11.11.11', dst='30.11.11.11')/UDP(sport=3000,dport=1024)  payload=incr  frameSize=256  numFrames=10

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_VNTag_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/VNTag_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/VNTag_stripping.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'vntag_strip_settings': {'enabled': True}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:11:11:11:02', dst='00:22:22:22:22:02')/VNTag(dir=1, dst=33, src=1024)/Dot1Q(vlan=50)/Dot1Q(vlan=102)/IP(src='10.11.11.11', dst='30.11.11.11')/TCP(sport=4000,dport=5000)  payload=incr  frameSize=256  numFrames=10

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_LISP_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/LISP_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/LISP_stripping.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'lisp_strip_settings': {'enabled': True}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='2c:21:72:65:c7:f0', dst='d8:24:bd:8a:28:f3')/IP(src='172.17.33.118', dst='20.20.20.11')/UDP()/LISP_Data(e=1, flags=1,locator_status=0x7000)/IP(src='10.33.33.33', dst='20.33.33.33')/TCP(sport=35984,dport=19523)  payload=incr  frameSize=1024  numFrames=10
    ${stream2}=  createStream  s2  Ether(src='2c:21:72:65:c7:f0', dst='d8:24:bd:8a:28:f3')/IP(src='172.17.33.118', dst='20.20.20.11')/UDP()/LISP_Data(i=1, n=1, flags=7, nonce=0x182, locator_status=0x7fff)/IPv6(src='1033::3333', dst='2033::3333')/TCP(sport=35984,dport=19523)  payload=incr  frameSize=202  numFrames=10
    ${stream3}=  createStream  s3  Ether(src='2c:21:72:65:c7:f0', dst='d8:24:bd:8a:28:f3')/IPv6(src='1721::3118', dst='2020::2011')/UDP()/LISP_Data(i=0, n=0, flags=2, locator_status=0xf111)/IP(src='10.33.33.33', dst='20.33.33.33')/TCP(sport=35984,dport=19523)  payload=incr  frameSize=303  numFrames=10

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Data_Masking_L2_Start
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/DataMasking.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/DataMasking.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'data_masking_settings': {'enabled': True, 'fill_value': 0xbb, 'length': 10, 'offset_layer': 'L2_START', 'offset_value': 1}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${payload}=  configurePayload  incr  40
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload}))  payload=None  frameSize=None  numFrames=1
    ${stream2}=  createStream  s2  CFP(src='11:00:aa:00:00:01', dst='11:00:bb:00:00:01', type=0x8903, ftag=10, ttl=3)/Ether(src='22:55:55:55:55:02', dst='22:66:66:66:66:02')/Dot1Q(vlan=102)/IP(src='10.10.10.2', dst='20.20.20.2')  payload=rand  frameSize=100  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Data_Masking_L2_End
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/DataMasking.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/DataMasking.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'data_masking_settings': {'enabled': True, 'fill_value': 0xcc, 'length': 12, 'offset_layer': 'L2_END', 'offset_value': 0}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${payload}=  configurePayload  incr  40
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload}))  payload=None  frameSize=None  numFrames=1
    ${stream2}=  createStream  s2  CFP(src='11:00:aa:00:00:01', dst='11:00:bb:00:00:01', type=0x8903, ftag=10, ttl=3)/Ether(src='22:55:55:55:55:02', dst='22:66:66:66:66:02')/Dot1Q(vlan=102)/IPv6()/TCP()  payload=rand  frameSize=100  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Data_Masking_L3_End
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddAFMR  ${nto1}  afm1  L1-AFM

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/DataMasking.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/DataMasking.pcap

    nto1.getAFMResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'data_masking_settings': {'enabled': True, 'fill_value': 0xdd, 'length': 16, 'offset_layer': 'L3_END', 'offset_value': 2}}

    afm1.attachResource  ${tp1}  10
    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${payload}=  configurePayload  incr  40
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/IP()/Raw(load=bytes(${payload}))  payload=None  frameSize=None  numFrames=1
    ${stream2}=  createStream  s2  CFP(src='11:00:aa:00:00:01', dst='11:00:bb:00:00:01', type=0x8903, ftag=10, ttl=3)/Ether(src='22:55:55:55:55:02', dst='22:66:66:66:66:02')/Dot1Q(vlan=102)/IP()/TCP()  payload=rand  frameSize=100  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    afm1.detachResource  ${tp1}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

*** Keywords ***
setupSuiteEnv
#    log  Release resource pool: ${RESOURCEPOOL}  console=true
#    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myRequestDef}=  Catenate  {'myPorts': {'type':'NTOPort',
    ...                                         'properties':{'ConnectionType':'standard'}},
    ...                          'myNTO': {'type': 'NTO'}}

    ${myMap}=  Catenate  {'myNTO': {'myPorts': ['np1', 'tp1']}}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myReservation}=  requestResourceBlocking  ${RESOURCETIMEOUT}  ${RESOURCEPOOL}  ${myRequestDef}  requestMapParentBound=${myMap}
    Set Global Variable  ${myReservation}

suiteCleanup
    releaseResources   ${myReservation['responseId']}
