*** Settings ***
Suite Setup    setupSuiteEnv

#Libraries
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  60

*** Test Cases ***
ATIP_Non_App_Forwarding
    AddNTOLocal  nto1  10.218.164.175
    AddATIP  ${nto1}  atip1  L1-ATIP

    &{np1Def}=  Create Dictionary  name=np1  port=P01  portMode=BIDIRECTIONAL  connType=standalone  mediaType=SFP_PLUS_10G
    &{np2Def}=  Create Dictionary  name=np2  port=P02  portMode=BIDIRECTIONAL  connType=standalone  mediaType=SFP_PLUS_10G

    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'BIDIRECTIONAL'}
    ${pg2}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'BIDIRECTIONAL'}

    AddPort  ${nto1}  ${np1Def}
    AddPort  ${nto1}  ${np2Def}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddFilter  ${nto1}  f2  PASS_ALL
    AddFilter  ${nto1}  f3  PASS_ALL

    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${np2}
    AddConn    ${nto1}  c2  ${np2}  ${f2}  ${np1}

    AddConn    ${nto1}  c3  ${np1}  ${f3}  None
    AddConn    ${nto1}  c4  ${np2}  ${f3}  None

    nto1.getATIPResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    atip1.attachResource  ${f3}
    #f3.attachATIPResource  ${atip1}

    #Dummy sleep here <replace w/ATIP test code>
    Sleep  5s

    nto1.cleanupNVSDevices


ATIP_Non_App_Forwarding_with_PGs
    AddNTOLocal  nto1  10.218.164.175
    AddATIP  ${nto1}  atip1  L1-ATIP

    &{np1Def}=  Create Dictionary  name=np1  port=P01  portMode=BIDIRECTIONAL  connType=standalone  mediaType=SFP_PLUS_10G
    &{np2Def}=  Create Dictionary  name=np2  port=P02  portMode=BIDIRECTIONAL  connType=standalone  mediaType=SFP_PLUS_10G

    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'BIDIRECTIONAL'}
    ${pg2}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'BIDIRECTIONAL'}

    AddPort  ${nto1}  ${np1Def}
    AddPort  ${nto1}  ${np2Def}

    AddPortGrp  ${nto1}  pg1  ${pg1}  ${np1}
    AddPortGrp  ${nto1}  pg2  ${pg2}  ${np2}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddFilter  ${nto1}  f2  PASS_ALL
    AddFilter  ${nto1}  f3  PASS_ALL

    AddConn    ${nto1}  c1  ${pg1}  ${f1}  ${pg2}
    AddConn    ${nto1}  c2  ${pg2}  ${f2}  ${pg1}

    AddConn    ${nto1}  c3  ${pg1}  ${f3}  None
    AddConn    ${nto1}  c4  ${pg2}  ${f3}  None

    nto1.getATIPResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    atip1.attachResource  ${f3}
    #f3.attachATIPResource  ${atip1}

    #Dummy sleep here <replace w/ATIP test code>
    Sleep  5s

    nto1.cleanupNVSDevices


ATIP_App_Forwarding
    AddNTOLocal  nto1  10.218.164.175
    AddATIP  ${nto1}  atip1  L1-ATIP

    &{np1Def}=  Create Dictionary  name=np1  port=P01  portMode=BIDIRECTIONAL  connType=standalone  mediaType=SFP_PLUS_10G
    &{np2Def}=  Create Dictionary  name=np2  port=P02  portMode=BIDIRECTIONAL  connType=standalone  mediaType=SFP_PLUS_10G
    &{tp1Def}=  Create Dictionary  name=tp1  port=P03  portMode=TOOL           connType=standalone  mediaType=SFP_PLUS_10G
    &{tp2Def}=  Create Dictionary  name=tp2  port=P04  portMode=TOOL           connType=standalone  mediaType=SFP_PLUS_10G
    &{tp3Def}=  Create Dictionary  name=tp3  port=P05  portMode=TOOL           connType=standalone  mediaType=SFP_PLUS_10G

    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'BIDIRECTIONAL'}
    ${pg2}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'BIDIRECTIONAL'}

    AddPort  ${nto1}  ${np1Def}
    AddPort  ${nto1}  ${np2Def}
    AddPort  ${nto1}  ${tp1Def}
    AddPort  ${nto1}  ${tp2Def}
    AddPort  ${nto1}  ${tp3Def}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddFilter  ${nto1}  f2  PASS_ALL
    AddFilter  ${nto1}  f3  PASS_ALL

    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${np2}
    AddConn    ${nto1}  c2  ${np2}  ${f2}  ${np1}

    AddConn    ${nto1}  c3  ${np1}  ${f3}  None
    AddConn    ${nto1}  c4  ${np2}  ${f3}  None

    AddConn    ${nto1}  c5  None  ${f3}  ${tp1}
    AddConn    ${nto1}  c6  None  ${f3}  ${tp2}
    AddConn    ${nto1}  c7  None  ${f3}  ${tp3}

    nto1.getATIPResources

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    atip1.attachResource  ${f3}
    #f3.attachATIPResource  ${atip1}

    @{dL1}=  Create List  ${tp1.portId}
    @{dL2}=  Create List  ${tp2.portId}
    @{dL3}=  Create List  ${tp3.portId}

    &{appFwd1}=  Create Dictionary  application_name=default                        dest_port_list=${dL1}
    &{appFwd2}=  Create Dictionary  application_name=pop3/smtp   vlan_id=100        dest_port_list=${dL2}
    &{appFwd3}=  Create Dictionary  application_name=google      vlan_id=200        dest_port_list=${dL3}


    f3.configureAppForwardingMaps  ${appFwd1}  ${appFwd2}  ${appFwd3}

    #Dummy sleep here <replace w/ATIP test code>
    Sleep  5s

    nto1.cleanupNVSDevices

*** Keywords ***
setupSuiteEnv
    Set Global Variable  &{ixChassisDict}  &{EMPTY}
