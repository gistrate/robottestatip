*** Settings ***
Suite Setup     setupSuiteEnv
Suite Teardown  suiteCleanup

#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-02.ann.is.keysight.com
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  60

*** Test Cases ***
Std_Vlan_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/Vlan_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/Vlan_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'ingress_count': 1, 'strip_mode': 'INGRESS'}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:33',dst='00:60:89:47:48:22')/Dot1Q(vlan=777)/IP()/UDP()  payload=ones  frameSize=80  numFrames=1
    ${stream2}=  createStream  s2  Ether(src='00:73:31:93:18:55',dst='00:60:89:47:48:44')/Dot1Q(vlan=111)/Dot1Q(vlan=222)/IP()/UDP()  payload=incr  frameSize=128  numFrames=1
    ${stream3}=  createStream  s3  Ether(src='00:73:31:93:18:77',dst='00:60:89:47:48:66')/Dot1Q(vlan=3)/Dot1Q(vlan=4)/Dot1Q(vlan=5)/IP()/UDP()  payload=altones  frameSize=233  numFrames=1
    ${stream4}=  createStream  s4  Ether(src='00:73:31:93:18:99',dst='00:60:89:47:48:88')/IP()/TCP()  payload=zeros  frameSize=77  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    np1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

Std_Port_Tagging
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/Port_tagging.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/Port_tagging.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    ${pp1}=  Catenate  {'std_port_tagging_settings': {'enabled': True, 'vlan_id': 999}}

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/Dot1Q(vlan=777)/Dot1Q(vlan=733)/MPLS(s=0,label=63886)/MPLS(s=0,label=51866)/MPLS(s=0,label=15323)/MPLS(s=0,label=22006)/MPLS(s=0,label=75855)/MPLS(s=0,label=20912)/MPLS(s=0,label=82346)/MPLS(label=31349)/Ether(src='00:82:28:84:99:86',dst='00:88:16:38:34:24',type=0x86dd)/IPv6(src='8282:1793:9436:6647:3267:7344:8266:3034',dst='3747:7478:6801:5779:5979:4355:6014:3075')/TCP(sport=7853,dport=1220)  payload=incr  frameSize=200  numFrames=5
    ${stream2}=  createStream  s2  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/MPLS(label=51866)/Ether(src='B4:99:BA:00:00:40',dst='2C:30:33:01:80:C2')/IP()  payload=incr  frameSize=65  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    np1.configurePacketProcessing  ${pp1}
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

AFMR_Fabric_Path_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    ${portInfo}=  tp1.getPortInfo
    ${afmModuleBased}=  Set Variable  ${portInfo['supported_feature_info']['afm_module_based']}
    ${afmResourceBased}=  Set Variable  ${portInfo['supported_feature_info']['afm_resource_based']}

    Run Keyword If  '${afmModuleBased}' == 'NOT_SUPPORTED' and '${afmResourceBased}' == 'NOT_SUPPORTED'  Fail

    ${afmrReservation}=  Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  requestAFMRResource

    ${afmrInfo}=  Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  getResourceInfoById  ${afmrReservation['myAFMR']['myAFMRLanes']['L1']['parent']}

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  AddAFMR  ${nto1}  afm1  ${afmrInfo['properties']['Name']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/CFP_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/CFP_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'fabric_path_strip_settings': {'enabled': True}}

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  afm1.attachResource  ${tp1}  ${afmrReservation['myAFMR']['myAFMRLanes']['L1']['properties']['Bandwidth']}

    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  CFP(src='11:00:aa:00:00:01', dst='11:00:bb:00:00:01', type=0x8903, ftag=10, ttl=3)/Ether(src='22:55:55:55:55:02', dst='22:66:66:66:66:02')/Dot1Q(vlan=102)/IP(src='10.10.10.2', dst='20.20.20.2')  payload=rand  frameSize=100  numFrames=1
    ${stream2}=  createStream  s2  CFP(src='11:22:33:44:55:66', dst='11:22:33:44:55:77')/Ether(src='00:11:11:00:00:08',dst='00:22:22:00:00:08')/MPLS(s=0,label=10008)/MPLS(label=20008)/IPv6(src='1000::0008',dst='2000::0008')/TCP(sport=1056,dport=2008)  payload=ones  frameSize=128  numFrames=10

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  afm1.detachResource  ${tp1}

    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  releaseResources   ${afmrReservation['responseId']}

AFMR_GTP_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    ${portInfo}=  tp1.getPortInfo
    ${afmModuleBased}=  Set Variable  ${portInfo['supported_feature_info']['afm_module_based']}
    ${afmResourceBased}=  Set Variable  ${portInfo['supported_feature_info']['afm_resource_based']}

    Run Keyword If  '${afmModuleBased}' == 'NOT_SUPPORTED' and '${afmResourceBased}' == 'NOT_SUPPORTED'  Fail

    ${afmrReservation}=  Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  requestAFMRResource

    ${afmrInfo}=  Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  getResourceInfoById  ${afmrReservation['myAFMR']['myAFMRLanes']['L1']['parent']}

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  AddAFMR  ${nto1}  afm1  ${afmrInfo['properties']['Name']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/GTP_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/GTP_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'gtp_strip_settings': {'enabled': True}}

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  afm1.attachResource  ${tp1}  ${afmrReservation['myAFMR']['myAFMRLanes']['L1']['properties']['Bandwidth']}

    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions (Note: 51d -> 204 dscp value (binary with two zeros postpended for 6-bit field).
    ${stream1}=  createStream  s1  Ether(src='11:11:11:11:11:11',dst='22:22:22:22:22:22')/IP(src='10.10.10.10',dst='11.11.11.11',tos=204)/UDP(dport=2123)/GTPHeader(S=0,gtp_type=255,teid=0x1010101)/IP(src='20.20.20.20',dst='31.31.31.31')  payload=incr  frameSize=512  numFrames=10
    ${stream2}=  createStream  s2  Ether(src='11:11:11:11:11:11',dst='22:22:22:22:22:22')/IP(src='10.10.10.10',dst='11.11.11.11',tos=204)/UDP(dport=2152)/GTPHeader(S=0,gtp_type=255,teid=0x1010101)/IP(src='20.20.20.20',dst='31.31.31.31')  payload=incr  frameSize=512  numFrames=10
    ${stream3}=  createStream  s3  Ether(src='2c:21:72:65:c7:f0',dst='d8:24:bd:8a:28:f3')/Dot1Q(vlan=1458)/Dot1Q(vlan=24)/IP(src='172.17.33.118',dst='20.20.20.11',tos=204)/UDP(dport=2123)/GTPHeader(S=0,gtp_type=255,teid=0x5555)/IP(src='20.20.20.20',dst='31.31.31.31')/UDP(sport=35984, dport=19523)  payload=incr  frameSize=512  numFrames=10
    ${stream4}=  createStream  s4  Ether(src='2c:21:72:65:c7:f0',dst='d8:24:bd:8a:28:f3')/Dot1Q(vlan=1458)/Dot1Q(vlan=24)/IP(src='172.17.33.118',dst='20.20.20.11',tos=204)/UDP(dport=2152)/GTPHeader(S=0,gtp_type=255,teid=0x5555)/IP(src='20.20.20.20',dst='31.31.31.31')/UDP(sport=35984, dport=19523)  payload=incr  frameSize=512  numFrames=10

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  afm1.detachResource  ${tp1}

    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  releaseResources   ${afmrReservation['responseId']}

AFMR_MPLS_Stripping
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

    ${portInfo}=  tp1.getPortInfo
    ${afmModuleBased}=  Set Variable  ${portInfo['supported_feature_info']['afm_module_based']}
    ${afmResourceBased}=  Set Variable  ${portInfo['supported_feature_info']['afm_resource_based']}

    Run Keyword If  '${afmModuleBased}' == 'NOT_SUPPORTED' and '${afmResourceBased}' == 'NOT_SUPPORTED'  Fail

    ${afmrReservation}=  Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  requestAFMRResource

    ${afmrInfo}=  Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  getResourceInfoById  ${afmrReservation['myAFMR']['myAFMRLanes']['L1']['parent']}

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  AddAFMR  ${nto1}  afm1  ${afmrInfo['properties']['Name']}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    ${captureFile}=  Set Variable  ${OUTPUTDIR}/MPLS_stripping.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/MPLS_stripping.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    ${pp1}=  Catenate  {'mpls_strip_settings': {'enabled': True, 'service_type': 'L2_VPN_WITHOUT_CONTROL'}}

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  afm1.attachResource  ${tp1}  ${afmrReservation['myAFMR']['myAFMRLanes']['L1']['properties']['Bandwidth']}

    tp1.configurePacketProcessing  ${pp1}

    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:73:31:93:18:46',dst='00:60:89:47:48:36')/Dot1Q(vlan=777)/Dot1Q(vlan=733)/MPLS(s=0,label=63886)/MPLS(s=0,label=51866)/MPLS(s=0,label=15323)/MPLS(s=0,label=22006)/MPLS(s=0,label=75855)/MPLS(s=0,label=20912)/MPLS(s=0,label=82346)/MPLS(label=31349)/Ether(src='00:82:28:84:99:86',dst='00:88:16:38:34:24',type=0x86dd)/IPv6(src='8282:1793:9436:6647:3267:7344:8266:3034',dst='3747:7478:6801:5779:5979:4355:6014:3075')/TCP(sport=7853,dport=1220)  payload=incr  frameSize=200  numFrames=5

    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  100

    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  frames=${np1Ixia.sentFrames}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  afm1.detachResource  ${tp1}

    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

    Run Keyword If  '${afmResourceBased}' == 'HARDWARE'  Run Keyword
    ...  releaseResources   ${afmrReservation['responseId']}

*** Keywords ***
setupSuiteEnv
#    log  Release resource pool: ${RESOURCEPOOL}  console=true
#    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myRequestDef}=  Catenate  {'myPorts': {'type':'NTOPort',
    ...                                         'properties':{'ConnectionType':'standard'}},
    ...                          'myNTO': {'type': 'NTO'}}

    ${myMap}=  Catenate  {'myNTO': {'myPorts': ['np1', 'tp1']}}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myReservation}=  requestResourceBlocking  ${RESOURCETIMEOUT}  ${RESOURCEPOOL}  ${myRequestDef}  requestMapParentBound=${myMap}
    Set Global Variable  ${myReservation}

suiteCleanup
    releaseResources   ${myReservation['responseId']}

requestAFMRResource
    ${myAFMRRequestDef}=  Catenate  {'myAFMRLanes': {'type': 'AFMR_Lane',
    ...                                              'properties': {}},
    ...                              'myAFMR': {'type': 'AFM_Resource'},
    ...                              'myNTO': {'type': 'NTO',
    ...                                        'properties': {'IPAddress': '${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}'}}}

    ${myAFMRMap}=  Catenate  {'myAFMR': {'myAFMRLanes': ['L1']}}

    log  Requesting AFMR resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myAFMRResv}=  requestResourceBlocking  ${RESOURCETIMEOUT}  ${RESOURCEPOOL}  ${myAFMRRequestDef}  requestMapParentBound=${myAFMRMap}
    [return]  ${myAFMRResv}
