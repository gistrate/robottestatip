*** Settings ***

Library    Remote    https://usaus-kvm-testSrv:8270     WITH NAME  srvr
Library    Remote    https://usaus-kvm-testClient:8270  WITH NAME  clnt

Suite Setup     Run Keywords  Get_Remote_Hosts_and_Verify_Connectivity  Generate_Server_SSL_Cert  Generate_Client_SSL_Cert
Suite Teardown  Stop_SSL_Server

*** Variables ***

*** Keywords ***
Get_Remote_Hosts_and_Verify_Connectivity
    ${output}=  srvr.getHostname
    Set Suite Variable   ${serverHost}   ${output}
    log  \nremote server hostname - ${serverHost}  console=true

    ${output}=  clnt.getHostname
    Set Suite Variable   ${clientHost}   ${output}
    log  remote client hostname - ${clientHost}  console=true

    srvr.checkPing   ${clientHost}
    clnt.checkPing   ${serverHost}

Stop_SSL_Server
    srvr.terminateOpenSSLServer  ${srvPID}

Generate_Server_SSL_Cert
    ${myUUID}=  srvr.generateUUID
    Set Suite Variable  ${serverSSLCert}  /tmp/${myUUID}.pem
    srvr.generateSSLCert     ${serverSSLCert}

Generate_Client_SSL_Cert
    ${myUUID}=  clnt.generateUUID
    Set Suite Variable  ${clientSSLCert}  /tmp/${myUUID}.pem
    clnt.generateSSLCert  ${clientSSLCert}

SSL_Connection_Test
    [Arguments]    ${serverPEM}  ${clientPEM}  ${host}  ${runtime}  ${timeout}
    ${srvPID}=  srvr.startOpenSSLServer  ${serverPEM}
    Set Suite Variable  ${srvPID}
    clnt.runSSLConnectionTest  ${clientPEM}  ${host}  ${runtime}  ${timeout}

*** Test Cases ***
Template_Testing_Example
    [Template]    SSL_Connection_Test
    ${serverSSLCert}  ${clientSSLCert}  ${serverHost}  5   120
    ${serverSSLCert}  ${clientSSLCert}  ${serverHost}  10  120
    ${serverSSLCert}  ${clientSSLCert}  ${serverHost}  15  120
    ${serverSSLCert}  ${clientSSLCert}  ${serverHost}  20  120
