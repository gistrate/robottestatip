*** Settings ***
Suite Setup     setupSuiteEnv
Suite Teardown  suiteCleanup

#Libraries
Library  ./resourceMgr_module/resourceMgr.py  10.218.174.57
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot
Test Teardown  nto1.cleanupNVSDevices

*** Variables ***
${RESOURCETIMEOUT}=  60


*** Test Cases ***
Test_6322

    AddNTOPktProc  nto1  ${myResource}
    AddPorts  ${nto1}  ${myHWList}  ${myResource}

    ${f1def}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-55-55-55-55-55"]}}}
    ${f2def}=  Catenate  {'criteria': {'logical_operation': 'AND', 'mac_src': {'addr': ["00-66-66-66-66-66"]}}}

    AddFilter  ${nto1}  f1  PASS_BY_CRITERIA  ${f1def}
    AddFilter  ${nto1}  f2  PASS_BY_CRITERIA  ${f2def}
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    AddConn    ${nto1}  c2  ${np1}  ${f2}  ${tp2}

    AddAFMR  ${nto1}  afm1  L1-AFM

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/orig.pcap

    ${captureFile1}=  Set Variable  ${OUTPUTDIR}/modified1.enc
    ${capturePcap1}=  Set Variable  ${OUTPUTDIR}/modified1.pcap

    ${captureFile2}=  Set Variable  ${OUTPUTDIR}/modified2.enc
    ${capturePcap2}=  Set Variable  ${OUTPUTDIR}/modified2.pcap

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}
    afm1.attachResource  ${tp2}
    afm1.attachResource  ${tp1}
    afm1.attachResource  ${f1}
    afm1.attachResource  ${f2}

    ${pp1}=  Catenate  {'fabric_path_strip_settings': {'enabled': True}}
    ${pp2}=  Catenate  {'mpls_strip_settings': {'enabled': True, 'service_type': 'L2_VPN_WITHOUT_CONTROL'}}
    #Stream definitions.
    ${stream1}=  createStream  s1  CFP(src='00:00:aa:00:00:01', dst='00:00:bb:00:00:04', type=0x8903, ftag=10, ttl=3)/Ether(src='00:55:55:55:55:55',dst='00:60:89:47:48:36')/MPLS(label=51866)/Ether(src='00:73:31:93:18:33',dst='00:60:89:47:48:22')/Dot1Q(vlan=777)/IP()/UDP(sport=1001,dport=2001)  payload=ones  frameSize=100  numFrames=1
    ${stream2}=  createStream  s2  Ether(src='00:66:66:66:66:66',dst='00:60:89:47:48:36')/MPLS(label=51866)/Ether(src='B4:99:BA:00:00:40',dst='2C:30:33:01:80:C2')/IP()  payload=incr  frameSize=100  numFrames=1

    np1Ixia.addIxStreams  ${stream1}  ${stream2}
    nto1.configureAllIxPorts

    np1.configurePacketProcessing  ${pp1}
    f1.configurePacketProcessing  ${pp2}
    f2.configurePacketProcessing  ${pp2}

    nto1.configureAllIxStreams
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    tp2Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp2Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  100
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureFile1}  1  100
    convertEncToPcap  ${captureFile1}  ${capturePcap1}
    tp1Ixia.scapyImportPcap  ${capturePcap1}

    tp2Ixia.exportIxFramesToFile  ${captureFile2}  1  100
    convertEncToPcap  ${captureFile2}  ${capturePcap2}
    tp2Ixia.scapyImportPcap  ${capturePcap2}

    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  frames=${strippedFrames}

    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}  ordered=${False}
    compareFrames  ${strippedFrames}  ${tp2Ixia.recdFrames}  ordered=${False}

    ${statsMap}=  Catenate  {'Network': {'np1': [ 's1' , 's2']}, 'Filter': {'f1': [ 's1' ], 'f2': [ 's2']}, 'Tool': {'tp1': [ 's1' ], 'tp2': ['s2']}}

    nto1.stats.analyzeNVSStats  ${statsMap}

    nto1.cleanupAllIxPorts

*** Keywords ***
setupSuiteEnv
#    log  Release resource pool: ${RESOURCEPOOL}  console=true
#    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myHWList}=  Create List
    ...  standard np1 any NETWORK
    ...  standard tp2 any TOOL
    ...  standard tp1 any TOOL
    Set Global Variable  ${myHWList}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myResource}=  requestResourceBlocking  ${myHWList}  ${RESOURCEPOOL}  NTO  ${RESOURCETIMEOUT}
    Set Global Variable  ${myResource}

suiteCleanup
    releaseResources   ${myResource}
