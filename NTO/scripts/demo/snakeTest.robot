*** Settings ***
Suite Setup    setupSuiteEnv

#Libraries
Library  ./NTO/frwk/Utils.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Test Cases ***
Snake Test
    AddNTOLocal  nto1  10.218.164.212

    #10Gb Card P2A port definitions.
    &{P2A-01Def}=  Create Dictionary   name=P2A-01   port=P2A-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-02Def}=  Create Dictionary   name=P2A-02   port=P2A-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-03Def}=  Create Dictionary   name=P2A-03   port=P2A-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-04Def}=  Create Dictionary   name=P2A-04   port=P2A-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-05Def}=  Create Dictionary   name=P2A-05   port=P2A-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-06Def}=  Create Dictionary   name=P2A-06   port=P2A-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-07Def}=  Create Dictionary   name=P2A-07   port=P2A-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-08Def}=  Create Dictionary   name=P2A-08   port=P2A-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-09Def}=  Create Dictionary   name=P2A-09   port=P2A-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-10Def}=  Create Dictionary   name=P2A-10   port=P2A-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-11Def}=  Create Dictionary   name=P2A-11   port=P2A-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-12Def}=  Create Dictionary   name=P2A-12   port=P2A-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-13Def}=  Create Dictionary   name=P2A-13   port=P2A-13   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-14Def}=  Create Dictionary   name=P2A-14   port=P2A-14   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-15Def}=  Create Dictionary   name=P2A-15   port=P2A-15   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2A-16Def}=  Create Dictionary   name=P2A-16   port=P2A-16   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P2A-01Def}
    AddPort  ${nto1}  ${P2A-02Def}
    AddPort  ${nto1}  ${P2A-03Def}
    AddPort  ${nto1}  ${P2A-04Def}
    AddPort  ${nto1}  ${P2A-05Def}
    AddPort  ${nto1}  ${P2A-06Def}
    AddPort  ${nto1}  ${P2A-07Def}
    AddPort  ${nto1}  ${P2A-08Def}
    AddPort  ${nto1}  ${P2A-09Def}
    AddPort  ${nto1}  ${P2A-10Def}
    AddPort  ${nto1}  ${P2A-11Def}
    AddPort  ${nto1}  ${P2A-12Def}
    AddPort  ${nto1}  ${P2A-13Def}
    AddPort  ${nto1}  ${P2A-14Def}
    AddPort  ${nto1}  ${P2A-15Def}
    AddPort  ${nto1}  ${P2A-16Def}

    #10Gb Card P2B port definitions.
    &{P2B-01Def}=  Create Dictionary   name=P2B-01   port=P2B-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-02Def}=  Create Dictionary   name=P2B-02   port=P2B-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-03Def}=  Create Dictionary   name=P2B-03   port=P2B-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-04Def}=  Create Dictionary   name=P2B-04   port=P2B-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-05Def}=  Create Dictionary   name=P2B-05   port=P2B-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-06Def}=  Create Dictionary   name=P2B-06   port=P2B-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-07Def}=  Create Dictionary   name=P2B-07   port=P2B-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-08Def}=  Create Dictionary   name=P2B-08   port=P2B-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-09Def}=  Create Dictionary   name=P2B-09   port=P2B-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-10Def}=  Create Dictionary   name=P2B-10   port=P2B-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-11Def}=  Create Dictionary   name=P2B-11   port=P2B-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-12Def}=  Create Dictionary   name=P2B-12   port=P2B-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-13Def}=  Create Dictionary   name=P2B-13   port=P2B-13   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-14Def}=  Create Dictionary   name=P2B-14   port=P2B-14   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-15Def}=  Create Dictionary   name=P2B-15   port=P2B-15   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P2B-16Def}=  Create Dictionary   name=P2B-16   port=P2B-16   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P2B-01Def}
    AddPort  ${nto1}  ${P2B-02Def}
    AddPort  ${nto1}  ${P2B-03Def}
    AddPort  ${nto1}  ${P2B-04Def}
    AddPort  ${nto1}  ${P2B-05Def}
    AddPort  ${nto1}  ${P2B-06Def}
    AddPort  ${nto1}  ${P2B-07Def}
    AddPort  ${nto1}  ${P2B-08Def}
    AddPort  ${nto1}  ${P2B-09Def}
    AddPort  ${nto1}  ${P2B-10Def}
    AddPort  ${nto1}  ${P2B-11Def}
    AddPort  ${nto1}  ${P2B-12Def}
    AddPort  ${nto1}  ${P2B-13Def}
    AddPort  ${nto1}  ${P2B-14Def}
    AddPort  ${nto1}  ${P2B-15Def}
    AddPort  ${nto1}  ${P2B-16Def}

    #10Gb Card P3 port definitions.
    &{P3-01Def}=  Create Dictionary   name=P3-01   port=P3-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-02Def}=  Create Dictionary   name=P3-02   port=P3-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-03Def}=  Create Dictionary   name=P3-03   port=P3-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-04Def}=  Create Dictionary   name=P3-04   port=P3-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-05Def}=  Create Dictionary   name=P3-05   port=P3-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-06Def}=  Create Dictionary   name=P3-06   port=P3-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-07Def}=  Create Dictionary   name=P3-07   port=P3-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-08Def}=  Create Dictionary   name=P3-08   port=P3-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-09Def}=  Create Dictionary   name=P3-09   port=P3-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-10Def}=  Create Dictionary   name=P3-10   port=P3-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-11Def}=  Create Dictionary   name=P3-11   port=P3-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-12Def}=  Create Dictionary   name=P3-12   port=P3-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-13Def}=  Create Dictionary   name=P3-13   port=P3-13   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-14Def}=  Create Dictionary   name=P3-14   port=P3-14   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-15Def}=  Create Dictionary   name=P3-15   port=P3-15   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-16Def}=  Create Dictionary   name=P3-16   port=P3-16   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-17Def}=  Create Dictionary   name=P3-17   port=P3-17   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-18Def}=  Create Dictionary   name=P3-18   port=P3-18   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-19Def}=  Create Dictionary   name=P3-19   port=P3-19   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-20Def}=  Create Dictionary   name=P3-20   port=P3-20   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-21Def}=  Create Dictionary   name=P3-21   port=P3-21   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-22Def}=  Create Dictionary   name=P3-22   port=P3-22   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-23Def}=  Create Dictionary   name=P3-23   port=P3-23   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-24Def}=  Create Dictionary   name=P3-24   port=P3-24   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-25Def}=  Create Dictionary   name=P3-25   port=P3-25   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-26Def}=  Create Dictionary   name=P3-26   port=P3-26   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-27Def}=  Create Dictionary   name=P3-27   port=P3-27   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-28Def}=  Create Dictionary   name=P3-28   port=P3-28   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-29Def}=  Create Dictionary   name=P3-29   port=P3-29   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-30Def}=  Create Dictionary   name=P3-30   port=P3-30   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-31Def}=  Create Dictionary   name=P3-31   port=P3-31   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-32Def}=  Create Dictionary   name=P3-32   port=P3-32   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-33Def}=  Create Dictionary   name=P3-33   port=P3-33   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-34Def}=  Create Dictionary   name=P3-34   port=P3-34   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-35Def}=  Create Dictionary   name=P3-35   port=P3-35   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-36Def}=  Create Dictionary   name=P3-36   port=P3-36   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-37Def}=  Create Dictionary   name=P3-37   port=P3-37   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-38Def}=  Create Dictionary   name=P3-38   port=P3-38   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-39Def}=  Create Dictionary   name=P3-39   port=P3-39   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-40Def}=  Create Dictionary   name=P3-40   port=P3-40   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-41Def}=  Create Dictionary   name=P3-41   port=P3-41   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-42Def}=  Create Dictionary   name=P3-42   port=P3-42   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-43Def}=  Create Dictionary   name=P3-43   port=P3-43   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-44Def}=  Create Dictionary   name=P3-44   port=P3-44   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-45Def}=  Create Dictionary   name=P3-45   port=P3-45   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-46Def}=  Create Dictionary   name=P3-46   port=P3-46   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-47Def}=  Create Dictionary   name=P3-47   port=P3-47   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P3-48Def}=  Create Dictionary   name=P3-48   port=P3-48   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P3-01Def}
    AddPort  ${nto1}  ${P3-02Def}
    AddPort  ${nto1}  ${P3-03Def}
    AddPort  ${nto1}  ${P3-04Def}
    AddPort  ${nto1}  ${P3-05Def}
    AddPort  ${nto1}  ${P3-06Def}
    AddPort  ${nto1}  ${P3-07Def}
    AddPort  ${nto1}  ${P3-08Def}
    AddPort  ${nto1}  ${P3-09Def}
    AddPort  ${nto1}  ${P3-10Def}
    AddPort  ${nto1}  ${P3-11Def}
    AddPort  ${nto1}  ${P3-12Def}
    AddPort  ${nto1}  ${P3-13Def}
    AddPort  ${nto1}  ${P3-14Def}
    AddPort  ${nto1}  ${P3-15Def}
    AddPort  ${nto1}  ${P3-16Def}
    AddPort  ${nto1}  ${P3-17Def}
    AddPort  ${nto1}  ${P3-18Def}
    AddPort  ${nto1}  ${P3-19Def}
    AddPort  ${nto1}  ${P3-20Def}
    AddPort  ${nto1}  ${P3-21Def}
    AddPort  ${nto1}  ${P3-22Def}
    AddPort  ${nto1}  ${P3-23Def}
    AddPort  ${nto1}  ${P3-24Def}
    AddPort  ${nto1}  ${P3-25Def}
    AddPort  ${nto1}  ${P3-26Def}
    AddPort  ${nto1}  ${P3-27Def}
    AddPort  ${nto1}  ${P3-28Def}
    AddPort  ${nto1}  ${P3-29Def}
    AddPort  ${nto1}  ${P3-30Def}
    AddPort  ${nto1}  ${P3-31Def}
    AddPort  ${nto1}  ${P3-32Def}
    AddPort  ${nto1}  ${P3-33Def}
    AddPort  ${nto1}  ${P3-34Def}
    AddPort  ${nto1}  ${P3-35Def}
    AddPort  ${nto1}  ${P3-36Def}
    AddPort  ${nto1}  ${P3-37Def}
    AddPort  ${nto1}  ${P3-38Def}
    AddPort  ${nto1}  ${P3-39Def}
    AddPort  ${nto1}  ${P3-40Def}
    AddPort  ${nto1}  ${P3-41Def}
    AddPort  ${nto1}  ${P3-42Def}
    AddPort  ${nto1}  ${P3-43Def}
    AddPort  ${nto1}  ${P3-44Def}
    AddPort  ${nto1}  ${P3-45Def}
    AddPort  ${nto1}  ${P3-46Def}
    AddPort  ${nto1}  ${P3-47Def}
    AddPort  ${nto1}  ${P3-48Def}

    #10Gb Card P4 port definitions.
    &{P4-01Def}=  Create Dictionary   name=P4-01   port=P4-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-02Def}=  Create Dictionary   name=P4-02   port=P4-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-03Def}=  Create Dictionary   name=P4-03   port=P4-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-04Def}=  Create Dictionary   name=P4-04   port=P4-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-05Def}=  Create Dictionary   name=P4-05   port=P4-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-06Def}=  Create Dictionary   name=P4-06   port=P4-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-07Def}=  Create Dictionary   name=P4-07   port=P4-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-08Def}=  Create Dictionary   name=P4-08   port=P4-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-09Def}=  Create Dictionary   name=P4-09   port=P4-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-10Def}=  Create Dictionary   name=P4-10   port=P4-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-11Def}=  Create Dictionary   name=P4-11   port=P4-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-12Def}=  Create Dictionary   name=P4-12   port=P4-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-13Def}=  Create Dictionary   name=P4-13   port=P4-13   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-14Def}=  Create Dictionary   name=P4-14   port=P4-14   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-15Def}=  Create Dictionary   name=P4-15   port=P4-15   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-16Def}=  Create Dictionary   name=P4-16   port=P4-16   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-17Def}=  Create Dictionary   name=P4-17   port=P4-17   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-18Def}=  Create Dictionary   name=P4-18   port=P4-18   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-19Def}=  Create Dictionary   name=P4-19   port=P4-19   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-20Def}=  Create Dictionary   name=P4-20   port=P4-20   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-21Def}=  Create Dictionary   name=P4-21   port=P4-21   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-22Def}=  Create Dictionary   name=P4-22   port=P4-22   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-23Def}=  Create Dictionary   name=P4-23   port=P4-23   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-24Def}=  Create Dictionary   name=P4-24   port=P4-24   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-25Def}=  Create Dictionary   name=P4-25   port=P4-25   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-26Def}=  Create Dictionary   name=P4-26   port=P4-26   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-27Def}=  Create Dictionary   name=P4-27   port=P4-27   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-28Def}=  Create Dictionary   name=P4-28   port=P4-28   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-29Def}=  Create Dictionary   name=P4-29   port=P4-29   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-30Def}=  Create Dictionary   name=P4-30   port=P4-30   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-31Def}=  Create Dictionary   name=P4-31   port=P4-31   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-32Def}=  Create Dictionary   name=P4-32   port=P4-32   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-33Def}=  Create Dictionary   name=P4-33   port=P4-33   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-34Def}=  Create Dictionary   name=P4-34   port=P4-34   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-35Def}=  Create Dictionary   name=P4-35   port=P4-35   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-36Def}=  Create Dictionary   name=P4-36   port=P4-36   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-37Def}=  Create Dictionary   name=P4-37   port=P4-37   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-38Def}=  Create Dictionary   name=P4-38   port=P4-38   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-39Def}=  Create Dictionary   name=P4-39   port=P4-39   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-40Def}=  Create Dictionary   name=P4-40   port=P4-40   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-41Def}=  Create Dictionary   name=P4-41   port=P4-41   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-42Def}=  Create Dictionary   name=P4-42   port=P4-42   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-43Def}=  Create Dictionary   name=P4-43   port=P4-43   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-44Def}=  Create Dictionary   name=P4-44   port=P4-44   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-45Def}=  Create Dictionary   name=P4-45   port=P4-45   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-46Def}=  Create Dictionary   name=P4-46   port=P4-46   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-47Def}=  Create Dictionary   name=P4-47   port=P4-47   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P4-48Def}=  Create Dictionary   name=P4-48   port=P4-48   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P4-01Def}
    AddPort  ${nto1}  ${P4-02Def}
    AddPort  ${nto1}  ${P4-03Def}
    AddPort  ${nto1}  ${P4-04Def}
    AddPort  ${nto1}  ${P4-05Def}
    AddPort  ${nto1}  ${P4-06Def}
    AddPort  ${nto1}  ${P4-07Def}
    AddPort  ${nto1}  ${P4-08Def}
    AddPort  ${nto1}  ${P4-09Def}
    AddPort  ${nto1}  ${P4-10Def}
    AddPort  ${nto1}  ${P4-11Def}
    AddPort  ${nto1}  ${P4-12Def}
    AddPort  ${nto1}  ${P4-13Def}
    AddPort  ${nto1}  ${P4-14Def}
    AddPort  ${nto1}  ${P4-15Def}
    AddPort  ${nto1}  ${P4-16Def}
    AddPort  ${nto1}  ${P4-17Def}
    AddPort  ${nto1}  ${P4-18Def}
    AddPort  ${nto1}  ${P4-19Def}
    AddPort  ${nto1}  ${P4-20Def}
    AddPort  ${nto1}  ${P4-21Def}
    AddPort  ${nto1}  ${P4-22Def}
    AddPort  ${nto1}  ${P4-23Def}
    AddPort  ${nto1}  ${P4-24Def}
    AddPort  ${nto1}  ${P4-25Def}
    AddPort  ${nto1}  ${P4-26Def}
    AddPort  ${nto1}  ${P4-27Def}
    AddPort  ${nto1}  ${P4-28Def}
    AddPort  ${nto1}  ${P4-29Def}
    AddPort  ${nto1}  ${P4-30Def}
    AddPort  ${nto1}  ${P4-31Def}
    AddPort  ${nto1}  ${P4-32Def}
    AddPort  ${nto1}  ${P4-33Def}
    AddPort  ${nto1}  ${P4-34Def}
    AddPort  ${nto1}  ${P4-35Def}
    AddPort  ${nto1}  ${P4-36Def}
    AddPort  ${nto1}  ${P4-37Def}
    AddPort  ${nto1}  ${P4-38Def}
    AddPort  ${nto1}  ${P4-39Def}
    AddPort  ${nto1}  ${P4-40Def}
    AddPort  ${nto1}  ${P4-41Def}
    AddPort  ${nto1}  ${P4-42Def}
    AddPort  ${nto1}  ${P4-43Def}
    AddPort  ${nto1}  ${P4-44Def}
    AddPort  ${nto1}  ${P4-45Def}
    AddPort  ${nto1}  ${P4-46Def}
    AddPort  ${nto1}  ${P4-47Def}
    AddPort  ${nto1}  ${P4-48Def}

    #10Gb Card P5 port definitions.
    &{P5-09Def}=  Create Dictionary   name=P5-09   port=P5-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-10Def}=  Create Dictionary   name=P5-10   port=P5-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-11Def}=  Create Dictionary   name=P5-11   port=P5-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-12Def}=  Create Dictionary   name=P5-12   port=P5-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-13Def}=  Create Dictionary   name=P5-13   port=P5-13   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-14Def}=  Create Dictionary   name=P5-14   port=P5-14   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-15Def}=  Create Dictionary   name=P5-15   port=P5-15   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-16Def}=  Create Dictionary   name=P5-16   port=P5-16   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-17Def}=  Create Dictionary   name=P5-17   port=P5-17   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-18Def}=  Create Dictionary   name=P5-18   port=P5-18   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-19Def}=  Create Dictionary   name=P5-19   port=P5-19   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-20Def}=  Create Dictionary   name=P5-20   port=P5-20   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-21Def}=  Create Dictionary   name=P5-21   port=P5-21   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-22Def}=  Create Dictionary   name=P5-22   port=P5-22   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-23Def}=  Create Dictionary   name=P5-23   port=P5-23   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P5-24Def}=  Create Dictionary   name=P5-24   port=P5-24   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P5-09Def}
    AddPort  ${nto1}  ${P5-10Def}
    AddPort  ${nto1}  ${P5-11Def}
    AddPort  ${nto1}  ${P5-12Def}
    AddPort  ${nto1}  ${P5-13Def}
    AddPort  ${nto1}  ${P5-14Def}
    AddPort  ${nto1}  ${P5-15Def}
    AddPort  ${nto1}  ${P5-16Def}
    AddPort  ${nto1}  ${P5-17Def}
    AddPort  ${nto1}  ${P5-18Def}
    AddPort  ${nto1}  ${P5-19Def}
    AddPort  ${nto1}  ${P5-20Def}
    AddPort  ${nto1}  ${P5-21Def}
    AddPort  ${nto1}  ${P5-22Def}
    AddPort  ${nto1}  ${P5-23Def}
    AddPort  ${nto1}  ${P5-24Def}

    #10Gb Card P6 port definitions.
    &{P6-01Def}=  Create Dictionary   name=P6-01   port=P6-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-02Def}=  Create Dictionary   name=P6-02   port=P6-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-03Def}=  Create Dictionary   name=P6-03   port=P6-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-04Def}=  Create Dictionary   name=P6-04   port=P6-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-05Def}=  Create Dictionary   name=P6-05   port=P6-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-06Def}=  Create Dictionary   name=P6-06   port=P6-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-07Def}=  Create Dictionary   name=P6-07   port=P6-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-08Def}=  Create Dictionary   name=P6-08   port=P6-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-09Def}=  Create Dictionary   name=P6-09   port=P6-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-10Def}=  Create Dictionary   name=P6-10   port=P6-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-11Def}=  Create Dictionary   name=P6-11   port=P6-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-12Def}=  Create Dictionary   name=P6-12   port=P6-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-13Def}=  Create Dictionary   name=P6-13   port=P6-13   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-14Def}=  Create Dictionary   name=P6-14   port=P6-14   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-15Def}=  Create Dictionary   name=P6-15   port=P6-15   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-16Def}=  Create Dictionary   name=P6-16   port=P6-16   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-17Def}=  Create Dictionary   name=P6-17   port=P6-17   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-18Def}=  Create Dictionary   name=P6-18   port=P6-18   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-19Def}=  Create Dictionary   name=P6-19   port=P6-19   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-20Def}=  Create Dictionary   name=P6-20   port=P6-20   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-21Def}=  Create Dictionary   name=P6-21   port=P6-21   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-22Def}=  Create Dictionary   name=P6-22   port=P6-22   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-23Def}=  Create Dictionary   name=P6-23   port=P6-23   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-24Def}=  Create Dictionary   name=P6-24   port=P6-24   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-25Def}=  Create Dictionary   name=P6-25   port=P6-25   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-26Def}=  Create Dictionary   name=P6-26   port=P6-26   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-27Def}=  Create Dictionary   name=P6-27   port=P6-27   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-28Def}=  Create Dictionary   name=P6-28   port=P6-28   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-29Def}=  Create Dictionary   name=P6-29   port=P6-29   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-30Def}=  Create Dictionary   name=P6-30   port=P6-30   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-31Def}=  Create Dictionary   name=P6-31   port=P6-31   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-32Def}=  Create Dictionary   name=P6-32   port=P6-32   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-33Def}=  Create Dictionary   name=P6-33   port=P6-33   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-34Def}=  Create Dictionary   name=P6-34   port=P6-34   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-35Def}=  Create Dictionary   name=P6-35   port=P6-35   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-36Def}=  Create Dictionary   name=P6-36   port=P6-36   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-37Def}=  Create Dictionary   name=P6-37   port=P6-37   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-38Def}=  Create Dictionary   name=P6-38   port=P6-38   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-39Def}=  Create Dictionary   name=P6-39   port=P6-39   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-40Def}=  Create Dictionary   name=P6-40   port=P6-40   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-41Def}=  Create Dictionary   name=P6-41   port=P6-41   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-42Def}=  Create Dictionary   name=P6-42   port=P6-42   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-43Def}=  Create Dictionary   name=P6-43   port=P6-43   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-44Def}=  Create Dictionary   name=P6-44   port=P6-44   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-45Def}=  Create Dictionary   name=P6-45   port=P6-45   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-46Def}=  Create Dictionary   name=P6-46   port=P6-46   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-47Def}=  Create Dictionary   name=P6-47   port=P6-47   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G
    &{P6-48Def}=  Create Dictionary   name=P6-48   port=P6-48   portMode=BIDIRECTIONAL  connType=standard  mediaType=SFP_PLUS_10G

    AddPort  ${nto1}  ${P6-01Def}
    AddPort  ${nto1}  ${P6-02Def}
    AddPort  ${nto1}  ${P6-03Def}
    AddPort  ${nto1}  ${P6-04Def}
    AddPort  ${nto1}  ${P6-05Def}
    AddPort  ${nto1}  ${P6-06Def}
    AddPort  ${nto1}  ${P6-07Def}
    AddPort  ${nto1}  ${P6-08Def}
    AddPort  ${nto1}  ${P6-09Def}
    AddPort  ${nto1}  ${P6-10Def}
    AddPort  ${nto1}  ${P6-11Def}
    AddPort  ${nto1}  ${P6-12Def}
    AddPort  ${nto1}  ${P6-13Def}
    AddPort  ${nto1}  ${P6-14Def}
    AddPort  ${nto1}  ${P6-15Def}
    AddPort  ${nto1}  ${P6-16Def}
    AddPort  ${nto1}  ${P6-17Def}
    AddPort  ${nto1}  ${P6-18Def}
    AddPort  ${nto1}  ${P6-19Def}
    AddPort  ${nto1}  ${P6-20Def}
    AddPort  ${nto1}  ${P6-21Def}
    AddPort  ${nto1}  ${P6-22Def}
    AddPort  ${nto1}  ${P6-23Def}
    AddPort  ${nto1}  ${P6-24Def}
    AddPort  ${nto1}  ${P6-25Def}
    AddPort  ${nto1}  ${P6-26Def}
    AddPort  ${nto1}  ${P6-27Def}
    AddPort  ${nto1}  ${P6-28Def}
    AddPort  ${nto1}  ${P6-29Def}
    AddPort  ${nto1}  ${P6-30Def}
    AddPort  ${nto1}  ${P6-31Def}
    AddPort  ${nto1}  ${P6-32Def}
    AddPort  ${nto1}  ${P6-33Def}
    AddPort  ${nto1}  ${P6-34Def}
    AddPort  ${nto1}  ${P6-35Def}
    AddPort  ${nto1}  ${P6-36Def}
    AddPort  ${nto1}  ${P6-37Def}
    AddPort  ${nto1}  ${P6-38Def}
    AddPort  ${nto1}  ${P6-39Def}
    AddPort  ${nto1}  ${P6-40Def}
    AddPort  ${nto1}  ${P6-41Def}
    AddPort  ${nto1}  ${P6-42Def}
    AddPort  ${nto1}  ${P6-43Def}
    AddPort  ${nto1}  ${P6-44Def}
    AddPort  ${nto1}  ${P6-45Def}
    AddPort  ${nto1}  ${P6-46Def}
    AddPort  ${nto1}  ${P6-47Def}
    AddPort  ${nto1}  ${P6-48Def}


    #40Gb Card P1 port definitions.
    &{P1-01Def}=  Create Dictionary   name=P1-01   port=P1-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-02Def}=  Create Dictionary   name=P1-02   port=P1-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-03Def}=  Create Dictionary   name=P1-03   port=P1-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-04Def}=  Create Dictionary   name=P1-04   port=P1-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-05Def}=  Create Dictionary   name=P1-05   port=P1-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-06Def}=  Create Dictionary   name=P1-06   port=P1-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-07Def}=  Create Dictionary   name=P1-07   port=P1-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-08Def}=  Create Dictionary   name=P1-08   port=P1-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-09Def}=  Create Dictionary   name=P1-09   port=P1-09   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-10Def}=  Create Dictionary   name=P1-10   port=P1-10   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-11Def}=  Create Dictionary   name=P1-11   port=P1-11   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-12Def}=  Create Dictionary   name=P1-12   port=P1-12   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-13Def}=  Create Dictionary   name=P1-13   port=P1-13   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-14Def}=  Create Dictionary   name=P1-14   port=P1-14   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-15Def}=  Create Dictionary   name=P1-15   port=P1-15   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P1-16Def}=  Create Dictionary   name=P1-16   port=P1-16   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G

    AddPort  ${nto1}  ${P1-01Def}
    AddPort  ${nto1}  ${P1-02Def}
    AddPort  ${nto1}  ${P1-03Def}
    AddPort  ${nto1}  ${P1-04Def}
    AddPort  ${nto1}  ${P1-05Def}
    AddPort  ${nto1}  ${P1-06Def}
    AddPort  ${nto1}  ${P1-07Def}
    AddPort  ${nto1}  ${P1-08Def}
    AddPort  ${nto1}  ${P1-09Def}
    AddPort  ${nto1}  ${P1-10Def}
    AddPort  ${nto1}  ${P1-11Def}
    AddPort  ${nto1}  ${P1-12Def}
    AddPort  ${nto1}  ${P1-13Def}
    AddPort  ${nto1}  ${P1-14Def}
    AddPort  ${nto1}  ${P1-15Def}
    AddPort  ${nto1}  ${P1-16Def}

    &{P1-01IxiaDef}=  Create Dictionary  ixIPaddr=10.218.164.237  ixPort=1/7/18  phyMode=Fiber
    AddIxiaPort  ${P1-01}  ${P1-01IxiaDef}

    &{P2A-01IxiaDef}=  Create Dictionary  ixIPaddr=10.218.164.237  ixPort=1/2/13  phyMode=Fiber
    AddIxiaPort  ${P2A-01}  ${P2A-01IxiaDef}

    #40Gb Card P2 port definitions.
    &{P2-01Def}=  Create Dictionary   name=P2-01   port=P2-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2-02Def}=  Create Dictionary   name=P2-02   port=P2-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2-03Def}=  Create Dictionary   name=P2-03   port=P2-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P2-04Def}=  Create Dictionary   name=P2-04   port=P2-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G

    AddPort  ${nto1}  ${P2-01Def}
    AddPort  ${nto1}  ${P2-02Def}
    AddPort  ${nto1}  ${P2-03Def}
    AddPort  ${nto1}  ${P2-04Def}


    #40Gb Card P5 port definitions.
    &{P5-01Def}=  Create Dictionary   name=P5-01   port=P5-01   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P5-02Def}=  Create Dictionary   name=P5-02   port=P5-02   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P5-03Def}=  Create Dictionary   name=P5-03   port=P5-03   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P5-04Def}=  Create Dictionary   name=P5-04   port=P5-04   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P5-05Def}=  Create Dictionary   name=P5-05   port=P5-05   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P5-06Def}=  Create Dictionary   name=P5-06   port=P5-06   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P5-07Def}=  Create Dictionary   name=P5-07   port=P5-07   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G
    &{P5-08Def}=  Create Dictionary   name=P5-08   port=P5-08   portMode=BIDIRECTIONAL  connType=standard  mediaType=QSFP_PLUS_40G

    AddPort  ${nto1}  ${P5-01Def}
    AddPort  ${nto1}  ${P5-02Def}
    AddPort  ${nto1}  ${P5-03Def}
    AddPort  ${nto1}  ${P5-04Def}
    AddPort  ${nto1}  ${P5-05Def}
    AddPort  ${nto1}  ${P5-06Def}
    AddPort  ${nto1}  ${P5-07Def}
    AddPort  ${nto1}  ${P5-08Def}

    AddFilter  ${nto1}  f1    PASS_ALL
    AddFilter  ${nto1}  f2    PASS_ALL
    AddFilter  ${nto1}  f3    PASS_ALL
    AddFilter  ${nto1}  f4    PASS_ALL
    AddFilter  ${nto1}  f5    PASS_ALL
    AddFilter  ${nto1}  f6    PASS_ALL
    AddFilter  ${nto1}  f7    PASS_ALL
    AddFilter  ${nto1}  f8    PASS_ALL
    AddFilter  ${nto1}  f9    PASS_ALL
    AddFilter  ${nto1}  f10   PASS_ALL
    AddFilter  ${nto1}  f11   PASS_ALL
    AddFilter  ${nto1}  f12   PASS_ALL
    AddFilter  ${nto1}  f13   PASS_ALL
    AddFilter  ${nto1}  f14   PASS_ALL
    AddFilter  ${nto1}  f15   PASS_ALL
    AddFilter  ${nto1}  f16   PASS_ALL
    AddFilter  ${nto1}  f17   PASS_ALL
    AddFilter  ${nto1}  f18   PASS_ALL
    AddFilter  ${nto1}  f19   PASS_ALL
    AddFilter  ${nto1}  f20   PASS_ALL
    AddFilter  ${nto1}  f21   PASS_ALL
    AddFilter  ${nto1}  f22   PASS_ALL
    AddFilter  ${nto1}  f23   PASS_ALL
    AddFilter  ${nto1}  f24   PASS_ALL
    AddFilter  ${nto1}  f25   PASS_ALL
    AddFilter  ${nto1}  f26   PASS_ALL
    AddFilter  ${nto1}  f27   PASS_ALL
    AddFilter  ${nto1}  f28   PASS_ALL
    AddFilter  ${nto1}  f29   PASS_ALL
    AddFilter  ${nto1}  f30   PASS_ALL
    AddFilter  ${nto1}  f31   PASS_ALL
    AddFilter  ${nto1}  f32   PASS_ALL
    AddFilter  ${nto1}  f33   PASS_ALL
    AddFilter  ${nto1}  f34   PASS_ALL
    AddFilter  ${nto1}  f35   PASS_ALL
    AddFilter  ${nto1}  f36   PASS_ALL
    AddFilter  ${nto1}  f37   PASS_ALL
    AddFilter  ${nto1}  f38   PASS_ALL
    AddFilter  ${nto1}  f39   PASS_ALL
    AddFilter  ${nto1}  f40   PASS_ALL
    AddFilter  ${nto1}  f41   PASS_ALL
    AddFilter  ${nto1}  f42   PASS_ALL
    AddFilter  ${nto1}  f43   PASS_ALL
    AddFilter  ${nto1}  f44   PASS_ALL
    AddFilter  ${nto1}  f45   PASS_ALL
    AddFilter  ${nto1}  f46   PASS_ALL
    AddFilter  ${nto1}  f47   PASS_ALL
    AddFilter  ${nto1}  f48   PASS_ALL
    AddFilter  ${nto1}  f49   PASS_ALL
    AddFilter  ${nto1}  f50   PASS_ALL
    AddFilter  ${nto1}  f51   PASS_ALL
    AddFilter  ${nto1}  f52   PASS_ALL
    AddFilter  ${nto1}  f53   PASS_ALL
    AddFilter  ${nto1}  f54   PASS_ALL
    AddFilter  ${nto1}  f55   PASS_ALL
    AddFilter  ${nto1}  f56   PASS_ALL
    AddFilter  ${nto1}  f57   PASS_ALL
    AddFilter  ${nto1}  f58   PASS_ALL
    AddFilter  ${nto1}  f59   PASS_ALL
    AddFilter  ${nto1}  f60   PASS_ALL
    AddFilter  ${nto1}  f61   PASS_ALL
    AddFilter  ${nto1}  f62   PASS_ALL
    AddFilter  ${nto1}  f63   PASS_ALL
    AddFilter  ${nto1}  f64   PASS_ALL
    AddFilter  ${nto1}  f65   PASS_ALL
    AddFilter  ${nto1}  f66   PASS_ALL
    AddFilter  ${nto1}  f67   PASS_ALL
    AddFilter  ${nto1}  f68   PASS_ALL
    AddFilter  ${nto1}  f69   PASS_ALL
    AddFilter  ${nto1}  f70   PASS_ALL
    AddFilter  ${nto1}  f71   PASS_ALL
    AddFilter  ${nto1}  f72   PASS_ALL
    AddFilter  ${nto1}  f73   PASS_ALL
    AddFilter  ${nto1}  f74   PASS_ALL
    AddFilter  ${nto1}  f75   PASS_ALL
    AddFilter  ${nto1}  f76   PASS_ALL
    AddFilter  ${nto1}  f77   PASS_ALL
    AddFilter  ${nto1}  f78   PASS_ALL
    AddFilter  ${nto1}  f79   PASS_ALL
    AddFilter  ${nto1}  f80   PASS_ALL
    AddFilter  ${nto1}  f81   PASS_ALL
    AddFilter  ${nto1}  f82   PASS_ALL
    AddFilter  ${nto1}  f83   PASS_ALL
    AddFilter  ${nto1}  f84   PASS_ALL
    AddFilter  ${nto1}  f85   PASS_ALL
    AddFilter  ${nto1}  f86   PASS_ALL
    AddFilter  ${nto1}  f87   PASS_ALL
    AddFilter  ${nto1}  f88   PASS_ALL
    AddFilter  ${nto1}  f89   PASS_ALL
    AddFilter  ${nto1}  f90   PASS_ALL
    AddFilter  ${nto1}  f91   PASS_ALL
    AddFilter  ${nto1}  f92   PASS_ALL
    AddFilter  ${nto1}  f93   PASS_ALL
    AddFilter  ${nto1}  f94   PASS_ALL
    AddFilter  ${nto1}  f95   PASS_ALL
    AddFilter  ${nto1}  f96   PASS_ALL
    AddFilter  ${nto1}  f97   PASS_ALL
    AddFilter  ${nto1}  f98   PASS_ALL
    AddFilter  ${nto1}  f99   PASS_ALL
    AddFilter  ${nto1}  f100  PASS_ALL
    AddFilter  ${nto1}  f101  PASS_ALL
    AddFilter  ${nto1}  f102  PASS_ALL
    AddFilter  ${nto1}  f103  PASS_ALL
    AddFilter  ${nto1}  f104  PASS_ALL
    AddFilter  ${nto1}  f105  PASS_ALL
    AddFilter  ${nto1}  f106  PASS_ALL
    AddFilter  ${nto1}  f107  PASS_ALL
    AddFilter  ${nto1}  f108  PASS_ALL
    AddFilter  ${nto1}  f109  PASS_ALL
    AddFilter  ${nto1}  f110  PASS_ALL
    AddFilter  ${nto1}  f111  PASS_ALL
    AddFilter  ${nto1}  f112  PASS_ALL
    AddFilter  ${nto1}  f113  PASS_ALL
    AddFilter  ${nto1}  f114  PASS_ALL
    AddFilter  ${nto1}  f115  PASS_ALL
    AddFilter  ${nto1}  f116  PASS_ALL
    AddFilter  ${nto1}  f117  PASS_ALL
    AddFilter  ${nto1}  f118  PASS_ALL
    AddFilter  ${nto1}  f119  PASS_ALL
    AddFilter  ${nto1}  f120  PASS_ALL
    AddFilter  ${nto1}  f121  PASS_ALL
    AddFilter  ${nto1}  f122  PASS_ALL
    AddFilter  ${nto1}  f123  PASS_ALL
    AddFilter  ${nto1}  f124  PASS_ALL
    AddFilter  ${nto1}  f125  PASS_ALL
    AddFilter  ${nto1}  f126  PASS_ALL
    AddFilter  ${nto1}  f127  PASS_ALL
    AddFilter  ${nto1}  f128  PASS_ALL
    AddFilter  ${nto1}  f129  PASS_ALL
    AddFilter  ${nto1}  f130  PASS_ALL
    AddFilter  ${nto1}  f131  PASS_ALL
    AddFilter  ${nto1}  f132  PASS_ALL
    AddFilter  ${nto1}  f133  PASS_ALL
    AddFilter  ${nto1}  f134  PASS_ALL
    AddFilter  ${nto1}  f135  PASS_ALL
    AddFilter  ${nto1}  f136  PASS_ALL
    AddFilter  ${nto1}  f137  PASS_ALL
    AddFilter  ${nto1}  f138  PASS_ALL
    AddFilter  ${nto1}  f139  PASS_ALL
    AddFilter  ${nto1}  f140  PASS_ALL
    AddFilter  ${nto1}  f141  PASS_ALL
    AddFilter  ${nto1}  f142  PASS_ALL
    AddFilter  ${nto1}  f143  PASS_ALL
    AddFilter  ${nto1}  f144  PASS_ALL
    AddFilter  ${nto1}  f145  PASS_ALL
    AddFilter  ${nto1}  f146  PASS_ALL
    AddFilter  ${nto1}  f147  PASS_ALL
    AddFilter  ${nto1}  f148  PASS_ALL
    AddFilter  ${nto1}  f149  PASS_ALL
    AddFilter  ${nto1}  f150  PASS_ALL
    AddFilter  ${nto1}  f151  PASS_ALL
    AddFilter  ${nto1}  f152  PASS_ALL
    AddFilter  ${nto1}  f153  PASS_ALL
    AddFilter  ${nto1}  f154  PASS_ALL
    AddFilter  ${nto1}  f155  PASS_ALL
    AddFilter  ${nto1}  f156  PASS_ALL
    AddFilter  ${nto1}  f157  PASS_ALL
    AddFilter  ${nto1}  f158  PASS_ALL
    AddFilter  ${nto1}  f159  PASS_ALL
    AddFilter  ${nto1}  f160  PASS_ALL
    AddFilter  ${nto1}  f161  PASS_ALL
    AddFilter  ${nto1}  f162  PASS_ALL
    AddFilter  ${nto1}  f163  PASS_ALL
    AddFilter  ${nto1}  f164  PASS_ALL
    AddFilter  ${nto1}  f165  PASS_ALL
    AddFilter  ${nto1}  f166  PASS_ALL
    AddFilter  ${nto1}  f167  PASS_ALL
    AddFilter  ${nto1}  f168  PASS_ALL
    AddFilter  ${nto1}  f169  PASS_ALL
    AddFilter  ${nto1}  f170  PASS_ALL
    AddFilter  ${nto1}  f171  PASS_ALL
    AddFilter  ${nto1}  f172  PASS_ALL
    AddFilter  ${nto1}  f173  PASS_ALL
    AddFilter  ${nto1}  f174  PASS_ALL
    AddFilter  ${nto1}  f175  PASS_ALL
    AddFilter  ${nto1}  f176  PASS_ALL
    AddFilter  ${nto1}  f177  PASS_ALL
    AddFilter  ${nto1}  f178  PASS_ALL
    AddFilter  ${nto1}  f179  PASS_ALL
    AddFilter  ${nto1}  f180  PASS_ALL
    AddFilter  ${nto1}  f181  PASS_ALL
    AddFilter  ${nto1}  f182  PASS_ALL
    AddFilter  ${nto1}  f183  PASS_ALL
    AddFilter  ${nto1}  f184  PASS_ALL
    AddFilter  ${nto1}  f185  PASS_ALL
    AddFilter  ${nto1}  f186  PASS_ALL
    AddFilter  ${nto1}  f187  PASS_ALL
    AddFilter  ${nto1}  f188  PASS_ALL
    AddFilter  ${nto1}  f189  PASS_ALL
    AddFilter  ${nto1}  f190  PASS_ALL
    AddFilter  ${nto1}  f191  PASS_ALL
    AddFilter  ${nto1}  f192  PASS_ALL
    AddFilter  ${nto1}  f193  PASS_ALL
    AddFilter  ${nto1}  f194  PASS_ALL
    AddFilter  ${nto1}  f195  PASS_ALL
    AddFilter  ${nto1}  f196  PASS_ALL
    AddFilter  ${nto1}  f197  PASS_ALL
    AddFilter  ${nto1}  f198  PASS_ALL
    AddFilter  ${nto1}  f199  PASS_ALL
    AddFilter  ${nto1}  f200  PASS_ALL
    AddFilter  ${nto1}  f201  PASS_ALL
    AddFilter  ${nto1}  f202  PASS_ALL
    AddFilter  ${nto1}  f203  PASS_ALL
    AddFilter  ${nto1}  f204  PASS_ALL
    AddFilter  ${nto1}  f205  PASS_ALL
    AddFilter  ${nto1}  f206  PASS_ALL
    AddFilter  ${nto1}  f207  PASS_ALL
    AddFilter  ${nto1}  f208  PASS_ALL
    AddFilter  ${nto1}  f209  PASS_ALL
    AddFilter  ${nto1}  f210  PASS_ALL
    AddFilter  ${nto1}  f211  PASS_ALL
    AddFilter  ${nto1}  f212  PASS_ALL
    AddFilter  ${nto1}  f213  PASS_ALL
    AddFilter  ${nto1}  f214  PASS_ALL
    AddFilter  ${nto1}  f215  PASS_ALL
    AddFilter  ${nto1}  f216  PASS_ALL
    AddFilter  ${nto1}  f217  PASS_ALL
    AddFilter  ${nto1}  f218  PASS_ALL

    AddConn    ${nto1}  c1    ${P1-01}   ${f1}    ${P1-03}
    AddConn    ${nto1}  c2    ${P1-03}   ${f2}    ${P1-04}
    AddConn    ${nto1}  c3    ${P1-04}   ${f3}    ${P1-05}
    AddConn    ${nto1}  c4    ${P1-05}   ${f4}    ${P1-06}
    AddConn    ${nto1}  c5    ${P1-06}   ${f5}    ${P1-07}
    AddConn    ${nto1}  c6    ${P1-07}   ${f6}    ${P1-08}
    AddConn    ${nto1}  c7    ${P1-08}   ${f7}    ${P1-09}
    AddConn    ${nto1}  c8    ${P1-09}   ${f8}    ${P1-10}
    AddConn    ${nto1}  c9    ${P1-10}   ${f9}    ${P1-11}
    AddConn    ${nto1}  c10   ${P1-11}   ${f10}   ${P1-12}
    AddConn    ${nto1}  c11   ${P1-12}   ${f11}   ${P1-13}
    AddConn    ${nto1}  c12   ${P1-13}   ${f12}   ${P1-14}
    AddConn    ${nto1}  c13   ${P1-14}   ${f13}   ${P1-15}
    AddConn    ${nto1}  c14   ${P1-15}   ${f14}   ${P1-16}
    AddConn    ${nto1}  c15   ${P1-16}   ${f15}   ${P2-01}
    AddConn    ${nto1}  c16   ${P2-01}   ${f16}   ${P2-02}
    AddConn    ${nto1}  c17   ${P2-02}   ${f17}   ${P2-03}
    AddConn    ${nto1}  c18   ${P2-03}   ${f18}   ${P2-04}
    AddConn    ${nto1}  c19   ${P2-04}   ${f19}   ${P5-01}
    AddConn    ${nto1}  c20   ${P5-01}   ${f20}   ${P5-02}
    AddConn    ${nto1}  c21   ${P5-02}   ${f21}   ${P5-03}
    AddConn    ${nto1}  c22   ${P5-03}   ${f22}   ${P5-04}
    AddConn    ${nto1}  c23   ${P5-04}   ${f23}   ${P5-05}
    AddConn    ${nto1}  c24   ${P5-05}   ${f24}   ${P5-06}
    AddConn    ${nto1}  c25   ${P5-06}   ${f25}   ${P5-07}
    AddConn    ${nto1}  c26   ${P5-07}   ${f26}   ${P5-08}
    AddConn    ${nto1}  c27   ${P5-08}   ${f27}   ${P1-02}

    AddConn    ${nto1}  c28   ${P2A-01}  ${f28}   ${P3-01}
    AddConn    ${nto1}  c29   ${P3-01}   ${f29}   ${P3-02}
    AddConn    ${nto1}  c30   ${P2A-04}  ${f30}   ${P3-03}
    AddConn    ${nto1}  c31   ${P3-03}   ${f31}   ${P3-04}
    AddConn    ${nto1}  c32   ${P2A-06}  ${f32}   ${P3-05}
    AddConn    ${nto1}  c33   ${P3-05}   ${f33}   ${P3-06}
    AddConn    ${nto1}  c34   ${P2A-08}  ${f34}   ${P3-07}
    AddConn    ${nto1}  c35   ${P3-07}   ${f35}   ${P3-08}
    AddConn    ${nto1}  c36   ${P2A-10}  ${f36}   ${P3-09}
    AddConn    ${nto1}  c37   ${P3-09}   ${f37}   ${P3-10}
    AddConn    ${nto1}  c38   ${P2A-12}  ${f38}   ${P3-11}
    AddConn    ${nto1}  c39   ${P3-11}   ${f39}   ${P3-12}
    AddConn    ${nto1}  c40   ${P2A-14}  ${f40}   ${P3-13}
    AddConn    ${nto1}  c41   ${P3-13}   ${f41}   ${P3-14}
    AddConn    ${nto1}  c42   ${P2A-16}  ${f42}   ${P3-15}
    AddConn    ${nto1}  c43   ${P3-15}   ${f43}   ${P3-16}

    AddConn    ${nto1}  c44   ${P3-02}   ${f44}   ${P2A-03}
    AddConn    ${nto1}  c45   ${P2A-03}  ${f45}   ${P2A-04}
    AddConn    ${nto1}  c46   ${P3-04}   ${f46}   ${P2A-05}
    AddConn    ${nto1}  c47   ${P2A-05}  ${f47}   ${P2A-06}
    AddConn    ${nto1}  c48   ${P3-06}   ${f48}   ${P2A-07}
    AddConn    ${nto1}  c49   ${P2A-07}  ${f49}   ${P2A-08}
    AddConn    ${nto1}  c50   ${P3-08}   ${f50}   ${P2A-09}
    AddConn    ${nto1}  c51   ${P2A-09}  ${f51}   ${P2A-10}
    AddConn    ${nto1}  c52   ${P3-10}   ${f52}   ${P2A-11}
    AddConn    ${nto1}  c53   ${P2A-11}  ${f53}   ${P2A-12}
    AddConn    ${nto1}  c54   ${P3-12}   ${f54}   ${P2A-13}
    AddConn    ${nto1}  c55   ${P2A-13}  ${f55}   ${P2A-14}
    AddConn    ${nto1}  c56   ${P3-14}   ${f56}   ${P2A-15}
    AddConn    ${nto1}  c57   ${P2A-15}  ${f57}   ${P2A-16}
    AddConn    ${nto1}  c58   ${P3-16}   ${f58}   ${P2B-01}
    AddConn    ${nto1}  c59   ${P2B-01}  ${f59}   ${P2B-02}

    AddConn    ${nto1}  c60   ${P2B-02}  ${f60}   ${P3-17}
    AddConn    ${nto1}  c61   ${P3-17}   ${f61}   ${P3-18}
    AddConn    ${nto1}  c62   ${P2B-04}  ${f62}   ${P3-19}
    AddConn    ${nto1}  c63   ${P3-19}   ${f63}   ${P3-20}
    AddConn    ${nto1}  c64   ${P2B-06}  ${f64}   ${P3-21}
    AddConn    ${nto1}  c65   ${P3-21}   ${f65}   ${P3-22}
    AddConn    ${nto1}  c66   ${P2B-08}  ${f66}   ${P3-23}
    AddConn    ${nto1}  c67   ${P3-23}   ${f67}   ${P3-24}
    AddConn    ${nto1}  c68   ${P2B-10}  ${f68}   ${P3-25}
    AddConn    ${nto1}  c69   ${P3-25}   ${f69}   ${P3-26}
    AddConn    ${nto1}  c70   ${P2B-12}  ${f70}   ${P3-27}
    AddConn    ${nto1}  c71   ${P3-27}   ${f71}   ${P3-28}
    AddConn    ${nto1}  c72   ${P2B-14}  ${f72}   ${P3-29}
    AddConn    ${nto1}  c73   ${P3-29}   ${f73}   ${P3-30}
    AddConn    ${nto1}  c74   ${P2B-16}  ${f74}   ${P3-31}
    AddConn    ${nto1}  c75   ${P3-31}   ${f75}   ${P3-32}

    AddConn    ${nto1}  c76   ${P3-18}   ${f76}   ${P2B-03}
    AddConn    ${nto1}  c77   ${P2B-03}  ${f77}   ${P2B-04}
    AddConn    ${nto1}  c78   ${P3-20}   ${f78}   ${P2B-05}
    AddConn    ${nto1}  c79   ${P2B-05}  ${f79}   ${P2B-06}
    AddConn    ${nto1}  c80   ${P3-22}   ${f80}   ${P2B-07}
    AddConn    ${nto1}  c81   ${P2B-07}  ${f81}   ${P2B-08}
    AddConn    ${nto1}  c82   ${P3-24}   ${f82}   ${P2B-09}
    AddConn    ${nto1}  c83   ${P2B-09}  ${f83}   ${P2B-10}
    AddConn    ${nto1}  c84   ${P3-26}   ${f84}   ${P2B-11}
    AddConn    ${nto1}  c85   ${P2B-11}  ${f85}   ${P2B-12}
    AddConn    ${nto1}  c86   ${P3-28}   ${f86}   ${P2B-13}
    AddConn    ${nto1}  c87   ${P2B-13}  ${f87}   ${P2B-14}
    AddConn    ${nto1}  c88   ${P3-30}   ${f88}   ${P2B-15}
    AddConn    ${nto1}  c89   ${P2B-15}  ${f89}   ${P2B-16}
    AddConn    ${nto1}  c90   ${P3-32}   ${f90}   ${P5-09}
    AddConn    ${nto1}  c91   ${P5-09}   ${f91}   ${P5-10}

    AddConn    ${nto1}  c92   ${P5-10}   ${f92}   ${P3-33}
    AddConn    ${nto1}  c93   ${P3-33}   ${f93}   ${P3-34}
    AddConn    ${nto1}  c94   ${P5-12}   ${f94}   ${P3-35}
    AddConn    ${nto1}  c95   ${P3-35}   ${f95}   ${P3-36}
    AddConn    ${nto1}  c96   ${P5-14}   ${f96}   ${P3-37}
    AddConn    ${nto1}  c97   ${P3-37}   ${f97}   ${P3-38}
    AddConn    ${nto1}  c98   ${P5-16}   ${f98}   ${P3-39}
    AddConn    ${nto1}  c99   ${P3-39}   ${f99}   ${P3-40}
    AddConn    ${nto1}  c100  ${P5-18}   ${f100}  ${P3-41}
    AddConn    ${nto1}  c101  ${P3-41}   ${f101}  ${P3-42}
    AddConn    ${nto1}  c102  ${P5-20}   ${f102}  ${P3-43}
    AddConn    ${nto1}  c103  ${P3-43}   ${f103}  ${P3-44}
    AddConn    ${nto1}  c104  ${P5-22}   ${f104}  ${P3-45}
    AddConn    ${nto1}  c105  ${P3-45}   ${f105}  ${P3-46}
    AddConn    ${nto1}  c106  ${P5-24}   ${f106}  ${P3-47}
    AddConn    ${nto1}  c107  ${P3-47}   ${f107}  ${P3-48}

    AddConn    ${nto1}  c108  ${P3-34}   ${f108}  ${P5-11}
    AddConn    ${nto1}  c109  ${P5-11}   ${f109}  ${P5-12}
    AddConn    ${nto1}  c110  ${P3-36}   ${f110}  ${P5-13}
    AddConn    ${nto1}  c111  ${P5-13}   ${f111}  ${P5-14}
    AddConn    ${nto1}  c112  ${P3-38}   ${f112}  ${P5-15}
    AddConn    ${nto1}  c113  ${P5-15}   ${f113}  ${P5-16}
    AddConn    ${nto1}  c114  ${P3-40}   ${f114}  ${P5-17}
    AddConn    ${nto1}  c115  ${P5-17}   ${f115}  ${P5-16}
    AddConn    ${nto1}  c116  ${P3-42}   ${f116}  ${P5-19}
    AddConn    ${nto1}  c117  ${P5-19}   ${f117}  ${P5-20}
    AddConn    ${nto1}  c118  ${P3-44}   ${f118}  ${P5-21}
    AddConn    ${nto1}  c119  ${P5-21}   ${f119}  ${P5-22}
    AddConn    ${nto1}  c120  ${P3-46}   ${f120}  ${P5-23}
    AddConn    ${nto1}  c121  ${P5-23}   ${f121}  ${P5-24}
    AddConn    ${nto1}  c122  ${P3-48}   ${f122}  ${P2A-02}
    AddConn    ${nto1}  c123  ${P2A-02}  ${f123}  ${P4-02}

    AddConn    ${nto1}  c124  ${P4-02}   ${f124}  ${P6-01}
    AddConn    ${nto1}  c125  ${P6-01}   ${f125}  ${P6-02}
    AddConn    ${nto1}  c126  ${P4-04}   ${f126}  ${P6-03}
    AddConn    ${nto1}  c127  ${P6-03}   ${f127}  ${P6-04}
    AddConn    ${nto1}  c128  ${P4-06}   ${f128}  ${P6-05}
    AddConn    ${nto1}  c129  ${P6-05}   ${f129}  ${P6-06}
    AddConn    ${nto1}  c130  ${P4-08}   ${f130}  ${P6-07}
    AddConn    ${nto1}  c131  ${P6-07}   ${f131}  ${P6-08}
    AddConn    ${nto1}  c132  ${P4-10}   ${f132}  ${P6-09}
    AddConn    ${nto1}  c133  ${P6-09}   ${f133}  ${P6-10}
    AddConn    ${nto1}  c134  ${P4-12}   ${f134}  ${P6-11}
    AddConn    ${nto1}  c135  ${P6-11}   ${f135}  ${P6-12}
    AddConn    ${nto1}  c136  ${P4-14}   ${f136}  ${P6-13}
    AddConn    ${nto1}  c137  ${P6-13}   ${f137}  ${P6-14}
    AddConn    ${nto1}  c138  ${P4-16}   ${f138}  ${P6-15}
    AddConn    ${nto1}  c139  ${P6-15}   ${f139}  ${P6-16}

    AddConn    ${nto1}  c140  ${P4-18}   ${f140}  ${P6-17}
    AddConn    ${nto1}  c141  ${P6-17}   ${f141}  ${P6-18}
    AddConn    ${nto1}  c142  ${P4-20}   ${f142}  ${P6-19}
    AddConn    ${nto1}  c143  ${P6-19}   ${f143}  ${P6-20}
    AddConn    ${nto1}  c144  ${P4-22}   ${f144}  ${P6-21}
    AddConn    ${nto1}  c145  ${P6-21}   ${f145}  ${P6-22}
    AddConn    ${nto1}  c146  ${P4-24}   ${f146}  ${P6-23}
    AddConn    ${nto1}  c147  ${P6-23}   ${f147}  ${P6-24}
    AddConn    ${nto1}  c148  ${P4-26}   ${f148}  ${P6-25}
    AddConn    ${nto1}  c149  ${P6-25}   ${f149}  ${P6-26}
    AddConn    ${nto1}  c150  ${P4-28}   ${f150}  ${P6-27}
    AddConn    ${nto1}  c151  ${P6-27}   ${f151}  ${P6-28}
    AddConn    ${nto1}  c152  ${P4-30}   ${f152}  ${P6-29}
    AddConn    ${nto1}  c153  ${P6-29}   ${f153}  ${P6-30}
    AddConn    ${nto1}  c154  ${P4-32}   ${f154}  ${P6-31}
    AddConn    ${nto1}  c155  ${P6-31}   ${f155}  ${P6-32}

    AddConn    ${nto1}  c156  ${P4-34}   ${f156}  ${P6-33}
    AddConn    ${nto1}  c157  ${P6-33}   ${f157}  ${P6-34}
    AddConn    ${nto1}  c158  ${P4-36}   ${f158}  ${P6-35}
    AddConn    ${nto1}  c159  ${P6-35}   ${f159}  ${P6-36}
    AddConn    ${nto1}  c160  ${P4-38}   ${f160}  ${P6-37}
    AddConn    ${nto1}  c161  ${P6-37}   ${f161}  ${P6-38}
    AddConn    ${nto1}  c162  ${P4-40}   ${f162}  ${P6-39}
    AddConn    ${nto1}  c163  ${P6-39}   ${f163}  ${P6-40}
    AddConn    ${nto1}  c164  ${P4-42}   ${f164}  ${P6-41}
    AddConn    ${nto1}  c165  ${P6-41}   ${f165}  ${P6-42}
    AddConn    ${nto1}  c166  ${P4-44}   ${f166}  ${P6-43}
    AddConn    ${nto1}  c167  ${P6-43}   ${f167}  ${P6-44}
    AddConn    ${nto1}  c168  ${P4-46}   ${f168}  ${P6-45}
    AddConn    ${nto1}  c169  ${P6-45}   ${f169}  ${P6-46}
    AddConn    ${nto1}  c170  ${P4-48}   ${f170}  ${P6-47}
    AddConn    ${nto1}  c171  ${P6-47}   ${f171}  ${P6-48}

    AddConn    ${nto1}  c172  ${P6-02}   ${f172}  ${P4-03}
    AddConn    ${nto1}  c173  ${P4-03}   ${f173}  ${P4-04}
    AddConn    ${nto1}  c174  ${P6-04}   ${f174}  ${P4-05}
    AddConn    ${nto1}  c175  ${P4-05}   ${f175}  ${P4-06}
    AddConn    ${nto1}  c176  ${P6-06}   ${f176}  ${P4-07}
    AddConn    ${nto1}  c177  ${P4-07}   ${f177}  ${P4-08}
    AddConn    ${nto1}  c178  ${P6-08}   ${f178}  ${P4-09}
    AddConn    ${nto1}  c179  ${P4-09}   ${f179}  ${P4-10}
    AddConn    ${nto1}  c180  ${P6-10}   ${f180}  ${P4-11}
    AddConn    ${nto1}  c181  ${P4-11}   ${f181}  ${P4-12}
    AddConn    ${nto1}  c182  ${P6-12}   ${f182}  ${P4-13}
    AddConn    ${nto1}  c183  ${P4-13}   ${f183}  ${P4-14}
    AddConn    ${nto1}  c184  ${P6-14}   ${f184}  ${P4-15}
    AddConn    ${nto1}  c185  ${P4-15}   ${f185}  ${P4-16}
    AddConn    ${nto1}  c186  ${P6-16}   ${f186}  ${P4-17}
    AddConn    ${nto1}  c187  ${P4-17}   ${f187}  ${P4-18}

    AddConn    ${nto1}  c188  ${P6-18}   ${f188}  ${P4-19}
    AddConn    ${nto1}  c189  ${P4-19}   ${f189}  ${P4-20}
    AddConn    ${nto1}  c190  ${P6-20}   ${f190}  ${P4-21}
    AddConn    ${nto1}  c191  ${P4-21}   ${f191}  ${P4-22}
    AddConn    ${nto1}  c192  ${P6-22}   ${f192}  ${P4-23}
    AddConn    ${nto1}  c193  ${P4-23}   ${f193}  ${P4-24}
    AddConn    ${nto1}  c194  ${P6-24}   ${f194}  ${P4-25}
    AddConn    ${nto1}  c195  ${P4-25}   ${f195}  ${P4-26}
    AddConn    ${nto1}  c196  ${P6-26}   ${f196}  ${P4-27}
    AddConn    ${nto1}  c197  ${P4-27}   ${f197}  ${P4-28}
    AddConn    ${nto1}  c198  ${P6-28}   ${f198}  ${P4-29}
    AddConn    ${nto1}  c199  ${P4-29}   ${f199}  ${P4-30}
    AddConn    ${nto1}  c200  ${P6-30}   ${f200}  ${P4-31}
    AddConn    ${nto1}  c201  ${P4-31}   ${f201}  ${P4-32}
    AddConn    ${nto1}  c202  ${P6-32}   ${f202}  ${P4-33}
    AddConn    ${nto1}  c203  ${P4-33}   ${f203}  ${P4-34}

    AddConn    ${nto1}  c204  ${P6-34}   ${f204}  ${P4-35}
    AddConn    ${nto1}  c205  ${P4-35}   ${f205}  ${P4-36}
    AddConn    ${nto1}  c206  ${P6-36}   ${f206}  ${P4-37}
    AddConn    ${nto1}  c207  ${P4-37}   ${f207}  ${P4-38}
    AddConn    ${nto1}  c208  ${P6-38}   ${f208}  ${P4-39}
    AddConn    ${nto1}  c209  ${P4-39}   ${f209}  ${P4-40}
    AddConn    ${nto1}  c210  ${P6-40}   ${f210}  ${P4-41}
    AddConn    ${nto1}  c211  ${P4-41}   ${f211}  ${P4-42}
    AddConn    ${nto1}  c212  ${P6-42}   ${f212}  ${P4-43}
    AddConn    ${nto1}  c213  ${P4-43}   ${f213}  ${P4-44}
    AddConn    ${nto1}  c214  ${P6-44}   ${f214}  ${P4-45}
    AddConn    ${nto1}  c215  ${P4-45}   ${f215}  ${P4-46}
    AddConn    ${nto1}  c216  ${P6-46}   ${f216}  ${P4-47}
    AddConn    ${nto1}  c217  ${P4-47}   ${f217}  ${P4-48}
    AddConn    ${nto1}  c218  ${P6-48}   ${f218}  ${P4-01}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Stream definitions.
    ${stream1}=  createStream  s1  Ether(src='00:11:22:33:44:55')/IP()  payload=rand  frameSize=64   numFrames=100
    ${stream2}=  createStream  s2  Ether(src='00:66:77:88:99:00')/IP()  payload=rand  frameSize=128   numFrames=100

    P1-01Ixia.addIxStreams   ${stream1}
    P2A-01Ixia.addIxStreams  ${stream2}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  continuous=True  percentPktRate=50.0

    Set Test Variable  ${maxIterations}  5
    :FOR  ${loop}  IN RANGE  0  ${maxIterations}
    \  Log  Traffic iteration ${loop} out of ${maxIterations} executing...  console=true
    \  P1-01Ixia.runIxContinuousTraffic
    \  P2A-01Ixia.runIxContinuousTraffic
    \  Sleep  30 seconds
    \  P1-01Ixia.stopIxTraffic
    \  P2A-01Ixia.stopIxTraffic
    \  Sleep  3 seconds
    \  ${P1-01IxiaTxFrames}=  P1-01Ixia.getIxPortTxStats  statName=txFrames
    \  ${P1-01RxFrames}=  P1-01.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  Should Be Equal As Integers  ${P1-01IxiaTxFrames}  ${P1-01RxFrames}
    \  ${P2A-01IxiaTxFrames}=  P2A-01Ixia.getIxPortTxStats  statName=txFrames
    \  ${P2A-01RxFrames}=  P2A-01.getPortStats  statName=NP_TOTAL_RX_COUNT_VALID_PACKETS
    \  Should Be Equal As Integers  ${P2A-01IxiaTxFrames}  ${P2A-01RxFrames}

#    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts

*** Keywords ***
setupSuiteEnv
    Set Global Variable  &{ixChassisDict}  &{EMPTY}

