*** Settings ***

Library    Remote    https://usaus-kvm-testSrv:8270     WITH NAME  srvr
Library    Remote    https://usaus-kvm-testClient:8270  WITH NAME  clnt

*** Variables ***

*** Test Cases ***
Get_Remote_Hostnames
    ${output}=  srvr.getHostname
    Set Suite Variable   ${serverHost}   ${output}
    log  \nremote server hostname - ${serverHost}  console=true

    ${output}=  clnt.getHostname
    Set Suite Variable   ${clientHost}   ${output}
    log  remote client hostname - ${clientHost}  console=true

Ping_Between_Remote_Hosts
    srvr.checkPing   ${clientHost}
    clnt.checkPing   ${serverHost}

Generate_Server_SSL_Cert
    ${myUUID}=  srvr.generateUUID
    Set Suite Variable  ${serverSSLCert}  /tmp/${myUUID}.pem
    srvr.generateSSLCert     ${serverSSLCert}

Generate_Client_SSL_Cert
    ${myUUID}=  clnt.generateUUID
    Set Suite Variable  ${clientSSLCert}  /tmp/${myUUID}.pem
    clnt.generateSSLCert  ${clientSSLCert}

SSL_Connection_Test
    ${srvPID}=  srvr.startOpenSSLServer  ${serverSSLCert}
    clnt.runSSLConnectionTest  ${clientSSLCert}  ${serverHost}  10  120
    srvr.terminateOpenSSLServer  ${srvPID}

