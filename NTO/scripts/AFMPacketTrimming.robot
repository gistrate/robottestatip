*** Settings ***
Suite Setup     setupSuiteEnv
Test Setup      setupTestEnv
Suite Teardown  suiteCleanup
#Test Teardown   testCleanup

#Libraries

Library  ./resourceMgr_module/resourceMgr.py   nto-dbmgr-02.ann.is.keysight.com
Library  ./NTO/frwk/Utils.py
Library  Collections
Library  Dialogs
Library  OperatingSystem

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
#Command line variables:   ${RESOURCEPOOL}
${RESOURCETIMEOUT}=  60
${noOfPacketsInStreams}=  100
${maxFrameLength}=  10221
${maxRetainedBytes}=  12266
${bwAllocation}=  10
${noOfStreams}=   3
${lineRate}=  100

*** Test Cases ***

AFM-PKT-TRIM-NP-DISABLE-2560
    #[Tags]  skip
    #Description: Test packet trimming disabled in NP. Pass the traffic.

    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': False, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'retained_bytes': 9}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_57}  ${afm_58}  ${afm_50}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_57' ,  'afm_58' ,  'afm_50']}, 'Filter': {'f1':[ 'afm_57' ,  'afm_58' ,  'afm_50']}, 'Tool': {'tp1': [ 'afm_57' ,  'afm_58' ,  'afm_50']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-NP-DISABLE-AFTER-ENABLE-2567
    #Description: Test packet trimming disable after enabled tests in NP. Pass the traffic.
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${captureFileDisabled}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim_disabled.enc
    ${capturePcapDisabled}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim_disabled.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC', 'retained_bytes': 1500}}
    ${pp3}=    Catenate  {"trim_settings": {'enabled': False, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'retained_bytes': 9}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_06}  ${afm_12}  ${afm_21}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_06' ,  'afm_12' ,  'afm_21']}, 'Filter': {'f1':[ 'afm_06' ,  'afm_12' ,  'afm_21']}, 'Tool': {'tp1': [ 'afm_06' ,  'afm_12' ,  'afm_21']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}

    log  Disable packet trimming feature and check statistics and captures again
    nto1.resetAllStats
    np1.configurePacketProcessing  ${pp3}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    tp1Ixia.exportIxFramesToFile  ${captureFileDisabled}  1  ${noOfPackets}
    convertEncToPcap  ${captureFileDisabled}  ${capturePcapDisabled}

    tp1Ixia.scapyImportPcap  ${capturePcapDisabled}
    ${np1NonTrimmedFrames}=  np1.pktProc.emulate  ${pp3}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1NonTrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    afm1.detachResource  ${np1}
    Remove Files  ${captureFile}  ${origCaptureFile}  ${captureFileDisabled}

AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-NP-2638
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+79, NP
     #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'retained_bytes': 79}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_09}  ${afm_31}  ${afm_41}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_09' ,  'afm_31' ,  'afm_41']}, 'Filter': {'f1':[ 'afm_09' ,  'afm_31' ,  'afm_41']}, 'Tool': {'tp1': [ 'afm_09' ,  'afm_31' ,  'afm_41']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-NP-2639
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+505, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'retained_bytes': 505}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_03}  ${afm_36}  ${afm_52}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_03' ,  'afm_36' ,  'afm_52']}, 'Filter': {'f1':[ 'afm_03' ,  'afm_36' ,  'afm_52']}, 'Tool': {'tp1': [ 'afm_03' ,  'afm_36' ,  'afm_52']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-ONLY-ONE-BYTE-NP-2644
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+13233 to trim one byte from 9000 packet below, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'retained_bytes': ${maxRetainedBytes}}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_45}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_45']}, 'Filter': {'f1':[ 'afm_45']}, 'Tool': {'tp1': [ 'afm_45']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-NP-2582
    #Description: Test packet trimming Retain MAC+9, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC', 'retained_bytes': 9}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_01}  ${afm_22}  ${afm_59}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_01' ,  'afm_22' ,  'afm_59']}, 'Filter': {'f1':[ 'afm_01' ,  'afm_22' ,  'afm_59']}, 'Tool': {'tp1': [ 'afm_01' ,  'afm_22' ,  'afm_59']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-NP-2583
    #Description: Test packet trimming Retain MAC+16342, NP

    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC', 'retained_bytes': ${maxRetainedBytes}}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_17}  ${afm_26}  ${afm_43}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_17' ,  'afm_26' ,  'afm_43']}, 'Filter': {'f1':[ 'afm_17' ,  'afm_26' ,  'afm_43']}, 'Tool': {'tp1': [ 'afm_17' ,  'afm_26' ,  'afm_43']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-NP-2584
    #Description: Test packet trimming Retain MAC+10, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC', 'retained_bytes': 10}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_06}  ${afm_12}  ${afm_21}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_06' ,  'afm_12' ,  'afm_21']}, 'Filter': {'f1':[ 'afm_06' ,  'afm_12' ,  'afm_21']}, 'Tool': {'tp1': [ 'afm_06' ,  'afm_12' ,  'afm_21']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-NP-2585
    #Description: Test packet trimming Retain MAC+3024, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC', 'retained_bytes': 3024}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_03}  ${afm_31}  ${afm_60}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_03' ,  'afm_31' ,  'afm_60']}, 'Filter': {'f1':[ 'afm_03' ,  'afm_31' ,  'afm_60']}, 'Tool': {'tp1': [ 'afm_03' ,  'afm_31' ,  'afm_60']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-RETAIN-MAC-TRIM-PARTIAL-HEADERS-NP-2591
    #Description: Test packet trimming Retain MAC+9, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC', 'retained_bytes': 9}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_01}  ${afm_22}  ${afm_59}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_01' ,  'afm_22' ,  'afm_59']}, 'Filter': {'f1':[ 'afm_01' ,  'afm_22' ,  'afm_59']}, 'Tool': {'tp1': [ 'afm_01' ,  'afm_22' ,  'afm_59']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-NP-2600
    #Description: Test packet trimming Retain MAC+VLAN+9, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN', 'retained_bytes': 9}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_09}  ${afm_21}  ${afm_52}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_09' ,  'afm_21' ,  'afm_52']}, 'Filter': {'f1':[ 'afm_09' ,  'afm_21' ,  'afm_52']}, 'Tool': {'tp1': [ 'afm_09' ,  'afm_21' ,  'afm_52']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-NP-2601
    #Description: Test packet trimming Retain MAC+VLAN+16342, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN', 'retained_bytes': ${maxRetainedBytes}}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_04}  ${afm_37}  ${afm_46}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_04' ,  'afm_37' ,  'afm_46']}, 'Filter': {'f1':[ 'afm_04' ,  'afm_37' ,  'afm_46']}, 'Tool': {'tp1': [ 'afm_04' ,  'afm_37' ,  'afm_46']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-NP-2602
    #Description: Test packet trimming Retain MAC+VLAN+100, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN', 'retained_bytes': 100}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_01}  ${afm_32}  ${afm_56}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_01' ,  'afm_32' ,  'afm_56']}, 'Filter': {'f1':[ 'afm_01' ,  'afm_32' ,  'afm_56']}, 'Tool': {'tp1': [ 'afm_01' ,  'afm_32' ,  'afm_56']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-NP-2603
    #Description: Test packet trimming Retain MAC+VLAN+2001, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN', 'retained_bytes': 2001}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_18}  ${afm_23}  ${afm_53}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_18' ,  'afm_23' ,  'afm_53']}, 'Filter': {'f1':[ 'afm_18' ,  'afm_23' ,  'afm_53']}, 'Tool': {'tp1': [ 'afm_18' ,  'afm_23' ,  'afm_53']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-MPLS-NP-2618
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+9, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS', 'retained_bytes': 9}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_02}  ${afm_26}  ${afm_54}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_02' ,  'afm_26' ,  'afm_54']}, 'Filter': {'f1':[ 'afm_02' ,  'afm_26' ,  'afm_54']}, 'Tool': {'tp1': [ 'afm_02' ,  'afm_26' ,  'afm_54']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-MPLS-NP-2619
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+16342, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS', 'retained_bytes': ${maxRetainedBytes}}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_13}  ${afm_36}  ${afm_53}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_13' ,  'afm_36' ,  'afm_53']}, 'Filter': {'f1':[ 'afm_13' ,  'afm_36' ,  'afm_53']}, 'Tool': {'tp1': [ 'afm_13' ,  'afm_36' ,  'afm_53']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-MPLS-NP-2620
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+1500, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS', 'retained_bytes': 1500}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_06}  ${afm_33}  ${afm_42}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_06' ,  'afm_33' ,  'afm_42']}, 'Filter': {'f1':[ 'afm_06' ,  'afm_33' ,  'afm_42']}, 'Tool': {'tp1': [ 'afm_06' ,  'afm_33' ,  'afm_42']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-MPLS-NP-2621
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+120, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS', 'retained_bytes': 120}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_12}  ${afm_21}  ${afm_45}  ${afm_56}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_12' ,  'afm_21' ,  'afm_45' ,  'afm_56']}, 'Filter': {'f1':[ 'afm_12' ,  'afm_21' ,  'afm_45' ,  'afm_56']}, 'Tool': {'tp1': [ 'afm_12' ,  'afm_21' ,  'afm_45' ,  'afm_56']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-MPLS-ONLY-ONE-BYTE-NP-2626
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+8941 to trim only one byte from 9000, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS', 'retained_bytes': 8941}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_45}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_45']}, 'Filter': {'f1':[ 'afm_45']}, 'Tool': {'tp1': [ 'afm_45']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-NP-2636
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+9, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'retained_bytes': 9}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_11}  ${afm_28}  ${afm_45}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_11' ,  'afm_28' ,  'afm_45']}, 'Filter': {'f1':[ 'afm_11' ,  'afm_28' ,  'afm_45']}, 'Tool': {'tp1': [ 'afm_11' ,  'afm_28' ,  'afm_45']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-NP-2637
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+16342, NP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'retained_bytes': ${maxRetainedBytes}}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${np1}  ${bwAllocation}
    np1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_10}  ${afm_31}  ${afm_46}
    #np1Ixia.addIxStreams   ${afm_31}  ${afm_10}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${np1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_10' ,  'afm_31' ,  'afm_46']}, 'Filter': {'f1':[ 'afm_10' ,  'afm_31' ,  'afm_46']}, 'Tool': {'tp1': [ 'afm_10' ,  'afm_31' ,  'afm_46']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${np1}


AFM-PKT-TRIM-TP-DISABLE-2562
    #Description: Test packet trimming disabled in TP. Pass the traffic.
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'enabled': False}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_57}  ${afm_58}  ${afm_50}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_57' ,  'afm_58' ,  'afm_50']}, 'Filter': {'f1':[ 'afm_57' ,  'afm_58' ,  'afm_50']}, 'Tool': {'tp1': [ 'afm_57' ,  'afm_58' ,  'afm_50']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-TP-DISABLE-AFTER-ENABLE-2569
    #Description: Test packet trimming Disable after Enable Test of Retain MAC+9 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${captureFileDisabled}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim_disabled.enc
    ${capturePcapDisabled}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim_disabled.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'retained_headers': 'MAC', 'enabled': True}}
    ${pp3}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'enabled': False}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_07}  ${afm_11}  ${afm_27}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Filter': {'f1':[ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Tool': {'tp1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}

    log  Disable packet trimming feature and check statistics and captures again
    nto1.resetAllStats
    tp1.configurePacketProcessing  ${pp3}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    tp1Ixia.exportIxFramesToFile  ${captureFileDisabled}  1  ${noOfPackets}
    convertEncToPcap  ${captureFileDisabled}  ${capturePcapDisabled}

    tp1Ixia.scapyImportPcap  ${capturePcapDisabled}
    ${np1NonTrimmedFrames}=  np1.pktProc.emulate  ${pp3}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${np1NonTrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-TP-2586
    #Description: Test packet trimming Retain MAC+9 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'retained_headers': 'MAC', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_07}  ${afm_11}  ${afm_27}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Filter': {'f1':[ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Tool': {'tp1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-TP-2587
    #Description: Test packet trimming Retain MAC+16342 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'retained_headers': 'MAC', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_08}  ${afm_38}  ${afm_41}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_08' ,  'afm_38' ,  'afm_41']}, 'Filter': {'f1':[ 'afm_08' ,  'afm_38' ,  'afm_41']}, 'Tool': {'tp1': [ 'afm_08' ,  'afm_38' ,  'afm_41']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-TP-2588
    #Description: Test packet trimming Retain MAC+1500 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 1500, 'retained_headers': 'MAC', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_02}  ${afm_16}  ${afm_36}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_02' ,  'afm_16' ,  'afm_36']}, 'Filter': {'f1':[ 'afm_02' ,  'afm_16' ,  'afm_36']}, 'Tool': {'tp1': [ 'afm_02' ,  'afm_16' ,  'afm_36']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-TP-2589
    #Description: Test packet trimming Retain MAC+1500 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 1500, 'retained_headers': 'MAC', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_15}  ${afm_31}  ${afm_47}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_15' ,  'afm_31' ,  'afm_47']}, 'Filter': {'f1':[ 'afm_15' ,  'afm_31' ,  'afm_47']}, 'Tool': {'tp1': [ 'afm_15' ,  'afm_31' ,  'afm_47']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-TRIM-ONLY-ONE-BYTE-TP-2590
    #Description: Test packet trimming only ONE byte TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 79, 'retained_headers': 'MAC', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_11}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_11']}, 'Filter': {'f1':[ 'afm_11']}, 'Tool': {'tp1': [ 'afm_11']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-TP-2604
    #Description: Test packet trimming Retain MAC+VLAN+9 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'retained_headers': 'MAC_VLAN', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_06}  ${afm_37}  ${afm_52}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_06' ,  'afm_37' ,  'afm_52']}, 'Filter': {'f1':[ 'afm_06' ,  'afm_37' ,  'afm_52']}, 'Tool': {'tp1': [ 'afm_06' ,  'afm_37' ,  'afm_52']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-TP-2605
    #Description: Test packet trimming Retain MAC+VLAN+16342 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'retained_headers': 'MAC_VLAN', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_13}  ${afm_26}  ${afm_53}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_13' ,  'afm_26' ,  'afm_53']}, 'Filter': {'f1':[ 'afm_13' ,  'afm_26' ,  'afm_53']}, 'Tool': {'tp1': [ 'afm_13' ,  'afm_26' ,  'afm_53']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-TP-2606
    #Description: Test packet trimming Retain MAC+VLAN+20 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 20, 'retained_headers': 'MAC_VLAN', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_16}  ${afm_22}  ${afm_51}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_16' ,  'afm_22' ,  'afm_51']}, 'Filter': {'f1':[ 'afm_16' ,  'afm_22' ,  'afm_51']}, 'Tool': {'tp1': [ 'afm_16' ,  'afm_22' ,  'afm_51']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-TP-2607
    #Description: Test packet trimming Retain MAC+VLAN+5000 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 5000, 'retained_headers': 'MAC_VLAN', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_05}  ${afm_36}  ${afm_42}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_05' ,  'afm_36' ,  'afm_42']}, 'Filter': {'f1':[ 'afm_05' ,  'afm_36' ,  'afm_42']}, 'Tool': {'tp1': [ 'afm_05' ,  'afm_36' ,  'afm_42']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-TRIM-ONLY-ONE-BYTE-TP-2608
    #Description: Test packet trimming Retain MAC+VLAN+8977 TP to trim only one from 9000
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 8977, 'retained_headers': 'MAC_VLAN', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_36}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_36']}, 'Filter': {'f1':[ 'afm_36']}, 'Tool': {'tp1': [ 'afm_36']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-TP-2622
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+9 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'retained_headers': 'MAC_VLAN_MPLS', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_16}  ${afm_33}  ${afm_51}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_16' ,  'afm_33' ,  'afm_51']}, 'Filter': {'f1':[ 'afm_16' ,  'afm_33' ,  'afm_51']}, 'Tool': {'tp1': [ 'afm_16' ,  'afm_33' ,  'afm_51']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-TP-2623
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+16342 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'retained_headers': 'MAC_VLAN_MPLS', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_07}  ${afm_29}  ${afm_46}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_07' ,  'afm_29' ,  'afm_46']}, 'Filter': {'f1':[ 'afm_07' ,  'afm_29' ,  'afm_46']}, 'Tool': {'tp1': [ 'afm_07' ,  'afm_29' ,  'afm_46']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-TP-2624
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+1500 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 1500, 'retained_headers': 'MAC_VLAN_MPLS', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_20}  ${afm_21}  ${afm_48}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_20' ,  'afm_21' ,  'afm_48']}, 'Filter': {'f1':[ 'afm_20' ,  'afm_21' ,  'afm_48']}, 'Tool': {'tp1': [ 'afm_20' ,  'afm_21' ,  'afm_48']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-TP-2625
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+9000 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9000, 'retained_headers': 'MAC_VLAN_MPLS', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_01}  ${afm_27}  ${afm_60}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_01' ,  'afm_27' ,  'afm_60']}, 'Filter': {'f1':[ 'afm_01' ,  'afm_27' ,  'afm_60']}, 'Tool': {'tp1': [ 'afm_01' ,  'afm_27' ,  'afm_60']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-TP-2640
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+9 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_08}  ${afm_21}  ${afm_46}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_08' ,  'afm_21' ,  'afm_46']}, 'Filter': {'f1':[ 'afm_08' ,  'afm_21' ,  'afm_46']}, 'Tool': {'tp1': [ 'afm_08' ,  'afm_21' ,  'afm_46']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-TP-2641
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+16342 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_17}  ${afm_40}  ${afm_52}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_17' ,  'afm_40' ,  'afm_52']}, 'Filter': {'f1':[ 'afm_17' ,  'afm_40' ,  'afm_52']}, 'Tool': {'tp1': [ 'afm_17' ,  'afm_40' ,  'afm_52']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-TP-2642
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+1500 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 1500, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_16}  ${afm_23}  ${afm_55}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_16' ,  'afm_23' ,  'afm_55']}, 'Filter': {'f1':[ 'afm_16' ,  'afm_23' ,  'afm_55']}, 'Tool': {'tp1': [ 'afm_16' ,  'afm_23' ,  'afm_55']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-TP-2643
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+15 TP
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 15, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'enabled': True}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${tp1}  ${bwAllocation}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_15}  ${afm_22}  ${afm_41}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tp1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tp1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_15' ,  'afm_22' ,  'afm_41']}, 'Filter': {'f1':[ 'afm_15' ,  'afm_22' ,  'afm_41']}, 'Tool': {'tp1': [ 'afm_15' ,  'afm_22' ,  'afm_41']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tp1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-DFAFMR-TC000926738
    #Description: Test packet trimming Retain MAC+1500 on DFAFMR
    #[Tags]   skip  Vision_One  7300_Gibson
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_headers': 'MAC', 'enabled': True, 'retained_bytes': 1500}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${f1}  ${bwAllocation}
    f1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_02}  ${afm_16}  ${afm_36}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${f1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${f1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_02' ,  'afm_16' ,  'afm_36']}, 'Filter': {'f1':[ 'afm_02' ,  'afm_16' ,  'afm_36']}, 'Tool': {'tp1': [ 'afm_02' ,  'afm_16' ,  'afm_36']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${f1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-DFAFMR-TC000926739
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+15 DFAFMR
    #[Tags]   skip  Vision_One  7300_Gibson
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_headers': 'MAC_VLAN_MPLS_L3', 'enabled': True, 'retained_bytes': 15}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${f1}  ${bwAllocation}
    f1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_15}  ${afm_22}  ${afm_41}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${f1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${f1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_15' ,  'afm_22' ,  'afm_41']}, 'Filter': {'f1':[ 'afm_15' ,  'afm_22' ,  'afm_41']}, 'Tool': {'tp1': [ 'afm_15' ,  'afm_22' ,  'afm_41']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${f1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-DFAFMR-TC000926740
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+16342 DFAFMR
    #[Tags]   skip  Vision_One  7300_Gibson
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_headers': 'MAC_VLAN_MPLS', 'enabled': True, 'retained_bytes': ${maxRetainedBytes}}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${f1}  ${bwAllocation}
    f1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_07}  ${afm_29}  ${afm_46}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${f1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${f1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_07' ,  'afm_29' ,  'afm_46']}, 'Filter': {'f1':[ 'afm_07' ,  'afm_29' ,  'afm_46']}, 'Tool': {'tp1': [ 'afm_07' ,  'afm_29' ,  'afm_46']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${f1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-DFAFMR-TC000926741
    #Description: Test packet trimming Retain MAC+VLAN+9 DFAFMR
    #[Tags]   skip  Vision_One  7300_Gibson
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_headers': 'MAC_VLAN', 'enabled': True, 'retained_bytes': 9}}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    afm1.attachResource  ${f1}  ${bwAllocation}
    f1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_06}  ${afm_37}  ${afm_52}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture


    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${f1TrimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${f1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_06' ,  'afm_37' ,  'afm_52']}, 'Filter': {'f1':[ 'afm_06' ,  'afm_37' ,  'afm_52']}, 'Tool': {'tp1': [ 'afm_06' ,  'afm_37' ,  'afm_52']}}
    #nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${f1}
    Remove Files  ${captureFile}  ${origCaptureFile}




AFM-PKT-TRIM-MAC-LBPG-32586
    #Description: Test packet trimming Retain MAC+9 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'enabled': True, 'retained_headers': 'MAC'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_07}  ${afm_11}  ${afm_27}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Filter': {'f1':[ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Tool': {'lbpg1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-LBPG-2654
    #Description: Test packet trimming Retain MAC+16342 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'enabled': True, 'retained_headers': 'MAC'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_08}  ${afm_38}  ${afm_41}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_08' ,  'afm_38' ,  'afm_41']}, 'Filter': {'f1':[ 'afm_08' ,  'afm_38' ,  'afm_41']}, 'Tool': {'lbpg1': [ 'afm_08' ,  'afm_38' ,  'afm_41']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-LBPG-2653
    #Description: Test packet trimming Retain MAC+1501 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 1501, 'enabled': True, 'retained_headers': 'MAC'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_02}  ${afm_16}  ${afm_36}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_02' ,  'afm_16' ,  'afm_36']}, 'Filter': {'f1':[ 'afm_02' ,  'afm_16' ,  'afm_36']}, 'Tool': {'lbpg1': [ 'afm_02' ,  'afm_16' ,  'afm_36']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-LBPG-32589
    #Description: Test packet trimming Retain MAC+9000 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9000, 'enabled': True, 'retained_headers': 'MAC'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_15}  ${afm_31}  ${afm_47}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_15' ,  'afm_31' ,  'afm_47']}, 'Filter': {'f1':[ 'afm_15' ,  'afm_31' ,  'afm_47']}, 'Tool': {'lbpg1': [ 'afm_15' ,  'afm_31' ,  'afm_47']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-LBPG-62604
    #Description: Test packet trimming Retain MAC+VLAN+9 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'enabled': True, 'retained_headers': 'MAC_VLAN'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_06}  ${afm_37}  ${afm_52}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_06' ,  'afm_37' ,  'afm_52']}, 'Filter': {'f1':[ 'afm_06' ,  'afm_37' ,  'afm_52']}, 'Tool': {'lbpg1': [ 'afm_06' ,  'afm_37' ,  'afm_52']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-LBPG-2668
    #Description: Test packet trimming Retain MAC+VLAN+16342 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'enabled': True, 'retained_headers': 'MAC_VLAN'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_13}  ${afm_26}  ${afm_53}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_13' ,  'afm_26' ,  'afm_53']}, 'Filter': {'f1':[ 'afm_13' ,  'afm_26' ,  'afm_53']}, 'Tool': {'lbpg1': [ 'afm_13' ,  'afm_26' ,  'afm_53']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-LBPG-62606
    #Description: Test packet trimming Retain MAC+VLAN+1500 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 1500, 'enabled': True, 'retained_headers': 'MAC_VLAN'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_16}  ${afm_22}  ${afm_51}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_16' ,  'afm_22' ,  'afm_51']}, 'Filter': {'f1':[ 'afm_16' ,  'afm_22' ,  'afm_51']}, 'Tool': {'lbpg1': [ 'afm_16' ,  'afm_22' ,  'afm_51']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-LBPG-62607
    #Description: Test packet trimming Retain MAC+VLAN+9000 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9000, 'enabled': True, 'retained_headers': 'MAC_VLAN'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_05}  ${afm_36}  ${afm_42}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_05' ,  'afm_36' ,  'afm_42']}, 'Filter': {'f1':[ 'afm_05' ,  'afm_36' ,  'afm_42']}, 'Tool': {'lbpg1': [ 'afm_05' ,  'afm_36' ,  'afm_42']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-LBPG-2683
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+9 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_16}  ${afm_33}  ${afm_51}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_16' ,  'afm_33' ,  'afm_51']}, 'Filter': {'f1':[ 'afm_16' ,  'afm_33' ,  'afm_51']}, 'Tool': {'lbpg1': [ 'afm_16' ,  'afm_33' ,  'afm_51']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-LBPG-2684
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+16342 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_07}  ${afm_29}  ${afm_46}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_07' ,  'afm_29' ,  'afm_46']}, 'Filter': {'f1':[ 'afm_07' ,  'afm_29' ,  'afm_46']}, 'Tool': {'lbpg1': [ 'afm_07' ,  'afm_29' ,  'afm_46']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-LBPG-92624
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+1500 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 1500, 'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_20}  ${afm_21}  ${afm_48}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_20' ,  'afm_21' ,  'afm_48']}, 'Filter': {'f1':[ 'afm_20' ,  'afm_21' ,  'afm_48']}, 'Tool': {'lbpg1': [ 'afm_20' ,  'afm_21' ,  'afm_48']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-LBPG-92625
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+9000 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9000, 'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_01}  ${afm_27}  ${afm_60}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_01' ,  'afm_27' ,  'afm_60']}, 'Filter': {'f1':[ 'afm_01' ,  'afm_27' ,  'afm_60']}, 'Tool': {'lbpg1': [ 'afm_01' ,  'afm_27' ,  'afm_60']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-LBPG-2698
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+9 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_08}  ${afm_21}  ${afm_46}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_08' ,  'afm_21' ,  'afm_46']}, 'Filter': {'f1':[ 'afm_08' ,  'afm_21' ,  'afm_46']}, 'Tool': {'lbpg1': [ 'afm_08' ,  'afm_21' ,  'afm_46']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-LBPG-2699
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+16342 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_17}  ${afm_40}  ${afm_52}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_17' ,  'afm_40' ,  'afm_52']}, 'Filter': {'f1':[ 'afm_17' ,  'afm_40' ,  'afm_52']}, 'Tool': {'lbpg1': [ 'afm_17' ,  'afm_40' ,  'afm_52']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-LBPG-122642
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+1500 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 1500, 'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_16}  ${afm_23}  ${afm_55}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_16' ,  'afm_23' ,  'afm_55']}, 'Filter': {'f1':[ 'afm_16' ,  'afm_23' ,  'afm_55']}, 'Tool': {'lbpg1': [ 'afm_16' ,  'afm_23' ,  'afm_55']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-LBPG-122643
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+9000 LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9000, 'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_15}  ${afm_22}  ${afm_41}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${lbpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_15' ,  'afm_22' ,  'afm_41']}, 'Filter': {'f1':[ 'afm_15' ,  'afm_22' ,  'afm_41']}, 'Tool': {'lbpg1': [ 'afm_15' ,  'afm_22' ,  'afm_41']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}





AFM-PKT-TRIM-MAC-NPG-12584
    #Description: Test packet trimming Retain MAC+1500, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_headers': 'MAC', 'retained_bytes': 1500, 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_06}  ${afm_12}  ${afm_21}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    ##${statsMap}=  Catenate  {'Network': {'npg1': [ 'afm_06', 'afm_12', 'afm_21']}, 'Filter': {'f1':[ 'afm_06', 'afm_12', 'afm_21']}, 'Tool': {'tp1': [ 'afm_06', 'afm_12', 'afm_21']}}
    ###nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-NPG-12585
    #Description: Test packet trimming Retain MAC+9000, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_headers': 'MAC', 'retained_bytes': 9000, 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_03}  ${afm_31}  ${afm_60}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-NPG-2663
    #Description: Test packet trimming Retain MAC+VLAN+9, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_headers': 'MAC_VLAN', 'retained_bytes': 9, 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_09}  ${afm_21}  ${afm_52}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-NPG-2648
    #Description: Test packet trimming Retain MAC+9, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'retained_headers': 'MAC', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_01}  ${afm_22}  ${afm_59}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-NPG-2649
    #Description: Test packet trimming Retain MAC+16342, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'retained_headers': 'MAC', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_17}  ${afm_26}  ${afm_43}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-NPG-2664
    #Description: Test packet trimming Retain MAC+VLAN+16342, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'retained_headers': 'MAC_VLAN', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_04}  ${afm_37}  ${afm_46}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-NPG-42602
    #Description: Test packet trimming Retain MAC+VLAN+1500, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 1500, 'retained_headers': 'MAC_VLAN', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_01}  ${afm_32}  ${afm_56}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-NPG-42603
    #Description: Test packet trimming Retain MAC+VLAN+9000, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9000, 'retained_headers': 'MAC_VLAN', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_18}  ${afm_23}  ${afm_53}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-MPLS-NPG-2678
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+9, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'retained_headers': 'MAC_VLAN_MPLS', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_02}  ${afm_26}  ${afm_54}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-MPLS-NPG-2679
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+16342, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'retained_headers': 'MAC_VLAN_MPLS', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_13}  ${afm_36}  ${afm_53}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-MPLS-NPG-72620
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+1500, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 1500, 'retained_headers': 'MAC_VLAN_MPLS', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_06}  ${afm_33}  ${afm_42}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-MPLS-NPG-72621
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+9000, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9000, 'retained_headers': 'MAC_VLAN_MPLS', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_12}  ${afm_21}  ${afm_45}  ${afm_56}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-NPG-2693
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+9, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_11}  ${afm_28}  ${afm_45}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-NPG-2694
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+16342, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': ${maxRetainedBytes}, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_10}  ${afm_31}  ${afm_46}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-NPG-102638
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+1500, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 1500, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_09}  ${afm_31}  ${afm_41}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-NPG-102639
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+9000, NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9000, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_03}  ${afm_36}  ${afm_52}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-TPG-2652
    #Description: Test packet trimming Retain MAC+9 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC', 'retained_bytes': 9}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_07}  ${afm_11}  ${afm_27}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Filter': {'f1':[ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Tool': {'tpg1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-TPG-22587
    #Description: Test packet trimming Retain MAC+16342 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC', 'retained_bytes': ${maxRetainedBytes}}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_08}  ${afm_38}  ${afm_41}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_08' ,  'afm_38' ,  'afm_41']}, 'Filter': {'f1':[ 'afm_08' ,  'afm_38' ,  'afm_41']}, 'Tool': {'tpg1': [ 'afm_08' ,  'afm_38' ,  'afm_41']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-TPG-22588
    #Description: Test packet trimming Retain MAC+1500 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC', 'retained_bytes': 1500}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_02}  ${afm_16}  ${afm_36}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_02' ,  'afm_16' ,  'afm_36']}, 'Filter': {'f1':[ 'afm_02' ,  'afm_16' ,  'afm_36']}, 'Tool': {'tpg1': [ 'afm_02' ,  'afm_16' ,  'afm_36']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-TPG-22589
    #Description: Test packet trimming Retain MAC+9000 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC', 'retained_bytes': 9000}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_15}  ${afm_31}  ${afm_47}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_15' ,  'afm_31' ,  'afm_47']}, 'Filter': {'f1':[ 'afm_15' ,  'afm_31' ,  'afm_47']}, 'Tool': {'tpg1': [ 'afm_15' ,  'afm_31' ,  'afm_47']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-TPG-2667
    #Description: Test packet trimming Retain MAC+VLAN+9 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN', 'retained_bytes': 9}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_06}  ${afm_37}  ${afm_52}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_06' ,  'afm_37' ,  'afm_52']}, 'Filter': {'f1':[ 'afm_06' ,  'afm_37' ,  'afm_52']}, 'Tool': {'tpg1': [ 'afm_06' ,  'afm_37' ,  'afm_52']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-TPG-52605
    #Description: Test packet trimming Retain MAC+VLAN+16342 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN', 'retained_bytes': ${maxRetainedBytes}}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_13}  ${afm_26}  ${afm_53}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_13' ,  'afm_26' ,  'afm_53']}, 'Filter': {'f1':[ 'afm_13' ,  'afm_26' ,  'afm_53']}, 'Tool': {'tpg1': [ 'afm_13' ,  'afm_26' ,  'afm_53']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-TPG-52606
    #Description: Test packet trimming Retain MAC+VLAN+1500 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN', 'retained_bytes': 1500}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_16}  ${afm_22}  ${afm_51}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_16' ,  'afm_22' ,  'afm_51']}, 'Filter': {'f1':[ 'afm_16' ,  'afm_22' ,  'afm_51']}, 'Tool': {'tpg1': [ 'afm_16' ,  'afm_22' ,  'afm_51']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-TPG-52607
    #Description: Test packet trimming Retain MAC+VLAN+9000 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN', 'retained_bytes': 9000}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_05}  ${afm_36}  ${afm_42}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_05' ,  'afm_36' ,  'afm_42']}, 'Filter': {'f1':[ 'afm_05' ,  'afm_36' ,  'afm_42']}, 'Tool': {'tpg1': [ 'afm_05' ,  'afm_36' ,  'afm_42']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-TPG-2682
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+9 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS', 'retained_bytes': 9}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_16}  ${afm_33}  ${afm_51}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_16' ,  'afm_33' ,  'afm_51']}, 'Filter': {'f1':[ 'afm_16' ,  'afm_33' ,  'afm_51']}, 'Tool': {'tpg1': [ 'afm_16' ,  'afm_33' ,  'afm_51']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-TPG-82623
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+16342 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS', 'retained_bytes': ${maxRetainedBytes}}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_07}  ${afm_29}  ${afm_46}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_07' ,  'afm_29' ,  'afm_46']}, 'Filter': {'f1':[ 'afm_07' ,  'afm_29' ,  'afm_46']}, 'Tool': {'tpg1': [ 'afm_07' ,  'afm_29' ,  'afm_46']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-TPG-82624
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+1500 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS', 'retained_bytes': 1500}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_20}  ${afm_21}  ${afm_48}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_20' ,  'afm_21' ,  'afm_48']}, 'Filter': {'f1':[ 'afm_20' ,  'afm_21' ,  'afm_48']}, 'Tool': {'tpg1': [ 'afm_20' ,  'afm_21' ,  'afm_48']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-TPG-82625
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+9000 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS', 'retained_bytes': 9000}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_01}  ${afm_27}  ${afm_60}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_01' ,  'afm_27' ,  'afm_60']}, 'Filter': {'f1':[ 'afm_01' ,  'afm_27' ,  'afm_60']}, 'Tool': {'tpg1': [ 'afm_01' ,  'afm_27' ,  'afm_60']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-TPG-2697
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+9 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'retained_bytes': 9}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_08}  ${afm_21}  ${afm_46}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_08' ,  'afm_21' ,  'afm_46']}, 'Filter': {'f1':[ 'afm_08' ,  'afm_21' ,  'afm_46']}, 'Tool': {'tpg1': [ 'afm_08' ,  'afm_21' ,  'afm_46']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-TPG-112641
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+16342 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'retained_bytes': ${maxRetainedBytes}}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_17}  ${afm_40}  ${afm_52}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_17' ,  'afm_40' ,  'afm_52']}, 'Filter': {'f1':[ 'afm_17' ,  'afm_40' ,  'afm_52']}, 'Tool': {'tpg1': [ 'afm_17' ,  'afm_40' ,  'afm_52']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}



AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-TPG-112642
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+1500 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'retained_bytes': 1500}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_16}  ${afm_23}  ${afm_55}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_16' ,  'afm_23' ,  'afm_55']}, 'Filter': {'f1':[ 'afm_16' ,  'afm_23' ,  'afm_55']}, 'Tool': {'tpg1': [ 'afm_16' ,  'afm_23' ,  'afm_55']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-VLAN-MPLS-L3-TPG-112643
    #Description: Test packet trimming Retain MAC+VLAN+MPLS+L3+9000 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap

    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC_VLAN_MPLS_L3', 'retained_bytes': 9000}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_15}  ${afm_22}  ${afm_41}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${tpg1TrimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    #${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_15' ,  'afm_22' ,  'afm_41']}, 'Filter': {'f1':[ 'afm_15' ,  'afm_22' ,  'afm_41']}, 'Tool': {'tpg1': [ 'afm_15' ,  'afm_22' ,  'afm_41']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}


AFM-PKT-TRIM-MAC-NPG-12584-2PORTS
    #Description: Test packet trimming Retain MAC+1500, NPG, 2 ports in a NPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig1.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig1.pcap
    ${origCaptureFile2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig2.enc
    ${origCapturePcap2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig2.pcap
    ${concatenatedCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig_concatenated.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_headers': 'MAC', 'retained_bytes': 1500, 'enabled': True}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    @{pgPortList}=  Create List  ${np1}  ${np2}
    ${noOfPackets}=  Evaluate  4 * ${noOfPacketsInStreams}
    ${noOfPacketsPerNetwork}=  Evaluate    2 * ${noOfPacketsInStreams}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${pgPortList}
    AddConn     ${nto1}  c1  ${npg1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${npg1}  ${bwAllocation}
    npg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_11}  ${afm_01}
    np2Ixia.addIxStreams   ${afm_41}  ${afm_42}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    np2Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    #sleep  1s
    np2Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPacketsPerNetwork}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np2Ixia.exportIxFramesToFile  ${origCaptureFile2}  1  ${noOfPacketsPerNetwork}
    convertEncToPcap  ${origCaptureFile2}  ${origCapturePcap2}

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}

    np1Ixia.disableInternalLoopback
    np2Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    @{captureList}  create list  ${origCapturePcap}  ${origCapturePcap2}
    concatenateCaptures  ${captureList}  ${concatenatedCapturePcap}
    ${trimmedFrames}=  np1.pktProc.emulate  ${pp2}  pcapFile=${concatenatedCapturePcap}  verbose=${False}
    compareFrames  ${trimmedFrames}  ${tp1Ixia.recdFrames}   ordered=${False}
    ##${statsMap}=  Catenate  {'Network': {'npg1': [ 'afm_06', 'afm_12', 'afm_21']}, 'Filter': {'f1':[ 'afm_06', 'afm_12', 'afm_21']}, 'Tool': {'tp1': [ 'afm_06', 'afm_12', 'afm_21']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${npg1}
    Remove Files  ${captureFile}  ${origCaptureFile}

AFM-PKT-TRIM-MAC-LBPG-32586_2PORTS
    #Description: Test packet trimming Retain MAC+9 LBPG, with 2 ports in a LBPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${captureFile2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim2.enc
    ${capturePcap2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim2.pcap
    ${pp2}=    Catenate  {"trim_settings": {'retained_bytes': 9, 'enabled': True, 'retained_headers': 'MAC'}}
    ${pg1}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    @{pgPortList}=  Create List  ${tp1}  ${tp2}
    ${noOfPackets}=  EVALUATE  2*${noOfPacketsInStreams}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  lbpg1  ${pg1}  ${pgPortList}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${lbpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${lbpg1}  ${bwAllocation}
    lbpg1.configurePacketProcessing  ${pp2}

    np1Ixia.addIxStreams   ${afm_16}  ${afm_23}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    tp2Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp2Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}

    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPacketsInStreams}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp2Ixia.exportIxFramesToFile  ${captureFile2}  1  ${noOfPacketsInStreams}
    convertEncToPcap  ${captureFile2}  ${capturePcap2}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    tp2Ixia.scapyImportPcap  ${capturePcap2}

    ${lbpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${True}
    ${combinedReceivedFrames}=  combine lists  ${tp1Ixia.recdFrames}  ${tp2Ixia.recdFrames}

    compareFrames  ${lbpg1TrimmedFrames}  ${combinedReceivedFrames}   ordered=${False}
    ##${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Filter': {'f1':[ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Tool': {'lbpg1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${lbpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}  ${captureFile2}


AFM-PKT-TRIM-MAC-TPG-2652_2PORTS
    #Description: Test packet trimming Retain MAC+9 TPG
    #[Tags]  skip
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_orig.pcap
    ${captureFile2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim2.enc
    ${capturePcap2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/packet_trim2.pcap
    ${pp2}=    Catenate  {"trim_settings": {'enabled': True, 'retained_headers': 'MAC', 'retained_bytes': 9}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    @{pgPortList}=  Create List  ${tp1}  ${tp2}
    ${noOfPackets}=  EVALUATE  2*${noOfPacketsInStreams}

    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${pgPortList}
    AddConn     ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    afm1.attachResource  ${tpg1}  ${bwAllocation}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams   ${afm_07}  ${afm_16}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    tp2Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    tp2Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPacketsInStreams}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp2Ixia.exportIxFramesToFile  ${captureFile2}  1  ${noOfPacketsInStreams}
    convertEncToPcap  ${captureFile2}  ${capturePcap2}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    tp2Ixia.scapyImportPcap  ${capturePcap2}
    ${tpg1TrimmedFrames}=  tp1.pktProc.emulate  ${pp2}  pcapFile=${origCapturePcap}  verbose=${False}
    ${combinedReceivedFrames}=  combine lists  ${tp1Ixia.recdFrames}  ${tp2Ixia.recdFrames}
    compareFrames  ${tpg1TrimmedFrames}  ${combinedReceivedFrames}   ordered=${False}
    ##${statsMap}=  Catenate  {'Network': {'np1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Filter': {'f1':[ 'afm_07' ,  'afm_11' ,  'afm_27']}, 'Tool': {'lbpg1': [ 'afm_07' ,  'afm_11' ,  'afm_27']}}
    ##nto1.stats.analyzeNVSStats  ${statsMap}
    afm1.detachResource  ${tpg1}
    Remove Files  ${captureFile}  ${origCaptureFile}  ${captureFile2}


*** Keywords ***

createStreams
    ${afm_37}=   createStream  afm_37  Ether(src='00:11:11:11:00:37',dst='00:22:22:22:00:37')/Dot1Q(vlan=137)/MPLS(label=1037)/IPv6(src='1000::37', dst='2000::37')/UDP(sport=1037, dport=2037)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_11}=   createStream  afm_11  Ether(src='00:11:11:11:00:11',dst='00:22:22:22:00:11')/IP(src='111.111.111.11', dst='222.222.222.11')/UDP(sport=1011, dport=2011)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_41}=   createStream  afm_41  Ether(src='00:11:11:11:00:41',dst='00:22:22:22:00:41')/Dot1Q(vlan=141)/Dot1Q(vlan=241)/IP(src='111.111.111.41', dst='222.222.222.41')/UDP(sport=1041, dport=2041)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_46}=   createStream  afm_46  Ether(src='00:11:11:11:00:46',dst='00:22:22:22:00:46')/Dot1Q(vlan=146)/Dot1Q(vlan=246)/IPv6(src='1000::46', dst='2000::46')/UDP(sport=1046, dport=2046)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_45}=   createStream  afm_45  Ether(src='00:11:11:11:00:45',dst='00:22:22:22:00:45')/Dot1Q(vlan=145)/Dot1Q(vlan=245)/MPLS(label=1045)/MPLS(label=2045)/MPLS(label=3045)/MPLS(label=4045)/MPLS(label=5045)/MPLS(label=6045)/MPLS(label=7045)/MPLS(label=8045)/IP(src='111.111.111.45', dst='222.222.222.45')/UDP(sport=1045, dport=2045)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_57}=   createStream  afm_57  Ether(src='00:11:11:11:00:57',dst='00:22:22:22:00:57')/MPLS(label=1057)/MPLS(label=2057)/IPv6(src='1000::57', dst='2000::57')/UDP(sport=1057, dport=2057)  payload=ones  frameSize=1400  numFrames=${noOfPacketsInStreams}
    ${afm_38}=   createStream  afm_38  Ether(src='00:11:11:11:00:38',dst='00:22:22:22:00:38')/Dot1Q(vlan=138)/MPLS(label=1038)/MPLS(label=2038)/IPv6(src='1000::38', dst='2000::38')/UDP(sport=1038, dport=2038)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_23}=   createStream  afm_23  Ether(src='00:11:11:11:00:23',dst='00:22:22:22:00:23')/Dot1Q(vlan=123)/MPLS(label=1023)/MPLS(label=2023)/IP(src='111.111.111.23', dst='222.222.222.23')/UDP(sport=1023, dport=2023)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_01}=   createStream  afm_01  Ether(src='00:11:11:11:00:01',dst='00:22:22:22:00:01')/IP(src='111.111.111.01', dst='222.222.222.01')/UDP(sport=1001, dport=2001)  payload=ones  frameSize=64  numFrames=${noOfPacketsInStreams}
    ${afm_42}=   createStream  afm_42  Ether(src='00:11:11:11:00:42',dst='00:22:22:22:00:42')/Dot1Q(vlan=142)/Dot1Q(vlan=242)/MPLS(label=1042)/IP(src='111.111.111.42', dst='222.222.222.42')/UDP(sport=1042, dport=2042)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_58}=   createStream  afm_58  Ether(src='00:11:11:11:00:58',dst='00:22:22:22:00:58')/Dot1Q(vlan=158)/MPLS(label=1058)/IP(src='111.111.111.58', dst='222.222.222.58')/UDP(sport=1058, dport=2058)  payload=ones  frameSize=1500  numFrames=${noOfPacketsInStreams}
    ${afm_29}=   createStream  afm_29  Ether(src='00:11:11:11:00:29',dst='00:22:22:22:00:29')/Dot1Q(vlan=129)/MPLS(label=1029)/MPLS(label=2029)/MPLS(label=3029)/MPLS(label=4029)/MPLS(label=5029)/MPLS(label=6029)/MPLS(label=7029)/IPv6(src='1000::29', dst='2000::29')/UDP(sport=1029, dport=2029)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_27}=   createStream  afm_27  Ether(src='00:11:11:11:00:27',dst='00:22:22:22:00:27')/Dot1Q(vlan=127)/MPLS(label=1027)/IPv6(src='1000::27', dst='2000::27')/UDP(sport=1027, dport=2027)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_03}=   createStream  afm_03  Ether(src='00:11:11:11:00:03',dst='00:22:22:22:00:03')/MPLS(label=1003)/MPLS(label=2003)/IP(src='111.111.111.03', dst='222.222.222.03')/UDP(sport=1003, dport=2003)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_16}=   createStream  afm_16  Ether(src='00:11:11:11:00:16',dst='00:22:22:22:00:16')/IPv6(src='1000::16', dst='2000::16')/UDP(sport=1016, dport=2016)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_22}=   createStream  afm_22  Ether(src='00:11:11:11:00:22',dst='00:22:22:22:00:22')/Dot1Q(vlan=122)/MPLS(label=1022)/IP(src='111.111.111.22', dst='222.222.222.22')/UDP(sport=1022, dport=2022)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_54}=   createStream  afm_54  Ether(src='00:11:11:11:00:54',dst='00:22:22:22:00:54')/Dot1Q(vlan=154)/Dot1Q(vlan=254)/MPLS(label=1054)/MPLS(label=2054)/MPLS(label=3054)/MPLS(label=4054)/MPLS(label=5054)/MPLS(label=6054)/MPLS(label=7054)/IP(src='111.111.111.54', dst='222.222.222.54')/UDP(sport=1054, dport=2054)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_06}=   createStream  afm_06  Ether(src='00:11:11:11:00:06',dst='00:22:22:22:00:06')/IPv6(src='1000::06', dst='2000::06')/UDP(sport=1006, dport=2006)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_21}=   createStream  afm_21  Ether(src='00:11:11:11:00:21',dst='00:22:22:22:00:21')/Dot1Q(vlan=121)/IP(src='111.111.111.21', dst='222.222.222.21')/UDP(sport=1021, dport=2021)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_05}=   createStream  afm_05  Ether(src='00:11:11:11:00:05',dst='00:22:22:22:00:05')/MPLS(label=1005)/MPLS(label=2005)/MPLS(label=3005)/MPLS(label=4005)/MPLS(label=5005)/MPLS(label=6005)/MPLS(label=7005)/MPLS(label=8005)/IP(src='111.111.111.05', dst='222.222.222.05')/UDP(sport=1005, dport=2005)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_40}=   createStream  afm_40  Ether(src='00:11:11:11:00:40',dst='00:22:22:22:00:40')/Dot1Q(vlan=140)/MPLS(label=1040)/MPLS(label=2040)/MPLS(label=3040)/MPLS(label=4040)/MPLS(label=5040)/MPLS(label=6040)/MPLS(label=7040)/MPLS(label=8040)/IPv6(src='1000::40', dst='2000::40')/UDP(sport=1040, dport=2040)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_59}=   createStream  afm_59  Ether(src='00:11:11:11:00:59',dst='00:22:22:22:00:59')/Dot1Q(vlan=159)/Dot1Q(vlan=259)/MPLS(label=1059)/MPLS(label=2059)/MPLS(label=3059)/MPLS(label=4059)/MPLS(label=5059)/MPLS(label=6059)/MPLS(label=7059)/IPv6(src='1000::59', dst='2000::59')/UDP(sport=1059, dport=2059)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_48}=   createStream  afm_48  Ether(src='00:11:11:11:00:48',dst='00:22:22:22:00:48')/Dot1Q(vlan=148)/Dot1Q(vlan=248)/MPLS(label=1048)/MPLS(label=2048)/IPv6(src='1000::48', dst='2000::48')/UDP(sport=1048, dport=2048)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_12}=   createStream  afm_12  Ether(src='00:11:11:11:00:12',dst='00:22:22:22:00:12')/MPLS(label=1012)/IP(src='111.111.111.12', dst='222.222.222.12')/UDP(sport=1012, dport=2012)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_12}=   createStream  afm_12  Ether(src='00:11:11:11:00:12',dst='00:22:22:22:00:12')/MPLS(label=1012)/IP(src='111.111.111.12', dst='222.222.222.12')/UDP(sport=1012, dport=2012)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_15}=   createStream  afm_15  Ether(src='00:11:11:11:00:15',dst='00:22:22:22:00:15')/MPLS(label=1015)/MPLS(label=2015)/MPLS(label=3015)/MPLS(label=4015)/MPLS(label=5015)/MPLS(label=6015)/MPLS(label=7015)/MPLS(label=8015)/IP(src='111.111.111.15', dst='222.222.222.15')/UDP(sport=1015, dport=2015)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_52}=   createStream  afm_52  Ether(src='00:11:11:11:00:52',dst='00:22:22:22:00:52')/Dot1Q(vlan=152)/Dot1Q(vlan=252)/MPLS(label=1052)/IP(src='111.111.111.52', dst='222.222.222.52')/UDP(sport=1052, dport=2052)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_32}=   createStream  afm_32  Ether(src='00:11:11:11:00:32',dst='00:22:22:22:00:32')/Dot1Q(vlan=132)/MPLS(label=1032)/IP(src='111.111.111.32', dst='222.222.222.32')/UDP(sport=1032, dport=2032)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_45}=   createStream  afm_45  Ether(src='00:11:11:11:00:45',dst='00:22:22:22:00:45')/Dot1Q(vlan=145)/Dot1Q(vlan=245)/MPLS(label=1045)/MPLS(label=2045)/MPLS(label=3045)/MPLS(label=4045)/MPLS(label=5045)/MPLS(label=6045)/MPLS(label=7045)/MPLS(label=8045)/IP(src='111.111.111.45', dst='222.222.222.45')/UDP(sport=1045, dport=2045)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_10}=   createStream  afm_10  Ether(src='00:11:11:11:00:10',dst='00:22:22:22:00:10')/MPLS(label=1010)/MPLS(label=2010)/MPLS(label=3010)/MPLS(label=4010)/MPLS(label=5010)/MPLS(label=6010)/MPLS(label=7010)/MPLS(label=8010)/IPv6(src='1000::10', dst='2000::10')/UDP(sport=1010, dport=2010)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_07}=   createStream  afm_07  Ether(src='00:11:11:11:00:07',dst='00:22:22:22:00:07')/MPLS(label=1007)/IPv6(src='1000::07', dst='2000::07')/UDP(sport=1007, dport=2007)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_51}=   createStream  afm_51  Ether(src='00:11:11:11:00:51',dst='00:22:22:22:00:51')/Dot1Q(vlan=151)/Dot1Q(vlan=251)/IP(src='111.111.111.51', dst='222.222.222.51')/UDP(sport=1051, dport=2051)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_20}=   createStream  afm_20  Ether(src='00:11:11:11:00:20',dst='00:22:22:22:00:20')/MPLS(label=1020)/MPLS(label=2020)/MPLS(label=3020)/MPLS(label=4020)/MPLS(label=5020)/MPLS(label=6020)/MPLS(label=7020)/MPLS(label=8020)/IPv6(src='1000::20', dst='2000::20')/UDP(sport=1020, dport=2020)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_28}=   createStream  afm_28  Ether(src='00:11:11:11:00:28',dst='00:22:22:22:00:28')/Dot1Q(vlan=128)/MPLS(label=1028)/MPLS(label=2028)/IPv6(src='1000::28', dst='2000::28')/UDP(sport=1028, dport=2028)  payload=ones  frameSize=500  numFrames=${noOfPacketsInStreams}
    ${afm_04}=   createStream  afm_04  Ether(src='00:11:11:11:00:04',dst='00:22:22:22:00:04')/MPLS(label=1004)/MPLS(label=2004)/MPLS(label=3004)/MPLS(label=4004)/MPLS(label=5004)/MPLS(label=6004)/MPLS(label=7004)/IP(src='111.111.111.04', dst='222.222.222.04')/UDP(sport=1004, dport=2004)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_31}=   createStream  afm_31  Ether(src='00:11:11:11:00:31',dst='00:22:22:22:00:31')/Dot1Q(vlan=131)/IP(src='111.111.111.31', dst='222.222.222.31')/UDP(sport=1031, dport=2031)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_09}=   createStream  afm_09  Ether(src='00:11:11:11:00:09',dst='00:22:22:22:00:09')/MPLS(label=1009)/MPLS(label=2009)/MPLS(label=3009)/MPLS(label=4009)/MPLS(label=5009)/MPLS(label=6009)/MPLS(label=7009)/IPv6(src='1000::09', dst='2000::09')/UDP(sport=1009, dport=2009)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_18}=   createStream  afm_18  Ether(src='00:11:11:11:00:18',dst='00:22:22:22:00:18')/MPLS(label=1018)/MPLS(label=2018)/IPv6(src='1000::18', dst='2000::18')/UDP(sport=1018, dport=2018)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_08}=   createStream  afm_08  Ether(src='00:11:11:11:00:08',dst='00:22:22:22:00:08')/MPLS(label=1008)/MPLS(label=2008)/IPv6(src='1000::08', dst='2000::08')/UDP(sport=1008, dport=2008)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_60}=   createStream  afm_60  Ether(src='00:11:11:11:00:60',dst='00:22:22:22:00:60')/Dot1Q(vlan=160)/Dot1Q(vlan=260)/MPLS(label=1060)/MPLS(label=2060)/MPLS(label=3060)/MPLS(label=4060)/MPLS(label=5060)/MPLS(label=6060)/MPLS(label=7060)/MPLS(label=8060)/IPv6(src='1000::60', dst='2000::60')/UDP(sport=1060, dport=2060)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_16}=   createStream  afm_16  Ether(src='00:11:11:11:00:16',dst='00:22:22:22:00:16')/IPv6(src='1000::16', dst='2000::16')/UDP(sport=1016, dport=2016)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_11}=   createStream  afm_11  Ether(src='00:11:11:11:00:11',dst='00:22:22:22:00:11')/IP(src='111.111.111.11', dst='222.222.222.11')/UDP(sport=1011, dport=2011)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_47}=   createStream  afm_47  Ether(src='00:11:11:11:00:47',dst='00:22:22:22:00:47')/Dot1Q(vlan=147)/Dot1Q(vlan=247)/MPLS(label=1047)/IPv6(src='1000::47', dst='2000::47')/UDP(sport=1047, dport=2047)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_33}=   createStream  afm_33  Ether(src='00:11:11:11:00:33',dst='00:22:22:22:00:33')/Dot1Q(vlan=133)/MPLS(label=1033)/MPLS(label=2033)/IP(src='111.111.111.33', dst='222.222.222.33')/UDP(sport=1033, dport=2033)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_36}=   createStream  afm_36  Ether(src='00:11:11:11:00:36',dst='00:22:22:22:00:36')/Dot1Q(vlan=136)/IPv6(src='1000::36', dst='2000::36')/UDP(sport=1036, dport=2036)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_02}=   createStream  afm_02  Ether(src='00:11:11:11:00:02',dst='00:22:22:22:00:02')/MPLS(label=1002)/IP(src='111.111.111.02', dst='222.222.222.02')/UDP(sport=1002, dport=2002)  payload=ones  frameSize=64  numFrames=${noOfPacketsInStreams}
    ${afm_26}=   createStream  afm_26  Ether(src='00:11:11:11:00:26',dst='00:22:22:22:00:26')/Dot1Q(vlan=126)/IPv6(src='1000::26', dst='2000::26')/UDP(sport=1026, dport=2026)  payload=ones  frameSize=9000  numFrames=${noOfPacketsInStreams}
    ${afm_50}=   createStream  afm_50  Ether(src='00:11:11:11:00:50',dst='00:22:22:22:00:50')/Dot1Q(vlan=150)/Dot1Q(vlan=250)/MPLS(label=1050)/MPLS(label=2050)/MPLS(label=3050)/MPLS(label=4050)/MPLS(label=5050)/MPLS(label=6050)/MPLS(label=7050)/IP(src='111.111.111.50', dst='222.222.222.50')/UDP(sport=1050, dport=2050)  payload=ones  frameSize=1600  numFrames=${noOfPacketsInStreams}
    ${afm_17}=   createStream  afm_17  Ether(src='00:11:11:11:00:17',dst='00:22:22:22:00:17')/MPLS(label=1017)/IPv6(src='1000::17', dst='2000::17')/UDP(sport=1017, dport=2017)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_43}=   createStream  afm_43  Ether(src='00:11:11:11:00:43',dst='00:22:22:22:00:43')/Dot1Q(vlan=143)/Dot1Q(vlan=243)/MPLS(label=1043)/MPLS(label=2043)/IP(src='111.111.111.43', dst='222.222.222.43')/UDP(sport=1043, dport=2043)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_56}=   createStream  afm_56  Ether(src='00:11:11:11:00:56',dst='00:22:22:22:00:56')/Dot1Q(vlan=156)/Dot1Q(vlan=256)/IPv6(src='1000::56', dst='2000::56')/UDP(sport=1056, dport=2056)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_53}=   createStream  afm_53  Ether(src='00:11:11:11:00:53',dst='00:22:22:22:00:53')/Dot1Q(vlan=153)/Dot1Q(vlan=253)/MPLS(label=1053)/MPLS(label=2053)/IP(src='111.111.111.53', dst='222.222.222.53')/UDP(sport=1053, dport=2053)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    ${afm_13}=   createStream  afm_13  Ether(src='00:11:11:11:00:13',dst='00:22:22:22:00:13')/MPLS(label=1013)/MPLS(label=2013)/IP(src='111.111.111.13', dst='222.222.222.13')/UDP(sport=1013, dport=2013)  payload=ones  frameSize=128  numFrames=${noOfPacketsInStreams}
    ${afm_55}=   createStream  afm_55  Ether(src='00:11:11:11:00:55',dst='00:22:22:22:00:55')/Dot1Q(vlan=155)/Dot1Q(vlan=255)/MPLS(label=1055)/MPLS(label=2055)/MPLS(label=3055)/MPLS(label=4055)/MPLS(label=5055)/MPLS(label=6055)/MPLS(label=7055)/MPLS(label=8055)/IP(src='111.111.111.55', dst='222.222.222.55')/UDP(sport=1055, dport=2055)  payload=ones  frameSize=${maxFrameLength}  numFrames=${noOfPacketsInStreams}
    set suite variable  ${afm_37}
    set suite variable  ${afm_11}
    set suite variable  ${afm_41}
    set suite variable  ${afm_46}
    set suite variable  ${afm_45}
    set suite variable  ${afm_57}
    set suite variable  ${afm_38}
    set suite variable  ${afm_23}
    set suite variable  ${afm_01}
    set suite variable  ${afm_42}
    set suite variable  ${afm_58}
    set suite variable  ${afm_29}
    set suite variable  ${afm_27}
    set suite variable  ${afm_03}
    set suite variable  ${afm_16}
    set suite variable  ${afm_22}
    set suite variable  ${afm_54}
    set suite variable  ${afm_06}
    set suite variable  ${afm_21}
    set suite variable  ${afm_05}
    set suite variable  ${afm_40}
    set suite variable  ${afm_59}
    set suite variable  ${afm_48}
    set suite variable  ${afm_12}
    set suite variable  ${afm_12}
    set suite variable  ${afm_15}
    set suite variable  ${afm_52}
    set suite variable  ${afm_32}
    set suite variable  ${afm_45}
    set suite variable  ${afm_10}
    set suite variable  ${afm_07}
    set suite variable  ${afm_51}
    set suite variable  ${afm_20}
    set suite variable  ${afm_28}
    set suite variable  ${afm_04}
    set suite variable  ${afm_31}
    set suite variable  ${afm_36}
    set suite variable  ${afm_09}
    set suite variable  ${afm_18}
    set suite variable  ${afm_08}
    set suite variable  ${afm_60}
    set suite variable  ${afm_16}
    set suite variable  ${afm_11}
    set suite variable  ${afm_47}
    set suite variable  ${afm_33}
    set suite variable  ${afm_36}
    set suite variable  ${afm_02}
    set suite variable  ${afm_26}
    set suite variable  ${afm_50}
    set suite variable  ${afm_17}
    set suite variable  ${afm_43}
    set suite variable  ${afm_56}
    set suite variable  ${afm_53}
    set suite variable  ${afm_13}
    set suite variable  ${afm_55}

setupTestEnv
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  np2  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp2  TOOL     ${myReservation['myNTO']['myPorts']}
    AddAFMR  ${nto1}  afm1  L1-AFM


setupSuiteEnv
    log   RESOURCE=${RESOURCEPOOL}   console=true
    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myRequestDef}=  Catenate  {'myPorts': {'type':'NTOPort',
    ...                                         'properties':{'ConnectionType':'standard'}},
    ...                          'myNTO': {'type': 'NTO'}}

    ${myMap}=  Catenate  {'myNTO': {'myPorts': ['np1', 'np2', 'tp1', 'tp2']}}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myReservation}=  requestResourceBlocking  ${RESOURCETIMEOUT}  ${RESOURCEPOOL}  ${myRequestDef}  requestMapParentBound=${myMap}
    Set Global Variable  ${myReservation}

    ${noOfPackets}=  evaluate    ${noOfStreams} * ${noOfPacketsInStreams}
    set suite variable  ${noOfPackets}

suiteCleanup
    releaseResources   ${myReservation['responseId']}

testCleanup
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts


