*** Settings ***
Suite Setup     setupSuiteEnv
Test Setup      setupTestEnv
Test Teardown   testCleanup
Suite Teardown  suiteCleanup


#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-02.ann.is.keysight.com
Library  ./NTO/frwk/Utils.py
Library  Collections
Library  Dialogs
Library  OperatingSystem

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
#Command line variables:   ${RESOURCEPOOL}
${RESOURCETIMEOUT}=  60
${noOfPacketsInStreams}=  100
${noOfStreams}=  16
${lineRate}=  1

*** Test Cases ***
STD-VLAN-STRIP-NP-TC000774164
# DESCRIPTION: This test verifies that 1 VLAN is stripped when VLAN Stripping with 1 count is enabled on the network port
# OWNER: ipopa
    #[TAGS]  skip
    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'ingress_count': 1, 'strip_mode': 'INGRESS'}}

    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.pcap

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    sleep  2  
    nto1.checkPortStatus

    np1.configurePacketProcessing  ${pp1}

    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}  ${stream6}  ${stream7}  ${stream8}  ${stream9}  ${stream10}  ${stream11}  ${stream12}  ${stream13}  ${stream14}  ${stream15}  ${stream16}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    log   Get statistics from the device
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats

    #set expected statistics for port and filter
    ${originalNoOfBytes}=  evaluate    2270 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    2210 * ${noOfPacketsInStreams}

    compareStatsOnNetwork   ${np1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}
    compareStatsOnTool   ${tp1ActualStats}  ${noOfPackets}  ${noOfPackets}  ${noOfBytesAfterStripping}
    compareStatsOnFilter  ${f1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}

STD-VLAN-STRIP-NPG-TC000774165
# DESCRIPTION: This test verifies that 1 VLAN is stripped when VLAN Stripping with count 1 is enabled on the interconnect network port group
# TEST-TRANSLATION-MODE: exclude BUFFALO
    #[TAGS]  skip

    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'ingress_count': 1, 'strip_mode': 'INGRESS'}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.pcap

    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddConn    ${nto1}  c1  ${npg1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    sleep  2  
    nto1.checkPortStatus
    npg1.configurePacketProcessing  ${pp1}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}  ${stream6}  ${stream7}  ${stream8}  ${stream9}  ${stream10}  ${stream11}  ${stream12}  ${stream13}  ${stream14}  ${stream15}  ${stream16}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}

    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    log   Get statistics from the device
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats
    ${originalNoOfBytes}=  evaluate    2270 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    2210 * ${noOfPacketsInStreams}
    compareStatsOnNetwork   ${np1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}
    compareStatsOnTool   ${tp1ActualStats}  ${noOfPackets}  ${noOfPackets}  ${noOfBytesAfterStripping}
    compareStatsOnFilter  ${f1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}

STD-VLAN-STRIP-NP-TP-TC000774167
# DESCRIPTION: VLAN Stripping enabled with count 1 on network port and with one count on the tool port
    #[TAGS]  skip

    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'ingress_count': 1, 'strip_mode': 'INGRESS'} }
    ${pp2}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'egress_count': 1, 'strip_mode': 'EGRESS'}}
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.pcap

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    sleep  2  
    nto1.checkPortStatus
    np1.configurePacketProcessing  ${pp1}
    tp1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}  ${stream6}  ${stream7}  ${stream8}  ${stream9}  ${stream10}  ${stream11}  ${stream12}  ${stream13}  ${stream14}  ${stream15}  ${stream16}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames1}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    ${strippedFrames2}=  tp1.pktProc.emulate  ${pp2}  frames=${strippedFrames1}  verbose=${True}
    compareFrames  ${strippedFrames2}  ${tp1Ixia.recdFrames}   ordered=${True}
    log   Get statistics from the device
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats

    #set expected statistics for port and filter
    ${originalNoOfBytes}=  evaluate    2270 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    2174 * ${noOfPacketsInStreams}
    compareStatsOnNetwork   ${np1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}
    compareStatsOnTool   ${tp1ActualStats}  ${noOfPackets}  ${noOfPackets}  ${noOfBytesAfterStripping}
    compareStatsOnFilter  ${f1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}


STD-VLAN-STRIP-NPG-TPG-TC000774166
# DESCRIPTION: This test verifies that 1 VLAN is stripped when VLAN Stripping with count 1 is enabled on interconnect network port group and count 1 on interconnect tool port group
    #[TAGS]  skip

    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'ingress_count': 1, 'strip_mode': 'INGRESS'}}
    ${pp2}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'egress_count': 1, 'strip_mode': 'EGRESS'}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    ${pg2}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.pcap

    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddPortGrp  ${nto1}  tpg1  ${pg2}  ${tp1}
    AddConn    ${nto1}  c1  ${npg1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    sleep  2  
    nto1.checkPortStatus
    npg1.configurePacketProcessing  ${pp1}
    tpg1.configurePacketProcessing  ${pp2}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}  ${stream6}  ${stream7}  ${stream8}  ${stream9}  ${stream10}  ${stream11}  ${stream12}  ${stream13}  ${stream14}  ${stream15}  ${stream16}
    #np1Ixia.addIxStreams    ${stream5}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames1}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    ${strippedFrames2}=  tp1.pktProc.emulate  ${pp2}  frames=${strippedFrames1}  verbose=${True}
    compareFrames  ${strippedFrames2}  ${tp1Ixia.recdFrames}   ordered=${True}

    log   Get statistics from the device
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats

    ${originalNoOfBytes}=  evaluate    2270 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    2174 * ${noOfPacketsInStreams}
    compareStatsOnNetwork   ${np1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}
    compareStatsOnTool   ${tp1ActualStats}  ${noOfPackets}  ${noOfPackets}  ${noOfBytesAfterStripping}
    compareStatsOnFilter  ${f1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}


STD-VLAN-STRIP-TP-TC000787035
# DESCRIPTION: This test verifies that 1 VLAN is stripped when VLAN Stripping with 1 count is enabled on the tool port
    #[TAGS]  skip

    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'egress_count': 1, 'strip_mode': 'EGRESS'}}
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.pcap

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    sleep  2  
    nto1.checkPortStatus
    tp1.configurePacketProcessing  ${pp1}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}  ${stream6}  ${stream7}  ${stream8}  ${stream9}  ${stream10}  ${stream11}  ${stream12}  ${stream13}  ${stream14}  ${stream15}  ${stream16}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture

    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}

    ${strippedFrames}=  tp1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats

    ${originalNoOfBytes}=  evaluate    2270 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    2250 * ${noOfPacketsInStreams}
    compareStatsOnNetwork   ${np1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}
    compareStatsOnTool   ${tp1ActualStats}  ${noOfPackets}  ${noOfPackets}  ${noOfBytesAfterStripping}
    compareStatsOnFilter  ${f1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}


STD-VLAN-STRIP-TPG-TC000787045
# DESCRIPTION: This test verifies that VLAN Stripping is enabled on an interconnect tool port group with count 1
    #[TAGS]  skip

    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'egress_count': 1, 'strip_mode': 'EGRESS'}}
    ${pg1}=  catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.pcap

    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn  ${nto1}  c1  ${np1}  ${f1}  ${tpg1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    sleep  2  
    nto1.checkPortStatus
    tpg1.configurePacketProcessing  ${pp1}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}  ${stream6}  ${stream7}  ${stream8}  ${stream9}  ${stream10}  ${stream11}  ${stream12}  ${stream13}  ${stream14}  ${stream15}  ${stream16}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1   ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1   ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    tp1Ixia.scapyImportPcap  ${capturePcap}

    ${strippedFrames}=  tp1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    log   Get statistics from the device
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats
    ${originalNoOfBytes}=  evaluate    2270 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    2250 * ${noOfPacketsInStreams}
    compareStatsOnNetwork   ${np1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}
    compareStatsOnTool   ${tp1ActualStats}  ${noOfPackets}  ${noOfPackets}  ${noOfBytesAfterStripping}
    compareStatsOnFilter  ${f1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}


STD-VLAN-STRIP-TPG-TC000787046
# DESCRIPTION: This test verifies that VLAN Stripping is enabled with count 2 on an interconnect tool port group
    #[TAGS]  skip

    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'egress_count': 2, 'strip_mode': 'EGRESS'}}
    ${pg1}=  catenate  {'type': 'INTERCONNECT', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}

    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.pcap

    AddFilter  ${nto1}  f1  PASS_ALL
    AddPortGrp  ${nto1}  tpg1  ${pg1}  ${tp1}
    AddConn  ${nto1}  c1  ${np1}  ${f1}  ${tpg1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    nto1.checkPortStatus
    sleep  2  
    tpg1.configurePacketProcessing  ${pp1}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}  ${stream6}  ${stream7}  ${stream8}  ${stream9}  ${stream10}  ${stream11}  ${stream12}  ${stream13}  ${stream14}  ${stream15}  ${stream16}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  tp1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    log   Get statistics from the device
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats

    #set expected statistics for port and filter

    ${originalNoOfBytes}=  evaluate    2270 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    2238 * ${noOfPacketsInStreams}
    compareStatsOnNetwork   ${np1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}
    compareStatsOnTool   ${tp1ActualStats}  ${noOfPackets}  ${noOfPackets}  ${noOfBytesAfterStripping}
    compareStatsOnFilter  ${f1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}


STD-VLAN-STRIP-NP-TC000787036
#DESCRIPTION: This test verifies that 2 VLANs are stripped when VLAN Stripping with count 2 is enabled on interconnect tool port group
    #[TAGS]  skip

    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'egress_count': 2, 'strip_mode': 'EGRESS'}}
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.pcap

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    sleep  2  
    nto1.checkPortStatus
    tp1.configurePacketProcessing  ${pp1}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}  ${stream6}  ${stream7}  ${stream8}  ${stream9}  ${stream10}  ${stream11}  ${stream12}  ${stream13}  ${stream14}  ${stream15}  ${stream16}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}

    ${strippedFrames}=  tp1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    log   Get statistics from the device
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats
    #set expected statistics for port and filter
    ${originalNoOfBytes}=  evaluate    2270 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    2238 * ${noOfPacketsInStreams}
    compareStatsOnNetwork   ${np1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}
    compareStatsOnTool   ${tp1ActualStats}  ${noOfPackets}  ${noOfPackets}  ${noOfBytesAfterStripping}
    compareStatsOnFilter  ${f1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}


STD-VLAN-STRIP-NP-TC000774172
# DESCRIPTION: This test verifies that 2 VLANs are stripped when VLAN Stripping with 2 count is enabled on the network port
    #[TAGS]  skip


    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'ingress_count': 2, 'strip_mode': 'Ingress'}}
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.pcap

    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${tp1}
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    sleep  2  
    nto1.checkPortStatus
    np1.configurePacketProcessing  ${pp1}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}  ${stream6}  ${stream7}  ${stream8}  ${stream9}  ${stream10}  ${stream11}  ${stream12}  ${stream13}  ${stream14}  ${stream15}  ${stream16}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}

    np1Ixia.enableInternalLoopback

    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture

    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}

    log   Get statistics from the device
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats

    #set expected statistics for port and filter

    ${originalNoOfBytes}=  evaluate    2270 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    2174 * ${noOfPacketsInStreams}
    compareStatsOnNetwork   ${np1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}
    compareStatsOnTool   ${tp1ActualStats}  ${noOfPackets}  ${noOfPackets}  ${noOfBytesAfterStripping}
    compareStatsOnFilter  ${f1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}


STD-VLAN-STRIP-NPG-TC000774173
# DESCRIPTION: This test verifies that 2 VLANs are stripped when VLAN Stripping with 2 count is enabled on the interconnect network port group
    #[TAGS]  skip

    ${pp1}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'ingress_count': 2, 'strip_mode': 'Ingress'}}
    ${pg1}=  Catenate  {'type': 'INTERCONNECT', 'mode': 'NETWORK', 'failover_mode': 'REBALANCE'}
    createStreams
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    ${captureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.enc
    ${capturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_strip.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/vlan_orig.pcap

    AddPortGrp  ${nto1}  npg1  ${pg1}  ${np1}
    AddFilter  ${nto1}  f1  PASS_ALL
    AddConn    ${nto1}  c1  ${npg1}  ${f1}  ${tp1}

    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts
    sleep  2  
    nto1.checkPortStatus
    npg1.configurePacketProcessing  ${pp1}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}  ${stream3}  ${stream4}  ${stream5}  ${stream6}  ${stream7}  ${stream8}  ${stream9}  ${stream10}  ${stream11}  ${stream12}  ${stream13}  ${stream14}  ${stream15}  ${stream16}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams  percentPktRate=${lineRate}
    np1Ixia.enableInternalLoopback
    tp1Ixia.startIxCapture
    np1Ixia.runIxTraffic
    tp1Ixia.stopIxCapture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  ${noOfPackets}
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    tp1Ixia.exportIxFramesToFile  ${captureFile}  1  ${noOfPackets}
    convertEncToPcap  ${captureFile}  ${capturePcap}
    np1Ixia.disableInternalLoopback
    tp1Ixia.scapyImportPcap  ${capturePcap}
    ${strippedFrames}=  np1.pktProc.emulate  ${pp1}  pcapFile=${origCapturePcap}  verbose=${True}
    compareFrames  ${strippedFrames}  ${tp1Ixia.recdFrames}   ordered=${True}
    log   Get statistics from the device
    ${np1ActualStats} =  np1.getPortStats  statName=all
    ${tp1ActualStats} =  tp1.getPortStats  statName=all
    ${f1ActualStats} =  f1.getFilterStats

    #set expected statistics for port and filter

    ${originalNoOfBytes}=  evaluate    2270 * ${noOfPacketsInStreams}
    ${noOfBytesAfterStripping}=  evaluate    2174 * ${noOfPacketsInStreams}
    compareStatsOnNetwork   ${np1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}
    compareStatsOnTool   ${tp1ActualStats}  ${noOfPackets}  ${noOfPackets}  ${noOfBytesAfterStripping}
    compareStatsOnFilter  ${f1ActualStats}  ${noOfPackets}   ${originalNoOfBytes}  ${noOfPackets}   ${originalNoOfBytes}

*** Keywords ***

createStreams
    ${stream1}=  createStream  s1  Ether(src='00:11:11:00:00:01',dst='00:22:22:00:00:01')/IP(src='10.10.10.1', dst='20.20.20.1')/TCP()  payload=ones  frameSize=100  numFrames=${noOfPacketsInStreams}
    ${stream2}=  createStream  s2  Ether(src='00:11:11:00:00:02',dst='00:22:22:00:00:02', type=0x8100)/Dot1Q(vlan=102)/IP(src='10.10.10.2', dst='20.20.20.2')/TCP()  payload=ones  frameSize=105  numFrames=${noOfPacketsInStreams}
    ${stream3}=  createStream  s3  Ether(src='00:11:11:00:00:03',dst='00:22:22:00:00:03', type=0x9100)/Dot1Q(vlan=103)/IP(src='10.10.10.3', dst='20.20.20.3')/UDP()  payload=ones  frameSize=115  numFrames=${noOfPacketsInStreams}
    ${stream4}=  createStream  s4  Ether(src='00:11:11:00:00:04',dst='00:22:22:00:00:04', type=0x88a8)/Dot1Q(vlan=104)/IP(src='10.10.10.4', dst='20.20.20.4')/TCP()  payload=zeros  frameSize=120  numFrames=${noOfPacketsInStreams}

    ${stream5}=  createStream  s5  Ether(src='00:11:11:00:00:05',dst='00:22:22:00:00:05', type=0x8100)/Dot1Q(vlan=105)/Dot1Q(vlan=205)/IP(src='10.10.10.5', dst='20.20.20.5')/TCP()  payload=ones  frameSize=125  numFrames=${noOfPacketsInStreams}
    ${stream6}=  createStream  s6  Ether(src='00:11:11:00:00:06',dst='00:22:22:00:00:06', type=0x9100)/Dot1Q(vlan=106)/Dot1Q(vlan=206)/IP(src='10.10.10.6', dst='20.20.20.6')/TCP()  payload=ones  frameSize=130  numFrames=${noOfPacketsInStreams}
    ${stream7}=  createStream  s7  Ether(src='00:11:11:00:00:07',dst='00:22:22:00:00:07', type=0x88a8)/Dot1Q(vlan=107)/Dot1Q(vlan=207)/IP(src='10.10.10.7', dst='20.20.20.7')/TCP()  payload=ones  frameSize=135  numFrames=${noOfPacketsInStreams}

    ${stream8}=  createStream  s8  Ether(src='00:11:11:00:00:08',dst='00:22:22:00:00:08', type=0x8100)/Dot1Q(vlan=108)/IPv6(src='2001:1111:2222:3333:4454:5555:7777:0008', dst='3ffe:1944:100:a:bc:2500:d0b:0008')/TCP()  payload=ones  frameSize=140  numFrames=${noOfPacketsInStreams}
    ${stream9}=  createStream  s9  Ether(src='00:11:11:00:00:09',dst='00:22:22:00:00:09', type=0x9100)/Dot1Q(vlan=109)/IPv6(src='2001:1111:2222:3333:4454:5555:7777:0009', dst='3ffe:1944:100:a:bc:2500:d0b:0009')/TCP()  payload=ones  frameSize=145  numFrames=${noOfPacketsInStreams}
    ${stream10}=  createStream  s10  Ether(src='00:11:11:00:00:0A',dst='00:22:22:00:00:0A', type=0x88a8)/Dot1Q(vlan=110)/IPv6(src='2001:1111:2222:3333:4454:5555:7777:000A', dst='3ffe:1944:100:a:bc:2500:d0b:000A')/TCP()  payload=ones  frameSize=150  numFrames=${noOfPacketsInStreams}

    ${stream11}=  createStream  s11  Ether(src='00:11:11:00:00:0B',dst='00:22:22:00:00:0B',type=0x8100)/Dot1Q(vlan=111)/Dot1Q(vlan=211)/IPv6(src='2001:1111:2222:3333:4454:5555:7777:000B', dst='3ffe:1944:100:a:bc:2500:d0b:000B')/TCP()  payload=ones  frameSize=155  numFrames=${noOfPacketsInStreams}
    ${stream12}=  createStream  s12  Ether(src='00:11:11:00:00:0C',dst='00:22:22:00:00:0C',type=0x9100)/Dot1Q(vlan=112)/Dot1Q(vlan=212)/IPv6(src='2001:1111:2222:3333:4454:5555:7777:000C', dst='3ffe:1944:100:a:bc:2500:d0b:000C')/TCP()  payload=ones  frameSize=160  numFrames=${noOfPacketsInStreams}
    ${stream13}=  createStream  s13  Ether(src='00:11:11:00:00:0D',dst='00:22:22:00:00:0D',type=0x88a8)/Dot1Q(vlan=113)/Dot1Q(vlan=213)/IPv6(src='2001:1111:2222:3333:4454:5555:7777:000D', dst='3ffe:1944:100:a:bc:2500:d0b:000D')/TCP()  payload=ones  frameSize=165  numFrames=${noOfPacketsInStreams}

    ${stream14}=  createStream  s14  Ether(src='00:11:11:00:00:0E',dst='00:22:22:00:00:0E',type=0x8100)/Dot1Q(vlan=114)/Dot1Q(vlan=216)/Dot1Q(vlan=316)/IP(src='10.10.10.14', dst='20.20.20.14')/TCP()  payload=ones  frameSize=170  numFrames=${noOfPacketsInStreams}
    ${stream15}=  createStream  s15  Ether(src='00:11:11:00:00:0F',dst='00:22:22:00:00:0F',type=0x9100)/Dot1Q(vlan=115)/Dot1Q(vlan=217)/Dot1Q(vlan=317)/IP(src='10.10.10.15', dst='20.20.20.15')/TCP()  payload=ones  frameSize=175  numFrames=${noOfPacketsInStreams}
    ${stream16}=  createStream  s16  Ether(src='00:11:11:00:00:10',dst='00:22:22:00:00:10',type=0x88a8)/Dot1Q(vlan=116)/Dot1Q(vlan=218)/Dot1Q(vlan=318)/IP(src='10.10.10.16', dst='20.20.20.16')/TCP()  payload=ones  frameSize=180  numFrames=${noOfPacketsInStreams}
     set suite variable  ${stream1}
     set suite variable  ${stream2}
     set suite variable  ${stream3}
     set suite variable  ${stream4}
     set suite variable  ${stream5}
     set suite variable  ${stream6}
     set suite variable  ${stream7}
     set suite variable  ${stream8}
     set suite variable  ${stream9}
     set suite variable  ${stream10}
     set suite variable  ${stream11}
     set suite variable  ${stream12}
     set suite variable  ${stream13}
     set suite variable  ${stream14}
     set suite variable  ${stream15}
     set suite variable  ${stream16}


setupTestEnv
    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}

setupSuiteEnv
    log   RESOURCE=${RESOURCEPOOL}   console=true
    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myRequestDef}=  Catenate  {'myPorts': {'type':'NTOPort',
    ...                                         'properties':{'ConnectionType':'standard'}},
    ...                          'myNTO': {'type': 'NTO'}}

    ${myMap}=  Catenate  {'myNTO': {'myPorts': ['np1', 'tp1']}}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myReservation}=  requestResourceBlocking  ${RESOURCETIMEOUT}  ${RESOURCEPOOL}  ${myRequestDef}  requestMapParentBound=${myMap}
    Set Global Variable  ${myReservation}

    ${noOfPackets}=  evaluate    ${noOfStreams} * ${noOfPacketsInStreams}
    set suite variable  ${noOfPackets}

suiteCleanup
    releaseResources   ${myReservation['responseId']}


testCleanup
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts