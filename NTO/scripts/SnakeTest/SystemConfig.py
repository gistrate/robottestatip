
# Reference system info
PrimaryDNSServer = "10.218.10.10"
AlternateDNSServer = "10.218.10.8"
SNMPAgentIP = "10.36.160.21"
SNMPAgentPort = 1612
SyslogServers = [ { "host" : "10.218.80.190",
                    "port" : 514,
                    "facility" : "LOCAL0"
                    },
                  { "host" : "10.218.80.193",
                    "port" : 524,
                    "facility" : "LOCAL1"
                    }
                  ]
RemoteAuthServerIP = "10.36.160.19"
RemoteRadiusSecret = "anueRadius"
ptpMasterAddr = "192.168.1.11"
ptpSlaveAddr = "192.168.1.100"
ptpSlaveGatewayAddr = "192.168.1.102"

sysConfig = { "log_level" : "INFO",
              "web_api_config" : { "enabled" : True,
                                   "port" : 8000,
                                   "token_timeout" : { "unit" : "MIN",
                                                       "value" : 100
                                                       }
                                  },
              "dns_config" : { "primary_server" : PrimaryDNSServer,
                               "alt_server" : AlternateDNSServer,
                               "suffix1" : "ixiacom.com"
                               },
              "snmp_config" : { "get_access" : [ { "community_string" : "ixiaV1",
                                                   "local_user" : None,
                                                   "version" : "V1"
                                                   },
                                                 { "community_string" : "ixiaV2",
                                                   "local_user" : None,
                                                   "version" : "V2"
                                                   },
                                                 { "community_string" : None,
                                                   "local_user" : {
                                                       "authn_protocol" : "HMAC_MD5_96",
                                                       "context_name" : None,
                                                       "engine_id" : None,
                                                       "name" : "ixiaV3requests",
                                                       "authn_password" : "ixiaV3password",
                                                       "security_level" : "AUTHN_ONLY"
                                                   },
                                                   "version" : "V3"
                                                   }
                                                 ],
                                "gets_enabled" : True,
                                "refresh_time" : 1,
                                "trap_recipients" : [ { "community_string" : "ixiaV2",
                                                        "host" : { "value" : SNMPAgentIP },
                                                        "port" : SNMPAgentPort,
                                                        "remote_user" : None,
                                                        "retry_count" : 1,
                                                        "timeout" : 5,
                                                        "traps" : [ "COLD_START",
                                                                    "WARM_START",
                                                                    "LINK_UP_DOWN",
                                                                    "AUTHENTICATION_FAILED",
                                                                    "CONSOLE_AUTHENTICATION_FAILED",
                                                                    "TX_DROPPED_PKTS",
                                                                    "RX_INVALID_PKTS",
                                                                    "RX_UTILIZATION",
                                                                    "TX_UTILIZATION",
                                                                    "ENTITY_CONFIG_CHANGE"
                                                                    ],
                                                        "version" : "V2"
                                                       },
                                                      { "community_string" : "ixiaV1",
                                                        "host" : { "value" : SNMPAgentIP },
                                                        "port" : SNMPAgentPort,
                                                        "remote_user" : None,
                                                        "retry_count" : 0,
                                                        "timeout" : 5,
                                                        "traps": ["COLD_START",
                                                                  "WARM_START",
                                                                  "LINK_UP_DOWN",
                                                                  "AUTHENTICATION_FAILED",
                                                                  "CONSOLE_AUTHENTICATION_FAILED",
                                                                  "ENTITY_CONFIG_CHANGE"
                                                        ],
                                                        "version" : "V1"
                                                        },
                                                      { "community_string" : None,
                                                        "host" : { "value" : SNMPAgentIP },
                                                        "port" : SNMPAgentPort,
                                                        "remote_user" : { "name" : "ixiaV3",
                                                                          "authn_protocol" : "HMAC_MD5_96",
                                                                          "authn_password" : "ixiaV3password",
                                                                          "privacy_protocol" : "DES",
                                                                          "privacy_password" : "ixiaV3password",
                                                                          "context_name" : "",
                                                                          "engine_id" : "",
                                                                          "security_level" : "AUTHN_AND_PRIVACY"
                                                                         },
                                                        "retry_count" : 1,
                                                        "timeout" : 5,
                                                        "traps": ["COLD_START",
                                                                  "WARM_START",
                                                                  "LINK_UP_DOWN",
                                                                  "AUTHENTICATION_FAILED",
                                                                  "CONSOLE_AUTHENTICATION_FAILED",
                                                                  "TX_DROPPED_PKTS",
                                                                  "RX_INVALID_PKTS",
                                                                  "RX_UTILIZATION",
                                                                  "TX_UTILIZATION",
                                                                  "ENTITY_CONFIG_CHANGE"
                                                                  ],
                                                        "version": "V3"
                                                        }
                                                     ],
                                "traps_enabled" : True
                                },
              "syslog_server_list" : SyslogServers,
              "ptp_config" : { "address_mode" : "UNICAST_ONLY",
                               "announce_receipt_timeout" : 3,
                               "clock_domain" : 0,
                               "dscp" : 0,
                               "enabled" : False,
                               "master_address" : { "ipv4_address" : ptpMasterAddr,
                                                    "ipv4_gateway" : "0.0.0.0",
                                                    "ipv4_netmask" : "0.0.0.0"
                                                    },
                               "slave_address" : { "ipv4_address" : ptpSlaveAddr,
                                                   "ipv4_gateway" : ptpSlaveGatewayAddr,
                                                   "ipv4_netmask" : "255.255.255.0"
                                                   },
                               "vlan_config" : { "enabled" : False,
                                                 "vlan_id" : 1,
                                                 "vlan_priority" : 0
                                                 }
                               },
              "system_info" : { "location" : "a galaxy far far away",
                                "name" : "TestMe",
                                "contact_info" : "me@home.com"
                                },
              "tool_management_view_enabled" : True,
              "login_banner_config" : { "text" : "New banner order" },
              "stats_polling_interval" : 1,
              "authentication_mode" : "RADIUS",
              "radius_servers" : { "common" : { "attrs" : [ { "logical_operation" : "OR",
                                                              "pairs" : [ { "name" : "Anue-Groups",
                                                                          "value" : "REG" } ],
                                                              "type" : "GROUP_NAMES"
                                                              },
                                                            { "logical_operation" : "OR",
                                                              "pairs" : [ { "name" : "Anue-Role",
                                                                          "value" : "ADMIN" } ],
                                                              "type" : "AUTHZ_ADMIN"
                                                              },
                                                            { "logical_operation" : "OR",
                                                              "pairs" : [ { "name" : "Anue-Role",
                                                                          "value" : "REG" } ],
                                                              "type" : "AUTHZ_REGULAR"
                                                              }
                                                            ],
                                                "enable_authz" : True,
                                                "enable_groups" : True
                                                },
                                   "servers" : [ { "acct_attrs" : [],
                                                   "acct_port" : 1813,
                                                   "authn_type" : "CHAP",
                                                   "enable_acct" : True,
                                                   "host" : RemoteAuthServerIP,
                                                   "secret" : RemoteRadiusSecret,
                                                   "port" : 1812,
                                                   "retry_count" : 2,
                                                   "timeout" : 10
                                                   }
                                                ]
                                   }
              }

sysInfo = { "NTOIP" : "10.36.168.212",
            "Paths" : [ { "PathName" : "Path1",
                          "NetworkPorts" : [ { "name" : "P3-01", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-01", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo" : { "resource" : { "id" : "L5-AFM",
                                                          "allocated_bandwidth" : 25
                                                          },
                                           "resource_attachment_config" : { "mpls_strip_settings" : { "enabled" : True,
                                                                                                      "service_type" : "L3_VPN"
                                                                                                      }
                                                                            }
                                           }
                          },
                        { "PathName" : "Path1a",
                          "NetworkPorts" : [ { "name" : "P4-01", "mode": "BIDIRECTIONAL"}],
                          "ToolPorts" : [ { "name" : "P3-01", "mode": "BIDIRECTIONAL"}]
                          },
                        { "PathName" : "Path2",
                          "NetworkPorts" : [ { "name" : "P3-04", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-03", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path2a",
                          "NetworkPorts" : [ { "name" : "P4-03", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-04", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName": "Path3",
                          "NetworkPorts": [{"name": "P3-06", "mode": "BIDIRECTIONAL"}],
                          "ToolPorts": [{"name": "P4-05", "mode": "BIDIRECTIONAL"}]
                        },
                        { "PathName": "Path3a",
                          "NetworkPorts": [{"name": "P4-05", "mode": "BIDIRECTIONAL"}],
                          "ToolPorts": [{"name": "P3-06", "mode": "BIDIRECTIONAL"}]
                        },
                        {"PathName": "Path4",
                         "NetworkPorts": [{"name": "P3-08", "mode": "BIDIRECTIONAL"}],
                         "ToolPorts": [{"name": "P4-07", "mode": "BIDIRECTIONAL"}]
                         },
                        {"PathName": "Path4a",
                         "NetworkPorts": [{"name": "P4-07", "mode": "BIDIRECTIONAL"}],
                         "ToolPorts": [{"name": "P3-08", "mode": "BIDIRECTIONAL"}]
                         },
                        { "PathName" : "Path5",
                          "NetworkPorts" : [ { "name" : "P3-10", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-09", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path5a",
                          "NetworkPorts" : [ { "name" : "P4-09", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-10", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path6",
                          "NetworkPorts" : [ { "name" : "P3-12", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-11", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path6a",
                          "NetworkPorts" : [ { "name" : "P4-11", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-12", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path7",
                          "NetworkPorts" : [ { "name" : "P3-14", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-13", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path7a",
                          "NetworkPorts" : [ { "name" : "P4-13", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-14", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path8",
                          "NetworkPorts" : [ { "name" : "P3-16", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-15", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path8a",
                          "NetworkPorts" : [ { "name" : "P4-15", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-16", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path9",
                          "NetworkPorts" : [ { "name" : "P4-02", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-03", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path9a",
                          "NetworkPorts" : [ { "name" : "P3-03", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-02", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path10",
                          "NetworkPorts" : [ { "name" : "P4-04", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-05", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path10a",
                          "NetworkPorts" : [ { "name" : "P3-05", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-04", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path11",
                          "NetworkPorts" : [ { "name" : "P4-06", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-07", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path11a",
                          "NetworkPorts" : [ { "name" : "P3-07", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-06", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path12",
                          "NetworkPorts" : [ { "name" : "P4-08", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-09", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path12a",
                          "NetworkPorts" : [ { "name" : "P3-09", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-08", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path13",
                          "NetworkPorts" : [ { "name" : "P4-10", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-11", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path13a",
                          "NetworkPorts" : [ { "name" : "P3-11", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-10", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path14",
                          "NetworkPorts" : [ { "name" : "P4-12", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-13", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path14a",
                          "NetworkPorts" : [ { "name" : "P3-13", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-12", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path15",
                          "NetworkPorts" : [ { "name" : "P4-14", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-15", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path15a",
                          "NetworkPorts" : [ { "name" : "P3-15", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-14", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path16",
                          "NetworkPorts" : [ { "name" : "P4-16", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-17", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path16a",
                          "NetworkPorts" : [ { "name" : "P3-17", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-16", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path17",
                          "NetworkPorts" : [ { "name" : "P3-18", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-17", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path17a",
                          "NetworkPorts" : [ { "name" : "P4-17", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-18", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path18",
                          "NetworkPorts" : [ { "name" : "P3-20", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-19", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path18a",
                          "NetworkPorts" : [ { "name" : "P4-19", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-20", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path19",
                          "NetworkPorts" : [ { "name" : "P3-22", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-21", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path19a",
                          "NetworkPorts" : [ { "name" : "P4-21", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-22", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path20",
                          "NetworkPorts" : [ { "name" : "P3-24", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-23", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path20a",
                          "NetworkPorts" : [ { "name" : "P4-23", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-24", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path21",
                          "NetworkPorts" : [ { "name" : "P3-26", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-25", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path21a",
                          "NetworkPorts" : [ { "name" : "P4-25", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-26", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path22",
                          "NetworkPorts" : [ { "name" : "P3-28", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-27", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path22a",
                          "NetworkPorts" : [ { "name" : "P4-27", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-28", "mode" : "BIDIRECTIONAL" } ]
                        },
                        {"PathName": "Path23",
                         "NetworkPorts": [{"name": "P3-30", "mode": "BIDIRECTIONAL"}],
                         "ToolPorts": [{"name": "P4-29", "mode": "BIDIRECTIONAL"}]
                        },
                        {"PathName": "Path23a",
                         "NetworkPorts": [{"name": "P4-29", "mode": "BIDIRECTIONAL"}],
                         "ToolPorts": [{"name": "P3-30", "mode": "BIDIRECTIONAL"}]
                        },
                        { "PathName" : "Path24",
                          "NetworkPorts" : [ { "name" : "P3-32", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-31", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path24a",
                          "NetworkPorts" : [ { "name" : "P4-31", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-32", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path25",
                          "NetworkPorts" : [ { "name" : "P4-18", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-19", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path25a",
                          "NetworkPorts" : [ { "name" : "P3-19", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-18", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path26",
                          "NetworkPorts" : [ { "name" : "P4-20", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-21", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path26a",
                          "NetworkPorts" : [ { "name" : "P3-21", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-20", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path27",
                          "NetworkPorts" : [ { "name" : "P4-22", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-23", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path27a",
                          "NetworkPorts" : [ { "name" : "P3-23", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-22", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path28",
                          "NetworkPorts" : [ { "name" : "P4-24", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-25", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path28a",
                          "NetworkPorts" : [ { "name" : "P3-25", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-24", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path29",
                          "NetworkPorts" : [ { "name" : "P4-26", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-27", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path29a",
                          "NetworkPorts" : [ { "name" : "P3-27", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-26", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path30",
                          "NetworkPorts" : [ { "name" : "P4-28", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-29", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path30a",
                          "NetworkPorts" : [ { "name" : "P3-29", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-28", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path31",
                          "NetworkPorts" : [ { "name" : "P4-30", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-31", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path31a",
                          "NetworkPorts" : [ { "name" : "P3-31", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-30", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path32",
                          "NetworkPorts" : [ { "name" : "P4-32", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-33", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path32a",
                          "NetworkPorts" : [ { "name" : "P3-33", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-32", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path33",
                          "NetworkPorts" : [ { "name" : "P3-34", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-33", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path33a",
                          "NetworkPorts" : [ { "name" : "P4-33", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-34", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path34",
                          "NetworkPorts" : [ { "name" : "P3-36", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-35", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path34a",
                          "NetworkPorts" : [ { "name" : "P4-35", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-36", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path35",
                          "NetworkPorts" : [ { "name" : "P3-38", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-37", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path35a",
                          "NetworkPorts" : [ { "name" : "P4-37", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-38", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path36",
                          "NetworkPorts" : [ { "name" : "P3-40", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-39", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path36a",
                          "NetworkPorts" : [ { "name" : "P4-39", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-40", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path37",
                          "NetworkPorts" : [ { "name" : "P3-42", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-41", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path37a",
                          "NetworkPorts" : [ { "name" : "P4-41", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-42", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path38",
                          "NetworkPorts" : [ { "name" : "P3-44", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-43", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path38a",
                          "NetworkPorts" : [ { "name" : "P4-43", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-44", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path39",
                          "NetworkPorts" : [ { "name" : "P3-46", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-45", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path39a",
                          "NetworkPorts" : [ { "name" : "P4-45", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-46", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path40",
                          "NetworkPorts" : [ { "name" : "P3-48", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-47", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path40a",
                          "NetworkPorts" : [ { "name" : "P4-47", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-48", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path41",
                          "NetworkPorts" : [ { "name" : "P4-34", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-35", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path41a",
                          "NetworkPorts" : [ { "name" : "P3-35", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-34", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path42",
                          "NetworkPorts" : [ { "name" : "P4-36", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-37", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path42a",
                          "NetworkPorts" : [ { "name" : "P3-37", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-36", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path43",
                          "NetworkPorts" : [ { "name" : "P4-38", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-39", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path43a",
                          "NetworkPorts" : [ { "name" : "P3-39", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-38", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path44",
                          "NetworkPorts" : [ { "name" : "P4-40", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-41", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path44a",
                          "NetworkPorts" : [ { "name" : "P3-41", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-40", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path45",
                          "NetworkPorts" : [ { "name" : "P4-42", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-43", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path45a",
                          "NetworkPorts" : [ { "name" : "P3-43", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-42", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path46",
                          "NetworkPorts" : [ { "name" : "P4-44", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-45", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path46a",
                          "NetworkPorts" : [ { "name" : "P3-45", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-44", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path47",
                          "NetworkPorts" : [ { "name" : "P4-46", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-47", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path47a",
                          "NetworkPorts" : [ { "name" : "P3-47", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-46", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path48",
                          "NetworkPorts" : [ { "name" : "P4-48", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P3-02", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path48a",
                          "NetworkPorts" : [ { "name" : "P3-02", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P4-48", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path49",
                          "NetworkPorts" : [ { "name" : "P1-01", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2-01", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path49a",
                          "NetworkPorts" : [ { "name" : "P2-01", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P1-01", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path50",
                          "NetworkPorts" : [ { "name" : "P1-04", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2-03", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path50a",
                          "NetworkPorts" : [ { "name" : "P2-03", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P1-04", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path51",
                          "NetworkPorts" : [ { "name" : "P1-06", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2A-01", "mode" : "BIDIRECTIONAL",
                                            "erspan_strip_settings" : { "port_mode": "NETWORK",
                                                                        "empty_header": False,
                                                                        "enabled": True
                                                                        }
                                            }
                                          ]
                          },
                        { "PathName" : "Path51a",
                          "NetworkPorts" : [ { "name" : "P2A-01", "mode" : "BIDIRECTIONAL",
                                               "erspan_strip_settings" : { "port_mode" : "NETWORK",
                                                                           "empty_header" : False,
                                                                           "enabled" : True
                                                                           }
                                               }
                                           ],
                          "ToolPorts" : [ { "name" : "P1-06", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path52",
                          "NetworkPorts" : [ { "name" : "P1-08", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2A-03", "mode" : "BIDIRECTIONAL",
                                            "l2gre_strip_settings" : { "port_mode": "NETWORK",
                                                                       "enabled": True
                                                                       }
                                            }
                                          ]
                        },
                        { "PathName" : "Path52a",
                          "NetworkPorts" : [ { "name" : "P2A-03", "mode" : "BIDIRECTIONAL",
                                               "l2gre_strip_settings" : { "port_mode" : "NETWORK",
                                                                          "enabled" : True
                                                                          }
                                               }
                                           ],
                          "ToolPorts" : [ { "name" : "P1-08", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path53",
                          "NetworkPorts" : [ { "name" : "P1-10", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2B-01", "mode" : "BIDIRECTIONAL",
                                            "gtp_strip_settings" : { "port_mode" : "NETWORK",
                                                                     "enabled" : True
                                                                     }
                                            }
                                          ]
                        },
                        { "PathName" : "Path53a",
                          "NetworkPorts" : [ { "name" : "P2B-01", "mode" : "BIDIRECTIONAL",
                                               "gtp_strip_settings" : { "port_mode" : "NETWORK",
                                                                        "enabled" : True
                                                                        }
                                               }
                                        ],
                          "ToolPorts" : [ { "name" : "P1-10", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path54",
                          "NetworkPorts" : [ { "name" : "P1-12", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2B-03", "mode" : "BIDIRECTIONAL",
                                            "vntag_strip_settings" : { "port_mode" : "NETWORK",
                                                                       "enabled" : True
                                                                       }
                                            }
                                          ]
                        },
                        { "PathName" : "Path54a",
                          "NetworkPorts" : [ { "name" : "P2B-03", "mode" : "BIDIRECTIONAL",
                                               "vntag_strip_settings" : { "port_mode" : "NETWORK",
                                                                          "enabled" : True
                                                                          }
                                               }
                                           ],
                          "ToolPorts" : [ { "name" : "P1-12", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path55",
                          "NetworkPorts" : [ { "name" : "P1-14", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P1-15", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path55a",
                          "NetworkPorts" : [ { "name" : "P1-15", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P1-14", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path56",
                          "NetworkPorts" : [ { "name" : "P1-16", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P1-02", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path56a",
                          "NetworkPorts" : [ { "name" : "P1-02", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P1-16", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path57",
                          "NetworkPorts" : [ { "name" : "P2-02", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P1-03", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path57a",
                          "NetworkPorts" : [ { "name" : "P1-03", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2-02", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path58",
                          "NetworkPorts" : [ { "name" : "P2-04", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P1-05", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path58a",
                          "NetworkPorts" : [ { "name" : "P1-05", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2-04", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path59",
                          "NetworkPorts" : [ { "name" : "P2A-02", "mode" : "BIDIRECTIONAL",
                                               "erspan_strip_settings" : { "port_mode" : "NETWORK",
                                                                           "empty_header" : False,
                                                                           "enabled" : True
                                                                           }
                                               }
                                           ],
                          "ToolPorts" : [ { "name" : "P1-07", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path59a",
                          "NetworkPorts" : [ { "name" : "P1-07", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2A-02", "mode" : "BIDIRECTIONAL",
                                            "erspan_strip_settings" : { "port_mode" : "NETWORK",
                                                                        "empty_header" : False,
                                                                        "enabled" : True
                                                                        }
                                            }
                                        ]
                        },
                        { "PathName" : "Path60",
                          "NetworkPorts" : [ { "name" : "P2A-04", "mode" : "BIDIRECTIONAL",
                                               "mpls_strip_settings" : { "port_mode" : "NETWORK",
                                                                         "enabled" : True,
                                                                         "service_type" : "L3_VPN"
                                                                         }
                                               }
                                           ],
                          "ToolPorts" : [ { "name" : "P1-09", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path60a",
                          "NetworkPorts" : [ { "name" : "P1-09", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2A-04", "mode" : "BIDIRECTIONAL",
                                            "mpls_strip_settings" : { "port_mode" : "NETWORK",
                                                                      "enabled" : True,
                                                                      "service_type" : "L3_VPN"
                                                                      }
                                            }
                                        ]
                        },
                        { "PathName" : "Path61",
                          "NetworkPorts" : [ { "name" : "P2B-02", "mode" : "BIDIRECTIONAL",
                                               "fabric_path_strip_settings" : { "port_mode" : "NETWORK",
                                                                                "enabled" : True
                                                                                },
                                               "vntag_strip_settings" : { "port_mode" : "NETWORK",
                                                                          "enabled" : True
                                                                          }
                                               }
                                           ],
                          "ToolPorts" : [ { "name" : "P1-11", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path61a",
                          "NetworkPorts" : [ { "name" : "P1-11", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2B-02", "mode" : "BIDIRECTIONAL",
                                            "fabric_path_strip_settings" : { "port_mode" : "NETWORK",
                                                                             "enabled" : True
                                                                             },
                                            "vntag_strip_settings" : { "port_mode" : "NETWORK",
                                                                       "enabled" : True
                                                                       }
                                            }
                                        ],
                        },
                        { "PathName" : "Path62",
                          "NetworkPorts" : [ { "name" : "P2B-04", "mode" : "BIDIRECTIONAL",
                                               "vxlan_strip_settings" : { "port_mode" : "NETWORK",
                                                                          "enabled" : True
                                                                          },
                                               "std_vlan_strip_settings" : { "count" : 1,
                                                                             "enabled" : True,
                                                                             "strip_mode" : "INGRESS"
                                                                             }
                                               }
                                           ],
                          "ToolPorts" : [ { "name" : "P1-13", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path62a",
                          "NetworkPorts" : [ { "name" : "P1-13", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P2B-04", "mode" : "BIDIRECTIONAL",
                                            "vxlan_strip_settings" : { "port_mode" : "NETWORK",
                                                                       "enabled" : True
                                                                       },
                                            "std_vlan_strip_settings" : { "count" : 1,
                                                                          "enabled" : True,
                                                                          "strip_mode" : "INGRESS"
                                                                          }
                                            }
                                        ]
                        },
                        { "PathName" : "Path63",
                          "NetworkPorts" : [ { "name" : "P5-01", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-01", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path63a",
                          "NetworkPorts" : [ { "name" : "P6-01", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-01", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path64",
                          "NetworkPorts" : [ { "name" : "P6-02", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-03", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path64a",
                          "NetworkPorts" : [ { "name" : "P5-03", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-02", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource" : { "id" : "L5-AFM",
                                                         "allocated_bandwidth" : 100
                                                         },
                                          "resource_attachment_config" : { "erspan_strip_settings" : { "enabled" : True,
                                                                                                       "empty_header" : False
                                                                                                       }
                                                                           }
                                         }
                          },
                        { "PathName" : "Path65",
                          "NetworkPorts" : [ { "name" : "P5-04", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-03", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path65a",
                          "NetworkPorts" : [ { "name" : "P6-03", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-04", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path66",
                          "NetworkPorts" : [ { "name" : "P6-04", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-05", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource": { "id" : "L6-AFM",
                                                        "allocated_bandwidth" : 100
                                                        },
                                          "resource_attachment_config" : { "mpls_strip_settings" : { "enabled" : True,
                                                                                                     "service_type" : "L3_VPN"
                                                                                                     }
                                                                           }
                                          }
                          },
                        { "PathName" : "Path66a",
                          "NetworkPorts" : [ { "name" : "P5-05", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-04", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource" : { "id" : "L5-AFM",
                                                         "allocated_bandwidth": 100
                                                         },
                                          "resource_attachment_config" : { "l2gre_strip_settings" : { "enabled" : True } }
                                          }
                          },
                        { "PathName" : "Path67",
                          "NetworkPorts" : [ { "name" : "P5-06", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-07", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path67a",
                          "NetworkPorts" : [ { "name" : "P5-07", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-06", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path68",
                          "NetworkPorts" : [ { "name" : "P5-08", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-09", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path68a",
                          "NetworkPorts" : [ { "name" : "P5-09", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-08", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path69",
                          "NetworkPorts" : [ { "name" : "P5-10", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-11", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path69a",
                          "NetworkPorts" : [ { "name" : "P5-11", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-10", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path70",
                          "NetworkPorts" : [ { "name" : "P5-12", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-05", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path70a",
                          "NetworkPorts" : [ { "name" : "P6-05", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-12", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource" : { "id" : "L6-AFM",
                                                         "allocated_bandwidth": 100
                                                         },
                                          "resource_attachment_config" : { "vxlan_strip_settings" : { "enabled" : True } }
                                          }
                          },
                        { "PathName" : "Path71",
                          "NetworkPorts" : [ { "name" : "P6-06", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-07", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path71a",
                          "NetworkPorts" : [ { "name" : "P6-07", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-06", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource": { "id" : "L6-AFM",
                                                        "allocated_bandwidth": 100
                                                        },
                                          "resource_attachment_config" : { "fabric_path_strip_settings" : { "enabled" : True },
                                                                           "vntag_strip_settings" : { "enabled" : True },
                                                                           "gtp_strip_settings" : { "enabled" : True }
                                                                           }
                                          }
                          },
                        { "PathName" : "Path72",
                          "NetworkPorts" : [ { "name" : "P6-08", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-09", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path72a",
                          "NetworkPorts" : [ { "name" : "P6-09", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-08", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path73",
                          "NetworkPorts" : [ { "name" : "P6-10", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-11", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource" : { "id" : "L6-AFM",
                                                         "allocated_bandwidth": 100
                                                         },
                                          "resource_attachment_config" : { "fabric_path_strip_settings" : { "enabled" : True },
                                                                           "vntag_strip_settings" : { "enabled" : True }
                                                                           }
                                         }
                          },
                        { "PathName" : "Path73a",
                          "NetworkPorts" : [ { "name" : "P6-11", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-10", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path74",
                          "NetworkPorts" : [ { "name" : "P6-12", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P5-02", "mode" : "BIDIRECTIONAL" } ]
                        },
                        { "PathName" : "Path74a",
                          "NetworkPorts" : [ { "name" : "P5-02", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P6-12", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo" : { "resource" : { "id" : "L5-AFM",
                                                          "allocated_bandwidth": 100
                                                          },
                                           "resource_attachment_config" : { "fabric_path_strip_settings" : { "enabled" : True } }
                                           }
                          }
                        ]
            }

sysInfo_myV1 = { "NTOIP" : "10.36.168.162",
            "Paths" : [ { "PathName" : "Path1",
                          "NetworkPorts" : [ { "name" : "P01", "mode" : "BIDIRECTIONAL",
                                               "resource" : { "id" : "L1-AFM",
                                                              "allocated_bandwidth" : 10 },
                                               "resource_attachment_config" : { "erspan_strip_settings" : { "enabled" : True,
                                                                                                            "empty_header" : False }
                                                                                }
                                               }
                                             ],
                          "ToolPorts" : [ { "name" : "P02", "mode" : "BIDIRECTIONAL",
                                            "resource" : { "id" : "L1-AFM",
                                                           "allocated_bandwidth" : 10 },
                                            "resource_attachment_config" : { "vxlan_strip_settings" : { "enabled" : True },
                                                                             },
                                            "std_vlan_strip_settings": { "count" : 1,
                                                                         "enabled" : True,
                                                                         "strip_mode" : "INGRESS" }
                                            }
                                          ],
                          "FilterInfo" : { "resource" : { "id" : "L1-AFM",
                                                          "allocated_bandwidth" : 10
                                                          },
                                           "resource_attachment_config" : { "mpls_strip_settings" : { "enabled" : True,
                                                                                                      "service_type" : "L3_VPN"
                                                                                                      }
                                                                            }
                                           }
                          },
                        { "PathName" : "Path2",
                          "NetworkPorts" : [ { "name" : "P02", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P01", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource": { "id": "L1-AFM",
                                                        "allocated_bandwidth": 40
                                                        },
                                          "resource_attachment_config": { "erspan_strip_settings": { "enabled": True,
                                                                                                     "empty_header" : False
                                                                                                     }
                                                                          }
                                          }

                          },
                        { "PathName" : "Path3",
                          "NetworkPorts" : [ { "name" : "P03", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P04", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource": { "id": "L1-AFM",
                                                        "allocated_bandwidth": 10
                                                      },
                                          "resource_attachment_config": { "l2gre_strip_settings" : { "enabled": True } }
                                          }

                          },
                        { "PathName" : "Path4",
                          "NetworkPorts" : [ { "name" : "P04", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P03", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource": { "id": "L1-AFM",
                                                        "allocated_bandwidth": 10
                                                        },
                                          "resource_attachment_config": { "vxlan_strip_settings": { "enabled": True } }
                                          }

                          },
                        { "PathName" : "Path5",
                          "NetworkPorts" : [ { "name" : "P05", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P06", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource": { "id": "L1-AFM",
                                                        "allocated_bandwidth": 10
                                                        },
                                          "resource_attachment_config": { "gtp_strip_settings": { "enabled": True },
                                                                          "fabric_path_strip_settings": { "enabled": True },
                                                                          "vntag_strip_settings": { "enabled": True }
                                                                          }
                                          }

                          },
                        { "PathName" : "Path6",
                          "NetworkPorts" : [ { "name" : "P06", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P05", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource": { "id": "L1-AFM",
                                                        "allocated_bandwidth": 10
                                                        },
                                          "resource_attachment_config": { "fabric_path_strip_settings": { "enabled": True },
                                                                          "vntag_strip_settings": { "enabled": True }
                                                                          }
                                          }
                          },
                        { "PathName" : "Path7",
                          "NetworkPorts" : [ { "name" : "P07", "mode" : "BIDIRECTIONAL" } ],
                          "ToolPorts" : [ { "name" : "P08", "mode" : "BIDIRECTIONAL" } ],
                          "FilterInfo": { "resource": { "id": "L1-AFM",
                                                        "allocated_bandwidth": 10
                                                        },
                                          "resource_attachment_config": { "fabric_path_strip_settings": { "enabled": True } }
                                          }
                          },
                        ]
            }
