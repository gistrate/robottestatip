*** Settings ***

Variables  SystemConfig.py

Library  Collections

Resource  NTO/frwk/RobotUtils.robot

Force Tags  NTO  performance  SnakeTest

Suite Setup        SetupNTO

*** Test Cases ***

RunSnakeTest
    log  Running snake test...  console=true

*** Keywords ***

SetupNTO
    log  Setting up for snake test...  console=true
    AddNTOLocal  nto1  ${sysInfo['NTOIP']}
#    nto1.clearSystem
# Delete clearFiltersAndPorts before check-in...just doing this for expediency during development
    nto1.clearFiltersAndPorts
    log  Setting system level configuration...  console=true
    nto1.setSystem  ${sysConfig}
    log  Configuring ports and filters...  console=true
    nto1.setPortAndFilterConfig  ${sysInfo}
    log  Checking port status...  console=true
    nto1.checkPortStatus
