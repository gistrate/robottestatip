*** Settings ***
Suite Setup    setupSuiteEnv
Suite Teardown  suiteCleanup
Test Teardown   testCleanup
#Libraries
Library  ./resourceMgr_module/resourceMgr.py  nto-dbmgr-02.ann.is.keysight.com

Library  Collections
Library  Dialogs
Library  OperatingSystem
Library  ./NTO/frwk/Utils.py

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot


*** Variables ***
#Command line variables:  ${USERNAME}, ${TESTDIR}, ${RESOURCEPOOL}
${RESOURCETIMEOUT}=  60


*** Test Cases ***

TC001075996_ARP_Reset_for_a_remote_end_ip_address_change_on_the_same_ip_subnet

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################


    #Virt Port local and remote definitions.
    ${localIPAddr}=  Set Variable  1.1.1.1
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    tp1Ixia.configureIxStreams  True  1

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 12  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ##########################################################
    ###2.Verify GRE configuration + change remote settings ###
    ##########################################################

    #Virt Port remote definitions
    ${remoteIPAddr}=   Set Variable  1.1.1.5

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Arp Reply Stream Definition
    ${gratArpReply2}=  createStream  s3  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Stop traffic
    tp1Ixia.stopIxTraffic

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    tp1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats
    tp1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream1}
    tp1Ixia.addIxStreams  ${gratArpReply2}
    tp1Ixia.configureIxStreams  True  1
    np1Ixia.configureIxStreams

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #Change Remote Settings for Virtual Port
    vp1.configurePort  remoteIP=${remoteIPAddr}

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################
    #Capture File Definitions
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}
    log  ${tp1Ixia.recdFrames}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  tp1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001075998_ARP_reset_for_a_remote_end_ip_change_on_a_different_IP_subnet

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr}=  Set Variable  1.1.1.1
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters
    sleep  9secs

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 12  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    #################################################################################
    ###2.Verify GRE configuration + change remote end ip on a different IP subnet ###
    #################################################################################

    #Virt Port remote definitions
    ${remoteIPAddr}=   Set Variable  2.2.2.2

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Arp Reply Stream Definition
    ${gratArpReply2}=  createStream  s3  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${defaultGateway}', hwdst='00:00:00:11:11:11', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    tp1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats
    tp1Ixia.clearIxPortStats

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${stream1}
    tp1Ixia.addIxStreams  ${gratArpReply2}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #Change Remote Settings for Virtual Port
    vp1.configurePort  remoteIP=${remoteIPAddr}

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}


    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001076000_ARP_reset_for_a_gateway_ip_address_change


    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr}=  Set Variable  1.1.1.1
    ${remoteIPAddr}=   Set Variable  2.2.2.2
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${defaultGateway}', hwdst='00:00:00:11:11:11', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ##########################################################################
    ###2.Verify GRE configuration + change local settings(default gateway) ###
    ##########################################################################

    #Virt Port local definitions
    ${defaultGateway}=  Set Variable  1.1.1.250
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Arp Reply Stream Definition
    ${gratArpReply2}=  createStream  s3  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${defaultGateway}', hwdst='00:00:00:11:11:11', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    tp1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats
    tp1Ixia.clearIxPortStats

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${stream1}
    tp1Ixia.addIxStreams  ${gratArpReply2}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #Change Remote Settings for Virtual Port
    vp1.configurePort  localSettings=${vp1Def}

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001076001_Virtual_Port_Remote_end_on_different_subnet

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr}=  Set Variable  1.1.1.1
    ${remoteIPAddr}=   Set Variable  2.2.2.2
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s1  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Arp Reply Stream Definition
    ${arpReply}=  createStream  s2  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${defaultGateway}', hwdst='00:00:00:11:11:11', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${arpReply}
    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}


    ###################################################################
    ###2.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}


    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001076038_PORTGROUP_Virtual_Port_Group_Add_Remove_Virtual_Port

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr1}=  Set Variable  1.1.1.1
    ${localIPAddr2}=  Set Variable  1.1.1.2
    ${remoteIPAddr}=   Set Variable  2.2.2.2
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}
    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr2}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${arpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${defaultGateway}', hwdst='00:00:00:11:11:11', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100
    ${arpReply2}=  createStream  s2  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${defaultGateway}', hwdst='00:00:00:11:11:11', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100
    ${stream1}=  createStream  s3  Ether(src='00:22:33:44:55:66')/IP(proto=(60))  payload=incr  frameSize=100   numFrames=200
    ${stream2}=  createStream  s4  Ether(src='00:22:33:44:55:66')/IP(proto=(61))  payload=incr  frameSize=100   numFrames=100

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    AddVirtPort  ${tp2}  vp2  ${vp2Def}  ${remoteIPAddr}

    #Port Group definitions.
    ${pg1def}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    @{pgPortList}=  Create List  ${tp1}  ${vp1}  ${tp2}  ${vp2}

    AddPortGrp  ${nto1}  tpg1  ${pg1def}  ${pgPortList}

    AddFilter  ${nto1}  f1    PASS_ALL

    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tpg1}

    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${arpReply1}
    tp2Ixia.addIxStreams  ${arpReply2}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request1.enc
    ${capturePcapArp1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request1.pcap

    ${captureFileArp2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request2.enc
    ${capturePcapArp2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request2.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    tp2Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    sleep  3secs
    nto1.rediscoverIds
    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    ${actualTp2IxiaStats}=   tp2Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6 and ${actualTp2IxiaStats["rxFrames"]} == 6    Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    tp2Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    tp2Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp1}  1  6
    convertEncToPcap  ${captureFileArp1}  ${capturePcapArp1}
    tp1Ixia.scapyImportPcap  ${capturePcapArp1}

    tp2Ixia.exportIxFramesToFile  ${captureFileArp2}  1  6
    convertEncToPcap  ${captureFileArp2}  ${capturePcapArp2}
    tp2Ixia.scapyImportPcap  ${capturePcapArp2}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr1}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr1}=  replaceDashWithColon  ${localMacAddr1}

    ${tp2Props}=  tp2.getPortInfo
    ${localMacAddr2}=  evaluate  ${tp2Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr2}=  replaceDashWithColon  ${localMacAddr2}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr1}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr1}=  replaceDashWithColon  ${remoteMacAddr1}

    ${vp2Props}=  vp2.getVirtPortInfo
    ${remoteMacAddr2}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr2}=  replaceDashWithColon  ${remoteMacAddr2}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent1}=  catenate  Ether(type=2054, src='${localMacAddr1}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr1}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes1}=  convertStreamToBytes  ${ExpectedArpFrameSent1}
    ${ExpectedArpFrameSentBytes1}=  create list  ${ExpectedArpFrameSentBytes1}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes1}  ${tp1Ixia.recdFrames}  ordered=${False}

    ${ExpectedArpFrameSent2}=  catenate  Ether(type=2054, src='${localMacAddr2}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr2}', psrc='${localIPAddr2}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes2}=  convertStreamToBytes  ${ExpectedArpFrameSent2}
    ${ExpectedArpFrameSentBytes2}=  create list  ${ExpectedArpFrameSentBytes2}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes2}  ${tp2Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###2.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Capture File Definitions
    ${captureGrePktEncFile1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream1.enc
    ${captureGrePktPcapFile1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream1.pcap
    ${captureGrePktPcapFileFiltered1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered1.pcap

    ${captureGrePktEncFile2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream2.enc
    ${captureGrePktPcapFile2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream2.pcap
    ${captureGrePktPcapFileFiltered2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered2.pcap

    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    tp2Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    tp2Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile1}  1  10000
    convertEncToPcap  ${captureGrePktEncFile1}  ${captureGrePktPcapFile1}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile1}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered1}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered1}

    #Download the capture from IxOS
    tp2Ixia.exportIxFramesToFile  ${captureGrePktEncFile2}  1  10000
    convertEncToPcap  ${captureGrePktEncFile2}  ${captureGrePktPcapFile2}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile2}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered2}

    #Import bytes into Ixia Port Object
    tp2Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered2}
    log  ${tp2Ixia.recdFrames}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing1}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr1}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr1}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr1}" }
    ...  }}
    ${pktProcessing2}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr2}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr2}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr2}" }
    ...  }}

    #Comparing frames and ignoring byte offset
    ${ExpectedEncapGreFrame1}=  np1.pktProc.emulate  ${pktProcessing1}  pcapFile=${origCapturePcap}
    compareFrames  ${ExpectedEncapGreFrame1}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${False}

    #Comparing frames and ignoring byte offset
    ${ExpectedEncapGreFrame2}=  np1.pktProc.emulate  ${pktProcessing2}  pcapFile=${origCapturePcap}
    compareFrames  ${ExpectedEncapGreFrame2}  ${tp2Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${False}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=300
        ...  np_total_rx_count_valid_packets=300
        ...  np_total_rx_count_bytes=30000
        ...  np_total_pass_count_packets=300
        ...  np_total_pass_count_bytes=30000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=300
        ...  df_total_insp_count_bytes=30000
        ...  df_total_pass_count_packets=300
        ...  df_total_pass_count_bytes=30000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=None
        ...  tp_total_pass_count_packets=None
        ...  tp_total_tx_count_packets=None
        ...  tp_total_tx_count_bytes=None
        ...  tp_total_gre_encap_count_packets=None
        ...  tp_total_gre_encap_percent_packets=None
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Run keyword if   ${actualTp1Stats["${key}"]} <= 0  fail  Load Balance was not done correctly

    # Verify Statistics on Tool Port
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=None
        ...  tp_total_pass_count_packets=None
        ...  tp_total_tx_count_packets=None
        ...  tp_total_tx_count_bytes=None
        ...  tp_total_gre_encap_count_packets=None
        ...  tp_total_gre_encap_percent_packets=None
    ${actualTp2Stats}=  tp2.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Run keyword if   ${actualTp2Stats["${key}"]} <= 0  fail  Load Balance was not done correctly

    # Verify Statistics on Tool Port Group
    &{expectedTpg1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=300
        ...  tp_total_pass_count_packets=300
        ...  tp_total_tx_count_packets=300
        ...  tp_total_tx_count_bytes=41400
        ...  tp_total_gre_encap_count_packets=300
    ${actualTpg1Stats}=  tpg1.getPortGrpStats  statName=all
    :FOR    ${key}    IN    @{expectedTpg1Stats.keys()}
    \    should be equal as integers    ${expectedTpg1Stats["${key}"]}     ${actualTpg1Stats["${key}"]}
    \    ${statSum}=  Evaluate  ${actualTp1Stats["${key}"]} + ${actualTp2Stats["${key}"]}
    \    should be equal as integers    ${actualTpg1Stats["${key}"]}    ${statSum}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001076041_PORTGROUP_Virtual_Port_Group_Add_Remove_Non_Virtual_Port

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr1}=  Set Variable  1.1.1.1
    ${localIPAddr2}=  Set Variable  1.1.1.2
    ${remoteIPAddr}=   Set Variable  2.2.2.2
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${arpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${defaultGateway}', hwdst='00:00:00:11:11:11', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100
    ${stream1}=  createStream  s3  Ether(src='00:22:33:44:55:66')/IP(proto=(60))  payload=incr  frameSize=100   numFrames=200
    ${stream2}=  createStream  s4  Ether(src='00:22:33:44:55:66')/IP(proto=(61))  payload=incr  frameSize=100   numFrames=100

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Port Group definitions.
    ${pg1def}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    @{pgPortList}=  Create List  ${tp1}  ${vp1}  ${tp2}

    AddPortGrp  ${nto1}  tpg1  ${pg1def}  ${pgPortList}

    AddFilter  ${nto1}  f1    PASS_ALL

    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tpg1}

    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${arpReply1}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request1.enc
    ${capturePcapArp1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request1.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp1}  1  6
    convertEncToPcap  ${captureFileArp1}  ${capturePcapArp1}
    tp1Ixia.scapyImportPcap  ${capturePcapArp1}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr1}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr1}=  replaceDashWithColon  ${localMacAddr1}


    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr1}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr1}=  replaceDashWithColon  ${remoteMacAddr1}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent1}=  catenate  Ether(type=2054, src='${localMacAddr1}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr1}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes1}=  convertStreamToBytes  ${ExpectedArpFrameSent1}
    ${ExpectedArpFrameSentBytes1}=  create list  ${ExpectedArpFrameSentBytes1}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes1}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###2.Send traffic , verify traffic and statistics  ###
    ###################################################################

    #Run traffic without capture
    np1Ixia.runIxTraffic  capture=${FALSE}
    sleep  5secs

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=300
        ...  np_total_rx_count_valid_packets=300
        ...  np_total_rx_count_bytes=30000
        ...  np_total_pass_count_packets=300
        ...  np_total_pass_count_bytes=30000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=300
        ...  df_total_insp_count_bytes=30000
        ...  df_total_pass_count_packets=300
        ...  df_total_pass_count_bytes=30000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=None
        ...  tp_total_pass_count_packets=None
        ...  tp_total_tx_count_packets=None
        ...  tp_total_tx_count_bytes=None
        ...  tp_total_gre_encap_count_packets=None
        ...  tp_total_gre_encap_percent_packets=None
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Run keyword if   ${actualTp1Stats["${key}"]} <= 0  fail  Load Balance was not done correctly

    # Verify Statistics on Tool Port
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=None
        ...  tp_total_pass_count_packets=None
        ...  tp_total_tx_count_packets=None
        ...  tp_total_tx_count_bytes=None
    ${actualTp2Stats}=  tp2.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Run keyword if   ${actualTp2Stats["${key}"]} <= 0  fail  Load Balance was not done correctly

    # Verify Statistics on Tool Port Group
    &{expectedTpg1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=300
        ...  tp_total_pass_count_packets=300
        ...  tp_total_tx_count_packets=300
    ${actualTpg1Stats}=  tpg1.getPortGrpStats  statName=all
    :FOR    ${key}    IN    @{expectedTpg1Stats.keys()}
    \    Log  ${expectedTpg1Stats["${key}"]}
    \    should be equal as integers    ${expectedTpg1Stats["${key}"]}     ${actualTpg1Stats["${key}"]}
    \    ${statSum}=  Evaluate  ${actualTp1Stats["${key}"]} + ${actualTp2Stats["${key}"]}
    \    should be equal as integers    ${actualTpg1Stats["${key}"]}    ${statSum}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001075997_No_ARP_reset_for_a_remote_end_ip_change_on_a_different_IP_subnet_if_the_original_remote_end_Ip_address_was_on_a_different_IP_subnet

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr}=  Set Variable  1.1.1.1
    ${remoteIPAddr}=   Set Variable  1.1.2.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${defaultGateway}', hwdst='00:00:00:11:11:11', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${defaultGateway}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ##########################################################
    ###2.Verify GRE configuration + change remote settings ###
    ##########################################################

    #Virt Port remote definitions
    ${remoteIPAddr}=   Set Variable  1.1.3.5

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Change Remote Settings for Virtual Port
    vp1.configurePort  remoteIP=${remoteIPAddr}
    sleep  2secs

    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}


    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001075999_No_ARP_reset_for_a_gateway_ip_address_change_if_the_remote_ip_address_is_on_the_same_subnet
    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr}=  Set Variable  1.1.1.1
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ##########################################################
    ###2.Verify GRE configuration + change local settings  ###
    ##########################################################

    #Virt Port remote definitions
    ${defaultGateway}=  Set Variable  1.1.1.250
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxStreams

    #Change Remote Settings for Virtual Port
    vp1.configurePort  localSettings=${vp1Def}
    sleep  2secs

    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}


    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001075980_Virtual_Port_Connection_Tab_migration
    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr}=  Set Variable  1.1.1.1
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Filter definitions
    ${f3def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}
    ${f4def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}
    ${f5def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}
    ${f6def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Filter creation
    AddFilter  ${nto1}  f1    DENY_ALL

    AddFilter  ${nto1}  f2    PASS_ALL

    AddFilter  ${nto1}  f3    PASS_BY_CRITERIA   ${f3def}

    AddFilter  ${nto1}  f4    DENY_BY_CRITERIA   ${f4def}

    AddFilter  ${nto1}  f5    PBC_UNMATCHED      ${f5def}

    AddFilter  ${nto1}  f6    DBC_MATCHED        ${f6def}

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    AddConn    ${nto1}  c2    ${np1}   ${f2}    ${tp1}

    AddConn    ${nto1}  c3    ${np1}   ${f3}    ${tp1}

    AddConn    ${nto1}  c4    ${np1}   ${f4}    ${tp1}

    AddConn    ${nto1}  c5    ${np1}   ${f5}    ${tp1}

    AddConn    ${nto1}  c6    ${np1}   ${f6}    ${tp1}

    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    nto1.initAllPortsAndFilters

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}


    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    vp1.configurePort

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ##########################################################
    ###2.Verify GRE configuration + change local settings  ###
    ##########################################################

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxStreams


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}


    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf3Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf3Stats}=  f3.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf3Stats.keys()}
    \    Log  ${expectedDf3Stats["${key}"]}
    \    should be equal as integers    ${expectedDf3Stats["${key}"]}     ${actualDf3Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf4Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf4Stats}=  f4.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf4Stats.keys()}
    \    Log  ${expectedDf4Stats["${key}"]}
    \    should be equal as integers    ${expectedDf4Stats["${key}"]}     ${actualDf4Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf5Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf5Stats}=  f5.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf5Stats.keys()}
    \    Log  ${expectedDf5Stats["${key}"]}
    \    should be equal as integers    ${expectedDf5Stats["${key}"]}     ${actualDf5Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf6Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf6Stats}=  f6.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf6Stats.keys()}
    \    Log  ${expectedDf6Stats["${key}"]}
    \    should be equal as integers    ${expectedDf6Stats["${key}"]}     ${actualDf6Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    #Reset Statistics
    nto1.resetStatsOnPorts
    nto1.resetStatsOnFilters
    sleep  5secs

    #Virtual Port definition
    ${vp2Def}=  Catenate  {'enabled': False, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Disable Tunneling on virtual port
    vp1.configurePort  localSettings=${vp2Def}

    sleep  5secs

    #Run traffic without capture
    np1Ixia.runIxTraffic  capture=${FALSE}
    sleep  5secs

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf3Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf3Stats}=  f3.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf3Stats.keys()}
    \    Log  ${expectedDf3Stats["${key}"]}
    \    should be equal as integers    ${expectedDf3Stats["${key}"]}     ${actualDf3Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf4Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf4Stats}=  f4.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf4Stats.keys()}
    \    Log  ${expectedDf4Stats["${key}"]}
    \    should be equal as integers    ${expectedDf4Stats["${key}"]}     ${actualDf4Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf5Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf5Stats}=  f5.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf5Stats.keys()}
    \    Log  ${expectedDf5Stats["${key}"]}
    \    should be equal as integers    ${expectedDf5Stats["${key}"]}     ${actualDf5Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf6Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf6Stats}=  f6.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf6Stats.keys()}
    \    Log  ${expectedDf6Stats["${key}"]}
    \    should be equal as integers    ${expectedDf6Stats["${key}"]}     ${actualDf6Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=10000
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001075992_Link-up_Partial_on_Virtual_Port_afterwards_Full_on_Virtual_Port_AND_TC001075993_TC001075994_TC001075995

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################


    #Virt Port local and remote definitions.
    ${localIPAddr}=  Set Variable  1.1.1.1
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Stream definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100
    ${stream2}=  createStream  s3  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxPorts
    tp1Ixia.configureIxStreams  True  1
    np1Ixia.configureIxStreams

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent


    #Verify Partial Link Status on PP
    &{expectedTp1LinkStatus}=  create dictionary
        ...  duplex=FULL
        ...  link_up=True
    &{actualTp1LinkStatus}=  tp1.getPortInfo
    &{actualTp1LinkStatus}=  evaluate  &{actualTp1LinkStatus}.get('link_status')
    :FOR    ${key}    IN    @{expectedTp1LinkStatus.keys()}
    \    Log  ${expectedTp1LinkStatus["${key}"]}
    \    should be equal as strings    ${expectedTp1LinkStatus["${key}"]}     ${actualTp1LinkStatus["${key}"]}

    #Verify Partial Link Status on VP
    &{expectedVp1LinkStatus}=  create dictionary
        ...  duplex=FULL
        ...  link_up=False
    &{actualVp1LinkStatus}=  vp1.getVirtPortInfo
    &{actualVp1LinkStatus}=  evaluate  &{actualVp1LinkStatus}.get('link_status')
    :FOR    ${key}    IN    @{expectedVp1LinkStatus.keys()}
    \    Log  ${expectedVp1LinkStatus["${key}"]}
    \    should be equal as strings    ${expectedVp1LinkStatus["${key}"]}     ${actualVp1LinkStatus["${key}"]}


    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    #Verify Full Link Status on PP
    &{expectedTp1LinkStatus}=  create dictionary
        ...  duplex=FULL
        ...  link_up=True
    &{actualTp1LinkStatus}=  tp1.getPortInfo
    &{actualTp1LinkStatus}=  evaluate  &{actualTp1LinkStatus}.get('link_status')
    :FOR    ${key}    IN    @{expectedTp1LinkStatus.keys()}
    \    Log  ${expectedTp1LinkStatus["${key}"]}
    \    should be equal as strings    ${expectedTp1LinkStatus["${key}"]}     ${actualTp1LinkStatus["${key}"]}

    #Verify Full Link Status on VP
    &{expectedVp1LinkStatus}=  create dictionary
        ...  duplex=FULL
        ...  link_up=True
    &{actualVp1LinkStatus}=  vp1.getVirtPortInfo
    &{actualVp1LinkStatus}=  evaluate  &{actualVp1LinkStatus}.get('link_status')
    :FOR    ${key}    IN    @{expectedVp1LinkStatus.keys()}
    \    Log  ${expectedVp1LinkStatus["${key}"]}
    \    should be equal as strings    ${expectedVp1LinkStatus["${key}"]}     ${actualVp1LinkStatus["${key}"]}


    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    #Stop traffic
    tp1ixia.stopIxTraffic

    #Wait until ARP Thread runs and removes the learned mac address, VP goes into Partial Link Status
    :FOR    ${i}    IN RANGE  0  360
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 112  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 359  fail  Less than 6 ARP REQs have been sent

    #Verify Partial Link Status on PP
    &{expectedTp1LinkStatus}=  create dictionary
        ...  duplex=FULL
        ...  link_up=True
    &{actualTp1LinkStatus}=  tp1.getPortInfo
    &{actualTp1LinkStatus}=  evaluate  &{actualTp1LinkStatus}.get('link_status')
    :FOR    ${key}    IN    @{expectedTp1LinkStatus.keys()}
    \    Log  ${expectedTp1LinkStatus["${key}"]}
    \    should be equal as strings    ${expectedTp1LinkStatus["${key}"]}     ${actualTp1LinkStatus["${key}"]}

    #Verify Partial Link Status on VP
    &{expectedVp1LinkStatus}=  create dictionary
        ...  duplex=FULL
        ...  link_up=False
    &{actualVp1LinkStatus}=  vp1.getVirtPortInfo
    &{actualVp1LinkStatus}=  evaluate  &{actualVp1LinkStatus}.get('link_status')
    :FOR    ${key}    IN    @{expectedVp1LinkStatus.keys()}
    \    Log  ${expectedVp1LinkStatus["${key}"]}
    \    should be equal as strings    ${expectedVp1LinkStatus["${key}"]}     ${actualVp1LinkStatus["${key}"]}

    #Clear Stats on Ports and Filters
    np1.resetPortStats
    tp1.resetPortStats
    f1.resetFilterStats

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    tp1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats
    tp1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream2}
    np1Ixia.configureIxStreams

    #Send Traffic into the VP while Partial Link Status
    #Run traffic without capture
    np1Ixia.runIxTraffic  capture=${FALSE}
    sleep  5secs

    :FOR    ${i}    IN RANGE  0  5
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 0  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 4   fail   Traffic passed Through Partial Link Down VP

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=0
        ...  tp_total_pass_count_packets=0
        ...  tp_total_tx_count_packets=0
        ...  tp_total_tx_count_bytes=0
        ...  tp_total_gre_encap_count_packets=0
        ...  tp_total_gre_encap_percent_packets=0
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001075984_Physical_Port_Tunneling_Configuration_is_kept_after_enabling_disabling_Tunnel_Origination
    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr}=  Set Variable  1.1.1.1
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Filter definitions
    ${f1def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_BY_CRITERIA   ${f1def}
    AddFilter  ${nto1}  f2    PBC_UNMATCHED

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    AddConn    ${nto1}  c2    ${np1}   ${f2}    ${tp1}


    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    nto1.initAllPortsAndFilters

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}


    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    vp1.configurePort

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxTraffic   capture=${FALSE}
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ##########################################################
    ###2.Verify GRE configuration + change local settings  ###
    ##########################################################

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream1}
    nto1.configureAllIxStreams


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}


    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    #Reset Statistics
    nto1.resetStatsOnPorts
    nto1.resetStatsOnFilters
    sleep  5secs

    #Virtual Port definition
    ${vp2Def}=  Catenate  {'enabled': False, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Disable Tunneling on virtual port
    vp1.configurePort  localSettings=${vp2Def}

    sleep  5secs

    #Run traffic without capture
    np1Ixia.runIxTraffic  capture=${FALSE}
    sleep  5secs

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=10000
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    ############################################################################################
    ###4.Send traffic , re-enable gre tunneling, verify encapsulated traffic and statistics  ###
    ############################################################################################

    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${gratArpReply2}=  createStream  s4  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Filter definitions for Port
    ${tp1def}=  catenate  {'filter_criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Sent Packets Stream Definition
    ${stream2}=  createStream  s3  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats
    tp1Ixia.deleteIxStreams
    tp1Ixia.clearIxPortStats

    #Reset Stats on ports and filters
    nto1.resetStatsOnPorts
    f1.resetFilterStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream2}
    tp1Ixia.addIxStreams  ${gratArpReply2}
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #Configure Filter on TP
    tp1.configurePortFilter  DENY_BY_CRITERIA  ${tp1def}

    #Enable GRE Origination
    vp1.configurePort  localSettings=${vp2Def}

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6  Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxTraffic   capture=${FALSE}
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    #################################
    ###5.Verify GRE configuration ###
    #################################

    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}


    ###################################################################
    ###6.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001076016_37_Virtual_Port_Restart

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr1}=  Set Variable  1.1.1.1
    ${localIPAddr2}=  Set Variable  1.1.1.2
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}
    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr2}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}


    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Filter definitions
    ${f1def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_BY_CRITERIA   ${f1def}
    AddFilter  ${nto1}  f2    PBC_UNMATCHED

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    AddConn    ${nto1}  c2    ${np1}   ${f2}    ${tp2}

    AddConn    ${nto1}  c3    ${np1}   ${f2}    ${tp1}

    AddConn    ${nto1}  c4    ${np1}   ${f1}    ${tp2}


    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    tp1Ixia.configureIxStreams  True  1

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    AddVirtPort  ${tp2}  vp2  ${vp2Def}  ${remoteIPAddr}

    #Configure VirtualPort
    vp1.configurePort
    vp2.configurePort

    #Disable Virtual Port
    vp2.disableVirtPort

    #Filter definitions for Port
    ${tpdef}=  catenate  {'filter_criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Configure Filter on TP
    tp1.configurePortFilter  DENY_BY_CRITERIA  ${tpdef}
    tp2.configurePortFilter  PASS_BY_CRITERIA  ${tpdef}

    #Clear Ixport Stats
    tp1Ixia.clearIxPortStats

    #Restart NTO
    nto1.restartNto

    #Re-discover Obj IDs
    nto1.rediscoverIds
    vp1.rediscoverVirtPortId
    vp2.rediscoverVirtPortId

    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 16   Exit For Loop
    \       sleep  1 sec
#    \    Run keyword if   ${i} == 29  fail  Less than 16 ARP REQs have been sent


    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream1}
    np1Ixia.configureIxStreams

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}
    log  ${tp1Ixia.recdFrames}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr1}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=0
        ...  tp_total_tx_count_packets=0
        ...  tp_total_tx_count_bytes=0
    ${actualTp2Stats}=  tp2.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Log  ${expectedTp2Stats["${key}"]}
    \    should be equal as integers    ${expectedTp2Stats["${key}"]}     ${actualTp2Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC00107601536_Virtual_Port_Clear_system

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr1}=  Set Variable  1.1.1.1
    ${localIPAddr2}=  Set Variable  1.1.1.2
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}
    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr2}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}


    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Filter definitions
    ${f1def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_BY_CRITERIA   ${f1def}
    AddFilter  ${nto1}  f2    PBC_UNMATCHED

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    AddConn    ${nto1}  c2    ${np1}   ${f2}    ${tp2}

    AddConn    ${nto1}  c3    ${np1}   ${f2}    ${tp1}

    AddConn    ${nto1}  c4    ${np1}   ${f1}    ${tp2}


    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    tp1Ixia.configureIxStreams  True  1

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    AddVirtPort  ${tp2}  vp2  ${vp2Def}  ${remoteIPAddr}

    #Configure VirtualPort
    vp1.configurePort
    vp2.configurePort

    #Disable Virtual Port
    vp2.disableVirtPort

    #Filter definitions for Port
    ${tpdef}=  catenate  {'filter_criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Configure Filter on TP
    tp1.configurePortFilter  DENY_BY_CRITERIA  ${tpdef}
    tp2.configurePortFilter  PASS_BY_CRITERIA  ${tpdef}


    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 16   Exit For Loop
    \       sleep  1 sec
#    \    Run keyword if   ${i} == 29  fail  Less than 16 ARP REQs have been sent


    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream1}
    np1Ixia.configureIxStreams

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}
    log  ${tp1Ixia.recdFrames}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr1}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=0
        ...  tp_total_tx_count_packets=0
        ...  tp_total_tx_count_bytes=0
    ${actualTp2Stats}=  tp2.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Log  ${expectedTp2Stats["${key}"]}
    \    should be equal as integers    ${expectedTp2Stats["${key}"]}     ${actualTp2Stats["${key}"]}

    #Clear system on NTO
    nto1.clearSystem

    #Re-set port IDs
    np1.rediscoverPortId
    tp1.rediscoverPortId
    tp2.rediscoverPortId

    #Verifying clear status enabled and mode
    ${np1Props}=  np1.getPortInfo
    ${np1Mode}=  evaluate  ${np1Props}.get('mode')
    ${np1Enabled}=  evaluate  ${np1Props}.get('enabled')
    ${expnp1Mode}=  catenate  NETWORK
    ${expnp1Enabled}=  catenate  False
    should be equal as strings  ${np1Mode}   ${expnp1Mode}
    should be equal as strings  ${np1Enabled}  ${expnp1Enabled}

    ${tp1Props}=  tp1.getPortInfo
    ${tp1Mode}=  evaluate  ${tp1Props}.get('mode')
    ${tp1Enabled}=  evaluate  ${np1Props}.get('enabled')
    ${exptp1Mode}=  catenate  NETWORK
    ${exptp1Enabled}=  catenate  False
    should be equal as strings  ${tp1Mode}  ${exptp1Mode}
    should be equal as strings  ${tp1Enabled}  ${exptp1Enabled}

    ${tp2Props}=  tp2.getPortInfo
    ${tp2Mode}=  evaluate  ${tp2Props}.get('mode')
    ${tp2Enabled}=  evaluate  ${np1Props}.get('enabled')
    ${exptp2Mode}=  catenate  NETWORK
    ${exptp2Enabled}=  catenate  False
    should be equal as strings  ${tp2Mode}  ${exptp2Mode}
    should be equal as strings  ${tp2Enabled}  ${exptp2Enabled}

    [Teardown]  NONE

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001108550_Virtual_Port_Clear_ports_and_filters

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr1}=  Set Variable  1.1.1.1
    ${localIPAddr2}=  Set Variable  1.1.1.2
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}
    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr2}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Filter definitions
    ${f1def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_BY_CRITERIA   ${f1def}
    AddFilter  ${nto1}  f2    PBC_UNMATCHED

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    AddConn    ${nto1}  c2    ${np1}   ${f2}    ${tp2}

    AddConn    ${nto1}  c3    ${np1}   ${f2}    ${tp1}

    AddConn    ${nto1}  c4    ${np1}   ${f1}    ${tp2}


    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    tp1Ixia.configureIxStreams  True  1

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    AddVirtPort  ${tp2}  vp2  ${vp2Def}  ${remoteIPAddr}

    #Configure VirtualPort
    vp1.configurePort
    vp2.configurePort

    #Disable Virtual Port
    vp2.disableVirtPort

    #Filter definitions for Port
    ${tpdef}=  catenate  {'filter_criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Configure Filter on TP
    tp1.configurePortFilter  DENY_BY_CRITERIA  ${tpdef}
    tp2.configurePortFilter  PASS_BY_CRITERIA  ${tpdef}


    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 16   Exit For Loop
    \       sleep  1 sec
#    \    Run keyword if   ${i} == 29  fail  Less than 16 ARP REQs have been sent


    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream1}
    np1Ixia.configureIxStreams

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback


    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}
    log  ${tp1Ixia.recdFrames}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr1}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=0
        ...  tp_total_tx_count_packets=0
        ...  tp_total_tx_count_bytes=0
    ${actualTp2Stats}=  tp2.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Log  ${expectedTp2Stats["${key}"]}
    \    should be equal as integers    ${expectedTp2Stats["${key}"]}     ${actualTp2Stats["${key}"]}

    #Clear ports and filters on NTO
    nto1.clearFiltersAndPorts

    #Re-set port IDs
    nto1.rediscoverIds

    #Verifying clear status enabled and mode
    ${np1Props}=  np1.getPortInfo
    ${np1Mode}=  evaluate  ${np1Props}.get('mode')
    ${np1Enabled}=  evaluate  ${np1Props}.get('enabled')
    ${expnp1Mode}=  catenate  NETWORK
    ${expnp1Enabled}=  catenate  False
    should be equal as strings  ${np1Mode}   ${expnp1Mode}
    should be equal as strings  ${np1Enabled}  ${expnp1Enabled}

    ${tp1Props}=  tp1.getPortInfo
    ${tp1Mode}=  evaluate  ${tp1Props}.get('mode')
    ${tp1Enabled}=  evaluate  ${np1Props}.get('enabled')
    ${exptp1Mode}=  catenate  NETWORK
    ${exptp1Enabled}=  catenate  False
    should be equal as strings  ${tp1Mode}  ${exptp1Mode}
    should be equal as strings  ${tp1Enabled}  ${exptp1Enabled}

    ${tp2Props}=  tp2.getPortInfo
    ${tp2Mode}=  evaluate  ${tp2Props}.get('mode')
    ${tp2Enabled}=  evaluate  ${np1Props}.get('enabled')
    ${exptp2Mode}=  catenate  NETWORK
    ${exptp2Enabled}=  catenate  False
    should be equal as strings  ${tp2Mode}  ${exptp2Mode}
    should be equal as strings  ${tp2Enabled}  ${exptp2Enabled}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001108549_Virtual_Port_Clear_configuration

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr1}=  Set Variable  1.1.1.1
    ${localIPAddr2}=  Set Variable  1.1.1.2
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}
    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr2}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}


    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Filter definitions
    ${f1def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_BY_CRITERIA   ${f1def}
    AddFilter  ${nto1}  f2    PBC_UNMATCHED

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    AddConn    ${nto1}  c2    ${np1}   ${f2}    ${tp2}

    AddConn    ${nto1}  c3    ${np1}   ${f2}    ${tp1}

    AddConn    ${nto1}  c4    ${np1}   ${f1}    ${tp2}


    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    tp1Ixia.configureIxStreams  True  1

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    AddVirtPort  ${tp2}  vp2  ${vp2Def}  ${remoteIPAddr}

    #Configure VirtualPort
    vp1.configurePort
    vp2.configurePort

    #Disable Virtual Port
    vp2.disableVirtPort

    #Filter definitions for Port
    ${tpdef}=  catenate  {'filter_criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Configure Filter on TP
    tp1.configurePortFilter  DENY_BY_CRITERIA  ${tpdef}
    tp2.configurePortFilter  PASS_BY_CRITERIA  ${tpdef}


    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 16   Exit For Loop
    \       sleep  1 sec
#    \    Run keyword if   ${i} == 29  fail  Less than 16 ARP REQs have been sent


    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream1}
    np1Ixia.configureIxStreams

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}
    log  ${tp1Ixia.recdFrames}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr1}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=0
        ...  tp_total_tx_count_packets=0
        ...  tp_total_tx_count_bytes=0
    ${actualTp2Stats}=  tp2.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Log  ${expectedTp2Stats["${key}"]}
    \    should be equal as integers    ${expectedTp2Stats["${key}"]}     ${actualTp2Stats["${key}"]}

    #Clear configuration on NTO
    nto1.clearConfig

    #Re-set port IDs
    np1.rediscoverPortId
    tp1.rediscoverPortId
    tp2.rediscoverPortId

    #Verifying clear status enabled and mode
    ${np1Props}=  np1.getPortInfo
    ${np1Mode}=  evaluate  ${np1Props}.get('mode')
    ${np1Enabled}=  evaluate  ${np1Props}.get('enabled')
    ${expnp1Mode}=  catenate  NETWORK
    ${expnp1Enabled}=  catenate  False
    should be equal as strings  ${np1Mode}   ${expnp1Mode}
    should be equal as strings  ${np1Enabled}  ${expnp1Enabled}

    ${tp1Props}=  tp1.getPortInfo
    ${tp1Mode}=  evaluate  ${tp1Props}.get('mode')
    ${tp1Enabled}=  evaluate  ${np1Props}.get('enabled')
    ${exptp1Mode}=  catenate  NETWORK
    ${exptp1Enabled}=  catenate  False
    should be equal as strings  ${tp1Mode}  ${exptp1Mode}
    should be equal as strings  ${tp1Enabled}  ${exptp1Enabled}

    ${tp2Props}=  tp2.getPortInfo
    ${tp2Mode}=  evaluate  ${tp2Props}.get('mode')
    ${tp2Enabled}=  evaluate  ${np1Props}.get('enabled')
    ${exptp2Mode}=  catenate  NETWORK
    ${exptp2Enabled}=  catenate  False
    should be equal as strings  ${tp2Mode}  ${exptp2Mode}
    should be equal as strings  ${tp2Enabled}  ${exptp2Enabled}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001076013_Import_Export_Full_export_and_Clear_ports_and_fields

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr1}=  Set Variable  1.1.1.1
    ${localIPAddr2}=  Set Variable  1.1.1.2
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}
    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr2}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}


    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100
    ${gratArpReply2}=  createStream  s3  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Filter definitions
    ${f1def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_BY_CRITERIA   ${f1def}
    AddFilter  ${nto1}  f2    PBC_UNMATCHED

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    AddConn    ${nto1}  c2    ${np1}   ${f2}    ${tp2}

    AddConn    ${nto1}  c3    ${np1}   ${f2}    ${tp1}

    AddConn    ${nto1}  c4    ${np1}   ${f1}    ${tp2}


    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    tp1Ixia.configureIxStreams  True  1

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    AddVirtPort  ${tp2}  vp2  ${vp2Def}  ${remoteIPAddr}

    #Configure VirtualPort
    vp1.configurePort
    vp2.configurePort

    #Disable Virtual Port
    vp2.disableVirtPort

    #Filter definitions for Port
    ${tpdef}=  catenate  {'filter_criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Configure Filter on TP
    tp1.configurePortFilter  DENY_BY_CRITERIA  ${tpdef}
    tp2.configurePortFilter  PASS_BY_CRITERIA  ${tpdef}


    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 16   Exit For Loop
    \       sleep  1 sec
#    \    Run keyword if   ${i} == 29  fail  Less than 16 ARP REQs have been sent


    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100
    ${stream2}=  createStream  s4  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream1}
    np1Ixia.configureIxStreams

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}
    log  ${tp1Ixia.recdFrames}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr1}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=0
        ...  tp_total_tx_count_packets=0
        ...  tp_total_tx_count_bytes=0
    ${actualTp2Stats}=  tp2.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Log  ${expectedTp2Stats["${key}"]}
    \    should be equal as integers    ${expectedTp2Stats["${key}"]}     ${actualTp2Stats["${key}"]}

    #Export Configuration
    ${exportFilePath}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/
    ${exportFile}=  nto1.exportConfig  ${exportFilePath}

    #Clear configuration on NTO
    nto1.clearFiltersAndPorts

    #clear ixia port stats
    tp1Ixia.clearIxPortStats
    tp1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats
    np1Ixia.deleteIxStreams

    np1Ixia.addIxStreams
    tp1Ixia.addIxStreams  ${gratArpReply2}
    nto1.configureAllIxPorts
    tp1Ixia.configureIxStreams  True  1
    np1Ixia.addIxStreams  ${stream2}
    np1Ixia.configureIxStreams

    #start capture
    tp1Ixia.startIxCapture

    #Import Configuration
    nto1.importConfig  ${exportFile}

    #Re-set port IDs
    np1.rediscoverPortId
    vp1.rediscoverVirtPortId
    vp2.rediscoverVirtPortId
    f1.rediscoverFilterId
    f2.rediscoverFilterId


    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 16   Exit For Loop
    \       sleep  1 sec
#    \    Run keyword if   ${i} == 29  fail  Less than 16 ARP REQs have been sent


    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Sent Packets Stream Definition
    ${stream5}=  createStream  s5  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream5}
    np1Ixia.configureIxStreams

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}
    log  ${tp1Ixia.recdFrames}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr1}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=0
        ...  tp_total_tx_count_packets=0
        ...  tp_total_tx_count_bytes=0
    ${actualTp2Stats}=  tp2.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Log  ${expectedTp2Stats["${key}"]}
    \    should be equal as integers    ${expectedTp2Stats["${key}"]}     ${actualTp2Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001076014_Import_Export_Traffic_config_export_and_Clear_configuration

    #Author: MSabadi

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    #################################
    ###1.Configure and Verify GRE ###
    #################################

    #Virt Port local and remote definitions.
    ${localIPAddr1}=  Set Variable  1.1.1.1
    ${localIPAddr2}=  Set Variable  1.1.1.2
    ${remoteIPAddr}=   Set Variable  1.1.1.4
    ${defaultGateway}=  Set Variable  1.1.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}
    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr2}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}


    #Arp Reply Stream Definition
    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100
    ${gratArpReply2}=  createStream  s3  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100

    #Filter definitions
    ${f1def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_BY_CRITERIA   ${f1def}
    AddFilter  ${nto1}  f2    PBC_UNMATCHED

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    AddConn    ${nto1}  c2    ${np1}   ${f2}    ${tp2}

    AddConn    ${nto1}  c3    ${np1}   ${f2}    ${tp1}

    AddConn    ${nto1}  c4    ${np1}   ${f1}    ${tp2}


    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${gratArpReply1}
    nto1.configureAllIxPorts
    tp1Ixia.configureIxStreams  True  1

    #Capture file definitions
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    sleep  2secs

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    AddVirtPort  ${tp2}  vp2  ${vp2Def}  ${remoteIPAddr}

    #Configure VirtualPort
    vp1.configurePort
    vp2.configurePort

    #Disable Virtual Port
    vp2.disableVirtPort

    #Filter definitions for Port
    ${tpdef}=  catenate  {'filter_criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}

    #Configure Filter on TP
    tp1.configurePortFilter  DENY_BY_CRITERIA  ${tpdef}
    tp2.configurePortFilter  PASS_BY_CRITERIA  ${tpdef}


    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 16   Exit For Loop
    \       sleep  1 sec
#    \    Run keyword if   ${i} == 29  fail  Less than 16 ARP REQs have been sent


    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Sent Packets Stream Definition
    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100
    ${stream2}=  createStream  s4  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream1}
    np1Ixia.configureIxStreams

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}
    log  ${tp1Ixia.recdFrames}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr1}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=0
        ...  tp_total_tx_count_packets=0
        ...  tp_total_tx_count_bytes=0
    ${actualTp2Stats}=  tp2.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Log  ${expectedTp2Stats["${key}"]}
    \    should be equal as integers    ${expectedTp2Stats["${key}"]}     ${actualTp2Stats["${key}"]}

    #Export Configuration
    ${exportFilePath}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/
    ${exportFile}=  nto1.exportConfig  ${exportFilePath}  exportType=TRAFFIC_CONFIG

    #Clear configuration on NTO
    nto1.clearConfig

    #clear ixia port stats
    tp1Ixia.clearIxPortStats
    tp1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats
    np1Ixia.deleteIxStreams

    np1Ixia.addIxStreams
    tp1Ixia.addIxStreams  ${gratArpReply2}
    nto1.configureAllIxPorts
    tp1Ixia.configureIxStreams  True  1
    np1Ixia.addIxStreams  ${stream2}
    np1Ixia.configureIxStreams

    #start capture
    tp1Ixia.startIxCapture

    #Import Configuration
    nto1.importConfig  ${exportFile}  importType=TRAFFIC_CONFIG

    #Re-set port IDs
    np1.rediscoverPortId
    vp1.rediscoverVirtPortId
    vp2.rediscoverVirtPortId
    f1.rediscoverFilterId
    f2.rediscoverFilterId


    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 16   Exit For Loop
    \       sleep  1 sec
#    \    Run keyword if   ${i} == 29  fail  Less than 16 ARP REQs have been sent


    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    sleep  2secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}


    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}

    ###################################################################
    ###3.Send traffic , verify encapsulated traffic and statistics  ###
    ###################################################################

    #Sent Packets Stream Definition
    ${stream5}=  createStream  s5  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100

    #Clear stream variables and ixPortStats
    np1Ixia.deleteIxStreams
    np1Ixia.clearIxPortStats

    #Stream creation and configuration on ixPorts
    np1Ixia.addIxStreams  ${stream5}
    np1Ixia.configureIxStreams

    #Capture File Definitions
    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}

    #Filter capture with only the desired packets
    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}

    #Import bytes into Ixia Port Object
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}
    log  ${tp1Ixia.recdFrames}

    #Emulate GRE encapsulation on sent frames
    ${pktProcessing}=  catenate  {
    ...  "tunnel_origination_settings":{
    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
    ...  "tunnel_origination_local_settings": {
    ...      "default_gateway": "${defaultGateway}",
    ...      "enabled": True,
    ...      "local_ip_address": "${localIPAddr1}",
    ...      "subnet_mask": "255.255.255.0" },
    ...  "tunnel_origination_remote_settings": {
    ...      "remote_ip_address": "${remoteIPAddr}",
    ...      "remote_mac_address": "${remoteMacAddr}" }
    ...  }}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}

    #Comparing frames and ignoring byte offset
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    #Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=100
        ...  np_total_rx_count_valid_packets=100
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=100
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=0
        ...  df_total_pass_count_bytes=0
        ...  df_total_deny_count_packets=100
        ...  df_total_deny_count_bytes=10000
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Dynamic Filter
    &{expectedDf2Stats}=  create dictionary
        ...  df_total_insp_count_packets=100
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=100
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf2Stats}=  f2.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
    \    Log  ${expectedDf2Stats["${key}"]}
    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=13800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Verify Statistics on Tool Port
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=100
        ...  tp_total_pass_count_packets=0
        ...  tp_total_tx_count_packets=0
        ...  tp_total_tx_count_bytes=0
    ${actualTp2Stats}=  tp2.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Log  ${expectedTp2Stats["${key}"]}
    \    should be equal as integers    ${expectedTp2Stats["${key}"]}     ${actualTp2Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown
#
#TC001107548_Virtual_Port_Change_QSFP_Mode
#
#    #Author: MSabadi
#
#    create directory  ${OUTPUTDIR}/${TEST NAME}/
#
#    #################################
#    ###1.Configure and Verify GRE ###
#    #################################
#
#    #Virt Port local and remote definitions.
#    ${localIPAddr1}=  Set Variable  1.1.1.1
#    ${localIPAddr2}=  Set Variable  1.1.1.2
#    ${remoteIPAddr}=   Set Variable  1.1.1.4
#    ${defaultGateway}=  Set Variable  1.1.1.254
#    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}
#    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr2}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}
#
#    #Arp Reply Stream Definition
#    ${gratArpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100
#    ${gratArpReply2}=  createStream  s3  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100
#
#    #Filter definitions
#    ${f1def}=  catenate  {'criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}
#
#    #Filter creation
#    AddFilter  ${nto1}  f1    PASS_BY_CRITERIA   ${f1def}
#    AddFilter  ${nto1}  f2    PBC_UNMATCHED
#
#    #Connection creation
#    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}
#
#    AddConn    ${nto1}  c2    ${np1}   ${f2}    ${tp2}
#
#    AddConn    ${nto1}  c3    ${np1}   ${f2}    ${tp1}
#
#    AddConn    ${nto1}  c4    ${np1}   ${f1}    ${tp2}
#
#    #Setup cleanup and initialization
#    nto1.setupNVSDevices
#    nto1.initAllIxPorts
#
#    #Stream creation and configuration on ports
#    tp1Ixia.addIxStreams  ${gratArpReply1}
#    nto1.configureAllIxPorts
#    tp1Ixia.configureIxStreams  True  1
#
#    #Capture file definitions
#    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.enc
#    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/arp_request.pcap
#
#    #Start capture on Ixia Port
#    tp1Ixia.startIxCapture
#    sleep  2secs
#
#    #NTO port and connection configuration
#    nto1.initAllPortsAndFilters
#
#    #Virt Port assignemnt
#    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
#    AddVirtPort  ${tp2}  vp2  ${vp2Def}  ${remoteIPAddr}
#
#    #Configure VirtualPort
#    vp1.configurePort
#    vp2.configurePort
#
#    #Disable Virtual Port
#    vp2.disableVirtPort
#
#    #Filter definitions for Port
#    ${tpdef}=  catenate  {'filter_criteria': {'logical_operation': "AND",'vlan': {'priority': 0,'vlan_id': '5'}}}
#
#    #Configure Filter on TP
#    tp1.configurePortFilter  DENY_BY_CRITERIA  ${tpdef}
#    tp2.configurePortFilter  PASS_BY_CRITERIA  ${tpdef}
#
#
#    #Should wait until the the VP sends ARP packets
#    :FOR    ${i}    IN RANGE  0  30
#    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
#    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 16   Exit For Loop
#    \       sleep  1 sec
##    \    Run keyword if   ${i} == 29  fail  Less than 16 ARP REQs have been sent
#
#
#    #Run traffic from Ixia Port
#    tp1Ixia.runIxContinuousTraffic
#    sleep  2secs
#
#    #Stop capture on Ixia Port
#    tp1Ixia.stopIxCapture
#    sleep  2secs
#
#    #Download the capture from IxOS
#    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
#    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
#    tp1Ixia.scapyImportPcap  ${capturePcapArp}
#
#
#    # Get MAC Addr from phy port -> local Mac Addr
#    ${tp1Props}=  tp1.getPortInfo
#    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
#    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}
#
#    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
#    ${vp1Props}=  vp1.getVirtPortInfo
#    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
#    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}
#
#    #Creating expected sent arp request
#    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
#    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
#    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}
#
#    #Compare download capture with previously created packets
#    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}
#
#    ###################################################################
#    ###3.Send traffic , verify encapsulated traffic and statistics  ###
#    ###################################################################
#
#    #Sent Packets Stream Definition
#    ${stream1}=  createStream  s2  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100
#    ${stream2}=  createStream  s4  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100
#
#    #Clear stream variables and ixPortStats
#    np1Ixia.deleteIxStreams
#    np1Ixia.clearIxPortStats
#
#    #Stream creation and configuration on ixPorts
#    np1Ixia.addIxStreams  ${stream1}
#    np1Ixia.configureIxStreams
#
#    #Capture File Definitions
#    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
#    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
#    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
#    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
#    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap
#
#    #Start capture for original capture
#    np1Ixia.enableInternalLoopback
#    sleep  10secs
#
#    #Start cature on Ixia Port
#    tp1Ixia.startIxCapture
#    sleep  3secs
#
#    #Run traffic without capture
#    np1Ixia.runIxTraffic
#    sleep  5secs
#
#    #Stop capture on Ixia Port
#    tp1Ixia.stopIxCapture
#    sleep  2secs
#
#    #Stop traffic
#    tp1ixia.stopIxTraffic
#
#    #copy roginal capture
#    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
#    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
#    np1Ixia.disableInternalLoopback
#
#    #Download the capture from IxOS
#    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
#    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
#
#    #Filter capture with only the desired packets
#    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}
#
#    #Import bytes into Ixia Port Object
#    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}
#    log  ${tp1Ixia.recdFrames}
#
#    #Emulate GRE encapsulation on sent frames
#    ${pktProcessing}=  catenate  {
#    ...  "tunnel_origination_settings":{
#    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
#    ...  "tunnel_origination_local_settings": {
#    ...      "default_gateway": "${defaultGateway}",
#    ...      "enabled": True,
#    ...      "local_ip_address": "${localIPAddr1}",
#    ...      "subnet_mask": "255.255.255.0" },
#    ...  "tunnel_origination_remote_settings": {
#    ...      "remote_ip_address": "${remoteIPAddr}",
#    ...      "remote_mac_address": "${remoteMacAddr}" }
#    ...  }}
#
#    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}
#
#    #Comparing frames and ignoring byte offset
#    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}
#
#    #Verify Statistics on Network Port
#    &{expectedNp1Stats}=  create dictionary
#        ...  np_total_rx_count_packets=100
#        ...  np_total_rx_count_valid_packets=100
#        ...  np_total_rx_count_bytes=10000
#        ...  np_total_pass_count_packets=100
#        ...  np_total_pass_count_bytes=10000
#        ...  np_total_deny_count_packets=0
#        ...  np_total_deny_count_bytes=0
#     ${actualNp1Stats}=  np1.getPortStats  statName=all
#    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
#    \    Log  ${expectedNp1Stats["${key}"]}
#    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
#
#    # Verify Statistics on Dynamic Filter
#    &{expectedDf1Stats}=  create dictionary
#        ...  df_total_insp_count_packets=100
#        ...  df_total_insp_count_bytes=10000
#        ...  df_total_pass_count_packets=0
#        ...  df_total_pass_count_bytes=0
#        ...  df_total_deny_count_packets=100
#        ...  df_total_deny_count_bytes=10000
#    ${actualDf1Stats}=  f1.getFilterStats  statName=all
#    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
#    \    Log  ${expectedDf1Stats["${key}"]}
#    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
#
#    # Verify Statistics on Dynamic Filter
#    &{expectedDf2Stats}=  create dictionary
#        ...  df_total_insp_count_packets=100
#        ...  df_total_insp_count_bytes=10000
#        ...  df_total_pass_count_packets=100
#        ...  df_total_pass_count_bytes=10000
#        ...  df_total_deny_count_packets=0
#        ...  df_total_deny_count_bytes=0
#    ${actualDf2Stats}=  f2.getFilterStats  statName=all
#    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
#    \    Log  ${expectedDf2Stats["${key}"]}
#    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}
#
#    # Verify Statistics on Tool Port
#    &{expectedTp1Stats}=  create dictionary
#        ...  tp_total_insp_count_packets=100
#        ...  tp_total_pass_count_packets=100
#        ...  tp_total_tx_count_packets=100
#        ...  tp_total_tx_count_bytes=13800
#        ...  tp_total_gre_encap_count_packets=100
#        ...  tp_total_gre_encap_percent_packets=1
#    ${actualTp1Stats}=  tp1.getPortStats  statName=all
#    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
#    \    Log  ${expectedTp1Stats["${key}"]}
#    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}
#
#    # Verify Statistics on Tool Port
#    &{expectedTp2Stats}=  create dictionary
#        ...  tp_total_insp_count_packets=100
#        ...  tp_total_pass_count_packets=0
#        ...  tp_total_tx_count_packets=0
#        ...  tp_total_tx_count_bytes=0
#    ${actualTp2Stats}=  tp2.getPortStats  statName=all
#    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
#    \    Log  ${expectedTp2Stats["${key}"]}
#    \    should be equal as integers    ${expectedTp2Stats["${key}"]}     ${actualTp2Stats["${key}"]}
#
#    #Export Configuration
#    ${exportFilePath}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/
#    ${exportFile}=  nto1.exportConfig  ${exportFilePath}  exportType=TRAFFIC_CONFIG
#
#    #Change QSFP mode
#    nto1.changeQSFPMode  LC3
#
#    #re set id for default ports
#    np1.rediscoverPortId
#    tp1.rediscoverPortId
#    tp2.rediscoverPortId
#
#    #Verifying clear status enabled and mode
#    ${np1Props}=  np1.getPortInfo
#    ${np1Mode}=  evaluate  ${np1Props}.get('mode')
#    ${np1Enabled}=  evaluate  ${np1Props}.get('enabled')
#    ${expnp1Mode}=  catenate  NETWORK
#    ${expnp1Enabled}=  catenate  False
#    should be equal as strings  ${np1Mode}   ${expnp1Mode}
#    should be equal as strings  ${np1Enabled}  ${expnp1Enabled}
#
#    ${tp1Props}=  tp1.getPortInfo
#    ${tp1Mode}=  evaluate  ${tp1Props}.get('mode')
#    ${tp1Enabled}=  evaluate  ${np1Props}.get('enabled')
#    ${exptp1Mode}=  catenate  NETWORK
#    ${exptp1Enabled}=  catenate  False
#    should be equal as strings  ${tp1Mode}  ${exptp1Mode}
#    should be equal as strings  ${tp1Enabled}  ${exptp1Enabled}
#
#    ${tp2Props}=  tp2.getPortInfo
#    ${tp2Mode}=  evaluate  ${tp2Props}.get('mode')
#    ${tp2Enabled}=  evaluate  ${np1Props}.get('enabled')
#    ${exptp2Mode}=  catenate  NETWORK
#    ${exptp2Enabled}=  catenate  False
#    should be equal as strings  ${tp2Mode}  ${exptp2Mode}
#    should be equal as strings  ${tp2Enabled}  ${exptp2Enabled}
#
#    #Change QSFP mode
#    nto1.changeQSFPMode  LC3
#
#    #clear ixia port stats
#    tp1Ixia.clearIxPortStats
#    tp1Ixia.deleteIxStreams
#    np1Ixia.clearIxPortStats
#    np1Ixia.deleteIxStreams
#
#    np1Ixia.addIxStreams
#    tp1Ixia.addIxStreams  ${gratArpReply2}
#    nto1.configureAllIxPorts
#    tp1Ixia.configureIxStreams  True  1
#    np1Ixia.addIxStreams  ${stream2}
#    np1Ixia.configureIxStreams
#
#    #start capture
#    tp1Ixia.startIxCapture
#
#    #Import Configuration
#    nto1.importConfig  ${exportFile}  importType=TRAFFIC_CONFIG
#
#    #Re-set port IDs
#    np1.rediscoverPortId
#    vp1.rediscoverVirtPortId
#    vp2.rediscoverVirtPortId
#    f1.rediscoverFilterId
#    f2.rediscoverFilterId
#
#
#    #Should wait until the the VP sends ARP packets
#    :FOR    ${i}    IN RANGE  0  30
#    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
#    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxFrames"]} <= 16   Exit For Loop
#    \       sleep  1 sec
##    \    Run keyword if   ${i} == 29  fail  Less than 16 ARP REQs have been sent
#
#
#    #Run traffic from Ixia Port
#    tp1Ixia.runIxContinuousTraffic
#    sleep  2secs
#
#    #Stop capture on Ixia Port
#    tp1Ixia.stopIxCapture
#    sleep  2secs
#
#    #Download the capture from IxOS
#    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
#    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
#    tp1Ixia.scapyImportPcap  ${capturePcapArp}
#
#
#    # Get MAC Addr from phy port -> local Mac Addr
#    ${tp1Props}=  tp1.getPortInfo
#    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
#    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}
#
#    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
#    ${vp1Props}=  vp1.getVirtPortInfo
#    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
#    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}
#
#    #Creating expected sent arp request
#    ${ExpectedArpFrameSent}=  catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
#    ${ExpectedArpFrameSentBytes}=  convertStreamToBytes  ${ExpectedArpFrameSent}
#    ${ExpectedArpFrameSentBytes}=  create list  ${ExpectedArpFrameSentBytes}
#
#    #Compare download capture with previously created packets
#    compareFrames  ${ExpectedArpFrameSentBytes}  ${tp1Ixia.recdFrames}  ordered=${False}
#
#    ###################################################################
#    ###3.Send traffic , verify encapsulated traffic and statistics  ###
#    ###################################################################
#
#    #Sent Packets Stream Definition
#    ${stream5}=  createStream  s5  Ether(src='00:22:33:44:55:66')/IP()  payload=incr  frameSize=100   numFrames=100
#
#    #Clear stream variables and ixPortStats
#    np1Ixia.deleteIxStreams
#    np1Ixia.clearIxPortStats
#
#    #Stream creation and configuration on ixPorts
#    np1Ixia.addIxStreams  ${stream5}
#    np1Ixia.configureIxStreams
#
#    #Capture File Definitions
#    ${captureGrePktEncFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.enc
#    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream.pcap
#    ${captureGrePktPcapFileFiltered}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/gre_encap_stream_filtered.pcap
#    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
#    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap
#
#    #Start capture for original capture
#    np1Ixia.enableInternalLoopback
#    sleep  10secs
#
#    #Start cature on Ixia Port
#    tp1Ixia.startIxCapture
#    sleep  3secs
#
#    #Run traffic without capture
#    np1Ixia.runIxTraffic
#    sleep  5secs
#
#    #Stop capture on Ixia Port
#    tp1Ixia.stopIxCapture
#    sleep  2secs
#
#    #Stop traffic
#    tp1ixia.stopIxTraffic
#
#    #copy roginal capture
#    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
#    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
#    np1Ixia.disableInternalLoopback
#
#    #Download the capture from IxOS
#    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  10000
#    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
#
#    #Filter capture with only the desired packets
#    filterCapture  ${captureGrePktPcapFile}  'eth.type == 0x0800'  ${captureGrePktPcapFileFiltered}
#
#    #Import bytes into Ixia Port Object
#    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFileFiltered}
#    log  ${tp1Ixia.recdFrames}
#
#    #Emulate GRE encapsulation on sent frames
#    ${pktProcessing}=  catenate  {
#    ...  "tunnel_origination_settings":{
#    ...  "tunnel_mac": {"mac_address": "${localMacAddr}" },
#    ...  "tunnel_origination_local_settings": {
#    ...      "default_gateway": "${defaultGateway}",
#    ...      "enabled": True,
#    ...      "local_ip_address": "${localIPAddr1}",
#    ...      "subnet_mask": "255.255.255.0" },
#    ...  "tunnel_origination_remote_settings": {
#    ...      "remote_ip_address": "${remoteIPAddr}",
#    ...      "remote_mac_address": "${remoteMacAddr}" }
#    ...  }}
#
#    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessing}  pcapFile=${origCapturePcap}
#
#    #Comparing frames and ignoring byte offset
#    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}
#
#    #Verify Statistics on Network Port
#    &{expectedNp1Stats}=  create dictionary
#        ...  np_total_rx_count_packets=100
#        ...  np_total_rx_count_valid_packets=100
#        ...  np_total_rx_count_bytes=10000
#        ...  np_total_pass_count_packets=100
#        ...  np_total_pass_count_bytes=10000
#        ...  np_total_deny_count_packets=0
#        ...  np_total_deny_count_bytes=0
#     ${actualNp1Stats}=  np1.getPortStats  statName=all
#    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
#    \    Log  ${expectedNp1Stats["${key}"]}
#    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
#
#    # Verify Statistics on Dynamic Filter
#    &{expectedDf1Stats}=  create dictionary
#        ...  df_total_insp_count_packets=100
#        ...  df_total_insp_count_bytes=10000
#        ...  df_total_pass_count_packets=0
#        ...  df_total_pass_count_bytes=0
#        ...  df_total_deny_count_packets=100
#        ...  df_total_deny_count_bytes=10000
#    ${actualDf1Stats}=  f1.getFilterStats  statName=all
#    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
#    \    Log  ${expectedDf1Stats["${key}"]}
#    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
#
#    # Verify Statistics on Dynamic Filter
#    &{expectedDf2Stats}=  create dictionary
#        ...  df_total_insp_count_packets=100
#        ...  df_total_insp_count_bytes=10000
#        ...  df_total_pass_count_packets=100
#        ...  df_total_pass_count_bytes=10000
#        ...  df_total_deny_count_packets=0
#        ...  df_total_deny_count_bytes=0
#    ${actualDf2Stats}=  f2.getFilterStats  statName=all
#    :FOR    ${key}    IN    @{expectedDf2Stats.keys()}
#    \    Log  ${expectedDf2Stats["${key}"]}
#    \    should be equal as integers    ${expectedDf2Stats["${key}"]}     ${actualDf2Stats["${key}"]}
#
#    # Verify Statistics on Tool Port
#    &{expectedTp1Stats}=  create dictionary
#        ...  tp_total_insp_count_packets=100
#        ...  tp_total_pass_count_packets=100
#        ...  tp_total_tx_count_packets=100
#        ...  tp_total_tx_count_bytes=13800
#        ...  tp_total_gre_encap_count_packets=100
#        ...  tp_total_gre_encap_percent_packets=1
#    ${actualTp1Stats}=  tp1.getPortStats  statName=all
#    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
#    \    Log  ${expectedTp1Stats["${key}"]}
#    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}
#
#    # Verify Statistics on Tool Port
#    &{expectedTp2Stats}=  create dictionary
#        ...  tp_total_insp_count_packets=100
#        ...  tp_total_pass_count_packets=0
#        ...  tp_total_tx_count_packets=0
#        ...  tp_total_tx_count_bytes=0
#    ${actualTp2Stats}=  tp2.getPortStats  statName=all
#    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
#    \    Log  ${expectedTp2Stats["${key}"]}
#    \    should be equal as integers    ${expectedTp2Stats["${key}"]}     ${actualTp2Stats["${key}"]}
#
#    # Cleaup the devices and ports
#    # Done during *** *** settings *** test teardown

TC001076002_23_-_Filtering_-_PBC_on_Phy_Port_before_enabling_Virtual_Port
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}

    #Stream Definition
    ${s1-stream-pass}=  createStream  s1-stream-pass  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/Dot1Q(vlan=100)/IP(src="10.0.0.1", dst="10.0.0.2")/UDP(sport=10001, dport=20002)  payload=incr
                        ...  frameSize=1000   numFrames=100
    ${s2-arpReply}=     createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='00:00:00:11:11:11', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1
    ${s3-stream-deny}=  createStream  s3-stream-deny  Ether(src='00:00:03:00:00:01', dst='00:00:03:00:00:02')/Dot1Q(vlan=300)/IP(src="30.0.0.1", dst="30.0.0.2")/UDP(sport=30001, dport=40002)  payload=incr
                        ...  frameSize=1000   numFrames=10

    ${fdef}=  Catenate  {'filter_criteria': {'logical_operation': 'AND',
    ...  'mac_src': {'addr': ["00-00-01-00-00-01"]},
    ...  'ipv4_src': {'addr': ["10.0.0.1"]},
    ...  "layer4_src_port": {"port": "10001"}
    ...  }}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Configure filter criteria
    tp1.configurePortFilter  PASS_BY_CRITERIA  ${fdef}

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream-pass}  ${s3-stream-deny}
    tp1Ixia.addIxStreams  ${s2-arpReply}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    # Start capture to receive ARP Request sent by VP
    tp1Ixia.startIxCapture

    # AddVirtPort  and configure it with <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    vp1.configurePort
    sleep  10secs
    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs

    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  120
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${False}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=110
        ...  np_total_rx_count_valid_packets=110
        ...  np_total_rx_count_bytes=110000
        ...  np_total_pass_count_packets=110
        ...  np_total_pass_count_bytes=110000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=110
        ...  df_total_insp_count_bytes=110000
        ...  df_total_pass_count_packets=110
        ...  df_total_pass_count_bytes=110000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=110
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=103800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Used in **settings** Test Teardown

TC001076003_24_-_Filtering_-_PBC_on_Virtual_Port_change_to_another_type_of_filtering
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}

    #Stream Definition
    ${s1-stream-pass}=  createStream  s1-stream-pass  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/Dot1Q(vlan=100)/IP(src="10.0.0.1", dst="10.0.0.2")/UDP(sport=10001, dport=20002)  payload=incr
                        ...  frameSize=1000   numFrames=100
    ${s2-arpReply}=     createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='00:00:00:11:11:11', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1
    ${s3-stream-deny}=  createStream  s3-stream-deny  Ether(src='00:00:03:00:00:01', dst='00:00:03:00:00:02')/Dot1Q(vlan=300)/IP(src="30.0.0.1", dst="30.0.0.2")/UDP(sport=30001, dport=40002)  payload=incr
                        ...  frameSize=1000   numFrames=10

    ${fdef}=  Catenate  {'filter_criteria': {'logical_operation': 'AND',
    ...  'mac_src': {'addr': ["00-00-01-00-00-01"]},
    ...  'ipv4_src': {'addr': ["10.0.0.1"]},
    ...  "layer4_src_port": {"port": "10001"}
    ...  }}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Configure filter criteria
    tp1.configurePortFilter  PASS_BY_CRITERIA  ${fdef}

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream-pass}  ${s3-stream-deny}
    tp1Ixia.addIxStreams  ${s2-arpReply}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    # Start capture to receive ARP Request sent by VP
    tp1Ixia.startIxCapture

    # AddVirtPort  and configure it with <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    vp1.configurePort
    sleep  10 secs
    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_s1.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_s1.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp_s1.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  120
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${False}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=110
        ...  np_total_rx_count_valid_packets=110
        ...  np_total_rx_count_bytes=110000
        ...  np_total_pass_count_packets=110
        ...  np_total_pass_count_bytes=110000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=110
        ...  df_total_insp_count_bytes=110000
        ...  df_total_pass_count_packets=110
        ...  df_total_pass_count_bytes=110000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=110
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=103800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Subtest 2
    log  Subtest #2 - filter criteria is changed
    #Filter the traffic for the other stream: s3-stream-deny and will deny s1-stream-pass
    ${fdef2}=  Catenate  {'filter_criteria': {'logical_operation': 'AND',
    ...  'mac_src': {'addr': ["00-00-03-00-00-01"]},
    ...  'ipv4_src': {'addr': ["30.0.0.1"]},
    ...  "layer4_src_port": {"port": "30001"}
    ...  }}
    tp1.configurePortFilter  PASS_BY_CRITERIA  ${fdef2}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_s3.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_s3.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp_s3.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  120
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${False}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=110
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Used in **settings** Test Teardown

TC001076004_25_-_Filtering_-_DBC_on_Phy_Port_before_enabling_Virtual_Port
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}

    #Stream Definition
    ${s1-stream-pass}=  createStream  s1-stream-pass  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/Dot1Q(vlan=100)/IP(src="10.0.0.1", dst="10.0.0.2")/UDP(sport=10001, dport=20002)  payload=incr
                        ...  frameSize=1000   numFrames=100
    ${s2-arpReply}=     createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='00:00:00:11:11:11', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1
    ${s3-stream-deny}=  createStream  s3-stream-deny  Ether(src='00:00:03:00:00:01', dst='00:00:03:00:00:02')/Dot1Q(vlan=300)/IP(src="30.0.0.1", dst="30.0.0.2")/UDP(sport=30001, dport=40002)  payload=incr
                        ...  frameSize=1000   numFrames=10


    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
     ${fdef}=  Catenate  {'filter_criteria': {'logical_operation': 'AND',
    ...  'mac_src': {'addr': ["00-00-03-00-00-01"]},
    ...  'ipv4_src': {'addr': ["30.0.0.1"]},
    ...  "layer4_src_port": {"port": "30001"}
    ...  }}
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Configure filter criteria
    tp1.configurePortFilter  DENY_BY_CRITERIA  ${fdef}

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream-pass}  ${s3-stream-deny}
    tp1Ixia.addIxStreams  ${s2-arpReply}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    # Start capture to receive ARP Request sent by VP
    tp1Ixia.startIxCapture

    # AddVirtPort  and configure it with <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    vp1.configurePort
    sleep  10 secs
    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  120
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${False}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=110
        ...  np_total_rx_count_valid_packets=110
        ...  np_total_rx_count_bytes=110000
        ...  np_total_pass_count_packets=110
        ...  np_total_pass_count_bytes=110000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=110
        ...  df_total_insp_count_bytes=110000
        ...  df_total_pass_count_packets=110
        ...  df_total_pass_count_bytes=110000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=110
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=103800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Used in **settings** Test Teardown

TC001076005_26_-_Filtering_-_DBC_on_Virtual_Port_change_to_another_type_of_filtering
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}

    #Stream Definition
    ${s1-stream-pass}=  createStream  s1-stream-pass  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/Dot1Q(vlan=100)/IP(src="10.0.0.1", dst="10.0.0.2")/UDP(sport=10001, dport=20002)  payload=incr
                        ...  frameSize=1000   numFrames=100
    ${s2-arpReply}=     createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='00:00:00:11:11:11', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1
    ${s3-stream-deny}=  createStream  s3-stream-deny  Ether(src='00:00:03:00:00:01', dst='00:00:03:00:00:02')/Dot1Q(vlan=300)/IP(src="30.0.0.1", dst="30.0.0.2")/UDP(sport=30001, dport=40002)  payload=incr
                        ...  frameSize=1000   numFrames=10

     #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    nto1.initAllIxPorts

    #Configure filter criteria
    ${fdef}=  Catenate  {'filter_criteria': {'logical_operation': 'AND',
    ...  'mac_src': {'addr': ["00-00-03-00-00-01"]},
    ...  'ipv4_src': {'addr': ["30.0.0.1"]},
    ...  "layer4_src_port": {"port": "30001"}
    ...  }}
    tp1.configurePortFilter  DENY_BY_CRITERIA  ${fdef}

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream-pass}  ${s3-stream-deny}
    tp1Ixia.addIxStreams  ${s2-arpReply}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    # Start capture to receive ARP Request sent by VP
    tp1Ixia.startIxCapture

    # AddVirtPort  and configure it with <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    vp1.configurePort
    sleep  10 secs
    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_s1.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_s1.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp_s1.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  120
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${False}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=110
        ...  np_total_rx_count_valid_packets=110
        ...  np_total_rx_count_bytes=110000
        ...  np_total_pass_count_packets=110
        ...  np_total_pass_count_bytes=110000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=110
        ...  df_total_insp_count_bytes=110000
        ...  df_total_pass_count_packets=110
        ...  df_total_pass_count_bytes=110000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=110
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=103800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Subtest 2
    log  Subtest #2 - filter criteria is changed
    log to console  Subtest #2 - filter criteria is changed
    #Filter the traffic for the other stream: s3-stream-deny and will deny s1-stream-pass
    ${fdef2}=  Catenate  {'filter_criteria': {'logical_operation': 'AND',
    ...  'mac_src': {'addr': ["00-00-01-00-00-01"]},
    ...  'ipv4_src': {'addr': ["10.0.0.1"]},
    ...  "layer4_src_port": {"port": "10001"}
    ...  }}
    tp1.configurePortFilter  DENY_BY_CRITERIA  ${fdef2}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_s3.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_s3.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp_s3.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  120
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${False}

    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=110
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Used from *** Settings *** Test Teardown

TC001076006_27_-_Virtual_Port_Configuration_-_valid_local/remote_ip_configuration_AND_TC001076024_AND_TC001075981_AND_TC001076007
    #Author: OBalas
    #TODO: to be reviewed when this bug is fixed ENH1439907 [NTO 4.7.1 ][GREOrigination] Local settings and Remote settings IP validation

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}

     #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters

    # AddVirtPort  and configure it with <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    vp1.configurePort
    sleep  10 secs

    # Valitate the vp cannot configure the following local Ip addr, local gw and remote ip

    # Test 1
    log  Test 1 - positive
    ${localSettings}=  Catenate  {'enabled': True, 'local_ip_address': '0.0.0.1', 'subnet_mask': '255.0.0.0',
                            ...  'default_gateway': '0.0.0.2' }
    ${remoteIPAddr}=  set variable  0.0.0.3
    run keyword   vp1.configurePort  remoteIP=${remoteIPAddr}  localSettings=${localSettings}

    # Test 2
    log  Test 2 - negative
    ${localSettings}=  Catenate  {'enabled': True, 'local_ip_address': '333.333.33.33', 'subnet_mask': '255.0.0.0',
                            ...  'default_gateway': '333.333.33.1' }
    ${remoteIPAddr}=  set variable  333.333.33.2
    run keyword and expect error  *ERROR*     vp1.configurePort  remoteIP=${remoteIPAddr}  localSettings=${localSettings}

    # Test 3
    log  Test 3 - negative
    ${localSettings}=  Catenate  {'enabled': True, 'local_ip_address': '255.255.255.255', 'subnet_mask': '255.255.255.255',
                            ...  'default_gateway': '255.255.255.255' }
    ${remoteIPAddr}=  set variable  255.255.255.255
    run keyword and expect error  *The subnet mask may not be all one*    vp1.configurePort  localSettings=${localSettings}
    run keyword  vp1.configurePort  remoteIP=${remoteIPAddr}

    # Test 4
    log  Test 4 - positive
    ${localSettings}=  Catenate  {'enabled': True, 'local_ip_address': '224.0.0.1', 'subnet_mask': '255.0.0.0',
                            ...  'default_gateway': '224.0.0.2' }
    ${remoteIPAddr}=  set variable  224.0.0.3
    run keyword  vp1.configurePort  localSettings=${localSettings}
    run keyword  vp1.configurePort  remoteIP=${remoteIPAddr}

    # Test 5
    log  Test 5 - positive
    ${localSettings}=  Catenate  {'enabled': True, 'local_ip_address': '127.0.0.1', 'subnet_mask': '255.0.0.0',
                            ...  'default_gateway': '127.0.0.2' }
    ${remoteIPAddr}=  set variable  127.0.0.3
    run keyword   vp1.configurePort  localSettings=${localSettings}
    run keyword   vp1.configurePort  remoteIP=${remoteIPAddr}

    # Test 6
    log  Test 6 - positive
    ${localSettings}=  Catenate  {'enabled': True, 'local_ip_address': '169.254.1.1', 'subnet_mask': '255.0.0.0',
                            ...  'default_gateway': '169.254.1.2' }
    ${remoteIPAddr}=  set variable  169.254.1.3
    run keyword  vp1.configurePort  localSettings=${localSettings}
    run keyword  vp1.configurePort  remoteIP=${remoteIPAddr}

    # Test 7 TC001076024
    log  Test 7 - negative
    ${localSettings}=  Catenate  {'enabled': True, 'local_ip_address': '1.2.3.4', 'subnet_mask': '255.0.0.0',
                            ...  'default_gateway': '0.0.0.0' }
    ${remoteIPAddr}=  set variable  5.6.7.8
    run keyword and expect error  *The Default Gateway address must be four numeric values (0 to 255) separated by periods*    vp1.configurePort  localSettings=${localSettings}
    run keyword  vp1.configurePort  remoteIP=${remoteIPAddr}

    #Test 8 TC001076007
    log  Test 8 - negative
    ${localSettings}=  Catenate  {'enabled': True, 'local_ip_address': '1.2.3.4', 'subnet_mask': '255.0.0.0',
                            ...  'default_gateway': '2.2.2.2' }
    ${remoteIPAddr}=  set variable  5.6.7.8
    run keyword and expect error  *The default gateway is not valid for the specified IP address and subnet mask*    vp1.configurePort  localSettings=${localSettings}
    run keyword  vp1.configurePort  remoteIP=${remoteIPAddr}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001076022_43_-_Virtual_Port_-_Remote_End_Mac_address_learning_ARP_Reply_AND_TC001076036
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    # arp reply is configured at a later step

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap

    tp1Ixia.startIxCapture

    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  2s
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    # Get MAC Addr from phy port = local Mac Addr and use it for creating  ARP stream
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}
    ${s2-arpReply}=  createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='${localMacAddr}')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='${localMacAddr}', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1
    tp1Ixia.addIxStreams  ${s2-arpReply}
    tp1Ixia.configureIxStreams
    tp1Ixia.runIxContinuousTraffic

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
   #Verify Link status on both vps for maximum 10 seconds
   :FOR    ${i}    IN RANGE  0  10
   \    ${vp1Props}=  vp1.getVirtPortInfo
   \    ${vp1LinkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
   \    run keyword if  '${vp1LinkStatus}' == 'True'  exit for loop
   \       sleep  1s

    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done within *** settings *** test teardown

TC001076023_44_-_Virtual_Port_-_Remote_End_Mac_address_learning_Gratuitous_ARP_Reply
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-gratArpReply}=  createStream  s2-gratArpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='ff:ff:ff:ff:ff:ff', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts
    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-gratArpReply}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    log  ${captureFileArp}
    log  ${capturePcapArp}
    tp1Ixia.startIxCapture


    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  10 secs
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}
    log  ${tp1Ixia.recdFrames}
    sleep  10secs

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}
    log  ${localMacAddr}
    log  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** settings *** test teardown

TC001076025_46_-_ARP_Reset_-_Gratuitous_ARP_reply_in_the_first_6_seconds_from_the_GRE_Termination_End_Point_AND_TC001076047
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-gratArpReply}=  createStream  s2-gratArpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='ff:ff:ff:ff:ff:ff', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-gratArpReply}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    log  ${captureFileArp}
    log  ${capturePcapArp}
    tp1Ixia.startIxCapture


    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  10 secs
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}
    log  ${tp1Ixia.recdFrames}

    #Check the TG RX port did not receive more than 5 frames
    ${actualTp1IxiaStats} =  tp1Ixia.getIxPortRxStats
    # rxFrames must be in interval 1 .. 5
    should be true    ${actualTp1IxiaStats["rxFrames"]} > 0 and ${actualTp1IxiaStats["rxFrames"]} < 10
    should be true    ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}


    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}
    #Verify Link status on both vps for maximum 10 seconds
    :FOR    ${i}    IN RANGE  0  10
    \    ${vp1Props}=  vp1.getVirtPortInfo
    \    ${vp1LinkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    \    run keyword if  '${vp1LinkStatus}' == 'True'  exit for loop
    \       sleep  1s

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}
    log  ${localMacAddr}
    log  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    #  Done during *** settings *** test teardown

TC001076025_47_-_ARP_Reset_-_Gratuitous_ARP_reply_in_the_first_6_seconds_from_the_Gateway_AND_TC001076048

    #Author: OBalas
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.2.25
    ${GatewayIPAddr}=   Set Variable  192.168.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${GatewayIPAddr}'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-gratArpReplyFromGw}=  createStream  s2-gratArpReplyFromGw  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${GatewayIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${GatewayIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=zeros  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-gratArpReplyFromGw}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    log  ${captureFileArp}
    log  ${capturePcapArp}
    tp1Ixia.startIxCapture


    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  10 secs
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}
    log  ${tp1Ixia.recdFrames}

    #Check the TG RX port did not receive more than 5 frames
    ${actualTp1IxiaStats} =  tp1Ixia.getIxPortRxStats
    # rxFrames must be in interval 1 .. 5
    should be true    ${actualTp1IxiaStats["rxFrames"]} > 0 and ${actualTp1IxiaStats["rxFrames"]} < 10
    should be true    ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}
    log  ${localMacAddr}
    log  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${GatewayIPAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done within *** *** settings *** test teardown

TC001076028_49_-_ARP_Reset_-_Gratuitous_ARP_reply_within_the_5_minute_reset_time_from_the_GW
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.2.25
    ${GatewayIPAddr}=   Set Variable  192.168.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${GatewayIPAddr}'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-gratArpReplyFromGw}=  createStream  s2-gratArpReplyFromGw  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${GatewayIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${GatewayIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=zeros  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-gratArpReplyFromGw}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    log  ${captureFileArp}
    log  ${capturePcapArp}
    tp1Ixia.startIxCapture

    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  40 secs
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}
    log  ${tp1Ixia.recdFrames}

    #Check the TG RX port did not receive more than 5 frames
    ${actualTp1IxiaStats} =  tp1Ixia.getIxPortRxStats
    # rxFrames must be in interval 1 .. 5
    should be true    ${actualTp1IxiaStats["rxFrames"]} >= 6
    should be true    ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}
    log  ${localMacAddr}
    log  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${GatewayIPAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done within *** *** settings *** test teardown

TC001076027_48_-_ARP_Reset_-_Gratuitous_ARP_reply_within_the_5_minute_reset_time_from_the_GRE_Termination_End_Point_AND_TC001076031
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-gratArpReply}=  createStream  s2-gratArpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='ff:ff:ff:ff:ff:ff', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-gratArpReply}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    log  ${captureFileArp}
    log  ${capturePcapArp}
    tp1Ixia.startIxCapture


    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  40 secs
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}
    log  ${tp1Ixia.recdFrames}

    #Check the TG RX port did not receive more than 5 frames
    ${actualTp1IxiaStats} =  tp1Ixia.getIxPortRxStats
    # rxFrames must be in interval 1 .. 5
    should be true    ${actualTp1IxiaStats["rxFrames"]} >= 6
    should be true    ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}


    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Verify that VP link is still up
    ${vp1Props}=  vp1.getVirtPortInfo
    ${vp1LinkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    should be true  '${vp1LinkStatus}' == 'True'

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    #  Done during *** settings *** test teardown

TC001076029_50_-_ARP_Reset_-_ARP_reply_in_the_first_6_seconds_from_the_GRE_Termination_End_Point

    #Author: OBalas
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${GatewayIPAddr}=   Set Variable  192.168.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${GatewayIPAddr}'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    # stream 2 (ARP Reply) is configured at a later step

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap

    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    tp1Ixia.startIxCapture
    nto1.initAllPortsAndFilters
    sleep  10s
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    #Wait for 10 seconds TG RX Port to get the ARP Request from NTO VP/TP
    :FOR    ${i}    IN RANGE  0  10
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   "${actualTp1IxiaStats["rxFrames"]}" != "0"   Exit For Loop
    \       sleep  1 sec
    \       continue for loop
    # Get MAC Addr from phy port = local Mac Addr and use it for creating  ARP stream
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}
    ${s2-arpReply}=  createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='${localMacAddr}')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='${localMacAddr}', pdst='${localIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1
    tp1Ixia.addIxStreams  ${s2-arpReply}
    tp1Ixia.configureIxStreams
    tp1Ixia.runIxContinuousTraffic

    #Check the TG RX port did not receive more than 5 frames
    ${actualTp1IxiaStats} =  tp1Ixia.getIxPortRxStats
    # rxFrames must be in interval 1 .. 5
    should be true    ${actualTp1IxiaStats["rxFrames"]} > 0 and ${actualTp1IxiaStats["rxFrames"]} <= 10
    should be true    ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}

    #Verify Link status on both vps for maximum 10 seconds
    :FOR    ${i}    IN RANGE  0  10
    \    ${vp1Props}=  vp1.getVirtPortInfo
    \    ${vp1LinkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    \    run keyword if  '${vp1LinkStatus}' == 'True'  exit for loop
    \       sleep  1s

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Used the cleanup from *** *** settings *** test teardown

TC001076030_51_-_ARP_Reset_-_ARP_reply_in_the_first_6_seconds_from_the_Gateway

    #Author: OBalas
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.10.25
    ${GatewayIPAddr}=   Set Variable  192.168.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${GatewayIPAddr}'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    # stream 2 is created at a later time

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap

    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    tp1Ixia.startIxCapture
    nto1.initAllPortsAndFilters
    sleep  2secs
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}
    ${s2-arpReply}=  createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='${localMacAddr}')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${GatewayIPAddr}', hwdst='${localMacAddr}', pdst='${localIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1
    tp1Ixia.addIxStreams  ${s2-arpReply}
    tp1Ixia.configureIxStreams
    tp1Ixia.runIxContinuousTraffic    # Get MAC Addr from phy port = local Mac Addr and use it for creating  ARP stream

    #Check the TG RX port did not receive more than 5 frames
    ${actualTp1IxiaStats}=  tp1Ixia.getIxPortRxStats
    should be true    ${actualTp1IxiaStats["rxFrames"]} > 0 and ${actualTp1IxiaStats["rxFrames"]} < 10
    should be true    ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}

    #Verify Link status on both vps for maximum 10 seconds
    :FOR    ${i}    IN RANGE  0  10
    \    ${vp1Props}=  vp1.getVirtPortInfo
    \    ${vp1LinkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    \    run keyword if  '${vp1LinkStatus}' == 'True'  exit for loop
    \       sleep  1s

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}
    log  ${localMacAddr}
    log  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${GatewayIPAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

     #Cleaup the devices and ports
     #Done during test teardonw

TC001076034_55_-_ARP_Reset_-_Virtual_Port_does_not_receive_grat_arp/arp_reply_after_the_first_ARP_Refresh

    #Author: OBalas
    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.10.25
    ${GatewayIPAddr}=   Set Variable  192.168.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${GatewayIPAddr}'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-arpReplyFromGw}=  createStream  s2-arpReplyFromGw  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${GatewayIPAddr}', hwdst='00:00:00:11:11:11', pdst='${GatewayIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-arpReplyFromGw}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    log  ${captureFileArp}
    log  ${capturePcapArp}
    tp1Ixia.startIxCapture

    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  1secs
    log to console  Nto ports initialized
    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} >= 6   Exit For Loop
    \       sleep  1 sec
    \    Run keyword if   ${i} == 29  fail  Less than 6 ARP REQs have been sent

    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}
    log  ${tp1Ixia.recdFrames}

    #Check the TG RX port did not receive more than 5 frames
    ${actualTp1IxiaStats}=  tp1Ixia.getIxPortRxStats
    # rxFrames must be in interval 1 .. 5
    should be true    ${actualTp1IxiaStats["rxFrames"]} > 0 and ${actualTp1IxiaStats["rxFrames"]} < 10
    should be true    ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}


    #Verify Link status
    ${vp1Props}=  vp1.getVirtPortInfo
    ${linkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    should be equal  '${linkStatus}'   'True'

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${GatewayIPAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    #Reset the Ixia TG RX port and wait for new ARP Request (this may take up to 6 mins)
    tp1Ixia.clearIxPortStats
    sleep  2secs
    #Should wait until the the VP sends ARP packets
    :FOR    ${i}    IN RANGE  0  360
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} == 6   Exit For Loop
    \       sleep  1 sec
    \       continue for loop

    # Do not send ARP Reply from TG port and check the VTP link
    :FOR    ${i}    IN RANGE  0  120
    \    ${vp1Props}=  vp1.getVirtPortInfo
    \    ${linkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    \    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    \    run keyword if  '${linkStatus}' == 'False' and '${remoteMacAddr}' == 'ff-ff-ff-ff-ff-ff'    Exit For Loop
    \        sleep  1 sec
    \    Run keyword if   ${i} == 119  fail  Re-Arp Thread has not cleared Remote MAC and Link Status in 2 Minutes after Re-ARP


    # Check that the NTO does not encap the packets
    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    np1Ixia.runIxContinuousTraffic
    sleep  3secs
    tp1Ixia.getIxPortRxStats

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=0
        ...  tp_total_pass_count_packets=0
        ...  tp_total_tx_count_packets=0
        ...  tp_total_tx_count_bytes=0
        ...  tp_total_gre_encap_count_packets=0
        ...  tp_total_gre_encap_percent_packets=0
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}
    #Verify that the TG RX port did not receive other frames than ARP Request - check frame lenght on all frames
    ${actualTp1IxiaStats}=  tp1Ixia.getIxPortRxStats
    should be true    ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}

     #Cleaup the devices and ports
     #Done at *** *** settings *** test teardown

TC001076031_52_-_ARP_Reset_-_ARP_reply_within_the_5_minute_reset_time_from_the_GRE_Termination_End_Point
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${GatewayIPAddr}=  set variable  192.168.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${GatewayIPAddr}'}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-arpReply}=  createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-arpReply}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    tp1Ixia.startIxCapture


    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  40 secs

    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    #Check the TG RX port did not receive more than 5 frames
    ${actualTp1IxiaStats} =  tp1Ixia.getIxPortRxStats
    # rxFrames must be in interval 1 .. 5
    should be true    ${actualTp1IxiaStats["rxFrames"]} >= 6
    should be true    ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Verify that VP link is still up
    ${vp1Props}=  vp1.getVirtPortInfo
    ${vp1LinkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    should be true  '${vp1LinkStatus}' == 'True'

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate
    ...    Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/
    ...    ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)/
    ...    Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    #  Done during *** settings *** test teardown

TC001076032_53_-_ARP_Reset_-_ARP_reply_within_the_5_minute_reset_time_from_the_GW
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.25.25
    ${GatewayIPAddr}=  set variable  192.168.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${GatewayIPAddr}'}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-arpReplyFromGw}=  createStream  s2-arpReplyFromGw  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${GatewayIPAddr}', hwdst='00:00:00:11:11:11', pdst='${GatewayIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-arpReplyFromGw}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    tp1Ixia.startIxCapture


    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  40 secs
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    #Check the TG RX port did not receive more than 5 frames
    ${actualTp1IxiaStats} =  tp1Ixia.getIxPortRxStats
    # rxFrames must be in interval 1 .. 5
    should be true    ${actualTp1IxiaStats["rxFrames"]} >= 6
    should be true    ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Verify that VP link is still up
    ${vp1Props}=  vp1.getVirtPortInfo
    ${vp1LinkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    should be true  '${vp1LinkStatus}' == 'True'

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate
    ...    Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/
    ...    ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${GatewayIPAddr}', plen=4, ptype=2048, hwlen=6)/
    ...    Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port. Filtered the traffic capture in case on a NTO ARP frame leaked
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    log  ${pktProcessingTunnelOriginatonDef}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}
    log  ${ExpectedEncapGreFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    #  Done during *** settings *** test teardown

TC001076033_54_-_ARP_Reset_-_ARP_Refresh_after_5_minutes_time_out
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.25.25
    ${GatewayIPAddr}=  set variable  192.168.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${GatewayIPAddr}'}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-arpReplyFromGw}=  createStream  s2-arpReplyFromGw  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${GatewayIPAddr}', hwdst='00:00:00:11:11:11', pdst='${GatewayIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-arpReplyFromGw}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    tp1Ixia.startIxCapture


    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  10 s
    log  Checking if we still have only 6 ARP Reply sent by VTP within 4 mins
    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    should be true  ${actualTp1IxiaStats["rxFrames"]} <= 10
    log  Waiting for the second chain of ARP Request sent by VTP (range is few seconds to maximum of ~ 5 mins)
    :FOR    ${i}    IN RANGE  0  360
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} > 6 and ${actualTp1IxiaStats["rxFrames"]} <= 15   Exit For Loop
    \       sleep  1 sec
    \       continue for loop
    sleep  4 min
    log  Waiting for the third chain of ARP Request sent by VTP (after ~ 5 min)
    :FOR    ${i}    IN RANGE  0  120
    \    ${actualTp1IxiaStats}=   tp1Ixia.getIxPortRxStats
    \    Run keyword if   ${actualTp1IxiaStats["rxFrames"]} > 12 and ${actualTp1IxiaStats["rxFrames"]} <= 22   Exit For Loop
    \       sleep  1 sec
    \       continue for loop
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  18
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Verify that VP link is still up
    sleep  2 s
    ${vp1Props}=  vp1.getVirtPortInfo
    ${vp1LinkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    should be true  '${vp1LinkStatus}' == 'True'

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate
    ...    Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/
    ...    ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${GatewayIPAddr}', plen=4, ptype=2048, hwlen=6)/
    ...    Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Cleaup the devices and ports
    #  Done during *** settings *** test teardown

TC001076035_56_-_Negative_-_Malformed_ARP_messages
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    # malformed aRP Reply contains opcode = 1 whom VTP should not consider
    # stream 2 is created at a later step

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap

    #We configure the port and filters
    nto1.initAllPortsAndFilters
    # Get MAC Addr from phy port = local Mac Addr and use it for creating malformed ARP stream
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}
    ${s2-malformedArpReply}=  createStream  s2-malformedArpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='${localMacAddr}')/ARP(hwtype=1, op='who-has', hwsrc='00:00:00:11:11:11', psrc='192.168.1.222', hwdst='${localMacAddr}', pdst='192.168.1.10', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1
    tp1Ixia.addIxStreams  ${s2-malformedArpReply}
    tp1Ixia.configureIxStreams
    tp1Ixia.runIxContinuousTraffic
    sleep  6 secs

    #Check the TG RX port did not receive more than 5 frames
    ${actualTp1IxiaStats} =  tp1Ixia.getIxPortRxStats
    # rxFrames must be in interval 1 .. 5
    should be true    ${actualTp1IxiaStats["rxFrames"]} > 0 and ${actualTp1IxiaStats["rxFrames"]} <= 6
    should be true    ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}

    #Verify Link status is partial
    ${vp1Props}=  vp1.getVirtPortInfo
    ${vp1LinkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    should be true  '${vp1LinkStatus}' == 'False'

    #Run traffic from network port
    np1Ixia.runIxTraffic  capture=False

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=0
        ...  tp_total_pass_count_packets=0
        ...  tp_total_tx_count_packets=0
        ...  tp_total_tx_count_bytes=0
        ...  tp_total_gre_encap_count_packets=0
        ...  tp_total_gre_encap_percent_packets=0
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    #  Done during *** settings *** test teardown

TC001076040_61_-_Virtual_Port_Group_-_Filtering_on_Virtual_Port
    # Author: OBalas
    # Improvment by waiting the vp to send ARP Req

    create directory  ${OUTPUTDIR}/${TEST NAME}/

    ##################################
    ###1.Configure and Verify GRE ####
    ##################################

    #Virt Port local and remote definitions.
    ${localIPAddr1}=  Set Variable  192.168.1.10
    ${localIPAddr2}=  Set Variable  192.168.1.20
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${defaultGateway}=  Set Variable  192.168.1.254
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}
    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr2}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${defaultGateway}'}

    #Arp Reply Stream Definition
    ${arpReply1}=  createStream  s1  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100
    ${arpReply2}=  createStream  s2  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${remoteIPAddr}', hwdst='00:00:00:11:11:11', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=100
    ${stream1}=  createStream  stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/Dot1Q(vlan=100)/IP(src="10.0.0.1", dst="10.0.0.2")/UDP(sport=10000, dport=10002)  payload=incr
                        ...  frameSize=1000   numFrames=100
    ${stream2}=  createStream  stream2  Ether(src='00:00:02:00:00:01', dst='00:00:02:00:00:02')/Dot1Q(vlan=200)/IP(src="20.0.0.1", dst="20.0.0.2")/UDP(sport=20001, dport=20002)  payload=incr
                        ...  frameSize=1000   numFrames=10

    #Virt Port assignemnt
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    AddVirtPort  ${tp2}  vp2  ${vp2Def}  ${remoteIPAddr}

    #Port Group definitions.
    ${tpg1def}=  Catenate  {'type': 'LOAD_BALANCE', 'mode': 'TOOL', 'failover_mode': 'REBALANCE'}
    ${fdef}=  Catenate  {'filter_criteria': {'logical_operation': 'AND',
    ...  'mac_src': {'addr': ["00-00-01-00-00-01"]},
    ...  'ipv4_src': {'addr': ["10.0.0.1"]},
    ...  "layer4_src_port": {"port": "10000"}
    ...  }}
    @{tpgPortList}=  Create List  ${tp1}  ${vp1}  ${tp2}  ${vp2}
    AddPortGrp  ${nto1}  tpg1  ${tpg1def}  ${tpgPortList}
    AddFilter  ${nto1}  f1    PASS_ALL
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${tpg1}

    #Setup cleanup and initialization
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    tp1Ixia.addIxStreams  ${arpReply1}
    tp2Ixia.addIxStreams  ${arpReply2}
    np1Ixia.addIxStreams  ${stream1}  ${stream2}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    #Capture file definitions
    ${captureFileArp1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request_port1.enc
    ${capturePcapArp1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request_port1.pcap
    ${captureFileArp2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request_port2.enc
    ${capturePcapArp2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request_port2.pcap

    #Start capture on Ixia Port
    tp1Ixia.startIxCapture
    tp2Ixia.startIxCapture

    #NTO port and connection configuration
    nto1.initAllPortsAndFilters
    sleep  5secs
    #Check the TG RX port receives ARP Request
    :FOR    ${i}    IN RANGE  0  30
    \    ${actualTp1IxiaStats} =  tp1Ixia.getIxPortRxStats
    \    ${actualTp2IxiaStats} =  tp2Ixia.getIxPortRxStats
    \    # rxFrames must be in interval 1 .. 5
    \    run keyword if    ${actualTp1IxiaStats["rxFrames"]} >= 6 and ${actualTp1IxiaStats["rxBytes"]} / 64 == ${actualTp1IxiaStats["rxFrames"]}  continue for loop
    \        sleep  1s
    \    run keyword if    ${actualTp2IxiaStats["rxFrames"]} >= 6 and ${actualTp2IxiaStats["rxBytes"]} / 64 == ${actualTp2IxiaStats["rxFrames"]}  exit for loop
    \        sleep  1s
    \    run keyword if    ${i} == 29  fail  Less Than 6 ARP Requests have been sent

    #Run traffic from Ixia Port
    tp1Ixia.runIxContinuousTraffic
    tp2Ixia.runIxContinuousTraffic

    #Verify Link status on both vps for maximum 10 seconds
    :FOR    ${i}    IN RANGE  0  10
    \    ${vp1Props}=  vp1.getVirtPortInfo
    \    ${vp2Props}=  vp2.getVirtPortInfo
    \    ${vp1LinkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    \    ${vp2LinkStatus}=  evaluate  ${vp2Props}.get('link_status').get('link_up')
    \    run keyword if  '${vp1LinkStatus}' == 'True' and '${vp2LinkStatus}' == 'True'  exit for loop
    \       sleep  1s
    \    run keyword if    ${i} == 9  fail  Link Status is Incorrect

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    tp2Ixia.stopIxCapture
    sleep  2secs

    #Download the capture from IxOS
    tp1Ixia.exportIxFramesToFile  ${captureFileArp1}  1  6
    convertEncToPcap  ${captureFileArp1}  ${capturePcapArp1}
    tp1Ixia.scapyImportPcap  ${capturePcapArp1}

    tp2Ixia.exportIxFramesToFile  ${captureFileArp2}  1  6
    convertEncToPcap  ${captureFileArp2}  ${capturePcapArp2}
    tp2Ixia.scapyImportPcap  ${capturePcapArp2}

    # Get MAC Addr from phy port -> local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr1}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr1}=  replaceDashWithColon  ${localMacAddr1}

    ${tp2Props}=  tp2.getPortInfo
    ${localMacAddr2}=  evaluate  ${tp2Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr2}=  replaceDashWithColon  ${localMacAddr2}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr1}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr1}=  replaceDashWithColon  ${remoteMacAddr1}

    ${vp2Props}=  vp2.getVirtPortInfo
    ${remoteMacAddr2}=  evaluate  ${vp2Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr2}=  replaceDashWithColon  ${remoteMacAddr2}

    #Creating expected sent arp request
    ${ExpectedArpFrameSent1}=  catenate  Ether(type=2054, src='${localMacAddr1}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr1}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes1}=  convertStreamToBytes  ${ExpectedArpFrameSent1}
    ${ExpectedArpFrameSentBytes1}=  create list  ${ExpectedArpFrameSentBytes1}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes1}  ${tp1Ixia.recdFrames}  ordered=${False}

    ${ExpectedArpFrameSent2}=  catenate  Ether(type=2054, src='${localMacAddr2}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr2}', psrc='${localIPAddr2}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${remoteIPAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentBytes2}=  convertStreamToBytes  ${ExpectedArpFrameSent2}
    ${ExpectedArpFrameSentBytes2}=  create list  ${ExpectedArpFrameSentBytes2}

    #Compare download capture with previously created packets
    compareFrames  ${ExpectedArpFrameSentBytes2}  ${tp2Ixia.recdFrames}  ordered=${False}

    #######################################################
    ###2.Send traffic , verify  traffic and statistics  ###
    #######################################################

    #Configure filter criteria
    tpg1.configurePortGrpFilter  PASS_BY_CRITERIA  ${fdef}
    sleep  3s
    #Run traffic without capture
    np1Ixia.runIxTraffic  capture=${FALSE}
    sleep  2s

    # Check the virtual port link status before getting the stats
    ${vp1Props}=  vp1.getVirtPortInfo
    ${vp2Props}=  vp2.getVirtPortInfo
    ${vp1LinkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    ${vp2LinkStatus}=  evaluate  ${vp2Props}.get('link_status').get('link_up')
    should be true  '${vp1LinkStatus}' == 'True'
    should be true  '${vp2LinkStatus}' == 'True'
    # Verify Statistics
    ${actualNp1Stats}=   np1.getPortStats  statName=all
    ${actualDf1Stats}=   f1.getFilterStats  statName=all
    ${actualTp1Stats}=   tp1.getPortStats  statName=all
    ${actualTp2Stats}=   tp2.getPortStats  statName=all
    ${actualTpg1Stats}=  tpg1.getPortGrpStats  statName=all
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=110
        ...  np_total_rx_count_valid_packets=110
        ...  np_total_rx_count_bytes=110000
        ...  np_total_pass_count_packets=110
        ...  np_total_pass_count_bytes=110000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=110
        ...  df_total_insp_count_bytes=110000
        ...  df_total_pass_count_packets=110
        ...  df_total_pass_count_bytes=110000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}

    # Verify Statistics on Tool Port 1
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=None
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Run keyword if   ${actualTp1Stats["${key}"]} <= 0  fail  Load Balance was not done correctly

    # Verify Statistics on Tool Port 2
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=None
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Run keyword if   ${actualTp2Stats["${key}"]} <= 0  fail  Load Balance was not done correctly

        # Verify Statistics on Tool Port Group
    &{expectedTpg1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=110
        ...  tp_total_pass_count_packets=100
        ...  tp_total_tx_count_packets=100
        ...  tp_total_tx_count_bytes=103800
        ...  tp_total_gre_encap_count_packets=100
        ...  tp_total_gre_encap_percent_packets=1
    :FOR    ${key}    IN    @{expectedTpg1Stats.keys()}
    \    should be equal as integers    ${expectedTpg1Stats["${key}"]}     ${actualTpg1Stats["${key}"]}
    \    ${statSum}=  Evaluate  ${actualTp1Stats["${key}"]} + ${actualTp2Stats["${key}"]}
    \    should be equal as integers    ${actualTpg1Stats["${key}"]}    ${statSum}

    #Set PASS ALL for the TPG
    tpg1.configurePortGrpFilter  PASS_ALL

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001076051_72_-_Virtual_Port_-_Vlan_Stripping_on
   #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/Dot1Q(vlan=100)/Dot1Q(vlan=200)/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-arpReply}=  createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='00:00:00:11:11:11', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    # Subtest #1 - 1 VLAN Stripping
    log  Subtest #1 - 1 VLAN Stripping
    log to console  Subtest #1 - 1 VLAN Stripping
    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-arpReply}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    log  ${captureFileArp}
    log  ${capturePcapArp}

    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    tp1Ixia.startIxCapture
    nto1.initAllPortsAndFilters
    sleep  10 secs
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}
    log  ${tp1Ixia.recdFrames}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}
    log  ${localMacAddr}
    log  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_subtest1.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_subtest1.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap


    ${pktProcessingVlanStripping}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'egress_count': 1, 'strip_mode': 'EGRESS'}}
    tp1.configurePacketProcessing  ${pktProcessingVlanStripping}

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    ${ExpectedVlanStippedFrame}=  np1.pktProc.emulate  ${pktProcessingVlanStripping}  pcapFile=${origCapturePcap}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  frames=${ExpectedVlanStippedFrame}


    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10340
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Subtest #2 - 2 VLAN Stripping (QinQ)
    log  Subtest #2 - 2 VLAN Stripping (QinQ)
    log to console   Subtest #2 - 2 VLAN Stripping (QinQ)

    #Reset port and add streams
    np1Ixia.clearIxPortStats
    tp1Ixia.clearIxPortStats
    np1.resetPortStats
    f1.resetFilterStats
    tp1.resetPortStats

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_subtest2.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_subtest2.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap


    ${pktProcessingQinQStripping}=  Catenate  {'std_vlan_strip_settings': {'enabled': True, 'egress_count': 2, 'strip_mode': 'EGRESS'}}
    tp1.configurePacketProcessing  ${pktProcessingQinQStripping}

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    ${ExpectedVlanStippedFrame}=  np1.pktProc.emulate  ${pktProcessingQinQStripping}  pcapFile=${origCapturePcap}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  frames=${ExpectedVlanStippedFrame}

    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10300
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001076054_75_-_Virtual_Port_-_Multiple_Ports_with_the_same_Remote_End_Point_Destination_or_GW
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps
    ${tp2PortName}=  set variable  P2A-04
    ${tp2IxiaPortName}=  set variable  1/11/4

    #Virt Port local definitions.
    ${localIPAddr1}=  Set Variable  192.168.10.10
    ${gwIPAddr1}=  Set Variable  192.168.10.254
    ${localIPAddr2}=  Set Variable  192.168.20.20
    ${gwIPAddr2}=  Set Variable  192.168.20.254
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${gwIPAddr1}'}
    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr2}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${gwIPAddr2}'}

    #Stream Definition
    ${s0-stream1}=  createStream  s0-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s1-arpReply}=  createStream  s1-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='${gwIPAddr1}', hwdst='00:00:00:11:11:11', pdst='${gwIPAddr1}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1
    #${s21-stream1}=  createStream  s21-stream1  Ether(src='00:00:02:00:00:01', dst='00:00:02:00:00:02')/IP()  payload=incr  frameSize=100   numFrames=100
    ${s2-arpReply}=  createStream  s2-arpReply  Ether(type=2054, src='00:00:00:22:22:22', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:22:22:22', psrc='${gwIPAddr2}', hwdst='00:00:00:22:22:22', pdst='${gwIPAddr2}', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    AddVirtPort  ${tp2}  vp2  ${vp2Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL
    AddFilter  ${nto1}  f2    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}
    AddConn    ${nto1}  c2    ${np1}   ${f2}    ${vp2}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s0-stream1}
    tp1Ixia.addIxStreams  ${s1-arpReply}
    tp2Ixia.addIxStreams  ${s2-arpReply}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_1_arp_request.enc
    ${captureFileArp2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_2_arp_request.enc
    ${capturePcapArp1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_1_arp_request.pcap
    ${capturePcapArp2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_2_arp_request.pcap
    tp1Ixia.startIxCapture
    tp2Ixia.startIxCapture


    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  10 secs
    tp1Ixia.runIxContinuousTraffic
    tp2Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    tp2Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp1}  1  6
    tp2Ixia.exportIxFramesToFile  ${captureFileArp2}  1  6
    convertEncToPcap  ${captureFileArp1}  ${capturePcapArp1}
    convertEncToPcap  ${captureFileArp2}  ${capturePcapArp2}
    tp1Ixia.scapyImportPcap  ${capturePcapArp1}
    tp2Ixia.scapyImportPcap  ${capturePcapArp2}
    log  ${tp1Ixia.recdFrames}
    log  ${tp2Ixia.recdFrames}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr1}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr1}=  replaceDashWithColon  ${localMacAddr1}
    ${tp2Props}=  tp2.getPortInfo
    ${localMacAddr2}=  evaluate  ${tp2Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr2}=  replaceDashWithColon  ${localMacAddr2}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    ${remoteMacAddr1}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr1}=  replaceDashWithColon  ${remoteMacAddr1}
    ${vp2Props}=  vp2.getVirtPortInfo
    ${remoteMacAddr2}=  evaluate  ${vp2Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr2}=  replaceDashWithColon  ${remoteMacAddr2}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent1}=   catenate  Ether(type=2054, src='${localMacAddr1}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr1}', psrc='${localIPAddr1}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${gwIPAddr1}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentHex1}=  convertStreamToBytes  ${ExpectedArpFrameSent1}
    ${ExpectedArpFrameSentHex1}=  create list  ${ExpectedArpFrameSentHex1}
    compareFrames  ${ExpectedArpFrameSentHex1}  ${tp1Ixia.recdFrames}  ordered=${False}
    ${ExpectedArpFrameSent2}=   catenate  Ether(type=2054, src='${localMacAddr2}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr2}', psrc='${localIPAddr2}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${gwIPAddr2}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    ${ExpectedArpFrameSentHex2}=  convertStreamToBytes  ${ExpectedArpFrameSent2}
    ${ExpectedArpFrameSentHex2}=  create list  ${ExpectedArpFrameSentHex2}
    compareFrames  ${ExpectedArpFrameSentHex2}  ${tp2Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port
    ${captureGrePktEncFile1}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_1_gre_encap_stream.enc
    ${captureGrePktEncFile2}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_2_gre_encap_stream.enc
    ${captureGrePktPcapFile1}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_1_gre_encap_stream.pcap
    ${captureGrePktPcapFile2}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_2_gre_encap_stream.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    tp2Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    tp2Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile1}  1  30
    tp2Ixia.exportIxFramesToFile  ${captureGrePktEncFile2}  1  30
    convertEncToPcap  ${captureGrePktEncFile1}  ${captureGrePktPcapFile1}
    convertEncToPcap  ${captureGrePktEncFile2}  ${captureGrePktPcapFile2}
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFile1}
    tp2Ixia.scapyImportPcap  ${captureGrePktPcapFile2}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef1}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr1}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "${gwIPAddr1}",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr1}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr1}"
    ...    }
    ...    }}
    log  ${pktProcessingTunnelOriginatonDef1}
    ${ExpectedEncapGreFrame1}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef1}  pcapFile=${origCapturePcap}
    compareFrames  ${ExpectedEncapGreFrame1}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    ${pktProcessingTunnelOriginatonDef2}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr2}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "${gwIPAddr2}",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr2}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr2}"
    ...    }
    ...    }}
    log  ${pktProcessingTunnelOriginatonDef2}
    ${ExpectedEncapGreFrame2}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef2}  pcapFile=${origCapturePcap}
    compareFrames  ${ExpectedEncapGreFrame2}  ${tp2Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${False}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}
    &{expectedTp2Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp2Stats}=  tp2.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp2Stats.keys()}
    \    Log  ${expectedTp2Stats["${key}"]}
    \    should be equal as integers    ${expectedTp2Stats["${key}"]}     ${actualTp2Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001076055_76_-_Virtual_Port_-_Fabric_Path_in_L2GRE_tunnel
   #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  CFP(src='00:00:aa:00:00:01', dst='00:00:bb:00:00:01', type=0x8903)/Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/Dot1Q(vlan=100)/Dot1Q(vlan=200)/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-arpReply}=  createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='00:00:00:11:11:11', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-arpReply}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    log  ${captureFileArp}
    log  ${capturePcapArp}

    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    tp1Ixia.startIxCapture
    nto1.initAllPortsAndFilters
    sleep  10secs
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}
    log  ${tp1Ixia.recdFrames}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}
    log  ${localMacAddr}
    log  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktPcapFile}

    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  pcapFile=${origCapturePcap}

    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done under *** *** settings *** test teardown

TC001076056_77_-_Virtual_Port_-_VP_cannot_be_deleted_while_tunneling_is_enabled
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}
    log  ${vp1Def}

    #Stream Definition
    ${s1-stream1}=  createStream  s1-stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/IP()  payload=incr  frameSize=1000   numFrames=10
    ${s2-arpReply}=  createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='00:00:00:11:11:11', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1-stream1}
    tp1Ixia.addIxStreams  ${s2-arpReply}
    nto1.configureAllIxPorts
    nto1.configureAllIxStreams
    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    tp1Ixia.startIxCapture

    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    nto1.initAllPortsAndFilters
    sleep  10secs
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}
    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    #Verify Link status
    ${vp1Props}=  vp1.getVirtPortInfo
    ${linkStatus}=  evaluate  ${vp1Props}.get('link_status').get('link_up')
    should be equal  '${linkStatus}'   'True'

    #Detele the vp1 when it is connected. It returns erros
    run keyword and expect error  *ERROR: Failed (errno = 500) to delete*   vp1.deleteVirtPortById

    # Cleaup the devices and ports
    # Used from *** settings *** test teardown

TC001076057_78_-_Virtual_Port_-_Same_Local_IP_on_multiple_tunnels
    #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps
    ${tp2PortName}=  set variable  P2A-04
    ${tp2IxiaPortName}=  set variable  1/11/4
    #Virt Port local definitions.
    ${localIPAddr1}=  Set Variable  192.168.10.10
    ${gwIPAddr1}=  Set Variable  192.168.10.254
    ${localIPAddr2}=  Set Variable  192.168.20.20
    ${gwIPAddr2}=  Set Variable  192.168.20.254
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr1}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${gwIPAddr1}'}
    ${vp2Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr2}', 'subnet_mask': '255.255.255.0', 'default_gateway': '${gwIPAddr2}'}

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}
    AddVirtPort  ${tp2}  vp2  ${vp2Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL
    AddFilter  ${nto1}  f2    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}
    AddConn    ${nto1}  c2    ${np1}   ${f2}    ${vp2}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initallPortsAndFilters

    ${localIPAddr2}=  Set Variable  192.168.10.10
    run keyword and expect error  *Tunneling local endpoint IP address must be unique across all ports*   vp1.configurePort  localSettings=${vp2Def}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown

TC001088826_86_-_Virtual_Port_-_Stripping_by_vlan
   #Author: OBalas

    create directory  ${OUTPUTDIR}/${TEST NAME}/
    #NTO, Nto and Ixia port definitions. / all port speed is 10Gbps

    #Virt Port local definitions.
    ${localIPAddr}=  Set Variable  192.168.1.10
    ${remoteIPAddr}=   Set Variable  192.168.1.25
    ${vp1Def}=  Catenate  {'enabled': True, 'local_ip_address': '${localIPAddr}', 'subnet_mask': '255.255.255.0', 'default_gateway': '192.168.1.254'}
    log  ${vp1Def}

    #Stream Definition
    ${s1stream1}=  createStream  s1stream1  Ether(src='00:00:01:00:00:01', dst='00:00:01:00:00:02')/Dot1Q(vlan=100)/Dot1Q(vlan=200)/IP()  payload=incr  frameSize=100   numFrames=10
    ${s2-arpReply}=  createStream  s2-arpReply  Ether(type=2054, src='00:00:00:11:11:11', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=2, hwsrc='00:00:00:11:11:11', psrc='192.168.1.25', hwdst='00:00:00:11:11:11', pdst='192.168.1.25', plen=4, ptype=2048, hwlen=6)  payload=rand  frameSize=64   numFrames=1

    # AddVirtPort  <phyPort> <instance_name> <vp_properties> <tunnel_remote_ip_addr>
    AddVirtPort  ${tp1}  vp1  ${vp1Def}  ${remoteIPAddr}

    #Filter creation
    AddFilter  ${nto1}  f1    PASS_ALL

    #Connection creation
    AddConn    ${nto1}  c1    ${np1}   ${f1}    ${vp1}

    #Setup cleanup and initiation
    nto1.setupNVSDevices
    nto1.initAllIxPorts

    # Subtest #1 - VLAN Stripping by VLANID - positive stream
    log  Subtest #1 - VLAN Stripping by VLANID - positive stream
    log to console  Subtest #1 - VLAN Stripping by VLANID - positive stream
    #Stream creation and configuration on ports
    np1Ixia.addIxStreams  ${s1stream1}
    tp1Ixia.addIxStreams  ${s2-arpReply}

    nto1.configureAllIxPorts
    nto1.configureAllIxStreams

    ${captureFileArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.enc
    ${capturePcapArp}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_arp_request.pcap
    log  ${captureFileArp}
    log  ${capturePcapArp}

    #We configure the port and filters here because we need the capture to start before the GRE tunneling is enabled so that we can capture all 6 arp requests
    tp1Ixia.startIxCapture
    nto1.initAllPortsAndFilters
    sleep  10secs
    tp1Ixia.runIxContinuousTraffic
    tp1Ixia.stopIxCapture
    sleep  2secs
    tp1Ixia.exportIxFramesToFile  ${captureFileArp}  1  6
    convertEncToPcap  ${captureFileArp}  ${capturePcapArp}
    tp1Ixia.scapyImportPcap  ${capturePcapArp}
    log  ${tp1Ixia.recdFrames}

    # Get MAC Addr from phy port = local Mac Addr
    ${tp1Props}=  tp1.getPortInfo
    ${localMacAddr}=  evaluate  ${tp1Props}.get('tunnel_mac').get('mac_address')
    ${localMacAddr}=  replaceDashWithColon  ${localMacAddr}

    # Get MAC Addr from virtual port -> remote Mac Addr (learned)
    ${vp1Props}=  vp1.getVirtPortInfo
    log  ${vp1Props}
    ${remoteMacAddr}=  evaluate  ${vp1Props}.get('tunnel_origination_remote_settings').get('remote_mac_address')
    ${remoteMacAddr}=  replaceDashWithColon  ${remoteMacAddr}
    log  ${localMacAddr}
    log  ${remoteMacAddr}

    # Verify the ARP Request that TP sent
    ${ExpectedArpFrameSent}=   catenate  Ether(type=2054, src='${localMacAddr}', dst='ff:ff:ff:ff:ff:ff')/ARP(hwtype=1, op=1, hwsrc='${localMacAddr}', psrc='${localIPAddr}', hwdst='ff:ff:ff:ff:ff:ff', pdst='${RemoteIpAddr}', plen=4, ptype=2048, hwlen=6)/Padding(load=b'\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00')
    log  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  convertStreamToBytes  ${ExpectedArpFrameSent}
    ${ExpectedArpFrameSentHex}=  create list  ${ExpectedArpFrameSentHex}
    compareFrames  ${ExpectedArpFrameSentHex}  ${tp1Ixia.recdFrames}  ordered=${False}

    # Sent Traffic through NP - DF - VTP and capture the packtes at the traffic generator Rx port
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_subtest1.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_subtest1.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp_subtest1.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap


    ${pktProcessingVlanStripping}=  Catenate  {"std_strip_by_vlan_settings": {"enabled": True, "strip_mode": "EGRESS", "vlan_id": 100} }
    tp1.configurePacketProcessing  ${pktProcessingVlanStripping}

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}


    # Verify the stream packets are encapsulated into L2GRE
    ${pktProcessingTunnelOriginatonDef}=  Catenate  {
    ...  "tunnel_origination_settings":{
    ...    "tunnel_mac": {"mac_address": "${localMacAddr}"},
    ...    "tunnel_origination_local_settings": {
    ...        "default_gateway": "192.168.1.254",
    ...        "enabled": True,
    ...        "local_ip_address": "${localIPAddr}",
    ...        "subnet_mask": "255.255.255.0"
    ...    },
    ...    "tunnel_origination_remote_settings": {
    ...        "remote_ip_address": "${remoteIPAddr}",
    ...        "remote_mac_address": "${remoteMacAddr}"
    ...    }}}

    ${ExpectedVlanStippedFrame}=  np1.pktProc.emulate  ${pktProcessingVlanStripping}  pcapFile=${origCapturePcap}  verbose=${True}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  frames=${ExpectedVlanStippedFrame}  verbose=${True}

    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${True}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=1000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=1000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=1000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=1000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=1340
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Subtest #2 - VLAN Stripping by VLANID - negative stream
    log  Subtest #2 - VLAN Stripping by VLANID - negative stream
    log to console  Subtest #2 - VLAN Stripping by VLANID - negative stream

    #Reset ports stat
    np1Ixia.clearIxPortStats
    tp1Ixia.clearIxPortStats
    np1.resetPortStats
    f1.resetFilterStats
    tp1.resetPortStats

    #Run and capture traffic that doesn't match vlan filter by vlan id
    ${captureGrePktEncFile}=  Set Variable   ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_subtest2.enc
    ${captureGrePktPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_subtest2.pcap
    ${captureGrePktNoArpPcapFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/tg_rx_gre_encap_stream_noArp_subtest2.pcap
    ${origCaptureFile}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.enc
    ${origCapturePcap}=  Set Variable  ${OUTPUTDIR}/${TEST NAME}/orig.pcap


    # Negative stream is that must pass without stripping by tool port
    ${s1stream2}=  createStream  s1stream2  Ether(src='00:00:22:00:00:21', dst='00:00:22:00:00:22')/Dot1Q(vlan=1234)/Dot1Q(vlan=100)/IP()  payload=incr  frameSize=1000   numFrames=10
    np1Ixia.deleteIxStreams
    np1Ixia.addIxStreams  ${s1stream2}
    nto1.configureAllIxStreams

    #Start capture for original capture
    np1Ixia.enableInternalLoopback
    sleep  10secs

    #Start cature on Ixia Port
    tp1Ixia.startIxCapture
    sleep  3secs

    #Run traffic without capture
    np1Ixia.runIxTraffic
    sleep  5secs

    #Stop capture on Ixia Port
    tp1Ixia.stopIxCapture
    sleep  2secs

    #Stop traffic
    tp1ixia.stopIxTraffic

    #copy roginal capture
    np1Ixia.exportIxFramesToFile  ${origCaptureFile}  1  10000
    convertEncToPcap  ${origCaptureFile}  ${origCapturePcap}
    np1Ixia.disableInternalLoopback

    tp1Ixia.exportIxFramesToFile  ${captureGrePktEncFile}  1  30
    convertEncToPcap  ${captureGrePktEncFile}  ${captureGrePktPcapFile}
    filterCapture  ${captureGrePktPcapFile}  '!arp'  ${captureGrePktNoArpPcapFile}
    tp1Ixia.scapyImportPcap  ${captureGrePktNoArpPcapFile}


    ${ExpectedVlanStippedFrame}=  np1.pktProc.emulate  ${pktProcessingVlanStripping}  pcapFile=${origCapturePcap}
    ${ExpectedEncapGreFrame}=  np1.pktProc.emulate  ${pktProcessingTunnelOriginatonDef}  frames=${ExpectedVlanStippedFrame}
    compareFrames  ${ExpectedEncapGreFrame}  ${tp1Ixia.recdFrames}  [(18,2),(24,2)]  ordered=${False}

    # Verify Statistics
    # Verify Statistics on Network Port
    &{expectedNp1Stats}=  create dictionary
        ...  np_total_rx_count_packets=10
        ...  np_total_rx_count_valid_packets=10
        ...  np_total_rx_count_bytes=10000
        ...  np_total_pass_count_packets=10
        ...  np_total_pass_count_bytes=10000
        ...  np_total_deny_count_packets=0
        ...  np_total_deny_count_bytes=0
     ${actualNp1Stats}=  np1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedNp1Stats.keys()}
    \    Log  ${expectedNp1Stats["${key}"]}
    \    should be equal as integers    ${expectedNp1Stats["${key}"]}     ${actualNp1Stats["${key}"]}
    # Verify Statistics on Dynamic Filter
    &{expectedDf1Stats}=  create dictionary
        ...  df_total_insp_count_packets=10
        ...  df_total_insp_count_bytes=10000
        ...  df_total_pass_count_packets=10
        ...  df_total_pass_count_bytes=10000
        ...  df_total_deny_count_packets=0
        ...  df_total_deny_count_bytes=0
    ${actualDf1Stats}=  f1.getFilterStats  statName=all
    :FOR    ${key}    IN    @{expectedDf1Stats.keys()}
    \    Log  ${expectedDf1Stats["${key}"]}
    \    should be equal as integers    ${expectedDf1Stats["${key}"]}     ${actualDf1Stats["${key}"]}
    # Verify Statistics on Tool Port
    &{expectedTp1Stats}=  create dictionary
        ...  tp_total_insp_count_packets=10
        ...  tp_total_pass_count_packets=10
        ...  tp_total_tx_count_packets=10
        ...  tp_total_tx_count_bytes=10380
        ...  tp_total_gre_encap_count_packets=10
        ...  tp_total_gre_encap_percent_packets=1
    ${actualTp1Stats}=  tp1.getPortStats  statName=all
    :FOR    ${key}    IN    @{expectedTp1Stats.keys()}
    \    Log  ${expectedTp1Stats["${key}"]}
    \    should be equal as integers    ${expectedTp1Stats["${key}"]}     ${actualTp1Stats["${key}"]}

    # Cleaup the devices and ports
    # Done during *** *** settings *** test teardown


*** Keywords ***
setupSuiteEnv
#    log   RESOURCE=${RESOURCEPOOL}   console=true
#    releaseResourcePool  ${RESOURCEPOOL}

    Set Global Variable  &{ixChassisDict}  &{EMPTY}

    ${myRequestDef}=  Catenate  {'myPorts': {'type':'NTOPort',
    ...                                         'properties':{'ConnectionType':'standard'}},
    ...                          'myNTO': {'type': 'NTO'}}

    ${myMap}=  Catenate  {'myNTO': {'myPorts': ['np1', 'tp1', 'tp2']}}

    log  Requesting resource(s) from pool: ${RESOURCEPOOL} ...\n  console=true
    ${myReservation}=  requestResourceBlocking  ${RESOURCETIMEOUT}  ${RESOURCEPOOL}  ${myRequestDef}  requestMapParentBound=${myMap}
    Set Global Variable  ${myReservation}

    AddNTO  nto1  ${myReservation['myNTO']['myPorts']['np1']['properties']['IPAddress']}

    AddPort  ${nto1}  np1  NETWORK  ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp1  TOOL     ${myReservation['myNTO']['myPorts']}
    AddPort  ${nto1}  tp2  TOOL     ${myReservation['myNTO']['myPorts']}

suiteCleanup
    releaseResources   ${myReservation['responseId']}

testCleanup
    nto1.cleanupNVSDevices
    nto1.cleanupAllIxPorts
    nto1.cleanupEphemeralNamespace