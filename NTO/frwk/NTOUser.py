class NTOUser:

    """This class represents an NTO user and its attributes.

       Parent Objects:

       * N/A

       Child Objects:

    """

    ROBOT_LIBRARY_DOC_FORMAT = "reST"
    ROBOT_LIBRARY_SCOPE = "TEST SUITE"
    ROBOT_LIBRARY_VERSION = "1.0"

    def __init__(self, uName, isAdmin):
        """ The following constructor creates a default user based on just a user name
        """
        self.email = uName + "@ixiacom.com"
        self.fullName = uName
        self.isLocked = False
        self.isSysAdmin = isAdmin
        self.loginId = uName
        self.password = uName
        self.phoneNum = "512-600-5400"

    def getAttrsForCreate(self):
        """ The following method returns the required elements for user creation.
            Note: the "is_locked" property is not writable during user creation and needs to be left out.
        """
        return {"email" : self.email,
                "full_name" : self.fullName,
                "is_sysadm" : self.isSysAdmin,
                "login_id" : self.loginId,
                "password" : self.password,
                "phone" : self.phoneNum}
