import json
from pprint import pprint

class NTOFilter:

    """A library for *documentation format* demonstration purposes.

    This documentation is created using reStructuredText__. Here is a link
    to the only \`Keyword\`.

    __ http://docutils.sourceforge.net
    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self):
        self.myNTO=None
        self.name=None
        self.defaultName=None
        self.filterMode=None
        self.filterDef=None
        self.id=None
        self.sourcePortIds=[]
        self.sourcePortGrpIds=[]
        self.destPortIds=[]
        self.destPortGrpIds=[]
        self.afmr = None
        self.afmrBandwidth = None
        self.streams = {}
        self.pktProc = None
        self.enabledPktProc = []
        self.stats = {'DF_TOTAL_INSP_COUNT_PACKETS': None,
                      'DF_TOTAL_INSP_COUNT_BYTES': None,
                      'DF_TOTAL_PASS_COUNT_PACKETS': None,
                      'DF_TOTAL_DENY_COUNT_PACKETS': None,
                      'DF_TOTAL_PASS_COUNT_BYTES': None,
                      'DF_TOTAL_DENY_COUNT_BYTES': None}

    def reinit(self):
        self.__init__()

    def create(self, n, fInfo):
        print('Creating filter...')
        self.myNTO = n
        r = self.myNTO.sess.post('https://%s:%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters']),headers={'content-type': 'application/json'}, data=json.dumps(fInfo), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to create filter [(%s): %s]' % (r.status_code, r.text))
        # capture the filter id returned from the NTO
        self.id = json.loads(r.text)['id']
        # After creating the filter, update from the NTO to get all non-specified (default) properties
        self.updateFromNTO()
        print("Created filter %s(%s)" % (self.name, self.id))

    def update(self, fInfo):
        print('Updating filter...')
        r = self.myNTO.sess.put('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters'], self.id),headers={'content-type': 'application/json'}, data=json.dumps(fInfo), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to update filter [(%s): %s]' % (r.status_code, r.text))
        # After creating the filter, update from the NTO to get all non-specified (default) properties
        self.updateFromNTO()

    def updateFromNTO(self):
        """ This method will update internal data members with information pulled from the NTO
        """
        propDict = self.getFilterInfo(self.id)
        for prop in propDict:
            if prop == 'name':
                self.name = propDict[prop]
            elif prop == 'default_name':
                self.defaultName = propDict[prop]
            elif prop == 'mode':
                self.filterMode = propDict[prop]
            elif prop == 'criteria':
                self.filterDef = propDict[prop]
            elif prop == 'id':
                self.id = propDict[prop]
            elif prop == 'source_port_list':
                self.sourcePortIds = propDict[prop]
            elif prop == 'source_port_group_list':
                self.sourcePortGrpIds = propDict[prop]
            elif prop == 'dest_port_list':
                self.destPortIds = propDict[prop]
            elif prop == 'dest_port_group_list':
                self.destPortGrpIds = propDict[prop]
#            else:
#                print("Update filter: ignoring property [%s]" % (prop))

    def setFilterName(self, filterName):
        self.name = filterName

    def setFilterDefaultName(self):
        self.defaultName = self.getFilterInfo()['default_name']

    def setFilterId(self, filterId):
        self.id = filterId

    def setParent(self, parentObj):
        self.myNTO = parentObj

    def setFilterMode(self, filterMode):
        self.filterMode = filterMode

    def setFilterDef(self, filterDef):
        self.filterDef = filterDef

    def rediscoverFilterId(self):
        """ Description:  Helper method used to re-set the Filter id class instance variable.

                 Arguments:
                     * n/a

                 Return Value: n/a """
        print('Issue ReST request to rediscover filter id for %s...' % self.defaultName)
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport,self.myNTO.urls['api/filters'], self.defaultName),headers={'content-type': 'application/json'}, verify=False)
        self.id = json.loads(r.text)['id']

    def showFilter(self):
        print('Filter: %s (mode=%s) = %s' % (self.name, self.filterMode, self.filterDef))

    def addAfmrObj(self, amfrObj, afmrBandwidth):
        """ Description:  Helper method used to set the afmr object class instance variable.

                    Arguments:
                        * amfrObj - Afmr object object associated with the filter.
                        * afmrBandwidth - Afmr Bandwidth object object associated with the filter.

                    Return Value: n/a """
        self.afmr = amfrObj
        self.afmrBandwidth = afmrBandwidth

    def addPktProcToFilterObj(self, pktProcObj):
        """ Description:  Helper method used to set the pktProc class instance variable.

            Arguments:
                * pktProcObj - Packet processing class instance associated with the port.

            Return Value: n/a """
        self.pktProc = pktProcObj

    def createDefaultPassAllFilter(self):
        if self.id is None:
            print('Issue ReST post request to create filter %s ...' % self.name)
            #Do NOT configure the filter name because it may have conflict with an existing filter in the NTO
            #that already has the same name from a previous test or different user:
            #   payload = {'name': self.name, 'mode': 'PASS_ALL'}
            payload = {'mode': 'PASS_ALL'}
            if self.sourcePortIds:
                payload['source_port_list'] = self.sourcePortIds
            if self.sourcePortGrpIds:
                payload['source_port_group_list'] = self.sourcePortGrpIds
            if self.destPortIds:
                payload['dest_port_list'] = self.destPortIds
            if self.destPortGrpIds:
                payload['dest_port_group_list'] = self.destPortGrpIds

            r = self.myNTO.sess.post('https://%s:%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
            if r.status_code != 200:
                raise Exception('ERROR: Unable to create filter %s on NTO chassis (%s) [status code = %s]' % (self.name, self.myNTO.ipaddr, r.status_code))
            else:
                self.id = json.loads(r.text)['id']
                self.setFilterDefaultName()
                print('Successfully created filter %s (id = %s, filterDefaultName = %s)' % (self.name, self.id, self.defaultName))
        else:
            print('Issue ReST put request to configure filter %s (id = %s) ...' % (self.name, self.id))
            payload = {}
            if self.sourcePortIds:
                payload['source_port_list'] = self.sourcePortIds
            if self.sourcePortGrpIds:
                payload['source_port_group_list'] = self.sourcePortGrpIds
            if self.destPortIds:
                payload['dest_port_list'] = self.destPortIds
            if self.destPortGrpIds:
                payload['dest_port_group_list'] = self.destPortGrpIds

            r = self.myNTO.sess.put('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters'], self.id), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
            if r.status_code != 200:
                raise Exception('ERROR: Unable to configure filter %s on NTO chassis (%s) [status code = %s]' % (self.name, self.myNTO.ipaddr, r.status_code))

        return(r.status_code)

    def checkFilterExists(self, myFilterId=None):
        if myFilterId is None:
            myFilterId = self.id
        print('Issue ReST post request to see if filter %s exists on NTO (%s) ...' % (myFilterId, self.myNTO.ipaddr))
        payload = {'id': myFilterId}
        r = self.myNTO.sess.post('https://%s:%s/%s/search' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to search for filter %s on NTO chassis (%s) [status code = %s]' % (myFilterId, self.myNTO.ipaddr, r.status_code))
        return (json.loads(r.text))

    def getFilterInfo(self, myFilterId=None):
        if myFilterId is None:
            myFilterId = self.id
        print('Issue ReST request to get filter %s info ...' % myFilterId)
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters'], myFilterId), headers={'content-type': 'application/json'}, verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to get filter %s details from NTO chassis (%s) [status code = %s]' % (myFilterId, self.myNTO.ipaddr, r.status_code))
        return (json.loads(r.text))

    def setFilterInfo(self, payload):
        print('Issue ReST request to set filter %s info ...' % self.id)
        r = self.myNTO.sess.put('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters'], self.id), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            print("ERROR: An error occurred while updating filter (%s): [%s] - %s" % (self.name, r.status_code, r.text))
            raise Exception("ERROR: An error occurred while updating filter (%s): [%s] - %s" % (self.name, r.status_code, r.text))
        return (r)

    def configureFilter(self, myFilterId=None, myFilterMode=None, myFilterDef=None):
        myFilterDict={}
        if myFilterId is None:
            if self.id is None:
                # This is to cover the filter creation case
                myFilterId = ""
            else:
                myFilterId = self.id
        if myFilterMode is None:
            myFilterMode = self.filterMode

            #Check to see if the filterMode is not pass all or deny all before assigning the filterDef.
            if myFilterMode != 'PASS_ALL' and myFilterMode !='DENY_ALL' and myFilterMode != 'PBC_UNMATCHED' and myFilterMode != 'DBC_MATCHED':
                if myFilterDef is None:
                    myFilterDef = self.filterDef

                #Convert the string to a dict and add the mode to the filter.
                myFilterDict = eval(myFilterDef)

            myFilterDict['mode'] = myFilterMode

        if myFilterId == "":
            print("Issue ReST post request to create filter [" + self.name + "]...")
            r = self.myNTO.sess.post('https://%s:%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters']), headers={'content-type': 'application/json'}, data=json.dumps(myFilterDict), verify=False)
        else:
            print('Issue ReST put request to configure filter %s with %s ...' % (myFilterId, self.filterDef))
            r = self.myNTO.sess.put('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters'], myFilterId), headers={'content-type': 'application/json'}, data=json.dumps(myFilterDict), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to configure filter %s on NTO chassis (%s) [status code = %s]' % (myFilterId, self.myNTO.ipaddr, r.status_code))
        # The following case is for filter creation and grabs the assigned filter id
        if myFilterId == "":
            self.id = json.loads(r.text)['id']
        return (r.status_code)

    def getFilterStats(self, myFilterId=None, statName=None):
        if myFilterId is None:
            myFilterId = self.id

        if statName is None or statName == 'all':
            print('Issue ReST post request to get filter %s stats ...' % myFilterId)
            payload = {'filter': myFilterId}
        else:
            print('Issue ReST post request to get filter %s stat %s ...' % (myFilterId, statName))
            payload = {'filter': myFilterId, 'stat_name': statName}

        r = self.myNTO.sess.post('https://%s:%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/stats']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            if statName is None:
                raise Exception('ERROR: Unable to get filter %s stats from NTO chassis (%s) [status code = %s]' % (myFilterId, self.myNTO.ipaddr, r.status_code))
            else:
                raise Exception('ERROR: Unable to get filter %s stat %s from NTO chassis (%s) [status code = %s]' % (myFilterId, statName, self.myNTO.ipaddr, r.status_code))

        if statName is None:
            retStats = json.loads(r.text)['stats_snapshot'][0]
            for stat in self.stats.keys():
                try:
                    self.stats[stat] = retStats[stat.lower()]
                except KeyError:
                    pass

            return (self.stats)
        elif statName == 'all':
            return(json.loads(r.text)['stats_snapshot'][0])
        else:
            return(json.loads(r.text)['stats_snapshot'][0][statName.lower()])

    def resetFilterStats(self, myFilterId=None):
        if myFilterId is None:
            myFilterId = self.id
        print('Issue ReST post request to reset filter %s stats ...' % myFilterId)
        payload = {'filter': myFilterId}
        r = self.myNTO.sess.post('https://%s:%s/%s/reset_stats' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/stats']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to reset filter %s stats from NTO chassis (%s) [status code = %s]' % (myFilterId, self.myNTO.ipaddr, r.status_code))
        return (r.status_code)

    def deleteFilterById(self, myFilterId=None):
        if myFilterId is None:
            myFilterId = self.id
        print('Issue ReST request to delete a filter by Id ...')
        r = self.myNTO.sess.delete('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters'], myFilterId), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to delete filter %s on NTO chassis (%s) [status code = %s]' % (myFilterId, self.myNTO.ipaddr, r.status_code))
        self.myNTO.filters[myFilterId].remove()
        return (r.status_code)

    def attachATIPResource(self, atipResource):
        # Check the ATIP status for READY before attempting to attach.
        ATIPstatus = atipResource.getATIPResourceInfo(property='resource_status')
        if ATIPstatus == 'READY':
            print('Issue ReST put request to attach ATIP resource (%s) to filter (%s) ...')
            r = self.myNTO.sess.put('https://%s:%s/%s/%s/enable' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/atip_resources'], atipResource.atipId), headers={'content-type': 'application/json'}, data=json.dumps({'filter_id': self.id}), verify=False)
            if r.status_code != 200:
                raise Exception('ERROR: Unable to attach ATIP (%s) to filter (%s) on NTO chassis (%s) [status code = %s]' % (atipResource.atipId, self.id, self.myNTO.ipaddr, r.status_code))
            return (r.status_code)
        else:
            raise Exception('ERROR: Unable to attach ATIP (%s) because it is not in READY state [status = %s]' % (atipResource.atipId, ATIPstatus))

    def configureAppForwardingMaps(self, *args):

        """ Description: Takes one or more appForwarding maps and adds them to the filter properties.

                    Arguments:
                        **args - One or more app forwarding maps (dictionaries).

                    Return Value: n/a """

        #Use list comprehension to set the app forwarding maps in the filter properties.
        r = self.setFilterInfo({'application_forwarding_map': [appFwdMap for appFwdMap in args]})
        if r.status_code != 200:
            raise Exception('ERROR: Failed (errno = %s) to set NTO (%s) filter (%s) application forwarding map - %s' % (r.status_code, self.myNTO.ipaddr, self.id, payload))

    def getResourceAttachmentType(self, myFilterId=None):
        """ Description:  ReST method to configure properties of a filter.

            Arguments:
                * myPortId - (optional) filter Id

            Return Value:
                * the resource attachment type from the ReST reponse string """
        if myFilterId is None:
            myFilterId = self.id

        print('Issue ReST request to get resource attachment type ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s?properties=resource_attachment_type' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters'], myFilterId), verify=False)
        return (json.loads(r.text)['resource_attachment_type'])



    def attachAFMResource(self, afmResource, allocBW=None):
        """ Description:  ReST method to attach an AFMR to a filter.

            Arguments:
                * afmResource - AFMR resouce object.
                * allocBW - BW to be allocated for this AFMR.

            Return Value: n/a """
        # Check the AFMR status for READY before attempting to attach.
        AFMRstatus = afmResource.getAFMResourceInfo(property='resource_status')
        if AFMRstatus == 'READY':
            print('Issue ReST put request to attach AFM resource to AFM filter object ...')
            print(self.myNTO.getSystemModel())
            if allocBW is None:
                if self.myNTO.getSystemModel() == "VISION_ONE":
                    allocBW = 10
                    print('Issue ReST put request to attach default AFM resource to AFM filter object VISION_ONE ...')
                elif self.myNTO.getSystemModel() == "7300":
                    allocBW = 25
                    print('Issue ReST put request to attach default AFM resource to AFM filter object 7300 ...')
            r = self.myNTO.sess.put('https://%s:%s/%s/%s/enable' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/recirculated_afm_resources'], afmResource.afmrId), headers={'content-type': 'application/json'},
                                    data=json.dumps({'allocated_bandwidth': allocBW, 'object_id': self.id}), verify=False)
            if r.status_code != 200:
                raise Exception(
                    'ERROR: Unable to attach AFMR (%s) to object (%s) on NTO chassis (%s) [status code = %s]' % (afmResource.afmrId, self.id, self.myNTO.ipaddr, r.status_code))
            self.addAfmrObj(afmResource, allocBW)
            afmResource.addAttachedObj(self)
            return (r.status_code)
        else:
            raise Exception('ERROR: Unable to attach AFMR (%s) because it is not in READY state [status = %s]' % (afmResource.afmrId, AFMRstatus))

    def detachResource(self, afmResource=None):
        """ Description:  ReST method to dettach an AFMR to a port.

                     Arguments:
                         * afmResource - AFMR resouce object.

                     Return Value: n/a """
        if afmResource is None:
            afmResource = self.afmr
        if afmResource is not None:
            print('Issue ReST put request to detach AFM resource from NTO port object ...')
            r = self.myNTO.sess.put('https://%s:%s/%s/%s/disable' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/recirculated_afm_resources'], afmResource.afmrId), headers={'content-type': 'application/json'},
                                    data=json.dumps({'object_id': self.id}), verify=False)
            if r.status_code != 200:
                raise Exception('ERROR: Unable to detach AFMR (%s) from object (%s) on NTO chassis (%s) [status code = %s]' % (afmResource.afmrId, self.id, self.myNTO.ipaddr, r.status_code))
            afmResource.attachedFilters.pop(afmResource.attachedFilters.index(self))
            self.afmr = None
            self.afmrBandwidth = None
            return (r.status_code)

    def configurePacketProcessing(self, pktProcessing):
        """ Description:  ReST method to configure AFMR packet processing for a filter.

            Arguments:
                * pktProcessing - dictionary containing the packet processing definition

            Return Value: n/a """
        print('Issue ReST request to configure port %s packet processing ...' % self.defaultName)
        pktProcDict = eval(pktProcessing)
        self.enabledPktProc.append(pktProcDict)


        #For AFMR, specify the packetprocessing in the resource_attachment_config.
        if self.getResourceAttachmentType() == 'RECIRCULATED_AFM':
            pktProcDict = {'resource_attachment_config': pktProcDict}
            pprint(pktProcDict)
        else:
            print("AFM is not attached to filter %s " % (self.defaultName))

        #Send the ReST put request.
        r = self.setFilterInfo(pktProcDict)
        if r.status_code != 200:
            print(r.text)
            raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) filter (%s) packet processing: %s\n%s' % (r.status_code, self.myNTO.ipaddr, self.id, pktProcDict, r.text.splitlines()[0]))
