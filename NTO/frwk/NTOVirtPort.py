import json
import time

class NTOVirtPort:

    """This library handles virtual port(s) for an NTO Port within the RobotFramework.  Virtual ports are only supported on tool mode ports.

       Parent Objects:

       * myPort - Port parent object.

       Child Objects:

       N/A.
    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self):
        self.myParent=None
        self.name=None
        self.defaultName=None
        self.portDef=None
        self.id=None
        self.streams={}
        self.prtDescription=None
        self.tunnelOrigLocalSettings=None
        self.tunnelOrigRemoteSettings=None

        self.stats = {'TOOL': {'TP_TOTAL_GRE_ENCAP_COUNT_PACKETS': None,
                               'TP_TOTAL_GRE_ENCAP_PERCENT_PACKETS': None}}

    def reinit(self):
        self.__init__()

    def setParent(self, parentObj):
        self.myParent = parentObj

    def setVirtPortName(self, vpName):
        self.portName = vpName

    def rediscoverVirtPortId(self):
        """ Description:  Helper method used to Re set the virtual port id class instance variable.

            Arguments:
                * n/a

            Return Value: n/a """
        print('Issue ReST request to rediscover Virtual Port id for %s...' % self.defaultName)
        r = self.myParent.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myParent.myNTO.ipaddr, self.myParent.myNTO.ipport, self.myParent.myNTO.urls['api/ports'], self.defaultName),headers={'content-type': 'application/json'}, verify=False)
        self.id = json.loads(r.text)['id']
        self.myParent.rediscoverPortId()

    def setTunnelOrigLocal(self, localSettingsStr):
        self.tunnelOrigLocalSettings=eval(localSettingsStr)

    def setTunnelOrigRemoteIP(self, remoteIP):
        self.tunnelOrigRemoteSettings = {'remote_ip_address': remoteIP}

    def setVirtPortDefaultName(self):
        """ Description:  Helper method used to set the virtual port default name class instance variable.

            Arguments:
                * vpDefaultName - default name for the virtual port.

            Return Value: n/a """
        self.defaultName = self.getVirtPortInfo()['default_name']

    def showVirtPort(self):
        print('Virtual Port %s: (def=%s)' % (self.name, self.portDef))

    def setVirtPortDescription(self, vpDesc):
        """ Description:  Helper method used to set the virtual port description class instance variable.

            Arguments:
                * vpDesc - Description of the virtual port.

            Return Value: n/a """
        self.portDescription = vpDesc

    def setVirtPortInfo(self, payload):
        """ Description:  ReST method used to create the virtual port or configure the virtual port's info.

            Arguments:
                * payload: Dictionary of virtual port attributes.

            Return Value:
                * ReST response object """

        if self.id is None:
            print('Issue ReST request to create virtual port ...')
            r = self.myParent.myNTO.sess.post('https://%s:%s/%s' % (self.myParent.myNTO.ipaddr, self.myParent.myNTO.ipport, self.myParent.myNTO.urls['api/ports']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        else:
            print('Issue ReST request to set virtual port info ...')
            r = self.myParent.myNTO.sess.put('https://%s:%s/%s/%s' % (self.myParent.myNTO.ipaddr, self.myParent.myNTO.ipport, self.myParent.myNTO.urls['api/ports'], self.id), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)

        return (r)

    def getVirtPortInfo(self, virtPortIdentifier=None):
        """ Description:  ReST method used to query the virtual port's info.

            Arguments:
                * virtPortIdentifier: (optional) virtual Port identifier.

            Return Value:
                * dictionary of virtual port info in from the ReST response string """
        if virtPortIdentifier is None: virtPortIdentifier = self.id
        print('Issue ReST request to get virtual port info for %s...' % virtPortIdentifier)
        r = self.myParent.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myParent.myNTO.ipaddr, self.myParent.myNTO.ipport, self.myParent.myNTO.urls['api/ports'], virtPortIdentifier), verify=False)
        print(json.loads(r.text))
        return (json.loads(r.text))

    def configurePort(self, remoteIP=None, localSettings=None):
        """ Description:  ReST method to create the dictionary for the VirtualPort creation or VirtualPort Property Update

            Arguments:
                * localSettings: (optional) localSettings Dict to configure localSettings on the ParentPort
                * remoteIP: (optional) remoteIP to configure remoteIP on VirtualPort
            Return Value: n/a """

        if self.id is None and remoteIP is None and localSettings is None:
            print('Issue ReST request to create virtual port %s tunnel orig remote settings ...' % self.name)
            virtPortProps = {}
            virtPortProps['name'] = '%s:%s<->????' % (self.name, self.name)
            virtPortProps['type'] = 'VIRTUAL_PORT'
            virtPortProps['parent_port_id'] = self.myParent.id

            if self.tunnelOrigRemoteSettings is not None:
                virtPortProps['tunnel_origination_remote_settings'] = self.tunnelOrigRemoteSettings

            # Send the ReST post request to create the virtual port
            r = self.setVirtPortInfo(virtPortProps)
            if r.status_code != 200:
                raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port (%s) tunnel orig remote settings: %s\n%s' % (r.status_code, self.myParent.myNTO.ipaddr, self.name, virtPortProps, r.text))
            else:
                self.id = json.loads(r.text)['id']
                self.setVirtPortDefaultName()
                print('Successfully created virtual port %s (id = %s, defaultName = %s)' % (self.name, self.id, self.name))

            #Enable tunnel origination on physical parent (tool) port.
            print('Issue ReST put request to configure port %s tunnel orig local settings ...' % self.myParent.name)
            if self.tunnelOrigLocalSettings is not None:
                payload = {}
                payload['tunnel_origination_local_settings'] = self.tunnelOrigLocalSettings

                r = self.myParent.setPortInfo(payload)
                if r.status_code != 200:
                    raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port (%s) tunnel orig local settings: %s\n%s' % (r.status_code, self.myParent.myNTO.ipaddr, self.myParent.id, payload, r.text))
            return

        #Send put request on the parent port to update the local settings
        if localSettings is not None:
            self.setTunnelOrigLocal(localSettings)
            print('Issue ReST put request to configure port %s tunnel orig local settings ...' % self.myParent.defaultName)
            payload = {}
            payload['tunnel_origination_local_settings'] = self.tunnelOrigLocalSettings

            r = self.myParent.setPortInfo(payload)
            if r.status_code != 200:
                raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port (%s) tunnel orig local settings: %s\n%s' % (r.status_code, self.myParent.myNTO.ipaddr, self.myParent.portId, payload, r.text))

        #Send put request on the virtual port to update the remote settings
        if remoteIP is not None :
            self.setTunnelOrigRemoteIP(remoteIP)
            print('Issue ReST request to configure virtual port %s tunnel orig remote settings ...' % self.name)
            virtPortProps = {}
            virtPortProps['tunnel_origination_remote_settings'] = self.tunnelOrigRemoteSettings

            # Send the ReST put request.
            r = self.setVirtPortInfo(virtPortProps)
            if r.status_code != 200:
                raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port (%s) tunnel orig remote settings: %s\n%s' % (r.status_code, self.myParent.myNTO.ipaddr, self.portName, virtPortProps, r.text))


    def enableVirtPort(self):
        """ Description:  ReST method to enable the port's virtual port.

            Arguments: n/a

            Return Value: n/a """
        print('Issue ReST request to enable a virtual port for port %s ...' % self.defaultName)

        self.tunnelOrigLocalSettings['enabled']=True
        payload={}
        payload['tunnel_origination_local_settings']=self.tunnelOrigLocalSettings

        r = self.myParent.setPortInfo(payload)
        if r.status_code != 200:
            raise Exception('ERROR: Failed (errno = %s) to enable NTO (%s) port (%s) settings: %s\n%s' % (r.status_code, self.myParent.myNTO.ipaddr, self.myParent.id, payload, r.text.splitlines()[0]))

    def disableVirtPort(self):
        """ Description:  ReST method to disable the port's virtual port.

            Arguments: n/a

            Return Value: n/a """

        self.tunnelOrigLocalSettings['enabled']=False
        payload={}
        payload['tunnel_origination_local_settings']=self.tunnelOrigLocalSettings

        r = self.myParent.setPortInfo(payload)
        if r.status_code != 200:
            raise Exception('ERROR: Failed (errno = %s) to enable NTO (%s) port (%s) settings: %s\n%s' % (r.status_code, self.myParent.myNTO.ipaddr, self.myParent.id, payload, r.text.splitlines()[0]))

    def deleteVirtPortById(self, myVirtPortId=None):
        """ Description:  ReST method to delete a virtual port.

            Arguments:
                myVirtPortId - Virtual port ID to delete (default=None).

            Return Value: n/a """
        if myVirtPortId is None:
            myVirtPortId = self.id
        print('Issue ReST request to delete a virtual port by Id ...')
        r = self.myParent.myNTO.sess.delete('https://%s:%s/%s/%s' % (self.myParent.myNTO.ipaddr, self.myParent.myNTO.ipport, self.myParent.myNTO.urls['api/ports'], myVirtPortId), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Failed (errno = %s) to delete NTO (%s) virtual port (%s) settings: %s' % (r.status_code, self.myParent.myNTO.ipaddr, myVirtPortId, r.text.splitlines()[0]))

    def getPortStats(self, myPortId=None, statName=None):
        """ Description:  ReST method to get the statistics for a virtual port.

            Arguments:
                * myPortId - (optional) port Id
                * statName - (optional) explicit name for a statistic

            Return Value:
                * Either the dictionary of all stats for a snapshot or the value for an explicitly named statistic """
        if myPortId is None:
            myPortId = self.id

        if statName is None or statName == 'all':
            print('Issue ReST post request to get port %s stats ...' % myPortId)
            payload = {'port': myPortId}
        else:
            print('Issue ReST post request to get port %s stat %s ...' % (myPortId, statName))
            payload = {'port': myPortId, 'stat_name': statName}

        r = self.myParent.myNTO.sess.post('https://%s:%s/%s' % (self.myParent.myNTO.ipaddr, self.myParent.myNTO.ipport, self.myParent.myNTO.urls['api/stats']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            if statName is None:
                raise Exception('ERROR: Unable to get virtual port %s stats from NTO chassis (%s) [status code = %s]' % (myPortId, self.myParent.myNTO.ipaddr, r.status_code))
            else:
                raise Exception('ERROR: Unable to get virtual port %s stat %s from NTO chassis (%s) [status code = %s]' % (myPortId, statName, self.myParent.myNTO.ipaddr, r.status_code))

        if statName is None:
            retStats = json.loads(r.text)['stats_snapshot'][0]
            for stat in self.stats[self.myParent.portMode].keys():
                try:
                    self.stats[self.myParent.portMode][stat] = retStats[stat.lower()]
                except KeyError:
                    pass

            return(self.stats[self.myParent.portMode])
        elif statName == 'all':
            return(json.loads(r.text)['stats_snapshot'][0])
        else:
            print('NTO Virtual Port %s %s = %s ...' % (myPortId, statName, json.loads(r.text)['stats_snapshot'][0][statName.lower()]))
            return(json.loads(r.text)['stats_snapshot'][0][statName.lower()])