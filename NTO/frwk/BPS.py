import json
import os

import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder


class BPS:

    """A library for *documentation format* demonstration purposes.

    This documentation is created using reStructuredText__. Here is a link
    to the only \`Keyword\`.

    __ http://docutils.sourceforge.net
    """

    def __init__(self, bpsIp, bpsUser, bpsPassword):
        self.bpsIp = bpsIp
        self.bpsUser = bpsUser
        self.bpsPassword = bpsPassword
        self.sess = requests.Session()

    def startBps(self,bpsTest, group):
        url = 'https://{}/api/v1/bps/tests/operations/start'.format(self.bpsIp)
        payload ={
            "modelname": bpsTest,
            "group": group}
        r = self.sess.post(url,
                          headers={'content-type': 'application/json'},
                          data=json.dumps(payload), verify=False)
        print(r.json())
        return r

    def authBps(self):
        url = 'https://{}/api/v1/auth/session'.format(self.bpsIp)
        payload = {"username": self.bpsUser,"password": self.bpsPassword}
        r = self.sess.post(url,
                           headers={'content-type': 'application/json'},
                           data=json.dumps(payload), verify=False)
        print(r.json())
        return r

    def stopBps(self, testid):
        url = 'https://{}/api/v1/bps/tests/operations/stop'.format(self.bpsIp)
        payload = {"testid": testid}
        r = self.sess.post(url,
                          headers={'content-type': 'application/json'},
                          data=json.dumps(payload), verify=False)
        print(r.json())
        return r

    def reserveBpsPorts(self, slot,portList, group):
        url = 'https://{}/api/v1/bps/ports/operations/reserve'.format(self.bpsIp)
        payload = {
            "slot": slot,
            "portList": portList,
            "group": group,
            "force": "true"
        }
        r = self.sess.post(url,
                           headers={'content-type': 'application/json'},
                           data=json.dumps(payload), verify=False)
        print(r.json())
        return r


#bps = BPS("10.215.157.144", "mihail","mihail")
#bps.authBps()
#bps.reserveBpsPorts("5", [0,1,2,3], "1")