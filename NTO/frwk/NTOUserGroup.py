class NTOUserGroup:

    """This class represents an NTO user group and its attributes.

       Parent Objects:

       * N/A

       Child Objects:

    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'GLOBAL'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self, gName):
        # the following elements are required for group creation
        self.autoCreate = False
        self.description = gName
        self.members = []
        self.name = gName
        self.owners = []
        # the remaining items can only be set by pulling group properties from the NTO
        self.accessibleFilters = []
        self.accessiblePorts = []
        self.created = ""
        self.history = []
        self.id = None
        self.modCount = 0


    def update(self, attrs):
        """ The following method updates the class data members using the attributes obtained
            from the NTO.
        """
        print("Updating group (" + self.name + ") attributes...")
        self.autoCreate = attrs['auto_created']
        self.description = attrs['name']
        self.members = attrs['members']
        self.name = attrs['name']
        self.owners = attrs['owners']
        self.accessibleFilters = attrs['accessible_filters']
        self.accessiblePorts = attrs['accessible_ports']
        self.created = attrs['created']
        self.history = attrs['history']
        self.id = attrs['id']
        self.modCount = attrs['mod_count']


    def getAttrsForCreate(self):
        """ The following method returns the required elements for group creation.
            Note: the "is_locked" property is not writable during user creation and needs to be left out.
        """
        return {"auto_created" : self.autoCreate,
                "description" : self.description,
                "members" : self.members,
                "name" : self.name,
                "owners" : self.owners}


    def getAttrsForUpdate(self):
        """ The following method returns the required elements for group creation.
            Note: the "is_locked" property is not writable during user creation and needs to be left out.
        """
        return {"auto_created" : self.autoCreate,
                "description" : self.description,
                "members" : self.members,
                "name" : self.name,
                "owners" : self.owners,
                "mod_count" : self.modCount}

    def addUser(self, uName):
        print("Adding user (" + str(uName) + ") to group (" + self.name + ")...")
        self.members.append(uName)