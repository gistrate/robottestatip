from pprint import pprint

# Import scapy and suppress nuisance warnings.
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
import codecs

# Load scapy protocol contributions [Note: order does matter (i.e. specific port bindings go first and anue should always go last)].
load_contrib('mpls')
load_contrib('gtp')
load_contrib('cfp')
load_contrib('vxlan')
load_contrib('erspan')
load_contrib('etaghdr')
load_contrib('vntag')
load_contrib('lisp_data')
load_contrib('anue')



class PktProc:

    """This library forms the base class for the NTO PacketProcessing emulation.

       Parent Objects:

       * n/a

       Child Objects:

       * n/a
    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self):
        self.myParent=None

    def reinit(self):
        self.__init__()

    def setParent(self, parentObj):
        """ Description:  Helper method used to set the NTO parent object.

            Arguments: n/a

            Return Value: n/a """
        self.myParent = parentObj

    def scapyDecodeBytes(self, pkt):
        """ Description:  Helper method used to evaluate byte string as scapy packet with starting layer CFP or Ether.

                    Arguments: pkt - Normal scapy packetObj or byte string

                    Return Value: n/a """

        pkt = bytes(pkt)
        if int(codecs.encode(pkt[12:14], 'hex'), 16) == 0x8903:
            return CFP(pkt)
        else:
            return Ether(pkt)


    def emulate(self, pktProcStr, frames=None, pcapFile=None, stat=False, verbose=False):
        """ Description: Emulate packet processing on a list of frames or frames within a pcap capture..

            Arguments:
                * pktProcStr - string (json) containing the packet processing definition
                * frames - (default=None) Input frames for packet processing emulation OR for stream name for stat analysis
                * pcapFile - (default=None) Pcap captured frame format for packet processing emulation.
                * verbose - (default=False) To set the verbosity level on displayed original and modified packets
                * stat - (default=False) Changes the behaviour or the method to not return modified streams but to update the streams from the NTO Obj in the Connection Path

            Return Value:
                * sentHex - List of packets with no emulation
                * emuPktHex - List of packets with emulation from methods
                * None - Returns None if method is used for stat analysis """

        #If Stat paramenter is set to True method is set for stat analysis
        if stat is True:
            origStream = frames
            frames = list(self.myParent.streams[frames]['pkt'])

        sentHex=[]
        #Check to see if frames are coming from a pcap file.
        if pcapFile is not None and pcapFile != 'None':
            decodedFrames = rdpcap(pcapFile)
        elif (frames is None or frames == 'None') and (pcapFile is None or pcapFile == 'None'):
            raise Exception('Error:  Both frames and pcapCapture arguments are None!')
        else:
            #Decode bytes objects into scapy frames.
            decodedFrames = [self.scapyDecodeBytes(pkt) for pkt in frames]
            print()
        #Eval the pktProcStr (if not None).
        if pktProcStr is None or pktProcStr == 'None':
            print ("WARNING:  No processing was performed on sent frames!!!")
            for pkt in decodedFrames:
                if verbose:
                    print('\nUnmodified:\n')
                    hexdump(pkt)
                    pkt.show()
                sentHex.append(bytes(pkt))

            return(sentHex)

        else:
            #Evaluate the packet processing string(json).
            if isinstance(pktProcStr, str):
                try:
                    pktProcDict = eval(pktProcStr)
                except SyntaxError:
                    raise Exception('Error:  Improper syntax used with the packet processing json.')
            else:
                pktProcDict=pktProcStr

        #Call to the other methods of the class based on the first string argument of the current method
        for key in pktProcDict.keys():
            pktProcMethod = getattr(self, key)

        emuPktHex = pktProcMethod(pktProcDict, decodedFrames , verbose)

        if stat is False:
            return (emuPktHex)
        #Update the parent's stream dict with AFM processing
        else:
            self.myParent.streams[origStream]['pkt'] = self.scapyDecodeBytes(emuPktHex[0])
            self.myParent.streams[origStream]['frameSize'] = len(emuPktHex[0])
            self.myParent.streams[origStream]['pkt'].display()


    def mpls_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate MPLS Stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'mpls_strip_settings' in pktProcDict:
            if pktProcDict['mpls_strip_settings']['enabled']:
                if pktProcDict['mpls_strip_settings']['service_type'] == 'L2_VPN_WITHOUT_CONTROL':
                    print('Stripping %s sent frames of MPLS headers...' % self.myParent.name)
                    for pkt in decodedFrames:

                        if pkt.haslayer(MPLS):
                            if verbose:
                                print('\nBefore:\n')
                                pkt.show()

                            #Skip to first MPLS layer.
                            stripPkt = pkt.getlayer(MPLS)

                            #Drop each MPLS layer until there are none left.
                            while stripPkt.haslayer(MPLS):
                                stripPkt = stripPkt.payload

                            if verbose:
                                print('\n\nAfter:\n')
                                stripPkt.show()
                                print()

                            #NTO pads stripped packets with zeros out to 60 bytes
                            if len(stripPkt) < 60:
                                sentHex.append(bytes(stripPkt) + (b'\x00' * (60 - len(stripPkt))))
                            else:
                                sentHex.append(bytes(stripPkt))

                        else:
                            #Save unmodified packet to sent list.
                            sentHex.append(bytes(pkt))


        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def fabric_path_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate Fabric Path Stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'fabric_path_strip_settings' in pktProcDict:

            if pktProcDict['fabric_path_strip_settings']['enabled']:
                print('Stripping %s sent frames of Cisco Fabric Path headers...' % self.myParent.name)
                for pkt in decodedFrames:

                    if pkt.haslayer(CFP):
                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()

                        #Strip off first CFP layer.
                        pkt = pkt[1]

                        if verbose:
                            print('\nAfter:\n')
                            pkt.show()
                            print()

                    #Save packet to sent list.
                    sentHex.append(bytes(pkt))

        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def trailer_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate Trailer Stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'trailer_strip_settings' in pktProcDict:
            if pktProcDict['trailer_strip_settings']['enabled']:
                print('Stripping %s sent frames of Anue trailers...' % self.myParent.name)
                for pkt in decodedFrames:

                    if pkt.haslayer(ANUE):
                        if verbose:
                            print('\nBefore:\n')
                            hexdump(pkt)
                            pkt.show()

                        #Slice byte string since it has no spaces to contend with.
#                        myByteStr = bytes(pkt)[0:(len(pkt) - len(ANUE()))]
                        if pkt[ANUE].type == 1:
                            myByteStr = bytes(pkt)[0:(len(pkt) - len(ANUE(type=1)))]
                        elif pkt[ANUE].type == 2:
                            myByteStr = bytes(pkt)[0:(len(pkt) - len(ANUE(type=2)))]
                        else:
                            myByteStr = bytes(pkt)[0:(len(pkt) - len(ANUE(type=3)))]

                        if verbose:
                            print('\nAfter:\n')
                            hexdump(myByteStr)
                            print()

                        sentHex.append(myByteStr)

                    else:
                        #Save packet to sent list.
                        sentHex.append(bytes(pkt))

        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def packet_length_trailer_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate Packet Lenght Trailer on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'packet_length_trailer_settings' in pktProcDict:
            if pktProcDict['packet_length_trailer_settings']['enabled']:
                print('Adding Anue trailer with original pkt length to %s sent frames...' % self.myParent.name)
                for pkt in decodedFrames:

                    if verbose:
                        print('\nBefore:\n')
                        hexdump(pkt)
                        pkt.show()


                    myByteStr = bytes(pkt/ANUE(type=1))

                    if verbose:
                        print('\nAfter:\n')
                        hexdump(myByteStr)
                        print()

                    sentHex.append(bytes(myByteStr))

        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def timestamp_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate Timestamp on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'timestamp_settings' in pktProcDict:
            if pktProcDict['timestamp_settings']['enabled']:
                print('Adding Anue trailer with local timestamp to %s sent frames...' % self.myParent.name)
                for pkt in decodedFrames:

                    if verbose:
                        print('\nBefore:\n')
                        pkt.show()
                        hexdump(pkt)

                    #Get current time.
                    timenow = time.time()

                    #Split time into two integer values.
                    time_upper = int(str(timenow).split('.')[0])
                    time_lower = int(str(timenow).split('.')[1])

                    myByteStr = bytes(pkt/ANUE(type=self.myParent.myNTO.timeStampSrc, time_int=time_upper, time_dec=time_lower))

                    if verbose:
                        print('\nAfter:\n')
                        hexdump(myByteStr)
                        print()

                    sentHex.append(bytes(myByteStr))
        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def std_vlan_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate Std Vlan Stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""

        sentHex=[]
        if 'std_vlan_strip_settings' in pktProcDict:
            if pktProcDict['std_vlan_strip_settings']['enabled']:
                for pkt in decodedFrames:

                    if verbose:
                        hexdump(pkt)
                        pkt.show()

                    if type(pkt) is Ether and hex(pkt.type) in ['0x8100', '0x9100', '0x88a8']:
                        #recreate the packet so all the layers are visible when etherType is 0x9100 or 0x88a8
                        upperLayers = pkt.payload
                        upperLayerDecoded = Dot1Q(bytes(upperLayers))
                        del pkt[1]
                        pkt.add_payload(upperLayerDecoded)

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()
                        #define tpid fom 802.1q header for which the second vlan can be stripped
                        VlanTPIDs=['0x8100']
                        if 'ingress_count' in pktProcDict['std_vlan_strip_settings'] or 'egress_count' in pktProcDict['std_vlan_strip_settings']:
                            if ('ingress_count' in pktProcDict['std_vlan_strip_settings']) and  (str(pktProcDict['std_vlan_strip_settings']['strip_mode']).upper()=='INGRESS'):
                                numVlansToStrip = pktProcDict['std_vlan_strip_settings']['ingress_count']
                                if verbose:  print('Striping %s sent frames of %s ingress Vlan header(s)...' % (self.myParent.name, numVlansToStrip))
                                #define tpid from Ethernet layers for which first vlan is stripped on the network port
                                etherTypes=['0x8100', '0x9100', '0x88a8']
                            if ('egress_count' in pktProcDict['std_vlan_strip_settings']) and (str(pktProcDict['std_vlan_strip_settings']['strip_mode']).upper()=='EGRESS'):
                                numVlansToStrip = pktProcDict['std_vlan_strip_settings']['egress_count']
                                if verbose:  print('Striping %s sent frames of %s egress Vlan header(s)...' % (self.myParent.name, numVlansToStrip))
                                # define tpid from Ethernet layers for which first vlan is stripped on the tool port
                                etherTypes=['0x8100']
                        else:
                            raise Exception('ERROR: Unknown definition for std_vlan_strip_settings %s' % pktProcDict['std_vlan_strip_settings'])
                        #get the entire packet
                        layer = pkt.firstlayer()
                        count = 0
                        tpid = 0
                        while not isinstance(layer, NoPayload):
                            if type(layer) is Ether:
                                #get ethertype from ethernet layer; this field will be used to strip the vlans only with a specific tpid defined before
                                tpid = hex(layer.type)
                            if (type(layer) is Dot1Q) and (count == 1 or count == 2) and (tpid in etherTypes):
                                if verbose:  print('Packet is modified')
                                if verbose:  print('First vlan is stripped')
                                #strip payload contains the layers of the packet that are above of the first 802.1q layer
                                stripPayload = layer.payload
                                vlanTPID = hex(layer.type)
                                newType=layer.type
                                # Do double Vlan stripping if enabled and 2nd Vlan is present.
                                if numVlansToStrip == 2 and (type(stripPayload) is Dot1Q) and (vlanTPID in VlanTPIDs):
                                    #strip payload contains the layers of the packet that are above of the second 802.1q layer
                                    newType = stripPayload.type
                                    stripPayload = stripPayload.payload
                                    if verbose:  print('Second vlan is stripped')
                                del pkt[count]
                                #add strip payload to the ethernet layer
                                pkt.add_payload(stripPayload)
                                # Force scapy recompute on L2 type field (i.e. stripping Dot1Q and leaving IP).
                                if verbose:  print('new type is %s' % hex(newType))
                                pkt[Ether].type=newType
                                break

                            # advance to the next layer
                            layer = layer.payload
                            count += 1
                        if verbose:
                            print('\nAfter:\n')
                            pkt.show()
                            print()
                    #Save packet to sent list.
                    sentHex.append(bytes(pkt))


        else:
             #Append unaltered packet to sent list.
             sentHex.append(bytes(pkt))

         #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def std_port_tagging_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate Std Port Tagging on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""

        sentHex = []
        if 'std_port_tagging_settings' in pktProcDict:
            if pktProcDict['std_port_tagging_settings']['enabled']:

                print('Adding %s VLAN tag %s to sent frames...' % (self.myParent.name, pktProcDict['std_port_tagging_settings']['vlan_id']))
                for pkt in decodedFrames:

                    if verbose:
                        print('\nBefore:\n')
                        pkt.show()

                    #Create a 802.1Q Vlan layer.
                    dot1q = Dot1Q(vlan=pktProcDict['std_port_tagging_settings']['vlan_id'])

                    #Add Vlan if first layer is Ether.
                    if type(pkt[0]) is Ether:
                        dot1q.add_payload(pkt[0].payload)
                        pkt[0].remove_payload()
                        pkt[0].add_payload(dot1q)

                    #Add Vlan if second layer is Ether (e.g. CFP frame)
                    elif type(pkt[1]) is Ether:
                        dot1q.add_payload(pkt[1].payload)
                        pkt[1].remove_payload()
                        pkt[1].add_payload(dot1q)

                    else:
                        raise Exception('ERROR: Unable to find ethernet header in first two layers of the frame!')

                    # Force scapy recompute on L2 type field (i.e. adding VLAN to Ether()/IP()/...
                    del (pkt[Ether].type)

                    if verbose:
                        print('\nAfter:\n')
                        pkt.show()
                        print()

                    #Save modified packet to sent list.
                    sentHex.append(bytes(pkt))

        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def std_strip_by_vlan_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate Std Strip by vlan  on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'std_strip_by_vlan_settings' in pktProcDict:
            if pktProcDict['std_strip_by_vlan_settings']['enabled']:
                for pkt in decodedFrames:
                    if pkt.haslayer(Dot1Q):
                        numVlansToStrip = 1

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()

                        #Iterate over the layers.
                        layer = pkt.firstlayer()
                        count = 0
                        while not isinstance(layer, NoPayload):
                            if (type(layer) is Dot1Q):
                                #stip only if the outer Dot1Q contains vlan_id specified, otherwise exit the loop
                                if (layer[Dot1Q].vlan == pktProcDict['std_strip_by_vlan_settings']['vlan_id']):
                                    # add 802.1q layer between Ether and IP
                                    stripPayload=layer.payload

                                    del pkt[count]
                                    pkt.add_payload(stripPayload)
                                break

                            # advance to the next layer
                            layer = layer.payload
                            count += 1

                            # Force scapy recompute on L2 type field (i.e. stripping Dot1Q and leaving IP).
                            del (pkt[Ether].type)

                        if verbose:
                            print('\nAfter:\n')
                            pkt.show()
                            print()

                    #Save packet to sent list.
                    sentHex.append(bytes(pkt))


        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def l2gre_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate L2GRE Stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'l2gre_strip_settings' in pktProcDict:
            if pktProcDict['l2gre_strip_settings']['enabled']:
                for pkt in decodedFrames:
                    if pkt.haslayer(GRE):

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()

                        # Get layers above GRE.
                        pkt = pkt.getlayer(GRE).payload

                        if verbose:
                            print('\nAfter:\n')
                            pkt.show()
                            print()

                    #Save packet to sent list.
                    sentHex.append(bytes(pkt))

        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def gtp_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate GTP Stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'gtp_strip_settings' in pktProcDict:
            if pktProcDict['gtp_strip_settings']['enabled']:
                for pkt in decodedFrames:
                    #Strip only if GTP-U (not for GTP-C)
                    if pkt.haslayer(GTPHeader) and pkt.getlayer(UDP).dport != 2123:

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()

                        #Get layers above GTPHeader.
                        stripped = pkt.getlayer(GTPHeader).payload

                        # Iterate over the layers.
                        layer = pkt.firstlayer()
                        count = 0
                        while not isinstance(layer, NoPayload):
                            if (type(layer) is IP) or (type(layer) is IPv6):
                                del pkt[count]
                                pkt.add_payload(stripped)
                                break

                            # advance to the next layer
                            layer = layer.payload
                            count += 1

                        if verbose:
                            print('\nAfter:\n')
                            pkt.show()
                            print()

                    #Save packet to sent list.
                    sentHex.append(bytes(pkt))

        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def vxlan_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate vxlan stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'vxlan_strip_settings' in pktProcDict:
            if pktProcDict['vxlan_strip_settings']['enabled']:
                for pkt in decodedFrames:

                    if pkt.haslayer(VXLAN):

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()

                        # Get layers above VXLAN.
                        pkt = pkt.getlayer(VXLAN).payload

                        if verbose:
                            print('\nAfter:\n')
                            pkt.show()
                            print()

                    #Save packet to sent list.
                    sentHex.append(bytes(pkt))

        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def erspan_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate erspan stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""

        sentHex = []
        if 'erspan_strip_settings' in pktProcDict:
            if pktProcDict['erspan_strip_settings']['enabled']:
                for pkt in decodedFrames:
                    if pkt.haslayer(ERSPAN):

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()

                        # Get layers above ERSPAN.
                        pkt = pkt.getlayer(ERSPAN).payload

                        if verbose:
                            print('\nAfter:\n')
                            pkt.show()
                            print()

                    #Save packet to sent list.
                    sentHex.append(bytes(pkt))

        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def etag_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate etag stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'etag_strip_settings' in pktProcDict:
            if pktProcDict['etag_strip_settings']['enabled']:
                for pkt in decodedFrames:
                    if pkt.haslayer(ETag):

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()

                        # Get layers above ETag.
                        stripped = pkt.getlayer(ETag).payload

                        # Iterate over the layers.
                        layer = pkt.firstlayer()
                        count = 0
                        while not isinstance(layer, NoPayload):

                            if (type(layer) is ETag):
                                del pkt[count]
                                pkt.add_payload(stripped)
                                break

                            # advance to the next layer
                            layer = layer.payload
                            count += 1

                        # Force scapy recompute on L2 type field.
                        del (pkt[Ether].type)

                        if verbose:
                            print('\nAfter:\n')
                            pkt.show()
                            print()

                    #Save packet to sent list.
                    sentHex.append(bytes(pkt))

        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def vntag_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate vntag stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'vntag_strip_settings' in pktProcDict:
            if pktProcDict['vntag_strip_settings']['enabled']:
                for pkt in decodedFrames:
                    if pkt.haslayer(VNTag):

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()

                        # Get layers above VNTag.
                        stripped = pkt.getlayer(VNTag).payload

                        # Iterate over the layers.
                        layer = pkt.firstlayer()
                        count = 0
                        while not isinstance(layer, NoPayload):

                            if (type(layer) is VNTag):
                                del pkt[count]
                                pkt.add_payload(stripped)
                                break

                            # advance to the next layer
                            layer = layer.payload
                            count += 1

                        # Force scapy recompute on L2 type field.
                        del (pkt[Ether].type)

                        if verbose:
                            print('\nAfter:\n')
                            pkt.show()
                            print()

                    #Save packet to sent list.
                    sentHex.append(bytes(pkt))


        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def lisp_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate lisp stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'lisp_strip_settings' in pktProcDict:
            if pktProcDict['lisp_strip_settings']['enabled']:
                for pkt in decodedFrames:
                    if pkt.haslayer(LISP_Data):

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()

                        # Get LISP_Data payload.
                        stripped = pkt.getlayer(LISP_Data).payload

                        # Delete everything above L2.
                        del pkt[1]
                        pkt.add_payload(stripped)

                        # Force scapy recompute on L2 type field.
                        del (pkt[Ether].type)

                        if verbose:
                            print('\nAfter:\n')
                            pkt.show()
                            print()

                    #Save packet to sent list.
                    sentHex.append(bytes(pkt))

        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)

    def trim_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate trimming on a list of Scapy decoded Pcap or packets

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets : trimmed packets in hex format"""
        sentHex = []

        if pktProcDict['trim_settings']['enabled']:
            if pktProcDict['trim_settings']['retained_headers'].upper() == 'MAC':  keepHdrs = [Ether]
            if pktProcDict['trim_settings']['retained_headers'].upper() == 'MAC_VLAN':  keepHdrs = [Ether, Dot1Q]
            if pktProcDict['trim_settings']['retained_headers'].upper() == 'MAC_VLAN_MPLS':  keepHdrs = [Ether, Dot1Q, MPLS]
            if pktProcDict['trim_settings']['retained_headers'].upper() == 'MAC_VLAN_MPLS_L3':  keepHdrs = [Ether, Dot1Q, MPLS, IP, IPv6]

            for pkt in decodedFrames:

                if verbose:
                    print('\nBefore:\n')
                    pkt.show()

                # Look for any keep headers inside pkt.
                if next((protocol for protocol in keepHdrs if protocol in pkt), None) is not None:

                    # Get entire packet
                    layer = pkt.firstlayer()
                    trim = False
                    countLayers = {'Ethernet': 0, '802.1Q': 0, 'MPLS': 0, 'IP': 0, 'IPv6': 0}
                    while not isinstance(layer, NoPayload):
                        #if more than 1 Ether layer is in the packet, trim the packet after the first Ethernet layer
                        if type(layer) is Ether and countLayers[layer.name] >= 1:
                            trim = True
                        # if more than 2 vlans are in the packet, trim the packet after 2 vlans
                        if type(layer) is Dot1Q and countLayers[layer.name] >= 2:
                            trim = True
                        # if more than 8 mpls layers are in the packet, trim the packet after 8 mpls layers
                        if type(layer) is MPLS and countLayers[layer.name] >= 8:
                            trim = True
                        # if more than 1 layer3 is in the packet,trim the packet after the first layer3: even ipv4 or ipv6
                        if (type(layer) is IP or type(layer) is IPv6) and (countLayers['IP'] >= 1 or countLayers['IPv6'] >= 1):
                            trim = True
                        #if the layer is not in keep headers, trim the packet
                        if type(layer) not in keepHdrs:
                            trim = True
                        #trim the packet
                        if trim is True:
                            # Get layer's length.
                            payloadLength = len(layer)
                            # Trim the packet
                            # if the number of configured bytes is greater than the length of the packet, the entire packet is kept
                            # if the packet remained after keeping the configured headers from retained_headers +retained bytes > packet length; the entire packet is kept
                            if pktProcDict['trim_settings']['retained_bytes'] >= payloadLength:
                                #trimmed = bytes(decodedFrames[index])
                                trimmed  = bytes(pkt)
                            else:
                                # to create trimmed packet, remove (retained_bytes-the number of bytes of the layers above keepHdrs) from the original pkt;
                                trimmed = bytes(pkt)[:(pktProcDict['trim_settings']['retained_bytes'] - payloadLength)]


                            # NTO pads stripped packets with zeros out to 60 bytes
                            if len(trimmed) < 60:
                                trimmed = trimmed + (b'\x00' * (60 - len(trimmed)))
                            if verbose:
                                print('\nAfter:\n')
                                scapyTrimmedPacket = self.scapyDecodeBytes(trimmed)
                                scapyTrimmedPacket.show()
                                # hexdump(trimmed)

                            sentHex.append(trimmed)
                            break
                        countLayers[layer.name] += 1
                        layer = layer.payload
                else:
                    # Save unmodified packet to sent list if a packet is not trimmed: ex: it is a CPF packet, the packet remains the same
                    sentHex.append(bytes(pkt))
        else:
            # if pktProcDict contains TRIM_SETTINGS, but enabled=False, sentHex will return the initial pkt
            for pkt in decodedFrames:
                sentHex.append(bytes(pkt))

        return (sentHex)

    def data_masking_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate data masking on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex = []
        if 'data_masking_settings' in pktProcDict:
            if pktProcDict['data_masking_settings']['enabled']:
                for pkt in decodedFrames:
                    fillVal = pktProcDict['data_masking_settings']['fill_value']
                    fillLen = pktProcDict['data_masking_settings']['length']
                    offLayer = pktProcDict['data_masking_settings']['offset_layer']
                    offVal = pktProcDict['data_masking_settings']['offset_value']

                    if offLayer.upper() == 'L2_START' and (pkt.haslayer(Ether) or pkt.haslayer(CFP)):

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()
                            hexdump(bytes(pkt))

                        maskedRaw = bytes(pkt)[:offVal] + bytes([fillVal] * fillLen) + bytes(pkt)[(offVal + fillLen):]

                        if verbose:
                            print('\nAfter:\n')
                            hexdump(maskedRaw)
                            print()

                        #Save packet to sent list.
                        sentHex.append(maskedRaw)

                    elif offLayer.upper() == 'L2_END' and (pkt.haslayer(Ether) or pkt.haslayer(CFP)):

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()
                            hexdump(bytes(pkt))

                        origPkt = pkt.copy()

                        # Iterate over the layers.
                        layer = pkt.firstlayer()
                        count = 0
                        l2offset = None
                        while not isinstance(layer, NoPayload):

                            if (type(layer) is CFP) or (type(layer) is Ether):
                                del pkt[count].payload

                                #Backup two bytes for CFP since the NTO overwrites 10-bit BitField("ftag") and 6-bit BitField("ttl").
                                if type(layer) is CFP:
                                    l2offset = len(pkt) - 2
                                else:
                                    l2offset = len(pkt)
                                break

                            # advance to the next layer
                            layer = layer.payload
                            count += 1

                        maskedRaw = bytes(origPkt)[:(l2offset + offVal)] + bytes([fillVal] * fillLen) + bytes(origPkt)[(l2offset + offVal + fillLen):]

                        if verbose:
                            print('\nAfter:\n')
                            hexdump(maskedRaw)
                            print()

                        # Save packet to sent list.
                        sentHex.append(maskedRaw)

                    elif offLayer.upper() == 'L3_END' and (pkt.haslayer(CFP) or pkt.haslayer(IP) or pkt.haslayer(IPv6)):

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()
                            hexdump(bytes(pkt))

                        origPkt = pkt.copy()

                        # Iterate over the layers.
                        layer = pkt.firstlayer()
                        count = 0
                        l2offset = None
                        while not isinstance(layer, NoPayload):

                            if (type(layer) is CFP) or (type(layer) is IP) or (type(layer) is IPv6):
                                del pkt[count].payload

                                # Backup two bytes for CFP since the NTO overwrites 10-bit BitField("ftag") and 6-bit BitField("ttl").
                                if type(layer) is CFP:
                                    l2offset = len(pkt) - 2
                                else:
                                    l2offset = len(pkt)
                                break

                            # advance to the next layer
                            layer = layer.payload
                            count += 1

                        maskedRaw = bytes(origPkt)[:(l2offset + offVal)] + bytes([fillVal] * fillLen) + bytes(origPkt)[(l2offset + offVal + fillLen):]

                        if verbose:
                            print('\nAfter:\n')
                            hexdump(maskedRaw)
                            print()

                        # Save packet to sent list.
                        sentHex.append(maskedRaw)

                    else:
                        #Save packet to sent list.
                        sentHex.append(bytes(pkt))

        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))


        return (sentHex)

    def pppoe_strip_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate pppoe stripping on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""
        sentHex=[]
        if 'pppoe_strip_settings' in pktProcDict:
            if pktProcDict['pppoe_strip_settings']['enabled']:
                for pkt in decodedFrames:
                    if pkt.haslayer(PPPoE) and pkt.haslayer(PPP) and (pkt[PPP].proto == 0x0021 or pkt[PPP].proto == 0x0057):

                        if verbose:
                            print('\nBefore:\n')
                            pkt.show()

                        # Get PPPoE payload.
                        stripped = pkt.getlayer(PPP).payload

                        # Delete everything above L2.
                        del pkt[1]
                        pkt.add_payload(stripped)

                        if verbose:
                            print('\nAfter:\n')
                            pkt.show()
                            print()

                    #Save packet to sent list.
                    sentHex.append(bytes(pkt))
        else:
            #Append unaltered packet to sent list.
            sentHex.append(bytes(pkt))

        #pdb.Pdb(stdout=sys.__stdout__).set_trace()
        return (sentHex)



    def tunnel_origination_settings(self, pktProcDict , decodedFrames, verbose=False):
        """ Description: Emulate tunnel origination GRE on a list of Scapy decoded Pcap or packets.

                    Arguments:
                        * pktProcDict - string (json) containing the packet processing definition
                        * decodedFrames - (default=None) Scapy decoded Pcap captured frame format for packet processing emulation.
                        * verbose - (default=False) To set the verbosity level on displayed original and modified packets

                    Return Value:
                        * sentHex - List of Emulated packets"""

        sentHex = []

        # Tunnel Origination
        if ('tunnel_origination_local_settings' in pktProcDict['tunnel_origination_settings']) and ('tunnel_origination_remote_settings' in pktProcDict['tunnel_origination_settings']) and ('tunnel_mac' in pktProcDict['tunnel_origination_settings']):

            # "tunnel_origination_settings:{
            # "tunnel_mac": {"mac_address": "${localMacAddr}"},
            #    "tunnel_origination_local_settings": {
            #        "default_gateway": "192.168.1.254",
            #        "enabled": true,
            #        "local_ip": "${localIPAddr}",
            #        "subnet_mask": "255.255.255.0"
            #   },
            #    "tunnel_origination_remote_settings": {
            #        "remote_ip": "${remoteIPAddr}",
            #        "remote_mac": "${remoteMacAddr}"
            #    }}
            print('Building GRE encap pkt %s ' % (self.myParent.name))
            print('Outer Ethernet Header w/ DMACAddr %s and SMACAddr %s...'
                  % (pktProcDict['tunnel_origination_settings']['tunnel_origination_remote_settings']['remote_mac_address'],
                 pktProcDict['tunnel_origination_settings']['tunnel_mac']['mac_address']))
            print('Outer IP Header w/ SrcIPAddr %s and DstMACAddr %s...'
                  % (pktProcDict['tunnel_origination_settings']['tunnel_origination_local_settings']['local_ip_address'],
                     pktProcDict['tunnel_origination_settings']['tunnel_origination_remote_settings']['remote_ip_address']))
            print('GRE Header w/ ProtocolType 0x6558...')

            for pkt in decodedFrames:

                if verbose:
                    print('\nBefore:\n')
                    pkt.show()

                # Build Outer packet
                outerPkt = Ether(dst=pktProcDict['tunnel_origination_settings']['tunnel_origination_remote_settings']['remote_mac_address'],
                                 src=pktProcDict['tunnel_origination_settings']['tunnel_mac']['mac_address'])\
                           /IP(src=pktProcDict['tunnel_origination_settings']['tunnel_origination_local_settings']['local_ip_address'],
                               dst=pktProcDict['tunnel_origination_settings']['tunnel_origination_remote_settings']['remote_ip_address'])\
                           /GRE(proto=0x6558)

                # Emulated packet containing outer built part and original packet (inner)
                pkt = outerPkt/pkt

                if verbose:
                    print('\nAfter:\n')
                    pkt.show()

                # Save modified packet to sent list.
                sentHex.append(bytes(pkt))

        return(sentHex)


