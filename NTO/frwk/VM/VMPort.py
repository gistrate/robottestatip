
"""
 VM NIC
 Author: OBalas

 Description: This is a syslog server that receives and logs events to log file

 Documentation:
    Site: https://wiki.ixiacom.com/display/NVSSysTest/Python+Automation+Framework+%28PAF%29+-+based+on+Robot+Framework
    Prerequisites:
        - OVA Template with a particular configuration
        OR
        - ESXi: ubuntu x64 machine with a particular configuration


"""


import paramiko
import re
import time


class VMPort:
    """ Description: A library for controlling virtual machines Ubuntu/(Windows?)
    """

    ROBOT_LIBRARY_SCOPE = 'TEST CASE'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self):
        self.myDeviceParrentPort = None # myNTOPort
        self.name = None
        self.VMIPAddr = None
        self.VMUser = None
        self.VMPass = None
        self.port = None # ethernet interface
        self.sshClient = None
        self.txFrames = 0
        self.txBytes = 0

    def setParent(self, parentObj):
        """ Description:  Assigns the Ixia port to NTO port, parentObj

            Arguments:
                * parentObj - the NTO port the Ixia port is being assigned to

            Return Value: n/a """
        self.myDeviceParrentPort = parentObj

    def setName(self, portName):
        """ Description:  names the Ixia port

            Arguments:
                *portName = the name to be assigned to the VM NIC Port/Interface

            Return Value: n/a """
        self.name = portName

    def setVMIPaddr(self, portDict):
        """ Description:

            Arguments: n/a

            Return Value: n/a """
        self.VMIPAddr = portDict['VMIPAddr']

    def setVMUser(self, portDict):
        """ Description:

            Arguments: n/a

            Return Value: n/a """
        self.VMUser = portDict['VMUser']

    def setVMPass(self, portDict):
        """ Description:

            Arguments: n/a

            Return Value: n/a """
        self.VMPass = portDict['VMPass']

    def setPort(self, portDict):
        """ Description:

            Arguments: n/a

            Return Value: n/a """
        self.port = portDict['port']

    def showVMPort(self):
        """ Description: Shows the IP Addresses for the Ixia port, the Ixia Chassis, and the user

            Arguments: n/a

            Return Value: n/a """
        print('VM IP Address = %s' % self.VMIPAddr)
        print('VM User = %s' % self.VMUser)
        print('VM Password = %s' % self.VMPass)
        print('VM Port = %s' % self.port)

    def getTxFrames(self):
        """ Description:

            Arguments: n/a

            Return Value: n/a """
        return self.txFrames

    def getTxBytes(self):
        """ Description:

            Arguments: n/a

            Return Value: n/a """
        return self.txBytes

    def loginToVM(self):
        """ Description: Connect to VM using ssh (linux), port 22"

            Arguments: n/a

            Return Value: n/a """

        self.sshClient = paramiko.SSHClient()
        self.sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            self.sshClient.connect(self.VMIPAddr, username = self.VMUser, password = self.VMPass)
        except Exception as e:
            raise Exception("Unable to connect to VM {} \n Error: {}".format(self.VMIPAddr, e))

    def sshExec(self, inputString):
        """ Description:  Execute Command on sshClient)

            Arguments: input command to send

            Return Value: if returned status is 0 it returns a list of strings, otherwise raise error """

        print("Executing shell command: {}".format(inputString))
        stdin, stdout, stderr = self.sshClient.exec_command(inputString)

        if stdout.channel.recv_exit_status() == 0:
            return stdout.readlines()
        else:
            print("Error Output: ")
            for line in stderr.readlines():
                print(line)
            raise Exception("Received status: ", stdout.channel.recv_exit_status())

    def tcpReplay(self, fileName, rateType="", rateValue=0, loop=1, loopDelay=0):
        """ Description: Execute command of tcpReplay
            Arguments:
                fileName = string pcap file to send
                rateType = string either pps[packets per second] or mbps [megabits per second]
                rateValue = fload efective rate of pps or mbps
                loop = integer total number of times to send the capture file
                loopDelay = integer delay between loops in miliseconds
            Returns:
                Dictionary contains packets, bytes, successful, failed, truncated, retried

            Example (Robot):
                 np1VMPort.tcpReplay  /home/testuser/TradeVision_Pcap_folder/NYSE-Arca.pcap  pps  10
                 ${stream}=  np1VMPort.tcpReplay  ${streamFileName}  rateType=pps  rateValue=2000  loop=2   loopDelay=1000
            """

        # Get counters to compare with what tcpReplay transmits
        countersBegin = self.getCounters()

        inputString = "sudo tcpreplay -i " + self.port
        if rateType == "pps":
            inputString += " --pps=" + rateValue
        elif rateType == "mbps":
            inputString += " --mbps=" + rateValue
        if loop != 1:
            inputString += " --loop=" + loop + " --loopdelay-ms=" + loopDelay
        #inputString += " -C, --fixcsum " + fileName
        inputString += " " + fileName

        # Define output dictionary with default values
        tcpReplayOutput = {}
        tcpReplayOutput["packets"] = 0
        tcpReplayOutput["bytes"] = 0
        tcpReplayOutput["successful packets"] = 0
        tcpReplayOutput["failed packets"] = 0
        tcpReplayOutput["retried packets"] = 0

        # Search for information about packet in the output
        output = self.sshExec(inputString)
        print("Output: \n")
        for line in output: # list containing the tcpReplay output
            print(line)
            if re.findall("Actual:\s+(\d+).*\((\d+)\s+bytes", line) != []:
                tcpReplayOutput["packets"] = int(re.findall("Actual:\s+(\d+).*\((\d+)\s+bytes", line)[0][0])
                tcpReplayOutput["bytes"] = int(re.findall("Actual:\s+(\d+).*\((\d+)\s+bytes", line)[0][1]) \
                                           + 4 * tcpReplayOutput["packets"] # assuming pcap doesnt have crc
                continue
            if re.findall("\s+Successful packets:\s+(\d+)", line) != []:
                tcpReplayOutput["successful packets"]= int(re.findall("\s+Successful packets:\s+(\d+)", line)[0])
                continue
            if re.findall("\s+Failed packets:\s+(\d+)", line) != []:
                tcpReplayOutput["failed packets"]= int(re.findall("\s+Failed packets:\s+(\d+)", line)[0])
                continue
            if re.findall("Truncated packets: (\d+)", line) != []:
                tcpReplayOutput["truncated packets"]= int(re.findall("\s+Truncated packets:\s+(\d+)", line)[0])
                continue
            if re.findall("\s+Retried packets (ENOBUFS):\s+(\d+)", line) != []:
                tcpReplayOutput["retried packets"]+= int(re.findall("\s+Retried packets (ENOBUFS):\s+(\d+)", line)[0])
                continue
            if re.findall("\s+Retried packets (EAGAIN):\s+(\d+)", line) != []:
                tcpReplayOutput["retried packets"]+= int(re.findall("Retried packets (EAGAIN):\s+(\d+)", line)[0])
        # Set class variable with the values reported by tcpReplay
        self.txFrames = tcpReplayOutput["packets"]
        self.txBytes = tcpReplayOutput["bytes"]  #assuming .pcap frame doesn't contain FCS

        # Check if the counters on the interface matches the tcpReplay output w.r.t to packets in a loop of 300 seconds
        retry = 300
        while retry > 0:
            retry -= 1
            countersEnd = self.getCounters()
            if self.txFrames == countersEnd["pkts tx"] - countersBegin["pkts tx"]:
                time.sleep(1) # still suspect an issue with nto port receiving all pkts
                return tcpReplayOutput
            else:
                time.sleep(1)
        raise Exception("VMPort does not report the same tx packets as the tcpReplay output: \n \
        Port tx pkts on interface: {} \n  \
        tcpReplay tx packets: {} \n  \
        Port stats at the begining: {} \n  \
        Port stats at the end: {} \n  \
        TcpReplayOutput: {} ".format(countersEnd["pkts tx"] - countersBegin["pkts tx"], self.txFrames, countersBegin, countersEnd, tcpReplayOutput))

    def getCounters(self):
        """ Description: Returns dictionary containing counters for frames and bytes, total and per packet type
                    It works only with one CPU set on VM
            Arguments:

            Returns:
                Dictionary contains packets, bytes, total and per type: ucast, mcast, bcast
                {'ucast bytes tx': 990725, 'bcast bytes rx': 31702996, 'ucast pkts tx': 10837,
                'ucast pkts rx': 52632, 'bytes rx': 197486468, 'pkts tx': 394972936, 'bcast bytes tx': 704,
                'mcast pkts rx': 2827, 'bcast pkts rx': 459632, 'bytes tx': 789945872, 'pkts rx': 98743234,
                'mcast bytes tx': 11767, 'bcast pkts tx': 14, 'ucast bytes rx': 65197735,
                'mcast bytes rx': 313242, 'mcast pkts tx': 123}

            """
        # Get counters from the interface using ethtool -S and process in order to return a dictionary
        allCountersListOfStrings = self.sshExec("ethtool -S " + self.port)
        allCountersString = (''.join(allCountersListOfStrings)).replace('\n', '')
        regExp = r'([u|m|b]cast\s+[bytes|pkts]+\s+[t|rx]+):\s+(\d+)'
        allStatsListTuples = re.findall(regExp, allCountersString)
        allStatsDict = dict((statName, int(statValue)) for statName, statValue in allStatsListTuples)
        # Create keys with total number of pkts and bytes per tx and rx
        allStatsDict["pkts rx"] = allStatsDict["ucast pkts rx"] + allStatsDict["mcast pkts rx"] + allStatsDict["bcast pkts rx"]
        allStatsDict["bytes rx"] = allStatsDict["ucast bytes rx"] + allStatsDict["mcast bytes rx"] + allStatsDict["bcast bytes rx"]
        allStatsDict["pkts tx"] = allStatsDict["ucast pkts tx"] + allStatsDict["mcast pkts tx"] + allStatsDict["bcast pkts tx"]
        allStatsDict["bytes tx"] = allStatsDict["ucast bytes tx"] + allStatsDict["mcast bytes tx"] + allStatsDict["bcast bytes tx"]
        print(allStatsDict)
        return allStatsDict

# def main():
#     vmp1 = VMPort()
#     portDef = {"VMIPAddr": "10.215.157.184", "VMUser": "root", "VMPass":"root", "port":"ens160" }
#     vmp1.setPort(portDef)
#     vmp1.setVMIPaddr(portDef)
#     vmp1.setVMUser(portDef)
#     vmp1.setVMPass(portDef)
#     vmp1.loginToVM()
#     vmp1.getCounters()

#if __name__ == "__main__":
    #main()

