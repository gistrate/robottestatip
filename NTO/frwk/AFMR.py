import json
from NTO.frwk.NTOPort import NTOPort
from NTO.frwk.NTOPortGroup import NTOPortGroup
from NTO.frwk.NTOFilter import NTOFilter


class AFMR:

    """A library for *documentation format* demonstration purposes.

    This documentation is created using reStructuredText__. Here is a link
    to the only \`Keyword\`.

    __ http://docutils.sourceforge.net
    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self):
        self.myNTO=None
        self.afmrName = None
        self.afmrDefaultName = None
        self.afmrId = None
        self.attachedPorts = []
        self.attachedFilters = []
        self.attachedPortGroups = []

    def reinit(self):
        self.__init__()

    def setParent(self, parentObj):
        self.myNTO = parentObj

    def setAFMResourceName(self, afmrName):
        self.afmrName = afmrName

    def addAttachedObj(self, myObj):
        if isinstance(myObj, NTOPort):
            self.attachedPorts.append(myObj)
        elif isinstance(myObj, NTOFilter):
            self.attachedFilters.append(myObj)
        elif isinstance(myObj, NTOPortGroup):
            self.attachedPortGroups.append(myObj)


    def findByDefaultName(self, afmrDefaultName):
        print('Issue ReST post request to search for AFMR with default name %s ...' % afmrDefaultName)
        payload = {'name': afmrDefaultName}
        r = self.myNTO.sess.post('https://%s:%s/%s/search' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/recirculated_afm_resources']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: An error occurred trying to locate AFM resource %s in NTO chassis (%s) [status code = %s]' % (afmrDefaultName, self.myNTO.ipaddr, r.status_code))

        #Check for empty list (e.g. AFMR defaultName not found).
        if len(json.loads(r.text)) != 0:
            self.afmrDefaultName = afmrDefaultName
            self.afmrId = json.loads(r.text)[0]['id']
        else:
            raise Exception('ERROR: Unable to locate AFM resource %s in NTO chassis (%s) [status code = %s]' % (afmrDefaultName, self.myNTO.ipaddr, r.status_code))
        return (r.text)

    def setAFMResourceId(self):
        self.afmrId = self.getAFMResourceInfo(self.afmrDefaultName, 'id')

        # Check for valid AFMR Id.
        if self.afmrId is None: raise Exception('ERROR: Unable to locate the actual id for AFMResource (%s) on NTO (%s)' % (self.afmrDefaultName, self.myNTO.ipaddr))


    def getAFMResourceInfo(self, afmrIdentifier=None, property=None):
        if afmrIdentifier is None: afmrIdentifier = self.afmrDefaultName
        print('Issue ReST request to get AFM Resource info for %s...' % afmrIdentifier)
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/recirculated_afm_resources'], afmrIdentifier), verify=False)
        if property is None:
            return(json.loads(r.text))
        else:
            return(json.loads(r.text)[property])

    def getLaneConfig(self):
        print('Issue ReST get request to attach AFM resource ...')
        r = self.myNTO.sess.get('https://%s:%s/recirculated_afm_resources/%s?properties=lane_config_list' % (self.myNTO.ipaddr, self.myNTO.ipport, self.afmrId), verify=False)
        return (r.text)

    def attachResource(self, myObj, allocBW=None):
        if allocBW is not None:  allocBW = allocBW.rstrip('G')

        #Check the AFMR status for READY before attempting to attach.
        AFMRstatus = self.getAFMResourceInfo(property='resource_status')
        if AFMRstatus == 'READY':
                myObj.attachAFMResource(self, allocBW)
        else:
            raise Exception('ERROR: Unable to attach AFMR (%s) because it is not in READY state [status = %s]' % (self.afmrId, AFMRstatus))

    def detachResource(self, myObj):
        print('Issue ReST put request to detach AFM resource from NTO object ...')
        myObj.detachResource(self)

