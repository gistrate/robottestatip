*** Keywords ***
CreateNTOInstance
    [Arguments]  ${ntoName}
    import library  NTO.frwk.NTO  WITH NAME  ${ntoName}
    ${myClassInstance}=  Get Library Instance  ${ntoName}
    Set Suite Variable  ${${ntoName}}  ${myClassInstance}

CreateNTOStatsInstance
    [Arguments]  ${ntoName}
    import library  NTO.frwk.NTOStats  WITH NAME  ${ntoName}.stats
    ${myClassInstance}=  Get Library Instance  ${ntoName}.stats
    Set Suite Variable  ${${ntoName}Stats}  ${myClassInstance}

CreateNTOStatsPktProcInstance
    [Arguments]  ${ntoName}
    import library  NTO.frwk.NTOStatsPktProc  WITH NAME  ${ntoName}.stats
    ${myClassInstance}=  Get Library Instance  ${ntoName}.stats
    Set Suite Variable  ${${ntoName}Stats}  ${myClassInstance}

AddNTOPktProc
    [Arguments]  ${ntoName}  ${myResource}  ${ipport}=8000
    ${retVal}=  Run Keyword and Ignore Error  Variable Should Not Exist  ${${ntoName}}  msg=None
    ${noExist}=  Get From List  ${retVal}  0
    Run Keyword If  '${noExist}' == 'PASS'  Run Keywords
    ...  CreateNTOInstance  ${ntoName}
    ...  AND  CreateNTOStatsPktProcInstance  ${ntoName}
    ...  AND  Run Keyword  ${ntoName}.addStatsToNtoObj  ${${ntoName}Stats}
    ...  ELSE  Run Keywords
    ...  Run Keyword  ${ntoName}.reinit
    ...  AND  Run Keyword  ${ntoName}.addStatsToNtoObj  ${${ntoName}Stats}
    ...  AND  Run Keyword  ${ntoName}.stats.reinit
    Run Keyword  ${ntoName}.setNTOName  ${ntoName}
    Run Keyword  ${ntoName}.setResource  NTO  ${myResource}
    Run Keyword  ${ntoName}.setIPPort  ${ipport}
    Run Keyword  ${ntoName}.setTimeStampSource
    Run Keyword  ${ntoName}.setSystemModel
    Run_Keyword  ${ntoName}.setSystemSWVersion
    Run_Keyword  ${ntoName}.setMaxFrameSize
    Run Keyword  ${ntoName}.stats.setParent  ${${ntoName}}

AddNTO
    [Arguments]  ${ntoName}  ${ipaddr}  ${ipport}=8000
    ${retVal}=  Run Keyword and Ignore Error  Variable Should Not Exist  ${${ntoName}}  msg=None
    ${noExist}=  Get From List  ${retVal}  0
    Run Keyword If  '${noExist}' == 'PASS'  Run Keywords
    ...  CreateNTOInstance  ${ntoName}
    ...  AND  CreateNTOStatsInstance  ${ntoName}
    ...  AND  Run Keyword  ${ntoName}.addStatsToNtoObj  ${${ntoName}Stats}
    ...  ELSE  Run Keywords
    ...  Run Keyword  ${ntoName}.reinit
    ...  AND  Run Keyword  ${ntoName}.addStatsToNtoObj  ${${ntoName}Stats}
    ...  AND  Run Keyword  ${ntoName}.stats.reinit
    Run Keyword  ${ntoName}.setNTOName  ${ntoName}
    Run Keyword  ${ntoName}.setIPAddr  ${ipaddr}
    Run Keyword  ${ntoName}.setIPPort  ${ipport}
    Run Keyword  ${ntoName}.setTimeStampSource
    Run Keyword  ${ntoName}.setSystemModel
    Run_Keyword  ${ntoName}.setSystemSWVersion
    Run_Keyword  ${ntoName}.setMaxFrameSize
    Run Keyword  ${ntoName}.stats.setParent  ${${ntoName}}

CreateAFMRInstance
    [Arguments]  ${parentObj}  ${afmrName}
    import library  NTO.frwk.AFMR  WITH NAME  ${afmrName}
    ${myClassInstance}=  Get Library Instance  ${afmrName}
    Set Suite Variable  ${${afmrName}}  ${myClassInstance}

AddAFMR
    [Arguments]  ${parentObj}  ${afmrName}  ${afmrDefaultName}
    ${retVal}=  Run Keyword and Ignore Error  Variable Should Not Exist  ${${afmrName}}  msg=None
    ${noExist}=  Get From List  ${retVal}  0
    Run Keyword If  '${noExist}' == 'PASS'  CreateAFMRInstance  ${parentObj}  ${afmrName}
    ...  ELSE  Run Keyword  ${afmrName}.reinit
    Run Keyword  ${afmrName}.setParent  ${parentObj}
    Run Keyword  ${afmrName}.setAFMResourceName  ${afmrName}
    Run Keyword  ${afmrName}.findByDefaultName  ${afmrDefaultName}
    Run Keyword  ${afmrName}.setAFMResourceId
    Run Keyword  ${parentObj.name}.addAFMRToNtoObj  ${${afmrName}}

CreateATIPInstance
    [Arguments]  ${parentObj}  ${atipName}
    import library  NTO.frwk.ATIP  WITH NAME  ${atipName}
    ${myClassInstance}=  Get Library Instance  ${atipName}
    Set Suite Variable  ${${atipName}}  ${myClassInstance}

AddATIP
    [Arguments]  ${parentObj}  ${atipName}  ${atipDefaultName}
    ${retVal}=  Run Keyword and Ignore Error  Variable Should Not Exist  ${${atipName}}  msg=None
    ${noExist}=  Get From List  ${retVal}  0
    Run Keyword If  '${noExist}' == 'PASS'  CreateATIPInstance  ${parentObj}  ${atipName}
    ...  ELSE  Run Keyword  ${atipName}.reinit
    Run Keyword  ${atipName}.setParent  ${parentObj}
    Run Keyword  ${atipName}.setATIPResourceName  ${atipName}
    Run Keyword  ${atipName}.findByDefaultName  ${atipDefaultName}
    Run Keyword  ${atipName}.setATIPResourceId
    Run Keyword  ${parentObj.name}.addATIPToNtoObj  ${${atipName}}

CreatePortInstance
    [Arguments]  ${parentObj}  ${name}
    import library  NTO.frwk.NTOPort  WITH NAME  ${name}
    ${myClassInstance}=  Get Library Instance  ${name}
    Set Suite Variable  ${${name}}  ${myClassInstance}

CreatePktProcInstance
    [Arguments]  ${parentName}
    import library  NTO.frwk.PktProc  WITH NAME  ${parentName}.pktProc
    ${myClassInstance}=  Get Library Instance  ${parentName}.pktProc
    Set Suite Variable  ${${parentName}PktProc}  ${myClassInstance}

AddPort
    [Arguments]  ${parentObj}  ${name}  ${mode}  ${portDict}
    ${retVal}=  Run Keyword and Ignore Error  Variable Should Not Exist  ${${name}}  msg=None
    ${noExist}=  Get From List  ${retVal}  0
    Run Keyword If  '${noExist}' == 'PASS'  Run Keywords
    ...  CreatePortInstance  ${parentObj}  ${name}
    ...  AND  CreatePktProcInstance  ${name}
    ...  ELSE  Run Keywords
    ...  Run Keyword  ${name}.reinit
    ...  AND  Run Keyword  ${name}.pktProc.reinit

    Run Keyword  ${name}.setParent  ${parentObj}
    Run Keyword  ${name}.setPortName  ${name}
    Run Keyword  ${name}.setPortDefaultName  ${portDict['${name}']}
    Run Keyword  ${name}.setPortId  ${portDict['${name}']}
    Run Keyword  ${name}.setPortMode  ${mode}
    Run Keyword  ${name}.setConnType  ${portDict['${name}']}
    Run Keyword  ${name}.setMediaType  ${portDict['${name}']}
    Run Keyword  ${name}.setCardType  ${portDict['${name}']}
    Run Keyword  ${name}.setLicenseType  ${portDict['${name}']}
    Run Keyword  ${name}.pktProc.setParent  ${${name}}
    Run Keyword  ${parentObj.name}.addPortToNtoObj  ${${name}}
    Run Keyword  ${name}.addPktProcToPortObj  ${${name}PktProc}

    Run Keyword If  '${${name}.connType}' == 'standard'  Run Keywords
    \  ...  AddIxiaPort  ${${name}}  ${portDict['${name}']['lockAlso']}
    \  ...  AND  Set Variable  ${${name}Ixia}

CreateVirtPortInstance
    [Arguments]  ${parentObj}  ${vpName}
    import library  NTO.frwk.NTOVirtPort  WITH NAME  ${vpName}
    ${myClassInstance}=  Get Library Instance  ${vpName}
    Set Suite Variable  ${${vpName}}  ${myClassInstance}

AddVirtPort
    [Arguments]  ${parentObj}  ${vpName}  ${tunnelOrigLocal}  ${greOrigRemoteIP}
    ${retVal}=  Run Keyword and Ignore Error  Variable Should Not Exist  ${${vpName}}  msg=None
    ${noExist}=  Get From List  ${retVal}  0
    Run Keyword If  '${noExist}' == 'PASS'  CreateVirtPortInstance  ${parentObj}  ${vpName}
    ...  ELSE  Run Keyword  ${vpName}.reinit
    Run Keyword  ${vpName}.setParent  ${parentObj}
    Run Keyword  ${vpName}.setVirtPortName  ${vpName}
    Run Keyword  ${vpName}.setTunnelOrigLocal  ${tunnelOrigLocal}
    Run Keyword  ${vpName}.setTunnelOrigRemoteIP  ${greOrigRemoteIP}
    Run Keyword  ${parentObj.myNTO.name}.addVirtPortToNtoObj  ${${vpName}}

CreatePortGrpInstance
    [Arguments]  ${parentObj}  ${pgName}
    import library  NTO.frwk.NTOPortGroup  WITH NAME  ${pgName}
    ${myClassInstance}=  Get Library Instance  ${pgName}
    Set Suite Variable  ${${pgName}}  ${myClassInstance}

AddPortGrp
    [Arguments]  ${parentObj}  ${pgName}  ${pgDef}  ${portList}
    ${retVal}=  Run Keyword and Ignore Error  Variable Should Not Exist  ${${pgName}}  msg=None
    ${noExist}=  Get From List  ${retVal}  0
    Run Keyword If  '${noExist}' == 'PASS'  CreatePortGrpInstance  ${parentObj}  ${pgName}
    ...  ELSE  Run Keyword  ${pgName}.reinit
    Run Keyword  ${pgName}.setParent  ${parentObj}
    Run Keyword  ${pgName}.setPortGrpName  ${pgName}
    Run Keyword  ${pgName}.setPortGrpDef  ${pgDef}
    Run Keyword  ${pgName}.setPortGrpPorts  ${portList}
    Run Keyword  ${parentObj.name}.addPortGrpToNtoObj  ${${pgName}}

CreateIxiaPortInstance
    [Arguments]  ${parentObj}
    import library  ixScapy_module.ixScapy  WITH NAME  ${parentObj.name}Ixia
    ${myClassInstance}=  Get Library Instance  ${parentObj.name}Ixia
    Set Suite Variable  ${${parentObj.name}Ixia}  ${myClassInstance}

AddIxiaPort
    [Arguments]  ${parentObj}  ${portDict}
    ${retVal}=  Run Keyword and Ignore Error  Variable Should Not Exist  ${${parentObj.name}Ixia}  msg=None
    ${noExist}=  Get From List  ${retVal}  0
    Run Keyword If  '${noExist}' == 'PASS'  CreateIxiaPortInstance  ${parentObj}
    ...  ELSE  Run Keyword  ${parentObj.name}Ixia.reinit
    Run Keyword  ${parentObj.name}Ixia.setParent  ${parentObj}
    Run Keyword  ${parentObj.name}Ixia.setName  ${parentObj.name}Ixia
    Run Keyword  ${parentObj.name}Ixia.setIxIPaddr  ${ixChassisDict}  ${portDict}
    Run Keyword  ${parentObj.name}Ixia.setIxPort  ${portDict}
    Run Keyword  ${parentObj.name}Ixia.setIxUser
    Run Keyword  ${parentObj.name}Ixia.setPhyMode  ${portDict}
    Run Keyword  ${parentObj.name}.addIxPortToNtoPortObj  ${${parentObj.name}Ixia}

CreateFilterInstance
    [Arguments]  ${parentObj}  ${filterName}
    import library  NTO.frwk.NTOFilter  WITH NAME  ${filterName}
    ${myClassInstance}=  Get Library Instance  ${filterName}
    Set Suite Variable  ${${filterName}}  ${myClassInstance}

AddFilter
    [Arguments]  ${parentObj}  ${filterName}  ${filterMode}  ${filterDef}=None
    ${retVal}=  Run Keyword and Ignore Error  Variable Should Not Exist  ${${filterName}}  msg=None
    ${noExist}=  Get From List  ${retVal}  0
    Run Keyword If  '${noExist}' == 'PASS'  Run Keywords
    ...  CreateFilterInstance  ${parentObj}  ${filterName}
    ...  AND  CreatePktProcInstance  ${filterName}
    #...  AND  Run Keyword  ${filterName}.addPktProcToFilterObj  ${${filterName}PktProc}
    ...  ELSE  Run Keywords
    ...  Run Keyword  ${filterName}.reinit
    ...  AND  Run Keyword  ${filterName}.pktProc.reinit
    Run Keyword  ${filterName}.setFilterName  ${filterName}
    Run Keyword  ${filterName}.setParent  ${parentObj}
    Run Keyword  ${filterName}.setFilterMode  ${filterMode}
    Run Keyword  ${filterName}.setFilterDef  ${filterDef}
    Run Keyword  ${filterName}.pktProc.setParent  ${${filterName}}
    Run Keyword  ${parentObj.name}.addFilterToNtoObj  ${${filterName}}
    Run Keyword  ${filterName}.addPktProcToFilterObj  ${${filterName}PktProc}

CreateConnInstance
    [Arguments]  ${parentObj}  ${connName}
    import library  NTO.frwk.NTOConn  WITH NAME  ${connName}
    ${myClassInstance}=  Get Library Instance  ${connName}
    Set Suite Variable  ${${connName}}  ${myClassInstance}

AddConn
    [Arguments]  ${parentObj}  ${connName}  ${netPort}=None  ${dynFilter}=None  ${toolPort}=None
    ${retVal}=  Run Keyword and Ignore Error  Variable Should Not Exist  ${${connName}}  msg=None
    ${noExist}=  Get From List  ${retVal}  0
    Run Keyword If  '${noExist}' == 'PASS'  CreateConnInstance  ${parentObj}  ${connName}
    ...  ELSE  Run Keyword  ${connName}.reinit
    Run Keyword  ${connName}.setConnName  ${connName}
    Run Keyword  ${connName}.setParent  ${parentObj}
    Run Keyword  ${connName}.setNetworkPort  ${netPort}
    Run Keyword  ${connName}.setDynamicFilter  ${dynFilter}
    Run Keyword  ${connName}.setToolPort  ${toolPort}
    Run Keyword  ${parentObj.name}.addConnToNtoObj  ${${connName}}

CleanupNVSDevices
    [Arguments]  ${myNTO}

    Run Keyword  ${myNTO.name}.delAllPortSrcFilters
    Run Keyword  ${myNTO.name}.delAllPortDestFilters
    Run Keyword  ${myNTO.name}.setAllPortsToDefaults
    Run Keyword  ${myNTO.name}.removeWarningFromSystemBanner

ChkNPForErrors
    [Arguments]  ${myNPstats}

    Run Keyword And Continue On Failure  Should Be Equal As Integers  &{myNPstats}[NP_TOTAL_RX_COUNT_INVALID_PACKETS]  0
    Run Keyword And Continue On Failure  Should Be Equal As Integers  &{myNPstats}[NP_TOTAL_RX_COUNT_FRAMES_TOO_LONG]  0
    Run Keyword And Continue On Failure  Should Be Equal As Integers  &{myNPstats}[NP_TOTAL_RX_COUNT_FCS_ERRORS]  0
    Run Keyword And Continue On Failure  Should Be Equal As Integers  &{myNPstats}[NP_TOTAL_RX_COUNT_ALIGNMENT_ERRORS]  0
    Run Keyword And Continue On Failure  Should Be Equal As Integers  &{myNPstats}[NP_TOTAL_RX_COUNT_SYMBOL_ERRORS]  0
    Run Keyword And Continue On Failure  Should Be Equal As Integers  &{myNPstats}[NP_TOTAL_RX_COUNT_COLLISIONS]  0
    Run Keyword And Continue On Failure  Should Be Equal As Integers  &{myNPstats}[NP_TOTAL_RX_COUNT_CRC_ALIGNMENT_ERRORS]  0
    Run Keyword And Continue On Failure  Should Be Equal As Integers  &{myNPstats}[NP_TOTAL_RX_COUNT_FRAGMENTS]  0
    Run Keyword And Continue On Failure  Should Be Equal As Integers  &{myNPstats}[NP_TOTAL_RX_COUNT_RUNTS]  0

ChkTPForErrors
    [Arguments]  ${myTPstats}

    Run Keyword And Continue On Failure  Should Be Equal As Integers  &{myTPstats}[TP_TOTAL_DROP_COUNT_PACKETS]  0