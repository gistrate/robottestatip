from robot.parsing.model import TestData
import difflib
from robot.parsing.model import Tags
import json
import logging
from pprint import pprint
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
import threading

class Utils:

    """This is a python utility library for NTO testing within the RobotFramework.

       Parent Objects: n/a

       Child Objects: n/a
    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    @staticmethod
    def getTestCaseList(filePath):
        testCaseList = []
        print ('Using robot parsing model to get test cases from a file...')
        suite = TestData(parent=None, source=filePath)
        for testcase in suite.testcase_table:
            testCaseList.append(testcase.name)
        return testCaseList

    @staticmethod
    def getTestCaseTags(filePath, testCaseName):
        tagList = []
        print('Using robot parsing model to get test cases from a file...')
        suite = TestData(parent=None, source=filePath)
        for testcase in suite.testcase_table:
            if testcase.name == testCaseName:
                for tag in testcase.tags:
                    tagList.append(tag)
        return tagList

    @staticmethod
    def matchTestCaseTag(filePath, testCaseName, tagName):
        tagList = []
        print('Using robot parsing model to get test cases from a file...')
        suite = TestData(parent=None, source=filePath)
        for testcase in suite.testcase_table:
            if testcase.name == testCaseName:
                if tagName in testcase.tags:
                    return(True)
                else:
                    return(False)

    @staticmethod
    def mergeDicts(defDict, newDict):
        new = {}
        new.update(defDict)
        new.update(newDict)
        return new

    @staticmethod
    def createStream(name, pktStr, payload=None, frameSize=None, numFrames=1, udf1=None, udf2=None, udf3=None, udf4=None, udf5=None, udf6=None):
        """ Description: Takes stream variables and the scapy packet definition(string) then combines them into a stream dict.

            Arguments:
                * name - Name of stream
                * pkt - Scapy packet definition string
                * payload - Type of payload option: zeros, ones, altones, rand, incr, ff, or <custom>
                * frameSize - Length in bytes of frame (including 4-byte CRC)
                * numFrames - Number of frames to send
                * udf1 - (default=None) User Defined Field 1 dictionary
                * udf2 - (default=None) User Defined Field 2 dictionary
                * udf3 - (default=None) User Defined Field 3 dictionary
                * udf4 - (default=None) User Defined Field 4 dictionary
                * udf5 - (default=None) User Defined Field 5 dictionary
                * udf6 - (default=None) User Defined Field 6 dictionary

            Return Value: streamDict - stream dictionary """
        streamDict = {'name': name, 'pkt': pktStr, 'payload':payload, 'frameSize':frameSize, 'numFrames':numFrames, 'udfs':[udf1, udf2, udf3, udf4, udf5, udf6]}
        return(streamDict)

    @staticmethod
    def convertEncToPcap(encFile, pcapFile):
        """ Description: Converts a .enc encoded Ixia capture to a wireshark pcap file format.

            Arguments:
                * encFile - filepath for .enc encoded capture file
                * pcapFile - filepath for .pcap encoded file

            Return Value: n/a """
        print('Using tshark -r %s -w %s to convert ".enc" capture file to pcap format...' % (encFile, pcapFile))
        subprocess.check_call('tshark -r %s -w %s' % (encFile, pcapFile), shell=True)


    @staticmethod
    def filterCapture(pcapFile, filter, filterPcapFile):
        """ Description: Filters a capture of unwanted packets. Ex: filterCapture  ${capturePktPcapFile}  '!arp'  ${capturePktPcapFileFiltered}

                    Arguments:
                        * pcapFile - filepath for file to be filtered
                        * filter - tshark/wireshark filter , Ex: eth.type == 0x0800
                        * filterPcapFile - filepath where the filtered capture will be stored
                    Return Value: n/a """

        print('Using tshark -r %s -Y %s -w %s to filter capture...' % (pcapFile, filter, filterPcapFile))
        subprocess.check_call('tshark -r %s -Y %s -w %s' % (pcapFile, filter, filterPcapFile), shell=True)

    @staticmethod
    def compareFrames(sentHexFrames, recdHexFrames, ignoreOffsetAndLen=None, ordered=True):
        """ Description: Compares lists of hex strings for equality (many-to-one mapping [ordered=False], one-to-one mapping [ordered=True]) and fails if an equal match is not found.

                Arguments:
                    * sentHexFrames - list of sent hex strings
                    * recdHexFrames - list of recd hex strings
                    * ignoreOffsetAndLen - (default=None) list of tuples containing offset position and length to ignore. Frame starts with index 0
                    * ordered - (default=True)  If True, it is a one-to-one, ordered comparison between two lists of frames.  If False, an unordered, one-to-many comparison between two lists of frames

                Return Value: n/a """

        #Excluding the offset from comparison
        if ignoreOffsetAndLen is not None:
            ignoreOffsetAndLen = eval("%s" % ignoreOffsetAndLen)
            print("Excluding offset and length from comparison")
            tempRecdHexFrames = recdHexFrames.copy()
            for frameId,frame in enumerate(recdHexFrames):
                for position,length in ignoreOffsetAndLen:
                    print("Overwriting offset position %s and offset length %s from received frame (second argument) to sent (first argument)" % (position, length))
                    print("Replacing frame ", frame[position:(position + length)], " in recdHexFrames with ", sentHexFrames[0][position:(position + length)])
                    frame = frame[:position] + frame[position:(position + length)].replace(frame[position:(position + length)], sentHexFrames[0][position:(position + length)]) + frame[position+length:]
                recdHexFrames[frameId] = frame

        #Perform ordered, one-to-one comparison of frames
        if ordered:
            if len(sentHexFrames) != len(recdHexFrames):
                raise Exception('ERROR:  Both lists of frames must have the same length for ordered comparison!')
            else:
                for i in range(len(sentHexFrames)):
                    if bytes(sentHexFrames[i]) != bytes(recdHexFrames[i]):
                        print('Mismatch detected between sent/processed frame [%s]:'% i)
                        hexdump(sentHexFrames[i])
                        print('And received frame [%s]:' % i)
                        hexdump(recdHexFrames[i])
                        raise Exception('ERROR:  Frame mismatch detected!')
        else:
            #Each recdHexFrames entry should match one of the sentHexFrames entries (i.e. many-to-one mapping).
            deltaList = [frame for frame in recdHexFrames if not frame in sentHexFrames]

            #Using difflib and sequence matchers, Handle the non-matching cases (if any).
            if deltaList:

                s = difflib.SequenceMatcher()

                #Iterate and display deltas with +95% confidence matchers.
                for item in deltaList:

                    print('\nERROR: Received frame does not have an exact match:')
                    hexdump(item)

                    #Assign received frame to seq1.
                    s.set_seq1(item)

                    for sentFrame in sentHexFrames:

                        #Assign sent frame to seq2.
                        s.set_seq2(sentFrame)
                        confRatio=s.ratio()

                        #Only display sent frames and deltas with confidence > 95%.
                        if confRatio >= 0.95 or len(sentHexFrames) == 1:
                            print('\nDebug: Possible sent/processed frame match with %.2f%% confidence:' % (confRatio*100))
                            hexdump(sentFrame)
                            print('\nDeltas:')

                            for tag, i1, i2, j1, j2 in s.get_opcodes():
                                if tag != 'equal': print("    %7s recdFrame[%x:%x]-(%s); sentFrame[%x:%x]-(%s)" % (tag, i1, i2, hexstr(item[i1:i2], onlyhex=1), j1, j2, hexstr(sentFrame[j1:j2], onlyhex=1)))

                raise Exception('ERROR:  One or more received frames do not match any of the sent/processed frames!')

        # Reverting sentHexFrames and recdHexFrames before offset exclusion
        if ignoreOffsetAndLen is not None:
            recdHexFrames = tempRecdHexFrames

    @staticmethod
    def configurePayload(payloadType, payloadLen):
        """ Description: Configures the payload based on type, frameSize, and the current headers overall length.

                Arguments:
                    * payloadType - rand, zeros, ff, ones, altones, incr, or <custom>
                    * paylodLen - length of the payload

                Return Value: configured payload as a list of numbers or chars"""

        payloadLen = int(payloadLen)
        print("\n\n\nAdding %s bytes of payload - %s" % (payloadLen, payloadType))

        if payloadType == 'rand':
            payload = [random.randint(0, 255) for i in range(0, payloadLen)]
        elif payloadType == 'zeros':
            payload = [0 for i in range(0, payloadLen)]
        elif payloadType == 'ff':
            payload = [255 for i in range(0, payloadLen)]
        elif payloadType == 'ones':
            payload = [17 for i in range(0, payloadLen)]
        elif payloadType == 'altones':
            payload = [16 for i in range(0, payloadLen)]
        elif payloadType == 'incr':
            payload = [i % 256 for i in range(0, payloadLen)]
        else:
            payload = list(payloadType)

        return(payload)

    @staticmethod
    def convertStreamtoBytes(stream):
        """ Description: Converts a given scapy format packet into bytes.

                        Arguments:
                            * Stream - a packet that will be converted with scapy into bytes


                        Return Value: Packet in the form of bytes"""
        scapyPktStream = eval(stream)
        print(scapyPktStream)
        bytesStream = bytes(scapyPktStream)
        return bytesStream

    @staticmethod
    def replaceDashWithColon(mac_address):
        """ Description: Converts a given string with - to : [used for MAC Address Format]

            Arguments:
                string - MAC address format 00-11-22-33-44-55

            Return Value: MAC Address format 00:11:22:33:44:55 """

        return ":".join(mac_address.split("-"))

    @staticmethod
    def compareStatsOnNetwork(actualStats,  rxPackets, rxBytes, txPackets, txBytes,pktProcessStats=None):
        failed=False
        expectedStats={
            'np_total_rx_count_valid_packets': rxPackets,
            'np_total_rx_count_invalid_packets':  0,
            'np_total_rx_count_fcs_errors': 0,
            'np_total_rx_count_crc_alignment_errors':  0,
            'np_total_rx_count_fragments': 0,
            'np_total_rx_count_alignment_errors':  0, 
            'np_total_rx_count_symbol_errors':  0,
            'np_total_rx_count_collisions':  0,
            'np_total_rx_count_runts':  0,
            'np_total_rx_count_packets': rxPackets,
            'np_total_rx_count_bytes': rxBytes,
            'np_total_pass_count_packets': txPackets,
            'np_total_pass_count_bytes': txBytes
        }
        pktProcessStatMap={
        'data_masking': 'np_total_data_masking_count_packets',
        'dedup': 'np_total_dedup_count_packets',
        'erspan_strip': 'np_total_erspan_strip_count_packets',
        'etag_strip': 'np_total_etag_strip_count_packets',
        'fabric_path_strip': 'np_total_fabric_path_strip_count_packets',
        'gtp_strip': 'np_total_gtp_strip_count_packets',
        'lwgre_strp': 'np_total_l2gre_strip_count_packets',
        'lisp_strip': 'np_total_lisp_strip_count_packets',
        'mpls_strip': 'np_total_mpls_strip_count_packets',
        'pppoe_strip': 'np_total_pppoe_strip_count_packets',
        'trim_bytes': 'np_total_trim_count_bytes',
        'trim': 'np_total_trim_count_packets',
        'tunnel_term': 'np_total_tunnel_term_count_packets',
        'vntag_strip': 'np_total_vntag_strip_count_packets',
        'vxlan_strip': 'np_total_vxlan_strip_count_packets'
        }
        if pktProcessStats is not None:
            pktProcessStats = eval(pktProcessStats)
            for pktProcessStat in pktProcessStats:
                expectedStats[pktProcessStatMap[pktProcessStat]]=pktProcessStats[pktProcessStat]

        for key1 in expectedStats:
            for key2 in actualStats:
                if key1==key2.lower():
                    if int(expectedStats[key1])!= int(actualStats[key2]):
                        print('%s: expected(%s)!=actual(%s)' % (key1, expectedStats[key1], actualStats[key2]))
                        failed =True
                    else:
                        print('%s: expected(%s)=actual(%s)' % (key1, expectedStats[key1], actualStats[key2]))

        if failed==True:
            raise Exception('Statistics mismatch for network port: check above the mismatched statistics')


    @staticmethod
    def compareStatsOnTool(actualStats, rxPackets, txPackets, txBytes,pktProcessStats=None):
        failed = False

        #'tp_total_pass_count_bytes': noOfBytes,
        expectedStats = {
            'tp_total_pass_count_packets': txPackets,
            'tp_total_tx_count_packets': txPackets,
            'tp_total_tx_count_bytes': txBytes,
            'tp_total_drop_count_packets': 0,
            'tp_total_insp_count_packets': rxPackets
        }

        pktProcessStatMap={
        'data_masking': 'tp_total_data_masking_count_packets',
        'dedup': 'tp_total_dedup_count_packets',
        'erspan_strip': 'tp_total_erspan_strip_count_packets',
        'etag_strip': 'tp_total_etag_strip_count_packets',
        'fabric_path_strip': 'tp_total_fabric_path_strip_count_packets',
        'gre_encap': 'tp_total_gre_encap_count_packets',
        'gtp_strip': 'tp_total_gtp_strip_count_packets',
        'lwgre_strp': 'tp_total_l2gre_strip_count_packets',
        'lisp_strip': 'tp_total_lisp_strip_count_packets',
        'mpls_strip': 'tp_total_mpls_strip_count_packets',
        'packet_length_trailer': 'tp_total_packet_length_trailer_count_packets',
        'pppoe_strip': 'tp_total_pppoe_strip_count_packets',
        'trailer_strip': 'tp_total_trailer_strip_count_packets',
        'trim_bytes': 'tp_total_trim_count_bytes',
        'trim': 'tp_total_trim_count_packets',
        'tunnel_term': 'tp_total_tunnel_term_count_packets',
        'vntag_strip': 'tp_total_vntag_strip_count_packets',
        'vxlan_strip': 'tp_total_vxlan_strip_count_packets'
        }

        if pktProcessStats is not None:
            pktProcessStats = eval(pktProcessStats)
            for pktProcessStat in pktProcessStats:
                expectedStats[pktProcessStatMap[pktProcessStat]]=pktProcessStats[pktProcessStat]
        for key1 in expectedStats:
            for key2 in actualStats:
                if key1 == key2.lower():
                    if int(expectedStats[key1]) != int(actualStats[key2]):
                        print('%s: expected(%s)!=actual(%s)' % (key1, expectedStats[key1], actualStats[key2]))
                        failed = True
                    else:
                        print('%s: expected(%s)=actual(%s)' % (key1, expectedStats[key1], actualStats[key2]))
        if failed == True:
           raise Exception('Statistics mismatch for Tool port: check above the mismatched statistics')

    @staticmethod
    def compareStatsOnFilter(actualStats, rxPackets, rxBytes, txPackets, txBytes, denyPackets=0, denyBytes=0,pktProcessStats=None):
        failed = False
        expectedStats = {
            'df_total_insp_count_packets': rxPackets,
            'df_total_insp_count_bytes': rxBytes,
            'df_total_deny_count_packets': denyPackets,
            'df_total_deny_count_bytes': denyBytes,
            'df_total_pass_count_packets': txPackets,
            'df_total_pass_count_bytes': txBytes
        }

        pktProcessStatMap={
        'data_masking': 'np_total_data_masking_count_packets',
        'dedup': 'np_total_dedup_count_packets',
        'erspan_strip': 'np_total_erspan_strip_count_packets',
        'etag_strip': 'np_total_etag_strip_count_packets',
        'fabric_path_strip': 'np_total_fabric_path_strip_count_packets',
        'gtp_strip': 'np_total_gtp_strip_count_packets',
        'lwgre_strp': 'np_total_l2gre_strip_count_packets',
        'lisp_strip': 'np_total_lisp_strip_count_packets',
        'mpls_strip': 'np_total_mpls_strip_count_packets',
        'pppoe_strip': 'np_total_pppoe_strip_count_packets',
        'trim_bytes': 'np_total_trim_count_bytes',
        'trim': 'np_total_trim_count_packets',
        'tunnel_term': 'np_total_tunnel_term_count_packets',
        'vntag_strip': 'np_total_vntag_strip_count_packets',
        'vxlan_strip': 'np_total_vxlan_strip_count_packets'
        }
        if pktProcessStats is not None:
            pktProcessStats = eval(pktProcessStats)
            for pktProcessStat in pktProcessStats:
                expectedStats[pktProcessStatMap[pktProcessStat]]=pktProcessStats[pktProcessStat]

        for key1 in expectedStats:
            for key2 in actualStats:
                if key1 == key2.lower():
                    if int(expectedStats[key1]) != int(actualStats[key2]):
                        print('%s: expected(%s)!=actual(%s)' % (key1, expectedStats[key1], actualStats[key2]))
                        failed = True
                    else:
                        print('%s: expected(%s)=actual(%s)' % (key1, expectedStats[key1], actualStats[key2]))
        if failed == True:
            raise Exception('Statistics mismatch for Filter: check above the mismatched statistics')


    @staticmethod
    def doActionOnAllNTOs(ntolist, action, *action_args):
        threadList =[]
        for nto in ntolist:
            try:
                method = getattr(nto, action)
            except AttributeError:
                raise NotImplementedError("Class %s does not implement %s" % (nto.__class__.__name__, action))
            try:
                t = threading.Thread(target=method,  args=action_args)
                t.start()
                threadList.append(t)
            except Exception as e:
                print('Unexpected exception occurred when creating threads:\n', e)
                raise
        for t in threadList:
            t.join()

    @staticmethod
    def concatenateCaptures(listOfcaptures, concatenatedCapture):
        """
        take a list of captures and return a new capture that concatenate the packets from all the other captures
        :param listOfcaptures:
        :param concatenatedCapture:
        :return: concatenatedCapture
        """
        for index, item in enumerate(listOfcaptures):
            packetsFromCapture = rdpcap(item)
            if index == 0:
                wrpcap(concatenatedCapture, packetsFromCapture)
            else:
                wrpcap(concatenatedCapture, packetsFromCapture, append=True)
        return concatenatedCapture

    @staticmethod
    def catenateToDict(myString):
        """ Description: Helper method to convert a concatenated string to a python dictionary.

            Arguments:
            * myString - a concatenated string (i.e. from Robot Concatenate keyword).

            Return Value: Python dictionary"""
        return eval(myString)
