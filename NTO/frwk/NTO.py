import requests
import json
import platform
import getpass
import time
import copy
from NTO.frwk.NTOUser import NTOUser
from NTO.frwk.NTOUserGroup import NTOUserGroup
from NTO.frwk.NTOPort import NTOPort
from NTO.frwk.NTOVirtPort import NTOVirtPort
from NTO.frwk.NTOPortGroup import NTOPortGroup
from NTO.frwk.NTOFilter import NTOFilter
from NTO.frwk.AFMR import AFMR
from pprint import pprint
from requests_toolbelt.multipart.encoder import MultipartEncoder
import os
import socket
import sys
import re

#Disable unverified HTTPS requests (cert verification is strongly advised).
if platform.system() in ['Darwin', 'Linux', 'Windows']:
    from requests.packages.urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

maxFrameSizeLookup = {'7300': {'maxFrameSize': 9380,  'maxFrameSize1G': 9380,  'maxFrameSize40G': 9380},
                      '6212': {'maxFrameSize': 12266, 'maxFrameSize1G': 12266},
                      'VISION_ONE': {'maxFrameSize': 12266, 'maxFrameSize1G': 12266, 'maxFrameSize40G': 9396},
                      'VISION_E100': {'maxFrameSize': 9412,  'maxFrameSize1G': 9412,  'maxFrameSize40G': 9412},
                      'VISION_E40': {'maxFrameSize': 12284,  'maxFrameSize1G': 12284, 'maxFrameSize40G': 12284},
                      '5288': {'maxFrameSize': 12266, 'maxFrameSize1G': 12266},
                      'TRADE_VISION': {'maxFrameSize': 12266},
                      'VISION_7712': {'maxFrameSize': 12266},
                      '5260': {'maxFrameSize': 12266, 'maxFrameSize1G': 12266}}

class NTO:

    """This library forms the base class for NTO testing within the RobotFramework.  It can support one NTO per instance and communicates with the NTO via a ReST protocol session.

       Parent Objects:

       * myParent - parent object (typically used in CTE testing only).

       Child Objects:

       * ports - list of port objects belonging to this NTO.
       * filters - list of filter objects belonging to this NTO.
       * conns - list of connection objects (NP/DF/TP assignments) belonging to this NTO.
    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self):
        self.myParent=None
        self.stats=None
        self.sess = requests.Session()
        self.userpass = ('admin', 'admin')
        self.sess.auth = self.userpass
        self.ipaddr = None
        self.ipport = None
        self.name = None
        self.sysmodel = None
        self.swver = None
        self.timeStampSrc = None
        self.ntpState = None
        self.maxFrameSize = None
        self.ports = []
        self.virtPorts = []
        self.portGrps = []
        self.filters = []
        self.conns = []
        self.afmrs = []
        self.atips = []
        # users contains a list (id & name only) of all defined users
        self.users = []
        # userGrps contains a list (id & name only) of all defined groups
        self.userGrps = []

        #NTO ReST URL dictionary that can be overridden by other classes (i.e. IFC_NTO)
        self.urls = {'api/ports': 'api/ports',
                     'api/port_groups': 'api/port_groups',
                     'api/filters': 'api/filters',
                     'api/recirculated_afm_resources': 'api/recirculated_afm_resources',
                     'api/atip_resources': 'api/atip_resources',
                     'api/monitors': 'api/monitors',
                     'api/stats': 'api/stats'}
        self.memAlloc = {}
        self.dynamicMemAlloc = {}
        self.networkMemAlloc = {}

    def reinit(self):
        self.__init__()

    def setNTOName(self, ntoName):
        """ Description:  Helper method used to set the NTO name class instance variable.

            Arguments: n/a

            Return Value: n/a """
        self.name = ntoName

    def setSystemModel(self, sysModel=None):
        """ Description:  Helper method used to set the NTO system model.

            Arguments: n/a

            Return Value: n/a """
        if sysModel is not None:
            self.sysmodel = sysModel
        else:
            self.sysmodel = self.getSystemModel()

        print('Setting system model - %s' % self.sysmodel)

    def setSystemSWVersion(self, swVersion=None):
        """ Description:  Helper method used to set the NTO system SW version.

            Arguments: n/a

            Return Value: n/a """
        if swVersion is not None:
            self.swver = swVersion
        else:
            self.swver = self.getSystemSWVersion()

        print('Setting system SW version - %s' % self.swver)

    def setMaxFrameSize(self):
        """ Description:  Helper method used to set the NTO maxFrameSize.

            Arguments: n/a

            Return Value: n/a """
        self.maxFrameSize = maxFrameSizeLookup[self.sysmodel]

    def setIPAddr(self, ipaddr):
        """ Description:  Helper method used to set the NTO IP Address class instance variable.

            Arguments: n/a

            Return Value: n/a """
        self.ipaddr = ipaddr

    def setIPPort(self, ipport):
        """ Description:  Helper method used to set the NTO IP port class instance variable.

            Arguments: n/a

            Return Value: n/a """
        self.ipport = ipport

    def setResource(self, product, resource):
        """ Description:  Method to derive the NTO IP address from Resource Manager JSON output.

            Arguments:
                * resource - Resource Manager JSON output string

            Return Value: n/a """
        # Derive NTO ipAddr from json; otherwise, directly assign it to the variable.
        try:
            resourceDict = json.loads(resource)
        except ValueError:
            self.ipaddr = resource
        else:
            for key, value in resourceDict['availableResourcesParentBound'][product].items():
                self.ipaddr = value[0]['properties']['IPAddress']
                break

    def checkPortStatus(self):
        """ This method checks that all known ports are link up.  Note that this only checks the ports in
            self.ports and does not necessarily check all ports on the NTO.
        """
        print("Checking port status...")
        allPortsUp = True
        for p in self.ports:
            if not p.getPortInfo()['link_status']['link_up']:
                print("WARNING: Port [%s] is down" % (p.name))
                allPortsUp = False

        if not allPortsUp:
            raise Exception("ERROR: One or more ports are link down!")
        else:
            print("SUCCESS: All ports are link up!")


    def getAFMR(self, afmrName):
        for a in self.afmrs:
            if a.afmrName == afmrName:
                return a
        r = AFMR()
        r.setParent(self)
        r.setAFMResourceName(afmrName)
        r.findByDefaultName(afmrName)
        print("Created AFM resource %s(%s)" % (r.afmrName, r.afmrId))
        self.addAFMRToNtoObj(r)
        return r

    def getGroups(self):
        """ Description:  ReST method used to query the groups on the NTO.

            Arguments: n/a

            Return Value: n/a
        """
        print ('Retrieving groups from NTO...')
        r = self.sess.get('https://%s:%s/api/groups' % (self.ipaddr, self.ipport), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to get groups on NTO chassis (%s) [status code = %s]' % (self.ipaddr, r.status_code))
        self.userGrps = json.loads(r.text)


    def getGroupDetails(self, gName):
        """ Description:  ReST method used to query a group's details on the NTO.

            Arguments: n/a

            Return Value:
                * r.text - ReST response string """
        print ('Retrieving group (' + gName + ') details from NTO...')
        r = self.sess.get('https://%s:%s/api/groups/%s' % (self.ipaddr, self.ipport, gName), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to get group detailss on NTO chassis (%s) [status code = %s]' % (self.ipaddr, r.status_code))
        return json.loads(r.text)


    def updateGroup(self, gName, attrs):
        print("Updating group (" + gName + ") on NTO...")

        r = self.sess.put('https://%s:%s/api/groups/%s' % (self.ipaddr, self.ipport, gName),
                          headers={'content-type': 'application/json'}, data=json.dumps(attrs), verify=False)
        if r.status_code != 200:
            print("Status code: " + str(r.status_code))
            print("Response: " + r.text)
            raise Exception('ERROR: Unable to update group details on NTO chassis (%s) [status code = %s]' % (self.ipaddr, r.status_code))

    def getLineBoards(self):
        """ Description:  ReST method used to query the line boards on the NTO.

            Arguments: n/a

            Return Value:
                * r.text - ReST response string """
        print ('Issue ReST request to get line boards...')
        r = self.sess.get('https://%s:%s/api/line_boards' % (self.ipaddr, self.ipport), verify=False)
        return(r.text)

    def getPorts(self):
        """ Description:  ReST method used to query the ports on the NTO.

            Arguments: n/a

            Return Value:
                * dictionary of ports in r.text which is the ReST response string """
        print ('Issue ReST request to get ports...')
        r = self.sess.get('https://%s:%s/%s' % (self.ipaddr, self.ipport, self.urls['api/ports']), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to get port list [%s: %s]' % (r.status_code, r.text))
        return(json.loads(r.text))

    def getPortGroups(self):
        """ Description:  REST method used to query the port grups on the NTO.

            Arguments: n/a

            Return Value:
                * dictionary of ports in r.text which is the ReST response string """
        print ('Issue REST request to get port groups...')
        r = self.sess.get('https://%s:%s/%s' % (self.ipaddr, self.ipport, self.urls['api/port_groups']), verify=False)
        return(json.loads(r.text))



    def getPortIds(self):
        """ Description:  Method to iterate over the ports dictionary and return a list of port Ids.

            Arguments: n/a

            Return Value:
                * myPortList - list of port Ids """
        print ('Returns list of portIds...')
        myPortList = []
        for port in self.ports:
            myPortList.append(port.id)
        return(myPortList)

    def getPortNames(self):
        """ Description:  Method to iterate over the ports dictionary and return a list of default port names.

            Arguments: n/a

            Return Value:
                * myPortDefaultNamesList - list of default port names """
        print('Returns list of default port names...')
        myPortDefaultNamesList = []
        for port in self.ports:
            myPortDefaultNamesList.append(port.defaultName)
        return (myPortDefaultNamesList)

    def getFilters(self):
        """ Description:  ReST method used to query the filters on the NTO.

            Arguments: n/a

            Return Value:
                * dictionary of filters in r.text which is the ReST response string """
        print ('Issue ReST request to get filters...')
        r = self.sess.get('https://%s:%s/%s' % (self.ipaddr, self.ipport, self.urls['api/filters']), verify=False)
        return(json.loads(r.text))

    def getUsers(self):
        """ Description:  ReST method used to query the users on the NTO.

            Arguments: n/a

            Return Value:
                * dictionary of users in r.text which is the ReST response string """
        print('Retrieving users from NTO...')
        r = self.sess.get('https://%s:%s/api/users' % (self.ipaddr, self.ipport), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to get users on NTO chassis (%s) [status code = %s]' % (self.ipaddr, r.status_code))
        self.users = json.loads(r.text)


    def getSystem(self, property=None):
        """ Description:  ReST method used to query the NTO system info.

            Arguments: n/a

            Return Value:
                * dictionary of system info in r.text which is the ReST response string """
        if property is None:
            print('Issue ReST get request to get system info...')
            r = self.sess.get('https://%s:%s/api/system' % (self.ipaddr, self.ipport), verify=False)
            return (json.loads(r.text))
        else:
            print('Issue ReST partial get request to get a system property info...')
            r = self.sess.get('https://%s:%s/api/system?properties=%s' % (self.ipaddr, self.ipport, property), verify=False)
            return (json.loads(r.text)[property])

    def setSystem(self, sysConfig):
        print('Setting system configuration...')
        pprint(sysConfig)
        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps(sysConfig), verify=False)
        if r.status_code != 202:
            raise Exception('ERROR: Unable to set the system info on NTO chassis (%s) [status code = %s].\ Server response: %s'
                % (self.ipaddr, r.status_code, r.text))

    def updatePort(self, pInfo):
        print('Updating port [%s]...' % (pInfo['name']))
        # determine if port will require resource attachment and save info for later
        isResource = False
        if "resource" in pInfo:
            rsrcSettings = pInfo.pop("resource")
            rsrcConfig = pInfo.pop("resource_attachment_config")
            isResource = True

        # Lookup port and create if not found
        try:
            p = self.getPort(pInfo['name'])
        except:
            p = NTOPort()
            # Push new config to port
            p.update(self, pInfo)
            # Fill out port data members based on NTO config
            p.updateFromNTO()
            self.addPortToNtoObj(p)
        if isResource:
            a = self.getAFMR(rsrcSettings['id'])
            p.attachAFMResource(a, rsrcSettings['allocated_bandwidth'])
            p.update({"resource_attachment_config": rsrcConfig})
            p.updateFromNTO()
        return p

    def createFilter(self, fInfo):
        f = NTOFilter()
        if "resource" in fInfo:
            rsrcSettings = fInfo.pop("resource")
            rsrcConfig = fInfo.pop("resource_attachment_config")
            f.create(self, fInfo)
            a = self.getAFMR(rsrcSettings['id'])
            f.attachAFMResource(a, rsrcSettings['allocated_bandwidth'])
            f.update({"resource_attachment_config" : rsrcConfig})
        else:
            f.create(self, fInfo)
        self.addFilterToNtoObj(f)
        return f

    def setPortAndFilterConfig(self, sysInfo):
        print('Setting port and filter configuration...')
        pprint(sysInfo)
        srcPorts = []
        destPorts = []
        for p in sysInfo['Paths']:
            print("Creating path [%s]..." % (p['PathName']))
            srcPorts = []
            destPorts = []
            for np in p['NetworkPorts']:
                np['enabled'] = True
                npObj = self.updatePort(np)
                srcPorts.append(npObj.id)

            for tp in p['ToolPorts']:
                tp['enabled'] = True
                tpObj = self.updatePort(tp)
                destPorts.append(tpObj.id)

            # if filter is not specified then create basic pass all filter
            f = {}
            if 'FilterInfo' in p:
                f = p['FilterInfo']
            if ('mode' not in f):
                f['mode'] = 'PASS_ALL'
            # add source and dest ports captured above
            f['source_port_list'] = srcPorts
            f['dest_port_list'] = destPorts
            filtObj = self.createFilter(f)

    def getMonitors(self):
        """ Description:  ReST method used to query the monitors on the NTO.

            Arguments: n/a

            Return Value:
                * dictionary of monitors in r.text which is the ReST response string """
        print('Issue ReST request to get monitors info...')
        r = self.sess.get('https://%s:%s/%s' % (self.ipaddr, self.ipport, self.urls['api/monitors']), verify=False)
        return (json.loads(r.text))

    def changePortSpeedConfig(self, portList, qsfp28Mode):
        """ Description:  ReST method used to change the speed configurations of one or more ports on a NTO.

            Arguments:
                portList - List of one or more ports to change configuration mode/speeds
                qsfp28Mode - Mode to set the QSFP28 Phy to (e.g. 'MODE_QSFP28', 'MODE_QSFP', or 'MODE_SFP')

            Return Value:
                * r.status_code - status from ReST post request """
        print('Issue ReST post request to change port(s) QSFP28 mode...')
        payload = {'port_list': portList, 'qsfp28_port_mode': qsfp28Mode}
        r = self.sess.post('https://%s:%s/api/actions/change_speed_configuration' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to set the port(s) %s QSFP28 mode (%s) on NTO chassis (%s) [status code = %s].\ Server response: %s'
                            % (portList, qsfp28Mode, self.ipaddr, r.status_code, r.text))
        return (r.status_code)

    def cleanupEphemeralNamespace(self):
        """ Description:  Clear all logical objects (e.g. virtual ports, port groups, filters, and connection objects) from the NTO namespace.

            Arguments: n/a

            Return Value: n/a """
        print('Clear all ephemeral objects in the %s namespace...' % self.name)
        self.virtPorts = []
        self.portGrps = []
        self.filters = []
        self.conns = []

    def clearFiltersAndPorts(self, namespaceOnly=False):
        """ Description:  ReST method used to clear all ports and filters on a NTO.

            WARNING:  This method should never be used in testing unless running against dedicated NTO resources!!!

            Arguments: n/a

            Return Value:
                * r.status_code - status from ReST post request """
        # Adding the following re-inits to keep NTO object consistent with physical NTO
        self.ports = []
        self.virtPorts = []
        self.portGrps = []
        self.filters = []
        self.conns = []
        self.afmrs = []
        self.atips = []

        if not namespaceOnly:
            print('\n\n\nWARNING:  The clearFiltersAndPorts method should never be used in testing unless running against dedicated NTO resources!!!\n\n\n')
            print('Issue ReST post request to clear all filters and ports on a chassis level...')
            r = self.sess.post('https://%s:%s/api/actions/clear_filters_and_ports' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, verify=False)
            if r.status_code != 200:
                raise Exception('ERROR: Unable to clear all filters and ports on NTO chassis (%s) [status code = %s]' % (self.ipaddr, r.status_code))

            return (r.status_code)



    def deleteMonitors(self):
        """ Description:  Method to get all monitors present on a NTO and then delete them via ReST.

            Arguments: n/a

            Return Value: n/a """
        print('Issue ReST request to delete all monitors...')
        r = self.sess.get('https://%s:%s/%s' % (self.ipaddr, self.ipport, self.urls['api/monitors']), verify=False)
        monitorList = json.loads(r.text)
        if not monitorList:
            print('No monitors present on system...')
        else:
            for item in monitorList:
                print('Deleting monitor %s (%s)' % (item['id'], item['name']))
                r = self.sess.delete('https://%s:%s/%s/%s' % (self.ipaddr, self.ipport, self.urls['api/monitors'], item['id']), verify=False)
                if r.status_code != 200:
                    raise Exception('ERROR: Unable to remove monitor (id: %s; name: %s [status code = %s])' % (item['id'], item['name'], r.status_code))

    def getSystemModel(self):
        """ Description:  ReST method used to get the system model info for a NTO.

            Arguments: n/a

            Return Value:
                * System model using the 'type' key of the dictionary from r.text which is the ReST response string """
        print('Issue ReST request to get system model info...')
        r = self.sess.get('https://%s:%s/api/system' % (self.ipaddr, self.ipport), verify=False)
        return(json.loads(r.text)['type'])

    def setStatsPollingInterval(self, statsPollInt):
        """ Description:  ReST method used to set the NTO's stats polling interval.

            Arguments:
                * statsPollInt - statistics polling interval (in seconds)

            Return Value:
                * r.status_code - ReST status code response """
        print('Issue ReST to put the stats polling interval ...')
        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps({'stats_polling_interval': int(statsPollInt)}), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to set the stats polling interval on NTO (%s) to %s secs: [status code = %s]' % (self.ipaddr, statsPollInt, r.status_code))
        return (r.status_code)

    def setWebAPITokenTimeout(self, unit, value, retries = 10):
        """ Description:  Method used to set the NTO's WebAPI token timeout.

            Arguments:
                * unit - string, valid values are 'SEC', 'MIN', 'HOUR', 'DAY', 'WEEK'
                * value - int

            Return Value: N/A """

        if unit not in ['SEC', 'MIN', 'HOUR', 'DAY', 'WEEK']:
            raise Exception('ERROR: Provided timeout unit [%s] is not valid. '
                            'Please see valid values for timeout unit' % unit)
        print('Issue REST command to update the WebAPI token timeout interval to %s %s on NTO %s'
              % (value, unit, self.ipaddr))
        payload = {'web_api_config': {'enabled': True,
                                      'port': int(self.ipport),
                                      'token_timeout': {'unit': unit, 'value': int(value)}}}
        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport),
                          headers={'content-type': 'application/json'},
                          data=json.dumps(payload),
                          verify=False)
        if r.status_code != 202:
            raise Exception('ERROR: Unable to update the WebAPI token timeout on NTO (%s) : [status code = %s].\n'
                            'Server response : %s' % (self.ipaddr, r.status_code, r.text))
        else:
            print('On NTO %s %s' % (self.ipaddr, json.loads(r.text)['message']))
            for i in range(retries):
                print('Sleep for 10 seconds...')
                time.sleep(10)
                try:
                    tokenTimeout = self.getSystem(property='web_api_config')['token_timeout']
                    break
                except requests.ConnectionError:
                    if i == retries-1:
                        raise Exception('ERROR: Unable to reconnect to NTO %s after triggering an WebAPI config update'
                                        % self.ipaddr)
                    else:
                        pass
            if tokenTimeout['unit'] != unit or tokenTimeout['value'] != int(value):
                raise Exception('ERROR: the WebAPI timeout settings were not updated.\n Current timeout settings are: %s'
                                % tokenTimeout)
            else:
                print('On NTO %s the WebAPI token timeout was updated to %s %s' % (self.ipaddr, value, unit))

    def getSystemBanner(self):
        """ Description:  ReST method used to get the system banner info for a NTO.

            Arguments: n/a

            Return Value:
                * System banner using the 'login_banner_config' and 'text' keys of the dictionary from r.text which is the ReST response string """
        print('Issue ReST request to get system banner...')
        r = self.sess.get('https://%s:%s/api/system?properties=login_banner_config' % (self.ipaddr, self.ipport), verify=False)
        return (json.loads(r.text)['login_banner_config']['text'])

    def updateSystemBannerWarning(self):
        """ Description:  ReST method used to update the NTO's banner warning that the system is in use.

            Arguments: n/a

            Return Value:
                * r.status_code - ReST status code response """
        myBanner = self.getSystemBanner()
        myPortsDefaultNamesList = self.getPortNames()
        newBanner = '<br>ATTENTION! %s is using host %s to run automated tests on %s' % (getpass.getuser(), platform.node(), myPortsDefaultNamesList)
        if myBanner != None:
            myBannerStrList = self.getSystemBanner().rstrip().split('\n')
            for line in list(myBannerStrList):
                if (portName in myBannerStrList for portName in myPortsDefaultNamesList):
                   myBannerStrList.remove(line)
            newBanner.join(myBannerStrList)

        print('Issue ReST request to put update to system banner...')
        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps({'login_banner_config': {'text': newBanner}}), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to update login banner warning on NTO (%s): [status code = %s]' % (self.ipaddr, r.status_code))

        return (r.status_code)

    def removeWarningFromSystemBanner(self):
        """ Description:  ReST method used to remove the in-use warning from the NTO's system banner.

            Arguments: n/a

            Return Value:
                * r.status_code - ReST status code response """
        if self.getSystemBanner() != None:
            myBannerStrList = self.getSystemBanner().rstrip().split('\n')
            myPortsDefaultNamesList = self.getPortNames()
            print('Issue ReST request to remove warning from system banner...')
            for line in list(myBannerStrList):
                if (portName in myBannerStrList for portName in myPortsDefaultNamesList):
                    myBannerStrList.remove(line)

            r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps({'login_banner_config': {'text': ''.join(myBannerStrList)}}), verify=False)
            if r.status_code != 200:
                raise Exception('ERROR: Unable to remove login banner warning on NTO (%s): [status code = %s].\n Server response: %s'
                                % (self.ipaddr, r.status_code, r.text))

            return (r.status_code)

    def getSystemSWVersion(self):
        """ Description:  ReST method used to get the system software version info for a NTO.

            Arguments: n/a

            Return Value:
                * Software version using the 'software_version' key of the dictionary from r.text which is the ReST response string """
        print('Issue ReST request to get system SW version info...')
        r = self.sess.get('https://%s:%s/api/system' % (self.ipaddr, self.ipport), verify=False)
        return(json.loads(r.text)['software_version'])

    def getInlineServiceChainIDListForDevice(self):
        """ Description:  ReST method used to query the inline service chain ID's list on the NTO.

            Arguments: n/a

            Return Value:
                * r.text which is the ReST response string """
        print('Issue ReST request to get Inline Service Chain info...')
        r = self.sess.get('https://%s:%s/api/inline_service_chains' % (self.ipaddr, self.ipport), verify=False)
        return (r.text)

    def addPortToNtoObj(self, portObj):
        """ Description:  Helper method to add a port to the NTO class instance list 'ports' while checking for duplicate names.

            Arguments:
                * portObj - object containing NTO port specific information

            Return Value: n/a """
        print("Adding port (" + str(portObj.name) + ") to NTO...")
        #Check for duplicate port names.
        duplicate = [port.name for port in self.ports if port.name == portObj.name]
        if duplicate:
            raise Exception('ERROR:  Duplicate port name %s already exists for this %s configuration!' % (duplicate, self.name))
        else:
            self.ports.append(portObj)

    def addVirtPortToNtoObj(self, virtPortObj):
        """ Description:  Helper method to add a virtual port to the NTO class instance list 'virtPorts' while checking for duplicate names.

            Arguments:
                * virtPortObj - object containing NTO virtual port specific information

            Return Value: n/a """
        #Check for duplicate port names.
        duplicate = [virtPort.virtPortName for virtPort in self.virtPorts if virtPort.portName == virtPortObj.portName]
        if duplicate:
            raise Exception('ERROR:  Duplicate virtual port name %s already exists for this %s configuration!' % (duplicate, self.name))
        else:
            self.virtPorts.append(virtPortObj)

    def addPortGrpToNtoObj(self, portGrpObj):
        """ Description:  Helper method to add a port group to the NTO class instance list 'portGrps' while checking for duplicate names.

            Arguments:
                * portGrpObj - object containing NTO port group specific information

            Return Value: n/a """
        #Check for duplicate port group names.
        duplicate = [portGrp.portGrpName for portGrp in self.portGrps if portGrp.portGrpName == portGrpObj.portGrpName]
        if duplicate:
            raise Exception('ERROR:  Duplicate port group name %s already exists for this %s configuration!' % (duplicate, self.name))
        else:
            self.portGrps.append(portGrpObj)

    def addFilterToNtoObj(self, filterObj):
        """ Description:  Helper method to add a filter to the NTO class instance list 'filters' while checking for duplicate names.

            Arguments:
                * filterObj - object containing NTO filter specific information

            Return Value: n/a """
        #Check for duplicate filter names.
        duplicate = [filter.name for filter in self.filters if filter.name == filterObj.name]
        if duplicate:
            raise Exception('ERROR:  Duplicate filter name %s already exists for this %s configuration!' % (duplicate, self.name))
        else:
            self.filters.append(filterObj)

    def addConnToNtoObj(self, connObj):
        """ Description:  Helper method to add a connection to the NTO class instance list 'conns' while checking for duplicate names.

            Arguments:
                * connObj - object containing connection specific information

            Return Value: n/a """
        #Check for duplicate connection names.
        duplicate = [conn.connName for conn in self.conns if conn.connName == connObj.connName]
        if duplicate:
            raise Exception('ERROR:  Duplicate connection name %s already exists for this %s configuration!' % (duplicate, self.name))
        else:
            self.conns.append(connObj)

    def addAFMRToNtoObj(self, afmrObj):
        """ Description:  Helper method to add an AFM resource to the NTO class instance list 'afmrs' while checking for duplicate names.

            Arguments:
                * afmrObj - object containing NTO AFM specific information

            Return Value: n/a """
        #Check for duplicate AFM Resource names.
        duplicate = [afmr.afmrName for afmr in self.afmrs if afmr.afmrName == afmrObj.afmrName]
        if duplicate:
            raise Exception('ERROR:  Duplicate AFM resource name %s already exists for this %s configuration!' % (duplicate, self.name))
        else:
            self.afmrs.append(afmrObj)

    def addATIPToNtoObj(self, atipObj):
        """ Description:  Helper method to add an ATIP resource to the NTO class instance list 'atips' while checking for duplicate names.

            Arguments:
                * atipObj - object containing NTO ATIP specific information

            Return Value: n/a """
        #Check for duplicate ATIP Resource names.
        duplicate = [atip.atipName for atip in self.atips if atip.atipName == atipObj.atipName]
        if duplicate:
            raise Exception('ERROR:  Duplicate ATIP resource name %s already exists for this %s configuration!' % (duplicate, self.name))
        else:
            self.atips.append(atipObj)

    def addStatsToNtoObj(self, statsObj):
        """ Description:  Helper method to add an stats object to the NTO class instance.

            Arguments:
                * statsObj - object containing NTO ATIP specific information

            Return Value: n/a """
        self.stats=statsObj

    def getStatsOnPorts(self, portMode=None, statName=None):
        """ Description:  ReST method to retrieve a named stat or all stats for all ports contained in the NTO 'ports' list.

            Arguments:
                * portMode  - <default = None> mode of the port (i.e. 'network' or 'tool')
                * statsName - <default = None> name of an individual statistic to return from the stats snapshot of one or more ports contained in the NTO 'ports' list

            Return Value:
                * If statsName = None <default>, returns dictionary of stats snapshot for one or more ports contained in the NTO 'ports' list
                * If statsName != None, returns a list of the stat specified in statsName for one or more ports contained in the NTO 'ports' list
        """
        if portMode is None:
            portList = [myPort.id for myPort in self.ports]
        else:
            portList = [myPort.id for myPort in self.ports if myPort.portMode == portMode]

        if statName is None:
            print('Issue ReST post request to get stats on ports %s ...' % portList)
            payload = {'port': portList}
        else:
            print('Issue ReST post request to get stat %s on %s ports %s ...' % (statName, portMode, portList))
            payload = {'port': portList, 'stat_name': statName}

        r = self.sess.post('https://%s:%s/%s' % (self.ipaddr, self.ipport, self.urls['api/stats']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            if statName is None:
                raise Exception('ERROR: Unable to get stats from ports on NTO chassis (%s) [status code = %s]' % (self.ipaddr, r.status_code))
            else:
                raise Exception('ERROR: Unable to get stat %s from %s ports on NTO chassis (%s) [status code = %s]' % (statName, portMode, self.ipaddr, r.status_code))

        if statName is None:
            return (json.loads(r.text)['stats_snapshot'])
        else:
            return ([{myStat['default_name']: myStat[statName.lower()]} for myStat in json.loads(r.text)['stats_snapshot']])

    def stopAllCaptures(self):
        """ Description:  Helper method to stop captures on one or more ports specified in the NTO's 'ports' list.

            Arguments: n/a

            Return Value: n/a """
        for myPort in self.ports:
            myPort.stopCaptureIfStarted()

    def delAllCaptureFiles(self):
        """ Description:  Helper method to delete port captures on one or more ports specified in the NTO's 'ports' list.

            Arguments: n/a

            Return Value: n/a """
        for myPort in self.ports:
            myPort.delAllPortCaptureFiles()

    def delAllPortGroupsAndSrcFilters(self):
        """ Description:  Helper method to delete port source filters on one or more ports specified in the NTO's 'ports' list.

            Arguments: n/a

            Return Value: n/a """
        for myPort in self.ports:
            myPort.deletePortGroupsAndSrcFilters()

    def delAllPortGroupsAndDestFilters(self):
        """ Description:  Helper method to delete port destination filters on one or more ports specified in the NTO's 'ports' list.

            Arguments: n/a

            Return Value: n/a """
        for myPort in self.ports:
            myPort.deletePortGroupsAndDestFilters()

    def delAllPortGroups(self):
        """ Description:  Helper method to delete port groups (if provisioned) on one or more ports specified in the NTO's 'portGrps' list.

            Arguments: n/a

            Return Value: n/a """
        for myPort in self.ports:
            myPort.deletePortGrp()

    def delAllVirtPorts(self):
        """ Description:  Helper method to delete virtual ports (if provisioned) on one or more ports specified in the NTO's 'ports' list.

            Arguments: n/a/

            Return Value: n/a """
        for myPort in self.ports:
            myPort.deleteVirtPorts()

    def setAllPortsToDefaults(self):
        """ Description:  Helper method to set defaults on one or more ports specified in the NTO's 'ports' list.

            Arguments: n/a

            Return Value: n/a """
        for myPort in self.ports:
            myPort.setPortToDefaults()

    def configAllFilters(self):
        """ Description:  Helper method that calls configureFilter on one or more filters specified in the NTO's 'filters' list.

            Arguments: n/a

            Return Value: n/a """
        for myFilter in self.filters:
            myFilter.configureFilter()

    def resetAllStats(self):
        """ Description:  Helper method that calls resetStatsOnPorts() and resetStatsOnFilters.

            Arguments: n/a

            Return Value: n/a """
        self.resetStatsOnPorts()
        self.resetStatsOnFilters()

    def initAllPortsAndFilters(self):
        """ Description:  Helper method that calls configPortsAndDefaultFilters(), configAllFilters(), and resetAllStats().

            Arguments: n/a

            Return Value: n/a """
        self.configPortsAndDefaultFilters()
        self.configAllFilters()
        self.resetAllStats()

    def resetAllIxPorts(self):
        """ Description:  Helper method that resets each Ixia port (if ixPortObj present) for one or more ports in the NTO 'ports' list.

            Arguments: n/a

            Return Value: n/a """
        for myPort in self.ports:
            if myPort.ixPortObj is not None:
                myPort.ixPortObj.resetIxPort()

    def clearAllIxPorts(self):
        """ Description:  Helper method that clears each Ixia port (if ixPortObj present) for one or more ports in the NTO 'ports' list.

            Arguments: n/a

            Return Value: n/a """
        for myPort in self.ports:
            if myPort.ixPortObj is not None:
                myPort.ixPortObj.clearIxPortStats()

    def cleanupAllIxPorts(self):
        """ Description:  Helper method that calls stopIxTraffic() and releaseIxPort() for each Ixia port (if ixPortObj present) for one or more ports in the NTO 'ports' list.

            Arguments: n/a

            Return Value: n/a """
        for myPort in self.ports:
            if myPort.ixPortObj is not None:
                myPort.ixPortObj.stopIxTraffic()
                myPort.ixPortObj.releaseIxPort()

    def initAllIxPorts(self):
        """ Description:  Helper method that calls resetAllIxPorts() and clearAllIxPorts().

            Arguments: n/a

            Return Value: n/a """
        self.resetAllIxPorts()
        self.clearAllIxPorts()

    def configureAllIxPorts(self):
        """ Description:  Helper method that calls configureIxPort() for each Ixia port (if ixPortObj present) for one or more ports in the NTO 'ports' list.

            Arguments: n/a

            Return Value: n/a """
        for myPort in self.ports:
            if myPort.ixPortObj is not None:
                myPort.ixPortObj.configureIxPort()

    def configureAllIxStreams(self, continuous=False, percentPktRate=None):
        """ Description:  Helper method that calls configureIxStreams() for each stream for each Ixia port (if ixPortObj present) for one or more ports in the NTO 'ports' list.

            Arguments: n/a

            Return Value: n/a """
        for myPort in self.ports:
            if myPort.ixPortObj is not None:
                if myPort.ixPortObj.streams:
                    if continuous==True or continuous=='True':
                        myPort.ixPortObj.configureIxStreams(continuous=True, percentPktRate=percentPktRate)
                    else:
                        myPort.ixPortObj.configureIxStreams(percentPktRate=percentPktRate)

    def setupNVSDevices(self):
        """ Description:  Helper method that calls several methods to configure NTO ports and filters for traffic.  Note that it also sets the stats polling interval to 1 second.

            Arguments: n/a

            Return Value: n/a """
        self.deleteMonitors()

        if (self.getSystemModel() in ['BUFFALO', 'VISION_ONE', '6322', '6212']):
            self.getInlineServiceChainIDListForDevice()
            #TBD - ResetInLineConfig

        self.stopAllCaptures()
        self.delAllCaptureFiles()

        #self.delAllPortGroupsAndSrcFilters()  #Not needed since all filters can be cleared from the dest list.
        self.delAllPortGroupsAndDestFilters()
        self.delAllVirtPorts()
        self.detachAFMResources()
        self.setAllPortsToDefaults()

        self.updateSystemBannerWarning()
        self.setStatsPollingInterval(1)

    def cleanupNVSDevices(self):
        """ Description:  Helper method that calls several methods to remove NTO ports and filters as well as reset the banner warning message.

            Arguments: n/a

            Return Value: n/a """
        self.delAllVirtPorts()
        self.detachAFMResources()
        self.delAllPortGroupsAndDestFilters()
        self.delAllPortGroupsAndSrcFilters()
        self.setAllPortsToDefaults()
        self.removeWarningFromSystemBanner()

    def resetStatsOnPorts(self, portList=None):
        """ Description:  Method to reset stats on one or more ports in the NTO 'ports' list.

            Arguments:
                * portList - <default = None> List of ports to override the NTO 'ports' list

            Return Value:
                * r.status_code - ReST post return status code """
        if portList is None:
            portList = [myPort.id for myPort in self.ports]
        print('Issue ReST post request to reset stats on ports %s ...' % portList)
        payload = {'PORT': portList}
        r = self.sess.post('https://%s:%s/%s/reset' % (self.ipaddr, self.ipport, self.urls['api/stats']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        # print("NTO Command, URL ", r.url)
        # print("NTO Command, Payload: ", payload)
        # print("NTO Response, Code: ",r.status_code)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to reset stats on ports %s for NTO chassis (%s) [status code = %s]' % (portList, self.ipaddr, r.status_code))
        return (r.status_code)

    def resetStatsOnIxPorts(self, portList=None):
        """ Description:  Method to reset stats on one or more Ixia ports connected to the NTO ports.

            Arguments:
                * portList - <default = None> List of ports to override the NTO 'ports' list

            Return Value:
                * r.status_code - ReST post return status code """
        if portList is None:
            for port in self.ports:
                port.ixPortObj.clearIxPortStats()
        else:
            for port in portList:
                port.ixPortObj.clearIxPortStats()


    def getStatsOnFilters(self, statName=None):
        """ Description:  ReST method to retrieve a named stat or all stats for all filters contained in the NTO 'filters' list.

            Arguments:
                * statsName - <default = None> name of an individual statistic to return from the stats snapshot of one or more filters contained in the NTO 'filters' list

            Return Value:
                * If statsName = None <default>, returns dictionary of stats snapshot for one or more filters contained in the NTO 'filters' list
                * If statsName != None, returns a list of the stat specified in statsName for one or more filters contained in the NTO 'filters' list
            """
        filterList = [myFilter.id for myFilter in self.filters]
        if statName is None:
            print('Issue ReST post request to get stats on filters %s ...' % filterList)
            payload = {'filter': filterList}
        else:
            print('Issue ReST post request to get stat %s on filters %s ...' % (statName, filterList))
            payload = {'filter': filterList, 'stat_name': statName}
        r = self.sess.post('https://%s:%s/%s' % (self.ipaddr, self.ipport, self.urls['api/stats']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            if statName is None:
                raise Exception('ERROR: Unable to get stats from filters on NTO chassis (%s) [status code = %s]' % (self.ipaddr, r.status_code))
            else:
                raise Exception('ERROR: Unable to get stat %s from filters on NTO chassis (%s) [status code = %s]' % (statName, self.ipaddr, r.status_code))

        if statName is None:
            return (json.loads(r.text)['stats_snapshot'])
        else:
            return ([{myStat['id']: myStat[statName.lower()]} for myStat in json.loads(r.text)['stats_snapshot']])

    def resetStatsOnFilters(self, filterList=None):
        """ Description:  Method to reset stats on one or more filters in the NTO 'filters' list.

            Arguments:
                * filterList - <default = None> List of filters to override the NTO 'filters' list

            Return Value:
                * r.status_code - ReST post return status code """
        if filterList is None:
            filterList = [myFilter.id for myFilter in self.filters]
        print('Issue ReST post request to reset stats on filters %s ...' % filterList)
        payload = {'FILTER': filterList}
        r = self.sess.post('https://%s:%s/%s/reset' % (self.ipaddr, self.ipport, self.urls['api/stats']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to reset stats on filters %s for NTO chassis (%s) [status code = %s]' % (filterList, self.ipaddr, r.status_code))
        return (r.status_code)

    def getAFMResources(self):
        print('Issue ReST request to get all AFM resources ...')
        r = self.sess.get('https://%s:%s/%s' % (self.ipaddr, self.ipport, self.urls['api/recirculated_afm_resources']), verify=False)
        pprint(r.text)
        return (r.text)

    def getATIPResources(self):
        print('Issue ReST request to get all AFM resources ...')
        r = self.sess.get('https://%s:%s/%s' % (self.ipaddr, self.ipport, self.urls['api/atip_resources']), verify=False)
        pprint(r.text)
        return (r.text)

    def detachAFMResources(self):
        """ Description:  Method to disconnect AFM Resource from one or more objects in the NTO.

            Arguments: n/a

            Return Value: n/a """

        for port in self.ports:
            port.detachResource()
        for filter in self.filters:
            filter.detachResource()
        #for portGroup in self.portGrps:
        #    portGroup.detachResource()


    def resetInLineConfig(self, ports):
        """ Description:  Method to reset in-line configuration for one or more ports in the NTO 'ports' list.

            Arguments:
                * ports - List of ports

            Return Value: n/a """
        # TODO for 7300 and Vision One
        pass

    def configPortsAndDefaultFilters(self):
        """ Description:  Method to configure each NP, DF, TP, and/or Port Group for one or more connections contained in the NTO 'conns' list.

            Arguments: n/a

            Return Value: n/a """

        configuredObjs=[]

        #Create a temp list and iterate over the connection objects.
        for myConn in self.conns:

            #Init the network ports and network port groups lists.
            networkIdList = []
            networkPGIdList = []

            #Iterate over the network objects.
            myList = []
            if type(myConn.network) is list:
                myList.extend(myConn.network)
            else:
                myList.append(myConn.network)

            for myObj in myList:
                if myObj is not None and myObj != 'None':
                    if isinstance(myObj, NTOPortGroup):
                        #Configure ports in Port Group but do NOT add them to the networkList since they may not all belong to this connection!
                        for myPort in myObj.ports:
                            if myPort not in configuredObjs:
                                myPort.configurePort()
                                configuredObjs.append(myPort)
                        if myObj not in configuredObjs:
                            myObj.createPortGrp()
                            configuredObjs.append(myObj)
                        networkPGIdList.append(myObj.portGrpId)
                    else:
                        if isinstance(myObj, NTOPort) and myObj not in configuredObjs:
                            myObj.configurePort()
                            configuredObjs.append(myObj)
                        networkIdList.append(myObj.id)

            #Init the tool ports and tool port groups lists.
            toolIdList = []
            toolPGIdList = []

            #Create a temp list and iterate over the tool objects.
            myList = []
            if type(myConn.tool) is list:
                myList.extend(myConn.tool)
            else:
                myList.append(myConn.tool)

            for myObj in myList:
                if myObj is not None and myObj != 'None':
                    if isinstance(myObj, NTOPortGroup):
                        #Configure ports in Port Group but do NOT add them to the toolList since they may not all belong to this connection!
                        for myPort in myObj.ports:
                            if myPort not in configuredObjs:
                                if isinstance(myPort, NTOVirtPort):
                                    if myPort.myParent not in configuredObjs:
                                        myPort.myParent.configurePort()
                                        configuredObjs.append(myPort.myParent)
                                myPort.configurePort()
                                configuredObjs.append(myPort)
                        if myObj not in configuredObjs:
                            myObj.createPortGrp()
                            configuredObjs.append(myObj)
                        toolPGIdList.append(myObj.portGrpId)
                    elif isinstance(myObj, NTOVirtPort):
                        if myObj not in configuredObjs:
                            if myObj.myParent not in configuredObjs:
                                myObj.myParent.configurePort()
                                configuredObjs.append(myObj.myParent)
                            myObj.configurePort()
                            configuredObjs.append(myObj)
                        toolIdList.append(myObj.id)
                    else:
                        if myObj not in configuredObjs:
                            myObj.configurePort()
                            configuredObjs.append(myObj)
                        toolIdList.append(myObj.id)

            #Iterate over the filter objects.
            myList = []
            if type(myConn.filter) is list:
                myList.extend(myConn.filter)
            else:
                myList.append(myConn.filter)

            for myDF in myList:
                myDF.sourcePortIds.extend(networkIdList)
                myDF.sourcePortGrpIds.extend(networkPGIdList)
                myDF.destPortIds.extend(toolIdList)
                myDF.destPortGrpIds.extend(toolPGIdList)
                myDF.createDefaultPassAllFilter()

    def importConfig(self, filePath, importType='FULL_IMPORT_FROM_BACKUP'):
        """ Description:  ReST method used to import config on the NTO.

                    Arguments:
                        filePath: Str with importFile Path ex: /home/Ubuntu/export.ata
                        importType: Type of export ex: 'TRAFFIC_CONFIG' or 'FULL_IMPORT_FROM_BACKUP'

                    Return Value: n/a """

        print('Importing (%s) config from: %s' % (importType, os.path.basename(filePath)))
        payload = {'import_type': importType}
        multipart = MultipartEncoder(
            fields={'field0': (os.path.basename(filePath), open(filePath, 'rb'), 'application/octet-stream'),
                    'field1': ("json", json.dumps(payload), 'application/json')})
        print(multipart)
        print(multipart.content_type)

        r = self.sess.post('https://%s:%s/api/actions/import' % (self.ipaddr, self.ipport), verify=False, data=multipart, headers={'Content-Type': multipart.content_type})

        if r.status_code != 200:
            raise Exception('ERROR: Unable to import on (%s) [status code = %s]' % (self.ipaddr, r.status_code))

        print("Import (%s) was successful from: %s" % (importType, filePath))


    def exportConfig(self, filePath, exportType='FULL_BACKUP'):

        """ Description:  ReST method used to export config on the NTO.

                    Arguments:
                        filePath: Str with exportFile Path ex: /home/Ubuntu/export.ata
                        exportType: Type of export ex: 'TRAFFIC_CONFIG' or 'FULL_BACKUP'

                    Return Value: n/a """

        print('Exporting (%s) config to: %s' % (exportType, os.path.basename(filePath)))
        filePath = filePath + self.getSystemModel() + '_' + self.ipaddr + '_' + exportType + '.ata'
        exportFile = open(filePath,'wb')
        exportFile.seek(0)
        payload = {'export_type': exportType}
        r = self.sess.post('https://%s:%s/api/actions/export' % (self.ipaddr, self.ipport), verify=False, data=json.dumps(payload), headers={'Content-Type': 'application/json'})

        if r.status_code != 200:
            raise Exception('ERROR: Unable to import configuration on (%s) [status code = %s]' % (self.ipaddr, r.status_code))

        exportFile.write(r.content)
        exportFile.close()
        print("Export (%s) was successful to: %s" % (exportType, filePath))
        return filePath

    def installLicense(self, fileName, entity=None):
        """ Description:  ReST multipart post used to install a license on the NTO.

                    Arguments:
                        fileName - path to license file
                        entity - (optional) None which is default or union for CTE system

                    Return Value: n/a """

        if entity is None:
            multipart = MultipartEncoder(fields={'field0': (os.path.basename(fileName), open(fileName, 'rb'), 'application/octet-stream')})
        elif entity == 'union':
            multipart = MultipartEncoder(fields={'field0': (None, json.dumps({'entity':'union'}), 'application/json'),
                                                 'field1': (os.path.basename(fileName), open(fileName, 'rb'), 'application/octet-stream')})

        print(multipart)
        print(multipart.content_type)
        print()

        print("####LICENSE DOWNLOAD STARTED")
        r = self.sess.post('https://%s:%s/api/actions/install_license' % (self.ipaddr, self.ipport), verify=False, data=multipart, headers={'Content-Type': multipart.content_type})

        #Check the return status code.
        if r.status_code != 200:
            raise Exception(
                'ERROR: Unable to install license on (%s) [status code = %s]' % (self.ipaddr, r.status_code))
        else:
            print("####LICENSE DOWNLOAD FINISHED for {}".format(self.ipaddr))

    def validateMaintenanceContract(self, licenseFile):
        """ Description:  Validate an installed maintenance contract license S/N and expiration match those of the license file.

                    Arguments:
                        licenseFile - path to licenseFile

                    Return Value: n/a """

        sysSerialNum = self.getSystem('hardware_info')['system_id']
        installedLicenseList = self.getSystem('license_feature_list')

        #Get the maintenance license dictionary from the ReST licenseList.
        installedMaintLicDict = next((item for item in installedLicenseList if ((item['description']=='Maintenance Contract') and (('Ixia Model-Serial %s' % sysSerialNum) in item['comment']))), None)
        print('Installed License Maintenance Contract - %s' % installedMaintLicDict)
        installedMaintLicExpTimeStr = time.strftime('%B %d, %Y %-I:%M:%S %p', time.gmtime(installedMaintLicDict['expiration_time']/1000.0))
        print('Installed License Maintenance Contract expiration time (epoch = %s) - %s' % (installedMaintLicDict['expiration_time'], installedMaintLicExpTimeStr))

        #Read license file (text only) into memory.
        readList=[]
        maintContractList=[]
        with open(licenseFile, 'r') as input:
            # Skips text before the beginning of the interesting block:
            for line in input:
                if 'BEGIN LICENSE KEY' in line:
                    break
                else:
                    readList.append(line.strip())

        #Iterate through the readList and find the Maintenance Contract block with appropriate System S/N.
        for count,line in enumerate(readList):
            if 'Maintenance Contract' in line and sysSerialNum in readList[count+1]:
                maintContractList=readList[count:count+4]

        print('License File Maintenance Contract - %s' % maintContractList)

        if not(maintContractList):
            raise Exception('ERROR:  Unable to find Maintenance Contract in license file: %s' % licenseFile)
        else:
            expTimeStr = next((item for item in maintContractList if 'Expires' in item), None)

            #Check to see if the installed Maint Contract expiration time str matches the license file Main Contract expiration time string (ignoring the timezone because it should read GMT instead).
            if not(installedMaintLicExpTimeStr in expTimeStr):
                raise Exception('ERROR: Installed maintenance contract expiration (epoch = %s): %s does not match actual license file: %s.' % (installedMaintLicDict['expiration_time'], installedMaintLicExpTimeStr, expTimeStr))


    def getUserId(self, u):
        """ Description: This command retrieves the user id for a particular user name.

              Arguments: u - the name of the user

              Return Value: the id of the user

        Sample usage:
            uid = nto1.getUserId  self.user
            uid = nto1.getUserId  admin
        """

        print("Getting user id for [" + u + "]...")
        # Retrieve the list of all users
        # NOTE: this is occasionally returning a "bad status line" error so adding some redundancy
        found = False
        for i in range(5):
            try:
                r = self.sess.get('https://%s:%s/api/users' % (self.ipaddr, self.ipport), verify=False)
                if r.status_code != 200:
                    raise Exception('ERROR: Unable to retrieve users.')
            except:
                print("Attempt to retrieve users failed. Retrying...")
                time.sleep(1)
            else:
                found = True
                break

        if found == False:
            raise Exception('ERROR: Unable to retrieve list of users. User name not found.')

        # Convert json to a dictionary for parsing
        d = json.loads(r.text)
        # Search the dictionary for the desired user
        # NOTE: the dictionary should never be empty as at least the admin account will exist
        found = False
        for i in d:
            if i['name'] == u:
                found = True
                uid = i['id']
                break
        if found == False:
            raise Exception('ERROR: User name not found.')

        print("The user id of [" + u + "] is [" + str(uid) + "].")
        return uid

    def getUserConfig(self, u):
        print("Retrieving user config...")
        r = self.sess.get('https://%s:%s/api/file/config/%s' % (self.ipaddr, self.ipport, u), verify=False)
        if r.status_code != 200:
            # A user that has just been created has no config file until they login for the first time.
            # In this case, we can create the config on the fly so ignore the error response.
            if (r.status_code == 404) and ("Not Found" in r.text):
                print("WARNING: User config does not exist...attempting to create it...")
                d = json.loads('{"confirm_options":{"confirm_login":"NEVER","license_agreement_accepted":true},'+
                               '"show_getting_started":false}')
            else:
                print("Status code: " + str(r.status_code))
                print("Response: " + r.text)
                raise Exception('ERROR: Unable to retrieve user config.')
        else:
            # Convert the json to a dictionary for parsing
            d = json.loads(r.text)
        # -------------------------------------------------
        # For debugging only - Print original user config for informational purposes
        # print("DEBUG: User config before changes:")
        # pprint(d)
        # -------------------------------------------------
        return d


    def disableLoginDialogs(self, u):
        """ Description: This command disables the confirmation dialogs displayed on login.

              Arguments: u - the user name for which to disable the dialogs

              Return Value: N/A

        Sample usage:
            nto1.disableLoginDialogs  ${user}
            nto1.disableLoginDialogs  admin
        """

        print("Disabling login dialogs...")
        # Get the user id of the user for inclusion in the subsequent request
        uid = self.getUserId(u)
        # Pull the user config from the NTO
        print("Retrieving user config...")
        r = self.sess.get('https://%s:%s/api/file/config/%s' % (self.ipaddr, self.ipport, u), verify=False)
        if r.status_code != 200:
            # A user that has just been created has no config file until they login for the first time.
            # In this case, we can create the config on the fly so ignore the error response.
            if (r.status_code == 404) and ("Not Found" in r.text):
                print("WARNING: User config does not exist...attempting to create it...")
                d = json.loads('{"confirm_options":{"confirm_login":"NEVER","license_agreement_accepted":true},'+
                               '"show_getting_started":false}')
            else:
                print("Status code: " + str(r.status_code))
                print("Response: " + r.text)
                raise Exception('ERROR: Unable to retrieve user config.')
        else:
            # Convert the json to a dictionary for parsing
            d = json.loads(r.text)

        # -------------------------------------------------
        # For debugging only - Print original user config for informational purposes
        # print("DEBUG: User config before changes:")
        # pprint(d)
        # -------------------------------------------------

        # Disable "last login" dialog
        d['confirm_options']['confirm_login'] = 'NEVER'
        # Disable "EULA" dialog
        d['confirm_options']['license_agreement_accepted'] = True
        # Disable "getting started" dialog
        d['show_getting_started'] = False

        # Create dictionary for posting back to server
        # NOTE: The content portion of the post must be a string and not a json object
        s = json.dumps(d)
        j = {'userid': u, 'content': s}

        # Tell NTO to update the user config with the options disabled
        print("Uploading modified user config...")
        r = self.sess.post('https://%s:%s/api/file/config/upload' % (self.ipaddr, self.ipport),
                           headers={'content-type': 'application/json'}, data=json.dumps(j), verify=False)
        if r.status_code != 200:
            print("Status code: " + str(r.status_code))
            print("Response: " + r.text)
            raise Exception('ERROR: Unable to upload user config.')

        # -------------------------------------------------
        # For debugging only - Pull user config and print to confirm changes were successful
        # print("Pulling thet user config to validate that changes were successfully applied...")
        # r = self.sess.get('https://%s:%s/api/file/config/%s' % (self.ipaddr, self.ipport, str(u)), verify=False)
        # if r.status_code != 200:
        #    print("Status code: " + str(r.status_code))
        #    print("Response: " + r.text)
        #    raise Exception('ERROR: Unable to retrieve user config.')
        # Convert the json to a dictionary for parsing
        # d = json.loads(r.text)
        # print("User config after changes:")
        # pprint(d)
        # -------------------------------------------------

        print("Login dialogs disabled successfully.")

    def getPort(self, pn):
        found = False
        for p in self.ports:
            if p.name == pn:
                return p

        raise Exception("No port with name ["+pn+"] found.")


    def deleteNTPservers(self):
        """ Description:  ReST method used to clear the NTO's NTP server list.

            Arguments: N/A

            Return Value:
                * r.status_code - ReST status code response """
        print('Issue ReST to put the stats polling interval ...')
        ntp_server_list = {'ntp_server_list': {'enabled':True,'servers':[]}}
        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps(ntp_server_list), verify=False)
        if r.status_code != 200:
            print("ERROR: %s" % (r.text))
            raise Exception('ERROR: Unable to clear the ntp_server_list on NTO (%s): [status code = %s]' % (self.ipaddr, r.status_code))
        return (r.status_code)

    def setAUTHmode(self, mode):
        """ Description:  ReST method used to set the NTO's stats polling interval.

            Arguments:
                * mode authentication_mode:  LOCAL , RADIUS or TACACS

            Return Value:
                * r.status_code - ReST status code response """
        print('Issue ReST to put the stats polling interval ...')
        auth_mode = {'authentication_mode': mode}
        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps(auth_mode), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to set AUTH mode on NTO (%s) to secs: [status code = %s]' % (self.ipaddr, r.status_code))
        return (r.status_code)

    def snmp_config(self):
        """ Description:  ReST method used to set the NTO's stats polling interval.

            Arguments:
                * statsPollInt - statistics polling interval (in seconds)

            Return Value:
                * r.status_code - ReST status code response """
        print('Issue ReST to put snmp config ...')
        snmp_config = {'snmp_config': {}}
        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps(snmp_config), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to set the snmp_config on NTO (%s) to secs: [status code = %s]' % (self.ipaddr, r.status_code))
        return (r.status_code)


    def userExists(self, uName):
        print("Checking for existence of user [" + uName + "]...")
        if (len(self.users) == 0):
            self.getUsers()

        found = False
        for i in range(0,len(self.users)):
            if uName == self.users[i]['name']:
                found = True
                break

        print("User existence is " + str(found))
        return found

    def deleteUser(self, uName, failIfDoesNotExist=False):
        print("Deleting user [" + uName + "]...")
        ue = self.userExists(uName)
        if (ue == False):
            if (failIfDoesNotExist == False):
                print("User does not exist..continuing...")
                return
            else:
                raise Exception("ERROR: Attempted to delete non-existent user: ["+uName+"]...")

        r = self.sess.delete('https://%s:%s/api/users/%s' % (self.ipaddr, self.ipport, uName), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to delete user (%s): [status code = %s]' % (uName, r.status_code))

        # update internal users list to pick up new user and id
        #self.getUsers()
        self.users = [d for d in self.users if d['name'] != uName]
        print("User ("+ uName + ") deleted successfully.")


    def createUser(self, uName, isAdmin=False, deleteFirst=False, reuseIfExists=False):
        print("Creating user [" + uName + "]...")
        # See if user already exists
        ue = self.userExists(uName)

        # If desired, delete old user before creating the new one
        if (ue and deleteFirst):
            self.deleteUser(uName)
            ue = False

        # If desired, simply reuse the existing user with the same name
        if (ue and reuseIfExists):
            return

        # If reuse was not desired but the user already exists then return an error
        if (ue and not reuseIfExists):
            raise Exception("ERROR: Cannot create user...user ["+uName+"] already exists.")

        # Otherwise, create a new user and return
        u = NTOUser(uName, isAdmin)
        attrs = u.getAttrsForCreate()
        r = self.sess.post('https://%s:%s/api/users' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps(attrs), verify=False)
        if r.status_code != 200:
            print("Failed to create user.  Received error ("+str(r.status_code)+"): " + r.text)
            raise Exception('ERROR: Unable to create user (%s): [status code = %s]' % (uName, r.status_code))

        # update internal users list to pick up new user and id
        #self.getUsers()
        resp = json.loads(r.text)
        self.users.append({'id' : resp['id'], 'name' : uName})
        print("User ("+ uName + ") created successfully.")


    def groupExists(self, gName):
        print("Checking for existence of group [" + gName + "]...")
        if (len(self.userGrps) == 0):
            self.getGroups()

        found = False
        for i in range(0, len(self.userGrps)):
            if gName == self.userGrps[i]['name']:
                found = True
                break

        print("Group existence is " + str(found))
        return found


    def deleteGroup(self, gName, failIfDoesNotExist=False):
        print("Deleting group [" + gName + "]...")
        ge = self.groupExists(gName)
        if (ge == False):
            if (failIfDoesNotExist == False):
                print("Group does not exist..continuing...")
                return
            else:
                raise Exception("ERROR: Attempted to delete non-existent group: [" + gName + "]...")

        r = self.sess.delete('https://%s:%s/api/groups/%s' % (self.ipaddr, self.ipport, gName), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to delete group (%s): [status code = %s]' % (gName, r.status_code))

        # update internal user groups list to pick up new group and id
        self.userGrps = [d for d in self.userGrps if d['name'] != gName]
        print("Group ("+ gName + ") deleted successfully.")


    def createGroup(self, gName, deleteFirst=False, reuseIfExists=False):
        print("Creating group [" + gName + "]...")
        # See if group already exists
        ge = self.groupExists(gName)

        # If desired, delete old group before creating the new one
        if (ge and deleteFirst):
            self.deleteGroup(gName)
            ge = False

        # If desired, simply reuse the existing group with the same name
        if (ge and reuseIfExists):
            return

        # If reuse was not desired but the user already exists then return an error
        if (ge and not reuseIfExists):
            raise Exception("ERROR: Cannot create group...group [" + gName + "] already exists.")

        # Otherwise, create a new group and return
        g = NTOUserGroup(gName)
        attrs = g.getAttrsForCreate()
        r = self.sess.post('https://%s:%s/api/groups' % (self.ipaddr, self.ipport),
                           headers={'content-type': 'application/json'}, data=json.dumps(attrs), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to create group (%s): [status code = %s]' % (gName, r.status_code))

        # update internal user groups list to pick up new group and id
        resp = json.loads(r.text)
        self.userGrps.append({'id': resp['id'], 'name': gName})
        print("Group ("+ gName + ") created successfully.")


    def addUserToGroup(self, uName, gName):
        print("Adding user [" + uName + "] to group [" + gName + "]...")
        # Confirm user and group already exist
        if not self.userExists(uName):
            raise Exception("User (" + uName + ") does not exist.")
        if not self.groupExists(gName):
            raise Exception("Group (" + gName + ") does not exist.")

        g = NTOUserGroup(gName)
        gd = self.getGroupDetails(gName)
        g.update(gd)
        g.addUser(uName)
        self.updateGroup(gName, g.getAttrsForUpdate())

    def getMonitorsInfo(self, monitor_name):
        """ Description:  ReST method used to query the groups on the NTO.

            Arguments: n/a

            Return Value: n/a
        """
        print ('Retrieving monitors from NTO...')
        monitor_name = str(monitor_name)
        r = self.sess.get("https://%s:%s/%s/%s" % (self.ipaddr, self.ipport, self.urls['api/monitors'], monitor_name), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to get monitors on NTO chassis (%s) [status code = %s]' % (self.ipaddr, r.status_code))
        return json.loads(r.text)

    def clearConfig(self):
        """ Description:  ReST method used to clear all ports and filters on a NTO.

            Arguments: n/a

            Return Value:
                * r.status_code - status from ReST post request """
        print('Issue ReST post request to clear config on a chassis level...')
        r = self.sess.post('https://%s:%s/api/actions/clear_config' % (self.ipaddr, self.ipport), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to clear config on NTO chassis (%s) [status code = %s]' % (self.ipaddr, r.status_code))
        return (r.status_code)

    def rediscoverIds(self):
        """ Description:  Method to rediscoverID for each AFMR, NP, DF, TP, and/or Port Group for one or more connections contained in the NTO 'conns' list.

            Arguments: n/a

            Return Value: n/a """

        for afmr in self.afmrs:
            afmr.setAFMResourceId()

        for myConn in self.conns:

            # Iterate over the network objects.
            myList = []
            if type(myConn.network) is list:
                myList.extend(myConn.network)
            else:
                myList.append(myConn.network)

            for myObj in myList:
                if myObj is not None and myObj != 'None':
                    if isinstance(myObj, NTOPortGroup):
                        for myPort in myObj.ports:
                            myPort.rediscoverPortId()
                        myObj.rediscoverPortGrpId()
                    if isinstance(myObj, NTOPort):
                        myObj.rediscoverPortId()

            myList = []
            if type(myConn.tool) is list:
                myList.extend(myConn.tool)
            else:
                myList.append(myConn.tool)

            for myObj in myList:
                if myObj is not None and myObj != 'None':
                    if isinstance(myObj, NTOPortGroup):
                        for myPort in myObj.ports:
                            if isinstance(myPort, NTOVirtPort):
                                myPort.rediscoverVirtPortId()
                            else:
                                myPort.rediscoverPortId()
                        myObj.rediscoverPortGrpId()
                    elif isinstance(myObj, NTOVirtPort):
                        myObj.rediscoverVirtPortId()
                    elif isinstance(myObj, NTOPort):
                        myObj.rediscoverPortId()

            myList = []
            if type(myConn.filter) is list:
                myList.extend(myConn.filter)
            else:
                myList.append(myConn.filter)

            for myObj in myList:
                if myObj is not None and myObj != 'None':
                    if isinstance(myObj, NTOFilter):
                        myObj.rediscoverFilterId()

    def checkNtoIsUp(self, upStatusRetries, lineCardsRetries, ntoIP=None):
        """ Description:  Method to check if a NTO is up

            Arguments:
                * upStatusRetries - int, Retries until NTO is up (1 retry/ 1 second)
                * lineCardsRetries -Retries until NTO LC are up (1 retry/10 second)
            Return Value: n/a  """
        if ntoIP is None:
            targetIP = self.ipaddr
        else:
            targetIP = ntoIP
        for sec in range(upStatusRetries):
            if sec == upStatusRetries-1:
                raise Exception("ERROR:NTO is not up since %s seconds.." % upStatusRetries )
            try:
                r = self.sess.get('https://%s:%s/api/system' % (targetIP, self.ipport), verify=False)
                if r.status_code != 200:
                    print("NTO stack on %s is not up yet..." % targetIP)
                    sys.stdout.flush()
                    time.sleep(1)
                    continue
                else:
                    systemDict = json.loads(r.text)
                    time.sleep(5)
                    print("NTO %s is up..." % targetIP)
                    break
            except requests.ConnectionError:
                print("NTO %s is not up yet..." % targetIP)
                sys.stdout.flush()
                time.sleep(1)
                continue

        if systemDict['type'] == '7300':
            for attempt in range(lineCardsRetries):
                if attempt == lineCardsRetries - 1:
                    raise Exception("ERROR:One of the NTO LC is not up since %s seconds.." % lineCardsRetries * 10)
                try:
                    r = self.sess.get('https://%s:%s/api/line_boards' % (targetIP, self.ipport), verify=False)
                    if r.status_code != 200:
                        time.sleep(10)
                        continue
                except requests.ConnectionError:
                    time.sleep(10)
                    continue
                lineBoardsList = json.loads(r.text)
                for lineBoard in lineBoardsList:
                    if lineBoard['line_board_status'] in ['PRESENT', 'NOT_PRESENT', 'READY']:
                        flag = "whileBreak"
                        print(lineBoard['id'], lineBoard['name'], lineBoard['line_board_status'])
                        continue
                    elif lineBoard['line_board_status'] in ['INITIALIZING', 'UPGRADING']:
                        flag = "whileCont"
                        print(lineBoard['id'], lineBoard['name'], lineBoard['line_board_status'])
                        break
                    elif lineBoard['line_board_status'] in ['FAULTY']:
                        print(lineBoard['id'], lineBoard['name'], lineBoard['line_board_status'])
                        raise Exception("ERROR:One of the NTO LC is FAULTY..")
                if flag == "whileCont":
                    time.sleep(5)
                    print("Line Cards on %s are not READY yet..." % targetIP)
                    continue
                elif flag == "whileBreak":
                    time.sleep(5)
                    print("Line Cards on %s are READY..." % targetIP)
                    break

    def restartNto(self, restartTimeout=60, upStatusRetries=900, lineCardsRetries=90):
        """ Description:  ReST method used to restart a NTO.

            Arguments:
                * restartTimeout - NTO timeout until restarts actuallly happens
                * upStatusRetries - Retries until NTO is up (1 retry/ 1 second)
                * lineCardsRetries -Retries until NTO LC are up (1 retry/10 second)

            Return Value: n/a  """

        restartTimeout = int(restartTimeout)
        upStatusRetries = int(upStatusRetries)
        lineCardsRetries = int(lineCardsRetries)

        print('Issue ReST post request to restart the NTO...')
        r = self.sess.post('https://%s:%s/api/actions/restart' % (self.ipaddr, self.ipport), verify=False)
        if r.status_code != 202:
            raise Exception('ERROR: Unable to restart NTO chassis (%s) [status code = %s]' % (self.ipaddr, r.status_code))

        #Nto timeout until restart actually happens
        time.sleep(restartTimeout)

        self.checkNtoIsUp(upStatusRetries, lineCardsRetries)



    def clearSystem(self, restartTimeout=60, upStatusRetries=900, lineCardsRetries=90):
        """ Description:  ReST method used to clear system a NTO.

            Arguments:
                * restartTimeout - NTO timeout until restarts actuallly happens
                * upStatusRetries - Retries until NTO is up (1 retry/ 1 second)
                * lineCardsRetries - Retries until NTO LC are up (1 retry/10 second)

            Return Value: n/a  """

        restartTimeout = int(restartTimeout)
        upStatusRetries = int(upStatusRetries)
        lineCardsRetries = int(lineCardsRetries)

        print('Issue ReST post request to restart the NTO...')
        r = self.sess.post('https://%s:%s/api/actions/clear_system' % (self.ipaddr, self.ipport), verify=False)
        if r.status_code != 202:
            raise Exception('ERROR: Unable to clear system NTO chassis (%s) [status code = %s]' % (self.ipaddr, r.status_code))
        #Nto timeout until restart actually happens
        time.sleep(restartTimeout)

        self.checkNtoIsUp(upStatusRetries, lineCardsRetries)

    def changeQSFPMode(self, lineBoardName=None):
        """ Description:  ReST method change QSFP mode.

                    Arguments:
                        * lineBoardName - name of the desired LC for 7300 system

                    Return Value: n/a  """

        r = self.sess.get('https://%s:%s/api/system' % (self.ipaddr, self.ipport), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to get system NTO info (%s) [status code = %s]' % (self.ipaddr, r.status_code))
        systemDict = json.loads(r.text)

        if systemDict['type'] == '7300':
            if lineBoardName is not None:
                payload={}
                r = self.sess.put('https://%s:%s/api/line_boards/%s/switch_mode' % (self.ipaddr, self.ipport, lineBoardName), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
                if r.status_code != 200:
                    raise Exception('ERROR: Unable to change QSFP mode on NTO (%s) Line Board(%s): [status code = %s]' % (self.ipaddr, lineBoardName ,r.status_code))
            elif lineBoardName is None:
                raise Exception('Specify lineBoard name...')
        else:
            if lineBoardName is None:
                r = self.sess.post('https://%s:%s/api/actions/change_qsfp_mode' % (self.ipaddr, self.ipport),headers={'content-type': 'application/json'}, verify=False)
                if r.status_code != 200:
                    raise Exception('ERROR: Unable to change QSFP mode on NTO (%s): [status code = %s]' % (self.ipaddr, r.status_code))
            elif lineBoardName is not None:
                raise Exception(self.getSystemModel() + ' does not have lineBoards')

    def getTimeStampSource(self):
        """ Description:  ReST method to get the current NTO Time Stamp Source.

            Arguments: n/a

            Return Value: n/a  """
        timeSrc = self.getSystem('timestamp_config')['time_source']
        print('NTO %s timestamp time source = %s' % (self.name, timeSrc.lower()))
        return(timeSrc.lower())

    def setTimeStampSource(self, source=None):
        """ Description:  ReST method to set the NTO Time Stamp Source.

            Arguments:
                * source - (default=None) specified time source 'LOCAL','NTP','GPS', '1588', or 'holdover'

            Return Value: n/a  """
        if source is None:
            self.timeStampSrc = self.getTimeStampSource()
        else:
            payload = {'timestamp_config': {'time_source': source}}
            r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'},data=json.dumps(payload), verify=False)
            if r.status_code != 200:
                raise Exception('ERROR: Unable to set TimeStamp Source %s on NTO (%s): [status code = %s] \n\n [NTO Exception: %s]' % (source ,self.ipaddr, r.status_code, r.text))
            else:
                self.timeStampSrc = source.lower()

    def setNtpServers(self, serverIpDn=None, enabled=True, checkSyncRetries=None):

        """ Description:  ReST method to set TimeStamp Source.

            Arguments:
                * serverIpDn - Ip/DomainName string or List of IP/DomainName strings
                * enabled  - Boolean True or False
                * checkSyncRetries  - number of retries to check sync (1 retry / 1 second)

            Return Value: n/a  """

        serverList = []
        if serverIpDn is not None:
            if type(serverIpDn) is list:
                serverList.extend(serverIpDn)
            else:
                serverList.append(serverIpDn)
        payload = {"ntp_server_list": {"enabled": enabled,"servers": serverList}}
        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport),
                          headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code == 200:
            self.ntpState = enabled
        elif r.status_code != 200:
            raise Exception(
                'ERROR: Unable to set Ntp Server %s on NTO (%s): [status code = %s] \n\n [NTO Exception: %s]' % (
                    serverIpDn, self.ipaddr, r.status_code, r.text))

        if checkSyncRetries is not None:
            checkSyncRetries = int(checkSyncRetries)

            for sec in range(checkSyncRetries):
                if sec == checkSyncRetries - 1:
                    raise Exception('ERROR:Ntp Server %s did not sync with  NTO (%s)' % (serverIpDn, self.ipaddr,))
                r = self.sess.get('https://%s:%s/api/system' % (self.ipaddr, self.ipport), verify=False)
                if r.status_code != 200:
                    raise Exception(
                        'ERROR: Unable to get Ntp Server syncStatus %s on NTO (%s): [status code = %s] \n\n [NTO Exception: %s]' % (
                            serverIpDn, self.ipaddr, r.status_code, r.text))
                else:
                    syncStatus = json.loads(r.text)['ntp_server_status']

                if syncStatus not in ['"IN_SYNC"']:
                    print("NTP server %s NOT in sync, syncStatus=%s" % (serverIpDn, syncStatus))
                    time.sleep(1)
                    continue
                else:
                    print("NTP server %s in sync, syncStatus=%s" % (serverIpDn, syncStatus))
                    break

    def getSystemTime(self):

        r = self.sess.get('https://%s:%s/api/system' % (self.ipaddr, self.ipport), verify=False)
        if r.status_code != 200:
            raise Exception(
                'ERROR: Unable to get date on NTO (%s): [status code = %s] \n\n [NTO Exception: %s]' %
                (self.ipaddr,r.status_code, r.text))

        return (json.loads(r.text)['system_time'])

    def createIFC(self, roleCheck=True):
        """ Description:  REST method used to create an IFC topology on the current NTO.

            Arguments:
                * roleCheck - (Default = True) Boolean which indicates if current role for the NTO should be checked before creating an IFC topology on current NTO.

            Return Value: n/a """

        if roleCheck == True:
            role = self.getSystem('system_role')
            if role != 'INDEPENDENT':
                raise Exception ('Error: Current NTO is not in standalone mode. '
                                 'Creating an IFC topology on NTO %s is not possible' % self.ipaddr)
        if self.getSystem('ntp_server_list')['enabled'] != True:
            raise Exception('Error: NTO %s doesn\'t have a valid NTP server configuration. An IFC cluster can\'t ve created on this NTO' % self.ipaddr)

        print('Creating an IFC topology on NTO %s' % self.ipaddr)
        payload = {}
        r = self.sess.post('https://%s:%s/api/cte_operations/create_topology' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False )
        if r.status_code != 200:
            raise Exception('ERROR: Unable to create an IFC NTO chassis (%s) [status code = %s] - %s' % ( self.ipaddr, r.status_code, r.text))

    def joinIFC(self, clusterIpAddr, roleCheck=True):
        """ Description:  REST method used to join an existing IFC topology with the current NTO.

            Arguments:

                * clusterIpAddr - Mandatory IP address (string) of the cluster to be joined
                * roleCheck - (Default = True) Boolean which indicates if current role for the NTO should be checked before creating an IFC topology on current NTO.

            Return Value: n/a """

        if roleCheck == True:
            role = self.getSystem('system_role')
            if role != 'INDEPENDENT':
                raise Exception ('Error: Current NTO is not in standalone mode. '
                                 'Joining an IFC topology with NTO %s is not possible' % self.ipaddr)
        if self.getSystem('ntp_server_list')['enabled'] != True:
            raise Exception('Error: NTO %s doesn\'t have a valid NTP server configuration. Current NTO can\'t join an IFC cluster' % self.ipaddr)

        print('Joining %s IFC topology with NTO %s' % (clusterIpAddr, self.ipaddr))
        payload = {'remote_device_ip': clusterIpAddr}
        r = self.sess.post('https://%s:%s/api/cte_operations/join_topology' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False )
        if r.status_code != 200:
            raise Exception('ERROR: Unable to join the %s IFC topology with NTO chassis (%s) [status code = %s] - %s' % (clusterIpAddr, self.ipaddr, r.status_code, r.text))

    def setSyslogServers(self, configInfo):

        """ Description:  ReST method to set Syslog Server.

            Arguments:
                *  configList argument is a list of dictionary containing the following format
                *    "host": "10.215.156.176",
                *    "port": 20514,
                *    "facility": "LOCAL0",
                *    "tls_enabled": False
            Example of usage (*.robot):
            ${syslogServersList}=  catenate   [{'host':'10.0.0.1', 'port': 514}, {'host': '10.0.0.4', 'port': 514, 'facility':'LOCAL2'}, {'host': '10.0.0.6', 'port': 10514, 'facility':'LOCAL1', 'tls_enabled':False}, {'port': 614}]
            nto1.setSyslogServers  ${syslogServersList}

            Return Value:   """

        payload = {"syslog_server_list": []}
        configList = eval(configInfo)
        for configDict in configList:
            # Define Default Values
            print(type(configList))
            print(configList)
            print(type(configDict))
            print(configDict)
            if 'facility' not in configDict.keys():
                configDict['facility'] = "LOCAL0"
            if 'host' not in configDict.keys():
                configDict['host'] = socket.gethostbyaddr(socket.gethostname())[2][0]
            if 'port' not in configDict.keys():
                configDict['port'] = 514
            if 'tls_enabled' not in configDict.keys():
                configDict['tls_enabled'] = False
            # Append dictionary to list
            payload['syslog_server_list'].append(configDict)

        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport),
                        headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to set Syslog Server List %s on NTO (%s): [status code = %s] \n\n [NTO Exception: %s]' \
                            % (payload, self.ipaddr, r.status_code, r.text))

    def initAllVMPorts(self):
        """ Description:  Helper method that calls initVM()

            Arguments: n/a

            Return Value: n/a """
        self.initAllVMs()

    def initAllVMs(self):
        """ Description:  Helper method that connect to each VM port (if VMPortObj present) for one or more ports in the NTO 'ports' list.

            Arguments: n/a

            Return Value: n/a """
        myVMList = []
        for myPort in self.ports:
            if myPort.VMPortObj is not None and myPort.VMPortObj.VMIPAddr not in myVMList:
                myVMList.append(myPort.VMPortObj.VMIPAddr)
                myPort.VMPortObj.loginToVM()

    def installSoftware(self, filePath, ntoIP=None):
        """ Description:  Method to install a software image on NTO

            Arguments: filePath , string, indicating the location of software image file

            Return Value: n/a"""
        if ntoIP is None:
            targetIP = self.ipaddr
        else:
            targetIP = ntoIP
        print('Installing software image from: %s' % (os.path.basename(filePath)))
        payload = {}
        multipart = MultipartEncoder(
            fields={'field0': (os.path.basename(filePath), open(filePath, 'rb'), 'application/octet-stream'),
                    'field1': ("json", json.dumps(payload), 'application/json')})
        r = self.sess.post('https://%s:%s/api/actions/install_software' % (targetIP, self.ipport), verify=False,
                           data=multipart, headers={'Content-Type': multipart.content_type})
        if r.status_code != 200:
            raise Exception('ERROR: Unable to install_software on (%s) [status code = %s].\n Server response: %s'
                % (targetIP, r.status_code, r.text))

        print("WebAPI call for installing %s on %s was successful.\n The system will reboot..." % (filePath, targetIP))
        # self.checkNtoIsUp(upStatusRetries=900, lineCardsRetries=90)
        # self.getSystemSWVersion()

    def removeFilterFromNtoObj(self, filterObj):
        """ Description:  Helper method to add a filter to the NTO class instance list 'filters' while checking for duplicate names.

            Arguments:
                * filterObj - object containing NTO filter specific information

            Return Value: n/a """
        #Check for duplicate filter names.
        duplicate = [filter.name for filter in self.filters if filter.name == filterObj.name]
        if duplicate:
            self.filters.remove(filterObj)
        else:
            raise Exception('ERROR:  Filter is not exist in current NTO list!')

    def removeConnFromNtoObj(self, connObj):
        """ Description:  Helper method to add a connection to the NTO class instance list 'conns' while checking for duplicate names.

            Arguments:
                * connObj - object containing connection specific information

            Return Value: n/a """
        #Check for duplicate connection names.
        duplicate = [conn.connName for conn in self.conns if conn.connName == connObj.connName]
        if duplicate:
            self.conns.remove(connObj)
        else:
            raise Exception('ERROR:  Connection is not exist in current NTO list!')



    def selectMySoftwareFile(self, folder):
        """ Description:  Method to identify the correct software image file for the current hardware platform
            It only searches for files in the provided folder, whithout looking into sub-folders
             Arguments:
                  *folder, string , the path to the folder where the new image files are located.

             Return Value: myImageFile, string, indicating path to the software image file """
        fileList = []
        myImageFile = ''
        matchStrings = {
            '7300' : '.+73xx-62xx[-]\d{6}[-]\d{8}[-]\d{6}\.zip$',
            'VISION_ONE' : '.+73xx-62xx[-]\d{6}[-]\d{8}[-]\d{6}\.zip$',
            'VISION_E100' : '.+VisionEdge[-]\d{6}[-]\d{8}[-]\d{6}\.zip$',
            'VISION_E40' : '.+VisionEdge[-]\d{6}[-]\d{8}[-]\d{6}\.zip$'
        }
        model = self.getSystem(property='type')
        for item in os.listdir(folder):
            if os.path.isfile(os.path.join(folder, item)) and re.match(matchStrings[model], item):
                myImageFile = os.path.join(folder, item)
        if myImageFile == '':
            raise Exception('ERROR: Software image needed for NTO %s is not present in the provided list'
                            % self.ipaddr)
        print('Selected file for %s is %s' % (self.ipaddr, myImageFile))
        return myImageFile

    def installMySoftwareFile(self, folder):
        """
        Description: Helper method to install a software image on a NTO after selecting the correct one from a folder
        Arguments:
                  *folder, string , the path to the folder where the new image files are located.

        Return Value: N/A
        """
        image = self.selectMySoftwareFile(folder)
        self.installSoftware(image)

    def configureMemoryAllocation(self, custom=None, dynamic=None, dynamicSip=None, filterBuildMode=None, priorityPortIdList=[],
                                  network=None, networkDynamicSipAllocationMix=None, tool=None, default=False):
        """
               Description: Helper method to configure filter memory allocation

               Arguments:
                   * custom : "CUSTOM_NONE", "CUSTOM_16_BYTE", "CUSTOM_32_BYTE"
                   * dynamic: "DISABLED" , "IPV4_000_IPV6_100", etc Please see Webapi doc from NTO
                   * dynamicSip: "DISABLED" , "IPV4_000_IPV6_100", etc Please see Webapi doc from NTO
                   * filterBuildMode: "INTERSECTION", "MIXED", "PRIORITY"
                   * priorityPortIdList : list of port IDs
                   * network: "DISABLED" , "IPV4_000_IPV6_100", etc Please see Webapi doc from NTO
                   * networkDynamicSipAllocationMix: "NP_000_DSIP_100", "NP_025_DSIP_075", "NP_050_DSIP_050", "NP_050_VRF_050"
                   * tool: "DISABLED" , "IPV4_000_IPV6_100", etc Please see Webapi doc from NTO
               Return Value: Rest Response
               """
        sysDict = self.getSystem()['memory_allocation']
        payload = {}

        if default is False:

            if custom is not None: sysDict['custom'] = custom

            if dynamic is not None: sysDict['dynamic'] = dynamic

            if dynamicSip is not None: sysDict['dynamic_sip'] = dynamicSip

            if filterBuildMode is not None: sysDict['filter_build_settings']['filter_build_mode'] = filterBuildMode

            if priorityPortIdList is not None: sysDict['filter_build_settings']['priority_port_id_list'] = priorityPortIdList

            if network is not None: sysDict['network'] = network

            if networkDynamicSipAllocationMix is not None: sysDict['network_dynamic_sip_allocation_mix'] = networkDynamicSipAllocationMix

            if tool is not None: sysDict['tool'] = tool

        elif default is True:
            sysDict =  {
                "custom": "CUSTOM_NONE",
                "dynamic": "L2L3L4_11_L2L3L4_NOMAC_89",
                "dynamic_sip": "IPV4_100_IPV6_000",
                "filter_build_settings": {
                    "filter_build_mode": "INTERSECTION",
                    "priority_port_id_list": []
                },
                "network": "L2L3L4_100",
                "network_dynamic_sip_allocation_mix": "NP_025_DSIP_075",
                "tool": "L2L3L4_100"}

        payload['memory_allocation'] = sysDict

        print(payload)

        r = self.sess.put('https://%s:%s/%s' % (self.ipaddr, self.ipport, 'api/system'), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        print(r.status_code)
        if r.status_code != 200:
            print(r.text)
            raise Exception("ERROR: An error occurred while configuring FMA (%s)" % sysDict)
        print(r.text)
        return (r)

    def filterPriority(self, source, filterList):
        """
                     Description: Helper method to configure filter memory allocation

                     Arguments:
                         * source : NETWORK source object
                         * filterList: List with filter python Obj priority in order, first index is highest priority
                                       (python obj elemets)

                     Return Value: N/A
                     """

        if isinstance(source, NTOPort): source = source.id
        elif isinstance(source,NTOPortGroup): source = source.portGrpId

        payload = {"prioritized_dest_filter_id_list" : [obj.id for obj in filterList], "source_port_id" : source}

        r = self.sess.post('https://%s:%s/%s' % (self.ipaddr, self.ipport, 'api/actions/change_filter_priority'), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)

        if r.status_code != 200:
            print(r.text)
            raise Exception("ERROR: An error occurred while filter priority for network side (%s)" % source)
        print(r.text)
        return (r)

    def getMemoryAlloc(self):

        """ Description:  ReST method used to query the NTO system info.

            Arguments: n/a

            Return Value:
                * dictionary of system info in r.text which is the ReST response string """

        t1 = self.getSystem("memory_allocation")
        t2 = self.getSystem("dynamic_filter_memory_allocation")
        t3 = self.getSystem("network_filter_memory_allocation")
        self.memAlloc = {'memory_allocation': t1}
        self.dynamicMemAlloc = {'dynamic_filter_memory_allocation': t2}
        self.networkMemAlloc = {'network_filter_memory_allocation': t3}


    def setMemoryAlloc(self):

        """ Description:  ReST method used to set memory allocation parameters.

            Arguments:

            Return Value:
                * r.status_code - ReST status code response """

        print('Issue ReST to put the stats polling interval ...')
        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps(self.memAlloc), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to set AUTH mode on NTO (%s) to secs: [status code = %s]' % (self.ipaddr, r.status_code))
        return (r.status_code)

        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps(self.dynamicMemAlloc), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to set AUTH mode on NTO (%s) to secs: [status code = %s]' % (self.ipaddr, r.status_code))
        return (r.status_code)

        r = self.sess.put('https://%s:%s/api/system' % (self.ipaddr, self.ipport), headers={'content-type': 'application/json'}, data=json.dumps(self.networkMemAlloc), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to set AUTH mode on NTO (%s) to secs: [status code = %s]' % (self.ipaddr, r.status_code))
        return (r.status_code)
