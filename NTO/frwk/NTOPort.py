import json
import time
import re
from pprint import pprint

class NTOPort:

    """This library handles the ports for an NTO within the RobotFramework.  It supports both network and tool port modes.

       Parent Objects:

       * myNTO - NTO parent object.

       Child Objects:

       N/A.
    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    #Define the default link speeds dict based on the media type.
    linkSpeeds = {'QSFP28':        '100G_FULL',
                  'QSFP_PLUS_40G': '40G_FULL',
                  'SFP28':         '25G_FULL',
                  'SFP_PLUS_10G':  '10G_FULL',
                  'SFP_1G':        '1G_FULL'}

    qsfp28Modes = {'QSFP28':        'MODE_QSFP28',
                   'QSFP_PLUS_40G': 'MODE_QSFP',
                   'SFP_PLUS_10G':  'MODE_SFP'}

    def __init__(self):
        self.myNTO=None
        self.id=None
        self.defaultName=None
        self.name=None
        self.portKeywords=None
        self.portDescription=None
        self.portMode=None
        self.connType=None
        self.mediaType=None
        self.cardType=None
        self.licenseType=None
        self.enabledState=None
        self.linkSettings=None
        self.linkUpDnTrap=None
        self.xmitLight=None
        self.pktProc=None
        self.enabledPktProc = []
        self.ixPortObj=None
        self.VMPortObj=None
        self.filterMode=None
        self.filterDef=None
        self.portGrpId=None
        self.afmr = None
        self.afmrBandwidth = None
        self.streams = {}
        self.stats = {'NETWORK': {'NP_TOTAL_RX_COUNT_PACKETS': None,
                                      'NP_TOTAL_RX_COUNT_BYTES': None,
                                      'NP_TOTAL_PASS_COUNT_PACKETS': None,
                                      'NP_TOTAL_PASS_COUNT_BYTES': None,
                                      'NP_TOTAL_RX_COUNT_VALID_PACKETS': None,
                                      'NP_TOTAL_RX_COUNT_INVALID_PACKETS': None,
                                      'NP_TOTAL_RX_COUNT_FCS_ERRORS': None,
                                      'NP_TOTAL_RX_COUNT_CRC_ALIGNMENT_ERRORS': None,
                                      'NP_TOTAL_RX_COUNT_FRAGMENTS': None,
                                      'NP_TOTAL_RX_COUNT_ALIGNMENT_ERRORS': None,
                                      'NP_TOTAL_RX_COUNT_SYMBOL_ERRORS': None,
                                      'NP_TOTAL_RX_COUNT_COLLISIONS': None,
                                      'NP_TOTAL_RX_COUNT_RUNTS': None,

                                      'NP_TOTAL_RX_COUNT_FRAMES_TOO_LONG': None,
                                      'NP_TOTAL_FABRIC_PATH_STRIP_COUNT_PACKETS': None,
                                      'NP_TOTAL_VNTAG_STRIP_COUNT_PACKETS': None,
                                      'NP_TOTAL_GTP_STRIP_COUNT_PACKETS': None,
                                      'NP_TOTAL_MPLS_STRIP_COUNT_PACKETS': None,
                                      'NP_TOTAL_DATA_MASKING_COUNT_PACKETS': None,
                                      'NP_TOTAL_TRIM_COUNT_PACKETS': None,
                                      'NP_TOTAL_TRIM_COUNT_BYTES': None},

                          'TOOL':    {'TP_TOTAL_INSP_COUNT_PACKETS': None,
                                      'TP_TOTAL_TX_COUNT_PACKETS': None,
                                      'TP_TOTAL_TX_COUNT_BYTES': None,
                                      'TP_TOTAL_DROP_COUNT_PACKETS': None,
                                      'TP_TOTAL_FABRIC_PATH_STRIP_COUNT_PACKETS': None,
                                      'TP_TOTAL_VNTAG_STRIP_COUNT_PACKETS': None,
                                      'TP_TOTAL_GTP_STRIP_COUNT_PACKETS': None,
                                      'TP_TOTAL_MPLS_STRIP_COUNT_PACKETS': None,
                                      'TP_TOTAL_DATA_MASKING_COUNT_PACKETS':None,
                                      'TP_TOTAL_TRIM_COUNT_PACKETS': None,
                                      'TP_TOTAL_TRIM_COUNT_BYTES': None,
                                      'TP_TOTAL_TRAILER_STRIP_COUNT_PACKETS': None}}

    def reinit(self):
        self.__init__()

    def update(self, n, pInfo):
        print('Updating port [%s] properties on NTO...' % (pInfo['name']))
        self.myNTO = n
        self.name = pInfo['name']
        r = self.myNTO.sess.put('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'],pInfo['name']), headers={'content-type': 'application/json'}, data=json.dumps(pInfo), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to update port (%s) [(%s): %s]' % (self.name, r.status_code, r.text))

    def updateFromNTO(self):
        """ This method will update internal data members with information pulled from the NTO
        """
        print('Updating port [%s] properties from NTO...' % (self.name))
        propDict = self.getPortInfo(self.name)
        for prop in propDict:
            if prop == 'name':
                self.name = propDict[prop]
            elif prop == 'default_name':
                self.defaultName = propDict[prop]
            elif prop == 'id':
                self.id = propDict[prop]
            elif prop == 'keywords':
                self.portKeywords = propDict[prop]
            elif prop == 'description':
                self.portDescription = propDict[prop]
            elif prop == 'mode':
                self.portMode = propDict[prop]
            elif prop == 'media_type':
                self.mediaType = propDict[prop]
            elif prop == 'enabled':
                self.enabledState = propDict[prop]
            elif prop == 'link_settings':
                self.linkSettings = propDict[prop]
            elif prop == 'link_up_down_trap_enabled':
                self.linkUpDnTrap = propDict[prop]
            elif prop == 'tx_light_status':
                self.xmitLight = propDict[prop]
            elif prop == 'filter_mode':
                self.filterMode = propDict[prop]
            elif prop == 'filter_criteria':
                self.filterDef = propDict[prop]
            elif prop == 'port_group_id':
                self.portGrpId = propDict[prop]
#            else:
#                print("Update port: ignoring property [%s]" % (prop))

    def setParent(self, parentObj):
        """ Description:  Helper method used to set the NTO parent object.

            Arguments: n/a

            Return Value: n/a """
        self.myNTO = parentObj

    def setPortId(self, portDict):
        """ Description:  Set the port ID if media_type matches the actual port.

            Arguments:
                * portDict - Sub-dictionary from Resource Manager containing a single port's information.

            Return Value: n/a """

        myPort = portDict['properties']['ID']
        print('Issue ReST request to get port info for %s...' % myPort)
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], myPort), verify=False)
        if r.status_code != 200:
            raise Exception("ERROR: An error occurred while getting info for port (%s): [%s] - %s" % (myPort, r.status_code, r.text))

        myPortInfo = json.loads(r.text)

        #Check for the proper port media type.
        if myPortInfo['media_type'] != portDict['properties']['MediaType']:
            raise Exception('ERROR:  Port %s media_type = %s is not (%s)' % (myPort, myPortInfo['media_type'], portDict['properties']['MediaType']))

        self.id = myPortInfo['id']

        # Check for valid port Id.
        if self.id is None: raise Exception('ERROR: Unable to locate the actual port Id (%s) for NTO (%s)' % (portDict['properties']['port'], self.myNTO.ipaddr))

    def rediscoverPortId(self):
        """ Description:  Helper method used to re-set the Port id class instance variable.

                   Arguments:
                       * n/a

                   Return Value: n/a """
        print('Issue ReST request to rediscover port id for %s...' % self.defaultName)
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.defaultName), verify=False)
        if r.status_code != 200:raise Exception('ERROR: Unable to get the actual port Id (%s) for NTO (%s)' % (self.defaultName, self.myNTO.ipaddr))
        self.id = json.loads(r.text)['id']

    def setPortDefaultName(self, portDict):
        """ Description:  Helper method used to set the port default name class instance variable.

            Arguments:
                * portDict - dictionary from resourceMgr associated with the port's name.

            Return Value: n/a """
        self.defaultName = portDict['properties']['ID']

    def setPortName(self, portName):
        """ Description:  Helper method used to set the port name class instance variable.

            Arguments:
                * portName - User defined name for the port.

            Return Value: n/a """
        self.name = portName

    def setPortKeywords(self, portKeywords):
        """ Description:  Helper method used to set the port keywords class instance variable.

            Arguments:
                * portDict - Sub-dictionary from Resource Manager containing a single port's information.

                Return Value: n/a """
        self.portKeywords = portKeywords

    def setPortDescription(self, portDesc):
        """ Description:  Helper method used to set the port description class instance variable.

            Arguments:
                * portDict - Sub-dictionary from Resource Manager containing a single port's information.

            Return Value: n/a """
        self.portDescription = portDesc

    def setCardType(self, portDict) :
        """ Description:  Helper method used to set the port's card type class instance variable.

            Arguments:
                * portDict - Sub-dictionary from Resource Manager containing a single port's information.

                Return Value: n/a """
        self.cardType = self.getCardType()

    def setLicenseType(self, portDict) :
        """ Description:  Helper method used to set the port's type class instance variable.

            Arguments:
                * portDict - Sub-dictionary from Resource Manager containing a single port's information.

                Return Value: n/a """
        self.licenseType = self.getLicenseType()

    def setPortMode(self, mode):
        """ Description:  Helper method used to set the port mode class instance variable.

            Arguments:
                * mode - User defined mode of port (i.e. NETWORK or TOOL).

                Return Value: n/a """
        self.portMode = mode

    def setConnType(self, portDict):
        """ Description:  Helper method used to set the connection type class instance variable.

            Arguments:
                * portDict - Sub-dictionary from Resource Manager containing a single port's information.

                Return Value: n/a """
        self.connType = portDict['properties']['ConnectionType']

    def setMediaType(self, portDict):
        """ Description:  Helper method used to set the media type class instance variable.

            Arguments:
                * portDict - Sub-dictionary from Resource Manager containing a single port's information.

                Return Value: n/a """
        self.mediaType = portDict['properties']['MediaType']

    def setEnabledState(self, state):
        """ Description:  Helper method used to set the enabled state class instance variable.

            Arguments:
                * state - State of the port.

            Return Value: n/a """
        self.enabledState = state

    def setLinkSettings(self, linkSetting):
        """ Description:  Helper method used to set the link setting class instance variable.

            Arguments:
                * linkSetting - Link setting of the port.

            Return Value: n/a """
        self.linkSettings = linkSetting

    def setLinkUpDnTrap(self, trapState):
        """ Description:  Helper method used to set the trap state class instance variable.

            Arguments:
                * trapState - Trap state of the port.

            Return Value: n/a """
        self.linkUpDnTrap = trapState

    def setXmitLight(self, lightState):
        """ Description:  Helper method used to set the light state class instance variable.

            Arguments:
                * lightState - Light state of the port.

            Return Value: n/a """
        self.xmitLight = lightState

    def addPktProcToPortObj(self, pktProcObj):
        """ Description:  Helper method used to set the pktProc class instance variable.

            Arguments:
                * pktProcObj - Packet processing class instance associated with the port.

            Return Value: n/a """
        self.pktProc = pktProcObj

    def addVMPortToNtoPortObj(self, VMPortObj):
        """ Description:  Helper method used to set the VM interface/port object class instance variable.

            Arguments:
                * VMPortObj - VM interface/port object associated with the port.

            Return Value: n/a """
        self.VMPortObj = VMPortObj

    def addIxPortToNtoPortObj(self, ixPortObj):
        """ Description:  Helper method used to set the ixia port object class instance variable.

            Arguments:
                * ixPortObj - Ixia port object associated with the port.

            Return Value: n/a """
        self.ixPortObj = ixPortObj

    def addAfmrObj(self, amfrObj, afmrBandwidth):
        """ Description:  Helper method used to set the ixia port object class instance variable.

                    Arguments:
                        * amfrObj - Afmr object object associated with the port.
                        * afmrBandwidth - Afmr Bandwidth object object associated with the port.

                    Return Value: n/a """
        self.afmr = amfrObj
        self.afmrBandwidth = afmrBandwidth

    def showParent(self):
        """ Description:  Method to display the parent NTO's IP address.

            Arguments: n/a

            Return Value: n/a """
        print('Here is my parent NTO IP address - %s' % self.myNTO.ipaddr)

    def showPort(self):
        """ Description:  Method to display the ports name and mode.

            Arguments: n/a

            Return Value: n/a """
        print('Port name = %s' % self.name)
        print('Port mode = %s' % self.portMode)

    def showStreams(self):
        """ Description:  Method to display the ixia streams for a port.

            Arguments: n/a

            Return Value: n/a """
        print('Port name = %s' % self.name)
        for key,value in self.ixPortObj.streams.items():
            print('Port stream %s = %s' % (key, value.streamDef))

    def getPortInfo(self, portIdentifier=None):
        """ Description:  ReST method used to query the port's info.

            Arguments:
                * portIdentifier: (optional) Port identifier.

            Return Value:
                * dictionary of port info in from the ReST response string """
        if portIdentifier is None: portIdentifier = self.id
        #print('Issue ReST request to get port info for %s...' % portIdentifier)
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], portIdentifier), verify=False)
        if r.status_code != 200:
            raise Exception("ERROR: An error occurred while getting info for port (%s): [%s] - %s" % (self.name, r.status_code, r.text))
        return (json.loads(r.text))

    def setPortInfo(self, payload):
        """ Description:  ReST method used to set the port's info.

            Arguments:
                * payload: Dictionary of port attributes.

            Return Value:
                * ReST response object """

        print('Issue ReST request to set port info ...')
        r = self.myNTO.sess.put('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        # print("NTO Command, URL ", r.url)
        # print("NTO Command, Payload: ", payload)
        if r.status_code != 200:
            raise Exception("ERROR: An error occurred while updating port (%s): [%s] - %s" % (self.name, r.status_code, r.text))
        return (r)

    def getPortDestFilters(self):
        """ Description:  ReST method used to query the port's destination filter list.

            Arguments: n/a

            Return Value:
                * dictionary containing the port's destination filter list from the ReST response string """
        print('Issue ReST request to get port destination filters ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), verify=False)
        return(json.loads(r.text)['dest_filter_list'])

    def deletePortGroupsAndDestFilters(self):
        """ Description:  Method to delete all associated port groups and destination filters for a port.

            Arguments: n/a

            Return Value: n/a """
        print('Issue ReST request to delete port destination filters ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), verify=False)

        portGrpId = json.loads(r.text)['port_group_id']
        if portGrpId:

            r2 = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/port_groups'], portGrpId), verify=False)

            #Delete and Port Group filters.
            for filterId in json.loads(r2.text)['dest_filter_list']:
                r3 = self.myNTO.sess.delete('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters'], filterId), verify=False)
                if r3.status_code != 200:  raise Exception('ERROR: Unable to delete port (%s) source filter (id=%s)' % (self.name, filterId))

            #Delete the Port Group.
            r4 = self.myNTO.sess.delete('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/port_groups'], portGrpId), verify=False)
            if r4.status_code != 200:  raise Exception('ERROR: Unable to delete port group (id=%s) on port (%s)' % (portGrpId, self.name))

        for filterId in json.loads(r.text)['dest_filter_list']:
            r5= self.myNTO.sess.delete('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters'], filterId), verify=False)
            if r5.status_code != 200:  raise Exception('ERROR: Unable to delete port (%s) dest filter (id=%s)' % (self.name, filterId))

    def getPortSrcFilters(self):
        """ Description:  ReST method used to query the port's source filter list.

            Arguments: n/a

            Return Value:
                * dictionary containing the port's source filter list from the ReST response string """
        print('Issue ReST request to get port source filters ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), verify=False)
        return(json.loads(r.text)['source_filter_list'])

    def deletePortGroupsAndSrcFilters(self):
        """ Description:  Method to delete all associated port groups and source filters for a port.

            Arguments: n/a

            Return Value: n/a """
        print('Issue ReST request to delete port source filters ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), verify=False)

        portGrpId = json.loads(r.text)['port_group_id']
        if portGrpId:

            r2 = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/port_groups'], portGrpId), verify=False)

            #Delete and Port Group filters.
            for filterId in json.loads(r2.text)['source_filter_list']:
                r3 = self.myNTO.sess.delete('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters'], filterId), verify=False)
                if r3.status_code != 200:  raise Exception('ERROR: Unable to delete port (%s) source filter (id=%s)' % (self.name, filterId))

            #Delete the Port Group.
            r4 = self.myNTO.sess.delete('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/port_groups'], portGrpId), verify=False)
            if r4.status_code != 200:  raise Exception('ERROR: Unable to delete port group (id=%s) on port (%s)' % (portGrpId, self.name))

        for filterId in json.loads(r.text)['source_filter_list']:
            r5= self.myNTO.sess.delete('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/filters'], filterId), verify=False)
            if r5.status_code != 200:  raise Exception('ERROR: Unable to delete port (%s) source filter (id=%s)' % (self.name, filterId))

    def deletePortGrp(self):
        """ Description:  ReST method to delete a port group if present for a port.

            Arguments: n/a

            Return Value: n/a """
        print('Issue ReST request to delete port group (if present) ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), verify=False)
        portGrpId = json.loads(r.text)['port_group_id']
        if portGrpId:
            r = self.myNTO.sess.delete('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/port_groups'], portGrpId), verify=False)
            if r.status_code != 200:  raise Exception('ERROR: Unable to delete port group (id=%s) on port (%s)' % (portGrpId, self.name))

    def deleteVirtPorts(self):
        """ Description:  ReST method to delete virtual port(s) if present for a port.

            Arguments: n/a

            Return Value: n/a """
        print('Issue ReST request to delete virtual port(s) (if present) ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), verify=False)
        portInfo=json.loads(r.text)

        #Disable tunneling if enabled (Note: this is a pre-requisite prior to deletion!).
        if 'tunnel_origination_local_settings' in portInfo:
            if portInfo['tunnel_origination_local_settings'] is not None:
                if portInfo['tunnel_origination_local_settings']['enabled']:
                    payload = {}
                    payload['tunnel_origination_local_settings'] = portInfo['tunnel_origination_local_settings']
                    payload['tunnel_origination_local_settings']['enabled'] = False
                    print('Issue ReST put request to put port info ...')
                    r = self.myNTO.sess.put('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
                    if r.status_code != 200:
                        raise Exception('ERROR: Failed (errno = %s) to disable NTO (%s) port (%s) origination tunneling: %s\n%s' % (r.status_code, self.myNTO.ipaddr, self.id, payload, r.text.splitlines()[0]))

        #Delete virtual ports in list.
        if 'virtual_ports_id_list' in portInfo:
            virtPortList = portInfo['virtual_ports_id_list']
            if virtPortList:
                for virtPortId in virtPortList:
                    r = self.myNTO.sess.delete('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], virtPortId), verify=False)
                    if r.status_code != 200:  raise Exception('ERROR: Unable to delete virtual port (id=%s) on port (%s)' % (virtPortId, self.name))

    def getCardId(self):
        """ Description:  ReST method used to query the card Id for a port.

            Arguments: n/a

            Return Value:
                * card Id from the ReST response string """
        print('Issue ReST request to get card Id of port ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), verify=False)
        return (json.loads(r.text)['type'])

    def getCardType(self):
        """ Description:  ReST method used to query the card type for a port.

            Arguments: n/a

            Return Value:
                * card type from the ReST response string """
        print('Issue ReST request to get card type of port ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), verify=False)
        return (json.loads(r.text)['type'])

    def getLicenseType(self):
        """ Description:  ReST method used to query the port's license type.

            Arguments: n/a

            Return Value:
                * port type from the ReST response string """
        print('Issue ReST request to get port license type ...')
        r = self.myNTO.sess.get('https://%s:%s/api/system' % (self.myNTO.ipaddr, self.myNTO.ipport), verify=False)
        pprint(json.loads(r.text)['port_assigned_license_map'][self.defaultName]['type'])

        return (json.loads(r.text)['port_assigned_license_map'][self.defaultName]['type'])

    def getPortGroupId(self):
        """ Description:  ReST method used to query the port group Id for a port.

            Arguments: n/a

            Return Value:
                * port group Id from the ReST response string """
        print('Issue ReST request to get port group Id of port ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), verify=False)
        return (json.loads(r.text)['port_group_id'])

    def getPortMode(self):
        """ Description:  ReST method used to query the port mode for a port.

            Arguments: n/a

            Return Value:
                * port mode from the ReST response string """
        print('Issue ReST request to get port mode ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], self.id), verify=False)
        return (json.loads(r.text)['mode'])

    def getPortFilter(self):
        """ Description:  ReST method used to query the filter mode and filter criteria for a port.

            Arguments: n/a

            Return Value:
                * List containing the filter mode and filter criteria """
        print('Issue ReST request to get port mode ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ippport, self.myNTO.urls['api/ports'], self.id), verify=False)
        return (json.loads(r.text)['filter_mode'],json.loads(r.text)['filter_criteria'])

    def getPortCaptureStatus(self):
        """ Description:  ReST method used to query the capture status for a port.

            Arguments: n/a

            Return Value:
                * ReST reponse string """
        print('Issue ReST request to get port capture status ...')
        r = self.myNTO.sess.get('https://%s:%s/capture_resources/%s?properties=status' % (self.myNTO.ipaddr, self.myNTO.ipport, self.id), verify=False)
        return (r.text)

    def stopPortCapture(self):
        """ Description:  ReST put method to stop the capture for a port.

            Arguments: n/a

            Return Value:
                * status code from the ReST reponse string """
        print('Issue ReST put request to stop port capture ...')
        r = self.myNTO.sess.put('https://%s:%s/capture_resources/%s/stop' % (self.myNTO.ipaddr, self.myNTO.ipport, self.id), verify=False)
        return (r.status_code)

    def stopCaptureIfStarted(self):
        """ Description:  Method to stop capture on a port if already started.

            Arguments: n/a

            Return Value: n/a """
        if (self.cardType == 'CAPTURE'):
            if (self.getPortCaptureStatus() in ('STARTED_EMPTY', 'STARTED_WITH_DATA', 'STARTED_TRIGGERED')):
                if self.stopPortCapture() != 200:
                    raise Exception('ERROR: Unable to stop capture on port %s' % self.name)
                else:
                    #Wait for capture to really complete.
                    time.sleep(3000)

    def getPortCaptureFiles(self):
        """ Description:  ReST method to get the capture files for a port.

            Arguments: n/a

            Return Value:
                * dictionary of capture files from the ReST reponse string """
        print('Issue ReST request to get port capture file(s) ...')
        r = self.myNTO.sess.get('https://%s:%s/capture_resources/%s/files' % (self.myNTO.ipaddr, self.myNTO.ipport, self.id), verify=False)
        return (json.loads(r.text))

    def delPortCaptureFile(self, fileName):
        """ Description:  ReST method to delete capture a file for a port.

            Arguments:
                * fileName = Path to capture file.

            Return Value:
                * status code from the ReST reponse string """
        print('Issue ReST delete request to remove capture file ...')
        fileDict={'file_name': fileName}
        r = self.myNTO.sess.delete('https://%s:%s/capture_resources/%s/delete_file' % (self.myNTO.ipaddr, self.myNTO.ipport, self.id), data=fileDict, verify=False)
        return (r.status_code)

    def delAllPortCaptureFiles(self):
        """ Description:  Method to iterate through and delete all of the capture files for a port.

            Arguments: n/a

            Return Value: n/a """
        if (self.cardType == 'CAPTURE'):
            fileList = self.getPortCaptureFiles()
            if fileList:
                for file in fileList:
                    if self.delPortCaptureFile(file) != 200: raise Exception('ERROR: Unable to delete capture file %s from port %s!' % (file, self.name))

    def setPortToDefaults(self):
        """ Description:  Method to initiailize a port to defaults.

            Arguments: n/a

            Return Value: n/a """
        myPortInfo = self.getPortInfo()

        portProps = {}

        #Determine if keys are older supports_* style or new supported_feature_info_dictionary.
        if 'supported_feature_info' in myPortInfo:

            if myPortInfo['supported_feature_info']['trailer_strip'] != 'NOT_SUPPORTED': portProps['trailer_strip_settings'] = {'enabled': False}
            if myPortInfo['supported_feature_info']['timestamp'] != 'NOT_SUPPORTED': portProps['timestamp_settings'] = {'enabled': False}
            if myPortInfo['supported_feature_info']['std_port_tagging'] != 'NOT_SUPPORTED': portProps['std_port_tagging_settings'] = {'enabled': False, 'vlan_id': 999}
            if myPortInfo['supported_feature_info']['vntag_strip'] != 'NOT_SUPPORTED': portProps['vntag_strip_settings'] = {'enabled': False}
            if myPortInfo['supported_feature_info']['l2gre_strip'] != 'NOT_SUPPORTED': portProps['l2gre_strip_settings'] = {'enabled': False}
            if myPortInfo['supported_feature_info']['gtp_strip'] != 'NOT_SUPPORTED': portProps['gtp_strip_settings'] = {'enabled': False}
            if myPortInfo['supported_feature_info']['mpls_strip'] != 'NOT_SUPPORTED': portProps['mpls_strip_settings'] = {'enabled': False}
            if myPortInfo['supported_feature_info']['erspan_strip'] != 'NOT_SUPPORTED': portProps['erspan_strip_settings'] = {'empty_header': False, 'enabled': False}
            if myPortInfo['supported_feature_info']['vxlan_strip'] != 'NOT_SUPPORTED': portProps['vxlan_strip_settings'] = {'enabled': False}
            if myPortInfo['supported_feature_info']['data_masking'] != 'NOT_SUPPORTED': portProps['data_masking_settings'] = {'enabled': False}
            if myPortInfo['supported_feature_info']['packet_length_trailer'] != 'NOT_SUPPORTED': portProps['packet_length_trailer_settings'] = {'enabled': False}
            if myPortInfo['supported_feature_info']['dedup'] != 'NOT_SUPPORTED': portProps['dedup_settings'] = {'enabled': False}
            if myPortInfo['supported_feature_info']['trim'] != 'NOT_SUPPORTED': portProps['trim_settings'] = {'enabled': False}
            if myPortInfo['supported_feature_info']['fabric_path_strip'] != 'NOT_SUPPORTED': portProps['fabric_path_strip_settings'] = {'enabled': False}

            if myPortInfo['supported_feature_info']['std_vlan_strip'] != 'NOT_SUPPORTED':
                portProps['std_vlan_strip_settings'] = {'egress_count': 0, 'enabled': False, 'ingress_count': 0, 'strip_mode': 'INGRESS'}

            if 'std_strip_by_vlan' in myPortInfo['supported_feature_info']:
                if myPortInfo['supported_feature_info']['std_strip_by_vlan'] != 'NOT_SUPPORTED':
                    portProps['std_strip_by_vlan_settings'] = {'enabled': False, 'strip_mode': 'EGRESS', 'vlan_id': 1}
        else:
            if myPortInfo['supports_trailer_strip']: portProps['trailer_strip_settings'] = {'enabled': False}
            if myPortInfo['supports_timestamp']: portProps['timestamp_settings'] = {'enabled': False}
            if myPortInfo['supports_std_port_tagging']: portProps['std_port_tagging_settings'] = {'enabled': False, 'vlan_id': 999}
            if myPortInfo['supports_vntag_strip']: portProps['vntag_strip_settings'] = {'enabled': False}
            if myPortInfo['supports_l2gre_strip']: portProps['l2gre_strip_settings'] = {'enabled': False}
            if myPortInfo['supports_gtp_strip']: portProps['gtp_strip_settings'] = {'enabled': False}
            if myPortInfo['supports_mpls_strip']: portProps['mpls_strip_settings'] = {'enabled': False}
            if myPortInfo['supports_erspan_strip']: portProps['erspan_strip_settings'] = {'empty_header': False, 'enabled': False}
            if myPortInfo['supports_vxlan_strip']: portProps['vxlan_strip_settings'] = {'enabled': False}
            if myPortInfo['supports_data_masking']: portProps['data_masking_settings'] = {'enabled': False}
            if myPortInfo['supports_packet_length_trailer']: portProps['packet_length_trailer_settings'] = {'enabled': False}
            if myPortInfo['supports_dedup']: portProps['dedup_settings'] = {'enabled': False}
            if myPortInfo['supports_trim']: portProps['trim_settings'] = {'enabled': False}
            if myPortInfo['supports_fabric_path_strip']: portProps['fabric_path_strip_settings'] = {'enabled': False}

            if myPortInfo['supports_std_vlan_strip']:
                portProps['std_vlan_strip_settings'] = {'egress_count': 0, 'enabled': False, 'ingress_count': 0, 'strip_mode': 'INGRESS'}
#TODO???        if 'NTO-4' not in self.myNTO.getSystemSWVersion():
#TODO???               portProps['std_vlan_strip_settings'] = {'enabled': False, 'count': 2}

        #JPM - temp workaround until "tunnel_origination" gets put into "supported_feature_info" dict.
        if 'tunnel_origination_local_settings' in myPortInfo:  portProps['tunnel_origination_local_settings']={'enabled': False, 'local_ip_address': None, 'subnet_mask': None, 'default_gateway': None}

        portProps['name'] = self.defaultName
        portProps['mode'] = 'NETWORK'
        portProps['enabled'] = True
        portProps['filter_criteria'] = {'logical_operation': 'AND'}
        portProps['filter_mode'] = 'PASS_ALL'

        #Send the ReST put request.
        r = self.setPortInfo(portProps)
        if r.status_code != 200:
            raise Exception('ERROR: Failed (errno = %s) to set NTO (%s) port (%s) defaults: %s' % (r.status_code, self.myNTO.ipaddr, self.id, portProps))

    def configurePort(self):
        """ Description:  ReST method to configure properties of a port.

            Arguments: n/a

            Return Value: n/a """
        print('Issue ReST request to configure port %s settings ...' % self.defaultName)
        portProps={}

        #Determine linkSettings.
        if ('100G' in self.linkSpeeds[self.mediaType]) and (self.connType.lower() == 'dac'):
            self.linkSettings = 'AUTO'
            portProps['forward_error_correction'] = True
        else:
            self.linkSettings = self.linkSpeeds[self.mediaType]

        #Determine port connType.
        myPortInfo = self.getPortInfo()
        if 'direct_attach_copper' in myPortInfo:
            if self.connType.lower() == 'standard':
                portProps['direct_attach_copper'] = False
            elif self.connType.lower() == 'dac':
                portProps['direct_attach_copper'] = True

        portProps['name'] = '%s:%s' % (self.name, self.defaultName)
        portProps['media_type'] = self.mediaType
        portProps['mode'] = self.portMode
        portProps['enabled'] = False
        portProps['link_settings'] = self.linkSettings

        #Send the ReST put request w/port disabled (workaround to get 40G fiber ports to link).
        r = self.setPortInfo(portProps)
        if r.status_code != 200:
            raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port (%s) settings: %s\n%s' % (r.status_code, self.myNTO.ipaddr, self.id, portProps, r.text.splitlines()[0]))

        #Enable the port (workaround to get 40G fiber ports to link).
        portProps['enabled'] = True
        r = self.setPortInfo(portProps)
        if r.status_code != 200:
            raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port (%s) settings: %s\n%s' % (r.status_code, self.myNTO.ipaddr, self.id, portProps, r.text.splitlines()[0]))

    def configurePortFilter(self, myFilterMode=None, myFilterDef=None):
        """ Description:  ReST method to configure filter properties of a port.

            Arguments:
                * myFilterMode - (optional) filter mode
                * myFilterDef - (optional) filter definition

            Return Value: n/a """
        print('Issue ReST request to configure port %s filter ...' % self.defaultName)
        myFilterDict = {}
        self.filterMode = myFilterMode

        # Check to see if the filterMode is not pass all or deny all before assigning the filterDef.
        if myFilterMode != 'PASS_ALL' and myFilterMode != 'DENY_ALL':
            # Convert the string to a dict and add the mode to the filter.
            self.filterDef = eval(myFilterDef)
            myFilterDict = self.filterDef

        myFilterDict['filter_mode'] = myFilterMode
        if self.afmr is not None:
            myFilterDictAfmr = {}
            myFilterDictAfmr["resource_attachment_config"] = myFilterDict
            myFilterDict = myFilterDictAfmr
        #Send the ReST put request.
        r = self.setPortInfo(myFilterDict)
        if r.status_code != 200:
            raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port (%s) with filter %s\n%s' % (r.status_code, self.myNTO.ipaddr, self.id, myFilterDict, r.text.splitlines()[0]))

    def configurePacketProcessing(self, pktProcessing):
        """ Description:  ReST method to configure AFMR packet processing for a port.

            Arguments:
                * pktProcessing - dictionary containing the packet processing definition
                                  user must configure pktProccesingin the same order as the FPGA

            Return Value: n/a """
        print('Issue ReST request to configure port %s packet processing ...' % self.defaultName)
        pktProcDict = eval(pktProcessing)

        #keep state and order of the packet processing configured on the object
        self.enabledPktProc.append(pktProcDict)


        #For AFMR, specify the packetprocessing in the resource_attachment_config.
        if self.getResourceAttachmentType() == 'RECIRCULATED_AFM' and list(pktProcDict)[0] not in ["std_vlan_strip_settings","std_strip_by_vlan_settings","std_port_tagging_settings"]:
            pktProcDict = {'resource_attachment_config': pktProcDict}
            pprint(pktProcDict)

        #Send the ReST put request.
        r = self.setPortInfo(pktProcDict)
        if r.status_code != 200:
            print(r.text)
            raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port (%s) packet processing: %s\n%s' % (r.status_code, self.myNTO.ipaddr, self.id, pktProcDict, r.text.splitlines()[0]))

    def attachAFMResource(self, afmResource, allocBW=None):
        """ Description:  ReST method to attach an AFMR to a port.

            Arguments:
                * afmResource - AFMR resouce object.
                * allocBW - BW to be allocated for this AFMR.

            Return Value: n/a """
        # Check the AFMR status for READY before attempting to attach.
        AFMRstatus = afmResource.getAFMResourceInfo(property='resource_status')
        if AFMRstatus == 'READY':
            print('Issue ReST put request to check the correct port mode ...')
            if self.getPortInfo()['mode'] != self.portMode:
                payload = {'mode': self.portMode}
                print('Issue ReST put request to configure the correct port mode ...')
                self.setPortInfo(payload)
            print('Issue ReST put request to attach AFM resource to AFM port object ...')
            print(self.myNTO.getSystemModel())
            if allocBW is None:
                if self.myNTO.getSystemModel() == "VISION_ONE":
                    allocBW = 10
                    print('Issue ReST put request to attach default AFM resource to AFM port object VISION_ONE ...')
                elif self.myNTO.getSystemModel() == "7300":
                    allocBW = 25
                    print('Issue ReST put request to attach default AFM resource to AFM port object 7300 ...')
            print('DEBUG: https://%s:%s/%s/%s/enable' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/recirculated_afm_resources'], afmResource.afmrId))
            pprint({'allocated_bandwidth': allocBW, 'object_id': self.id, 'port_mode': self.portMode})
            r = self.myNTO.sess.put('https://%s:%s/%s/%s/enable' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/recirculated_afm_resources'], afmResource.afmrId), headers={'content-type': 'application/json'},
                                    data=json.dumps({'allocated_bandwidth': allocBW, 'object_id': self.id, 'port_mode': self.portMode}), verify=False)
            if r.status_code != 200:
                raise Exception(
                    'ERROR: Unable to attach AFMR (%s) to object (%s) on NTO chassis (%s) [status code = %s]' % (afmResource.afmrId, self.id, self.myNTO.ipaddr, r.status_code))
            self.addAfmrObj(afmResource, allocBW)
            afmResource.addAttachedObj(self)
            return (r.status_code)
        else:
            raise Exception('ERROR: Unable to attach AFMR (%s) because it is not in READY state [status = %s]' % (afmResource.afmrId, AFMRstatus))

    def detachResource(self, afmResource=None):
        """ Description:  ReST method to dettach an AFMR to a port.

                    Arguments:
                        * afmResource - AFMR resouce object.

                    Return Value: n/a """

        if afmResource is None:
            afmResource = self.afmr
        if afmResource is not None:
            print('Issue ReST put request to detach AFM resource from NTO port object ...')
            r = self.myNTO.sess.put('https://%s:%s/%s/%s/disable' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/recirculated_afm_resources'], afmResource.afmrId), headers={'content-type': 'application/json'},
                                    data=json.dumps({'object_id': self.id}), verify=False)
            if r.status_code != 200:
                raise Exception('ERROR: Unable to detach AFMR (%s) from object (%s) on NTO chassis (%s) [status code = %s]' % (afmResource.afmrId, self.id, self.myNTO.ipaddr, r.status_code))
            afmResource.attachedPorts.pop(afmResource.attachedPorts.index(self))
            self.afmr = None
            self.afmrBandwidth = None
            return (r.status_code)
        else:
            if self.getResourceAttachmentType() == 'RECIRCULATED_AFM':
                myAFMRid = self.getResourceAttachmentConfig(property='resource_id')
                print('Issue ReST put request to detach AFM resource (%s) on NTO (%s)' % (myAFMRid,self.myNTO.ipaddr))
                r = self.myNTO.sess.put('https://%s:%s/%s/%s/disable' % (
                self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/recirculated_afm_resources'], myAFMRid),
                                  headers={'content-type': 'application/json'},
                                  data=json.dumps({'object_id': self.id}), verify=False)
                if r.status_code != 200:
                    raise Exception(
                        'ERROR: Unable to detach AFMR (%s) from port (%s) on NTO chassis (%s) [status code = %s]' % (
                        myAFMRid, self.id, self.myNTO.ipaddr, r.status_code))

    def getResourceAttachmentType(self, myPortId=None):
        """ Description:  ReST method to configure properties of a port.

            Arguments:
                * myPortId - (optional) port Id

            Return Value:
                * the resource attachment type from the ReST reponse string """
        if myPortId is None:
            myPortId = self.id

        print('Issue ReST request to get resource attachment type ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s?properties=resource_attachment_type' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], myPortId), verify=False)
        return (json.loads(r.text)['resource_attachment_type'])

    def getResourceAttachmentConfig(self, myPortId=None, property=None):
        """ Description:  ReST method to configure properties of a port.

            Arguments:
                * myPortId - (optional) port Id
                * property - (optional) resource attachment property

            Return Value:
                * Either the full resource attachement config if property is not specified or just the individual property """
        if myPortId is None:
            myPortId = self.id

        print('Issue ReST request to get resource attachment type ...')
        r = self.myNTO.sess.get('https://%s:%s/%s/%s?properties=resource_attachment_config' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/ports'], myPortId), verify=False)
        if property is None:
            return (json.loads(r.text)['resource_attachment_config'])
        else:
            return (json.loads(r.text)['resource_attachment_config'][property])

    def getPortStats(self, myPortId=None, statName=None):
        """ Description:  ReST method to get the statistics for a port.

            Arguments:
                * myPortId - (optional) port Id
                * statName - (optional) explicit name for a statistic

            Return Value:
                * Either the dictionary of all stats for a snapshot or the value for an explicitly named statistic """
        if myPortId is None:
            myPortId = self.id

        if statName is None or statName == 'all':
            print('Issue ReST post request to get port %s stats ...' % myPortId)
            payload = {'port': myPortId}
        else:
            print('Issue ReST post request to get port %s stat %s ...' % (myPortId, statName))
            payload = {'port': myPortId, 'stat_name': statName}

        r = self.myNTO.sess.post('https://%s:%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/stats']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            if statName is None:
                raise Exception('ERROR: Unable to get port %s stats from NTO chassis (%s) [status code = %s]' % (myPortId, self.myNTO.ipaddr, r.status_code))
            else:
                raise Exception('ERROR: Unable to get port %s stat %s from NTO chassis (%s) [status code = %s]' % (myPortId, statName, self.myNTO.ipaddr, r.status_code))

        if statName is None:
            retStats = json.loads(r.text)['stats_snapshot'][0]
            for stat in self.stats[self.portMode].keys():
                try:
                    self.stats[self.portMode][stat] = retStats[stat.lower()]
                except KeyError:
                    pass

            return(self.stats[self.portMode])
        elif statName == 'all':
            return(json.loads(r.text)['stats_snapshot'][0])
        else:
            print('NTO Port %s %s = %s ...' % (myPortId, statName, json.loads(r.text)['stats_snapshot'][0][statName.lower()]))
            return(json.loads(r.text)['stats_snapshot'][0][statName.lower()])

    def resetPortStats(self, myPortId=None):
        """ Description:  ReST method to reset the statistics of a port.

            Arguments:
                * myPortId - (optional) port Id

            Return Value:
                * status code for the ReST response string. """
        if myPortId is None:
            myPortId = self.id
        print('Issue ReST post request to reset port %s stats ...' % myPortId)
        payload = {'PORT': myPortId}
        r = self.myNTO.sess.post('https://%s:%s/%s/reset_stats' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/stats']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to reset port %s stats from NTO chassis (%s) [status code = %s]' % (myPortId, self.myNTO.ipaddr, r.status_code))
        return (r.status_code)

    def enableVirtPort(self, enabled, local_ip, subnet_mask, default_gateway=None):
        """ Description:  ReST method to enable the port's virtual port.

            Arguments:
                enabled - GRE origination enable flag
                local_ip - Local GRE origination IP address
                subnet_mask - Local GRE origination subnet mask
                default_gateway - (optional) GRE origination default gateway

            Return Value: n/a """
        print('Issue ReST request to enable a virtual port for port %s ...' % self.defaultName)
        if enabled.lower() == 'false':
            enabled=False
        else:
            enabled=True

        portProps={}
        self.tunnelOrigLocalSettings = {'enabled': enabled, 'local_ip': local_ip, 'subnet_mask': subnet_mask, 'default_gateway': default_gateway}
        portProps['tunnel_origination_local_settings']=self.tunnelOrigLocalSettings

        #Send the ReST put request.
        r = self.setPortInfo(portProps)
        if r.status_code != 200:
            raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port (%s) settings: %s\n%s' % (r.status_code, self.myNTO.ipaddr, self.id, portProps, r.text.splitlines()[0]))

    def disableVirtPort(self):
        """ Description:  ReST method to disable the port's virtual port.

            Arguments: n/a

            Return Value: n/a """
        print('Issue ReST request to disable a virtual port for port %s ...' % self.defaultName)

        self.tunnelOrigLocalSettings['enabled']=False
        portProps={}
        portProps['tunnel_origination_local_settings']=self.tunnelOrigLocalSettings

        #Send the ReST put request.
        r = self.setPortInfo(portProps)
        if r.status_code != 200:
            raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port (%s) settings: %s\n%s' % (r.status_code, self.myNTO.ipaddr, self.id, portProps, r.text.splitlines()[0]))

    def confirmEnabledState(self, state=None):
        """ Description:  Validate the state of this port via the ReST API

            Arguments: state(optional) - True/False indicating enabled or disabled

            Return Value: n/a """
        # if an expected state is not specified then use the port's internal state
        if (state == None):
            targetState = self.enabledState
        else:
            targetState = state

        print("Confirming port enabled state(%s)..." % (targetState))
        passed = False
        for i in range(5):
            portDict = self.getPortInfo()
            if (portDict['enabled'] != targetState):
                print("*** Exercising wait loop - NTOPort::confirmEnabledState ***")
                time.sleep(1)
            else:
                passed = True
                break

        if not passed:
            raise Exception("Expecting port enabled state of (%s) but found (%s)." % (targetState,portDict['enabled']))

        # Set the internale state based on the returned value from the system
        self.enabledState = targetState

    def confirmAccessPolicy(self, access, groups=None):
        """ Description:  Validate the acess policy of this port via the ReST API

            Arguments: access - "MODIFY_" or "CONNECT_" followed by "ALLOW_ALL", "REQUIRE_MEMBER", "REQUIRE_ADMIN"
                        groups(optional) - the list of groups that should have access

            Return Value: n/a """
        if (groups == None):
            print("Confirming access policy (%s) for port(%s/%s)..." % (access, self.name, self.portMode))
            groups = []
        else:
            print("Confirming access policy (%s) for port(%s/%s) with group(s) %s..." % (access, self.name, self.portMode, groups))

        policy = access[access.index("_")+1:]
        targetDict = {"groups" : groups, "policy" : policy}
        # Access settings are returned under different headings for network and tool ports as well as for modify/connect
        if ("MODIFY" in access):
            targetSettings = "modify_access_settings"
        else:
            if (self.portMode == "NETWORK"):
                targetSettings = "connect_out_access_settings"
            else:
                targetSettings = "connect_in_access_settings"

        passed = False
        for i in range(5):
            portDict = self.getPortInfo()
            if (portDict[targetSettings] == targetDict):
                print("Access settings(%s) confirmed." % (targetSettings))
                passed = True
                break
            else:
                print("DEBUG: modify_access_settings: " + str(portDict["modify_access_settings"]))
                print("DEBUG: connect_out_access_settings: " + str(portDict["connect_out_access_settings"]))
                print("DEBUG: connect_in_access_settings: " + str(portDict["connect_in_access_settings"]))
                print("*** Exercising wait loop - NTOPort::confirmAccessPolicy ***")
                time.sleep(1)

        if not passed:
            raise Exception("Expecting port access policy of (%s) but found (%s)." % (targetDict,portDict[targetSettings]))

    def confirmConnection(self, f, existStatus=True):
        # Initialize to not successful result
        connectionExists=not existStatus
        print("Confirming connection to filter(%s) is %s..." % (f.id, existStatus))
        if (self.portMode == "NETWORK"):
            targetList = "dest_filter_list"
        else:
            targetList = "source_filter_list"
        passed = False
        for i in range(5):
            portDict = self.getPortInfo()
            fList = portDict[targetList]
            if (int(f.id) in fList):
                print("Connection presence confirmed.")
                connectionExists = True
            else:
                print("Connection does not exist.")
                connectionExists = False
            if (connectionExists == existStatus):
                passed = True
                break
            else:
                print("*** Exercising wait loop - NTOPort::confirmConnection ***")
                time.sleep(1)

        if not passed:
            raise Exception("Connection confirmation failed...expected %s and found %s." % (existStatus, connectionExists))
