import json
import time

class NTOPortGroup:

    """A library for *documentation format* demonstration purposes.

    This documentation is created using reStructuredText__. Here is a link
    to the only \`Keyword\`.

    __ http://docutils.sourceforge.net
    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self):
        self.myNTO=None
        self.portGrpName=None
        self.portGrpDefaultName=None
        self.portGrpDef=None
        self.portGrpId=None
        self.mode=None
        self.type=None
        self.failover_mode=None
        self.ports=[]
        self.filterDef = None
        self.filterMode = None
        self.afmr = None
        self.afmrBandwidth = None
        self.streams={}

        self.stats = {'NETWORK': {'NP_TOTAL_RX_COUNT_PACKETS': None,
                                  'NP_TOTAL_RX_COUNT_BYTES': None,
                                  'NP_TOTAL_PASS_COUNT_PACKETS': None,
                                  'NP_TOTAL_PASS_COUNT_BYTES': None,
                                  'NP_TOTAL_RX_COUNT_VALID_PACKETS': None,
                                  'NP_TOTAL_RX_COUNT_INVALID_PACKETS': None,
                                  'NP_TOTAL_RX_COUNT_FCS_ERRORS': None,
                                  'NP_TOTAL_RX_COUNT_CRC_ALIGNMENT_ERRORS': None,
                                  'NP_TOTAL_RX_COUNT_FRAGMENTS': None,
                                  'NP_TOTAL_RX_COUNT_ALIGNMENT_ERRORS': None,
                                  'NP_TOTAL_RX_COUNT_SYMBOL_ERRORS': None,
                                  'NP_TOTAL_RX_COUNT_COLLISIONS': None,
                                  'NP_TOTAL_RX_COUNT_RUNTS': None,

                                  'NP_TOTAL_RX_COUNT_FRAMES_TOO_LONG': None,
                                  'NP_TOTAL_FABRIC_PATH_STRIP_COUNT_PACKETS': None,
                                  'NP_TOTAL_VNTAG_STRIP_COUNT_PACKETS': None,
                                  'NP_TOTAL_GTP_STRIP_COUNT_PACKETS': None,
                                  'NP_TOTAL_MPLS_STRIP_COUNT_PACKETS': None,
                                  'NP_TOTAL_DATA_MASKING_COUNT_PACKETS': None,
                                  'NP_TOTAL_TRIM_COUNT_PACKETS': None,
                                  'NP_TOTAL_TRIM_COUNT_BYTES': None},

                      'TOOL': {'TP_TOTAL_INSP_COUNT_PACKETS': None,
                               'TP_TOTAL_TX_COUNT_PACKETS': None,
                               'TP_TOTAL_TX_COUNT_BYTES': None,
                               'TP_TOTAL_DROP_COUNT_PACKETS': None,
                               'TP_TOTAL_FABRIC_PATH_STRIP_COUNT_PACKETS': None,
                               'TP_TOTAL_VNTAG_STRIP_COUNT_PACKETS': None,
                               'TP_TOTAL_GTP_STRIP_COUNT_PACKETS': None,
                               'TP_TOTAL_MPLS_STRIP_COUNT_PACKETS': None,
                               'TP_TOTAL_DATA_MASKING_COUNT_PACKETS': None,
                               'TP_TOTAL_TRIM_COUNT_PACKETS': None,
                               'TP_TOTAL_TRIM_COUNT_BYTES': None,
                               'TP_TOTAL_TRAILER_STRIP_COUNT_PACKETS': None}}

    def reinit(self):
        self.__init__()

    def setParent(self, parentObj):
        self.myNTO = parentObj

    def setPortGrpName(self, pgName):
        self.portGrpName = pgName

    def setPortGrpDefaultName(self):
        self.portGrpDefaultName = self.getPortGrpInfo()['default_name']

    def rediscoverPortGrpId(self):
        """ Description:  Helper method used to Re set the port Group id class instance variable.

            Arguments:
                * n/a

            Return Value: n/a """
        print('Issue ReST request to rediscover Port Group id for %s...' % self.portGrpDefaultName)
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/port_groups'], self.portGrpDefaultName),headers={'content-type': 'application/json'}, verify=False)
        self.portGrpId = json.loads(r.text)['id']

    def setPortGrpDef(self, pgDef):
        self.portGrpDef = pgDef

    def setPortGrpMode(self, pgDef):
        self.mode = pgDef['mode']

    def setPortGrpType(self, pgDef):
        self.type = pgDef['type']

    def setPortGrpFailoverMode(self, pgDef):
        if 'failover_mode' in pgDef:
            self.failover_mode = pgDef['failover_mode']

    def setPortGrpPorts(self, portList):
        if isinstance(portList, list):
            self.ports.extend(portList)
        else:
            self.ports.append(portList)

    def showPortGrp(self):
        print('Port Group %s: (def=%s) ports = %s' % (self.portGrpName, self.portGrpDef, self.ports))

    def setPortGrpInfo(self, payload):
        print('Issue ReST request to set port group info ...')
        r = self.myNTO.sess.put('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/port_groups'], self.portGrpId), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        return (r)

    def addAfmrObj(self, amfrObj, afmrBandwidth):
        """ Description:  Helper method used to set the ixia port object class instance variable.

                    Arguments:
                        * amfrObj - Afmr object object associated with the port.
                        * afmrBandwidth - Afmr Bandwidth object object associated with the port.

                    Return Value: n/a """
        self.afmr = amfrObj
        self.afmrBandwidth = afmrBandwidth


    def getPortGrpInfo(self, portGrpIdentifier=None):
        """ Description:  ReST method used to query the port Grp's info.

            Arguments:
                * portGrpIdentifier: (optional) virtual Port identifier.

            Return Value:
                * dictionary of port Grp info in from the ReST response string """
        if portGrpIdentifier is None: portGrpIdentifier = self.portGrpId

        print('Issue ReST request to get port Grp info for %s...' % portGrpIdentifier)
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (
        self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/port_groups'],portGrpIdentifier), verify=False)
        return (json.loads(r.text))

    def createPortGrp(self):
        #Fill in the class instance variables from the port group definition.
        # Convert the string to a dict and add the type, mode, and failover_mode to the filter.
        myPortGrpDefDict = eval(self.portGrpDef)
        self.type = myPortGrpDefDict['type']
        self.mode = myPortGrpDefDict['mode']
        if 'failover_mode' in myPortGrpDefDict:
            self.failover_mode = myPortGrpDefDict['failover_mode']
            payload = {'name': '%s<->???' % self.portGrpName, 'type': self.type, 'mode': self.mode, 'failover_mode': self.failover_mode, 'port_list': [port.id for port in self.ports]}
        else:
            payload = {'name': '%s<->???' % self.portGrpName, 'type': self.type, 'mode': self.mode, 'port_list': [port.id for port in self.ports]}

        print('Issue ReST post request to create port group %s ...' % self.portGrpName)
        r = self.myNTO.sess.post('https://%s:%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/port_groups']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to create port group %s on NTO chassis (%s) [status code = %s]' % (self.portGrpName, self.myNTO.ipaddr, r.status_code))
        else:
            self.portGrpId = json.loads(r.text)['id']
            self.setPortGrpDefaultName()
            print('Successfully created port group %s (id = %s, default name = %s)' % (self.portGrpName, self.portGrpId, self.portGrpDefaultName))

        #Add the portGrpId to all of the ports within the group.
        for port in self.ports:
            port.portGrpId = self.portGrpId

    def deletePortGrpById(self, myPortGrpId=None):
        if myPortGrpId is None:
            myPortGrpId = self.portGrpId
        print('Issue ReST request to delete a port group by Id ...')
        r = self.myNTO.sess.delete('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/port_groups'], myPortGrpId), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to delete port group %s on NTO chassis (%s) [status code = %s]' % (myPortGrpId, self.myNTO.ipaddr, r.status_code))

        #Set portGrpId to None for all of the ports within the group.
        for port in self.ports:
            port.portGrpId = None

        self.myNTO.portGrps[myPortGrpId].remove()
        return (r.status_code)

    def getPortGrpStats(self, myPortGrpId=None, statName=None):
        if myPortGrpId is None:
            myPortGrpId = self.portGrpId

        if statName is None or statName == 'all':
            print('Issue ReST post request to get port group %s stats ...' % myPortGrpId)
            payload = {'port_group': myPortGrpId}
        else:
            print('Issue ReST post request to get port group %s stat %s ...' % (myPortGrpId, statName))
            payload = {'port_group': myPortGrpId, 'stat_name': statName}

        r = self.myNTO.sess.post('https://%s:%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/stats']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            if statName is None:
                raise Exception('ERROR: Unable to get port group %s stats from NTO chassis (%s) [status code = %s]' % (myPortGrpId, self.myNTO.ipaddr, r.status_code))
            else:
                raise Exception('ERROR: Unable to get port group %s stat %s from NTO chassis (%s) [status code = %s]' % (myPortGrpId, statName, self.myNTO.ipaddr, r.status_code))

        if statName is None:
            retStats = json.loads(r.text)['stats_snapshot'][0]
            for stat in self.stats[self.mode].keys():
                try:
                    self.stats[self.mode][stat] = retStats[stat.lower()]
                except KeyError:
                    pass

            return (self.stats[self.mode])
        elif statName == 'all':
            return (json.loads(r.text)['stats_snapshot'][0])
        else:
            return (json.loads(r.text)['stats_snapshot'][0][statName.lower()])

    def configurePortGrpFilter(self, myFilterMode=None, myFilterDef=None):
        """ Description:  ReST method to configure filter properties of a port grop.

            Arguments:
                * myFilterMode - (optional) filter mode
                * myFilterDef - (optional) filter definition

            Return Value: n/a """
        print('Issue ReST request to configure port group %s filter ...' % self.portGrpName)
        myFilterDict = {}
        self.filterMode = myFilterMode

        # Check to see if the filterMode is not pass all or deny all before assigning the filterDef.
        if myFilterMode != 'PASS_ALL' and myFilterMode != 'DENY_ALL':
            # Convert the string to a dict and add the mode to the filter.
            self.filterDef = eval(myFilterDef)
            myFilterDict = self.filterDef

        myFilterDict['filter_mode'] = myFilterMode

        #Send the ReST put request.
        r = self.setPortGrpInfo(myFilterDict)
        if r.status_code != 200:
            raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port group (%s) with filter %s\n%s' \
                            % (r.status_code, self.myNTO.ipaddr, self.portGrpName, myFilterDict, r.text))

    def configurePacketProcessing(self, pktProcessing):
        """ Description:  ReST method to configure AFMR and not AFMR packet processing for a port group.
            If pktProcessing key represents a feature that needs AFMR, pktProcessing dictionary is modified
            so the packet processing to be added on resource_attachment_config
            Arguments:
                * pktProcessing - dictionary containing the packet processing definition

            Return Value: n/a """

        pktProcDict = eval(pktProcessing)
        if next(iter(pktProcDict)) not in ['std_vlan_strip_settings', 'std_port_tagging', 'std_strip_by_vlan_settings']:
            if self.getPortGrpInfo()['resource_attachment_type']=="RECIRCULATED_AFM":
                #modify pktProcDict to add pktProcessing on AFM Resource
                pktProcDict={'resource_attachment_config': pktProcDict}
            else:
                raise Exception('ERROR: Cannot configure advanced packet processing: %s on object with id %s without '
                                'RECIRCULATED_AFM attached on it' % (next(iter(pktProcDict)) ,self.portGrpName))
        print('Issue ReST request to configure port %s packet processing ...' % self.portGrpName)
        r = self.setPortGrpInfo(pktProcDict)
        if r.status_code != 200:
            print(r.text)
            raise Exception('ERROR: Failed (errno = %s) to configure NTO (%s) port group (%s) packet processing: %s\n%s' \
                            % (r.status_code, self.myNTO.ipaddr, self.portGrpName, pktProcDict, r.text))


    def attachAFMResource(self, afmResource, allocBW=None):
        """ Description:  ReST method to attach an AFMR to a port group.

            Arguments:
                * afmResource - AFMR resouce object.
                * allocBW - BW to be allocated for this AFMR.

            Return Value: n/a """
        # Check the AFMR status for READY before attempting to attach.
        AFMRstatus = afmResource.getAFMResourceInfo(property='resource_status')
        if AFMRstatus == 'READY':
            print('Issue ReST put request to attach AFM resource to AFM port object ...')
            print(self.myNTO.getSystemModel())
            if allocBW is None:
                if self.myNTO.getSystemModel() == "VISION_ONE":
                    allocBW = 10
                    print('Issue ReST put request to attach default AFM resource to AFM port object VISION_ONE ...')
                elif self.myNTO.getSystemModel() == "7300":
                    allocBW = 25
                    print('Issue ReST put request to attach default AFM resource to AFM port object 7300 ...')
            r = self.myNTO.sess.put('https://%s:%s/%s/%s/enable' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/recirculated_afm_resources'], afmResource.afmrId),
                                    headers={'content-type': 'application/json'},
                                    data=json.dumps({'allocated_bandwidth': allocBW, 'object_id': self.portGrpId, 'port_mode': self.mode}), verify=False)
            if r.status_code != 200:
                raise Exception(
                    'ERROR: Unable to attach AFMR (%s) to object (%s) on NTO chassis (%s) [status code = %s]' % (afmResource.afmrId, self.portGrpId, self.myNTO.ipaddr, r.status_code))
            self.addAfmrObj(afmResource, allocBW)
            afmResource.addAttachedObj(self)
            return (r.status_code)
        else:
            raise Exception('ERROR: Unable to attach AFMR (%s) because it is not in READY state [status = %s]' % (afmResource.afmrId, AFMRstatus))

    def detachResource(self, afmResource):
        print('Issue ReST put request to detach AFM resource from NTO port object ...')
        r = self.myNTO.sess.put('https://%s:%s/%s/%s/disable' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/recirculated_afm_resources'], afmResource.afmrId), headers={'content-type': 'application/json'},
                                data=json.dumps({'object_id': self.portGrpId}), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to detach AFMR (%s) from object (%s) on NTO chassis (%s) [status code = %s]' % (afmResource.afmrId, self.portGrpId, self.myNTO.ipaddr, r.status_code))
        afmResource.attachedPortGroups.pop(afmResource.attachedPortGroups.index(self))
        self.afmr = None
        self.afmrBandwidth = None
        return (r.status_code)
