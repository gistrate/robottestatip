import re

ixtestfile = '/Users/jamesmckinion/PycharmProjects/ixia/test/robot/NTO_Demo_Tests/myFullDynamicFiltersIPv4.txt'
robotout = '/Users/jamesmckinion/PycharmProjects/ixia/test/robot/NTO_Demo_Tests/myDynamicFiltersIPv4.robot'

SUITE_HEADER = """\
*** Settings ***
Suite Setup     setupTestEnv

#Libraries
Library  NTO_Tcl_Lib2.py  ${TESTDIR}  ${USERNAME}
Library  ResourceMgr.py  auto-tst-prd-w1
Library  Utils.py
Library  Parallel.py  pybot

#Resource files
Resource  RobotUtils.robot

*** Variables ***
${RESOURCEPOOL}=  Automation_5288-150
${RESOURCETIMEOUT}=  10

*** Test Cases ***
parallel_runner
    [Tags]  non_parallel
    parallel_runner2
"""

SUITE_FOOTER = """
*** Keywords ***
setupTestEnv
    log  TESTDIR=${TESTDIR} USERNAME=${USERNAME}  console=true
    setTclAutoPath
    sourceIxTesterLibraries
    getTclPackages
"""

class IxTestParser:

    """A library for *documentation format* demonstration purposes.

    This documentation is created using reStructuredText__. Here is a link
    to the only \`Keyword\`.

    __ http://docutils.sourceforge.net
    """

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self, ixtestfile, robotout):
        self.ifh = open(ixtestfile, 'r')
        self.ofh = open(robotout, 'w')


    def parseInputFile(self):

        #Read file into list of strings with newlines stripped.
        lines = self.ifh.read().splitlines()

        #Remove any blank lines.
        lines = list(filter(None, lines))

        #Create list with testcases as sublists.
        testctr=0
        suite=[]
        for line in lines:
            if line == 'START TEST':
                sublist=[]
            elif line == 'END TEST':
                suite.append(sublist)
                testctr+=1
            else:
                sublist.append(line)

        print('Discovered testcases: %d' % testctr)

        return(suite)


    def convertTestCases(self, suite):

        #Add the suite header.
        self.ofh.write(SUITE_HEADER)

        #Transform each testcase in the suite.
        testctr=0
        for testcase in suite:
            dictFlag=False
            cmdFlag=False

            #Use list comprehension with regex to find HW, stream, and filter defs.
            hwList = [re.findall('(?<=\{).+?(?=\})', k) for k in testcase if 'HW:' in k]
            streams = [re.search("stream\s*[\(]+\s*(.*)[\)]+", k).groups(1)[0] for k in testcase if 'stream' in k]
            filters = [re.search("filter\s*[\(]+\s*(.*)[\)]+", k).groups(1)[0] for k in testcase if 'filter' in k]
            testcase = [k for k in testcase if ('stream' not in k) and ('filter' not in k)]

            #Iterate over the lines in the testcase.
            for line in testcase:
                if line.startswith('#'):
                    if line.startswith('# ID: '):
                        #print(line.lstrip('# ID: '))
                        self.ofh.write('\n' + line.lstrip('# ID: ') + '\n')

                        #Create the HW list.
                        self.ofh.write('\n    ${hwList}=  Create List\n')
                        for item in hwList[0]:
                            self.ofh.write('    ...  %s\n' % item)

                        #Create the streams list.
                        self.ofh.write('\n    ${streamList}=  Create List\n')
                        for stream in streams:
                            self.ofh.write('    ...  %s\n' % stream)

                        #Create the filters list.
                        self.ofh.write('\n    ${filterList}=  Create List\n')
                        for filter in filters:
                            self.ofh.write('    ...  %s\n' % filter)

                        #Create the variable dict.
                        self.ofh.write('\n    ${testDict} =  Create Dictionary\n')
                        line = line.lstrip('# ')
                        self.ofh.write('    ...  ' + re.sub(r':\s*', '=', line) + '\n')

                    elif line.startswith('#'):
                        #Convert HW into a list.
                        if 'HW:' in line:
                            self.ofh.write('    ...  ' + re.sub(':(.*)', '=${hwList}', line.lstrip('# ')) + '\n')
                            self.ofh.write('    ...  STREAMS=${streamList}\n')
                            self.ofh.write('    ...  FILTERS=${filterList}\n')
                        else:
                            line = line.lstrip('# ')
                            self.ofh.write('    ...  ' + re.sub(r':\s*', '=', line) + '\n')

                else:
                    if 'Setup_NVS_Devices' in line:
                        product = re.search("Setup_NVS_Devices\s*(\w*)", line).groups(1)[0]
                        self.ofh.write('\n    ${resource}=  requestResourceBlocking  ${hwList}  ${RESOURCEPOOL}  %s  ${RESOURCETIMEOUT}\n' % product)
                        self.ofh.write('\n    Setup_NVS_Devices  ${testDict}  ${resource}\n')

                    elif '{' not in line:
                        self.ofh.write('    %s\n' % line.replace(' ', '  '))

                    else:
                        line = re.sub(r'\s[\{(.*)\}]', '  ', line, count=1)
                        line = re.sub(r'\}$', '', line)
                        self.ofh.write('    %s\n' % line)

            self.ofh.write('    releaseResources  ${resource}\n')
            testctr+=1

        #Add the suite footer.
        self.ofh.write(SUITE_FOOTER)

        print('Converted testcases: %d' % testctr)

    def closeFileHandles(self):
        self.ifh.close()
        self.ofh.close()


parser=IxTestParser(ixtestfile, robotout)
suiteList = parser.parseInputFile()
parser.convertTestCases(suiteList)
parser.closeFileHandles()
