#from NTO import NTO
#import NTO
from NTO.frwk.NTOPortGroup import NTOPortGroup
from pprint import pprint

class NTOStats:

    """This library forms the base statistics class for the NTO parent.

       Parent Objects:

       * n/a

       Child Objects:

       * n/a
    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self):
        self.myNTO=None

    def reinit(self):
        self.__init__()

    def setParent(self, parentObj):
        """ Description:  Helper method used to set the NTO parent object.

            Arguments: n/a

            Return Value: n/a """
        self.myNTO = parentObj

    def analyzeNVSStats(self, statsMap):
        """ Description:  Helper method to call chkNetworkStats(), chkDynFilterStats(), and chkToolStats().

            Arguments:
                * statsMap - Mapping of streams after each egress point for the network, dynamic filter, and tool objects listed

            Return Value: n/a """
        self.chkNetworkStats(statsMap)
        self.chkDynFilterStats(statsMap)
        self.chkToolStats(statsMap)

    def chkNetworkStats(self, statsMap):
        """ Description:  Helper method to check the Network Port statistics for streams specified in statsMap.

            Arguments:
                * statsMap - Mapping of streams after each egress point for the NP, DF, and TP listed

            Return Value: n/a """
        try:
            statsMapDict = eval(statsMap)
        except SyntaxError:
            raise Exception('Error:  Improper syntax used with the statistics mapping dictionary.')

        for npname, streamList in statsMapDict['Network'].items():
            try:
                npObj = next(port for port in self.myNTO.ports if port.name == npname)
            except StopIteration:
                npObj = next(portGrp for portGrp in self.myNTO.portGrps if portGrp.portGrpName == npname)
                npstats = npObj.getPortGrpStats()
            else:
                npstats = npObj.getPortStats()

            # Verify that no physical layer errors were encountered on the NP port.
            if npstats['NP_TOTAL_RX_COUNT_INVALID_PACKETS'] != 0:  raise Exception('ERROR:  Network port %s counter NP_TOTAL_RX_COUNT_INVALID_PACKETS is non-zero!' % npname)
            if npstats['NP_TOTAL_RX_COUNT_FRAMES_TOO_LONG'] != 0:  raise Exception('ERROR:  Network port %s counter NP_TOTAL_RX_COUNT_FRAMES_TOO_LONG is non-zero!' % npname)
            if npstats['NP_TOTAL_RX_COUNT_FCS_ERRORS'] != 0:  raise Exception('ERROR:  Network port %s counter NP_TOTAL_RX_COUNT_FCS_ERRORS is non-zero!' % npname)
            if npstats['NP_TOTAL_RX_COUNT_ALIGNMENT_ERRORS'] != 0:  raise Exception('ERROR:  Network port %s counter NP_TOTAL_RX_COUNT_ALIGNMENT_ERRORS is non-zero!' % npname)
            if npstats['NP_TOTAL_RX_COUNT_SYMBOL_ERRORS'] != 0:  raise Exception('ERROR:  Network port %s counter NP_TOTAL_RX_COUNT_SYMBOL_ERRORS is non-zero!' % npname)
            if npstats['NP_TOTAL_RX_COUNT_COLLISIONS'] != 0:  raise Exception('ERROR:  Network port %s counter NP_TOTAL_RX_COUNT_COLLISIONS is non-zero!' % npname)
            if npstats['NP_TOTAL_RX_COUNT_CRC_ALIGNMENT_ERRORS'] != 0:  raise Exception('ERROR:  Network port %s counter NP_TOTAL_RX_COUNT_CRC_ALIGNMENT_ERRORS is non-zero!' % npname)
            if npstats['NP_TOTAL_RX_COUNT_FRAGMENTS'] != 0:  raise Exception('ERROR:  Network port %s counter NP_TOTAL_RX_COUNT_FRAGMENTS is non-zero!' % npname)
            if npstats['NP_TOTAL_RX_COUNT_RUNTS'] != 0:  raise Exception('ERROR:  Network port %s counter NP_TOTAL_RX_COUNT_RUNTS is non-zero!' % npname)

            #Determine if Port or Port Group and create a list.
            if isinstance(npObj, NTOPortGroup):
                portList = npObj.ports
            else:
                portList = [npObj]

            expIxiaTxFrames = 0
            expIxiaTxBytes = 0
            ixTxStats = {'txBytes': 0, 'txFrames': 0}

            #Aggregate the stats for each Port Group port (once through for Network Ports).
            for myport in portList:
                if myport.ixPortObj != None:
                    ixCtrDict = myport.ixPortObj.getIxPortTxStats()

                    ixTxStats['txBytes'] += ixCtrDict['txBytes']
                    ixTxStats['txFrames'] += ixCtrDict['txFrames']

                    #Aggregate the Ixia port outbound stream stats.
                    for stream in myport.ixPortObj.streams:
                        expIxiaTxFrames += int(myport.ixPortObj.streams[stream]['numFrames'])
                        expIxiaTxBytes  += int(myport.ixPortObj.streams[stream]['numFrames']) * int(myport.ixPortObj.streams[stream]['frameSize'])

            #Compare Ixia actual Tx w/Stream definitions.
            if ixTxStats['txFrames'] == expIxiaTxFrames:
                print('Ixia counters for network port %s Tx frames (%s) matches expected sent frames (%s)' % (npname, ixTxStats['txFrames'], expIxiaTxFrames))
            else:
                raise Exception('ERROR:  Ixia counters for network port %s Ixia Tx frames (%s) does not match expected sent frames (%s)!' % (npname, ixTxStats['txFrames'], expIxiaTxFrames))

            if ixTxStats['txBytes'] == expIxiaTxBytes:
                print('Ixia counters for network port %s Tx bytes (%s) matches expected sent bytes (%s)' % (npname, ixTxStats['txBytes'], expIxiaTxBytes))
            else:
                raise Exception('ERROR:  Ixia counters for network port %s Tx bytes (%s) does not match expected sent bytes (%s)!' % (npname, ixTxStats['txBytes'], expIxiaTxBytes))

            #Compare NP actual Rx w/Ixia actual Tx.
            if ixTxStats['txFrames'] == npstats['NP_TOTAL_RX_COUNT_PACKETS']:
                print('Network port %s receive frames (%s) matches expected inbound frames (%s)' % (npname, npstats['NP_TOTAL_RX_COUNT_PACKETS'], ixTxStats['txFrames']))
            else:
                #JPM - Debug for BUG1452766.
                print(npstats)
                for port in self.myNTO.ports:
                    pprint(port.getPortStats())
                    pprint(port.ixPortObj.getIxPortTxStats())
                for filter in self.myNTO.filter: pprint(filter.getFilterStats())

                raise Exception('ERROR:  Network port %s receive frames (%s) does not match expected inbound frames (%s)!' % (npname, npstats['NP_TOTAL_RX_COUNT_PACKETS'], ixTxStats['txFrames']))

            if ixTxStats['txFrames'] == npstats['NP_TOTAL_RX_COUNT_VALID_PACKETS']:
                print('Network port %s valid receive frames (%s) matches expected inbound frames (%s)' % (npname, npstats['NP_TOTAL_RX_COUNT_VALID_PACKETS'], ixTxStats['txFrames']))
            else:
                #JPM - Debug for BUG1452766.
                pprint(npstats)
                for port in self.myNTO.ports:
                    pprint(port.getPortStats())
                    pprint(port.ixPortObj.getIxPortTxStats())
                for filter in self.myNTO.filter: pprint(filter.getFilterStats())
                raise Exception('ERROR:  Network port %s valid receive frames (%s) does not match expected inbound frames (%s)!' % (npname, npstats['NP_TOTAL_RX_COUNT_VALID_PACKETS'], ixTxStats['txFrames']))

            if ixTxStats['txBytes'] == npstats['NP_TOTAL_RX_COUNT_BYTES']:
                print('Network port %s receive bytes (%s) matches expected inbound bytes (%s)' % (npname, npstats['NP_TOTAL_RX_COUNT_BYTES'], ixTxStats['txBytes']))
            else:
                #JPM - Debug for BUG1452766.
                pprint(npstats)
                for port in self.myNTO.ports:
                    pprint(port.getPortStats())
                    pprint(port.ixPortObj.getIxPortTxStats())
                for filter in self.myNTO.filter: pprint(filter.getFilterStats())
                raise Exception('ERROR:  Network port %s receive bytes (%s) does not match expected inbound bytes (%s)!' % (npname, npstats['NP_TOTAL_RX_COUNT_BYTES'], ixTxStats['txBytes']))

            expNpPassFrames = 0
            expNpPassBytes = 0
            for myport in portList:
                if myport.ixPortObj != None:

                    #Aggregate the Network Port outbound stream stats.
                    for stream in streamList:
                        if (isinstance(npObj, NTOPortGroup) and stream in myport.ixPortObj.streams) or not (isinstance(npObj, NTOPortGroup)):
                            expNpPassFrames += int(myport.ixPortObj.streams[stream]['numFrames'])
                            expNpPassBytes += int(myport.ixPortObj.streams[stream]['numFrames']) * int(myport.ixPortObj.streams[stream]['frameSize'])

            if expNpPassFrames == npstats['NP_TOTAL_PASS_COUNT_PACKETS']:
                print('Network port %s pass frames (%s) matches expected NP pass frames (%s)' % (npname, npstats['NP_TOTAL_PASS_COUNT_PACKETS'], expNpPassFrames))
            else:
                pprint(npstats)
                raise Exception('ERROR:  Network port %s pass frames (%s) does not match expected NP pass frames (%s)!' % (npname, npstats['NP_TOTAL_PASS_COUNT_PACKETS'], expNpPassFrames))

            if expNpPassBytes == npstats['NP_TOTAL_PASS_COUNT_BYTES']:
                print('Network port %s pass bytes (%s) matches expected NP pass bytes (%s)' % (npname, npstats['NP_TOTAL_PASS_COUNT_BYTES'], expNpPassBytes))
            else:
                pprint(npstats)
                raise Exception('ERROR:  Network port %s pass bytes (%s) does not match expected NP pass bytes (%s)!' % (npname, npstats['NP_TOTAL_PASS_COUNT_BYTES'], expNpPassBytes))

    def chkDynFilterStats(self, statsMap):
        """ Description:  Helper method to check the Dynamic Filter statistics for streams specified in statsMap.

            Arguments:
                * statsMap - Mapping of streams after each egress point for the NP, DF, and TP listed

            Return Value: n/a """

        #Dummy print for output spacing.
        print("")

        try:
            statsMapDict = eval(statsMap)
        except SyntaxError:
            raise Exception('Error:  Improper syntax used with the statistics mapping dictionary.')

        for dfname, streamList in statsMapDict['Filter'].items():
            myfilter = next(filter for filter in self.myNTO.filters if filter.name == dfname)
            dfstats = myfilter.getFilterStats()

            #Iterate over the connections to aggregate inbound streams to the filter.
            inboundStreams = {}
            for conn in self.myNTO.conns:
                if conn.filter.name == dfname:

                    #Create a port list from the network Port Group or the network Port in the connection.
                    if isinstance(conn.network, NTOPortGroup):
                        portList = conn.network.ports
                    else:
                        portList = [conn.network]

                    #Iterate over the Port Group port list; otherwise, iterate over the single port.
                    for port in portList:
                        for stream, streamDef in port.ixPortObj.streams.items():
                            for npname, npStreamList in statsMapDict['Network'].items():
                                if stream in npStreamList and stream not in inboundStreams:
                                    inboundStreams[stream]=streamDef

            expDfRxFrames = 0
            expDfRxBytes = 0
            #Aggregate filter receive stats from all conns/streams.
            for stream, streamDef in inboundStreams.items():
                expDfRxFrames += int(streamDef['numFrames'])
                expDfRxBytes  += int(streamDef['numFrames']) * int(streamDef['frameSize'])

            if expDfRxFrames == dfstats['DF_TOTAL_INSP_COUNT_PACKETS']:
                print('Dynamic filter %s inspected frames (%s) matches expected DF receive frames (%s)' % (dfname, dfstats['DF_TOTAL_INSP_COUNT_PACKETS'], expDfRxFrames))
            else:
                pprint(dfstats)
                raise Exception('ERROR:  Dynamic filter %s inspected frames (%s) does not match expected DF receive frames (%s)!' % (dfname, dfstats['DF_TOTAL_INSP_COUNT_PACKETS'], expDfRxFrames))

            if expDfRxBytes == dfstats['DF_TOTAL_INSP_COUNT_BYTES']:
                print('Dynamic filter %s inspected bytes (%s) matches expected DF receive bytes (%s)' % (dfname, dfstats['DF_TOTAL_INSP_COUNT_BYTES'], expDfRxBytes))
            else:
                pprint(dfstats)
                raise Exception('ERROR:  Dynamic filter %s inspected bytes (%s) does not match DF expected receive bytes (%s)!' % (dfname, dfstats['DF_TOTAL_INSP_COUNT_BYTES'], expDfRxBytes))

            expDfPassFrames = 0
            expDfPassBytes = 0
            # Aggregate the Dynamic Filter outbound stream stats.
            for stream in streamList:
                expDfPassFrames += int(inboundStreams[stream]['numFrames'])
                expDfPassBytes += int(inboundStreams[stream]['numFrames']) * int(inboundStreams[stream]['frameSize'])

            if expDfPassFrames == dfstats['DF_TOTAL_PASS_COUNT_PACKETS']:
                print('Dynamic filter %s pass frames (%s) matches expected DF pass frames (%s)' % (dfname, dfstats['DF_TOTAL_PASS_COUNT_PACKETS'], expDfPassFrames))
            else:
                pprint(dfstats)
                raise Exception('ERROR:  Dynamic filter %s pass frames (%s) does not match expected DF pass frames (%s)!' % (dfname, dfstats['DF_TOTAL_PASS_COUNT_PACKETS'], expDfPassFrames))

            if expDfPassBytes == dfstats['DF_TOTAL_PASS_COUNT_BYTES']:
                print('Dynamic filter %s pass bytes (%s) matches expected DF pass bytes (%s)' % (dfname, dfstats['DF_TOTAL_PASS_COUNT_BYTES'], expDfPassBytes))
            else:
                pprint(dfstats)
                raise Exception('ERROR:  Dynamic filter %s pass bytes (%s) does not match expected DF pass bytes (%s)!' % (dfname, dfstats['DF_TOTAL_PASS_COUNT_BYTES'], expDfPassBytes))

            expDfDenyFrames = expDfRxFrames - expDfPassFrames
            expDfDenyBytes = expDfRxBytes - expDfPassBytes

            if expDfDenyFrames == dfstats['DF_TOTAL_DENY_COUNT_PACKETS']:
                print('Dynamic filter %s deny frames (%s) matches expected DF deny frames (%s)' % (dfname, dfstats['DF_TOTAL_DENY_COUNT_PACKETS'], expDfDenyFrames))
            else:
                pprint(dfstats)
                raise Exception('ERROR:  Dynamic filter %s deny frames (%s) does not match expected DF deny frames (%s)!' % (dfname, dfstats['DF_TOTAL_DENY_COUNT_PACKETS'], expDfDenyFrames))

            if expDfDenyBytes == dfstats['DF_TOTAL_DENY_COUNT_BYTES']:
                print('Dynamic filter %s deny bytes (%s) matches expected DF deny bytes (%s)' % (dfname, dfstats['DF_TOTAL_DENY_COUNT_BYTES'], expDfDenyBytes))
            else:
                pprint(dfstats)
                raise Exception('ERROR:  Dynamic filter %s deny bytes (%s) does not match expected DF deny bytes (%s)!' % (dfname, dfstats['DF_TOTAL_DENY_COUNT_BYTES'], expDfDenyBytes))

    def chkToolStats(self, statsMap):
        """ Description:  Helper method to check the Tool Port statistics for streams specified in statsMap.

            Arguments:
                * statsMap - Mapping of streams after each egress point for the NP, DF, and TP listed

            Return Value: n/a """

        #Dummy print for output spacing.
        print("")

        try:
            statsMapDict = eval(statsMap)
        except SyntaxError:
            raise Exception('Error:  Improper syntax used with the statistics mapping dictionary.')

        for tpname, streamList in statsMapDict['Tool'].items():

            try:
                tpObj = next(port for port in self.myNTO.ports if port.name == tpname)
            except StopIteration:
                tpObj = next(portGrp for portGrp in self.myNTO.portGrps if portGrp.portGrpName == tpname)
                tpstats = tpObj.getPortGrpStats()
            else:
                tpstats = tpObj.getPortStats()

            # Iterate over the connections to aggregate filters for the tool port.
            connectedFilters = []
            for conn in self.myNTO.conns:
                if isinstance(conn.tool, NTOPortGroup):

                    #If tpname matches the tool Port Group Name
                    if tpname == conn.tool.portGrpName:
                        connectedFilters.append(conn.filter.name)

                    #If tpObj is a port within the conn Port Group.
                    if tpObj in conn.tool.ports:
                        connectedFilters.append(conn.filter.name)
                else:
                    #If tpname matches the tool Port Name
                    if tpname == conn.tool.name:
                        connectedFilters.append(conn.filter.name)

            # Iterate over the connections with connected filters to aggregate inbound streams to the tool port.
            inboundStreams = {}
            for conn in self.myNTO.conns:
                if conn.filter.name in connectedFilters:

                    # Create a port list from the network Port Group or the network Port in the connection.
                    if isinstance(conn.network, NTOPortGroup):
                        portList = conn.network.ports
                    else:
                        portList = [conn.network]

                    # Iterate over the Port Group port list; otherwise, iterate over the single port.
                    for port in portList:
                        for stream, streamDef in port.ixPortObj.streams.items():
                            for dfname, dfStreamList in statsMapDict['Filter'].items():
                                if conn.filter.name == dfname and stream in dfStreamList and stream not in inboundStreams:
                                    inboundStreams[stream] = streamDef

            expTpRxFrames = 0
            expTpRxBytes = 0

            # Aggregate filter receive stats from all conns/streams.
            for stream, streamDef in inboundStreams.items():
                expTpRxFrames += int(streamDef['numFrames'])
                expTpRxBytes += int(streamDef['numFrames']) * int(streamDef['frameSize'])

            #Do NOT check expected Rx frames for ports within a tool Port Group since there currently isn't a way to map them back to the individual filters.
            #Note:  All tool port groups are defaulted to loadbalance even if provisioned as interconnect (per Nikki).
            if isinstance(tpObj, NTOPortGroup) or tpObj.portGrpId is None:
                if expTpRxFrames == tpstats['TP_TOTAL_INSP_COUNT_PACKETS']:
                    print('Tool port %s inspected frames (%s) matches expected TP receive frames (%s)' % (tpname, tpstats['TP_TOTAL_INSP_COUNT_PACKETS'], expTpRxFrames))
                else:
                    pprint(tpstats)
                    raise Exception('ERROR:  Tool port %s inspected frames (%s) does not match expected TP receive frames (%s)!' % (tpname, tpstats['TP_TOTAL_INSP_COUNT_PACKETS'], expTpRxFrames))

            expTpTxFrames = 0
            expTpTxBytes = 0

            # Aggregate the Tool Port outbound stream stats.
            for stream in streamList:
                expTpTxFrames += int(inboundStreams[stream]['numFrames'])
                expTpTxBytes += int(inboundStreams[stream]['numFrames']) * int(inboundStreams[stream]['frameSize'])

            if expTpTxFrames == tpstats['TP_TOTAL_TX_COUNT_PACKETS']:
                print('Tool port %s sent frames (%s) matches expected outbound frames (%s)' % (tpname, tpstats['TP_TOTAL_TX_COUNT_PACKETS'], expTpTxFrames))
            else:
                pprint(tpstats)
                raise Exception('ERROR:  Tool port %s sent frames (%s) does not match expected outbound frames (%s)!' % (tpname, tpstats['TP_TOTAL_TX_COUNT_PACKETS'], expTpTxFrames))

            if expTpTxBytes == tpstats['TP_TOTAL_TX_COUNT_BYTES']:
                print('Tool port %s sent bytes (%s) matches expected outbound bytes (%s)' % (tpname, tpstats['TP_TOTAL_TX_COUNT_BYTES'], expTpTxBytes))
            else:
                pprint(tpstats)
                raise Exception('ERROR:  Tool port %s sent bytes (%s) does not match expected outbound bytes (%s)!' % (tpname, tpstats['TP_TOTAL_TX_COUNT_BYTES'], expTpTxBytes))

            #Determine if Port or Port Group and add to list.
            if isinstance(tpObj, NTOPortGroup):
                portList = tpObj.ports
            else:
                portList = [tpObj]

            # Aggregate the stats for each Port Group port (once through for Network Ports).
            ixRxStats = {'rxBytes': 0, 'rxFrames': 0, 'overSizeFrames': 0}
            for myport in portList:
                if myport.ixPortObj != None:
                    ixCtrDict = myport.ixPortObj.getIxPortRxStats()

                    ixRxStats['rxBytes'] += ixCtrDict['rxBytes']
                    ixRxStats['rxFrames'] += ixCtrDict['rxFrames']
                    ixRxStats['overSizeFrames'] += ixCtrDict['overSizeFrames']

            # Compare Ixia actual Tx w/Stream definitions.
            if ixRxStats['rxFrames'] + ixRxStats['overSizeFrames'] == expTpTxFrames:
                print('Ixia port for tool port %s Rx frames (%s) matches expected receive frames (%s)' % (tpname, ixRxStats['rxFrames'], expTpTxFrames))
            else:
                raise Exception('ERROR:  Ixia port for tool port %s Rx frames (%s) does not match expected receive frames (%s)!' % (tpname, ixRxStats['rxFrames'], expTpTxFrames))

            if ixRxStats['rxBytes'] == expTpTxBytes:
                print('Ixia port for tool port %s Rx bytes (%s) matches expected receive bytes (%s)' % (tpname, ixRxStats['rxBytes'], expTpTxBytes))
            else:
                raise Exception('ERROR:  Ixia port for tool port %s Rx bytes (%s) does not match expected receive bytes (%s)!' % (tpname, ixRxStats['rxBytes'], expTpTxBytes))

