class NTOConn:


    """This library forms the connection class for the NTO parent.  There can be one or more connection class child instances under a single NTO instance.

       Parent Objects:

       * myNTO - NTO parent object.

       Child Objects:

       * n/a
    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self):
        self.myNTO=None
        self.connName=None
        self.network=None
        self.filter=None
        self.tool=None

    def reinit(self):
        self.__init__()

    def setConnName(self, connName):
        """ Description:  Method to set the name of the connection.

            Arguments:
                * connName - String name of the connection

            Return Value: n/a """
        self.connName = connName

    def setParent(self, parentObj):
        """ Description:  Method to set the parent for the connection object.

            Arguments:
                * parentObj - Parent object of the connection instance

            Return Value: n/a """
        self.myNTO = parentObj

    def setNetworkPort(self, npObj):
        """ Description:  Method to designate the Network Port object for the connection.

            Arguments:
                * npObj - Network port object

            Return Value: n/a """
        self.network = npObj

    def setDynamicFilter(self, dfObj):
        """ Description:  Method to designate the Dynamic Filter object for the connection.

            Arguments:
                * dfObj - Dynamic filter object

            Return Value: n/a """
        self.filter = dfObj

    def setToolPort(self, tpObj):
        """ Description:  Method to designate the Tool Port object for the connection.

            Arguments:
                * tpObj - Tool port object

            Return Value: n/a """
        self.tool = tpObj

    def showConn(self):
        """ Description:  Method to display the connection's name, np, df, and tp objects.

            Arguments: n/a

            Return Value: n/a """
        print('Connection: %s = %s; %s; %s' % (self.connName, self.network, self.filter, self.tool))

