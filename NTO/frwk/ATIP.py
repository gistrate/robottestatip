from pprint import pprint
import requests
import json
import logging
from requests_toolbelt.multipart.encoder import MultipartEncoder
import os
import subprocess


class ATIP:
    """A library for *documentation format* demonstration purposes.

    This documentation is created using reStructuredText__. Here is a link
    to the only \`Keyword\`.

    __ http://docutils.sourceforge.net
    """

    ROBOT_LIBRARY_DOC_FORMAT = 'reST'
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'

    def __init__(self):
        self.myNTO=None
        self.atipName = None
        self.atipDefaultName = None
        self.atipId = None

        self.userpass = ('admin', 'admin')
        self.sess = requests.Session()
        self.sess.auth = self.userpass

    def reinit(self):
        self.__init__()

    def setParent(self, parentObj):
        self.myNTO = parentObj

    def setATIPResourceName(self, atipName):
        self.atipName = atipName

    def findByDefaultName(self, atipDefaultName):
        print('Issue ReST post request to search for ATIP with default name %s ...' % atipDefaultName)
        payload = {'name': atipDefaultName}
        r = self.myNTO.sess.post('https://%s:%s/%s/search' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/atip_resources']), headers={'content-type': 'application/json'}, data=json.dumps(payload), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: An error occurred trying to locate ATIP resource %s in NTO chassis (%s) [status code = %s]' % (atipDefaultName, self.myNTO.ipaddr, r.status_code))

        #Check for empty list (e.g. ATIP defaultName not found).
        if len(json.loads(r.text)) != 0:
            self.atipDefaultName = atipDefaultName
            self.atipId = json.loads(r.text)[0]['id']
        else:
            raise Exception('ERROR: Unable to locate ATIP resource %s in NTO chassis (%s) [status code = %s]' % (atipDefaultName, self.myNTO.ipaddr, r.status_code))
        return (r.text)

    def setATIPResourceId(self):
        self.atipId = self.getATIPResourceInfo(self.atipDefaultName, 'id')

        # Check for valid AFMR Id.
        if self.atipId is None: raise Exception('ERROR: Unable to locate the actual id for ATIP Resource (%s) on NTO (%s)' % (self.atipDefaultName, self.myNTO.ipaddr))

    def getATIPResourceInfo(self, atipIdentifier=None, property=None):
        if atipIdentifier is None: atipIdentifier = self.atipDefaultName
        print('Issue ReST request to get ATIP Resource info for %s...' % atipIdentifier)
        r = self.myNTO.sess.get('https://%s:%s/%s/%s' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/atip_resources'], atipIdentifier), verify=False)
        pprint(json.loads(r.text))
        if property is None:
            return(json.loads(r.text))
        else:
            return(json.loads(r.text)[property])

    def attachResource(self, filterObj):
        # Check the ATIP status for READY before attempting to attach.
        ATIPstatus = self.getATIPResourceInfo(property='resource_status')
        if ATIPstatus == 'READY':
            print('Issue ReST put request to attach ATIP resource (%s) to filter object (%s)...' % (self.atipName, filterObj.name))
            r = self.myNTO.sess.put('https://%s:%s/%s/%s/enable' % (self.myNTO.ipaddr, self.myNTO.ipport,
                                                                    self.myNTO.urls['api/atip_resources'], self.atipId), headers={'content-type': 'application/json'},
                                    data=json.dumps({'filter_id': filterObj.id}), verify=False)
            if r.status_code != 200:
                raise Exception('ERROR: Unable to attach ATIP (%s) to filter (%s) on NTO chassis (%s) [status code = %s]' % (self.atipId, filterObj.filterId, self.myNTO.ipaddr, r.status_code))
            return (r.status_code)
        else:
            raise Exception('ERROR: Unable to attach ATIP (%s) because it is not in READY state [status = %s]' % (self.atipId, ATIPstatus))

    def detachResource(self, filterObj):
        print('Issue ReST put request to detach ATIP resource (%s) from filter object (%s) ...' % (self.atipName, filterObj.name))
        r = self.myNTO.sess.put('https://%s:%s/%s/%s/disable' % (self.myNTO.ipaddr, self.myNTO.ipport, self.myNTO.urls['api/atip_resources'], self.atipId), headers={'content-type': 'application/json'}, data=json.dumps({'filter_id': filterObj.filterId}), verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to detach ATIP (%s) from filter (%s) on NTO chassis (%s) [status code = %s]' % (self.atipId, filterObj.filterId, self.myNTO.ipaddr, r.status_code))
        return (r.status_code)

    def getDataMasking(self, cookies):
        print('Issue ReST request to get data masking config...')
        r = self.sess.get('https://%s/atie/rest/dataMasking' % self.ipaddr, cookies=cookies, verify=False)
        return (r.text)

    def getHeaderMasks(self, cookies):
        print('Issue ReST request to get data masking config...')
        r = self.sess.get('https://%s/atie/rest/headerMasks' % self.ipaddr, cookies=cookies, verify=False)
        return (r.text)

    def getFilterStats(self, cookies, filterName):

        print('Issue ReST request to get top filter stats config...')
        r = self.sess.get('https://%s/atie/rest/lists/top10?lists=rules' % self.ipaddr, cookies=cookies, verify=False)
        filterStats=r.json()
        if len([element.get('msg') for element in filterStats.get('stats').get('list') if element.get('msg') == filterName]) == 0:
            return -1
        print('Get stats for top filters')
        return [element.get('totalCount') for element in filterStats.get('stats').get('list') if
                element.get('msg') == filterName][0]


    def getTopAppStats(self, cookies, application):
        print('Issue ReST request to get top apps stats config...')
        r = self.sess.get('https://%s/atie/rest/lists/top10?lists=apps' % self.ipaddr, cookies=cookies, verify=False)
        topAppsStats=r.json()
        print('Get stats for top apps')
        return [element.get('totalCount') for element in topAppsStats.get('stats').get('list') if element.get('id') == application][0]

    def getNetflowStats(self, cookies, conn):
        print('Issue ReST request to get top apps stats config...')
        r = self.sess.get('https://%s/atie/rest/engineinfo/netflow' % self.ipaddr, cookies=cookies, verify=False)
        netflowStats = r.json()
        print('Get stats for top apps')
        return [element.get('data_records_sent') for element in netflowStats.get('status').get('conn')][int(conn)]

    def configureDataMasking(self, cookies, enable, char):
        print('Issue ReST request to configure data masking...')
        payload = {
            "enabled": enable,
            "maskChar": char
        }
        r = self.sess.post('https://%s/atie/rest/dataMasking' % self.ipaddr,
                           headers={'content-type': 'application/json'}, data=json.dumps(payload), cookies=cookies,
                           verify=False)
        pprint(r)
        return (r.status_code)

    def createHeaderMask(self, cookies, offset, name, header, length):
        print('Issue ReST request to create a header mask..')
        payload = {
            "startOffset": offset,
            "name": name,
            "header": header,
            "maskLength": length

        }

        r = self.sess.post('https://%s/atie/rest/headerMasks' % self.ipaddr,
                           headers={'content-type': 'application/json'}, data=json.dumps(payload), cookies=cookies,
                           verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to configure header mask %s on ATIP card (%s) [status code = %s]' % (
                self.name, self.ipaddr, r.status_code))

        return (r.status_code)

    def deleteHeaderMask(self, cookies):
        print('Issue ReST request to delete a header mask...')
        r = self.sess.get('https://%s/atie/rest/headerMasks' % self.ipaddr, cookies=cookies, verify=False)
        headerMaskList = r.json()
        if not headerMaskList:
            print('No header mask config present on system...')
        else:
            for k, item in headerMaskList.items():
                print('Deleting header mask %s (%s)' % (item['id'], item['name']))
                r = self.sess.delete('https://%s/atie/rest/headerMasks/%s' % (self.ipaddr, item['id']), cookies=cookies,
                                     verify=False)
                if r.status_code != 200:
                    raise Exception('ERROR: Unable to remove header mask (id: %s; name: %s [status code = %s])' % (
                    item['id'], item['name'], r.status_code))

    def deleteAllFilters(self, cookies):
        print('Issue ReST request to delete all filters...')
        r = self.sess.get('https://%s/atie/rest/ruleList' % self.ipaddr, cookies=cookies, verify=False)
        allFiltersList = r.json()
        if not allFiltersList.get('rule'):
            print('No filter config present on system...')
        else:
            for item in allFiltersList.get('rule'):
                print('Deleting filter %s (%s)' % (item['id'], item['name']))
                r = self.sess.delete('https://%s/atie/rest/rule/%s' % (self.ipaddr, item['id']), cookies=cookies,
                                     verify=False)
                if r.status_code != 200:
                    raise Exception('ERROR: Unable to remove filter (id: %s; name: %s [status code = %s])' % (
                    item['id'], item['name'], r.status_code))

    def resetStats(self, cookies):
        """ Description:  ReST method used to reset stats.

                    Arguments: n/a

                    Return Value: n/a """
        print('Issue ReST request to reset stats on ATIP..')
        r = self.sess.get('https://%s/atie/rest/system/resetStats' % self.ipaddr, cookies=cookies, verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to reset stats %s on ATIP card (%s) [status code = %s]' % (
                self.name, self.ipaddr, r.status_code))

        return (r.status_code)

    def configureFilter(self, cookies, application, name, headerId, vlanId, forwardState):
        print('Issue ReST request to create a new filter..')
        payload = {
                    "conditions": [
                        {
                        "apps": [
                         {
                         "id": application
                        }
                        ],
                     "type": "app"
                     }
                    ],
                    "id": 0,
                    "isExplore": "false",
                    "decrypted": "false",
                    "netflow": "false",
                    "name": name,
                    "hdrMaskUUIDS": headerId,
                    "forward": forwardState,
                    "transmitIds": vlanId
                    }

        r = self.sess.post('https://%s/atie/rest/rule/-1' % self.ipaddr,
                           headers={'content-type': 'application/json'}, data=json.dumps(payload), cookies=cookies,
                           verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to configure a filter %s on ATIP card (%s) [status code = %s]' % (
                self.name, self.ipaddr, r.status_code))




        return (r.status_code)

    def configureDecryptFilter(self, cookies,name,decrypt, vlanId, forwardState):
        print('Issue ReST request to create a new filter..')
        payload = {

                    "id": 0,
                    "isExplore": "false",
                    "decrypted": decrypt,
                    "netflow": "false",
                    "name": name,
                    "forward": forwardState,
                    "transmitIds": vlanId
                    }

        r = self.sess.post('https://%s/atie/rest/rule/-1' % self.ipaddr,
                           headers={'content-type': 'application/json'}, data=json.dumps(payload), cookies=cookies,
                           verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to configure a filter %s on ATIP card (%s) [status code = %s]' % (
                self.name, self.ipaddr, r.status_code))

        return (r.status_code)

    def configureNetflowGlobalConfig(self, cookies, enable):
        print('Issue ReST request to create a new filter..')
        payload = {
            "netflowL7ImsiMapping": "false",
            "netflowL7SrcLongitude": "true",
            "netflowL7SrcRegionName": "true",
            "netflowL7AppID": "true",
            "netflowL7SrcCityName": "true",
            "timeout": 30,
            "enabled": enable,
            "netflow_l7": "true",
            "netflowL7DeviceId": "true",
            "netflowL7EncryptionCipher": "true",
            "netflowL7SrcCountryCode": "true",
            "netflowL7SrcRegionCode": "true",
            "netflowL7URI": "false",
            "netflowL7EncryptionKeyLen": "true",
            "netflowL7DstRegionName": "true",
            "netflowL7DstCityName": "true",
            "netflowL7DstLatitude": "true",
            "netflowL7DstCountryName": "true",
            "enableVlanMaps": "false",
            "netflowL7Domain": "false",
            "netflowL7BrowserId": "true",
            "biFlows": "false",
            "netflowL7SrcLatitude": "true",
            "netflowL7DstLongitude": "true",
            "netflowL7DstRegionCode": "true",
            "netflowL7BrowserName": "true",
            "netflowL7DeviceName": "true",
            "netflowL7SrcCountryName": "true",
            "version": 10,
            "netflowL7AppName": "true",
            "netflowL7ConnEncryptType": "true",
            "netflowL7DNSText": "false",
            "netflowL7DstCountryCode": "true",
            "netflowL7UserAgent": "false"
        }

        r = self.sess.post('https://%s/atie/rest/netflowGlobalConfig' % self.ipaddr,
                           headers={'content-type': 'application/json'}, data=json.dumps(payload), cookies=cookies,
                           verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to configure netflow %s on ATIP card (%s) [status code = %s]' % (
                self.name, self.ipaddr, r.status_code))

        return (r.status_code)

    def configureNetflowCardConfig(self, cookies):
        print('Issue ReST request to create a new filter..')
        payload = {
        "gw": "192.168.30.100",
        "odid": 125,
        "netmask": "255.255.254.0",
        "ipMethod": "STATIC",
        "dns": "8.8.8.8",
        "id": 1,
        "ip_addr": "192.168.30.1",
        "enabled": "true",
        "mac": "00:1b:6e:01:4e:f3"
        }

        r = self.sess.post('https://%s/atie/rest/netflowCardConfig' % self.ipaddr,
                           headers={'content-type': 'application/json'}, data=json.dumps(payload), cookies=cookies,
                           verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to configure netflow %s on ATIP card (%s) [status code = %s]' % (
                self.name, self.ipaddr, r.status_code))

        return (r.status_code)

    def configureNetflowCollectorConfig(self, cookies):
        print('Issue ReST request to create a new filter..')
        payload = {
            "port": 4739,
            "proto": "UDP",
            "id": 0,
            "collector": "192.168.30.100",
            "enabled": "true"
        }

        r = self.sess.post('https://%s/atie/rest/netflowCollectorConfig' % self.ipaddr,
                           headers={'content-type': 'application/json'}, data=json.dumps(payload), cookies=cookies,
                           verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to configure netflow %s on ATIP card (%s) [status code = %s]' % (
                self.name, self.ipaddr, r.status_code))

        return (r.status_code)

    def configureGeoFilter(self, cookies, name, country, vlanId, forwardState):
        print('Issue ReST request to create a new filter..')
        payload = {
            "isExplore": "false",
            "netflow": "false",
            "deny": "false",
            "forward": forwardState,
            "transmitIds": vlanId,
            "name": name,
            "id": 0,
            "conditions": {
                "geos": {
                    "country": country
                },
                "type": "geo"
            },
            "decrypted": "false"

        }

        r = self.sess.post('https://%s/atie/rest/rule/-1' % self.ipaddr,
                           headers={'content-type': 'application/json'}, data=json.dumps(payload), cookies=cookies,
                           verify=False)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to configure a filter %s on ATIP card (%s) [status code = %s]' % (
                self.name, self.ipaddr, r.status_code))

        return (r.status_code)

    def pushRuleToNP(self, cookies):
        print('Issue ReST request to send filter rule to NP..')
        payload = {}

        r = self.sess.post('https://%s/atie/rest/rulePush/-1' % self.ipaddr,
                           headers={'content-type': 'application/json'}, data=json.dumps(payload), cookies=cookies,
                           verify=False)
        pprint(r)
        pprint(r.status_code)
        if r.status_code != 200:
            raise Exception('ERROR: Unable to send filter  rule %s on ATIP card (%s) [status code = %s]' % (
                self.name, self.ipaddr, r.status_code))


        return (r.status_code)

    def getCookie(self, username, password):

        tempDict = {'password': password, 'userid': username}
        r = requests.post('https://%s/atie/rest/login' % self.ipaddr, data=tempDict, verify=False)

        print(r.status_code)

        return r.cookies

    def runTraffic(self):

        subprocess.call(['E:\\bpsFiles\\bpsh-win32.exe', 'E:\\bpsFiles\\testBPS.tcl'])

        print('test over')





    def importConfig(self,file_name, ntoIp):
        """ Description:  ReST method used to import config on the NTO.

                    Arguments: n/a

                    Return Value: n/a """
        sess = requests.Session()
        userpass = ('admin', 'admin')
        sess.auth = userpass
        ipaddr = ntoIp
        # ipaddr = '10.215.157.81'
        ipport = '8000'
        url = 'https://%s:%s/api/actions/import' % (ipaddr, ipport)
        print(os.path.basename(file_name))

        payload = {'import_type': 'FULL_IMPORT_FROM_BACKUP'}
        multipart = MultipartEncoder(
            fields={'field0': (os.path.basename(file_name), open(file_name, 'rb'), 'application/octet-stream'),
                    'field1': ("json", json.dumps(payload), 'application/json')})
        print(multipart)
        print(multipart.content_type)

        # You must initialize logging, otherwise you'll not see debug output.
        logging.basicConfig()
        logging.getLogger().setLevel(logging.DEBUG)
        requests_log = logging.getLogger("requests.packages.urllib3")
        requests_log.setLevel(logging.DEBUG)
        requests_log.propagate = True
        print()
        print("####DOWNLOADING started")
        r = sess.post(url, verify=False, data=multipart, headers={'Content-Type': multipart.content_type})

        print("Header:" + str(r.request.headers) + "\n")
        print(r.text + "/n")
        print("Request" + str(r.request))
        # print("Data: " + str(r.request.data))
        if r.status_code != 200:
            raise Exception('ERROR: Unable to install build on (%s) [status code = %s]' % (ipaddr, r.status_code))
        else:
            print("####DOWNLOADING FINISHED for {}".format(ipaddr))


    def postSSLCertificateFiles(self, cookies, certName, certificate, privateKey):
        """ Description:  ReST method used to install SSl cert and key.

                            Arguments: n/a

                            Return Value: n/a """

        url = 'https://%s/atie/rest/sslCertificates' % self.ipaddr
        f1 = open(certificate, 'r')
        contentCertificate = f1.read()
        print(contentCertificate)

        f2 = open(privateKey, 'r')
        contentPrivateKey = f2.read()
        contentPrivateKey = contentPrivateKey[:11] + 'RSA ' + contentPrivateKey[11:]

        endIndex = contentPrivateKey.index('END')

        contentPrivateKey = contentPrivateKey[:endIndex + 4] + 'RSA ' + contentPrivateKey[endIndex + 4:]

        print(contentPrivateKey)

        filesDict = {'privateKey': (None, contentPrivateKey), 'name': (None, certName), 'enabled': (None, 'true'), 'certificate': (None, contentCertificate)}
        r = requests.post(url, files=filesDict, cookies=cookies, verify=False)

        print(r.status_code)

        print(r.json())

    def configureSSL(self, cookies, enable):
        print('Issue ReST request to configure SSL...')
        payload = {
            "passiveSSLDecryption": enable
        }
        r = self.sess.post('https://%s/atie/rest/sslGlobalConfig/' % self.ipaddr,
                           headers={'content-type': 'application/json'}, data=json.dumps(payload), cookies=cookies,
                           verify=False)
        pprint(r)
        return (r.status_code)

    def installLicenseATIP(self, file_name, ntoIp):
        """ Description:  ReST method used to install license on the NTO.

                    Arguments: n/a

                    Return Value: n/a """
        sess = requests.Session()
        userpass = ('admin', 'admin')
        sess.auth = userpass
        ipaddr = ntoIp
        # ipaddr = '10.215.157.81'
        ipport = '8000'
        url = 'https://%s:%s/api/actions/install_license' % (ipaddr, ipport)
        print(os.path.basename(file_name))

        multipart = MultipartEncoder(
            fields={'field0': (os.path.basename(file_name), open(file_name, 'rb'), 'application/octet-stream')})
        print(multipart)
        print(multipart.content_type)

        # You must initialize logging, otherwise you'll not see debug output.
        logging.basicConfig()
        logging.getLogger().setLevel(logging.DEBUG)
        requests_log = logging.getLogger("requests.packages.urllib3")
        requests_log.setLevel(logging.DEBUG)
        requests_log.propagate = True
        print()
        print("####DOWNLOADING started")
        r = sess.post(url, verify=False, data=multipart, headers={'Content-Type': multipart.content_type})

        print("Header:" + str(r.request.headers) + "\n")
        print(r.text + "/n")
        print("Request" + str(r.request))
        # print("Data: " + str(r.request.data))
        if r.status_code != 200:
            raise Exception(
                'ERROR: Unable to install build on (%s) [status code = %s]' % (ipaddr, r.status_code))
        else:
            print("####DOWNLOADING FINISHED for {}".format(ipaddr))



#TEST SCENARIO
# atip1=ATIP("atip1",my_ipaddr)
#pprint ("Import NTO configuration")

# nto = NTO()
# nto.ipaddr = '10.215.157.126'
# nto.ipport='8000'
# ntoPort = NTOPort()
# ntoPort.myNTO=nto
# print(ntoPort.getPortStats("P47","TP_TOTAL_TX_COUNT_PACKETS"))

#nto.installLicense(file_name="IxiaLicenseB_2097_Cristina_20160916_1.txt")

#atip1.installLicense(file_name="IxiaLicenseB_2097_Cristina_20160916_1.txt", ntoIp="10.215.157.127")
#nto.importConfig(file_name="10.218.164.211_7300_20160913_4.5.1.7_FULL_BACKUP.ata")
#cookies=atip1.getCookie("admin","admin")
#atip1.postSSLCertificateFiles(cookies, "certAutomation", "cert1024.crt", "key1024.key")
#atip1.configureSSL(cookies,True)
#pprint (cookies)
#pprint ("Get datamasking current config")
#pprint(atip1.getDataMasking(cookies))
#pprint ("Enable data masking")
#atip1.configureDataMasking(cookies, True,"C")
#pprint ("Configure a header mask")
#atip1.createHeaderMask(cookies,6,"L2","L2",6)
#pprint ("Create a new filter with an app condition and a header mask rule")
#atip1.configureFilter(cookies,"netflix", "demoFilter", 0, 100, "true")
#atip1.configureDecryptFilter(cookies,"ssl_decrypt_fwd", "true", 100, "true")
#atip1.configureDecryptFilter(cookies,"ssl_decrypt", "true", 100, "false")
#atip1.configureGeoFilter(cookies,"China", "CN", 100, "true")
#atip1.pushRuleToNP(cookies)
#pprint ("Run traffic")
#subprocess.call(['E:\\bpsFiles\\bpsh-win32.exe','E:\\bpsFiles\\testBPS.tcl'])
#pprint ("Get filter stats")
#filterStats=atip1.getFilterStats(cookies)
#pprint(filterStats)
#pprint(filterStats.get('stats').get('list').get('totalCount'))
#pprint ("Get top apps stats")
#topAppStats=atip1.getTopAppStats(cookies)
#sessions = [element.get('totalCount') for element in topAppStats.get('stats').get('list') if element.get('id') == 'facebook']
#pprint (sessions[0])









