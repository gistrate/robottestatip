from http import HTTPStatus

import jsonpickle

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.Collector import Collector
from atipAutoFrwk.config.atip.NetflowCollectorConfig import NetflowCollectorConfig
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.NetflowStats import NetflowStats
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.services.atip.NetflowService import NetflowService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Netflow import Netflow
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
import ipaddress

from scripts.performance.ixFlowHighRate.BpsTestActions import ChangeNetflowVersion, ChangeNetflowState, \
    ChangeIxFlowState, ChangeIpMethod


class SuiteConfig(StatsDefaultConfig):
    httpService = ApplicationType.HTTP_80.appName
    maximizedDashboard = DashboardType.MAXIMIZED
    netflowPkts = NetflowStats.TOTAL_PKTS
    netflowRecords = NetflowStats.DATA_RECORDS
    statsType = StatsType.APPS
    timeInterval = TimeInterval.HOUR
    bpsDuration = 1200
    firstCollectorIP = StatsDefaultConfig.envVars.netflowCollectorConnectionConfig.host
    # ATIP config
    atipConfig = AtipConfig()
    netflowGlobalConfig = NetflowConfig()
    netflowGlobalConfig.enabled = True
    netflowGlobalConfig.version = 10
    netflowGlobalConfig.enableAllIxFlowFields(netflowGlobalConfig)
    atipConfig.setNetflowGlobal(netflowGlobalConfig)
    atipConfig.netflowCards[0].cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].enabled = True
    atipConfig.netflowCards[0].cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].odid = 100
    atipConfig.netflowCards[0].cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].ipMethod = 'STATIC'
    atipConfig.netflowCards[0].cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].dns = '8.8.8.8'
    atipConfig.netflowCards[0].cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].netmask = '255.255.255.0'
    atipConfig.netflowCards[0].cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].gw = '192.168.30.100'
    atipConfig.netflowCards[0].cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].ip_addr = '192.168.30.1'
    firstCollector = Collector(0, 100, 'Samples', True, 'UDP', 4739, firstCollectorIP)
    atipConfig.netflowCollectors[0].collectorList[0]=firstCollector
    netflowGeoFilter = FilterConfig("testGeoFilter")
    FilterConditionConfig.addGeoCondition(netflowGeoFilter, GeoLocation.PAKISTAN)
    netflowGeoFilter.netflow = True
    netflowGeoFilter.collectors = [0]
    atipConfig.addFilters(netflowGeoFilter)
    bpsTestConfig = BpsTestConfig('IxFlow-AllFields_highRate_20min', 1200)
    changeNetflowVersion = ChangeNetflowVersion(StatsDefaultConfig.atipSession)
    changeNetflowState = ChangeNetflowState(StatsDefaultConfig.atipSession)
    changeIxFlowState = ChangeIxFlowState(StatsDefaultConfig.atipSession)
    changeIpMethod = ChangeIpMethod(StatsDefaultConfig.atipSession,atipConfig.netflowCards[0].cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]])
