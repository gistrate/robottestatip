*** Settings ***
Documentation  IxFlow High Rate test suite.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    atipAutoFrwk.webApi.traffic.BpsOperations
Library    atipAutoFrwk.webApi.traffic.Login  WITH NAME  bpsLogin
Library    atipAutoFrwk.webApi.traffic.PortsOperations  WITH NAME  portsOperations
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.services.atip.stats.PerformanceService
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.services.atip.NetflowService

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP and BPS
Suite Teardown     Clean ATIP

Test Template  Configure Device and Run BPS Test

*** Test Cases ***

TC000967139  TC000967139  ${changeNetflowVersion}  240
    [Template]  Configure Device with all IxFlow fields and Run BPS Test

TC000965700  TC000965700  ${changeNetflowState}  240
    [Template]  Configure Device with all IxFlow fields and Run BPS Test

TC000967142  TC000967142  ${changeIxFlowState}  240
    [Template]  Configure Device with all IxFlow fields and Run BPS Test

TC000965699  TC000965699  None  240
    [Template]  Configure Device with all IxFlow fields and Run BPS Test

TC000967580  TC000967580    ${changeNetflowState}  240
    [Template]  Configure Device with DNS fields and Run BPS Test

TC000967579  TC000967579  None  240
    [Template]  Configure Device with DNS fields and Run BPS Test

TC000967633  TC000967633  ${changeIpMethod}  120
    [Template]  Configure Device with DNS fields and Run BPS Test


*** Keywords ***
Configure Device with all IxFlow fields and Run BPS Test
    [Arguments]  ${testId}  ${action}  ${validateFrequency}

    # Reset Stats
    resetStats  ${atipSession}
    sshRemoveCoreFilesInNP  ${envVars}
	Run Keyword Unless  '${action}' == 'None'  addBpsTestAction   ${action}
	performActionUntilBpsTestCompletion  ${envVars}  ${bpsSession}  ${bpsTestConfig}  ${validateFrequency}
    clearBpsTestAction
	# Validate no crashes in core files
    validateNoCrashesInNPCoreFile  ${atipSession}  ${envVars}  ${testId}


*** Keywords ***
Configure Device with DNS fields and Run BPS Test
    [Arguments]  ${testId}  ${action}  ${validateFrequency}

    # Reset Stats
    resetStats  ${atipSession}
    ${fields} =  create list  netflowL7Domain  netflowL7UserAgent  netflowL7DNSText  netflowL7URI
    enableIxFlowFields  ${atipSession}  ${fields}
    sshRemoveCoreFilesInNP  ${envVars}
	Run Keyword Unless  '${action}' == 'None'  addBpsTestAction   ${action}
    performActionUntilBpsTestCompletion  ${envVars}  ${bpsSession}  ${bpsTestConfig}  ${validateFrequency}
    clearBpsTestAction
	# Validate no crashes in core files
    validateNoCrashesInNPCoreFile  ${atipSession}  ${envVars}  ${testId}

Configure ATIP and BPS
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    configureAtip  ${atipSession}  ${atipConfig}  ${envVars}

    # Configure BPS
    bpslogin.Login  ${bpsSession}
    portsOperations.reserveBpsPorts  ${bpsSession}  ${envVars.bpsSlot}  ${envVars.bpsPortList}  ${envVars.bpsPortGroup}

Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    atipLogout.Logout  ${atipSession}



