from http import HTTPStatus

import jsonpickle
from atipAutoFrwk.webApi.atip.Netflow import Netflow
from atipAutoFrwk.webApi.traffic.BpsOperations import IBpsTestAction
import time


class ChangeNetflowVersion(IBpsTestAction):

    def __init__(self, atipSession):
        self.atipSession = atipSession

    def doAction(self):

        print("------------> switch netflow version")

        globalConfig = Netflow.getGlobalConfig(self.atipSession)
        # switch
        globalConfig.version = self.switchNetflowVersion(globalConfig.version)
        Netflow.updateGlobalConfig(self.atipSession, globalConfig)
        obj1 = Netflow.getGlobalConfig(self.atipSession)
        print("current netflow version {}".format(jsonpickle.encode(obj1.version)))
        # switch back
        globalConfig.version = self.switchNetflowVersion(globalConfig.version)
        Netflow.updateGlobalConfig(self.atipSession, globalConfig)
        obj2 = Netflow.getGlobalConfig(self.atipSession)
        print("current netflow version {}".format(jsonpickle.encode(obj2.version)))

    def switchNetflowVersion(self, netflowVersion):
        if netflowVersion == 10:
            return 9
        return 10


class ChangeNetflowState(IBpsTestAction):
    def __init__(self, atipSession):
        self.atipSession = atipSession

    def doAction(self):

        print("------------> switch netflow state")

        globalConfig = Netflow.getGlobalConfig(self.atipSession)
        # switch
        globalConfig.enabled = self.switchNetflowState(globalConfig.enabled)
        Netflow.updateGlobalConfig(self.atipSession, globalConfig)
        obj1= Netflow.getGlobalConfig(self.atipSession)
        print("current netflow state {}".format(jsonpickle.encode(obj1.enabled)))
        # switch back
        globalConfig.enabled = self.switchNetflowState(globalConfig.enabled)
        Netflow.updateGlobalConfig(self.atipSession, globalConfig)
        obj2= Netflow.getGlobalConfig(self.atipSession)
        print("current netflow state {}".format(jsonpickle.encode(obj2.enabled)))

    def switchNetflowState(self, netflowState):
        if netflowState == True:
            return False
        return True


class ChangeIxFlowState(IBpsTestAction):
    def __init__(self, atipSession):
        self.atipSession = atipSession

    def doAction(self):

        print("------------> switch ixFlow state")

        globalConfig = Netflow.getGlobalConfig(self.atipSession)
        # switch
        globalConfig.biFlows = self.switchIxFlowState(globalConfig.biFlows)
        Netflow.updateGlobalConfig(self.atipSession, globalConfig)
        obj1 = Netflow.getGlobalConfig(self.atipSession)
        print("current netflow ixflow state {}".format(jsonpickle.encode(obj1.version)))
        # switch back
        globalConfig.biFlows = self.switchIxFlowState(globalConfig.biFlows)
        Netflow.updateGlobalConfig(self.atipSession, globalConfig)
        obj2 = Netflow.getGlobalConfig(self.atipSession)
        print("current netflow ixflow state {}".format(jsonpickle.encode(obj2.version)))

    def switchIxFlowState(self, ixFlowState):
        if ixFlowState == True:
            return False
        return True

class ChangeIpMethod(IBpsTestAction):
    def __init__(self, atipSession, cardConfig):
        self.atipSession = atipSession
        self.cardConfig = cardConfig

    def doAction(self):
        ':type cardConfig: Card'

        print("------------> switch ip method")
        # switch
        print("cardConfig {}".format(jsonpickle.encode(self.cardConfig)))
        self.cardConfig.ipMethod = 'DHCP'
        Netflow.updateCard(self.atipSession, self.cardConfig)
        # switch back
        time.sleep(120)
        self.cardConfig.ipMethod = 'STATIC'
        Netflow.updateCard(self.atipSession, self.cardConfig)



