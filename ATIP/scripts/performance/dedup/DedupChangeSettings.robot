*** Settings ***
Documentation  Dedup change settings test suite.

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    atipAutoFrwk.webApi.traffic.BpsOperations
Library    atipAutoFrwk.webApi.traffic.Login  WITH NAME  bpsLogin
Library    atipAutoFrwk.webApi.traffic.PortsOperations  WITH NAME  portsOperations
Library    atipAutoFrwk.services.atip.NPService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.services.atip.stats.PerformanceService
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.services.atip.stats.ValidateDedupStatsService
Library    atipAutoFrwk.webApi.atip.Filters
Resource    scripts/functional/dedup/DedupConfigLib.robot

Default Tags  ATIP  hardware  virtual
Suite Setup  Create Dedup Config
Test Template  Validate Dedup
Suite Teardown  Dedup Teardown

*** Test Cases ***
TC000922970  TC000922970  ${changeDedupState}  ${bpsTestConfigChange1}  180
TC000922978  TC000922978  ${changeHeaderValue}  ${bpsTestConfigChange1}  180
TC000933349  TC000933349  ${changeExpectedDataRate}   ${bpsTestConfigChange1}  180
TC000933346  TC000933346  ${changeDedupState}  ${bpsTestConfigChange2}  180
TC000933347  TC000933347  ${changeHeaderValue}  ${bpsTestConfigChange2}  180
TC000933350  TC000933350  ${changeExpectedDataRate}  ${bpsTestConfigChange2}  180


*** Keywords ***

Validate Dedup
    [Arguments]  ${testId}  ${action}  ${bpsTest}  ${validateFrequency}
    # Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    # Configure BPS
    bpslogin.Login  ${bpsSession}
    portsOperations.reserveBpsPorts  ${bpsSession}  ${envVars.bpsSlot}  ${envVars.bpsPortList}  ${envVars.bpsPortGroup}
    sshRemoveCoreFilesInNP  ${envVars}
    # Configure Dedup
	configureAtip  ${atipSession}  ${atipConfig}
	Run Keyword Unless  '${action}' == 'None'  addBpsTestAction   ${action}
    performActionUntilBpsTestCompletion  ${envVars}  ${bpsSession}  ${bpsTest}  ${validateFrequency}
    clearBpsTestAction
	# Validate no crashes in core files
    validateNoCrashesInNPCoreFile  ${atipSession}  ${envVars}  ${testId}
    validateNoResetsInNPDumpFile  ${atipSession}  ${envVars}  ${testId}

Dedup Teardown
    Create NTO Config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    logout  ${atipSession}


