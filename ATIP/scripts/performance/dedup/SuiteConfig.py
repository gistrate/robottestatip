
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.DedupGlobalConfig import DedupGlobalConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.services.atip.bpsTestActions.VerifyDedupDropAction import VerifyDedupDropAction, ChangeDedupState, ChangeExpectedDataRate, ChangeHeaderValue, VerifyDedupUnhandledPkts
from atipAutoFrwk.webApi.WebApiSession import WebApiSession




class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    timeInterval = TimeInterval.HOUR
    # ATIP config
    dedupFilter = FilterConfig("DedupFilter")
    FilterConditionConfig.addAppCondition(dedupFilter, ApplicationType.NETFLIX)
    dedupFilter.forward = True
    dedupFilter.transmitIds = [100]

    atipConfig = AtipConfig()
    dedupConfig = DedupGlobalConfig("844600", True, "NONE")
    atipConfig.dedup = dedupConfig
    atipConfig.addFilters(dedupFilter)
    verifyDedupVariation = VerifyDedupDropAction(atipSession)
    verifyDedupUnhandledPkts = VerifyDedupUnhandledPkts(atipSession)
    changeDedupState = ChangeDedupState(atipSession)
    changeHeaderValue = ChangeHeaderValue(atipSession)
    changeExpectedDataRate = ChangeExpectedDataRate(atipSession)
    bpsTestConfigChange1 = BpsTestConfig('10Gbps-Netflix_30min', 1800)
    bpsTestConfigChange2 = BpsTestConfig('AppSimMix-2ports_30min', 1800)



