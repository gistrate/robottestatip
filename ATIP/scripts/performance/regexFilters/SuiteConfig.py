from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.DataMaskingConfig import DataMaskingConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType

class SuiteConfig(StatsDefaultConfig):

   def configureRgxFilter(self, webApiSession, filterName, regex, vlanId=100):

        atipConfig = AtipConfig()

        dataMasking = DataMaskingConfig(True)
        atipConfig.setDataMasking(dataMasking)

        filterConfig = FilterConfig(filterName)
        filterConfig.forward = True
        FilterConditionConfig.addCondition(filterConfig, FilterConditionType.RGX, regex)
        if self.envVars.atipType == AtipType.HARDWARE:
            filterConfig.addTransmitIds(vlanId)
        atipConfig.addFilters(filterConfig)

        return atipConfig
