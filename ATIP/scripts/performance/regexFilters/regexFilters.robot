*** Settings ***
Documentation  Regex filters test suite.

Variables  SuiteConfig.py

Library    scripts.performance.regexFilters.SuiteConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    atipAutoFrwk.webApi.traffic.BpsOperations
Library    atipAutoFrwk.webApi.traffic.Login  WITH NAME  bpsLogin
Library    atipAutoFrwk.webApi.traffic.PortsOperations  WITH NAME  portsOperations
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk/services/atip/stats/PerformanceService.py

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP BPS
Suite Teardown     Clean ATIP

Test Template  Configure Device and Run BPS Test

*** Test Cases ***

regex_perf_65_high_850Mbps_vatip    regex_perf_850Mbps_vatip    regex_65_high    .*\\x0d\\x0a.*    1230    60    bits    850000000      15    0.001    5
    [Tags]  virtual
regex_perf_65_low_850Mbps_vatip     regex_perf_850Mbps_vatip    regex_65_low     H[A-Z]{3}       1230    60    bits    850000000      15    0.001    5
    [Tags]  virtual
regex_perf_65_high_1600_vatip    regex_perf_1600_vatip       regex_13_high    .*\\x0d\\x0a.*    1230    60    bits    1600000000     15    2.5    5
    [Tags]  virtual
regex_perf_65_low_1600_vatip     regex_perf_1600_vatip       regex_13_low     H[A-Z]{3}       1230    60    bits    1600000000     15    2.5    5
    [Tags]  virtual
regex_perf_65_high_6_5Gbps_default    regex_perf_6_5Gbps_default  regex_65_high    .*\\x0d\\x0a.*    1230    60    bits    6500000000     15    0    5
    [Tags]  hardware
regex_perf_65_low_6_5Gbps_default     regex_perf_6_5Gbps_default  regex_65_low     H[A-Z]{3}       1230    60    bits    6500000000     15    0    5
    [Tags]  hardware
regex_perf_65_high_13Gbps_default    regex_perf_13Gbps_default   regex_13_high    .*\\x0d\\x0a.*    1230    60    bits    13000000000    15    0    5
    [Tags]  hardware
regex_perf_65_low_13Gbps_default     regex_perf_13Gbps_default   regex_13_low     H[A-Z]{3}       1230    60    bits    13000000000    15    0    5
    [Tags]  hardware
regex_perf_65_high_26Gbps_default    regex_perf_26Gbps_default   regex_26_high    .*\\x0d\\x0a.*    1230    60    bits    26000000000    15    0    5
    [Tags]  hardware
regex_perf_65_low_26Gbps_default     regex_perf_26Gbps_default   regex_26_low     H[A-Z]{3}       1230    60    bits    26000000000    15    0    5
    [Tags]  hardware

*** Keywords ***
Configure Device and Run BPS Test
    [Arguments]  ${bpsFile}  ${filterName}  ${regex}  ${duration}  ${validateDropRateFrequency}  ${rateType}  ${rate}  ${percent}  ${hwDropPercent}  ${swDropPercent}

    # Reset Stats
    resetStats  ${atipSession}

	sshRemoveCoreFilesInNP  ${envVars}

	# Create Regex filterCapture
	${atipConfig}=  configureRgxFilter  ${atipSession}  ${filterName}  ${regex}
    createConfig  ${atipSession}  ${atipConfig}  ${envVars}

    # Convert hwDropPercent to float (vAtip uses float values)
    ${hwFloatDropPcnt} =  Convert to Number  ${hwDropPercent}

	# Send traffic and verify performance
	checkPerformance  ${envVars}  ${atipSession}  ${bpsSession}  ${bpsFile}  ${duration}  ${rateType}  ${validateDropRateFrequency}  ${rate}  ${percent}  ${hwFloatDropPcnt}  ${swDropPercent}

	# Validate no crashes in core files
	validateNoCrashesInNPCoreFile  ${atipSession}  ${envVars}  ${TEST NAME}


Configure ATIP BPS
    atipLogin.Login  ${atipSession}
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}

    # Configure BPS
    bpslogin.Login  ${bpsSession}
    portsOperations.reserveBpsPorts  ${bpsSession}  ${envVars.bpsSlot}  ${envVars.bpsPortList}  ${envVars.bpsPortGroup}

Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    atipLogout.logout  ${atipSession}
