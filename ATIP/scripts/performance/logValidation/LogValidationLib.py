from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType
from atipAutoFrwk.webApi.atip.StaticApps import StaticApps
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.data.atip.MaxItemsPerCategory import MaxItemsPerCategory
from atipAutoFrwk.services.atip.FiltersService import FiltersService

class LogValidationLib(object):

    @classmethod
    def createAllStaticAppFilters(cls, webApiSession):

        Filters.deleteAllFilters(webApiSession)

        # get static applications list
        staticAppList = StaticApps.getStaticApps(webApiSession)

        if staticAppList == None:
             raise Exception('The number of static applications retrieved is 0, no filter can be created')
        elif len(staticAppList) < MaxItemsPerCategory.MAX_STATIC_APPS.maxNumber:
             raise Exception('The number of static applications retrieved is {}, should be {}.'.format(len(staticAppList), MaxItemsPerCategory.MAX_STATIC_APPS.maxNumber))

        allFilters = []
        # create a filter for each static application from staticAppList
        for staticApp in staticAppList:
            filterConfig = FilterConfig()
            filterConfig.name = '{:*<4}'.format(staticApp['id'])
            FilterConditionConfig.addCondition(filterConfig, FilterConditionType.APP, staticApp)
            allFilters.append(filterConfig)
        FiltersService.createFilter(webApiSession, allFilters)
