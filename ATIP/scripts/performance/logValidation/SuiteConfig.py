from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.webApi.WebApiSession import WebApiSession


class SuiteConfig(object):

    # environment
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)

    npId=1



