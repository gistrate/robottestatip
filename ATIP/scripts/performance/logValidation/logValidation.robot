*** Settings ***
Documentation  Log Validation test suite.

Variables  SuiteConfig.py

Library    scripts.performance.logValidation.LogValidationLib
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.webApi.traffic.Login  WITH NAME  bpsLogin
Library    atipAutoFrwk.webApi.traffic.PortsOperations  WITH NAME  portsOperations
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.webApi.atip.DataMasking
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.atip.stats.DebugPage.DebugPage

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP BPS
Suite Teardown     atipLogout.Logout  ${atipSession}


*** Test Cases ***

# validate NP Logfile
TC000603968   DYNAMIC_1APP-tom_test-1_com   test-1.com   dynamic-app-gen.log.0
    [Template]  Dynamic Apps New Application Is Detected By NP

# validate no NP crashes
TC000692566   ATIP-Perf_30Gbps-10minutes
    [Template]  Configure Device and Run BPS Test
TC000699603   ATIP-Perf_30Gbps_VLANs-10minutes
    [Template]  Configure Device and Run BPS Test
TC000929526   ATIP-MaxSPS_StackScrambler_10min
    [Template]  Configure Device and Run BPS Test

CreateFilterForEachStaticApp-checkForErrors
    Create Filter For Each Static App

CheckForParseErrorsOnNPBootUp
    Parse Error Check

*** Keywords ***
Dynamic Apps New Application Is Detected By NP
    [Arguments]  ${bpsFile}  ${searchString}  ${fileToSearch}

    #Start sending traffic
    Run BPS Traffic  ${bpsFile}

    Sleep  10s

	validateStringInFile  ${atipSession}  ${envVars}  ${fileToSearch}  ${searchString}  atip-mw-daemon


Configure Device and Run BPS Test
    [Arguments]  ${bpsFile}

    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}

	sshRemoveCoreFilesInNP  ${envVars}
	sshRemoveDumpFilesInNP  ${envVars}

    #Start sending traffic
    Run BPS Traffic  ${bpsFile}

	# Validate no crashes in core files or watchdog resets
	validateNoCrashesInNPCoreFile  ${atipSession}  ${envVars}  ${TEST NAME}
    validateNoResetsInNPDumpFile  ${atipSession}  ${envVars}  ${TEST NAME}

Run BPS Traffic
    [Arguments]  ${bpsScript}

    ${bpsTestConfig}=  createBpsTestConfig  ${bpsScript}  840
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

Create Filter For Each Static App
	
    # ssh to the NP and delete the /var/log/all file
    sshRemoveNPLogFile  ${envVars}

    # clean DB and restart NP; a new /var/log/all file will be create dictionary
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    resetAtipAndWaitUntilNPReady  ${atipSession}  ${envVars}

    # create one filter for each static application
    atipLogin.Login  ${atipSession}
    createAllStaticAppFilters  ${atipSession}

    # validate that the /var/log/all file does not contain any parse errors which would indicate that the XML files are broken
    validateNoParseErrorNPLogFile  ${atipSession}  ${envVars}

    # repeat validation for ERROR
    validateNoErrorNPLogFile  ${atipSession}  ${envVars}


Parse Error Check
    # ssh to the NP and delete the /var/log/all file
    sshRemoveNPLogFile  ${envVars}

    # clean DB and restart NP; a new /var/log/all file will be create dictionary
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    resetAtipAndWaitUntilNPReady  ${atipSession}  ${envVars}

    # validate that the /var/log/all file does not contain any parse errors which would indicate that the XML files are broken
    atipLogin.Login  ${atipSession}
    validateNoParseErrorNPLogFile  ${atipSession}  ${envVars}

    # repeat validation for ERROR
    validateNoErrorNPLogFile  ${atipSession}  ${envVars}

Configure ATIP BPS
    atipLogin.Login  ${atipSession}

    # Configure BPS
    bpslogin.Login  ${bpsSession}
    portsOperations.reserveBpsPorts  ${bpsSession}  ${envVars.bpsSlot}  ${envVars.bpsPortList}  ${envVars.bpsPortGroup}

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}