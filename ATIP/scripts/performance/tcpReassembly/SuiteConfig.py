
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.webApi.WebApiSession import WebApiSession




class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    timeInterval = TimeInterval.HOUR
    defaultGroupId = 0





