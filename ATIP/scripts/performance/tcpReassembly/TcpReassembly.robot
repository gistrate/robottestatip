*** Settings ***
Documentation  Tcp Reassembly test suite.

Variables  SuiteConfig.py

Library    scripts.performance.regexFilters.SuiteConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    atipAutoFrwk.webApi.traffic.BpsOperations
Library    atipAutoFrwk.webApi.traffic.Login  WITH NAME  bpsLogin
Library    atipAutoFrwk.webApi.traffic.PortsOperations  WITH NAME  portsOperations
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.services.atip.stats.PerformanceService
Library    atipAutoFrwk.webApi.atip.TcpMode

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP BPS


Test Template  Configure Device and Run BPS Test

*** Test Cases ***

ATIP-Perf_AppSimMix_26Gbps_30min_Perfect_Storm_low    ATIP-Perf_AppSimMix_26Gbps_30min_Perfect_Storm_low    1900    10    bits    26000000000    10    0.1    0
    [Tags]  hardware


*** Keywords ***
Configure Device and Run BPS Test
    [Arguments]  ${bpsFile}  ${duration}  ${validateDropRateFrequency}  ${rateType}  ${rate}  ${percent}  ${hwDropPercent}  ${swDropPercent}
    # Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    # Reset Stats
    resetStats  ${atipSession}

	sshRemoveCoreFilesInNP  ${envVars}

	# Enable TCP reassembly
    ${tcpConfig0}=  getTcpConfig  ${atipSession}  ${defaultGroupId}
    ${tcpConfig0.enabledAlways}=  set variable  True
    updateTcpConfig  ${atipSession}  ${tcpConfig0}  ${defaultGroupId}

    # Convert hwDropPercent to float (vAtip uses float values)
    ${hwFloatDropPcnt} =  Convert to Number  ${hwDropPercent}

	# Send traffic and verify performance
	checkPerformance  ${envVars}  ${atipSession}  ${bpsSession}  ${bpsFile}  ${duration}  ${rateType}  ${validateDropRateFrequency}  ${rate}  ${percent}  ${hwFloatDropPcnt}  ${swDropPercent}

	# Validate no crashes in core files
	validateNoCrashesInNPCoreFile  ${atipSession}  ${envVars}  ${TEST NAME}
	validateNoResetsInNPDumpFile  ${atipSession}  ${envVars}  ${TEST NAME}

	# Disable TCP reassembly
    ${tcpConfig0}=  getTcpConfig  ${atipSession}  ${defaultGroupId}
    ${tcpConfig0.enabledAlways}=  set variable  False
    updateTcpConfig  ${atipSession}  ${tcpConfig0}  ${defaultGroupId}


Configure ATIP BPS
    atipLogin.Login  ${atipSession}

    # Configure BPS
    bpslogin.Login  ${bpsSession}
    portsOperations.reserveBpsPorts  ${bpsSession}  ${envVars.bpsSlot}  ${envVars.bpsPortList}  ${envVars.bpsPortGroup}


