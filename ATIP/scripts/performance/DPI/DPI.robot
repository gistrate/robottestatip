*** Settings ***
Documentation  DPI FPS Performance test suite.

Variables  scripts/functional/stats/lib/StatsDefaultConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    atipAutoFrwk.webApi.traffic.BpsOperations
Library    atipAutoFrwk.webApi.traffic.Login  WITH NAME  bpsLogin
Library    atipAutoFrwk.webApi.traffic.PortsOperations  WITH NAME  portsOperations
Library    atipAutoFrwk.services.atip.NPService
Library    scripts/functional/stats/lib/StatsDefaultConfig.py
Library    atipAutoFrwk/services/atip/stats/PerformanceService.py


Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP BPS
Suite Teardown     atipLogout.Logout  ${atipSession}

Test Template  Configure Device and Run BPS Test

*** Test Cases ***
#                                                                                 bpsTest                                                                       duration  intervalPerformanceCheck
#                                                                                 rateType  rate       percent       hwDropPercent  swDropPercent
# Througput Validation
vatip_1Gbps_simpleHTTP                                                          vatip_1Gbps_simpleHTTP                                                           300               10
                                                                         ...    bits    1000000000        5                0.5           0
    [Tags]  virtual
vATIP_1.5Gbps_5min_US_MobileCarriers2016_newFlowset                             vATIP_1.5Gbps_5min_US_MobileCarriers2016_newFlowset                              300               10
                                                                         ...    bits    1500000000        5                0.5           0
    [Tags]  virtual
vATIP_1.5Gbps_5min_US_MobileCarriers2016_10Kfps_30KsimActive                    vATIP_1.5Gbps_5min_US_MobileCarriers2016_10Kfps_30KsimActive                     300               10
                                                                         ...    bits    1500000000        5                0.5           0
    [Tags]  virtual
vATIP_1.5Gbps_5min_US_MobileCarriers2016_10Ksess_30Ksimflows_448MSS_1792Window  vATIP_1.5Gbps_5min_US_MobileCarriers2016_10Ksess_30Ksimflows_448MSS_1792Window   300               10
                                                                         ...    bits    1500000000        5                0.5           0
    [Tags]  virtual
vATIP_3Gbps_5min_US_MobileCarriers2016_10Ksess_30Ksim_MSS1460                   vATIP_3Gbps_5min_US_MobileCarriers2016_10Ksess_30Ksim_MSS1460                    300               10
                                                                         ...    bits    3000000000        8                0.5           0
    [Tags]  virtual
vATIP_2Gbps_5min_US_MobileCarriers2016_10Ksess_30Ksim_1460MSS                   vATIP_2Gbps_5min_US_MobileCarriers2016_10Ksess_30Ksim_1460MSS                    300               10
                                                                         ...    bits    2000000000        5                0.5           0
    [Tags]  virtual
BPS_VE_AppSim_HTTP_1M-Max_Throughput_vatip                                      BPS_VE_AppSim_HTTP_1M-Max_Throughput_vatip                                       300               10
                                                                         ...    bits   2000000000        5                0.5           0
    [Tags]  virtual
BPS_VE_AppSim_HTTP_1M-Max_Throughput_vatip_3Gbps_5min_MSS1460                   BPS_VE_AppSim_HTTP_1M-Max_Throughput_vatip_3Gbps_5min_MSS1460                    300               10
                                                                         ...    bits    3000000000        5                0.5           0
    [Tags]  virtual
BPS_VE_AppSim_HTTP_1M-Max_Throughput_vatip_3Gbps_5min_MSS448_TCPwin65K          BPS_VE_AppSim_HTTP_1M-Max_Throughput_vatip_3Gbps_5min_MSS448_TCPwin65K           300               10
                                                                         ...    bits    3000000000        5                0.5           0
    [Tags]  virtual

# DPI Validation
vatip_1.5Gbps_RR_64bytePacket_10min                                             vatip_1.5Gbps_RR_64bytePacket_10min                                               600              10
                                                                         ...    bits    1500000000        5                7           0
    [Tags]  virtual
vatip_500Mbps_RR_512bytePacket_10min                                            vatip_500Mbps_RR_512bytePacket_10min                                              600              10
                                                                         ...    bits     500000000        5                0.5           0
    [Tags]  virtual
vatip_1Gbps_RR_512bytePacket_10min                                              vatip_1Gbps_RR_512bytePacket_10min                                                600              10
                                                                         ...    bits    1000000000        5                0.5           0
    [Tags]  virtual
vatip_3Gbps_RR_1518bytePacket_10min                                             vatip_3Gbps_RR_1518bytePacket_10min                                               600              10
                                                                         ...    bits    3000000000        5                0.5           0
    [Tags]  virtual

# FPS Flows Per Second Validation
ATIePerf_AppSim50k-FPS_20min                                                    ATIePerf_AppSim50k-FPS_20min                                                     1200              10
                                                                         ...    sessions     50000        5                0.5           0
    [Tags]  virtual


# AppSimFPS Validation
#                                                                                 bpsTest                                                                       duration  intervalPerformanceCheck
#                                                                                 rateType  rate       percent       hwDropPercent  swDropPercent
ATIePerf_AppSim100k-FPS_20min                                                   ATIePerf_AppSim100k-FPS_20min                                                    1200              10
                                                                         ...    sessions    100000        5               0            0
    [Tags]  hardware
ATIePerf_AppSim200k-FPS_20min                                                   ATIePerf_AppSim200k-FPS_20min                                                    1200              10
                                                                         ...    sessions    200000        5               0            0
    [Tags]  hardware
ATIePerf_AppSim300k-FPS_20min                                                   ATIePerf_AppSim300k-FPS_20min                                                    1200              10
                                                                         ...    sessions    300000        5               0            0
    [Tags]  hardware
ATIePerf_AppSim400k-FPS_20min                                                   ATIePerf_AppSim400k-FPS_20min                                                    1200              10
                                                                         ...    sessions    400000        5               0            0
    [Tags]  hardware
ATIePerf_AppSim500k-FPS_20min                                                   ATIePerf_AppSim500k-FPS_20min                                                    1200              10
                                                                         ...    sessions    500000        5               0            0
    [Tags]  hardware
ATIP-Perf_AppSimHTTP-13Gbps-500kCPS_20min                                       ATIP-Perf_AppSimHTTP-13Gbps-500kCPS_20min                                        1200              10
                                                                         ...    sessions    500000       10               0           50
    [Tags]  hardware


# Packet size Validation
#                                                                                 bpsTest                                                                       duration  intervalPerformanceCheck
#                                                                                 rateType  rate       percent       hwDropPercent  swDropPercent
6Gbps-RR_64bytePacket_20min                                                     6Gbps-RR_64bytePacket_20min                                                      1200              10
                                                                         ...    bits     6000000000      5                0           0
    [Tags]  hardware
12Gbps-RR_128bytePacket_20min                                                   12Gbps-RR_128bytePacket_20min                                                    1200              10
                                                                         ...    bits    12000000000      5                0           0
    [Tags]  hardware
20Gbps-RR_256bytePacket_20min                                                   20Gbps-RR_256bytePacket_20min                                                    1200              10
                                                                         ...    bits    20000000000     10                0           0
    [Tags]  hardware
20Gbps-RR_500bytePacket_20min                                                   20Gbps-RR_500bytePacket_20min                                                    1200              10
                                                                         ...    bits    20000000000      5                0           0
    [Tags]  hardware
30Gbps-RR_256bytePacket_20min                                                   30Gbps-RR_256bytePacket_20min                                                    1200              10
                                                                         ...    bits    30000000000      8                0          88
    [Tags]  hardware
30Gbps-RR_500bytePacket_Random_20min                                            30Gbps-RR_500bytePacket_Random_20min                                             1200              10
                                                                         ...    bits    30000000000      8                0          40
    [Tags]  hardware


# AppSimMix Validation
#                                                                                 bpsTest                                                                       duration  intervalPerformanceCheck
#                                                                                 rateType  rate       percent       hwDropPercent  swDropPercent
ATIP-Perf_AppSimMix_26Gbps_30min_Perfect_Storm_low                              ATIP-Perf_AppSimMix_26Gbps_30min_Perfect_Storm_low                               1900               10
                                                                         ...    bits  26000000000       10               0           0
    [Tags]  hardware
ICMP_Session_Sender_30Gbps_20min                                                ICMP_Session_Sender_30Gbps_20min                                                 1200               10
                                                                         ...    bits  30000000000        5               0           0
    [Tags]  hardware
mihail_GTP_10Gbps                                                               mihail_GTP_10Gbps                                                                1200               10
                                                                         ...    bits  10000000000        5               0           0
    [Tags]  hardware

*** Keywords ***
Configure Device and Run BPS Test
    [Arguments]  ${bpsFile}  ${duration}  ${validateDropRateFrequency}  ${rateType}  ${rate}  ${percent}  ${hwDropPercent}  ${swDropPercent}

    # Reset Stats
    resetStats  ${atipSession}

	sshRemoveCoreFilesInNP  ${envVars}

	# Send traffic and verify performance
	atipLogin.Login  ${atipSession}
	checkPerformance  ${envVars}  ${atipSession}  ${bpsSession}  ${bpsFile}  ${duration}  ${rateType}  ${validateDropRateFrequency}  ${rate}  ${percent}  ${hwDropPercent}  ${swDropPercent}

	# Validate no crashes in core files
	validateNoCrashesInNPCoreFile  ${atipSession}  ${envVars}  ${TEST NAME}

Configure ATIP BPS
    atipLogin.Login  ${atipSession}

    # Configure BPS
    bpslogin.Login  ${bpsSession}
    portsOperations.reserveBpsPorts  ${bpsSession}  ${envVars.bpsSlot}  ${envVars.bpsPortList}  ${envVars.bpsPortGroup}

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    
