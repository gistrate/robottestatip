*** Settings ***
Documentation  SSL Performance test suite.

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    atipAutoFrwk.webApi.traffic.BpsOperations
Library    atipAutoFrwk.webApi.traffic.Login  WITH NAME  bpsLogin
Library    atipAutoFrwk.webApi.traffic.PortsOperations  WITH NAME  portsOperations
Library    atipAutoFrwk.services.atip.NPService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.services.atip.stats.PerformanceService
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.services.atip.stats.ValidateSSLStatsService.ValidateSSLStatsService
Library    atipAutoFrwk.webApi.atip.Filters

Default Tags  ATIP  functional  hardware  virtual

*** Test Cases ***
TC1  SSL-Perf-5kHPS_RSA_3DES_EDE_CBC_SHA_1024_TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC2  SSL-Perf-900HPS_RSA_3DES_EDE_CBC_SHA_2048_TLS12  BreakingPoint_serverA_2048  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC3  SSL-Perf-15Gbps_1MConc_RSA3DESEDECBCSHA1024TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Decryption
TC4  SSL-Perf-15Gbps_1MConc_RSA3DESEDECBCSHA2048TLS12  BreakingPoint_serverA_2048  1200  10  5
    [Template]  SSL Performance Scenario - Decryption
TC5  SSL-Perf-5kHPS_RSA_AES_256_CBC_SHA_1024_TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC6  SSL-Perf-900HPS_RSA_AES_256_CBC_SHA_2048_TLS12  BreakingPoint_serverA_2048  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC7  SSL-Perf-15Gbps_1MConc_RSAAES256CBCSHA1024TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Decryption
TC8  SSL-Perf-15Gbps_1MConc_RSAAES256CBCSHA2048TLS12  BreakingPoint_serverA_2048  1200  10  5
    [Template]  SSL Performance Scenario - Decryption
TC9  SSL-Perf-5kHPS_RSA_AES_128_CBC_SHA_1024_TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC10  SSL-Perf-900HPS_RSA_AES_128_CBC_SHA_2048_TLS12  BreakingPoint_serverA_2048  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC11  SSL-Perf-15Gbps_1MConc_RSAAES128CBCSHA1024TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Decryption
TC12  SSL-Perf-15Gbps_1MConc_RSAAES128CBCSHA2048TLS12  BreakingPoint_serverA_2048  1200  10  5
    [Template]  SSL Performance Scenario - Decryption
TC13  SSL-Perf-5kHPS_RSA_AES_128_CBC_SHA256_1024_TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC14  SSL-Perf-900HPS_RSA_AES_128_CBC_SHA256_2048_TLS12  BreakingPoint_serverA_2048  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC15  SSL-Perf-15Gbps_1MConc_RSAAES128CBCSHA2561024TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Decryption
TC16  SSL-Perf-15Gbps_1MConc_RSAAES128CBCSHA2562048TLS12  BreakingPoint_serverA_2048  1200  10  5
    [Template]  SSL Performance Scenario - Decryption
TC17  SSL-Perf-5kHPS_RSA_AES_256_CBC_SHA256_1024_TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC18  SSL-Perf-900HPS_RSA_AES_256_CBC_SHA256_2048_TLS12  BreakingPoint_serverA_2048  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC19  SSL-Perf-15Gbps_1MConc_RSAAES256CBCSHA2561024TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Decryption
TC20  SSL-Perf-15Gbps_1MConc_RSAAES256CBCSHA2562048TLS12  BreakingPoint_serverA_2048  1200  10  5
    [Template]  SSL Performance Scenario - Decryption
TC21  SSL-Perf-5kHPS_RSA_AES_256_GCM_SHA384_1024_TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC22  SSL-Perf-900HPS_RSA_AES_256_GCM_SHA384_2048_TLS12  BreakingPoint_serverA_2048  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC23  SSL-Perf-5kHPS_RSA_RC4_128_SHA_1024_TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC24  SSL-Perf-900HPS_RSA_RC4_128_SHA_2048_TLS12  BreakingPoint_serverA_2048  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC25  SSL-Perf-15Gbps_1MConc_RSARC4128SHA1024TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Decryption
TC26  SSL-Perf-15Gbps_1MConc_RSARC4128SHA2048TLS12  BreakingPoint_serverA_2048  1200  10  5
    [Template]  SSL Performance Scenario - Decryption
TC27  SSL-Perf-5kHPS_RSA_RC4_128_MD5_1024_TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC28  SSL-Perf-900HPS_RSA_RC4_128_MD5_2048_TLS12  BreakingPoint_serverA_2048  300  10  5
    [Template]  SSL Performance Scenario - Handshake per Sec
TC29  SSL-Perf-15Gbps_1MConc_RSARC4128MD51024TLS12  BreakingPoint_serverA_1024  300  10  5
    [Template]  SSL Performance Scenario - Decryption
TC30  SSL-Perf-15Gbps_1MConc_RSARC4128MD52048TLS12  BreakingPoint_serverA_2048  1200  10  5
    [Template]  SSL Performance Scenario - Decryption


*** Keywords ***

SSL Performance Scenario - Handshake per Sec
    [Arguments]  ${bpsFile}  ${certAndKeyName}  ${duration}  ${validateDropRateFrequency}  ${swDropPercent}

    # Reset Stats
    atipLogin.login  ${atipSession}
    resetStats  ${atipSession}

    # Configure BPS
    bpslogin.Login  ${bpsSession}
    portsOperations.reserveBpsPorts  ${bpsSession}  ${envVars.bpsSlot}  ${envVars.bpsPortList}  ${envVars.bpsPortGroup}

    # Remove core files
	sshRemoveCoreFilesInNP  ${envVars}

	# Configure SSL
	${response}  updateSslSettings  ${atipSession}    True
    should be equal  ${response.passiveSSLDecryption}  ${True}
	${response}  deleteAllCerts  ${atipSession}

    ${fileCertPath}=  Set variable  ${bpsKeysPath}/${certAndKeyName}.crt
    ${fileKeyPath}=  Set variable   ${bpsKeysPath}/${certAndKeyName}.key
    ${response}  uploadSeparateFiles  ${atipSession}  ${certAndKeyName}  ${fileCertPath}  ${fileKeyPath}

    deleteAllFilters  ${atipSession}
    createFilter  ${atipSession}  ${sslFilter}
    rulePush  ${atipSession}
    ${hwDropPcnt} =  Convert to Number  0

	# Send traffic and verify performance
	${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  ${duration}
	${initRx}=  getRxPacketsFromDebugPage  ${atipSession}  ${envVars}
    addBpsTestAction   ${verifyDrop}
    Call method    ${verifyDrop}  setSwDropPercent  ${swDropPercent}

    resetStats  ${atipSession}
	performActionUntilBpsTestCompletion  ${envVars}  ${bpsSession}  ${bpsTestConfig}  ${validateDropRateFrequency}
	${totalSessions}=  getBpsTotalSessions  ${bpsSession}  ${bpsTestConfig}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${totalSessions}
    checkSslKeyFails  ${atipSession}

	# Validate no crashes in core files
	validateNoCrashesInNPCoreFile  ${atipSession}  ${envVars}  ${bpsFile}
    checkHWDrop  ${envVars}  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  ${initRx}  ${hwDropPcnt}
    clearBpsTestAction


SSL Performance Scenario - Decryption
    [Arguments]  ${bpsFile}  ${certAndKeyName}  ${duration}  ${validateDropRateFrequency}  ${swDropPercent}

    # Reset Stats
    atipLogin.login  ${atipSession}
    ${stats}=  getSSLStatsFromDebugPage  ${atipSession}
    Log  ${stats}
    resetStats  ${atipSession}

    # Configure BPS
    bpslogin.Login  ${bpsSession}
    portsOperations.reserveBpsPorts  ${bpsSession}  ${envVars.bpsSlot}  ${envVars.bpsPortList}  ${envVars.bpsPortGroup}

    # Remove core files
	sshRemoveCoreFilesInNP  ${envVars}

	# Configure SSL
	${response}  updateSslSettings  ${atipSession}    True
    should be equal  ${response.passiveSSLDecryption}  ${True}
	${response}  deleteAllCerts  ${atipSession}

    ${fileCertPath}=  Set variable  ${bpsKeysPath}/${certAndKeyName}.crt
    ${fileKeyPath}=  Set variable   ${bpsKeysPath}/${certAndKeyName}.key
    ${response}  uploadSeparateFiles  ${atipSession}  ${certAndKeyName}  ${fileCertPath}  ${fileKeyPath}

    deleteAllFilters  ${atipSession}
    createFilter  ${atipSession}  ${sslFilter}
    rulePush  ${atipSession}

    ${hwFloatDropPcnt} =  Convert to Number  0
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  ${duration}
	${initRx}=  getRxPacketsFromDebugPage  ${atipSession}  ${envVars}
    addBpsTestAction   ${verifyDrop}
    Call method    ${verifyDrop}  setSwDropPercent  ${swDropPercent}
    ${stats}=  getSSLStatsFromDebugPage  ${atipSession}
    Log  ${stats}
    resetStats  ${atipSession}
    ${stats}=  getSSLStatsFromDebugPage  ${atipSession}
    Log  ${stats}
	performActionUntilBpsTestCompletion  ${envVars}  ${bpsSession}  ${bpsTestConfig}  ${validateDropRateFrequency}
	${totalBytes}=  getBpsTotalBytes  ${bpsSession}  ${bpsTestConfig}
	sleep  60
    validateSslDecryptedBytesByPercentage  ${atipSession}  ${totalBytes}
    checkSslKeyFails  ${atipSession}
    
	# Validate no crashes in core files
	validateNoCrashesInNPCoreFile  ${atipSession}  ${envVars}  ${bpsFile}
	checkHWDrop  ${envVars}  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  ${initRx}  ${hwFloatDropPcnt}
    clearBpsTestAction

