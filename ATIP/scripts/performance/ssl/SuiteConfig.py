from http import HTTPStatus

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.services.atip.bpsTestActions.VerifyDropAction import VerifyDropAction
from atipAutoFrwk.webApi.WebApiSession import WebApiSession


class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    appStatsType = StatsType.APPS
    dynAppStatsType = StatsType.DYNAMIC
    bpsTestConfig1 = BpsTestConfig('ATIP-Perf_30Gbps-10minutes', 600)
    bpsKeysPath = "../../../frwk/src/atipAutoFrwk/data/bpskeys"
    verifyDrop = VerifyDropAction(atipSession, envVars)
    sslFilter = FilterConfig("DecryptedFilter")
    sslFilter.decrypted = True

