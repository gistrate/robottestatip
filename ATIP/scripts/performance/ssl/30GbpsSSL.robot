*** Settings ***
Documentation  SSL Performance test suite.

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    atipAutoFrwk.webApi.traffic.Login  WITH NAME  bpsLogin
Library    atipAutoFrwk.webApi.traffic.PortsOperations  WITH NAME  portsOperations
Library    atipAutoFrwk.services.atip.NPService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.services.atip.stats.PerformanceService


Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure setup
Suite Teardown     atipLogout.Logout  ${atipSession}


*** Test Cases ***


TC000692566

    ${response}  updateSslSettings  ${atipSession}    True
    should be equal  ${response.passiveSSLDecryption}  ${True}
    # Reset Stats
    resetStats  ${atipSession}
    sshRemoveCoreFilesInNP  ${envVars}

	# Send traffic and verify performance
	runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
	# Validate no crashes in core files
	validateNoCrashesInNPCoreFile  ${atipSession}  ${envVars}  ${bpsTestConfig1}

*** Keywords ***
Configure setup
    atipLogin.Login  ${atipSession}

    # Configure BPS
    bpslogin.Login  ${bpsSession}
    portsOperations.reserveBpsPorts  ${bpsSession}  ${envVars.bpsSlot}  ${envVars.bpsPortList}  ${envVars.bpsPortGroup}

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}

