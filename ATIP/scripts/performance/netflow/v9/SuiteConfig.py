from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.Collector import Collector
from atipAutoFrwk.config.atip.NetflowAccelerationConfig import NetflowAccelerationConfig
from atipAutoFrwk.config.atip.NetflowCardConfig import NetflowCardConfig
from atipAutoFrwk.config.atip.NetflowCollectorConfig import NetflowCollectorConfig
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.NetflowStats import NetflowStats
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.webApi.WebApiSession import WebApiSession


import ipaddress


class SuiteConfig(object):
    # environment
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    maximizedDashboard = DashboardType.MAXIMIZED
    netflowRecords = NetflowStats.DATA_RECORDS
    atipSessions = TopStats.TOTAL_COUNT
    bpsSessions = BpsStatsType.SESSIONS
    statsTypeApp = StatsType.APPS
    timeInterval = TimeInterval.HOUR
    firstCollectorIP = envVars.netflowCollectorConnectionConfig.host
    secondCollectorIP = str(ipaddress.IPv4Address(firstCollectorIP) + 1)
    # ATIP config
    atipConfig = AtipConfig()
    if envVars.atipType == AtipType.VIRTUAL:
        netflowAcceleration=NetflowAccelerationConfig()
        netflowAcceleration.enableTurboMode=True
        atipConfig.setNetflowAcceleration(netflowAcceleration)
    netflowGlobalConfig = NetflowConfig()
    netflowGlobalConfig.enabled = True
    netflowGlobalConfig.version = 9
    netflowGlobalConfig.biFlows = True
    netflowGlobalConfig.timeout = 0
    atipConfig.setNetflowGlobal(netflowGlobalConfig)
    netflowCards = NetflowCardConfig()
    netflowCards.cardList[envVars.atipSlotNumber[0]].enabled = True
    netflowCards.cardList[envVars.atipSlotNumber[0]].odid = 100
    netflowCards.cardList[envVars.atipSlotNumber[0]].ipMethod = 'STATIC'
    netflowCards.cardList[envVars.atipSlotNumber[0]].dns = '8.8.8.8'
    netflowCards.cardList[envVars.atipSlotNumber[0]].netmask = '255.255.255.0'
    netflowCards.cardList[envVars.atipSlotNumber[0]].gw = '192.168.30.100'
    netflowCards.cardList[envVars.atipSlotNumber[0]].ip_addr = '192.168.30.1'
    atipConfig.setNetflowCards(netflowCards)
    firstCollector = Collector(0, 100, 'Samples', True, 'UDP', 4739, firstCollectorIP)
    secondCollector = Collector(1, 100, 'Samples', True, 'UDP', 4739, secondCollectorIP)
    netflowCollectors = NetflowCollectorConfig()
    netflowCollectors.collectorList[0]=firstCollector
    atipConfig.setNetflowCollectors(netflowCollectors)







