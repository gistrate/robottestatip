*** Settings ***
Documentation  Netflow performance tests - Combine Bidirectional OFF -v9.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.services.atip.utility.IxFlowParsing
Library    atipAutoFrwk.services.atip.SystemService
Library    Collections
Library    Telnet   30 seconds
Library    BuiltIn
Library    atipAutoFrwk.webApi.atip.Netflow
Library    String
Library    atipAutoFrwk.services.TestCondition
Library    atipAutoFrwk.webApi.atip.EngineInfo



Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Suite Setup
Test Template   Validate Netflow Performance

Suite Teardown   Suite Teardown

*** Test Cases ***

#            # testId        bpsTest                          duration  dropPcnt
TC3102      TC3102        NetFlow_TCP_100_CPS                   240       0
    [Tags]  hardware
TC3106      TC3106        NetFlow_TCP_50K_CPS                   240       0
    [Tags]  hardware
TC3109      TC3109        NetFlow_TCP_100K_CPS                  240       0
    [Tags]  hardware
TC3110      TC3110        NetFlow_TCP_100K_CPS_20min            1200      0
    [Tags]  hardware
TC3113      TC3113        NetFlow_UDP_100_flows                 240       0
    [Tags]  hardware
TC3117      TC3117        NetFlow_UDP_50K_flows                 240       0.01
TC3120      TC3120        NetFlow_UDP_100K_flows                240       0.01
    [Tags]  hardware  BUG1485488
TC3121      TC3121        NetFlow_UDP_100K_flows_20min          1200      0.01
    [Tags]  hardware  BUG1485488
vTC3118    vTC3118    NetFlow_UDP_50K_flows_20mins              1200      0.01
    [Tags]  virtual
vTC3122     vTC3122     NetFlow_TCP_50KCPS_v2_5min              300       0.01
    [Tags]  virtual
vTC1139     vTC1139     NetFlow_TCP_50KCPS_v2_20min             1200      0.01
    [Tags]  virtual






*** Keywords ***
Suite Setup
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    ${netflowGlobalConfig.biFlows}=  set variable  ${False}
    ConfigureAtip  ${atipSession}  ${atipConfig}  ${envVars}
    ${initialLog}=  CATENATE  Results for Combine Bidirectional OFF -v9 ${\n}
    set suite variable  ${finalLog}  ${initialLog}

Suite Teardown
    Log  ${finalLog}

Validate Netflow Performance
    [Arguments]  ${testId}  ${bpsFile}  ${duration}  ${dropPcnt}
    resetStats  ${atipSession}
    Log  ${bpsFile}
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  ${duration}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig}
    ${bpsSessionsComp}  sumStatItem  ${bpsComponentsStats}  ${bpsSessions}
    ${stat}  getTopStats  ${atipSession}  ${statsTypeApp}  ${timeInterval}
    ${atipSessions}  getAtipStat  ${stat}  ${atipSessions}
#    To be uncommented when below bug is fixed
#    #BUG1485366: 	App distribution window does not show the correct number of sessions when running UDP traffic
#    should be equal as integers  ${bpsSessionsComp}  ${atipSessions}
    ${records}=  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    ${dropPcnt}=  convert to number  ${dropPcnt}
    ${expectedValue}=  evaluate  ${bpsSessionsComp}*2
    ${actualPercent}=  evaluate  (1-${records}/${expectedValue})*100
    ${finalLog}=  CATENATE  ${finalLog} ${testId} ${bpsFile} BPS_Sessions: ${bpsSessionsComp} ATIP_Dashboard_Sessions: ${atipSessions} Collector_Stats: ${records} Actual Drop: ${actualPercent}% ${\n}
    set suite variable  ${finalLog}  ${finalLog}
    compareWithTolerance  ${expectedValue}  ${records}  ${dropPcnt}


