*** Settings ***

#Libraries
Library  ./NTO/frwk/Utils.py
Library  ./NTO/frwk/NTOPortGroup.py
Library  Collections

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  10

*** Test Cases ***
#ATIP_App_Forwarding
#    AddNTOLocal  nto1  10.38.185.27

Create NTO Config
#    [Arguments]  ${env}  ${ntoConfig}


    import library  atipAutoFrwk.config.Environment  WITH NAME  environment
	${env}=  get library instance  environment

	import library  atipAutoFrwk.config.nto.NTOConfig  WITH NAME  ntoConfigFile
	${ntoConfig}=  get library instance  ntoConfigFile

    AddNTOLocal  nto1  ${env.ntoConnectionConfig.host}
    #AddNTOLocal  nto1  10.38.185.27

    Log To Console  Destroying NTO Configuration !
    nto1.clearConfig
    Sleep  2m
    Log To Console  NTO Configuration Started !

    #Add atip resources
    :FOR    ${atipId}    IN    @{env.atipSlotNumber}
        \  AddATIP  ${nto1}  atip${atipId}   L${atipId}-ATIP

#    AddATIP  ${nto1}  atip4  L4-ATIP

    # Add traffic ports
    ${npId} =  set variable  1
    :FOR    ${key}    IN    @{ntoConfig.trafficPorts}
        \  &{npDef}=  Create Dictionary  name=np${npId}  port=${key}  portMode=BIDIRECTIONAL  connType=standalone  mediaType=SFP_PLUS_10G
        \  ${npId}=  evaluate  ${npId}+1
        \  AddPort  ${nto1}  ${npDef}
        \  Log  Crt np id:
        \  Log  ${npId}


#    # Add NetFlow Port Group
    :FOR    ${key}    IN    @{ntoConfig.netflowPG}
        \  &{npDef}=  Create Dictionary  name=np${npId}  port=${key}  portMode=NETWORK        connType=standalone  mediaType=SFP_PLUS_10G
        \  AddPort  ${nto1}  ${npDef}
        \  ${pg1}=  Catenate  {'type': 'NETFLOW', 'mode': 'BIDIRECTIONAL', 'failover_mode': 'REBALANCE'}
        \  ${npg}=  set variable  ${np${npId}}
        \  AddPortGrp  ${nto1}  NetFlow  ${pg1}  ${npg}
        \  ${npId}=  evaluate  ${npId}+1
        \  Log  Crt np id:
        \  Log  ${npId}

    # Add forward tool ports

    ${tpId} =  set variable  1
    :FOR    ${key}    IN    @{ntoConfig.appForwardingPorts}
        \  &{tpDef}=  Create Dictionary  name=tp${tpId}  port=${key}  portMode=TOOL           connType=standalone  mediaType=SFP_PLUS_10G
        \  ${tpId}=  evaluate  ${tpId}+1
        \  AddPort  ${nto1}  ${tpDef}
        \  Log  Crt tp id:
        \  Log  ${tpId}

    #Add Filters
    ${portLenght} =  get length  ${ntoConfig.trafficPorts}
    ${atipLenght} =  get length  ${env.atipSlotNumber}
    ${totalFilters} =  evaluate  ${portLenght}+${atipLenght}
    #${filterId} =  set variable  1
    :FOR    ${filterId}    IN RANGE  ${totalFilters}
        \  ${filterId}=  evaluate  ${filterId}+1
        \  AddFilter  ${nto1}  f${filterId}  PASS_ALL
        \  Log  Crt Filter id:
        \  Log  ${filterId}

#    AddFilter  ${nto1}  f1  PASS_ALL
#    AddFilter  ${nto1}  f2  PASS_ALL
#    AddFilter  ${nto1}  f3  PASS_ALL
#    AddFilter  ${nto1}  f4  PASS_ALL
#    AddFilter  ${nto1}  f5  PASS_ALL
#    AddFilter  ${nto1}  f6  PASS_ALL
#    AddFilter  ${nto1}  f7  PASS_ALL
#    AddFilter  ${nto1}  f8  PASS_ALL
#    AddFilter  ${nto1}  f9  PASS_ALL

#Add bi-di Connetions between traffic ports and filters
    ${zero} =  set variable  0
    ${connId} =  set variable  0
    ${connId2} =  set variable  0
    :FOR    ${connId}    IN RANGE  ${portLenght}
        \  ${connId}=  evaluate  ${connId}+1
        \  ${par}=  evaluate  ${connId}%2
        \  ${eval}=    set variable if    ${par} == ${zero}  ${True}  ${False}
        \  Log  ${eval}
        \  run keyword if  ${eval} == ${False}  CustomForLoop  ${connId}  ${portLenght}
        ...  ELSE  Log  Odd index!


#    AddConn    ${nto1}  c1  ${np1}  ${f1}  ${np2}
#    AddConn    ${nto1}  c2  ${np2}  ${f2}  ${np1}
#    AddConn    ${nto1}  c3  ${np3}  ${f3}  ${np4}
#    AddConn    ${nto1}  c4  ${np4}  ${f4}  ${np3}
#    AddConn    ${nto1}  c5  ${np5}  ${f5}  ${np6}
#    AddConn    ${nto1}  c6  ${np6}  ${f6}  ${np5}
#    AddConn    ${nto1}  c7  ${np7}  ${f7}  ${np8}
#    AddConn    ${nto1}  c8  ${np8}  ${f8}  ${np7}

#Add Connetions from traffic ports and atip dynamic filter
    :FOR    ${resFilterId}    IN RANGE  ${atipLenght}
        \  ${resFilterId}=  evaluate  ${portLenght}+${resFilterId}+1
        \  Log  ${resFilterId}
        \  ResourcesForLoop  ${resFilterId}  ${portLenght}
#    :FOR    ${conntId}    IN RANGE  ${portLenght}
#        \  ${conntId}=  evaluate  ${conntId}+1
#        \  ${trfcId}=  evaluate  ${portLenght}+${conntId}
#        \  ${trfpf2}=  evaluate  ${portLenght}+1
#        \  ${trfpf3}=  set variable  ${f${trfpf2}}
#        \  ${trfpf4}=  set variable  ${np${conntId}}
#        \  AddConn    ${nto1}  c${trfcId}  ${trfpf4}  ${trfpf3}  None

#    AddConn    ${nto1}  c9  ${np1}  ${f9}  None
#    AddConn    ${nto1}  c10  ${np2}  ${f9}  None
#    AddConn    ${nto1}  c11  ${np3}  ${f9}  None
#    AddConn    ${nto1}  c12  ${np4}  ${f9}  None
#    AddConn    ${nto1}  c13  ${np5}  ${f9}  None
#    AddConn    ${nto1}  c14  ${np6}  ${f9}  None
#    AddConn    ${nto1}  c15  ${np7}  ${f9}  None
#    AddConn    ${nto1}  c16  ${np8}  ${f9}  None

#Add Connetions forward ports and atip dynamic filters

    ${fwdPortLenght} =  get length  ${ntoConfig.appForwardingPorts}
    :FOR    ${fwpId}    IN RANGE  ${fwdPortLenght}
        \  ${fwpId}=  evaluate  ${fwpId}+1
        \  ${fwf1}=  set variable  f${npId}
        \  ${connId3}=  evaluate  ${portLenght}*${portLenght}+${fwpId}
        \  ${fwf2}=  evaluate  ${portLenght}+1
        \  Log  ${fwf2}
        \  ${fwf3}=  set variable  ${f${fwf2}}
        \  ${fwf5}=  set variable  ${tp${fwpId}}
        \  Log  ${fwf3}
        \  Log  ${fwf5}
        \  AddConn    ${nto1}  c${connId3}  None  ${fwf3}  ${fwf5}

#
#    AddConn    ${nto1}  c17  None  ${f9}  ${tp1}
#    AddConn    ${nto1}  c18  None  ${f9}  ${tp2}
#
    nto1.getATIPResources
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    NetFlow.createPortGrp
#
#    atip4.attachResource  ${f9}
    ${id}=  set variable  1
    Log  ${id}
    :FOR    ${resId}    IN    @{env.atipSlotNumber}
        \  ${ffId1}=  evaluate  ${portLenght}+${id}
        \  ${id}=  evaluate  ${id}+1
        \  ${ffId2}=  set variable  ${f${ffId1}}
        \  ${ffId3}=  set variable  atip${resId}
        \  Log  ${ffId2}
        \  Log  ${resId}
        \  Log  ${ffId3}
        \  run keyword  ${ffId3}.attachResource  ${ffId2}

# Creating lists with appFwd ports
#
#    :FOR    ${fwp6Id}    IN RANGE  ${fwdPortLenght}
#        \  ${fwp6Id}=  evaluate  ${fwp6Id}+1
#        \  ${dl1}=  set variable  dL${fwp6Id}
#        \  ${fwp7Id}=  set variable  ${tp${fwp6Id}.id}
#        \  @{${dl1}}=  Create List  ${fwp7Id}
    @{dL1}=  Create List  ${tp1.id}
    Log  ${tp1.id}
    @{dL2}=  Create List  ${tp2.id}


    &{appFwd1}=  Create Dictionary  application_name=default                        dest_port_list=${dL1}
    &{appFwd2}=  Create Dictionary  application_name=appForward   vlan_id=100,200,300       dest_port_list=${dL2}

    f9.configureAppForwardingMaps  ${appFwd1}  ${appFwd2}
#    f10.configureAppForwardingMaps  ${appFwd1}  ${appFwd2}
#    f11.configureAppForwardingMaps  ${appFwd1}  ${appFwd2}
#
##    #Dummy sleep here <replace w/ATIP test code>
#    Sleep  1m
#
#    #nto1.cleanupNVSDevices
#
#    Log To Console  NTO is Configured !

*** Keywords ***

CustomForLoop
    [Arguments]  ${connId}  ${portLenght}


    ${connId2}=  evaluate  ${connId}+1
    Log  ${connId}
    Log  ${connId2}
    ${ntp1}=  set variable  ${np${connId}}
    ${ntp2}=  set variable  ${np${connId2}}
    ${tf1}=  set variable  ${f${connId}}
    ${tf2}=  set variable  ${f${connId2}}
    AddConn    ${nto1}  c${connId}  ${ntp1}  ${tf1}  ${ntp2}
    AddConn    ${nto1}  c${connId2}  ${ntp2}  ${tf2}  ${ntp1}

ResourcesForLoop
    [Arguments]  ${resFilterId}  ${portLenght}

    :FOR    ${conntId}    IN RANGE  ${portLenght}
        \  ${conntId}=  evaluate  ${conntId}+1
        \  ${atippId}=  evaluate  ${resFilterId}-${portLenght}
        \  Log  ${atippId}
        \  ${trfcId}=  evaluate  ${portLenght}*${atippId}+${conntId}
        \  ${trfpf3}=  set variable  ${f${resFilterId}}
        \  ${trfpf4}=  set variable  ${np${conntId}}
        \  AddConn    ${nto1}  c${trfcId}  ${trfpf4}  ${trfpf3}  None