from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig

#
#Define ATIP session
#Define  BPS session
class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)

    # BPS test config
    bpsTestConfig = BpsTestConfig('ATIP_Upgrade_Static_Dynamic_SSL_CustomApp', 310)

    #ATIP Config : filters
    facebookFilter = FilterConfig("Facebook_mihail")
    FilterConditionConfig.addAppCondition(facebookFilter, ApplicationType.FACEBOOK)


    atipConfig = AtipConfig()
    atipConfig.addFilters(facebookFilter)