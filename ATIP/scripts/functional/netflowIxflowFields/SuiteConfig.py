import ipaddress
from http import HTTPStatus

import jsonpickle

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.Collector import Collector
from atipAutoFrwk.config.atip.NetflowCardConfig import NetflowCardConfig
from atipAutoFrwk.config.atip.NetflowCollectorConfig import NetflowCollectorConfig
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.NetflowStats import NetflowStats
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.services.atip.NetflowService import NetflowService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Netflow import Netflow
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
from atipAutoFrwk.data.atip.IxFlowType import IxFlowType


class SuiteConfig(StatsDefaultConfig):

    maximizedDashboard = DashboardType.MAXIMIZED
    netflowPkts = NetflowStats.TOTAL_PKTS
    firstCollectorIP = StatsDefaultConfig.envVars.netflowCollectorConnectionConfig.host
    secondCollectorIP = str(ipaddress.IPv4Address(firstCollectorIP) + 1)
    bpsDuration = 400
    # ATIP config
    atipConfig = AtipConfig()
    netflowGlobalConfig = NetflowConfig()
    netflowGlobalConfig.enabled = True
    netflowGlobalConfig.version = 10
    atipConfig.setNetflowGlobal(netflowGlobalConfig)
    netflowCards = NetflowCardConfig()
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].enabled = True
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].odid = 100
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].ipMethod = 'STATIC'
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].dns = '8.8.8.8'
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].netmask = '255.255.255.0'
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].gw = '192.168.30.100'
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].ip_addr = '192.168.30.1'
    atipConfig.setNetflowCards(netflowCards)
    firstCollector = Collector(0, 100, 'Samples', True, 'UDP', 4739, firstCollectorIP)
    netflowCollectors = NetflowCollectorConfig()
    netflowCollectors.collectorList[0]=firstCollector
    atipConfig.setNetflowCollectors(netflowCollectors)
    bpsTestConfig = BpsTestConfig('automation_3_static_apps', 80)
    netflowRecords = NetflowStats.DATA_RECORDS







