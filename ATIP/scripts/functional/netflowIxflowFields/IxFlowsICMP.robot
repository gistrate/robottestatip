*** Settings ***
Documentation  IxFlow dns fields tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    Collections
Library    Telnet   30 seconds
Library    BuiltIn
Library    atipAutoFrwk.webApi.atip.Netflow
Library    String
Library    atipAutoFrwk.services.TestCondition
Library    atipAutoFrwk.webApi.atip.EngineInfo
Library    atipAutoFrwk.services.atip.utility.IxFlowParsing
Library    atipAutoFrwk.config.atip.NetflowConfig
Library    atipAutoFrwk.services.atip.NetflowService
Library    atipAutoFrwk.webApi.atip.Netflow
Library    atipAutoFrwk.services.atip.SystemService
Library    Collections
Resource    scripts/functional/netflow/NetflowUtils.robot

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Suite Setup
Suite Teardown     Clean ATIP

*** Test Cases ***

Netflow - EchoRequest_Unidirectional_IPv4

    ${bpsTestName}=     set variable    IPv4_Request_ICMP
    ${testId} =  set variable  TC001056989
    ${expectedValue1} =  set variable  0x00000842
    ${ixFlowFilters} =  set variable  "cflow.icmp_type_code_ipv4"
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${expectedValue1}
    should be equal as integers  ${records}  ${value1}
    Execute Command  rm ${testId}*

Netflow - EchoRequestReply_Bidirectional_IPv4

    Enable Combine Bidirectional  ${netflowGlobalConfig}
    ${bpsTestName}=     set variable    IPv4_Request_Reply
    ${testId} =  set variable  TC001014888
    ${expectedValue1} =  set variable  0x00000842
    ${expectedValue2} =  set variable  0x00000043
    ${ixFlowFilters} =  set variable  "cflow.icmp_type_code_ipv4"
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${expectedValue1}
    ${value2} = 	Get From Dictionary 	${dict}  ${expectedValue2}
    should be equal as integers  1  ${value1}
    should be equal as integers  1  ${value2}
    Disable Combine Bidirectional  ${netflowGlobalConfig}
    Execute Command  rm ${testId}*

Netflow - PacketTooBig_Unidirectional_IPv6

    ${bpsTestName}=     set variable    IPv6_PacketTooBig
    ${testId} =  set variable  TC001015587
    ${expectedValue1} =  set variable  2
    ${expectedValue2} =  set variable  0
    ${ixFlowFilters} =  set variable  "cflow.icmp_ipv6_type,cflow.icmp_ipv6_code"
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${expectedValue1}
    should be equal as integers  1  ${value1}
    ${value2} = 	Get From Dictionary 	${dict}  ${expectedValue2}
    should be equal as integers  1  ${value2}
    Execute Command  rm ${testId}*


Netflow - RouterAdvertisement_Bidirectional_IPv6

    Enable Combine Bidirectional  ${netflowGlobalConfig}
    ${bpsTestName}=     set variable    IPv6_RouterAdvert
    ${testId} =  set variable  TC001016473
    ${expectedValue1} =  set variable  134
    ${expectedValue2} =  set variable  0
    ${expectedValue3} =  set variable  130
    ${ixFlowFilters} =  set variable  "cflow.icmp_ipv6_type,cflow.icmp_ipv6_code"
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${expectedValue1}
    ${value2} = 	Get From Dictionary 	${dict}  ${expectedValue2}
    ${value3} = 	Get From Dictionary 	${dict}  ${expectedValue3}
    should be equal as integers  1  ${value1}
    should be equal as integers  2  ${value2}
    should be equal as integers  1  ${value3}
    Disable Combine Bidirectional  ${netflowGlobalConfig}
    Execute Command  rm ${testId}*



*** Keywords ***
Suite Setup
    Configure Netfow on ATIP
    Update Active Flow Timeout  ${netflowGlobalConfig}
    Connect on Linux Box
    ${command} =  copyFileToLinux
    Write  ${command}

Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    logout  ${atipSession}


