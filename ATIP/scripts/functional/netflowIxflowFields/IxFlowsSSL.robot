*** Settings ***

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.Login    WITH NAME  atipLogin
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.config.atip.NetflowCollectorConfig
Library    atipAutoFrwk.config.traffic.BpsTestConfig  a  1
Library    atipAutoFrwk.services.atip.NetflowService    WITH NAME    serNetflowConfig
Library    atipAutoFrwk.webApi.atip.Netflow             WITH NAME    Netflow
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    Telnet   30 seconds
Library    Collections
Library    String
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.services.TestCondition
Library    atipAutoFrwk.webApi.atip.EngineInfo
Library    atipAutoFrwk.services.atip.utility.IxFlowParsing
Library    atipAutoFrwk.config.atip.NetflowConfig
Resource    scripts/functional/netflow/NetflowUtils.robot

Suite Setup        Setup Netflow SSL Encrypt ATIP
Suite Teardown     Teardown Netflow SSL Encrypt ATIP


Default Tags  ATIP  functional  hardware  virtual

*** Test Cases ***

Cipher1  TC1  ATIP_SSL-HTTP1_0-keySize512  Encrypted  TLS_RSA_WITH_RC4_128_MD5  128
    [Template]  Netflow SSL Enc Fields Encrypted passiveDecrypt Disabled Cipher
Cipher2  TC2  ATIP_SSL-HTTP1_0-keySize2048-AES  Encrypted  TLS_RSA_WITH_AES_256_CBC_SHA  256
    [Template]  Netflow SSL Enc Fields Encrypted passiveDecrypt Disabled Cipher

Netflow SSL Enc Fields Encrypted passiveDecrypt Enabled no Cert
    [Tags]  hardware
    # Test variables
    ${testId}=    set variable    TC3
    ${bpsTestName}=     set variable    ATIP_SSL-HTTP1_0-keySize512
    ${encryptType}=     set variable    Encrypted
    ${cipherName}=      set variable    TLS_RSA_WITH_RC4_128_MD5
    ${keyLength}=       set variable    128
    ${passiveDecrypt}=  set variable    False
    ${ixFlowFilters}=  set variable    "cflow.pie.ixia.conn-encryption-type","cflow.pie.ixia.encryption-cipher","cflow.pie.ixia.encryption-keylen"
    #  Enable Netflow Global, Fields, Card, Collector
    Enable Netflow Global Config
    Enable All SSL Fields
    Enable Service Netflow Card Config
    Config First Collector  ${envVars.netflowCollectorConnectionConfig.host}
    Enabled SSL Passive Decryption Config
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${envVars.netflowCollectorConnectionConfig.host}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${cipherName}
    should not be equal as integers  0  ${value1}
    ${value2} = 	Get From Dictionary 	${dict}  ${keyLength}
    should not be equal as integers  0  ${value2}
    ${value3} = 	Get From Dictionary 	${dict}  ${encryptType}
    should not be equal as integers  0  ${value3}
    Execute Command  rm ${testId}*




Netflow SSL Enc Fields Decrypted passiveDecrypt Enabled and Cert
    [Tags]  hardware
    # Test variables
    ${testId}=    set variable    TC4
    ${bpsTestName}=     set variable    ATIP_SSL-HTTP1_0-keySize512
    ${encryptType}=     set variable    Decrypted
    ${cipherName}=      set variable    TLS_RSA_WITH_RC4_128_MD5
    ${keyLength}=       set variable    128
    ${ixFlowFilters}=  set variable    "cflow.pie.ixia.conn-encryption-type","cflow.pie.ixia.encryption-cipher","cflow.pie.ixia.encryption-keylen"
    #  Enable Netflow Global, Fields, Card, Collector
    Enable Netflow Global Config
    Enable All SSL Fields
    Enable Service Netflow Card Config
    Config First Collector  ${envVars.netflowCollectorConnectionConfig.host}
    Enabled SSL Passive Decryption Config
    Upload Separate Files Test
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${envVars.netflowCollectorConnectionConfig.host}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${cipherName}
    should not be equal as integers  0  ${value1}
    ${value2} = 	Get From Dictionary 	${dict}  ${keyLength}
    should not be equal as integers  0  ${value2}
    ${value3} = 	Get From Dictionary 	${dict}  ${encryptType}
    should not be equal as integers  0  ${value3}
    Execute Command  rm ${testId}*





*** Keywords ***

Netflow SSL Enc Fields Encrypted passiveDecrypt Disabled Cipher
    [Arguments]  ${testId}  ${bpsTestName}  ${encryptType}  ${cipherName}  ${keyLength}
    ${passiveDecrypt}=  set variable    False
    ${ixFlowFilters}=  set variable    "cflow.pie.ixia.conn-encryption-type","cflow.pie.ixia.encryption-cipher","cflow.pie.ixia.encryption-keylen"
    Enable Netflow Global Config
    Enable All SSL Fields
    Enable Service Netflow Card Config
    Config First Collector  ${envVars.netflowCollectorConnectionConfig.host}

    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${envVars.netflowCollectorConnectionConfig.host}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${cipherName}
    should not be equal as integers  0  ${value1}
    ${value2} = 	Get From Dictionary 	${dict}  ${keyLength}
    should not be equal as integers  0  ${value2}
    ${value3} = 	Get From Dictionary 	${dict}  ${encryptType}
    should not be equal as integers  0  ${value3}
    Execute Command  rm ${testId}*


Disable Netflow Global Config
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${atipSession}
    ${objNetflowGlobalConfig.enabled}=  Set Variable    False
    Netflow.updateGlobalConfig  ${atipSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${atipSession}
    should not be true  ${objNetflowGlobalConfig.enabled}

Enable Netflow Global Config
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${atipSession}
    ${objNetflowGlobalConfig.enabled}=  Set Variable    True
    Netflow.updateGlobalConfig  ${atipSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${atipSession}
    should be true  ${objNetflowGlobalConfig.enabled}

Disable All SSL Fields
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${atipSession}
    ${objNetflowGlobalConfig.netflowL7ConnEncryptType}=     Set Variable    False
    ${objNetflowGlobalConfig.netflowL7EncryptionCipher}=    set variable    False
    ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}=    set variable    False
    Netflow.updateGlobalConfig  ${atipSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${atipSession}
    should not be true  ${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    should not be true  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    should not be true  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

Enable All SSL Fields
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${atipSession}
    ${objNetflowGlobalConfig.netflowL7ConnEncryptType}=     Set Variable    True
    ${objNetflowGlobalConfig.netflowL7EncryptionCipher}=    set variable    True
    ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}=    set variable    True
    Netflow.updateGlobalConfig  ${atipSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${atipSession}
    should be true  ${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    should be true  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    should be true  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

Enable Service Netflow Card Config
    [Tags]  ${hardware}
    ${objNetflowCardConfig}=   serNetflowConfig.Get Card Config      ${atipSession}     ${httpStatusOK}
    ${cardToUpdate}=    Get From List   ${objNetflowCardConfig.cardList}    ${envVars.atipSlotNumber[0]}
    ${cardToUpdate.enabled}=    set variable    True
    ${cardToUpdate.gw}=    set variable    192.168.30.100
    ${cardToUpdate.dns}=    set variable    8.8.8.8
    ${cardToUpdate.odid}=    set variable    100
    ${cardToUpdate.ipMethod}=    set variable    STATIC
    ${cardToUpdate.ip_addr}=    set variable    192.168.30.1
    ${cardToUpdate.netmask}=    set variable    255.255.255.0
    updateCard    ${atipSession}    ${cardToUpdate}
    ${objNetflowCardConfig}=   serNetflowConfig.Get Card Config      ${atipSession}     ${httpStatusOK}

Config First Collector
    [Arguments]     ${ipCollector}
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${atipSession}    ${httpStatusOK}
    ${collectorToUpdate}=   Get From List   ${listNetflowCollectorConfig.collectorList}   ${0}
    ${collectorToUpdate.unit}=   set variable  Samples
    ${collectorToUpdate.port}=   set variable  ${4739}
    ${collectorToUpdate.rate}=   set variable  ${100}
    ${collectorToUpdate.proto}=   set variable  UDP
    ${collectorToUpdate.collector}=   set variable  ${ipCollector}
    ${collectorToUpdate.enabled}=   set variable  True
    updateCollectorConfig  ${atipSession}  ${collectorToUpdate}
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${atipSession}    ${httpStatusOK}

Disable the Collector
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${atipSession}    ${httpStatusOK}
    ${collectorToUpdate}=   Get From List   ${listNetflowCollectorConfig.collectorList}   ${0}
    ${collectorToUpdate.enabled}=   set variable  False
    updateCollectorConfig  ${atipSession}  ${collectorToUpdate}
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${atipSession}    ${httpStatusOK}

Reset SSL Passive Decryption Config
    ${response}  updateSslSettings  ${atipSession}    False
    should be equal  ${response.passiveSSLDecryption}  ${False}

Enabled SSL Passive Decryption Config
    ${response}  updateSslSettings  ${atipSession}    True
    should be equal  ${response.passiveSSLDecryption}  ${True}

Get SSL Passive Decryption Config
    ${response}  getSslSettings  ${atipSession}
    should be equal  ${response.passiveSSLDecryption}  ${True}


Upload Separate Files Test
    ${fileCert}=  Set variable  ../../../frwk/src/atipAutoFrwk/data/bpskeys/BreakingPoint_serverA_512.crt
    ${fileKey}=  Set variable   ../../../frwk/src/atipAutoFrwk/data/bpskeys/BreakingPoint_serverA_512.key
    ${name}=  Set variable  BreakingPoint_serverA_512
    ${response}  uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert}  ${fileKey}

Delete All SSL Certificates Separate File
    ${response}  deleteAllCerts  ${atipSession}

Disable SSL Passive Decryption Config
    ${response}  updateSslSettings  ${atipSession}    False
    should be equal  ${response.passiveSSLDecryption}  ${False}

Setup Netflow SSL Encrypt ATIP
    atipLogin.login  ${atipSession}

Teardown Netflow SSL Encrypt ATIP
    Run keyword if  '${envVars.atipType}'=='${hardware}'  deleteAllCerts  ${atipSession}
    Run keyword if  '${envVars.atipType}'=='${hardware}'  Reset SSL Passive Decryption Config
    Disable Netflow Global Config
    logout  ${atipSession}
