*** Settings ***
Documentation  IxFlow fields tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    Collections
Library    Telnet   30 seconds
Library    BuiltIn
Library    atipAutoFrwk.webApi.atip.Netflow
Library    String
Library    atipAutoFrwk.services.TestCondition
Library    atipAutoFrwk.webApi.atip.EngineInfo
Library    atipAutoFrwk.services.atip.utility.IxFlowParsing
Library    atipAutoFrwk.config.atip.NetflowConfig
Library    atipAutoFrwk.services.atip.NetflowService
Library    atipAutoFrwk.webApi.atip.Netflow
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService

Resource    scripts/functional/netflow/NetflowUtils.robot

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Suite Setup
Suite Teardown     Clean ATIP


*** Test Cases ***
IxFlow geo validation
    ${testId} =  Set variable  TC11111
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.source-ip-country-code,cflow.pie.ixia.source-ip-country-name,cflow.pie.ixia.destination-ip-country-code,cflow.pie.ixia.destination-ip-country-name"
    @{informElemToContain} =  create list  srcCountryCode  srcCountryName  dstCountryCode  dstCountryName
    ${fields} =  create list  netflowL7SrcCountryCode  netflowL7DstCountryCode  netflowL7DstCountryName  netflowL7SrcCountryName
    deleteAllFilters  ${atipSession}
    enableIxFlowFields  ${atipSession}  ${fields}
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    Validate Information Elements  ${testId}  ${firstCollectorIP}  ${informElemToContain}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${expectedValue} =  evaluate  ${records}/2
    Log  ${expectedValue}
    ${value1} = 	Get From Dictionary 	${dict}  GB
    should be equal as integers  ${expectedValue}  ${value1}
    ${value2} = 	Get From Dictionary 	${dict}  FR
    should be equal as integers  ${expectedValue}  ${value2}
    ${value3} = 	Get From Dictionary 	${dict}  United Kingdom
    should be equal as integers  ${expectedValue}  ${value3}
    ${value4} = 	Get From Dictionary 	${dict}  France
    should be equal as integers  ${expectedValue}  ${value4}
    Execute Command  rm ${testId}*


IxFlow application fields - application name validation
    ${testId} =  Set variable  TC22222
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.l7-application-name"
    ${fields} =  create list  netflowL7AppName
    @{informElemToContain} =  create list  l7appName
    enableIxFlowFields  ${atipSession}  ${fields}
    Connect on Linux Box
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    Validate Information Elements  ${testId}  ${firstCollectorIP}  ${informElemToContain}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  amazon
    ${value2} = 	Get From Dictionary 	${dict}  facebook
    ${value3} = 	Get From Dictionary 	${dict}  netflix
    ${total} =  evaluate  ${value1}+${value2}+${value3}
    should be equal as integers  ${records}  ${total}
    Execute Command  rm ${testId}*

IxFlow application fields - user agent validation
    # Test variables
    ${bpsTestName}=     set variable    UserAgent-iPad-pakistan
    ${testId} =  set variable  TC70707
    ${ixFlowFilters} =  set variable  "cflow.pie.ixia.http-user-agent"
    ${expectedValue} =  set variable  mozilla/5.0 (ipad; cpu os 5_1 like mac os x) applewebkit/534.46 (khtml, like gecko ) version/5.1 mobile/9b176 safari/7534.48.3
    @{informElemToContain} =  create list  userAgent
    ${fields} =  create list  netflowL7UserAgent
    enableIxFlowFields  ${atipSession}  ${fields}
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    Validate Information Elements  ${testId}  ${firstCollectorIP}  ${informElemToContain}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${expectedValue}
    should be equal as integers  ${records}  ${value1}
    Execute Command  rm ${testId}*



IxFlow application fields - uri validation
    # Test variables
    ${bpsTestName}=     set variable    HTTP-URI_pakistan
    ${testId} =  set variable  TC80808
    ${expectedValue} =  set variable  /WiGenre?agid=1365&pn=2&np=1&actionMethod=json
    @{informElemToContain} =  create list  uri
    ${fields} =  create list  netflowL7URI
    ${ixFlowFilters} =  set variable  "cflow.pie.ixia.http-uri"
    enableIxFlowFields  ${atipSession}  ${fields}
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    Validate Information Elements  ${testId}  ${firstCollectorIP}  ${informElemToContain}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${expectedValue}
    should be equal as integers  ${records}  ${value1}
    Execute Command  rm ${testId}*


IxFlow application fields - hostname validation
    # Test variables
    ${bpsTestName}=     set variable    HTTP-URI_pakistan
    ${testId} =  set variable  TC90909
    ${expectedValue} =  set variable  www.netflix.com
    @{informElemToContain} =  create list  httpHostName
    ${fields} =  create list  netflowL7Domain
    ${ixFlowFilters} =  set variable  "cflow.pie.ixia.hostname"
    enableIxFlowFields  ${atipSession}  ${fields}
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    Validate Information Elements  ${testId}  ${firstCollectorIP}  ${informElemToContain}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${expectedValue}
    should be equal as integers  ${records}  ${value1}
    Execute Command  rm ${testId}*




IxFlow application fields - dns text validation
    # Test variables
    ${bpsTestName}=     set variable    DNS_txt_rec_Pakistan
    ${testId} =  set variable  TC99999
    ${fields} =  create list  netflowL7DNSText
    ${ixFlowFilters} =  set variable  "cflow.pie.ixia.dns-txt"
    ${dnsTextValue} =  set variable  Some random garbage to be passed in the DNS TXT field
    @{informElemToContain} =  create list  dnsTxt
    enableIxFlowFields  ${atipSession}  ${fields}
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    Validate Information Elements  ${testId}  ${firstCollectorIP}  ${informElemToContain}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${dnsTextValue}
    should be equal as integers  ${records}  ${value1}
    Execute Command  rm ${testId}*


IxFlow device validation
    ${testId} =  Set variable  TC33333
    @{informElemToContain} =  create list  deviceId  browserName
    ${fields} =  create list  netflowL7DeviceId  netflowL7BrowserName
    ${ixFlowFilters} =  set variable  "cflow.pie.ixia.os-device-id,cflow.pie.ixia.browser-name"
    enableIxFlowFields  ${atipSession}  ${fields}
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    Validate Information Elements  ${testId}  ${firstCollectorIP}  ${informElemToContain}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  4
    should be equal as integers  ${records}  ${value1}
    ${value2} = 	Get From Dictionary 	${dict}  Chrome
    should be equal as integers  ${records}  ${value2}
    Execute Command  rm ${testId}*


IxFlow negative scenario - user agent

    # Test variables
    Configure Netfow on ATIP
    ${bpsTestName}=     set variable    UserAgent-iPad-pakistan
    ${fields} =  create list  netflowL7UserAgent
    ${ixFlowFilters} =  set variable  http-user-agent
    ${ixFlowValue} =  set variable  mozilla/5.0 (ipad; cpu os 5_1 like mac os x) applewebkit/534.46 (khtml, like gecko ) version/5.1 mobile/9b176 safari/7534.48.3
    ${testId} =  Set variable  TC000965693
    disableIxFlowFields  ${atipSession}  ${fields}
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${output}=  Analyze traffic  ${testId}  ${ixFlowFilters}  ${ixFlowValue}  ${firstCollectorIP}
    should be equal as integers    ${output}  0
    Execute Command  rm ${testId}*


IxFlow negative scenario - uri

    # Test variables
    Configure Netfow on ATIP
    ${bpsTestName}=     set variable    HTTP-URI_pakistan
    ${fields} =  create list  netflowL7URI
    ${ixFlowFilters} =  set variable  http-uri
    ${ixFlowValue} =  set variable  /WiGenre?agid=1365&pn=2&np=1&actionMethod=json
    ${testId} =  Set variable  TC000965695
    disableIxFlowFields  ${atipSession}  ${fields}
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${output}=  Analyze traffic  ${testId}  ${ixFlowFilters}  ${ixFlowValue}  ${firstCollectorIP}
    should be equal as integers    ${output}  0
    Execute Command  rm ${testId}*


IxFlow negative scenario - hostname

    # Test variables
    Configure Netfow on ATIP
    ${bpsTestName}=     set variable    HTTP-URI_pakistan
    ${fields} =  create list  netflowL7Domain
    ${ixFlowFilters} =  set variable  hostname
    ${ixFlowValue} =  set variable  www.netflix.com
    ${testId} =  Set variable  TC000965697
    disableIxFlowFields  ${atipSession}  ${fields}
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${output}=  Analyze traffic  ${testId}  ${ixFlowFilters}  ${ixFlowValue}  ${firstCollectorIP}
    should be equal as integers    ${output}  0
    Execute Command  rm ${testId}*


IxFlow negative scenario - dns

    # Test variables
    Configure Netfow on ATIP
    ${bpsTestName}=     set variable    DNS_txt_rec_Pakistan
    ${fields} =  create list  netflowL7DNSText
    ${ixFlowFilters} =  set variable  dns-txt
    ${ixFlowValue} =  set variable  Some random garbage to be passed in the DNS TXT field
    ${testId} =  Set variable  TC000965691
    disableIxFlowFields  ${atipSession}  ${fields}
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${output}=  Analyze traffic  ${testId}  ${ixFlowFilters}  ${ixFlowValue}  ${firstCollectorIP}
    should be equal as integers    ${output}  0
    Execute Command  rm ${testId}*

*** Keywords ***
Suite Setup
    Configure Netfow on ATIP
    Connect on Linux Box
    ${command} =  copyFileToLinux
    Write  ${command}

Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    logout  ${atipSession}



















