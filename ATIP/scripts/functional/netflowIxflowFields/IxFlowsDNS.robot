*** Settings ***
Documentation  IxFlow dns fields tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    Collections
Library    Telnet   30 seconds
Library    BuiltIn
Library    atipAutoFrwk.webApi.atip.Netflow
Library    String
Library    atipAutoFrwk.services.TestCondition
Library    atipAutoFrwk.webApi.atip.EngineInfo
Library    atipAutoFrwk.services.atip.utility.IxFlowParsing
Library    atipAutoFrwk.config.atip.NetflowConfig
Library    atipAutoFrwk.services.atip.NetflowService
Library    atipAutoFrwk.webApi.atip.Netflow
Library    atipAutoFrwk.services.atip.SystemService
Library    Collections
Resource    scripts/functional/netflow/NetflowUtils.robot

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Suite Setup
Suite Teardown     Clean ATIP

*** Test Cases ***
IxFlow application fields - DNS_HesiodClass
    Configure Netfow on ATIP
    Update Active Flow Timeout  ${netflowGlobalConfig}
    ${bpsTestName}=     set variable    dns_netflow_hs
    ${testId} =  set variable  TC001089632
    ${expectedValue1} =  set variable  www.google.com.
    ${expectedValue2} =  set variable  HS
    ${fields} =  create list  netflowL7DNSHostnameQuery  netflowL7DNSHostnameResponse  netflowL7DNSClass
    ${ixFlowFilters} =  set variable  "cflow.pie.ixia.dns-question-names,cflow.pie.ixia.dns-classes"
    enableIxFlowFields  ${atipSession}  ${fields}  ${False}
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${expectedValue1}
    should be equal as integers  ${records}  ${value1}
    ${value2} = 	Get From Dictionary 	${dict}  ${expectedValue2}
    should be equal as integers  ${records}  ${value2}
    Execute Command  rm ${testId}*


IxFlow application fields - DNS_MultipleQueryResponse
    Configure Netfow on ATIP
    Update Active Flow Timeout  ${netflowGlobalConfig}
    ${bpsTestName}=     set variable    netflow_dns_multiple
    ${testId} =  set variable  TC001056453
    ${expectedValue1} =  set variable  www.google.com.,www.facebook.com.,csi.gstatic.com.
    ${expectedValue2} =  set variable  IN,CS,CH
    ${fields} =  create list  netflowL7DNSHostnameQuery  netflowL7DNSHostnameResponse  netflowL7DNSClass
    ${ixFlowFilters} =  set variable  "cflow.pie.ixia.dns-question-names,cflow.pie.ixia.dns-classes"
    enableIxFlowFields  ${atipSession}  ${fields}  ${False}
    resetStats  ${atipSession}
    Connect on Linux Box
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestName}  ${bpsDuration}
    Run and Capture traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    Log  ${records}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  ${expectedValue1}
    should be equal as integers  ${records}  ${value1}
    ${value2} = 	Get From Dictionary 	${dict}  ${expectedValue2}
    should be equal as integers  ${records}  ${value2}
    Execute Command  rm ${testId}*



*** Keywords ***
Suite Setup
    Configure Netfow on ATIP
    Connect on Linux Box
    ${command} =  copyFileToLinux
    Write  ${command}

Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    logout  ${atipSession}


