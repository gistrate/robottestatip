*** Settings ***
Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System  WITH NAME  AtipSystem
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Users
Library    atipAutoFrwk.webApi.atip.Status
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.webApi.atip.Filters  WITH NAME  atipFilters
Library    atipAutoFrwk.config.atip.filter.FilterConditionConfig
Library	   atipAutoFrwk.webApi.atip.CustomIP
Library    atipAutoFrwk.webApi.atip.CustomAppConfig
Library    atipAutoFrwk.webApi.atip.Netflow             WITH NAME    Netflow
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.webApi.atip.MplsParsing  WITH NAME  Mpls
Library    atipAutoFrwk.webApi.atip.DataMasking
Library    atipAutoFrwk.services.atip.LogoutService  WITH NAME  LogoutService
Library    atipAutoFrwk.services.atip.LoginService  WITH NAME  LoginService

#Libraries
Library  ./NTO/frwk/Utils.py
Library  ./NTO/frwk/NTOPortGroup.py
Library  Collections
Library  String

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot
#Resource    scripts/functional/appSignatures/pcapSigValidation/NTOConfigPcapLib.robot

#Suite Setup        Create NTO Config RBAC
#Suite Teardown     Recover ATIP Env

#Test Setup    Log into the given session  ${adminSession}
#Test Teardown    Logout from the given session  ${adminSession}


*** Variables ***
${RESOURCETIMEOUT}=  10
${atipSlot}=   ${envVars.atipSlotNumber[0]}



*** Keywords ***

Log into the given session
    [Arguments]     ${session}=${atipSession}

    log  \n
    #login    ${session}
    LoginService.waitUntilLoggedIn  ${session}
    #LogoutService.waitUntilLoggedout  ${session}
    #LoginService.waitUntilLoggedIn  ${session}
    #log  Loggging in
##    ${result}=  getStatus  ${session}
##    ${str}=  Catenate  ${result['user']['userName']}
##    ...  ${result['user']['userType']}
##    ...  ${result['user']['overallStatus']}
##    log  ${str}

Logout from the given session
    [Arguments]     ${session}=${atipSession}

    logout  ${session}
    LogoutService.waitUntilLoggedout  ${session}

Create NTO Config RBAC
    import library  atipAutoFrwk.config.Environment  WITH NAME  environment
	${env}=  get library instance  environment

	import library  atipAutoFrwk.config.nto.NTOConfig  WITH NAME  ntoConfigFile
	${ntoConfig}=  get library instance  ntoConfigFile

    AddNTO  nto1  ${env.ntoConnectionConfig.host}

    nto1.setAUTHmode  LOCAL
    nto1.createUser  userA  ${FALSE}  ${TRUE}  ${FALSE}
    nto1.createUser  userB  ${FALSE}  ${TRUE}  ${FALSE}
    nto1.createUser  userC  ${FALSE}  ${TRUE}  ${FALSE}
    nto1.createUser  userD  ${FALSE}  ${TRUE}  ${FALSE}
    nto1.createGroup  gView  ${TRUE}  ${FALSE}
    nto1.createGroup  gModify  ${TRUE}  ${FALSE}
    nto1.addUserToGroup  userA  gView
    nto1.addUserToGroup  userB  gView
    nto1.addUserToGroup  userA  gModify
    nto1.addUserToGroup  userC  gModify

#Test NTO RBAC Modify Require Member
#Test Modify Require Group
    ${listGroup}=  create list  gModify
    ${policy}=  set variable  REQUIRE_MEMBER
    ${access_settings}=  set variable  modify_access_settings
    ApplyAtipAccessControl  ${nto1}  ${atipSlot}  ${listGroup}  ${policy}  ${access_settings}
#Test NTO RBAC View Require Member
#Test View Require Group
    ${listGroup}=  create list  gView
    ${policy}=  set variable  REQUIRE_MEMBER
    ${access_settings}=  set variable  view_access_settings
    ApplyAtipAccessControl  ${nto1}  ${atipSlot}  ${listGroup}  ${policy}  ${access_settings}

    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    logout  ${atipSession}
    LogoutService.waitUntilLoggedout  ${atipSession}


ApplyAtipAccessControl
    [Arguments]  ${nto}  ${atip}  ${listGroup}  ${policy}  ${access_settings}

    ${dictContentType}=  create dictionary  content-type=application/json

    ${dictAttribute}=  create dictionary  groups=${listGroup}  policy=${policy}
    ${dictAccessSettings}=  create dictionary  ${access_settings}=${dictAttribute}
    ${jsonAccessSettings}=  evaluate  json.dumps(${dictAccessSettings})  json
    ${result}=  call method  ${nto.sess}  put
    ...    https://${nto.ipaddr}:${nto.ipport}/api/atip_resources/L${atip}-ATIP
    ...    headers=${dictContentType}
    ...    data=${jsonAccessSettings}
    should be equal as integers  ${result.status_code}  ${200}

