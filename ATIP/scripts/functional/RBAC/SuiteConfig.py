from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.ApplicationGroupType import AppGroupType
from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig

class SuiteConfig(object):

    envVars = Environment()
    envVars.adminConnectionConfig = ConnectionConfig('admin', 'admin',
                                                  envVars.atipConnectionConfig.host, envVars.atipConnectionConfig.port,
                                                  'atie/rest')
    envVars.userAConnectionConfig = ConnectionConfig('userA', 'userA',
                                                  envVars.atipConnectionConfig.host, envVars.atipConnectionConfig.port,
                                                  'atie/rest')
    envVars.userBConnectionConfig = ConnectionConfig('userB', 'userB',
                                                  envVars.atipConnectionConfig.host, envVars.atipConnectionConfig.port,
                                                  'atie/rest')
    envVars.userCConnectionConfig = ConnectionConfig('userC', 'userC',
                                                  envVars.atipConnectionConfig.host, envVars.atipConnectionConfig.port,
                                                  'atie/rest')
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    adminSession = WebApiSession(envVars.adminConnectionConfig)
    userASession = WebApiSession(envVars.userAConnectionConfig)
    userBSession = WebApiSession(envVars.userBConnectionConfig)
    userCSession = WebApiSession(envVars.userCConnectionConfig)

    appStatsType = StatsType.APPS
    dynAppStatsType = StatsType.DYNAMIC

    minimizedDashboard = DashboardType.MINIMIZED.statsLimit
    maximizedDashboard = DashboardType.MAXIMIZED.statsLimit

    # expected values
    httpStatusOK = HTTPStatus.OK.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL

    singleApp = FilterConfig("SingleStaticApp")
    FilterConditionConfig.addAppCondition(singleApp, ApplicationType.FACEBOOK)
    Apple = ApplicationType.APPLE

