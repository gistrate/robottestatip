*** Settings ***

Resource    ResourceRBAC.robot
Force Tags  ATIP  functional  ${hardware}  apps

Suite Setup        Create NTO Config RBAC
#Suite Teardown     Recover ATIP Env

Test Setup    Log into the given session  ${userASession}
Test Teardown    Logout from the given session  ${userASession}


*** Variables ***
${RESOURCETIMEOUT}=  10
${atipSlot}=   ${envVars.atipSlotNumber[0]}


*** Test Cases ***

Confirm ATIP RBAC Full Access Login Allowed
    log  Loggging as userA Full Access
    ${result}=  getStatus  ${userASession}
    ${str}=  Catenate  ${result['user']['userName']}
    ...  ${result['user']['userType']}
    ...  ${result['user']['overallStatus']}
    log  ${str}
    should be equal as strings  ${result['user']['userName']}   userA
    should be equal as strings  ${result['user']['userType']}   kNonAdmin
    should be equal as strings  ${result['user']['overallStatus']}   Full Access

Test ATIP RBAC Full Access Delete all and Create a Filter and Modify the Filter
    log  Delete all Filters
    atipFilters.deleteAllFilters  ${userASession}
    log  Create a Filter
    ${filterConfig}=  atipFilters.createFilter  ${userASession}  ${singleApp}
    log  RulePush
    atipFilters.rulePush  ${userASession}

    log  \n
    log  Modify the filter
    log  Add AppCondition Apple
    addAppCondition  ${filterConfig}  ${Apple}
    log  Update the Filter
    atipFilters.updateFilter  ${userASession}  ${filterConfig}
    log  RulePush
    atipFilters.rulePush  ${userASession}

Test ATIP RBAC Full Access System Settings should Fail
    log  Reset Stats
    ${ErrorMessage}=  run keyword and expect error  *  resetStats  ${userASession}
    log  Failed as Expected: ${ErrorMessage}

Test ATIP RBAC Full Access Upload CustomIP
    Log  Uploading Custom Geo file  console=true
    ${file}=  Set variable  ${EXECDIR}/../../../frwk/src/atipAutoFrwk/data/atip/customgeo/IP_locations_restructured1.csv
    uploadCustomIp  ${userASession}  ${file}

Test ATIP RBAC Full Access Upload CustomApp
    Log  Uploading Custom App file  console=true
    ${file}=  Set variable  ${EXECDIR}/../../../frwk/src/atipAutoFrwk/data/custapps/sip-voip.xml
    uploadCustomAppFile  ${userASession}  ${file}

Test ATIP RBAC Full Access Netflow Config
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${userASession}
    ${objNetflowGlobalConfig.enabled}=  Set Variable    True
    Netflow.updateGlobalConfig  ${userASession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${userASession}
    should be true  ${objNetflowGlobalConfig.enabled}
    ${objNetflowGlobalConfig.enabled}=  Set Variable    False
    Netflow.updateGlobalConfig  ${userASession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${userASession}
    should not be true  ${objNetflowGlobalConfig.enabled}

Test ATIP RBAC Full Access SSL Config
    ${response}  updateSslSettings  ${userASession}    True
    should be equal  ${response.passiveSSLDecryption}  ${True}
    ${response}  updateSslSettings  ${userASession}    False
    should be equal  ${response.passiveSSLDecryption}  ${False}

Test ATIP RBAC Full Access MPLS Parsing Config
    ${mplsParsingConfig}  Mpls.getMplsParsingConfig  ${userASession}
    ${mplsParsingConfig.enabled}  set variable  True
    Mpls.updateMplsParsing  ${userASession}  ${mplsParsingConfig}
    ${mplsParsingConfig.enabled}  set variable  False
    Mpls.updateMplsParsing  ${userASession}  ${mplsParsingConfig}

Test ATIP RBAC Full Access Data Masking Config
    ${dataMaskingConfig}  getDataMaskingConfig  ${userASession}
    ${dataMaskingConfig.enabled}  set variable  True
    updateDataMasking  ${userASession}  ${dataMaskingConfig}
    ${dataMaskingConfig.enabled}  set variable  False
    updateDataMasking  ${userASession}  ${dataMaskingConfig}


