*** Settings ***

Resource    ResourceRBAC.robot
Force Tags  ATIP  functional  ${hardware}  apps

Suite Setup        Create NTO Config RBAC
#Suite Teardown     Recover ATIP Env

Test Setup    Log into the given session  ${adminSession}
Test Teardown    Logout from the given session  ${adminSession}


*** Variables ***
${RESOURCETIMEOUT}=  10
${atipSlot}=   ${envVars.atipSlotNumber[0]}


*** Test Cases ***

Test ATIP RBAC Admin Delete all and Create a Filter and Modify the Filter
    log  Delete all Filters
    atipFilters.deleteAllFilters  ${adminSession}
    #Create filter with App Condition Netflix
    log  Create a Filter
    ${filterConfig}=  atipFilters.createFilter  ${adminSession}  ${singleApp}
    log  RulePush
    atipFilters.rulePush  ${adminSession}

    log  \n
    log  Modify the filter
    log  Add AppCondition Apple
    #${filterConfig}=  Set Variable  ${singleApp}
    addAppCondition  ${filterConfig}  ${Apple}
    log  Update the Filter
    atipFilters.updateFilter  ${adminSession}  ${filterConfig}
    log  RulePush
    atipFilters.rulePush  ${adminSession}

Test ATIP RBAC Admin System Settings
    log  Reset Stats
    resetStats  ${adminSession}


Test ATIP RBAC Admin Upload CustomIP
    Log  Uploading Custom Geo file  console=true
    ${file}=  Set variable  ${EXECDIR}/../../../frwk/src/atipAutoFrwk/data/atip/customgeo/IP_locations_restructured1.csv
    uploadCustomIp  ${adminSession}  ${file}

Test ATIP RBAC Admin Upload CustomApp
    Log  Uploading Custom App file  console=true
    ${file}=  Set variable  ${EXECDIR}/../../../frwk/src/atipAutoFrwk/data/custapps/sip-voip.xml
    uploadCustomAppFile  ${adminSession}  ${file}

Test ATIP RBAC Admin Netflow Config
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${adminSession}
    ${objNetflowGlobalConfig.enabled}=  Set Variable    True
    Netflow.updateGlobalConfig  ${adminSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${adminSession}
    should be true  ${objNetflowGlobalConfig.enabled}
    ${objNetflowGlobalConfig.enabled}=  Set Variable    False
    Netflow.updateGlobalConfig  ${adminSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${adminSession}
    should not be true  ${objNetflowGlobalConfig.enabled}

Test ATIP RBAC Admin SSL Config
    ${response}  updateSslSettings  ${adminSession}    True
    should be equal  ${response.passiveSSLDecryption}  ${True}
    ${response}  updateSslSettings  ${adminSession}    False
    should be equal  ${response.passiveSSLDecryption}  ${False}

Test ATIP RBAC Admin MPLS Parsing Config
    ${mplsParsingConfig}  Mpls.getMplsParsingConfig  ${adminSession}
    ${mplsParsingConfig.enabled}  set variable  True
    Mpls.updateMplsParsing  ${adminSession}  ${mplsParsingConfig}
    ${mplsParsingConfig.enabled}  set variable  False
    Mpls.updateMplsParsing  ${adminSession}  ${mplsParsingConfig}

Test ATIP RBAC Admin Data Masking Config
    ${dataMaskingConfig}  getDataMaskingConfig  ${adminSession}
    ${dataMaskingConfig.enabled}  set variable  True
    updateDataMasking  ${adminSession}  ${dataMaskingConfig}
    ${dataMaskingConfig.enabled}  set variable  False
    updateDataMasking  ${adminSession}  ${dataMaskingConfig}


