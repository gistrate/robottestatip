*** Settings ***

Resource    ResourceRBAC.robot
Force Tags  ATIP  functional  ${hardware}  apps

Suite Setup        Create NTO Config RBAC
#Suite Teardown     Recover ATIP Env

Test Setup    Log into the given session  ${userBSession}
Test Teardown    Logout from the given session  ${userBSession}


*** Variables ***
${RESOURCETIMEOUT}=  10
${atipSlot}=   ${envVars.atipSlotNumber[0]}


*** Test Cases ***

Confirm ATIP RBAC Read Only Login Allowed
    log  Loggging as userB Read Only
    ${result}=  getStatus  ${userBSession}
    ${str}=  Catenate  ${result['user']['userName']}
    ...  ${result['user']['userType']}
    ...  ${result['user']['overallStatus']}
    log  ${str}
    should be equal as strings  ${result['user']['userName']}   userB
    should be equal as strings  ${result['user']['userType']}   kNonAdmin
    should be equal as strings  ${result['user']['overallStatus']}   Read-Only

Test ATIP RBAC Read Only Create a Filter Denied
    log  Create a Filter
    ${ErrorMessage}=  run keyword and expect error  *  atipFilters.createFilter  ${userBSession}  ${singleApp}
    log  Failed as Expected: ${ErrorMessage}
    log  RulePush
    ${ErrorMessage}=  run keyword and expect error  *  atipFilters.rulePush  ${userBSession}
    log  Failed as Expected: ${ErrorMessage}

    log  \n
    log  Modify the filter
    log  Add AppCondition Apple
    ${filterConfig}=  Set Variable  ${singleApp}
    addAppCondition  ${filterConfig}  ${Apple}
    # This is the placeholder to adapt NP_Group change.
    # Mihai and Noopur keep changing AddCondition keyword behavior.
    # I'm going to wait for them to settle down into an agreement.
    #log  Update the Filter
    #${ErrorMessage}=  run keyword and expect error  *  atipFilters.updateFilterByName  ${userBSession}  SingleStaticApp  ${filterConfig}
    #log  Failed as Expected: ${ErrorMessage}
    #log  RulePush
    #${ErrorMessage}=  run keyword and expect error  *  atipFilters.rulePush  ${userBSession}
    #log  Failed as Expected: ${ErrorMessage}


Test ATIP RBAC Read Only System Settings should Fail
    log  Reset Stats
    ${ErrorMessage}=  run keyword and expect error  *  resetStats  ${userBSession}
    log  Failed as Expected: ${ErrorMessage}


Test ATIP RBAC Read Only Upload CustomIP Denied
    Log  Uploading Custom Geo file  console=true
    ${file}=  Set variable  ${EXECDIR}/../../../frwk/src/atipAutoFrwk/data/atip/customgeo/IP_locations_restructured1.csv
    ${ErrorMessage}=  run keyword and expect error  *  uploadCustomIp  ${userBSession}  ${file}
    log  Failed as Expected: ${ErrorMessage}

Test ATIP RBAC Read Only Upload CustomApp Denied
    Log  Uploading Custom App file  console=true
    ${file}=  Set variable  ${EXECDIR}/../../../frwk/src/atipAutoFrwk/data/custapps/sip-voip.xml
    ${ErrorMessage}=  run keyword and expect error  *  uploadCustomAppFile  ${userBSession}  ${file}
    log  Failed as Expected: ${ErrorMessage}

Test ATIP RBAC Read Only Netflow Config Denied
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${userBSession}
    ${objNetflowGlobalConfig.enabled}=  Set Variable    True
    ${ErrorMessage}=  run keyword and expect error  *  Netflow.updateGlobalConfig  ${userBSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    log  Failed as Expected: ${ErrorMessage}

Test ATIP RBAC Read Only SSL Config Denied
    ${ErrorMessage}=  run keyword and expect error  *  updateSslSettings  ${userBSession}    True
    log  Failed as Expected: ${ErrorMessage}

Test ATIP RBAC Read Only MPLS Parsing Config Denied
    ${mplsParsingConfig}  Mpls.getMplsParsingConfig  ${userBSession}
    ${mplsParsingConfig.enabled}  set variable  True
    ${ErrorMessage}=  run keyword and expect error  *  Mpls.updateMplsParsing  ${userBSession}  ${mplsParsingConfig}
    log  Failed as Expected: ${ErrorMessage}

Test ATIP RBAC Read Only Data Masking Config Denied
    ${dataMaskingConfig}  getDataMaskingConfig  ${userBSession}
    ${dataMaskingConfig.enabled}  set variable  True
    ${ErrorMessage}=  run keyword and expect error  *  updateDataMasking  ${userBSession}  ${dataMaskingConfig}
    log  Failed as Expected: ${ErrorMessage}


