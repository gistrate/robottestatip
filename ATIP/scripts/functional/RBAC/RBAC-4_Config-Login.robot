*** Settings ***

Resource    ResourceRBAC.robot
Force Tags  ATIP  functional  ${hardware}  apps

Suite Setup        Create NTO Config RBAC
#Suite Teardown     Recover ATIP Env
#Test Setup    Log into the given session  ${adminSession}
#Test Teardown    Logout from the given session  ${adminSession}


*** Variables ***
${RESOURCETIMEOUT}=  10
${atipSlot}=   ${envVars.atipSlotNumber[0]}


*** Test Cases ***

Test NTO RBAC View Allow All
#Test View Allow All
    ${listEmpty}=  create list
    ${policy}=  set variable  ALLOW_ALL
    ${access_settings}=  set variable  view_access_settings
    ApplyAtipAccessControl  ${nto1}  ${atipSlot}  ${listEmpty}  ${policy}  ${access_settings}

Test NTO RBAC Modify Allow All
#Test Modify Allow All
    ${listEmpty}=  create list
    ${policy}=  set variable  ALLOW_ALL
    ${access_settings}=  set variable  modify_access_settings
    ApplyAtipAccessControl  ${nto1}  ${atipSlot}  ${listEmpty}  ${policy}  ${access_settings}

Test NTO RBAC View Require Admin
#Test View Require Admin
    ${listEmpty}=  create list
    ${policy}=  set variable  REQUIRE_ADMIN
    ${access_settings}=  set variable  view_access_settings
    ApplyAtipAccessControl  ${nto1}  ${atipSlot}  ${listEmpty}  ${policy}  ${access_settings}

Test NTO RBAC Modify Require Admin
#Test Modify Require Admin
    ${listEmpty}=  create list
    ${policy}=  set variable  REQUIRE_ADMIN
    ${access_settings}=  set variable  modify_access_settings
    ApplyAtipAccessControl  ${nto1}  ${atipSlot}  ${listEmpty}  ${policy}  ${access_settings}

Test NTO RBAC View Require Member
#Test View Require Group
    ${listGroup}=  create list  gView
    ${policy}=  set variable  REQUIRE_MEMBER
    ${access_settings}=  set variable  view_access_settings
    ApplyAtipAccessControl  ${nto1}  ${atipSlot}  ${listGroup}  ${policy}  ${access_settings}

Test NTO RBAC Modify Require Member
#Test Modify Require Group
    ${listGroup}=  create list  gModify
    ${policy}=  set variable  REQUIRE_MEMBER
    ${access_settings}=  set variable  modify_access_settings
    ApplyAtipAccessControl  ${nto1}  ${atipSlot}  ${listGroup}  ${policy}  ${access_settings}


Test ATIP RBAC Admin Login Allowed
    log  \n
    login    ${adminSession}
    log  Loggging as admin
    ${result}=  getStatus  ${adminSession}
    ${str}=  Catenate  ${result['user']['userName']}
    ...  ${result['user']['userType']}
    ...  ${result['user']['overallStatus']}
    should be equal as strings  ${result['user']['userName']}   admin
    should be equal as strings  ${result['user']['userType']}   kAdmin
    should be equal as strings  ${result['user']['overallStatus']}   Full Access
    log  ${str}
    logout   ${adminSession}

Test ATIP RBAC Full Access Login Allowed
    log  \n
    login    ${userASession}
    log  Loggging as userA Full Access
    ${result}=  getStatus  ${userASession}
    ${str}=  Catenate  ${result['user']['userName']}
    ...  ${result['user']['userType']}
    ...  ${result['user']['overallStatus']}
    should be equal as strings  ${result['user']['userName']}   userA
    should be equal as strings  ${result['user']['userType']}   kNonAdmin
    should be equal as strings  ${result['user']['overallStatus']}   Full Access
    log  ${str}
    logout   ${userASession}

Test ATIP RBAC Read Only Login Allowed
    log  \n
    login    ${userBSession}
    log  Loggging as userB Read-Only
    ${result}=  getStatus  ${userBSession}
    ${str}=  Catenate  ${result['user']['userName']}
    ...  ${result['user']['userType']}
    ...  ${result['user']['overallStatus']}
    should be equal as strings  ${result['user']['userName']}   userB
    should be equal as strings  ${result['user']['userType']}   kNonAdmin
    should be equal as strings  ${result['user']['overallStatus']}   Read-Only
    log  ${str}
    logout   ${userBSession}

Test ATIP RBAC No Access Login Refused
    log  \n
    login    ${userBSession}
    log  Loggging as userC No Access
    ${ErrorMessage}=  run keyword and expect error  *  login    ${userCSession}
    log  Failed as Expected: ${ErrorMessage}
    #${result}=  getStatus  ${userCSession}
    #logout   ${userCSession}


Test ATIP RBAC Admin Login Allowed Retry
    log  \n
    login    ${adminSession}
    log  Loggging as admin
    ${result}=  getStatus  ${adminSession}
    ${str}=  Catenate  ${result['user']['userName']}
    ...  ${result['user']['userType']}
    ...  ${result['user']['overallStatus']}
    should be equal as strings  ${result['user']['userName']}   admin
    should be equal as strings  ${result['user']['userType']}   kAdmin
    should be equal as strings  ${result['user']['overallStatus']}   Full Access
    log  ${str}
    logout   ${adminSession}

