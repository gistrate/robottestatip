from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.webApi.atip.stats.Pie import Pie
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.webApi.atip.TopFilters import TopFilters
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.webApi.traffic.BpsComponentStats import BpsComponentStats
from atipAutoFrwk.services.traffic.BpsComponentStatsService import BpsComponentStatsService
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.data.traffic.RealTimeStats import RealTimeStats
import re
import jsonpickle

class EncryptedFilterLib(object):

    def configureFilter(self, filterName, forward=False, vlanId='', encrypted=False, dropTraffic=False, appName=None):
        filterConfig = FilterConfig(filterName)
        filterConfig.forward = forward
        filterConfig.encrypted = encrypted
        filterConfig.dropTraffic = dropTraffic
        if StatsDefaultConfig.envVars.atipType == AtipType.HARDWARE:
            filterConfig.addTransmitIds(vlanId)

        if not isinstance(appName, list):
            for member in ApplicationType.__members__.values():
                if member.appName == appName:
                    FilterConditionConfig.addAppCondition(filterConfig, member)
        else:
            # build list of AppType objects corresponding to appList
            appType = []
            for app in appName:
                for member in ApplicationType.__members__.values():
                    if member.appName == app:
                        appType.append(member)

            FilterConditionConfig.addAppCondition(filterConfig, appType)

        return filterConfig


    @classmethod
    def getFilterStatsActualValue(cls, topFiltersStats, filterName, typeStat):
        '''
        Return counter for specified statistic
        :param topFiltersStats: top 10 filter statistics
        :param filterName: name of filter to get statistics for
        :param typeStat: statistics field to get value for
        :return: specific statistic value
        '''
        actualValue = 0
        if not isinstance(topFiltersStats['stats'][0]['list'], list):
            topFiltersStats['stats'][0]['list'] = [topFiltersStats['stats'][0]['list']]
        for statsItem in topFiltersStats['stats'][0]['list']:
            if statsItem['msg'] == filterName:
                if typeStat == TopStats.TOTAL_COUNT:
                    actualValue = statsItem['totalCount']
                elif typeStat == TopStats.TOTAL_BYTES:
                    actualValue = statsItem['totalBytes']
                elif typeStat == TopStats.TOTAL_PKTS:
                    actualValue = statsItem['totalPkts']
                break

        return actualValue

    @classmethod
    def getPieStatsActualValue(cls, pieStatsContainer, app, typeStat):
        '''
        Return counter for specified statistic
        :param pieStatsContainer: all applications statistics
        :param app: application name to get statistics for
        :param typeStat: statistics field to get value for
        :return: specific statistic value
        '''
        actualValue = 0
        pieData = pieStatsContainer.getList()
        for pieStatsItem in pieData:
            if pieStatsItem.getName() == app:
                if typeStat == TopStats.TOTAL_COUNT:
                    actualValue = pieStatsItem.getSessions()
                elif typeStat == TopStats.TOTAL_BYTES:
                    actualValue = pieStatsItem.getBytes()
                elif typeStat == TopStats.TOTAL_PKTS:
                    actualValue = pieStatsItem.getPkts()
                break

        return actualValue

    @classmethod
    def getAppTopStats(cls, webApiSession, applications, statType):

        appStats = 0

        topStats = Pie.getTopStats(webApiSession, StatsType.APPS)

        if not topStats:
            raise Exception('No statistics')

        if not isinstance(applications, list):
            applications = [applications]

        for item in topStats.getList():
            for app in applications:
                if item.getName() == app:
                    appStats = cls.getPieStatsActualValue(topStats, app, statType)

        return appStats

    @classmethod
    def configFilterCommand(cls, testId, displayFilter='', format="text", vlanId="", outFilename=""):
        tsharkCommand = ""
        pcap = '{0}.pcap'.format(testId)
        resultFilename = testId

        if outFilename != "":
            resultFilename = outFilename

        if displayFilter:
            if format == "text":
                if vlanId != "":
                    tsharkCommand = "tshark -r {0} -Y '{1} and vlan.id=={2}' > {3}.result".format(pcap, displayFilter, vlanId, resultFilename)
                else:
                    tsharkCommand = "tshark -r {0} -Y '{1}' > {2}.result".format(pcap, displayFilter, resultFilename)
            elif format == "hex":
                tsharkCommand = "tshark -r {0} -Y '{1}' > {2}.result".format(pcap, displayFilter, resultFilename)
            else:
                raise Exception("Use correct filter capture parameter: text or hex value")
        else:
            tsharkCommand = "tshark -r {0} > {1}.result".format(pcap, resultFilename)

        return tsharkCommand

    @classmethod
    def verifyComponentStats(cls, webApiSession, bpsSession, bpsTestConfig, bpsComponent, filterName):
        bpsStats = BpsComponentStats.getAllComponentsStats(bpsSession, bpsTestConfig)
        bpsSessions = BpsComponentStatsService.getStatItem(bpsStats['{}'.format(bpsComponent)], BpsStatsType.SESSIONS)
        topFilters = TopFilters.getTopFiltersStats(webApiSession)
        filterSessions = cls.getFilterStatsActualValue(topFilters, filterName, TopStats.TOTAL_COUNT)

        if int(bpsSessions) != filterSessions:
            raise Exception('Number of sessions bps/filter is not the same. BPS sessions: {}, filter sessions: {}'.format(bpsSession,filterSessions))

    def verifyTotalStats(self, atipStat, bpsSession, bpsTestConfig):
        bpsTotalPkts = BpsStatsService.getRealTimeStatsByName(bpsSession, bpsTestConfig.testId, RealTimeStats.ETH_TX_FRAMES)

        if bpsTotalPkts != float(atipStat):
            raise Exception('Total stats bps/atip are not the same. BPS TX: {}, Atip RX: {}'.format(bpsTotalPkts, atipStat))

    def returnRgxValue(self, stats, regex):
        return re.findall(r'{}'.format(regex), stats)








