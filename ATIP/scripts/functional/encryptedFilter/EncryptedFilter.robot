*** Settings ***
Documentation  Encrypted Filter test suite.

Variables  SuiteConfig.py
Variables  ../stats/lib/StatsDefaultConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.Netflow
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.services.atip.utility.IxFlowParsing
Library    atipAutoFrwk.webApi.atip.EngineInfo
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.services.atip.CaptureService
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.data.traffic.RealTimeStats
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    Telnet  timeout=30  prompt=$
Library    BuiltIn
Library    String
Library    EncryptedFilterLib.py

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP
Suite Teardown     Clear ATIP


*** Test Cases ***

# EncryptedTest5              bpsTest                             filter1       filter2       app      filterHost
TC001011889              FacebookAndEncrypted_TC001011889        Encrypted     Facebook     facebook  facebook.com
    [Teardown]  Close All Connections
    [Template]  Verify Encrypted 1

# EncryptedTest6              bpsTest                 filter1       filter2      filter3       app1      app2      filterHost
TC001011899               Encrypted_TC001011899      Encrypted     Netflix      Facebook    netflix    facebook   netflix.com:443
    [Setup]  Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    [Template]  Verify Encrypted 2
    [Tags]  hardware


*** Keywords ***
Verify Encrypted 1
    [Arguments]  ${bpsFile}  ${filter1}  ${filter2}  ${app}  ${filterHost}

    deleteAllFilters  ${atipSession}

    # create encrypted drop filter
    ${dropFilterConfig}=  configureFilter  ${filter1}  ${False}  ${EMPTY}  ${True}  ${True}  ${None}
    createFilter  ${atipSession}  ${dropFilterConfig}

    # create Facebook app forwarding filter
    ${appFwdFilterConfig}=  configureFilter  ${filter2}  ${True}  100  ${False}  ${False}  ${app}
    createFilter  ${atipSession}  ${appFwdFilterConfig}

    # push changes to NP
    rulePush  ${atipSession}

    resetStats  ${atipSession}
    getRxProcPacketsFromDebugPage  ${atipSession}  ${envVars}

    Connect on Linux Box
    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap -B 30720
    Read Until  ${envVars.linuxConnectionConfig.port}

    # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  2

    Connect on Linux Box
    Write  cd captures

    ${debugStats}=  readDebug  ${atipSession}

    # verify number of session per component on Atip against corresponding values from BPS; Top Filter stats are used
    verifyComponentStats  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  appsim_1  ${filter1}
    verifyComponentStats  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  appsim_3  ${filter2}

    # verify total rx packets on Atip against total tx packets from BPS; I have used ipp_tcp statistics because is accurate all the time
    @{ippTcp}=  returnRgxValue  ${debugStats}  NP::${envVars.atipSlotNumber[0]}[\\S\\s]+?(\\d+)\\s+(\\d+)\\s+ipp_tcp
    verifyTotalStats  ${ippTcp[0][0]}  ${bpsSession}  ${bpsTestConfig}

    # filter linux box capture to get total number of packets received and compare it with tx packets from debug page
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}  ${EMPTY}  text  ${EMPTY}  ${TEST NAME}_1
    Execute Command  ${filteringCommand}
    @{tx}=  returnRgxValue  ${debugStats}  NP::${envVars.atipSlotNumber[0]}[\\S\\s]+?(\\d+)\\s+(\\d+)\\s+tx\\s
    matches validation  ${TEST NAME}_1  ${tx[0][0]}

    # validate that BPS TX pkts for component 3 is matching TX Atip;
    # BPS TX pkts for Facebook component is not TX pkts from BPS for that component, because it would be hard to validate against TX Atip pkts (don't know for sure which pkts are dropped by kernel or filter and not forwarded)
    # but is calculated as number of sessions * 4 pkts (these pkts are further forwarded by Atip (2 data pkts and 2 FIN ACK pkts)
    ${bpsStats}=  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig}
    ${filter3Stats}=  getStatItem  ${bpsStats['appsim_3']}  ${bpsSessions}
    ${calcForwardTraffic}=  evaluate  ${filter3Stats} * 4

    should be equal as integers  ${tx[0][0]}  ${calcForwardTraffic}

Verify Encrypted 2
    [Arguments]  ${bpsFile}  ${filter1}  ${filter2}  ${filter3}  ${app1}  ${app2}  ${filterHost}

    deleteAllFilters  ${atipSession}

    # enable SSL
    updateSslSettings  ${atipSession}  True
    deleteAllCerts  ${atipSession}

    # upload SSL Cert and key
    uploadSeparateFiles  ${atipSession}  ${filter2}  ${serverCert}  ${serverKey}

    ${dropFilterConfig}=  configureFilter  ${filter1}  ${False}  ${EMPTY}  ${True}  ${True}  ${None}
    createFilter  ${atipSession}  ${dropFilterConfig}

    ${appFwdFilterConfig}=  configureFilter  ${filter2}  ${True}  100  ${False}  ${False}  ${app1}
    createFilter  ${atipSession}  ${appFwdFilterConfig}

    ${dropFilterAppConfig}=  configureFilter  ${filter3}  ${False}  ${EMPTY}  ${False}  ${True}  ${app2}
    createFilter  ${atipSession}  ${dropFilterAppConfig}

    # push changes to NP
    rulePush  ${atipSession}

    resetStats  ${atipSession}
    getRxProcPacketsFromDebugPage  ${atipSession}  ${envVars}

    Connect on Linux Box
    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap -B 30720
    Read Until  ${envVars.linuxConnectionConfig.port}

    # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  1

    Connect on Linux Box
    Write  cd captures

    # filter linux box capture to get number of Netflix sessions and compare it with Top Filters and pie App Dsitribution session number for Netflix
    ${displayFilter}=  catenate  http.host == "${filterHost}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_2
    Execute Command  ${filteringCommand}
    ${topFilters}=  getTopFiltersStats  ${atipSession}
    ${pieStats}=  getAppTopStats  ${atipSession}  ${app1}  ${atipSessions}
    ${filterSessions}=   getFilterStatsActualValue  ${topFilters}  ${filter2}  ${atipSessions}
    matches validation  ${TEST NAME}_2  ${filterSessions}
    matches validation  ${TEST NAME}_2  ${pieStats}

    # filter linux box capture to verify the other traffic is not present
    ${displayFilter}=  catenate  http.host != "${filterHost}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_3
    Execute Command  ${filteringCommand}
    matches validation  ${TEST NAME}_3  0

    # filter linux box capture to get total number of packets received and compare it with tx packets from debug page
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}  ${EMPTY}  text  ${EMPTY}  ${TEST NAME}_1
    Execute Command  ${filteringCommand}
    ${debugStats}=  readDebug  ${atipSession}
    @{tx}=  returnRgxValue  ${debugStats}  NP::${envVars.atipSlotNumber[0]}[\\S\\s]+?(\\d+)\\s+(\\d+)\\s+tx\\s
    matches validation  ${TEST NAME}_1  ${tx[0][0]}

    # verify total rx packets on Atip against total tx packets from BPS; I have used ipp_tcp statistics because is accurate all the time
    @{ippTcp}=  returnRgxValue  ${debugStats}  NP::${envVars.atipSlotNumber[0]}[\\S\\s]+?(\\d+)\\s+(\\d+)\\s+ipp_tcp
    verifyTotalStats  ${ippTcp[0][0]}  ${bpsSession}  ${bpsTestConfig}

    # verify number of session per component on Atip against corresponding values from BPS; Top Filter stats are used
    verifyComponentStats  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  appsim_1  ${filter1}
    verifyComponentStats  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  appsim_2  ${filter2}
    verifyComponentStats  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  appsim_3  ${filter3}

    ${bpsStats}=  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig}
    ${filter2Stats}=  getStatItem  ${bpsStats['appsim_2']}  ${bpsSessions}
    ${calcForwardTraffic}=  evaluate  ${filter2Stats} * 7

    should be equal as integers  ${tx[0][0]}  ${calcForwardTraffic}


Matches Validation
    [Arguments]  ${STRING}  ${expectedValue}
    ${output}=  Execute Command  wc -l < ${STRING}.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Be Equal As Integers  ${statsCaptureLB}  ${expectedValue}

Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [return]  ${output}

Connect on Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Read
    [return]  ${openTelnet}

Become SU
    Write  su
    sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt

Stop Capture
    [Arguments]  ${index}
    Switch Connection  ${index}
    Write Control Character     BRK
    Read Until Prompt
    Close Connection

Configure ATIP
    atipLogin.Login  ${atipSession}

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}

    Connect on Linux Box
    # be sure that "captures" folder is deleted because it happened to remain created by root and had write protection against "atip" user; hence, no capture filter result file could be created by "atip" user when running tshark filtering command
    Become SU
    Write  rm -rf captures
    Write  exit

    # create temp folder for testing captures; it should be owned by "atip" user
    # create temp folder for testing captures
    Write  mkdir captures
    ${command} =  copyFileToLinux
    Write  ${command}
    Close Connection

Clear ATIP
    deleteAllFilters  ${atipSession}

    Connect on Linux Box
    Execute Command  rm -rf captures

    atipLogout.Logout  ${atipSession}