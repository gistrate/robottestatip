'''
Created on Jul 21, 2017

@author: dona
'''
from ConfigFile import ConfigFile

config = ConfigFile()

PATH = '/opt/atie/scripts/patches/'
SCRIPTPATH = PATH + 'r1.5/db_update_r1.5.2.sql' 
PSQL_COMMAND_USER = 'psql -U '
PSQL_COMMAND_ALL = ' -a '
PSQL_COMMAND_FILE = '-f '
PSQL_COMMAND_DATABASE = ' -d '
SQL_COMMAND = PSQL_COMMAND_USER + config.user + PSQL_COMMAND_DATABASE + config.datName + PSQL_COMMAND_ALL + PSQL_COMMAND_FILE