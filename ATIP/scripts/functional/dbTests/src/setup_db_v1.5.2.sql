ALTER ROLE postgres SET statement_timeout = 600000;

create table rule (
	id int8 primary key, 
	name varchar(120) not null, 
	session_id character varying(255), 
	is_active bool, 
	forward boolean default false, 
	netflow boolean default false, 
	decryptedOnly bool default false, 
	deny boolean default false, 
	is_explore boolean, 
	priority bigint default 1000
);

create table gre_tunnel_conf (id bigint primary key);
 create index rule_name on rule(name, is_active);
 create index rule_sess on rule(session_id);
 
CREATE TABLE application
(
  id bigint NOT NULL,
  content_hash character varying(255),
  hidden boolean,
  is_active boolean,
  last_update timestamp without time zone,
  name character varying(128),
  orgname character varying(255),
  CONSTRAINT application_pkey PRIMARY KEY (id),
  CONSTRAINT application_name_key UNIQUE (name)
);

create index application_name on application(name,is_active);

CREATE TABLE dynamic_application
(
  id bigint NOT NULL,
  name character varying(128),
  is_active boolean,
  content_hash character varying(255),
  xml character varying,
  hidden boolean DEFAULT false,
  last_update timestamp without time zone,
  orgname character varying(255),
  device character varying(255),
  port character varying(255),
  transport character varying(255),
  CONSTRAINT dynamic_application_pkey PRIMARY KEY (id),
  CONSTRAINT dynamic_application_name_key UNIQUE (name)
);
create index dynamic_application_name on dynamic_application(name,is_active);
create index dyn_app_hash on dynamic_application(name,content_hash);

CREATE TABLE services_application
(
  id bigint NOT NULL,
  name character varying(128),
  is_active boolean,
  hidden boolean DEFAULT false,
  CONSTRAINT services_application_pkey PRIMARY KEY (id),
  CONSTRAINT services_application_name_key UNIQUE (name)
);
create index services_application_name on services_application(name);

CREATE TABLE app_seq
(
  key character varying(255) NOT NULL,
  next bigint,
  CONSTRAINT app_seq_pkey PRIMARY KEY (key)
);



create table match_action (id int8 primary key) ;
create table rule_filter (id int8 primary key, rule_id int8 references rule(id) on delete cascade,type character varying(32) NOT NULL) ;
 create index rule_filter_rid on rule_filter(rule_id,type);
create table app_signature(id int8 primary key, xml_id varchar(128) unique, xml text) ;
 create index app_signature_xmlid on app_signature(xml_id);
create table app_action (id int8 primary key, unique_name varchar(256) unique, app_id int8 references application(id) on delete cascade);
 create index app_action_fk1 on app_action(app_id);
create table tag (id int8 primary key, tag_group varchar(32), tag_name varchar(32)) ;
 create index tag_grp on tag(tag_group, tag_name);
create table device_type (id int8 primary key, name varchar(32), is_mobile bool);
create table port_description (id int8 primary key, name varchar(32)) ;
create table stat_interface (id int8 primary key, ts timestamp without time zone NOT NULL);
 create index stat_interface_ts on stat_interface(ts);
create table stat_pkt (id int8 primary key, ts timestamp without time zone NOT NULL);
 create index stat_pkt_ts on stat_pkt(ts);

create table stat_application (id int8 primary key, ts timestamp without time zone NOT NULL, app character varying(255),app_action_id bigint, app_id bigint);
 create index stat_app_ts on stat_application(ts,app);
 
CREATE TABLE stat_agg_app(id int8 primary key, ts timestamp without time zone NOT NULL, app character varying(255));
 create index stat_agg_app_1 on stat_agg_app(ts,app);
CREATE TABLE stat_agg_app_20sec_inv(ts timestamp without time zone NOT NULL, app character varying(255), bytes int8, pkts int8, flows int8);
 create index stat_agg_app_20sec_inv_1 on stat_agg_app_20sec_inv(ts,app);
CREATE TABLE stat_agg_app_20min_inv(ts timestamp without time zone NOT NULL, app character varying(255), bytes int8, pkts int8, flows int8);
 create index stat_agg_app_20min_inv_1 on stat_agg_app_20min_inv(ts,app);
CREATE TABLE stat_agg_match(id int8 primary key, ts timestamp without time zone NOT NULL, rule_id int8);
 create index stat_agg_match_1 on stat_agg_match(ts,rule_id);
CREATE TABLE stat_agg_match_20sec_inv(ts timestamp without time zone NOT NULL, rule_id int8, bytes int8, pkts int8, flows int8);
 create index stat_agg_match_20sec_inv_1 on stat_agg_match_20sec_inv(ts,rule_id);
CREATE TABLE stat_agg_match_20min_inv(ts timestamp without time zone NOT NULL, rule_id int8, bytes int8, pkts int8, flows int8);
 create index stat_agg_match_20min_inv_1 on stat_agg_match_20min_inv(ts,rule_id);
 
 create index stat_app_aid on stat_application(ts,app_id);
create table stat_geo (id int8 primary key, ts timestamp without time zone NOT NULL, c_cid bigint, s_cid bigint);
 create index stat_geo_tsc on stat_geo(ts,c_cid);
 create index stat_geo_tss on stat_geo(ts,s_cid);
create table stat_device (id int8 primary key, ts timestamp without time zone NOT NULL, app varchar(255), c_cid bigint);
 create index stat_device_tsc on stat_device(ts,app,c_cid);

create table stat_provider 
(
	id int8 primary key,  
	ts timestamp without time zone NOT NULL, 
	asn bigint,
	conns bigint, 
	pkts_rx bigint, 
	pkts_tx bigint, 
	bytes_rx bigint, 
	bytes_tx bigint
);
 create index stat_provider_ts_id on stat_provider(ts, id);
 create index stat_provider_asn on stat_provider(asn);
 
CREATE TABLE stat_match(id int8 primary key, ts timestamp without time zone NOT NULL, rule_id int8, device varchar(255));
 create index stat_match_trd on stat_match(ts,rule_id);
 
create table stat_util (last_stat_match bigint);
CREATE TABLE appaction_appsignature(action_id bigint NOT NULL,sig_id bigint NOT NULL, CONSTRAINT appaction_appsignature_pkey PRIMARY KEY (action_id, sig_id), CONSTRAINT fk8ab70f5ff1f95f92 FOREIGN KEY (sig_id) REFERENCES app_signature (id) ON DELETE cascade,CONSTRAINT fk8ab70f5ffb8cd6ef FOREIGN KEY (action_id) REFERENCES app_action (id) ON DELETE cascade);
 create index appaction_appsignature_aid on appaction_appsignature(action_id);
 create index appaction_appsignature_sid on appaction_appsignature(sig_id);
CREATE TABLE appaction_tag ( appaction_id bigint NOT NULL, tag_id bigint NOT NULL, CONSTRAINT appaction_tag_pkey PRIMARY KEY (appaction_id, tag_id), CONSTRAINT fk4fd266f26b8d9d08 FOREIGN KEY (tag_id) REFERENCES tag (id) on delete cascade, CONSTRAINT fk4fd266f2ea0c31ee FOREIGN KEY (appaction_id) REFERENCES app_action (id) on delete cascade ); 
 create index appaction_tag_aid on appaction_tag(appaction_id);
 create index appaction_tag_tid on appaction_tag(tag_id);
CREATE TABLE application_appsignature ( app_id bigint NOT NULL, sig_id bigint NOT NULL, CONSTRAINT application_appsignature_pkey PRIMARY KEY (app_id, sig_id), CONSTRAINT fke9377a46eeeabff9 FOREIGN KEY (app_id) REFERENCES application (id) on delete cascade, CONSTRAINT fke9377a46f1f95f92 FOREIGN KEY (sig_id) REFERENCES app_signature (id) on delete cascade);
 create index application_appsignature_aid on application_appsignature(app_id);
 create index application_appsignature_sid on application_appsignature(sig_id);
CREATE TABLE application_tag ( app_id bigint NOT NULL, tag_id bigint NOT NULL, CONSTRAINT application_tag_pkey PRIMARY KEY (app_id, tag_id), CONSTRAINT fkb5adcc2b6b8d9d08 FOREIGN KEY (tag_id) REFERENCES tag (id) on delete cascade, CONSTRAINT fkb5adcc2beeeabff9 FOREIGN KEY (app_id) REFERENCES application (id) on delete cascade ); 
 create index application_tag_aid on application_tag(app_id);
 create index application_tag_tid on application_tag(tag_id);
CREATE TABLE appsig_tag ( sig_id bigint NOT NULL, tag_id bigint NOT NULL, CONSTRAINT appsig_tag_pkey PRIMARY KEY (sig_id, tag_id), CONSTRAINT fk5cb9e9eb6b8d9d08 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE, CONSTRAINT fk5cb9e9ebf1f95f92 FOREIGN KEY (sig_id) REFERENCES app_signature (id) ON DELETE CASCADE ); 
 create index appsig_tag_tid on appsig_tag(tag_id);
 create index appsig_tag_sid on appsig_tag(sig_id);
CREATE TABLE app_appaction ( app_id bigint NOT NULL, action_id bigint NOT NULL, CONSTRAINT app_appaction_pkey PRIMARY KEY (app_id, action_id), CONSTRAINT fkf9081af9eeeabff9 FOREIGN KEY (app_id) REFERENCES application (id) , CONSTRAINT fkf9081af9fb8cd6ef FOREIGN KEY (action_id) REFERENCES app_action (id) ); 
 create index app_appaction_aid on app_appaction(action_id);
 create index app_appaction_acid on app_appaction(app_id);
CREATE TABLE dynapp_appsignature ( app_id bigint NOT NULL, sig_id bigint NOT NULL, CONSTRAINT dynapp_appsignature_pkey PRIMARY KEY (app_id, sig_id), CONSTRAINT fk32e7958eb6ecfac8 FOREIGN KEY (app_id) REFERENCES dynamic_application (id) on delete cascade, CONSTRAINT fk32e7958ef1f95f92 FOREIGN KEY (sig_id) REFERENCES app_signature (id) on delete cascade ); 
 create index dynapp_appsignature_sid on dynapp_appsignature(sig_id);
 create index dynapp_appsignature_aid on dynapp_appsignature(app_id);
CREATE TABLE dynapp_appaction( app_id bigint NOT NULL, action_id bigint NOT NULL, CONSTRAINT dynapp_appaction_pkey PRIMARY KEY (app_id, action_id), CONSTRAINT fk9c04a120b6ecfac8 FOREIGN KEY (app_id) REFERENCES dynamic_application (id) on delete cascade, CONSTRAINT fk9c04a120fb8cd6ef FOREIGN KEY (action_id) REFERENCES app_action (id) on delete cascade ); 
 create index dynapp_appaction_acid on dynapp_appaction(action_id);
 create index dynapp_appaction_aid on dynapp_appaction(app_id);
CREATE TABLE dynapp_tag ( dynapp_id bigint NOT NULL, tag_id bigint NOT NULL, CONSTRAINT dynapp_tag_pkey PRIMARY KEY (dynapp_id, tag_id), CONSTRAINT fkf47c5fe36b8d9d08 FOREIGN KEY (tag_id) REFERENCES tag (id) on delete cascade, CONSTRAINT fkf47c5fe376040281 FOREIGN KEY (dynapp_id) REFERENCES dynamic_application (id) on delete cascade ); 
 create index dynapp_tag_did on dynapp_tag(dynapp_id);
 create index dynapp_tag_tid on dynapp_tag(tag_id);
CREATE TABLE rule_filter_application ( id bigint NOT NULL, rule_filter_id bigint, app_action_id bigint, app_id bigint NOT NULL, CONSTRAINT rule_filter_application_pkey PRIMARY KEY (id), CONSTRAINT rule_filter_application_rule_filter_id_fkey FOREIGN KEY (rule_filter_id) REFERENCES rule_filter (id) ON DELETE CASCADE ); 
 create index rule_filter_application_rfid on rule_filter_application(rule_filter_id);
 create index rule_filter_application_aaid on rule_filter_application(app_action_id);
 create index rule_filter_application_aid on rule_filter_application(app_id);
CREATE TABLE rule_filter_dyn_application ( id bigint NOT NULL, app_id bigint NOT NULL, rule_filter_id bigint, CONSTRAINT rule_filter_dyn_application_pkey PRIMARY KEY (id), CONSTRAINT fk830d5a66768276ab FOREIGN KEY (rule_filter_id) REFERENCES rule_filter (id) on delete cascade ); 
 create index rule_filter_dyn_application_rfid on rule_filter_dyn_application(rule_filter_id);
 create index rule_filter_dyn_application_aid on rule_filter_dyn_application(app_id);
CREATE TABLE rule_filter_geo ( id bigint NOT NULL, rule_filter_id bigint, geo_city bigint, geo_country bigint, geo_region bigint, CONSTRAINT rule_filter_geo_pkey PRIMARY KEY (id), CONSTRAINT rule_filter_geo_rule_filter_id_fkey FOREIGN KEY (rule_filter_id) REFERENCES rule_filter (id) ON DELETE CASCADE ); 
 create index rule_filter_geo_rfid on rule_filter_geo(rule_filter_id);
 create index rule_filter_geo_gid on rule_filter_geo(geo_country, geo_region, geo_city);
CREATE TABLE rule_filter_ip_range ( id bigint NOT NULL, rule_filter_id bigint, CONSTRAINT rule_filter_ip_range_pkey PRIMARY KEY (id),CONSTRAINT rule_filter_ip_range_id_fkey FOREIGN KEY (rule_filter_id) REFERENCES rule_filter (id) on delete cascade );
CREATE TABLE rulefilter_tag ( filter_id bigint NOT NULL, tag_id bigint NOT NULL, CONSTRAINT rulefilter_tag_pkey PRIMARY KEY (filter_id, tag_id), CONSTRAINT fk37454a4f3b6f702e FOREIGN KEY (filter_id) REFERENCES rule_filter (id) on delete cascade, CONSTRAINT fk37454a4f6b8d9d08 FOREIGN KEY (tag_id) REFERENCES tag (id) on delete cascade ); 
 create index rulefilter_tag_fid on rulefilter_tag(filter_id);
 create index rulefilter_tag_tid on rulefilter_tag(tag_id);
CREATE TABLE rule_filter_portrange (id bigint NOT NULL, rule_filter_id bigint, startIndex bigint NOT NULL, endIndex bigint NOT NULL, CONSTRAINT rulefilter_portrange_pkey PRIMARY KEY (id), CONSTRAINT FK_RULE_FILTER_PORTRANGE_RULE_FILTER FOREIGN KEY (rule_filter_id) REFERENCES rule_filter (id) on delete cascade ); 
 create index rule_filter_portrange_pid on rule_filter_portrange(rule_filter_id);
 create index rule_filter_portrange_fid on rule_filter_portrange(id);
CREATE TABLE rule_filter_protocol ( id bigint NOT NULL, rule_filter_id bigint, protocol bigint  NOT NULL, CONSTRAINT rulefilter_protocol_pkey PRIMARY KEY (id), CONSTRAINT FK_RULE_FILTER_PROTOCOL_RULE_FILTER FOREIGN KEY (rule_filter_id) REFERENCES rule_filter (id) on delete cascade ); 
 create index rule_filter_protocol_pid on rule_filter_protocol(id);
 create index rule_filter_protocol_fid on rule_filter_protocol(rule_filter_id);
CREATE TABLE rule_filter_ip_address_range ( id bigint NOT NULL, rule_filter_id bigint, start_range varchar(50)  NOT NULL,  end_range varchar(50)  NOT NULL, source varchar(50), CONSTRAINT rulefilter_ip_address_range_pkey PRIMARY KEY (id), CONSTRAINT FK_RULE_FILTER_IP_ADDRESS_RANGE_RULE_FILTER FOREIGN KEY (rule_filter_id) REFERENCES rule_filter (id) on delete cascade ); 
 create index rule_filter_ip_address_range_pid on rule_filter_ip_address_range(id);
 create index rule_filter_ip_address_rangel_fid on rule_filter_ip_address_range(rule_filter_id);
CREATE TABLE netflow_card_conf (id int8 primary key);
insert into netflow_card_conf values(0);
insert into netflow_card_conf values(1);
insert into netflow_card_conf values(2);
insert into netflow_card_conf values(3);
insert into netflow_card_conf values(4);
insert into netflow_card_conf values(5);

CREATE TABLE netflow_collector_conf (id int8 primary key, rate real);
insert into netflow_collector_conf values(0,0);
insert into netflow_collector_conf values(1,0);
insert into netflow_collector_conf values(2,0);
insert into netflow_collector_conf values(3,0);
insert into netflow_collector_conf values(4,0);
insert into netflow_collector_conf values(5,0);
insert into netflow_collector_conf values(6,0);
insert into netflow_collector_conf values(7,0);
insert into netflow_collector_conf values(8,0);
insert into netflow_collector_conf values(9,0);

CREATE TABLE rule_collector
(
  rule_id bigint NOT NULL,
  collector_id bigint NOT NULL,
  CONSTRAINT rule_collector_pkey PRIMARY KEY (rule_id, collector_id),
  CONSTRAINT FK_rule_collector__netflow_collector_conf FOREIGN KEY (collector_id)
      REFERENCES netflow_collector_conf (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT FK_rule_collector__rule FOREIGN KEY (rule_id)
      REFERENCES rule (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE netflow_global_conf (id int8 primary key) ;

CREATE TABLE system_alert (id int8 primary key,ts timestamp without time zone NOT NULL);
  create index system_alert_ts on system_alert(ts);
CREATE TABLE json_layout (id bigint, json_data varchar(8000), name varchar(256), layout_user varchar(256));

insert into stat_util values (0);

create table version_info(schema_version varchar(50));
insert into version_info values ('1.1.0');

CREATE TABLE netflow_vlan_map(id int8 primary key) ;
create sequence netflow_vlan_map_id_seq start with 100000;

alter table netflow_global_conf add column enable_vlan_maps boolean default false;

CREATE TABLE ssl_global_conf (id int8 primary key) ;
CREATE TABLE ssl_certificates (id int8 primary key) ;
CREATE TABLE stats_global_config (id int8 primary key) ;

CREATE TABLE ssl_port_map
(
  id bigint NOT NULL,
  enabled boolean,
  port_from integer,
  port_to integer,
  description character varying(255),
  CONSTRAINT ssl_port_map_pkey PRIMARY KEY (id)
) ;

create sequence ssl_port_map_id_seq start with 10000;
CREATE OR REPLACE FUNCTION insert_ssl_port_map(port_from integer, port_to integer, description character varying(255)) RETURNS VOID AS $$ 
DECLARE
	rowsCount integer := 0;
BEGIN 
    EXECUTE format('select count(*) from ssl_port_map where port_from=%s and port_to=%s and description=%s', quote_literal(port_from), quote_literal(port_to), quote_literal(description)) INTO rowsCount;
    IF rowsCount = 0 THEN 
		insert into ssl_port_map values(nextval('ssl_port_map_id_seq'), true, port_from, port_to, description);
    END IF; 
END; 
$$ LANGUAGE 'plpgsql';

select insert_ssl_port_map(443, 80, 'http protocol over TLS/SSL');
select insert_ssl_port_map(261, 80, 'IIOP Name service over TLS/SSL');
select insert_ssl_port_map(448, 80, 'DDM-SSL');
select insert_ssl_port_map(465, 25, 'smtp protocol over TLS/SSL');
select insert_ssl_port_map(563, 119, 'nntp protocol over TLS/SSL');
select insert_ssl_port_map(614, 80, 'SSLshell');
select insert_ssl_port_map(636, 389, 'ldap protocol over TLS/SSL');
select insert_ssl_port_map(989, 20, 'ftp protocol, data, over TLS/SSL');
select insert_ssl_port_map(990, 21, 'ftp control over TLS/SSL');
select insert_ssl_port_map(992, 23, 'telnet protocol over TLS/SSL');
select insert_ssl_port_map(993, 143, 'imap4 protocol over TLS/SSL');
select insert_ssl_port_map(994, 194, 'irc protocol over TLS/SSL');
select insert_ssl_port_map(995, 110, 'pop3 protocol over TLS/SSL');


CREATE TABLE mpls_global_conf (id int8 primary key);
CREATE TABLE datamasking_global_conf (id int8 primary key) ;
CREATE TABLE datamasking_regex
(
  id bigint NOT NULL,
  name character varying(255),
  offset_begin integer,
  offset_end integer,
  regex character varying(255),
  readonly boolean,
  creditcard boolean,
  CONSTRAINT datamasking_regex_pkey PRIMARY KEY (id)
) ;
CREATE SEQUENCE datamasking_regex_id_seq MINVALUE 1 MAXVALUE 256 START 1 CYCLE;

CREATE OR REPLACE FUNCTION insert_regex_mask(name character varying(255), regex character varying(255), creditcard boolean) RETURNS VOID AS $$ 
DECLARE
	rowsCount integer := 0;
BEGIN 
    EXECUTE format('select count(*) from datamasking_regex where name=%s', quote_literal(name)) INTO rowsCount;
    IF rowsCount = 0 THEN 
	insert into datamasking_regex values(nextval('datamasking_regex_id_seq'), name, 0, 0, regex, true, creditcard);
    END IF; 
END; 

$$ LANGUAGE 'plpgsql';

select insert_regex_mask('Visa', '4[0-9]{15}', true);
select insert_regex_mask('Mastercard', '5[1-5][0-9]{14}', true);
select insert_regex_mask('Amex', '3[47][0-9]{13}', true);
select insert_regex_mask('DinersClub', '(30[0-5][0-9]{11})|(3[68][0-9]{12})', true);
select insert_regex_mask('Discover', '(6011[0-9]{12})|(65[0-9]{14})', true);
select insert_regex_mask('JCB', '(2131[0-9]{11})|(1800[0-9]{11})|(35[0-9]{14})', true);
select insert_regex_mask('Email', '[a-zA-Z0-9._]{2,20}@[a-zA-Z.]{2,20}\.[a-zA-z]{2,4}', false);
select insert_regex_mask('SSN', '[0-578][0-9]{2}[- ][0-9]{2}[- ][0-9]{4}', false);


CREATE TABLE datamasking_header(id int primary key);
CREATE SEQUENCE datamasking_header_id_seq MINVALUE 0 MAXVALUE 499 START 1 CYCLE;

CREATE TABLE datamasking_stat
(
  uuid bigint NOT NULL,
  rpt_count bigint,
  type integer,
  PRIMARY KEY (uuid, type)
);

CREATE TABLE datamasking_macrewrite_settings (id int8 primary key, enabled boolean) ;
CREATE TABLE datamasking_macrewrite
(
  id bigint NOT NULL,
  name character varying(255),
  regex character varying(255),
  field varchar(50),
  replacement varchar(50),
  
  CONSTRAINT ddatamasking_macrewrite_pkey PRIMARY KEY (id)
) ;
CREATE SEQUENCE datamasking_macrewrite_id_seq MINVALUE 0 MAXVALUE 499 START 1 CYCLE;

create sequence ssl_certificates_id_seq start with 1000;
create sequence ruledbo_id_seq start with 100000;
create sequence rule_filter_application_id_seq start with 100000;
create sequence rule_filter_geo_id_seq start with 100000;
create sequence rule_filter_portrange_id_seq start with 100000;
create sequence rule_filter_protocol_id_seq start with 100000;
create sequence rule_filter_ip_address_range_id_seq start with 100000;
create sequence flowactiondbo_id_seq start with 100000;
create sequence rulefilterdbo_id_seq start with 10000;
create sequence appactiondbo_id_seq start with 10000;
create sequence applicationdbo_id_seq start with 100000;
create sequence appsignaturedbo_id_seq start with 10000;
create sequence tagdbo_id_seq start with 10000;
create sequence matchactiondbo_id_seq start with 10000;
create sequence stat_match_id_seq start with 10000;
create sequence statinterfacedbo_id_seq start with 10000;
create sequence statapplicationdbo_id_seq start with 10000;
create sequence stat_pktstat_id_seq start with 10000;
create sequence statgeodbo_id_seq start with 10000;
create sequence statdevicedbo_id_seq start with 10000;
create sequence detectedapplicationdbo_id_seq start with 10000;
create sequence rule_filter_portdbo_id_seq start with 10000;
create index app_sig_id on app_signature(id);
create index app_action_id on app_action(id);
create index app_id on application(id);
create index dynapp_id on dynamic_application(id);

CREATE TABLE detected_application
(
  id bigint NOT NULL,
  device character varying(255),
  hash character varying(255) NOT NULL,
  last_update timestamp without time zone,
  port character varying(255),
  transport character varying(255),
  xml character varying(8000) NOT NULL,
  CONSTRAINT detected_application_pkey PRIMARY KEY (id)
) ;

create index detected_application_lastupdate on detected_application(last_update);
create index detected_application_hash on detected_application(hash);
  
create table stat_interface_20sec_inv (
  ts timestamp without time zone NOT NULL,
  total_rx_bytes bigint,
  total_rx_packets bigint,
  total_rx_errors bigint,
  l7_bytes bigint,
  l7_packets bigint,
  sessions bigint,
  rx_np_drop_bytes bigint,
  rx_np_err_bytes bigint,
  rx_np_drop_pkts bigint,
  rx_np_err_pkts bigint
);

create index stat_interface_20sec_inv_tx on stat_interface_20sec_inv(ts);

create table stat_interface_20min_inv (
  ts timestamp without time zone NOT NULL,
  total_rx_bytes bigint,
  total_rx_packets bigint,
  total_rx_errors bigint,
  l7_bytes bigint,
  l7_packets bigint,
  sessions bigint,
  rx_np_drop_bytes bigint,
  rx_np_err_bytes bigint,
  rx_np_drop_pkts bigint,
  rx_np_err_pkts bigint
);

create index stat_interface_20_min_ts on stat_interface_20min_inv(ts);

CREATE TABLE stat_geo_20sec_inv
(
  ts timestamp without time zone NOT NULL,
  c_cid bigint,
  c_rid bigint,
  s_cid bigint,
  s_rid bigint,
  rx_if integer,
  c_bytes bigint,
  conns bigint,
  c_pkts bigint,
  s_bytes bigint,
  s_pkts bigint
);
CREATE INDEX stat_geo_20sec_inv1 ON stat_geo_20sec_inv USING btree (ts, c_cid, c_rid);
CREATE INDEX stat_geo_20sec_inv2 ON stat_geo_20sec_inv USING btree (ts, s_cid, s_rid);
create index stat_geo_20sec_inv3 on stat_geo_20sec_inv(ts, s_cid, c_cid);

CREATE TABLE stat_geo_20min_inv
(
  ts timestamp without time zone NOT NULL,
  c_cid bigint,
  s_cid bigint,
  rx_if integer,
  c_bytes bigint,
  conns bigint,
  c_pkts bigint,
  s_bytes bigint,
  s_pkts bigint
);
CREATE INDEX stat_geo_20min_inv1 ON stat_geo_20min_inv USING btree (ts, c_cid);
CREATE INDEX stat_geo_20min_inv2 ON stat_geo_20min_inv USING btree (ts, s_cid);
create index stat_geo_20min_inv3 on stat_geo_20min_inv(ts, s_cid, c_cid);

CREATE TABLE stat_application_20sec_inv
(
  ts timestamp without time zone NOT NULL,
  c_cid bigint,
  c_rid bigint,
  s_cid bigint,
  s_rid bigint,
  app character varying(255),
  app_id bigint,
  type character varying(255),
  rx_if integer,
  c_bytes bigint,
  c_conns bigint,
  c_pkts bigint,
  s_bytes bigint,
  s_conns bigint,
  s_pkts bigint
);
CREATE INDEX stat_application_20sec_inv1 ON stat_application_20sec_inv USING btree (ts, app, type, c_cid, c_rid);
CREATE INDEX stat_application_20sec_inv2 ON stat_application_20sec_inv USING btree (ts, app_id, type, c_cid, c_rid);

CREATE TABLE stat_application_20min_inv
(
  ts timestamp without time zone NOT NULL,
  c_cid bigint,
  s_cid bigint,
  app character varying(255),
  app_id bigint,
  type character varying(255),
  rx_if integer,
  c_bytes bigint,
  c_conns bigint,
  c_pkts bigint,
  s_bytes bigint,
  s_conns bigint,
  s_pkts bigint
);
CREATE INDEX stat_application_20min_inv1 ON stat_application_20min_inv USING btree (ts, app, type, c_cid);
CREATE INDEX stat_application_20min_inv2 ON stat_application_20min_inv USING btree (ts, app_id, type, c_cid);

CREATE TABLE stat_device_20sec_inv
(
  ts timestamp without time zone NOT NULL,
  app varchar(255),
  device varchar(255),
  dev_ver varchar(255),
  br varchar(255),
  brver varchar(255),
  bytes bigint,
  flows bigint,
  pkts bigint
);
CREATE INDEX stat_device_20sec_inv1 ON stat_device_20sec_inv USING btree (ts, app, device, dev_ver);
CREATE INDEX stat_device_20sec_inv2 ON stat_device_20sec_inv USING btree (ts, app, br, brver);
CREATE INDEX stat_device_20sec_inv3 ON stat_device_20sec_inv USING btree (ts, device, dev_ver, br, brver);

CREATE TABLE stat_device_20min_inv
(
  ts timestamp without time zone NOT NULL,
  app varchar(255),
  device varchar(255),
  dev_ver varchar(255),
  br varchar(255),
  brver varchar(255),
  bytes bigint,
  flows bigint,
  pkts bigint
);
CREATE INDEX stat_device_20min_inv1 ON stat_device_20min_inv USING btree (ts, app, device, dev_ver);
CREATE INDEX stat_device_20min_inv2 ON stat_device_20min_inv USING btree (ts, app, br, brver);
CREATE INDEX stat_device_20min_inv3 ON stat_device_20min_inv USING btree (ts, device, dev_ver, br, brver);


CREATE TABLE stat_provider_20sec_inv
(
 	ts timestamp without time zone NOT NULL, 
	asn bigint,
	conns bigint, 
	pkts_rx bigint, 
	pkts_tx bigint, 
	bytes_rx bigint, 
	bytes_tx bigint
);
CREATE INDEX stat_provider_20sec_inv1 ON stat_provider_20sec_inv USING btree (ts, asn);

CREATE TABLE stat_provider_20min_inv
(
	ts timestamp without time zone NOT NULL, 
	asn bigint,
	conns bigint, 
	pkts_rx bigint, 
	pkts_tx bigint, 
	bytes_rx bigint, 
	bytes_tx bigint
);
CREATE INDEX stat_provider_20min_inv1 ON stat_provider_20min_inv USING btree (ts, asn);

CREATE TABLE stat_match_20sec_inv
(
  ts timestamp without time zone NOT NULL,
  c_cid bigint,
  c_rid bigint,
  c_ctid bigint,
  s_cid bigint,
  s_rid bigint,
  s_ctid bigint,
  rule_id bigint,
  device character varying(255),
  app character varying(512),
  rx_if integer,
  s_bytes bigint,
  c_bytes bigint,
  conns bigint,
  c_pkts bigint,
  s_pkts bigint
);
CREATE INDEX stat_match_20sec_inv1 ON stat_match_20sec_inv USING btree (ts, rule_id, c_cid, c_rid, c_ctid);
CREATE INDEX stat_match_20sec_inv2 ON stat_match_20sec_inv USING btree (ts, rule_id, rx_if, device, c_cid, c_rid, c_ctid);
CREATE INDEX stat_match_20sec_inv3 ON stat_match_20sec_inv USING btree (ts, rule_id, s_cid, s_rid, s_ctid);
CREATE INDEX stat_match_20sec_inv4 ON stat_match_20sec_inv USING btree (ts, rule_id, rx_if, device, s_cid, s_rid, s_ctid);
create index stat_match_20sec_inv5 on stat_match_20sec_inv (ts, rule_id, s_cid, c_cid);

CREATE TABLE stat_match_20min_inv
(
  ts timestamp without time zone NOT NULL,
  c_cid bigint,
  s_cid bigint,
  rule_id bigint,
  device character varying(255),
  rx_if integer,
  app character varying(512),
  s_bytes bigint,
  c_bytes bigint,
  conns bigint,
  c_pkts bigint,
  s_pkts bigint
);
CREATE INDEX stat_match_20min_inv1 ON stat_match_20min_inv USING btree (ts, rule_id, c_cid, s_cid);
CREATE INDEX stat_match_20min_inv2 ON stat_match_20min_inv USING btree (ts, rule_id, rx_if, device, c_cid, s_cid);
CREATE INDEX stat_match_20min_inv3 ON stat_match_20min_inv USING btree (ts, rule_id, s_cid, c_cid);
CREATE INDEX stat_match_20min_inv4 ON stat_match_20min_inv USING btree (ts, rule_id, rx_if, device, s_cid, c_cid);
create index stat_match_20min_inv5 on stat_match_20min_inv (ts, rule_id, s_cid, c_cid);

CREATE TABLE stat_match_detail_agg
(
  ts timestamp without time zone NOT NULL,
  rule_id bigint,
  c_cid bigint,
  c_rid bigint,
  c_ctid bigint,
  s_cid bigint,
  s_rid bigint,
  s_ctid bigint,
  dst_ip character varying(255),
  src_ip character varying(255),
  device character varying(255),
  rx_if bigint,
  s_bytes bigint,
  c_bytes bigint,
  conns bigint,
  c_pkts bigint,
  s_pkts bigint,
  latency bigint,
  latency_samples bigint,
  app character varying(512)
);
CREATE INDEX stat_match_detail_agg1 ON stat_match_detail_agg USING btree (ts, rule_id, device, rx_if);
CREATE INDEX stat_match_detail_agg2 ON stat_match_detail_agg USING btree (ts, rule_id, c_cid, c_rid, c_ctid, s_cid, s_rid, s_ctid, dst_ip, src_ip, device, app, rx_if);
CREATE INDEX stat_match_detail_agg3 ON stat_match_detail_agg USING btree (ts, rule_id, dst_ip, src_ip);
create index stat_match_detail_agg4 ON stat_match_detail_agg USING btree (ts, rule_id, device, c_cid, s_cid, dst_ip, src_ip, app);
create index stat_match_detail_agg5 ON stat_match_detail_agg USING btree (ts);

CREATE TABLE stat_match_detail_agg_day
(
  ts timestamp without time zone NOT NULL,
  rule_id bigint,
  c_cid bigint,
  c_rid bigint,
  c_ctid bigint,
  s_cid bigint,
  s_rid bigint,
  s_ctid bigint,
  dst_ip character varying(255),
  src_ip character varying(255),
  device character varying(255),
  rx_if bigint,
  s_bytes bigint,
  c_bytes bigint,
  conns bigint,
  c_pkts bigint,
  s_pkts bigint,
  latency bigint,
  latency_samples bigint,
  app character varying(512)
);
CREATE INDEX stat_match_detail_agg_day1 ON stat_match_detail_agg_day USING btree (ts, rule_id, device, rx_if);
CREATE INDEX stat_match_detail_agg_day2 ON stat_match_detail_agg_day USING btree (ts, rule_id, c_cid, c_rid, c_ctid, s_cid, s_rid, s_ctid, dst_ip, src_ip, device, app, rx_if);
CREATE INDEX stat_match_detail_agg_day3 ON stat_match_detail_agg_day USING btree (ts, rule_id, dst_ip, src_ip);
create index stat_match_detail_agg_day4 ON stat_match_detail_agg_day USING btree (ts, rule_id, device, c_cid, s_cid, dst_ip, src_ip, app);
create index stat_match_detail_agg_day5 ON stat_match_detail_agg_day USING btree (ts);

CREATE TABLE stat_global
(
  id bigint NOT NULL,
  npoffset bigint,
  sslactiveflows bigint,
  sslalerts bigint,
  sslcertificateidents bigint,
  sslciphersunsuported bigint,
  ssldecryptedbytes bigint,
  ssldecryptedconns bigint,
  ssldecryptedpackets bigint,
  sslencryptedbytes bigint,
  sslencryptedconns bigint,
  sslencryptedpackets bigint,
  sslkeyfails bigint,
  sslkeymatches bigint,
  sslkeysunmatched bigint,
  sslresumefails bigint,
  sslresumedconns bigint,
  sslstandardconns bigint,
  dedupcoll bigint,
  dedupdropped bigint,
  deduppassed bigint,
  ts timestamp without time zone NOT NULL,
  CONSTRAINT stat_global_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
create index stat_global_ts on stat_global(ts);

CREATE TABLE IF NOT EXISTS nto_global_conf
(
  id bigint NOT NULL,
  debug_mode boolean,
  dod_enabled boolean,
  fips_enabled boolean,
  log_tls_handshake boolean,
  login_sess_timeout bigint,
  tls_enabled boolean,
  web_api_addr character varying(255),
  web_api_port integer,
  web_api_timeout bigint,
  CONSTRAINT nto_global_conf_pkey PRIMARY KEY (id)
);

create table if not exists stat_global_netflow 
(
	id int8 primary key,  
	ts timestamp without time zone NOT NULL, 
	np int8,
	dropped_flows bigint, 
	dropped_flow_packets bigint, 
	dropped_flow_bytes bigint
);

DO
$$
BEGIN
	create index id_stat_global_netflow_ts_id on stat_global_netflow(ts, id);
EXCEPTION WHEN duplicate_table THEN
        RAISE NOTICE 'id_stat_global_netflow_ts_id index already exists, skipping ... ';
END
$$ LANGUAGE plpgsql;

DO
$$
BEGIN
	create index id_stat_global_netflow_np on stat_global_netflow(np);
EXCEPTION WHEN duplicate_table THEN
        RAISE NOTICE 'id_stat_global_netflow_np index already exists, skipping';
END
$$ LANGUAGE plpgsql;


CREATE TABLE if not exists stat_global_netflow_20sec_inv
(
 	ts timestamp without time zone NOT NULL, 
	np int8,
	dropped_flows bigint, 
	dropped_flow_packets bigint, 
	dropped_flow_bytes bigint
);

DO
$$
BEGIN
	create index id_stat_global_netflow_20sec_inv_np ON stat_global_netflow_20sec_inv USING btree (np);
EXCEPTION WHEN duplicate_table THEN
        RAISE NOTICE 'id_stat_global_netflow_20sec_inv_np index already exists, skipping';
END
$$ LANGUAGE plpgsql;

CREATE TABLE if not exists stat_global_netflow_20min_inv
(
	ts timestamp without time zone NOT NULL, 
	np int8,
	dropped_flows bigint, 
	dropped_flow_packets bigint, 
	dropped_flow_bytes bigint
);

DO
$$
BEGIN
	CREATE INDEX id_stat_global_netflow_20min_inv_np ON stat_global_netflow_20min_inv USING btree (np);
EXCEPTION WHEN duplicate_table THEN
        RAISE NOTICE 'id_stat_global_netflow_20min_inv_np index already exists, skipping';
END
$$ LANGUAGE plpgsql;


DO
$$
BEGIN

if ((
   SELECT count(1)
   FROM   information_schema.tables 
   WHERE  table_schema = 'public'
   AND    table_name in ('app_seq', 'application', 'dynamic_application', 'services_application')
) = 4) then

	if exists (select (1) from app_seq where key='app_seq') then
		UPDATE app_seq SET next = (
		SELECT GREATEST (
				(SELECT coalesce(max(id),0) FROM application),
				(SELECT coalesce(max(id),0) FROM dynamic_application), 
				(SELECT coalesce(max(id),0) FROM services_application)
			) + 1
		)
		WHERE key='app_seq';
	else 
		insert into app_seq (key, next) values ('app_seq', 1);
	end if;
end if;

END
$$ LANGUAGE plpgsql;

DO
$$
BEGIN
	IF NOT EXISTS (SELECT 1 FROM pg_class c WHERE c.relkind = 'S' AND relname = 'appactiondbo_id_seq') THEN
		create sequence appactiondbo_id_seq start with 10000;
	END IF;
	PERFORM setval('appactiondbo_id_seq',(select coalesce(max(id),0) + 1 from app_action));

END
$$ LANGUAGE plpgsql; 

DO
$$
BEGIN
	IF NOT EXISTS (SELECT 1 FROM pg_class c WHERE c.relkind = 'S' AND relname = 'appsignaturedbo_id_seq') THEN
		create sequence appsignaturedbo_id_seq start with 10000;
	END IF;
	PERFORM setval('appsignaturedbo_id_seq',(select coalesce(max(id),0) + 1 from app_signature));

END
$$ LANGUAGE plpgsql; 

DO
$$
BEGIN
	IF NOT EXISTS (SELECT 1 FROM pg_class c WHERE c.relkind = 'S' AND relname = 'tag_id_seq') THEN
		create sequence tag_id_seq start with 10000;
	END IF;
	PERFORM setval('tag_id_seq',(select coalesce(max(id),0) + 1 from tag));

END
$$ LANGUAGE plpgsql; 



CREATE OR REPLACE FUNCTION insert_deny_filter() RETURNS VOID AS $$ 
DECLARE
	rowsCount integer := 0;
BEGIN 
    EXECUTE format('select count(*) from rule where id=1000') INTO rowsCount;
    IF rowsCount = 0 THEN 
		insert into rule(id, name, is_active, is_explore, deny) values(1000, 'Unmatched traffic', true, false, false);
	ELSE
		EXECUTE format('select count(*) from rule where id=1000 and is_active=false') INTO rowsCount;
		IF rowsCount <> 0 THEN
			delete from rule where id=1000 and is_active=false;
			insert into rule(id, name, is_active, is_explore, deny) values(1000, 'Unmatched traffic', true, false, false);
		END IF;
    END IF; 
END; 
$$ LANGUAGE 'plpgsql';

select insert_deny_filter();

CREATE OR REPLACE FUNCTION rebuild_rpt_buckets_by_hr() RETURNS int AS $$ 
BEGIN 
	IF (select count(*) from rpt_buckets_by_hr) = 0 OR
	   (select now() + cast ('5 minutes' as interval)) > (select max(end_time) from rpt_buckets_by_hr) OR 
	   (select now()) <= (select min(end_time) from rpt_buckets_by_hr) 
	THEN  
		LOCK TABLE rpt_buckets_by_hr IN EXCLUSIVE MODE; 
		truncate table rpt_buckets_by_hr; 
		insert into rpt_buckets_by_hr   
			select (select cast (current_date - cast('7 days' as interval) as date) + cast ( n || ' seconds' as interval)) start_time,  
			(select cast (current_date as date) - cast('7 days' as interval) + cast ((n+20) || ' seconds' as interval)) end_time from generate_series(0, (15*24*3*60*20-20), 20) n  ;  
		return 1; 
	END IF; 
	return 0; 
	exception when others then  
		create table rpt_buckets_by_hr as  
			select (select cast (current_date - cast('7 days' as interval) as date) + cast ( n || ' seconds' as interval)) start_time,  
			(select cast (current_date as date) - cast('7 days' as interval) + cast ((n+20) || ' seconds' as interval)) end_time from generate_series(0, (15*24*3*60*20-20), 20) n ;  
			create index b1_hr on rpt_buckets_by_hr(start_time, end_time); 
			create index b2_hr on rpt_buckets_by_hr(end_time, start_time); 
	return 2; 
END; 
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION rebuild_rpt_buckets_by_day() RETURNS int AS $$ 
BEGIN 
	IF (select count(*) from rpt_buckets_by_day) = 0 OR
	   (select now() + cast ('20 minutes' as interval)) > (select max(end_time) from rpt_buckets_by_day) OR
	   (select now()) <= (select min(end_time) from rpt_buckets_by_hr) 
	THEN  
		LOCK TABLE rpt_buckets_by_day IN EXCLUSIVE MODE; 
		truncate table rpt_buckets_by_day; 
		insert into rpt_buckets_by_day   
			select cast (current_date - cast('30 days' as interval) as date) + cast ( n || ' seconds' as interval) start_time,   
(select cast (current_date - cast('30 days' as interval) as date) + cast ((n+1200) || ' seconds' as interval)) end_time from generate_series(0, (32*24*3*60*20-20), 1200) n;   
		return 1; 
	END IF; 
	return 0; 
	exception when others then  
		create table rpt_buckets_by_day as  
			select cast (current_date - cast('30 days' as interval) as date) + cast ( n || ' seconds' as interval) start_time,   
			(select cast (current_date - cast('30 days' as interval) as date) + cast ((n+1200) || ' seconds' as interval)) end_time from generate_series(0, (32*24*3*60*20-20), 1200) n;  
			create index b1_day on rpt_buckets_by_day(start_time, end_time); 
			create index b2_day on rpt_buckets_by_day(end_time, start_time); 
	return 2; 
END; 
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION aggregate_stat_global() RETURNS int AS $$
DECLARE

BEGIN
	delete from stat_global where ts < now() - cast('1 DAY' as interval);
	return 0;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION clear_data_from_rpt_buckets_by_day() RETURNS int AS $$
BEGIN
	IF EXISTS (SELECT 1 FROM pg_class WHERE relname = 'rpt_buckets_by_day')
	THEN
		delete from rpt_buckets_by_day;
	END IF;
	return 0;
END;
$$ LANGUAGE plpgsql;

select clear_data_from_rpt_buckets_by_day();
select rebuild_rpt_buckets_by_day();
select rebuild_rpt_buckets_by_hr();

WITH new_priority AS (
   SELECT id,
          ROW_NUMBER() OVER (ORDER BY id) AS priority
   FROM rule WHERE (is_active='true' AND is_explore='false' AND id <> 1000)
)
UPDATE rule
  SET priority = new_priority.priority
FROM new_priority
WHERE new_priority.id = rule.id;

UPDATE rule SET priority='1000' where id='1000';

create table asnum_map 
(
	as_number 	int8 not null primary key, 
	as_name		varchar(500)
);
create index asnum_map_name on asnum_map(as_name);

DO
$$
BEGIN
	COPY asnum_map(as_number, as_name)
	FROM '/usr/opt/atie/control/ati_asnum_map.csv' DELIMITER ',' CSV HEADER;
END
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION delete_all_stats()
  RETURNS integer AS
$BODY$
declare
        tname character varying(255);
        int_test        INTEGER;
        input_refc      refcursor;
BEGIN

        FOR tname IN
           (select table_name from information_schema.tables
                where table_catalog = 'atie'
                and table_name like 'stat_%'
                and table_name <> 'stats_global_config')
        LOOP
                BEGIN
                        execute 'truncate ' || quote_ident(tname);

                        raise notice 'Truncated: %', tname;
                EXCEPTION WHEN OTHERS THEN
                        --continue
                END;
        END LOOP;

        delete from datamasking_stat;

        return 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;




----------- --------------- ----------------------- ------------------- 
----------- Autogenerated code. Do not modify it manually.       --------
----------- Project location: //import/atie/<release_branch>/dbhelper/ --
----------- Place the custom code behind it                       --------
----------- Start: Aggregate Stat Functions                       --------
----------- ----------------------- --------------- -------------- -------


-- Function: aggregate_stat_global_netflow() 
-- DROP FUNCTION aggregate_stat_global_netflow();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_global_netflow() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_global_netflow_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_global_netflow where ts <= maxBucketTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_global_netflow_20sec_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	np         bigint  , 
 	dropped_flows bigint ,
	dropped_flow_packets bigint ,
	dropped_flow_bytes bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_global_netflow_20sec_inv_tmp on stat_global_netflow_20sec_inv_tmp(	ts , 
	np); 
create index idx2_stat_global_netflow_20sec_inv_tmp on stat_global_netflow_20sec_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_global_netflow_20sec_inv_tmp 
select start_time,
	np , 
	coalesce(sum(dropped_flows),0) as dropped_flows,
	coalesce(sum(dropped_flow_packets),0) as dropped_flow_packets,
	coalesce(sum(dropped_flow_bytes),0) as dropped_flow_bytes 
from(
     select start_time, 
	np , 
            	coalesce(sum(dropped_flows),0) as dropped_flows,
	coalesce(sum(dropped_flow_packets),0) as dropped_flow_packets,
	coalesce(sum(dropped_flow_bytes),0) as dropped_flow_bytes 
     from rpt_buckets_by_hr b 
     right join
        (select ts, 
	np , 
 	dropped_flows,
	dropped_flow_packets,
	dropped_flow_bytes 
            from stat_global_netflow
         where ts >= maxMatchTime and ts < maxBucketTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	np) combined 
where (dropped_flows+dropped_flow_packets+dropped_flow_bytes) > 0 
group by start_time , 
	np; 

UPDATE stat_global_netflow_20sec_inv 
SET 	dropped_flows = coalesce(stat_global_netflow_20sec_inv.dropped_flows, 0) +temp.dropped_flows ,
	dropped_flow_packets = coalesce(stat_global_netflow_20sec_inv.dropped_flow_packets, 0) +temp.dropped_flow_packets ,
	dropped_flow_bytes = coalesce(stat_global_netflow_20sec_inv.dropped_flow_bytes, 0) +temp.dropped_flow_bytes  
FROM stat_global_netflow_20sec_inv_tmp temp 
WHERE  	stat_global_netflow_20sec_inv.ts = temp.ts  AND 
	stat_global_netflow_20sec_inv.np = temp.np ; 

INSERT INTO stat_global_netflow_20sec_inv 
(	 ts , 
	np , 
 	dropped_flows,
	dropped_flow_packets,
	dropped_flow_bytes ) 
 SELECT temp.ts, 
 	 temp.np  ,
 	 temp.dropped_flows  ,
 	 temp.dropped_flow_packets  ,
 	 temp.dropped_flow_bytes   
	FROM stat_global_netflow_20sec_inv_tmp temp 
	LEFT OUTER JOIN stat_global_netflow_20sec_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.np = temp.np ) 
		 WHERE agg.ts IS NULL; 

 delete from stat_global_netflow where id <= maxId; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_global_netflow_hour() 
-- DROP FUNCTION delete_old_values_stat_global_netflow_hour();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_global_netflow_hour() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_global_netflow_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_global_netflow where ts <= maxBucketTime; 



 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_global_netflow
	where ts < (nowTime - cast('10 minutes' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: aggregate_stat_global_netflow_day() 
-- DROP FUNCTION aggregate_stat_global_netflow_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_global_netflow_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_global_netflow_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_global_netflow_20min_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	np         bigint  , 
 	dropped_flows bigint ,
	dropped_flow_packets bigint ,
	dropped_flow_bytes bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_global_netflow_20min_inv_tmp on stat_global_netflow_20min_inv_tmp(	ts , 
	np); 
create index idx2_stat_global_netflow_20min_inv_tmp on stat_global_netflow_20min_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_global_netflow_20min_inv_tmp 
select start_time,
	np , 
	coalesce(sum(dropped_flows),0) as dropped_flows,
	coalesce(sum(dropped_flow_packets),0) as dropped_flow_packets,
	coalesce(sum(dropped_flow_bytes),0) as dropped_flow_bytes 
from(
     select start_time, 
	np , 
            	coalesce(sum(dropped_flows),0) as dropped_flows,
	coalesce(sum(dropped_flow_packets),0) as dropped_flow_packets,
	coalesce(sum(dropped_flow_bytes),0) as dropped_flow_bytes 
     from rpt_buckets_by_day b 
     right join
        (select ts, 
	np , 
 	dropped_flows,
	dropped_flow_packets,
	dropped_flow_bytes 
            from stat_global_netflow_20sec_inv
         where ts >= maxMatchTime and ts < nowTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	np) combined 
 
group by start_time , 
	np; 

UPDATE stat_global_netflow_20min_inv 
SET 	ts = temp.ts,
	np = temp.np ,
	dropped_flows = temp.dropped_flows ,
	dropped_flow_packets = temp.dropped_flow_packets ,
	dropped_flow_bytes = temp.dropped_flow_bytes  
FROM stat_global_netflow_20min_inv_tmp temp 
WHERE  	stat_global_netflow_20min_inv.ts = temp.ts  AND 
	stat_global_netflow_20min_inv.np = temp.np ; 

INSERT INTO stat_global_netflow_20min_inv 
(	 ts , 
	np , 
 	dropped_flows,
	dropped_flow_packets,
	dropped_flow_bytes ) 
 SELECT temp.ts, 
 	 temp.np  ,
 	 temp.dropped_flows  ,
 	 temp.dropped_flow_packets  ,
 	 temp.dropped_flow_bytes   
	FROM stat_global_netflow_20min_inv_tmp temp 
	LEFT OUTER JOIN stat_global_netflow_20min_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.np = temp.np ) 
		 WHERE agg.ts IS NULL; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_global_netflow_day() 
-- DROP FUNCTION delete_old_values_stat_global_netflow_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_global_netflow_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxTimeFor20SecTables timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_global_netflow_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 


select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxTimeFor20SecTables; 

 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_global_netflow_20sec_inv
	where ts < maxTimeFor20SecTables;

delete from stat_global_netflow_20min_inv
	where ts < (nowTime - cast('1 week' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;
-- Function: aggregate_stat_agg_app() 
-- DROP FUNCTION aggregate_stat_agg_app();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_agg_app() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_agg_app_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_agg_app where ts <= maxBucketTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_agg_app_20sec_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	app        character varying(255)  , 
 	bytes      bigint ,
	pkts       bigint ,
	flows      bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_agg_app_20sec_inv_tmp on stat_agg_app_20sec_inv_tmp(	ts , 
	app); 
create index idx2_stat_agg_app_20sec_inv_tmp on stat_agg_app_20sec_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_agg_app_20sec_inv_tmp 
select start_time,
	app , 
	coalesce(sum(bytes),0) as bytes,
	coalesce(sum(pkts),0) as pkts,
	coalesce(sum(flows),0) as flows 
from(
     select start_time, 
	app , 
            	coalesce(sum(bytes),0) as bytes,
	coalesce(sum(pkts),0) as pkts,
	coalesce(sum(flows),0) as flows 
     from rpt_buckets_by_hr b 
     right join
        (select ts, 
	app , 
 	bytes,
	pkts,
	flows 
            from stat_agg_app
         where ts >= maxMatchTime and ts < maxBucketTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	app) combined 
where app is not null 
group by start_time , 
	app; 

UPDATE stat_agg_app_20sec_inv 
SET 	bytes = coalesce(stat_agg_app_20sec_inv.bytes, 0) +temp.bytes ,
	pkts = coalesce(stat_agg_app_20sec_inv.pkts, 0) +temp.pkts ,
	flows = coalesce(stat_agg_app_20sec_inv.flows, 0) +temp.flows  
FROM stat_agg_app_20sec_inv_tmp temp 
WHERE  	stat_agg_app_20sec_inv.ts = temp.ts  AND 
	stat_agg_app_20sec_inv.app = temp.app ; 

INSERT INTO stat_agg_app_20sec_inv 
(	 ts , 
	app , 
 	bytes,
	pkts,
	flows ) 
 SELECT temp.ts, 
 	 temp.app  ,
 	 temp.bytes  ,
 	 temp.pkts  ,
 	 temp.flows   
	FROM stat_agg_app_20sec_inv_tmp temp 
	LEFT OUTER JOIN stat_agg_app_20sec_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.app = temp.app ) 
		 WHERE agg.ts IS NULL; 

 delete from stat_agg_app where id <= maxId; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_agg_app_hour() 
-- DROP FUNCTION delete_old_values_stat_agg_app_hour();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_agg_app_hour() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_agg_app_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_agg_app where ts <= maxBucketTime; 



 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_agg_app
	where ts < (nowTime - cast('10 minutes' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: aggregate_stat_agg_app_day() 
-- DROP FUNCTION aggregate_stat_agg_app_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_agg_app_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_agg_app_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_agg_app_20min_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	app        character varying(255)  , 
 	bytes      bigint ,
	pkts       bigint ,
	flows      bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_agg_app_20min_inv_tmp on stat_agg_app_20min_inv_tmp(	ts , 
	app); 
create index idx2_stat_agg_app_20min_inv_tmp on stat_agg_app_20min_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_agg_app_20min_inv_tmp 
select start_time,
	app , 
	coalesce(sum(bytes),0) as bytes,
	coalesce(sum(pkts),0) as pkts,
	coalesce(sum(flows),0) as flows 
from(
     select start_time, 
	app , 
            	coalesce(sum(bytes),0) as bytes,
	coalesce(sum(pkts),0) as pkts,
	coalesce(sum(flows),0) as flows 
     from rpt_buckets_by_day b 
     right join
        (select ts, 
	app , 
 	bytes,
	pkts,
	flows 
            from stat_agg_app_20sec_inv
         where ts >= maxMatchTime and ts < nowTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	app) combined 
 
group by start_time , 
	app; 

UPDATE stat_agg_app_20min_inv 
SET 	ts = temp.ts,
	app = temp.app ,
	bytes = temp.bytes ,
	pkts = temp.pkts ,
	flows = temp.flows  
FROM stat_agg_app_20min_inv_tmp temp 
WHERE  	stat_agg_app_20min_inv.ts = temp.ts  AND 
	stat_agg_app_20min_inv.app = temp.app ; 

INSERT INTO stat_agg_app_20min_inv 
(	 ts , 
	app , 
 	bytes,
	pkts,
	flows ) 
 SELECT temp.ts, 
 	 temp.app  ,
 	 temp.bytes  ,
 	 temp.pkts  ,
 	 temp.flows   
	FROM stat_agg_app_20min_inv_tmp temp 
	LEFT OUTER JOIN stat_agg_app_20min_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.app = temp.app ) 
		 WHERE agg.ts IS NULL; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_agg_app_day() 
-- DROP FUNCTION delete_old_values_stat_agg_app_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_agg_app_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxTimeFor20SecTables timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_agg_app_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 


select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxTimeFor20SecTables; 

 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_agg_app_20sec_inv
	where ts < maxTimeFor20SecTables;

delete from stat_agg_app_20min_inv
	where ts < (nowTime - cast('1 week' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;
-- Function: aggregate_stat_device() 
-- DROP FUNCTION aggregate_stat_device();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_device() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_device_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_device where ts <= maxBucketTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_device_20sec_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	app        character varying(255) ,
	device     character varying(255) ,
	dev_ver    character varying(255) ,
	br         character varying(255) ,
	brver      character varying(255)  , 
 	bytes      bigint ,
	pkts       bigint ,
	flows      bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_device_20sec_inv_tmp on stat_device_20sec_inv_tmp(	ts , 
	app,
	device,
	dev_ver,
	br,
	brver); 
create index idx2_stat_device_20sec_inv_tmp on stat_device_20sec_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 
INSERT INTO stat_device_20sec_inv_tmp
select start_time, 

	app,
	device,
	dev_ver,
	br,
	brver, 
	coalesce(sum(bytes),0) as bytes,
	coalesce(sum(pkts),0) as pkts,
	coalesce(sum(flows),0) as flows 
		from (
 ---------------------------------------------------------------------- 
 --  This part is customized 											 
 --  '-' is used in Java code and it is replaced with UNKNOWN test.     
  --  Also bytes = bytes_tx + bytes_rx  and  pkts = pkts_tx+pkts_rx      
 ---------------------------------------------------------------------- 
		select start_time, app, coalesce(device,'-') as device,coalesce(dev_ver,'-') as dev_ver, coalesce(br,'-') as br, coalesce(brver,'-') as brver, 
              coalesce(round(sum(bytes),0),0) as bytes, coalesce(round(sum(flows),0),0) as flows, coalesce(round(sum(pkts),0),0) as pkts
		from rpt_buckets_by_hr b right join 
		(select ts, app, coalesce(device,'-') as device, coalesce(dev_ver,'-') as dev_ver, coalesce(br,'-') as br, coalesce(brver,'-') as brver, 
               bytes_tx + bytes_rx as bytes, flows, pkts_tx+pkts_rx as pkts from stat_device
		  where ts >= maxMatchTime
		  and ts < maxBucketTime) si
		 on b.start_time < si.ts and b.end_time >= si.ts
		where b.start_time >= maxMatchTime and b.end_time < nowTime
		group by start_time, app,coalesce(device,'-'),coalesce(dev_ver,'-'), coalesce(br,'-'), coalesce(brver,'-')
		) tbl
		where (bytes+flows+pkts) > 0
		group by start_time, 
	app,
	device,
	dev_ver,
	br,
	brver; 
UPDATE stat_device_20sec_inv 
SET 	bytes = coalesce(stat_device_20sec_inv.bytes, 0) +temp.bytes ,
	pkts = coalesce(stat_device_20sec_inv.pkts, 0) +temp.pkts ,
	flows = coalesce(stat_device_20sec_inv.flows, 0) +temp.flows  
FROM stat_device_20sec_inv_tmp temp 
WHERE  	stat_device_20sec_inv.ts = temp.ts  AND 
	stat_device_20sec_inv.app = temp.app AND 
	stat_device_20sec_inv.device = temp.device AND 
	stat_device_20sec_inv.dev_ver = temp.dev_ver AND 
	stat_device_20sec_inv.br = temp.br AND 
	stat_device_20sec_inv.brver = temp.brver ; 

INSERT INTO stat_device_20sec_inv 
(	 ts , 
	app,
	device,
	dev_ver,
	br,
	brver , 
 	bytes,
	pkts,
	flows ) 
 SELECT temp.ts, 
 	 temp.app  ,
 	 temp.device  ,
 	 temp.dev_ver  ,
 	 temp.br  ,
 	 temp.brver  ,
 	 temp.bytes  ,
 	 temp.pkts  ,
 	 temp.flows   
	FROM stat_device_20sec_inv_tmp temp 
	LEFT OUTER JOIN stat_device_20sec_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.app = temp.app  AND 	 agg.device = temp.device  AND 	 agg.dev_ver = temp.dev_ver  AND 	 agg.br = temp.br  AND 	 agg.brver = temp.brver ) 
		 WHERE agg.ts IS NULL; 

 delete from stat_device where id <= maxId; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_device_hour() 
-- DROP FUNCTION delete_old_values_stat_device_hour();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_device_hour() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_device_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_device where ts <= maxBucketTime; 



 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_device
	where ts < (nowTime - cast('10 minutes' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: aggregate_stat_device_day() 
-- DROP FUNCTION aggregate_stat_device_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_device_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_device_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_device_20min_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	app        character varying(255) ,
	device     character varying(255) ,
	dev_ver    character varying(255) ,
	br         character varying(255) ,
	brver      character varying(255)  , 
 	bytes      bigint ,
	pkts       bigint ,
	flows      bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_device_20min_inv_tmp on stat_device_20min_inv_tmp(	ts , 
	app,
	device,
	dev_ver,
	br,
	brver); 
create index idx2_stat_device_20min_inv_tmp on stat_device_20min_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_device_20min_inv_tmp 
select start_time,
	app,
	device,
	dev_ver,
	br,
	brver , 
	coalesce(sum(bytes),0) as bytes,
	coalesce(sum(pkts),0) as pkts,
	coalesce(sum(flows),0) as flows 
from(
     select start_time, 
	app,
	device,
	dev_ver,
	br,
	brver , 
            	coalesce(sum(bytes),0) as bytes,
	coalesce(sum(pkts),0) as pkts,
	coalesce(sum(flows),0) as flows 
     from rpt_buckets_by_day b 
     right join
        (select ts, 
	app,
	device,
	dev_ver,
	br,
	brver , 
 	bytes,
	pkts,
	flows 
            from stat_device_20sec_inv
         where ts >= maxMatchTime and ts < nowTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	app,
	device,
	dev_ver,
	br,
	brver) combined 
 
group by start_time , 
	app,
	device,
	dev_ver,
	br,
	brver; 

UPDATE stat_device_20min_inv 
SET 	ts = temp.ts,
	app = temp.app ,
	device = temp.device ,
	dev_ver = temp.dev_ver ,
	br = temp.br ,
	brver = temp.brver ,
	bytes = temp.bytes ,
	pkts = temp.pkts ,
	flows = temp.flows  
FROM stat_device_20min_inv_tmp temp 
WHERE  	stat_device_20min_inv.ts = temp.ts  AND 
	stat_device_20min_inv.app = temp.app AND 
	stat_device_20min_inv.device = temp.device AND 
	stat_device_20min_inv.dev_ver = temp.dev_ver AND 
	stat_device_20min_inv.br = temp.br AND 
	stat_device_20min_inv.brver = temp.brver ; 

INSERT INTO stat_device_20min_inv 
(	 ts , 
	app,
	device,
	dev_ver,
	br,
	brver , 
 	bytes,
	pkts,
	flows ) 
 SELECT temp.ts, 
 	 temp.app  ,
 	 temp.device  ,
 	 temp.dev_ver  ,
 	 temp.br  ,
 	 temp.brver  ,
 	 temp.bytes  ,
 	 temp.pkts  ,
 	 temp.flows   
	FROM stat_device_20min_inv_tmp temp 
	LEFT OUTER JOIN stat_device_20min_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.app = temp.app  AND 	 agg.device = temp.device  AND 	 agg.dev_ver = temp.dev_ver  AND 	 agg.br = temp.br  AND 	 agg.brver = temp.brver ) 
		 WHERE agg.ts IS NULL; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_device_day() 
-- DROP FUNCTION delete_old_values_stat_device_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_device_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxTimeFor20SecTables timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_device_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 


select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxTimeFor20SecTables; 

 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_device_20sec_inv
	where ts < maxTimeFor20SecTables;

delete from stat_device_20min_inv
	where ts < (nowTime - cast('1 week' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;
-- Function: aggregate_stat_agg_match() 
-- DROP FUNCTION aggregate_stat_agg_match();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_agg_match() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_agg_match_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_agg_match where ts <= maxBucketTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_agg_match_20sec_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	rule_id    bigint  , 
 	bytes      bigint ,
	pkts       bigint ,
	flows      bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_agg_match_20sec_inv_tmp on stat_agg_match_20sec_inv_tmp(	ts , 
	rule_id); 
create index idx2_stat_agg_match_20sec_inv_tmp on stat_agg_match_20sec_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_agg_match_20sec_inv_tmp 
select start_time,
	rule_id , 
	coalesce(sum(bytes),0) as bytes,
	coalesce(sum(pkts),0) as pkts,
	coalesce(sum(flows),0) as flows 
from(
     select start_time, 
	rule_id , 
            	coalesce(sum(bytes),0) as bytes,
	coalesce(sum(pkts),0) as pkts,
	coalesce(sum(flows),0) as flows 
     from rpt_buckets_by_hr b 
     right join
        (select ts, 
	rule_id , 
 	bytes,
	pkts,
	flows 
            from stat_agg_match
         where ts >= maxMatchTime and ts < maxBucketTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	rule_id) combined 
where rule_id is not null 
group by start_time , 
	rule_id; 

UPDATE stat_agg_match_20sec_inv 
SET 	bytes = coalesce(stat_agg_match_20sec_inv.bytes, 0) +temp.bytes ,
	pkts = coalesce(stat_agg_match_20sec_inv.pkts, 0) +temp.pkts ,
	flows = coalesce(stat_agg_match_20sec_inv.flows, 0) +temp.flows  
FROM stat_agg_match_20sec_inv_tmp temp 
WHERE  	stat_agg_match_20sec_inv.ts = temp.ts  AND 
	stat_agg_match_20sec_inv.rule_id = temp.rule_id ; 

INSERT INTO stat_agg_match_20sec_inv 
(	 ts , 
	rule_id , 
 	bytes,
	pkts,
	flows ) 
 SELECT temp.ts, 
 	 temp.rule_id  ,
 	 temp.bytes  ,
 	 temp.pkts  ,
 	 temp.flows   
	FROM stat_agg_match_20sec_inv_tmp temp 
	LEFT OUTER JOIN stat_agg_match_20sec_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.rule_id = temp.rule_id ) 
		 WHERE agg.ts IS NULL; 

 delete from stat_agg_match where id <= maxId; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_agg_match_hour() 
-- DROP FUNCTION delete_old_values_stat_agg_match_hour();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_agg_match_hour() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_agg_match_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_agg_match where ts <= maxBucketTime; 



 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_agg_match
	where ts < (nowTime - cast('10 minutes' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: aggregate_stat_agg_match_day() 
-- DROP FUNCTION aggregate_stat_agg_match_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_agg_match_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_agg_match_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_agg_match_20min_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	rule_id    bigint  , 
 	bytes      bigint ,
	pkts       bigint ,
	flows      bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_agg_match_20min_inv_tmp on stat_agg_match_20min_inv_tmp(	ts , 
	rule_id); 
create index idx2_stat_agg_match_20min_inv_tmp on stat_agg_match_20min_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_agg_match_20min_inv_tmp 
select start_time,
	rule_id , 
	coalesce(sum(bytes),0) as bytes,
	coalesce(sum(pkts),0) as pkts,
	coalesce(sum(flows),0) as flows 
from(
     select start_time, 
	rule_id , 
            	coalesce(sum(bytes),0) as bytes,
	coalesce(sum(pkts),0) as pkts,
	coalesce(sum(flows),0) as flows 
     from rpt_buckets_by_day b 
     right join
        (select ts, 
	rule_id , 
 	bytes,
	pkts,
	flows 
            from stat_agg_match_20sec_inv
         where ts >= maxMatchTime and ts < nowTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	rule_id) combined 
 
group by start_time , 
	rule_id; 

UPDATE stat_agg_match_20min_inv 
SET 	ts = temp.ts,
	rule_id = temp.rule_id ,
	bytes = temp.bytes ,
	pkts = temp.pkts ,
	flows = temp.flows  
FROM stat_agg_match_20min_inv_tmp temp 
WHERE  	stat_agg_match_20min_inv.ts = temp.ts  AND 
	stat_agg_match_20min_inv.rule_id = temp.rule_id ; 

INSERT INTO stat_agg_match_20min_inv 
(	 ts , 
	rule_id , 
 	bytes,
	pkts,
	flows ) 
 SELECT temp.ts, 
 	 temp.rule_id  ,
 	 temp.bytes  ,
 	 temp.pkts  ,
 	 temp.flows   
	FROM stat_agg_match_20min_inv_tmp temp 
	LEFT OUTER JOIN stat_agg_match_20min_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.rule_id = temp.rule_id ) 
		 WHERE agg.ts IS NULL; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_agg_match_day() 
-- DROP FUNCTION delete_old_values_stat_agg_match_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_agg_match_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxTimeFor20SecTables timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_agg_match_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 


select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxTimeFor20SecTables; 

 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_agg_match_20sec_inv
	where ts < maxTimeFor20SecTables;

delete from stat_agg_match_20min_inv
	where ts < (nowTime - cast('1 week' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;
-- Function: aggregate_stat_geo() 
-- DROP FUNCTION aggregate_stat_geo();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_geo() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_geo_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_geo where ts <= maxBucketTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_geo_20sec_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	c_cid      bigint ,
	c_rid      bigint ,
	s_cid      bigint ,
	s_rid      bigint  , 
 	c_bytes    bigint ,
	s_bytes    bigint ,
	conns      bigint ,
	c_pkts     bigint ,
	s_pkts     bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_geo_20sec_inv_tmp on stat_geo_20sec_inv_tmp(	ts , 
	c_cid,
	c_rid,
	s_cid,
	s_rid); 
create index idx2_stat_geo_20sec_inv_tmp on stat_geo_20sec_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_geo_20sec_inv_tmp 
select start_time,
	c_cid,
	c_rid,
	s_cid,
	s_rid , 
	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts 
from(
     select start_time, 
	c_cid,
	c_rid,
	s_cid,
	s_rid , 
            	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts 
     from rpt_buckets_by_hr b 
     right join
        (select ts, 
	c_cid,
	c_rid,
	s_cid,
	s_rid , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts 
            from stat_geo
         where ts >= maxMatchTime and ts < maxBucketTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	c_cid,
	c_rid,
	s_cid,
	s_rid) combined 
where (c_bytes+conns+c_pkts+s_bytes+s_pkts) > 0 
group by start_time , 
	c_cid,
	c_rid,
	s_cid,
	s_rid; 

UPDATE stat_geo_20sec_inv 
SET 	c_bytes = coalesce(stat_geo_20sec_inv.c_bytes, 0) +temp.c_bytes ,
	s_bytes = coalesce(stat_geo_20sec_inv.s_bytes, 0) +temp.s_bytes ,
	conns = coalesce(stat_geo_20sec_inv.conns, 0) +temp.conns ,
	c_pkts = coalesce(stat_geo_20sec_inv.c_pkts, 0) +temp.c_pkts ,
	s_pkts = coalesce(stat_geo_20sec_inv.s_pkts, 0) +temp.s_pkts  
FROM stat_geo_20sec_inv_tmp temp 
WHERE  	stat_geo_20sec_inv.ts = temp.ts  AND 
	stat_geo_20sec_inv.c_cid = temp.c_cid AND 
	stat_geo_20sec_inv.c_rid = temp.c_rid AND 
	stat_geo_20sec_inv.s_cid = temp.s_cid AND 
	stat_geo_20sec_inv.s_rid = temp.s_rid ; 

INSERT INTO stat_geo_20sec_inv 
(	 ts , 
	c_cid,
	c_rid,
	s_cid,
	s_rid , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts ) 
 SELECT temp.ts, 
 	 temp.c_cid  ,
 	 temp.c_rid  ,
 	 temp.s_cid  ,
 	 temp.s_rid  ,
 	 temp.c_bytes  ,
 	 temp.s_bytes  ,
 	 temp.conns  ,
 	 temp.c_pkts  ,
 	 temp.s_pkts   
	FROM stat_geo_20sec_inv_tmp temp 
	LEFT OUTER JOIN stat_geo_20sec_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.c_cid = temp.c_cid  AND 	 agg.c_rid = temp.c_rid  AND 	 agg.s_cid = temp.s_cid  AND 	 agg.s_rid = temp.s_rid ) 
		 WHERE agg.ts IS NULL; 

 delete from stat_geo where id <= maxId; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_geo_hour() 
-- DROP FUNCTION delete_old_values_stat_geo_hour();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_geo_hour() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_geo_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_geo where ts <= maxBucketTime; 



 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_geo
	where ts < (nowTime - cast('10 minutes' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: aggregate_stat_geo_day() 
-- DROP FUNCTION aggregate_stat_geo_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_geo_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_geo_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_geo_20min_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	c_cid      bigint ,
	s_cid      bigint  , 
 	c_bytes    bigint ,
	s_bytes    bigint ,
	conns      bigint ,
	c_pkts     bigint ,
	s_pkts     bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_geo_20min_inv_tmp on stat_geo_20min_inv_tmp(	ts , 
	c_cid,
	s_cid); 
create index idx2_stat_geo_20min_inv_tmp on stat_geo_20min_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_geo_20min_inv_tmp 
select start_time,
	c_cid,
	s_cid , 
	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts 
from(
     select start_time, 
	c_cid,
	s_cid , 
            	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts 
     from rpt_buckets_by_day b 
     right join
        (select ts, 
	c_cid,
	s_cid , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts 
            from stat_geo_20sec_inv
         where ts >= maxMatchTime and ts < nowTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	c_cid,
	s_cid) combined 
 
group by start_time , 
	c_cid,
	s_cid; 

UPDATE stat_geo_20min_inv 
SET 	ts = temp.ts,
	c_cid = temp.c_cid ,
	s_cid = temp.s_cid ,
	c_bytes = temp.c_bytes ,
	s_bytes = temp.s_bytes ,
	conns = temp.conns ,
	c_pkts = temp.c_pkts ,
	s_pkts = temp.s_pkts  
FROM stat_geo_20min_inv_tmp temp 
WHERE  	stat_geo_20min_inv.ts = temp.ts  AND 
	stat_geo_20min_inv.c_cid = temp.c_cid AND 
	stat_geo_20min_inv.s_cid = temp.s_cid ; 

INSERT INTO stat_geo_20min_inv 
(	 ts , 
	c_cid,
	s_cid , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts ) 
 SELECT temp.ts, 
 	 temp.c_cid  ,
 	 temp.s_cid  ,
 	 temp.c_bytes  ,
 	 temp.s_bytes  ,
 	 temp.conns  ,
 	 temp.c_pkts  ,
 	 temp.s_pkts   
	FROM stat_geo_20min_inv_tmp temp 
	LEFT OUTER JOIN stat_geo_20min_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.c_cid = temp.c_cid  AND 	 agg.s_cid = temp.s_cid ) 
		 WHERE agg.ts IS NULL; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_geo_day() 
-- DROP FUNCTION delete_old_values_stat_geo_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_geo_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxTimeFor20SecTables timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_geo_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 


select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxTimeFor20SecTables; 

 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_geo_20sec_inv
	where ts < maxTimeFor20SecTables;

delete from stat_geo_20min_inv
	where ts < (nowTime - cast('1 week' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;
-- Function: aggregate_stat_interface_20sec_inv() 
-- DROP FUNCTION aggregate_stat_interface_20sec_inv();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_interface_20sec_inv() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
	 maxIdSec bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_interface_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_interface where ts <= maxBucketTime; 


select max(id) into maxIdSec from stat_pkt where ts <= maxBucketTime; 

 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_interface_20sec_inv_tmp(
	ts timestamp without time zone NOT NULL, 
  
 	total_rx_bytes bigint ,
	total_rx_packets bigint ,
	total_rx_errors bigint ,
	l7_bytes   bigint ,
	l7_packets bigint ,
	sessions   bigint ,
	rx_np_drop_bytes bigint ,
	rx_np_err_bytes bigint ,
	rx_np_drop_pkts bigint ,
	rx_np_err_pkts bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx2_stat_interface_20sec_inv_tmp on stat_interface_20sec_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 
insert into stat_interface_20sec_inv_tmp 
 ---------------------------------------------------------------------- 
 --  This part is not generated, it is bulk inserted    				 
 ---------------------------------------------------------------------- 
		select sp.start_time,hw_bytes,hw_pkts,rx_np_err_pkts, l7_bytes,l7_packets,l7_sessions,rx_np_drop_bytes, rx_np_err_bytes ,rx_np_drop_pkts, rx_np_err_pkts
		from (
			select start_time, sum(coalesce(rx_engine_bytes,0)) as l7_bytes, sum(coalesce(rx_engine_packets,0)) as l7_packets, sum(coalesce(total_flows,0)) as l7_sessions
			from rpt_buckets_by_hr b left outer join 
			(select ts, rx_engine_bytes, rx_engine_packets, total_flows from stat_interface
			  where ts >= maxMatchTime
			  and ts < maxBucketTime) si
			 on b.start_time < si.ts and b.end_time >= si.ts
			where b.start_time >= maxMatchTime and b.end_time < nowTime
			group by start_time
		) sp join
		(
			select start_time, sum(coalesce(rx_hw_bytes,0)) as hw_bytes, 
			 sum(coalesce(rx_hw_pkts,0)) as hw_pkts, sum(coalesce(rx_drop_bytes,0)) as rx_np_drop_bytes,
			 sum(coalesce(rx_err_bytes,0)) as rx_np_err_bytes, sum(coalesce(rx_drop_pkts,0)) as rx_np_drop_pkts, sum(coalesce(rx_err_pkts,0)) as rx_np_err_pkts
			from rpt_buckets_by_hr b left outer join 
			(select ts, rx_hw_bytes, rx_hw_pkts, eth_ipv4_bytes, eth_ipv6_bytes, eth_other_bytes, eth_ipv4_pkts, eth_ipv6_pkts, eth_other_pkts, rx_drop_bytes, rx_err_bytes, rx_drop_pkts, rx_err_pkts from stat_pkt
			  where ts >= maxMatchTime
			  and ts < maxBucketTime) si
			 on b.start_time < si.ts and b.end_time >= si.ts
			where b.start_time >= maxMatchTime and b.end_time < nowTime
			group by start_time
		) si on sp.start_time = si.start_time
		where hw_bytes > 0 ; 
 ---------------------------------------------------------------------- 
UPDATE stat_interface_20sec_inv 
SET 	total_rx_bytes = coalesce(stat_interface_20sec_inv.total_rx_bytes, 0) +temp.total_rx_bytes ,
	total_rx_packets = coalesce(stat_interface_20sec_inv.total_rx_packets, 0) +temp.total_rx_packets ,
	total_rx_errors = coalesce(stat_interface_20sec_inv.total_rx_errors, 0) +temp.total_rx_errors ,
	l7_bytes = coalesce(stat_interface_20sec_inv.l7_bytes, 0) +temp.l7_bytes ,
	l7_packets = coalesce(stat_interface_20sec_inv.l7_packets, 0) +temp.l7_packets ,
	sessions = coalesce(stat_interface_20sec_inv.sessions, 0) +temp.sessions ,
	rx_np_drop_bytes = coalesce(stat_interface_20sec_inv.rx_np_drop_bytes, 0) +temp.rx_np_drop_bytes ,
	rx_np_err_bytes = coalesce(stat_interface_20sec_inv.rx_np_err_bytes, 0) +temp.rx_np_err_bytes ,
	rx_np_drop_pkts = coalesce(stat_interface_20sec_inv.rx_np_drop_pkts, 0) +temp.rx_np_drop_pkts ,
	rx_np_err_pkts = coalesce(stat_interface_20sec_inv.rx_np_err_pkts, 0) +temp.rx_np_err_pkts  
FROM stat_interface_20sec_inv_tmp temp 
WHERE  	stat_interface_20sec_inv.ts = temp.ts 
; 

INSERT INTO stat_interface_20sec_inv 
(	 ts  
 , 
 	total_rx_bytes,
	total_rx_packets,
	total_rx_errors,
	l7_bytes,
	l7_packets,
	sessions,
	rx_np_drop_bytes,
	rx_np_err_bytes,
	rx_np_drop_pkts,
	rx_np_err_pkts ) 
 SELECT temp.ts, 
 	 temp.total_rx_bytes  ,
 	 temp.total_rx_packets  ,
 	 temp.total_rx_errors  ,
 	 temp.l7_bytes  ,
 	 temp.l7_packets  ,
 	 temp.sessions  ,
 	 temp.rx_np_drop_bytes  ,
 	 temp.rx_np_err_bytes  ,
 	 temp.rx_np_drop_pkts  ,
 	 temp.rx_np_err_pkts   
	FROM stat_interface_20sec_inv_tmp temp 
	LEFT OUTER JOIN stat_interface_20sec_inv agg 
 		 ON (agg.ts = temp.ts  ) 
		 WHERE agg.ts IS NULL; 

 delete from stat_interface where id <= maxId; 

 delete from stat_pkt where id <= maxIdSec; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_interface_hour() 
-- DROP FUNCTION delete_old_values_stat_interface_hour();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_interface_hour() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_interface_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_interface where ts <= maxBucketTime; 



 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_interface
	where ts < (nowTime - cast('10 minutes' as interval));

delete from stat_pkt where ts < nowTime - cast ('10 minutes' as interval); 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: aggregate_stat_interface_day() 
-- DROP FUNCTION aggregate_stat_interface_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_interface_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_interface_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_interface_20min_inv_tmp(
	ts timestamp without time zone NOT NULL, 
  
 	total_rx_bytes bigint ,
	total_rx_packets bigint ,
	total_rx_errors bigint ,
	l7_bytes   bigint ,
	l7_packets bigint ,
	sessions   bigint ,
	rx_np_drop_bytes bigint ,
	rx_np_err_bytes bigint ,
	rx_np_drop_pkts bigint ,
	rx_np_err_pkts bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx2_stat_interface_20min_inv_tmp on stat_interface_20min_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_interface_20min_inv_tmp 
select start_time,
  
	coalesce(sum(total_rx_bytes),0) as total_rx_bytes,
	coalesce(sum(total_rx_packets),0) as total_rx_packets,
	coalesce(sum(total_rx_errors),0) as total_rx_errors,
	coalesce(sum(l7_bytes),0) as l7_bytes,
	coalesce(sum(l7_packets),0) as l7_packets,
	coalesce(sum(sessions),0) as sessions,
	coalesce(sum(rx_np_drop_bytes),0) as rx_np_drop_bytes,
	coalesce(sum(rx_np_err_bytes),0) as rx_np_err_bytes,
	coalesce(sum(rx_np_drop_pkts),0) as rx_np_drop_pkts,
	coalesce(sum(rx_np_err_pkts),0) as rx_np_err_pkts 
from(
     select start_time, 
  
            	coalesce(sum(total_rx_bytes),0) as total_rx_bytes,
	coalesce(sum(total_rx_packets),0) as total_rx_packets,
	coalesce(sum(total_rx_errors),0) as total_rx_errors,
	coalesce(sum(l7_bytes),0) as l7_bytes,
	coalesce(sum(l7_packets),0) as l7_packets,
	coalesce(sum(sessions),0) as sessions,
	coalesce(sum(rx_np_drop_bytes),0) as rx_np_drop_bytes,
	coalesce(sum(rx_np_err_bytes),0) as rx_np_err_bytes,
	coalesce(sum(rx_np_drop_pkts),0) as rx_np_drop_pkts,
	coalesce(sum(rx_np_err_pkts),0) as rx_np_err_pkts 
     from rpt_buckets_by_day b 
     right join
        (select ts, 
  
 	total_rx_bytes,
	total_rx_packets,
	total_rx_errors,
	l7_bytes,
	l7_packets,
	sessions,
	rx_np_drop_bytes,
	rx_np_err_bytes,
	rx_np_drop_pkts,
	rx_np_err_pkts 
            from stat_interface_20sec_inv
         where ts >= maxMatchTime and ts < nowTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time  
) combined 
 
group by start_time  
; 

UPDATE stat_interface_20min_inv 
SET 	ts = temp.ts,
	total_rx_bytes = temp.total_rx_bytes ,
	total_rx_packets = temp.total_rx_packets ,
	total_rx_errors = temp.total_rx_errors ,
	l7_bytes = temp.l7_bytes ,
	l7_packets = temp.l7_packets ,
	sessions = temp.sessions ,
	rx_np_drop_bytes = temp.rx_np_drop_bytes ,
	rx_np_err_bytes = temp.rx_np_err_bytes ,
	rx_np_drop_pkts = temp.rx_np_drop_pkts ,
	rx_np_err_pkts = temp.rx_np_err_pkts  
FROM stat_interface_20min_inv_tmp temp 
WHERE  	stat_interface_20min_inv.ts = temp.ts 
; 

INSERT INTO stat_interface_20min_inv 
(	 ts  
 , 
 	total_rx_bytes,
	total_rx_packets,
	total_rx_errors,
	l7_bytes,
	l7_packets,
	sessions,
	rx_np_drop_bytes,
	rx_np_err_bytes,
	rx_np_drop_pkts,
	rx_np_err_pkts ) 
 SELECT temp.ts, 
 	 temp.total_rx_bytes  ,
 	 temp.total_rx_packets  ,
 	 temp.total_rx_errors  ,
 	 temp.l7_bytes  ,
 	 temp.l7_packets  ,
 	 temp.sessions  ,
 	 temp.rx_np_drop_bytes  ,
 	 temp.rx_np_err_bytes  ,
 	 temp.rx_np_drop_pkts  ,
 	 temp.rx_np_err_pkts   
	FROM stat_interface_20min_inv_tmp temp 
	LEFT OUTER JOIN stat_interface_20min_inv agg 
 		 ON (agg.ts = temp.ts  ) 
		 WHERE agg.ts IS NULL; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_interface_day() 
-- DROP FUNCTION delete_old_values_stat_interface_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_interface_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxTimeFor20SecTables timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_interface_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 


select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxTimeFor20SecTables; 

 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_interface_20sec_inv
	where ts < maxTimeFor20SecTables;

delete from stat_interface_20min_inv
	where ts < (nowTime - cast('1 week' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;
-- Function: aggregate_stat_application() 
-- DROP FUNCTION aggregate_stat_application();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_application() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_application_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_application where ts <= maxBucketTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_application_20sec_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	app        character varying(255) ,
	app_id     bigint ,
	c_cid      bigint ,
	type       character varying(255)  , 
 	c_bytes    bigint ,
	c_conns    bigint ,
	c_pkts     bigint ,
	s_bytes    bigint ,
	s_conns    bigint ,
	s_pkts     bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_application_20sec_inv_tmp on stat_application_20sec_inv_tmp(	ts , 
	app,
	app_id,
	c_cid,
	type); 
create index idx2_stat_application_20sec_inv_tmp on stat_application_20sec_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_application_20sec_inv_tmp 
select start_time,
	app,
	app_id,
	c_cid,
	type , 
	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(c_conns),0) as c_conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(s_conns),0) as s_conns,
	coalesce(sum(s_pkts),0) as s_pkts 
from(
     select start_time, 
	app,
	app_id,
	c_cid,
	type , 
            	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(c_conns),0) as c_conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(s_conns),0) as s_conns,
	coalesce(sum(s_pkts),0) as s_pkts 
     from rpt_buckets_by_hr b 
     right join
        (select ts, 
	app,
	app_id,
	c_cid,
	type , 
 	c_bytes,
	c_conns,
	c_pkts,
	s_bytes,
	s_conns,
	s_pkts 
            from stat_application
         where ts >= maxMatchTime and ts < maxBucketTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	app,
	app_id,
	c_cid,
	type) combined 
where app is not null 
group by start_time , 
	app,
	app_id,
	c_cid,
	type; 

UPDATE stat_application_20sec_inv 
SET 	c_bytes = coalesce(stat_application_20sec_inv.c_bytes, 0) +temp.c_bytes ,
	c_conns = coalesce(stat_application_20sec_inv.c_conns, 0) +temp.c_conns ,
	c_pkts = coalesce(stat_application_20sec_inv.c_pkts, 0) +temp.c_pkts ,
	s_bytes = coalesce(stat_application_20sec_inv.s_bytes, 0) +temp.s_bytes ,
	s_conns = coalesce(stat_application_20sec_inv.s_conns, 0) +temp.s_conns ,
	s_pkts = coalesce(stat_application_20sec_inv.s_pkts, 0) +temp.s_pkts  
FROM stat_application_20sec_inv_tmp temp 
WHERE  	stat_application_20sec_inv.ts = temp.ts  AND 
	stat_application_20sec_inv.app = temp.app AND 
	stat_application_20sec_inv.app_id = temp.app_id AND 
	stat_application_20sec_inv.c_cid = temp.c_cid AND 
	stat_application_20sec_inv.type = temp.type ; 

INSERT INTO stat_application_20sec_inv 
(	 ts , 
	app,
	app_id,
	c_cid,
	type , 
 	c_bytes,
	c_conns,
	c_pkts,
	s_bytes,
	s_conns,
	s_pkts ) 
 SELECT temp.ts, 
 	 temp.app  ,
 	 temp.app_id  ,
 	 temp.c_cid  ,
 	 temp.type  ,
 	 temp.c_bytes  ,
 	 temp.c_conns  ,
 	 temp.c_pkts  ,
 	 temp.s_bytes  ,
 	 temp.s_conns  ,
 	 temp.s_pkts   
	FROM stat_application_20sec_inv_tmp temp 
	LEFT OUTER JOIN stat_application_20sec_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.app = temp.app  AND 	 agg.app_id = temp.app_id  AND 	 agg.c_cid = temp.c_cid  AND 	 agg.type = temp.type ) 
		 WHERE agg.ts IS NULL; 

 delete from stat_application where id <= maxId; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_application_hour() 
-- DROP FUNCTION delete_old_values_stat_application_hour();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_application_hour() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_application_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_application where ts <= maxBucketTime; 



 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_application
	where ts < (nowTime - cast('10 minutes' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: aggregate_stat_application_day() 
-- DROP FUNCTION aggregate_stat_application_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_application_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_application_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_application_20min_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	app        character varying(255) ,
	app_id     bigint ,
	c_cid      bigint ,
	type       character varying(255)  , 
 	c_bytes    bigint ,
	c_conns    bigint ,
	c_pkts     bigint ,
	s_bytes    bigint ,
	s_conns    bigint ,
	s_pkts     bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_application_20min_inv_tmp on stat_application_20min_inv_tmp(	ts , 
	app,
	app_id,
	c_cid,
	type); 
create index idx2_stat_application_20min_inv_tmp on stat_application_20min_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_application_20min_inv_tmp 
select start_time,
	app,
	app_id,
	c_cid,
	type , 
	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(c_conns),0) as c_conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(s_conns),0) as s_conns,
	coalesce(sum(s_pkts),0) as s_pkts 
from(
     select start_time, 
	app,
	app_id,
	c_cid,
	type , 
            	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(c_conns),0) as c_conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(s_conns),0) as s_conns,
	coalesce(sum(s_pkts),0) as s_pkts 
     from rpt_buckets_by_day b 
     right join
        (select ts, 
	app,
	app_id,
	c_cid,
	type , 
 	c_bytes,
	c_conns,
	c_pkts,
	s_bytes,
	s_conns,
	s_pkts 
            from stat_application_20sec_inv
         where ts >= maxMatchTime and ts < nowTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	app,
	app_id,
	c_cid,
	type) combined 
 
group by start_time , 
	app,
	app_id,
	c_cid,
	type; 

UPDATE stat_application_20min_inv 
SET 	ts = temp.ts,
	app = temp.app ,
	app_id = temp.app_id ,
	c_cid = temp.c_cid ,
	type = temp.type ,
	c_bytes = temp.c_bytes ,
	c_conns = temp.c_conns ,
	c_pkts = temp.c_pkts ,
	s_bytes = temp.s_bytes ,
	s_conns = temp.s_conns ,
	s_pkts = temp.s_pkts  
FROM stat_application_20min_inv_tmp temp 
WHERE  	stat_application_20min_inv.ts = temp.ts  AND 
	stat_application_20min_inv.app = temp.app AND 
	stat_application_20min_inv.app_id = temp.app_id AND 
	stat_application_20min_inv.c_cid = temp.c_cid AND 
	stat_application_20min_inv.type = temp.type ; 

INSERT INTO stat_application_20min_inv 
(	 ts , 
	app,
	app_id,
	c_cid,
	type , 
 	c_bytes,
	c_conns,
	c_pkts,
	s_bytes,
	s_conns,
	s_pkts ) 
 SELECT temp.ts, 
 	 temp.app  ,
 	 temp.app_id  ,
 	 temp.c_cid  ,
 	 temp.type  ,
 	 temp.c_bytes  ,
 	 temp.c_conns  ,
 	 temp.c_pkts  ,
 	 temp.s_bytes  ,
 	 temp.s_conns  ,
 	 temp.s_pkts   
	FROM stat_application_20min_inv_tmp temp 
	LEFT OUTER JOIN stat_application_20min_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.app = temp.app  AND 	 agg.app_id = temp.app_id  AND 	 agg.c_cid = temp.c_cid  AND 	 agg.type = temp.type ) 
		 WHERE agg.ts IS NULL; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_application_day() 
-- DROP FUNCTION delete_old_values_stat_application_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_application_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxTimeFor20SecTables timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_application_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 


select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxTimeFor20SecTables; 

 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_application_20sec_inv
	where ts < maxTimeFor20SecTables;

delete from stat_application_20min_inv
	where ts < (nowTime - cast('1 week' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;
-- Function: aggregate_stat_provider() 
-- DROP FUNCTION aggregate_stat_provider();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_provider() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_provider_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_provider where ts <= maxBucketTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_provider_20sec_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	asn        bigint  , 
 	pkts_rx    bigint ,
	pkts_tx    bigint ,
	conns      bigint ,
	bytes_rx   bigint ,
	bytes_tx   bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_provider_20sec_inv_tmp on stat_provider_20sec_inv_tmp(	ts , 
	asn); 
create index idx2_stat_provider_20sec_inv_tmp on stat_provider_20sec_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_provider_20sec_inv_tmp 
select start_time,
	asn , 
	coalesce(sum(pkts_rx),0) as pkts_rx,
	coalesce(sum(pkts_tx),0) as pkts_tx,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(bytes_rx),0) as bytes_rx,
	coalesce(sum(bytes_tx),0) as bytes_tx 
from(
     select start_time, 
	asn , 
            	coalesce(sum(pkts_rx),0) as pkts_rx,
	coalesce(sum(pkts_tx),0) as pkts_tx,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(bytes_rx),0) as bytes_rx,
	coalesce(sum(bytes_tx),0) as bytes_tx 
     from rpt_buckets_by_hr b 
     right join
        (select ts, 
	asn , 
 	pkts_rx,
	pkts_tx,
	conns,
	bytes_rx,
	bytes_tx 
            from stat_provider
         where ts >= maxMatchTime and ts < maxBucketTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	asn) combined 
where (conns+pkts_rx+pkts_tx+bytes_rx+bytes_tx) > 0 
group by start_time , 
	asn; 

UPDATE stat_provider_20sec_inv 
SET 	pkts_rx = coalesce(stat_provider_20sec_inv.pkts_rx, 0) +temp.pkts_rx ,
	pkts_tx = coalesce(stat_provider_20sec_inv.pkts_tx, 0) +temp.pkts_tx ,
	conns = coalesce(stat_provider_20sec_inv.conns, 0) +temp.conns ,
	bytes_rx = coalesce(stat_provider_20sec_inv.bytes_rx, 0) +temp.bytes_rx ,
	bytes_tx = coalesce(stat_provider_20sec_inv.bytes_tx, 0) +temp.bytes_tx  
FROM stat_provider_20sec_inv_tmp temp 
WHERE  	stat_provider_20sec_inv.ts = temp.ts  AND 
	stat_provider_20sec_inv.asn = temp.asn ; 

INSERT INTO stat_provider_20sec_inv 
(	 ts , 
	asn , 
 	pkts_rx,
	pkts_tx,
	conns,
	bytes_rx,
	bytes_tx ) 
 SELECT temp.ts, 
 	 temp.asn  ,
 	 temp.pkts_rx  ,
 	 temp.pkts_tx  ,
 	 temp.conns  ,
 	 temp.bytes_rx  ,
 	 temp.bytes_tx   
	FROM stat_provider_20sec_inv_tmp temp 
	LEFT OUTER JOIN stat_provider_20sec_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.asn = temp.asn ) 
		 WHERE agg.ts IS NULL; 

 delete from stat_provider where id <= maxId; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_provider_hour() 
-- DROP FUNCTION delete_old_values_stat_provider_hour();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_provider_hour() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_provider_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_provider where ts <= maxBucketTime; 



 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_provider
	where ts < (nowTime - cast('10 minutes' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: aggregate_stat_provider_day() 
-- DROP FUNCTION aggregate_stat_provider_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_provider_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_provider_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_provider_20min_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	asn        bigint  , 
 	pkts_rx    bigint ,
	pkts_tx    bigint ,
	conns      bigint ,
	bytes_rx   bigint ,
	bytes_tx   bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_provider_20min_inv_tmp on stat_provider_20min_inv_tmp(	ts , 
	asn); 
create index idx2_stat_provider_20min_inv_tmp on stat_provider_20min_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_provider_20min_inv_tmp 
select start_time,
	asn , 
	coalesce(sum(pkts_rx),0) as pkts_rx,
	coalesce(sum(pkts_tx),0) as pkts_tx,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(bytes_rx),0) as bytes_rx,
	coalesce(sum(bytes_tx),0) as bytes_tx 
from(
     select start_time, 
	asn , 
            	coalesce(sum(pkts_rx),0) as pkts_rx,
	coalesce(sum(pkts_tx),0) as pkts_tx,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(bytes_rx),0) as bytes_rx,
	coalesce(sum(bytes_tx),0) as bytes_tx 
     from rpt_buckets_by_day b 
     right join
        (select ts, 
	asn , 
 	pkts_rx,
	pkts_tx,
	conns,
	bytes_rx,
	bytes_tx 
            from stat_provider_20sec_inv
         where ts >= maxMatchTime and ts < nowTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	asn) combined 
 
group by start_time , 
	asn; 

UPDATE stat_provider_20min_inv 
SET 	ts = temp.ts,
	asn = temp.asn ,
	pkts_rx = temp.pkts_rx ,
	pkts_tx = temp.pkts_tx ,
	conns = temp.conns ,
	bytes_rx = temp.bytes_rx ,
	bytes_tx = temp.bytes_tx  
FROM stat_provider_20min_inv_tmp temp 
WHERE  	stat_provider_20min_inv.ts = temp.ts  AND 
	stat_provider_20min_inv.asn = temp.asn ; 

INSERT INTO stat_provider_20min_inv 
(	 ts , 
	asn , 
 	pkts_rx,
	pkts_tx,
	conns,
	bytes_rx,
	bytes_tx ) 
 SELECT temp.ts, 
 	 temp.asn  ,
 	 temp.pkts_rx  ,
 	 temp.pkts_tx  ,
 	 temp.conns  ,
 	 temp.bytes_rx  ,
 	 temp.bytes_tx   
	FROM stat_provider_20min_inv_tmp temp 
	LEFT OUTER JOIN stat_provider_20min_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.asn = temp.asn ) 
		 WHERE agg.ts IS NULL; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_provider_day() 
-- DROP FUNCTION delete_old_values_stat_provider_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_provider_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxTimeFor20SecTables timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_provider_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 


select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxTimeFor20SecTables; 

 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_provider_20sec_inv
	where ts < maxTimeFor20SecTables;

delete from stat_provider_20min_inv
	where ts < (nowTime - cast('1 week' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;
-- Function: aggregate_stat_match() 
-- DROP FUNCTION aggregate_stat_match();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_match() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_match_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_match where ts <= maxBucketTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_match_20sec_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	rule_id    bigint ,
	device     character varying(255) ,
	c_cid      bigint ,
	c_rid      bigint ,
	c_ctid     bigint ,
	s_cid      bigint ,
	s_rid      bigint ,
	s_ctid     bigint  , 
 	c_bytes    bigint ,
	s_bytes    bigint ,
	conns      bigint ,
	c_pkts     bigint ,
	s_pkts     bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_match_20sec_inv_tmp on stat_match_20sec_inv_tmp(	ts , 
	rule_id,
	device,
	c_cid,
	c_rid,
	c_ctid,
	s_cid,
	s_rid,
	s_ctid); 
create index idx2_stat_match_20sec_inv_tmp on stat_match_20sec_inv_tmp(ts); 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_match_detail_agg_tmp(
	ts timestamp without time zone NOT NULL, 
	rule_id    bigint ,
	device     character varying(255) ,
	c_cid      bigint ,
	s_cid      bigint ,
	dst_ip     character varying(255) ,
	src_ip     character varying(255) ,
	app        character varying(255)  , 
 	c_bytes    bigint ,
	s_bytes    bigint ,
	conns      bigint ,
	c_pkts     bigint ,
	s_pkts     bigint ,
	latency    bigint ,
	latency_samples bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_match_detail_agg_tmp on stat_match_detail_agg_tmp(	ts , 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app); 
create index idx2_stat_match_detail_agg_tmp on stat_match_detail_agg_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_match_20sec_inv_tmp 
select start_time,
	rule_id,
	device,
	c_cid,
	c_rid,
	c_ctid,
	s_cid,
	s_rid,
	s_ctid , 
	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts 
from(
     select start_time, 
	rule_id,
	device,
	c_cid,
	c_rid,
	c_ctid,
	s_cid,
	s_rid,
	s_ctid , 
            	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts 
     from rpt_buckets_by_hr b 
     right join
        (select ts, 
	rule_id,
	device,
	c_cid,
	c_rid,
	c_ctid,
	s_cid,
	s_rid,
	s_ctid , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts 
            from stat_match
         where ts >= maxMatchTime and ts < maxBucketTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	rule_id,
	device,
	c_cid,
	c_rid,
	c_ctid,
	s_cid,
	s_rid,
	s_ctid) combined 
where (c_bytes+conns+c_pkts+s_bytes+s_pkts) > 0 
group by start_time , 
	rule_id,
	device,
	c_cid,
	c_rid,
	c_ctid,
	s_cid,
	s_rid,
	s_ctid; 


 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_match_detail_agg_tmp 
select start_time,
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app , 
	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts,
	coalesce(sum(latency),0) as latency,
	coalesce(sum(latency_samples),0) as latency_samples 
from(
     select start_time, 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app , 
            	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts,
	coalesce(sum(latency),0) as latency,
	coalesce(sum(latency_samples),0) as latency_samples 
     from rpt_buckets_by_hr b 
     right join
        (select ts, 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts,
	latency,
	latency_samples 
            from stat_match
         where ts >= maxMatchTime and ts < maxBucketTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app) combined 
where (c_bytes) > 0 
group by start_time , 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app; 

UPDATE stat_match_20sec_inv 
SET 	c_bytes = coalesce(stat_match_20sec_inv.c_bytes, 0) +temp.c_bytes ,
	s_bytes = coalesce(stat_match_20sec_inv.s_bytes, 0) +temp.s_bytes ,
	conns = coalesce(stat_match_20sec_inv.conns, 0) +temp.conns ,
	c_pkts = coalesce(stat_match_20sec_inv.c_pkts, 0) +temp.c_pkts ,
	s_pkts = coalesce(stat_match_20sec_inv.s_pkts, 0) +temp.s_pkts  
FROM stat_match_20sec_inv_tmp temp 
WHERE  	stat_match_20sec_inv.ts = temp.ts  AND 
	stat_match_20sec_inv.rule_id = temp.rule_id AND 
	stat_match_20sec_inv.device = temp.device AND 
	stat_match_20sec_inv.c_cid = temp.c_cid AND 
	stat_match_20sec_inv.c_rid = temp.c_rid AND 
	stat_match_20sec_inv.c_ctid = temp.c_ctid AND 
	stat_match_20sec_inv.s_cid = temp.s_cid AND 
	stat_match_20sec_inv.s_rid = temp.s_rid AND 
	stat_match_20sec_inv.s_ctid = temp.s_ctid ; 

INSERT INTO stat_match_20sec_inv 
(	 ts , 
	rule_id,
	device,
	c_cid,
	c_rid,
	c_ctid,
	s_cid,
	s_rid,
	s_ctid , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts ) 
 SELECT temp.ts, 
 	 temp.rule_id  ,
 	 temp.device  ,
 	 temp.c_cid  ,
 	 temp.c_rid  ,
 	 temp.c_ctid  ,
 	 temp.s_cid  ,
 	 temp.s_rid  ,
 	 temp.s_ctid  ,
 	 temp.c_bytes  ,
 	 temp.s_bytes  ,
 	 temp.conns  ,
 	 temp.c_pkts  ,
 	 temp.s_pkts   
	FROM stat_match_20sec_inv_tmp temp 
	LEFT OUTER JOIN stat_match_20sec_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.rule_id = temp.rule_id  AND 	 agg.device = temp.device  AND 	 agg.c_cid = temp.c_cid  AND 	 agg.c_rid = temp.c_rid  AND 	 agg.c_ctid = temp.c_ctid  AND 	 agg.s_cid = temp.s_cid  AND 	 agg.s_rid = temp.s_rid  AND 	 agg.s_ctid = temp.s_ctid ) 
		 WHERE agg.ts IS NULL; 
UPDATE stat_match_detail_agg 
SET 	c_bytes = coalesce(stat_match_detail_agg.c_bytes, 0) +temp.c_bytes ,
	s_bytes = coalesce(stat_match_detail_agg.s_bytes, 0) +temp.s_bytes ,
	conns = coalesce(stat_match_detail_agg.conns, 0) +temp.conns ,
	c_pkts = coalesce(stat_match_detail_agg.c_pkts, 0) +temp.c_pkts ,
	s_pkts = coalesce(stat_match_detail_agg.s_pkts, 0) +temp.s_pkts ,
	latency = coalesce(stat_match_detail_agg.latency, 0) +temp.latency ,
	latency_samples = coalesce(stat_match_detail_agg.latency_samples, 0) +temp.latency_samples  
FROM stat_match_detail_agg_tmp temp 
WHERE  	stat_match_detail_agg.ts = temp.ts  AND 
	stat_match_detail_agg.rule_id = temp.rule_id AND 
	stat_match_detail_agg.device = temp.device AND 
	stat_match_detail_agg.c_cid = temp.c_cid AND 
	stat_match_detail_agg.s_cid = temp.s_cid AND 
	stat_match_detail_agg.dst_ip = temp.dst_ip AND 
	stat_match_detail_agg.src_ip = temp.src_ip AND 
	stat_match_detail_agg.app = temp.app ; 

INSERT INTO stat_match_detail_agg 
(	 ts , 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts,
	latency,
	latency_samples ) 
 SELECT temp.ts, 
 	 temp.rule_id  ,
 	 temp.device  ,
 	 temp.c_cid  ,
 	 temp.s_cid  ,
 	 temp.dst_ip  ,
 	 temp.src_ip  ,
 	 temp.app  ,
 	 temp.c_bytes  ,
 	 temp.s_bytes  ,
 	 temp.conns  ,
 	 temp.c_pkts  ,
 	 temp.s_pkts  ,
 	 temp.latency  ,
 	 temp.latency_samples   
	FROM stat_match_detail_agg_tmp temp 
	LEFT OUTER JOIN stat_match_detail_agg agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.rule_id = temp.rule_id  AND 	 agg.device = temp.device  AND 	 agg.c_cid = temp.c_cid  AND 	 agg.s_cid = temp.s_cid  AND 	 agg.dst_ip = temp.dst_ip  AND 	 agg.src_ip = temp.src_ip  AND 	 agg.app = temp.app ) 
		 WHERE agg.ts IS NULL; 

 delete from stat_match where id <= maxId; 

 return 0;
END;
$$ LANGUAGE plpgsql;





-- Function: delete_old_values_stat_match_hour() 
-- DROP FUNCTION delete_old_values_stat_match_hour();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_match_hour() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxBucketTime timestamp without time zone; 
	 maxId bigint; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('1 minutes' as interval),now() - cast('5 minutes' as interval)) into maxMatchTime from stat_match_20sec_inv;
select now() into nowTime;
select max(end_time) into maxBucketTime from rpt_buckets_by_hr where end_time < now(); 
select max(id) into maxId from stat_match where ts <= maxBucketTime; 



 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_match
	where ts < (nowTime - cast('10 minutes' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;-- Function: aggregate_stat_match_day() 
-- DROP FUNCTION aggregate_stat_match_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_match_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_match_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 



 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_match_20min_inv_tmp(
	ts timestamp without time zone NOT NULL, 
	rule_id    bigint ,
	device     character varying(255) ,
	c_cid      bigint ,
	s_cid      bigint  , 
 	c_bytes    bigint ,
	s_bytes    bigint ,
	conns      bigint ,
	c_pkts     bigint ,
	s_pkts     bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_match_20min_inv_tmp on stat_match_20min_inv_tmp(	ts , 
	rule_id,
	device,
	c_cid,
	s_cid); 
create index idx2_stat_match_20min_inv_tmp on stat_match_20min_inv_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_match_20min_inv_tmp 
select start_time,
	rule_id,
	device,
	c_cid,
	s_cid , 
	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts 
from(
     select start_time, 
	rule_id,
	device,
	c_cid,
	s_cid , 
            	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts 
     from rpt_buckets_by_day b 
     right join
        (select ts, 
	rule_id,
	device,
	c_cid,
	s_cid , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts 
            from stat_match_20sec_inv
         where ts >= maxMatchTime and ts < nowTime) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= maxMatchTime  and b.end_time < nowTime 
     group by start_time , 
	rule_id,
	device,
	c_cid,
	s_cid) combined 
 
group by start_time , 
	rule_id,
	device,
	c_cid,
	s_cid; 

UPDATE stat_match_20min_inv 
SET 	ts = temp.ts,
	rule_id = temp.rule_id ,
	device = temp.device ,
	c_cid = temp.c_cid ,
	s_cid = temp.s_cid ,
	c_bytes = temp.c_bytes ,
	s_bytes = temp.s_bytes ,
	conns = temp.conns ,
	c_pkts = temp.c_pkts ,
	s_pkts = temp.s_pkts  
FROM stat_match_20min_inv_tmp temp 
WHERE  	stat_match_20min_inv.ts = temp.ts  AND 
	stat_match_20min_inv.rule_id = temp.rule_id AND 
	stat_match_20min_inv.device = temp.device AND 
	stat_match_20min_inv.c_cid = temp.c_cid AND 
	stat_match_20min_inv.s_cid = temp.s_cid ; 

INSERT INTO stat_match_20min_inv 
(	 ts , 
	rule_id,
	device,
	c_cid,
	s_cid , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts ) 
 SELECT temp.ts, 
 	 temp.rule_id  ,
 	 temp.device  ,
 	 temp.c_cid  ,
 	 temp.s_cid  ,
 	 temp.c_bytes  ,
 	 temp.s_bytes  ,
 	 temp.conns  ,
 	 temp.c_pkts  ,
 	 temp.s_pkts   
	FROM stat_match_20min_inv_tmp temp 
	LEFT OUTER JOIN stat_match_20min_inv agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.rule_id = temp.rule_id  AND 	 agg.device = temp.device  AND 	 agg.c_cid = temp.c_cid  AND 	 agg.s_cid = temp.s_cid ) 
		 WHERE agg.ts IS NULL; 

 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: delete_old_values_stat_match_day() 
-- DROP FUNCTION delete_old_values_stat_match_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_match_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxTimeFor20SecTables timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_match_20min_inv;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 


select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxTimeFor20SecTables; 

 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_match_20sec_inv
	where ts < maxTimeFor20SecTables;

delete from stat_match_20min_inv
	where ts < (nowTime - cast('1 week' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;


-- Function: aggregate_stat_match_detail_day() 
-- DROP FUNCTION aggregate_stat_match_detail_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION aggregate_stat_match_detail_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 interimTime timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
 select coalesce(max(ts) - cast('20 minutes' as interval), now() - cast('40 minutes' as interval)) into maxMatchTime from stat_match_detail_agg_day;
select GREATEST(maxMatchTime, now() - cast('90 minutes' as interval)) into maxMatchTime; 
select now() into nowTime;


	 select (maxMatchTime +  cast('20 minutes' as interval)) into interimTime; 
 while (nowTime >= maxMatchTime) loop 
    raise notice ' *** nowTime:            %', nowTime; 
    raise notice ' *** maxMatchTime:	    %', maxMatchTime;
    raise notice ' *** interimTime:	    %', interimTime; 

 -- Create a temp table to keep data that need to be added/inserted or updated 
CREATE TEMPORARY TABLE stat_match_detail_agg_day_tmp(
	ts timestamp without time zone NOT NULL, 
	rule_id    bigint ,
	device     character varying(255) ,
	c_cid      bigint ,
	s_cid      bigint ,
	dst_ip     character varying(255) ,
	src_ip     character varying(255) ,
	app        character varying(255)  , 
 	c_bytes    bigint ,
	s_bytes    bigint ,
	conns      bigint ,
	c_pkts     bigint ,
	s_pkts     bigint ,
	latency    bigint ,
	latency_samples bigint ) on commit drop;



 -- Create an index on the temp table 
create index idx1_stat_match_detail_agg_day_tmp on stat_match_detail_agg_day_tmp(	ts , 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app); 
create index idx2_stat_match_detail_agg_day_tmp on stat_match_detail_agg_day_tmp(ts); 



 --Insert new values (for timestamps that are not yet in the aggregated table 

insert into stat_match_detail_agg_day_tmp 
select start_time,
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app , 
	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts,
	coalesce(sum(latency),0) as latency,
	coalesce(sum(latency_samples),0) as latency_samples 
from(
     select start_time, 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app , 
            	coalesce(sum(c_bytes),0) as c_bytes,
	coalesce(sum(s_bytes),0) as s_bytes,
	coalesce(sum(conns),0) as conns,
	coalesce(sum(c_pkts),0) as c_pkts,
	coalesce(sum(s_pkts),0) as s_pkts,
	coalesce(sum(latency),0) as latency,
	coalesce(sum(latency_samples),0) as latency_samples 
     from rpt_buckets_by_day b 
     right join
        (select ts, 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts,
	latency,
	latency_samples 
            from stat_match_detail_agg
         where ts >= (maxMatchTime - cast('1 minutes' as interval)) and ts < (interimTime + cast('1 minutes' as interval))) si 
         on b.start_time < si.ts and b.end_time >= si.ts 
     where b.start_time >= (maxMatchTime - cast('1 minutes' as interval))  and b.end_time < (interimTime + cast('1 minutes' as interval)) 
     group by start_time , 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app) combined 
 
group by start_time , 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app; 

UPDATE stat_match_detail_agg_day 
SET 	ts = temp.ts,
	rule_id = temp.rule_id ,
	device = temp.device ,
	c_cid = temp.c_cid ,
	s_cid = temp.s_cid ,
	dst_ip = temp.dst_ip ,
	src_ip = temp.src_ip ,
	app = temp.app ,
	c_bytes = temp.c_bytes ,
	s_bytes = temp.s_bytes ,
	conns = temp.conns ,
	c_pkts = temp.c_pkts ,
	s_pkts = temp.s_pkts ,
	latency = temp.latency ,
	latency_samples = temp.latency_samples  
FROM stat_match_detail_agg_day_tmp temp 
WHERE  	stat_match_detail_agg_day.ts = temp.ts  AND 
	stat_match_detail_agg_day.rule_id = temp.rule_id AND 
	stat_match_detail_agg_day.device = temp.device AND 
	stat_match_detail_agg_day.c_cid = temp.c_cid AND 
	stat_match_detail_agg_day.s_cid = temp.s_cid AND 
	stat_match_detail_agg_day.dst_ip = temp.dst_ip AND 
	stat_match_detail_agg_day.src_ip = temp.src_ip AND 
	stat_match_detail_agg_day.app = temp.app ; 

INSERT INTO stat_match_detail_agg_day 
(	 ts , 
	rule_id,
	device,
	c_cid,
	s_cid,
	dst_ip,
	src_ip,
	app , 
 	c_bytes,
	s_bytes,
	conns,
	c_pkts,
	s_pkts,
	latency,
	latency_samples ) 
 SELECT temp.ts, 
 	 temp.rule_id  ,
 	 temp.device  ,
 	 temp.c_cid  ,
 	 temp.s_cid  ,
 	 temp.dst_ip  ,
 	 temp.src_ip  ,
 	 temp.app  ,
 	 temp.c_bytes  ,
 	 temp.s_bytes  ,
 	 temp.conns  ,
 	 temp.c_pkts  ,
 	 temp.s_pkts  ,
 	 temp.latency  ,
 	 temp.latency_samples   
	FROM stat_match_detail_agg_day_tmp temp 
	LEFT OUTER JOIN stat_match_detail_agg_day agg 
 		 ON (agg.ts = temp.ts  AND  	 agg.rule_id = temp.rule_id  AND 	 agg.device = temp.device  AND 	 agg.c_cid = temp.c_cid  AND 	 agg.s_cid = temp.s_cid  AND 	 agg.dst_ip = temp.dst_ip  AND 	 agg.src_ip = temp.src_ip  AND 	 agg.app = temp.app ) 
		 WHERE agg.ts IS NULL; 
 select interimTime into maxMatchTime;
 select (interimTime +  cast('20 minutes' as interval)) into interimTime; 
 drop table stat_match_detail_agg_day_tmp;
 --drop index idx1_stat_match_detail_agg_day_tmp;
 --drop index idx2_stat_match_detail_agg_day_tmp;

 end loop;

 raise notice ' >>> %:Max ts in stat_match_detail_agg_day: %', clock_timestamp(), (select max(ts) from stat_match_detail_agg_day);
 return 0;
END;
$$ LANGUAGE plpgsql;





-- Function: delete_old_values_stat_match_detail_day() 
-- DROP FUNCTION delete_old_values_stat_match_detail_day();


 -- Function Header
 CREATE OR REPLACE FUNCTION delete_old_values_stat_match_detail_day() RETURNS int AS $$ 

 -- Declare local variables 
DECLARE 
	 maxMatchTime timestamp without time zone; 
	 nowTime timestamp without time zone; 
	 maxTimeFor20SecTables timestamp without time zone; 
BEGIN


 -- Init maxMatchTime (what is the oldest ts to be considered) and now time (start function execution time) 
select coalesce(max(ts) - cast('20 minutes' as interval),now() - cast('1 day' as interval)) into maxMatchTime from stat_match_detail_agg_day;
select now() into nowTime;
select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxMatchTime; 


select GREATEST(maxMatchTime, nowTime - cast('90 minutes' as interval)) into maxTimeFor20SecTables; 

 -- Delete old values from aggregation table
 -- 20sec table should keep data for 1 hour +/- a small delta
 -- 20min table should keep data for 7 days +/- a small delta  
delete from stat_match_detail_agg
	where ts < maxTimeFor20SecTables;

delete from stat_match_detail_agg_day
	where ts < (nowTime - cast('1 week' as interval));


 return 0;
END;
$$ LANGUAGE plpgsql;
----------- ----------------------- ---------------
----------- End: Aggregate Stat Functions  --------
----------- End: Auto generated code       --------
----------- ----------------------- ---------------


CREATE OR REPLACE FUNCTION aggregate_all_tables_sec() RETURNS int AS $$ 
 BEGIN
	perform aggregate_stat_global_netflow();
	perform aggregate_stat_agg_app();
	perform aggregate_stat_device();
	perform aggregate_stat_agg_match();
	perform aggregate_stat_geo();
	perform aggregate_stat_interface_20sec_inv();
	perform aggregate_stat_application();
	perform aggregate_stat_provider();
	perform aggregate_stat_match();

	 return 0;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION aggregate_all_tables_min() RETURNS int AS $$ 
 BEGIN
	perform aggregate_stat_global_netflow_day();
	perform aggregate_stat_agg_app_day();
	perform aggregate_stat_device_day();
	perform aggregate_stat_agg_match_day();
	perform aggregate_stat_geo_day();
	perform aggregate_stat_interface_day();
	perform aggregate_stat_application_day();
	perform aggregate_stat_provider_day();
	perform aggregate_stat_match_day();
	perform aggregate_stat_match_detail_day();

	 return 0;
END;
$$ LANGUAGE plpgsql;

