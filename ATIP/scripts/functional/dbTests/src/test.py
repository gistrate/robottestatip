'''
Created on Jul 5, 2017

@author: dona
'''

import os
import psycopg2
import subprocess
import unittest
from ConfigFile import ConfigFile
import constant


                                                    
               
class Test(unittest.TestCase):
    
    
    def runEachScriptFromPath(self, path, filePath, checkError):
        print("Running ", path)
        result=subprocess.check_output(constant.SQL_COMMAND + path , stderr = subprocess.STDOUT, shell = True).decode('utf-8')
         
        if checkError == True:
            for line in result.splitlines():
                if "ERROR: " in line:
                    print(line)
                    self.fail('Found errors in: {}'.format(filePath))
          
        if checkError == False and "ERROR: " in result:
            print("Errors in: ", filePath)   
            
               
    
    def setUp(self):
        config = ConfigFile()
        try:
            self.connection = psycopg2.connect(host = config.host, user = config.user, dbname = config.dbName, port = config.portNumber)
            self.connection.set_isolation_level(0)
            print("Connected to database postgres...")   
            self.cur = self.connection.cursor()       
        except Exception as e:
            print(e) 
            self.fail("Unable to connect to postgres")
            
        self.cur.execute("SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = 'atie'")
        try:
            self.cur.execute("select * from pg_database where datname = 'atie'")
            checkedExistence = self.cur.fetchall()
            if len(checkedExistence) > 0:
                print("database exists")
                self.cur.execute("DROP DATABASE atie;")
                self.connection.commit()
                print("database atie dropped")
                self.cur.execute("CREATE DATABASE atie;")   
                print("atie recreated")
            else:
                print("database does not exist") 
                self.cur.execute("CREATE DATABASE atie;") 
                print("atie created")  
        except Exception as e:
            print(e)         
        
               
        
    def tearDown(self):
        pass

        
    def testFreshInstallDB(self):
            self.runEachScriptFromPath('/opt/atie/control/setup_db.sql', None, True)
              
    
    def testCheckScriptsForPreviousVersion(self): 
            self.runEachScriptFromPath('setup_db_v1.5.2.sql', None, True)
            pathToDir = constant.PATH
            listOfAllFiles = [] 
                  
            for (dirpath, dirnames, filenames) in sorted(os.walk(constant.PATH)):
                for file in filenames:
                    pathToFile = dirpath + os.path.sep + file  
                    listOfAllFiles.append(pathToFile)
            listOfAllFiles.sort()
                
                                 
            for pathToFile in listOfAllFiles:
                currentIndexOfFile = listOfAllFiles.index(pathToFile)
                indexOfSearchedFile = listOfAllFiles.index(constant.SCRIPTPATH)  
                if  currentIndexOfFile <= indexOfSearchedFile:
                    self.runEachScriptFromPath(pathToFile, pathToFile, False)
                else:
                    self.runEachScriptFromPath(pathToFile, pathToFile, True)    
                         

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
     
    

            
          
                    

