from atipAutoFrwk.data.atip.NetflowStats import NetflowStats

class SuiteConfig(object):
    netflowRecords = NetflowStats.DATA_RECORDS
    captureLocation = '/home/atip/captures/'

