from http import HTTPStatus

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.ApplicationGroupType import AppGroupType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.webApi.WebApiSession import WebApiSession



class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    timeInterval = TimeInterval.HOUR
    France = GeoLocation.FRANCE
    China = GeoLocation.CHINA
    Amazon = ApplicationType.AMAZON
    Apple = ApplicationType.APPLE
    Netflix = ApplicationType.NETFLIX
    expectedStatus = HTTPStatus.INTERNAL_SERVER_ERROR
    # ATIP config
    singleGeo = FilterConfig("SigleGeo")
    FilterConditionConfig.addGeoCondition(singleGeo, GeoLocation.CHINA)
    multipleGeo = FilterConfig("MultipleGeos")
    FilterConditionConfig.addGeoCondition(multipleGeo, [GeoLocation.INDIA, GeoLocation.CHINA])
    singleApp = FilterConfig("SingleApp")
    FilterConditionConfig.addAppCondition(singleApp, ApplicationType.FACEBOOK)
    appAndActionFilter = FilterConfig("SingleAction")
    FilterConditionConfig.addAppActionCondition(appAndActionFilter, ApplicationType.NETFLIX, ["ActionAndAdventure"])
    multipleActions = FilterConfig("SingleAppWithMultipleAction")
    singleApp = FilterConfig("SingleStaticApp")
    FilterConditionConfig.addAppCondition(singleApp, ApplicationType.FACEBOOK)
    dynApp = FilterConfig("SingleDynApp")
    FilterConditionConfig.addAppCondition(dynApp, ApplicationType.DYNAMIC1)
    appAndActionFilter = FilterConfig("SingleAction")
    FilterConditionConfig.addAppActionCondition(appAndActionFilter, ApplicationType.NETFLIX, ["ActionAndAdventure"])
    multipleActions = FilterConfig("SingleAppWithMultipleAction")
    FilterConditionConfig.addAppActionCondition(multipleActions, ApplicationType.NETFLIX, ["ActionAndAdventure", "ChildrenAndFamily"])
    multipleApps = FilterConfig("MultipleApps")
    FilterConditionConfig.addAppCondition(multipleApps, [ApplicationType.NETFLIX, ApplicationType.AMAZON])
    customFilter = FilterConfig("CustomApp")
    FilterConditionConfig.addAppCondition(customFilter, ApplicationType.IXIACOM)
    singleGroup = FilterConfig("SingleAppGroup")
    FilterConditionConfig.addAppGroupCondition(singleGroup, AppGroupType.STEAMING)
    dynGroup = FilterConfig("DynamicGroup")
    FilterConditionConfig.addAppGroupCondition(dynGroup, AppGroupType.DYNAMIC)
    multipleGroups = FilterConfig("MultipleAppGroup")
    FilterConditionConfig.addAppGroupCondition(multipleGroups,[AppGroupType.STEAMING, AppGroupType.HTTP, AppGroupType.NETFLIX])
    geoMultipleApp = FilterConfig("GeoAndMultipleApps")
    FilterConditionConfig.addAppCondition(geoMultipleApp, [ApplicationType.AMAZON, ApplicationType.FACEBOOK])
    FilterConditionConfig.addGeoCondition(geoMultipleApp, GeoLocation.CHINA)
    geoAndAppGroup = FilterConfig("GeoGroup")
    FilterConditionConfig.addAppGroupCondition(geoAndAppGroup, AppGroupType.NETFLIX)
    FilterConditionConfig.addGeoCondition(geoAndAppGroup, GeoLocation.INDIA)
    appAndAppGroup = FilterConfig("AppAndAppGroup")
    FilterConditionConfig.addAppGroupCondition(appAndAppGroup, AppGroupType.HTTP)
    FilterConditionConfig.addAppCondition(appAndAppGroup, ApplicationType.NETFLIX)

    # Test: Validate Filter Name

    bpsTestConfigHw1 = BpsTestConfig('BasicFilters_auto_HW', 80)
    bpsTestConfigVirt1 = BpsTestConfig('BasicFilters_auto_VIRTUAL', 80)
    bpsTestConfig3 = BpsTestConfig('Group-Netflix', 190)
    bpsTestConfigHw6 = BpsTestConfig('Netflix_ChildrenFamily-ActionAdv', 140)
    bpsTestConfigVirt6 = BpsTestConfig('Netflix_ChildrenFamily-ActionAdv_Virt', 140)
    bpsTestConfig7 = BpsTestConfig('CustomApp_auto', 80)
    bpsTestConfig4 = BpsTestConfig('DynApp_auto', 400)
    bpsTestConfigHw44 = BpsTestConfig('DynApp_auto_1min', 70)
    bpsTestConfigVirt44 = BpsTestConfig('DynApp_auto_1min_virt', 70)
    appsim1 = AppSimConfig('appsim_1', ApplicationType.AMAZON, OSType.Linux, BrowserType.BonEcho, GeoLocation.CHINA,
                           GeoLocation.CHINA, PacketSizes(70, 362, 450, 66))
    appsim2 = AppSimConfig('appsim_2', ApplicationType.NETFLIX, OSType.Linux, BrowserType.SeaMonkey,
                           GeoLocation.INDIA, GeoLocation.INDIA, PacketSizes(70, 517, 450, 66))
    appsim3 = AppSimConfig('appsim_3', ApplicationType.FACEBOOK, OSType.MacOS, BrowserType.SeaMonkey,
                           GeoLocation.CHINA,
                           GeoLocation.CHINA, PacketSizes(70, 643, 450, 66))
    appsim4 = AppSimConfig('appsim_4', ApplicationType.LINKEDIN, OSType.Linux, BrowserType.BonEcho, GeoLocation.INDIA,
                           GeoLocation.INDIA, PacketSizes(70, 464, 450, 66))

    appsim5 = AppSimConfig('appsim_1', ApplicationType.DYNAMIC1, OSType.Linux, BrowserType.BonEcho, GeoLocation.CHINA,
                           GeoLocation.CHINA)
    appsim6 = AppSimConfig('appsim_2', ApplicationType.DYNAMIC2, OSType.Linux, BrowserType.SeaMonkey,
                           GeoLocation.INDIA, GeoLocation.INDIA)
    appsim7 = AppSimConfig('appsim_3', ApplicationType.DYNAMIC3, OSType.MacOS, BrowserType.SeaMonkey,
                           GeoLocation.THAILAND,
                           GeoLocation.THAILAND)
    appsim8 = AppSimConfig('appsim_4', ApplicationType.DYNAMIC4, OSType.Linux, BrowserType.BonEcho, GeoLocation.ITALY,
                           GeoLocation.ITALY)
    if  envVars.atipType == AtipType.VIRTUAL:
        bpsTestConfig1 = bpsTestConfigVirt1
        bpsTestConfig6 = bpsTestConfigVirt6
        bpsTestConfig44 = bpsTestConfigVirt44
    else:
        bpsTestConfig1 = bpsTestConfigHw1
        bpsTestConfig6 = bpsTestConfigHw6
        bpsTestConfig44 = bpsTestConfigHw44
    bpsTestConfig1.addAppSim([appsim1, appsim2, appsim3, appsim4])
    bpsTestConfig44.addAppSim([appsim5, appsim6, appsim7, appsim8])

    strFacebook = ApplicationType.FACEBOOK.appName
    strAOL = ApplicationType.AOL.appName
    strAmazon = ApplicationType.AMAZON.appName
    strLinkedin = ApplicationType.LINKEDIN.appName
    strNetflix = ApplicationType.NETFLIX.appName
    strTech = ApplicationType.TECHCRUNCH.appName
    strBusiness = ApplicationType.BUSINESSINSIDER.appName
    strBored = ApplicationType.BOREDPANDA.appName
    strAustralia = GeoLocation.AUSTRALIA.countryName
    strChina = GeoLocation.CHINA.countryName
    strUS = GeoLocation.UNITED_STATES.countryName
    strIxiaCom = ApplicationType.IXIACOM.appName
    strDyn1 = ApplicationType.DYNAMIC1.appName
    bpsSessions = BpsStatsType.SESSIONS
    atipSessions = TopStats.TOTAL_COUNT
    statsTypeApp = StatsType.APPS
    statsTypeRules = StatsType.RULES
    statsTypeGeo = StatsType.GEO
    statsTypeDyn = StatsType.DYNAMIC
    npId0 = str(envVars.atipSlotNumber[0])

    atipConfig = AtipConfig()


