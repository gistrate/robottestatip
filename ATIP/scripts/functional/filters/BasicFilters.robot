*** Settings ***
Documentation  Filters basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.webApi.atip.CustomAppConfig
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.Apps
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    atipAutoFrwk.config.atip.filter.FilterConditionConfig
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService  WITH NAME  GeneralService
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    atipAutoFrwk.services.atip.NPService


Default Tags  hardware  virtual

Suite Setup        Suite setup
Suite Teardown     logout  ${atipSession}


*** Test Cases ***
Filter - single geo condition
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    #Create filter with Geo Condition China
    ${filterConfig}=  createFilter  ${atipSession}  ${singleGeo}
    rulePush  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    checkFilterExistsInTopStats  ${atipSession}  ${filterConfig}  ${timeInterval}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig1}
    ${bpsSessionsComp1}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    ${bpsSessionsComp3}  getStatItem  ${bpsComponentsStats['appsim_3']}  ${bpsSessions}
    ${bpsSessionsChina}  Evaluate  ${bpsSessionsComp1} + ${bpsSessionsComp3}
    #Verify Top Countries window
    ${stat}  ListStats.getTopStats  ${atipSession}  ${statsTypeGeo}  ${timeInterval}
    checkAtipListStats  ${stat}  ${strChina}  ${atipSessions}  ${bpsSessionsChina}
    #Verify Top Filters window
    checkAtipTopStatsTotalSessions  ${atipSession}  ${singleGeo}  ${bpsSessionsChina}  ${timeInterval}
    #Update filter with Geo Condition France
    resetConditions  ${filterConfig}
    addGeoCondition  ${filterConfig}  ${France}
    updateFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}
    resetStats  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    checkFilterDoesNotExistsInTopStats  ${atipSession}  ${filterConfig}  ${timeInterval}

Filter - multiple geo conditions
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    #Create filter with geo conditions; China and Italy (both should match)
    ${filterConfig}=  createFilter  ${atipSession}  ${multipleGeo}
    rulePush  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    checkFilterExistsInTopStats  ${atipSession}  ${multipleGeo}  ${timeInterval}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig1}
    ${totalSessions}  sumStatItem  ${bpsComponentsStats}  ${bpsSessions}
    #Verify Top Filters window
    checkAtipTopStatsTotalSessions  ${atipSession}  ${multipleGeo}  ${totalSessions}  ${timeInterval}
    #Update filter with geo conditions; China and France (only China should match)
    resetConditions  ${filterConfig}
    ${countries}  create list  ${France}  ${China}
    addGeoCondition  ${filterConfig}  ${countries}
    updateFilter  ${atipSession}  ${filterConfig}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    checkFilterExistsInTopStats  ${atipSession}  ${multipleGeo}  ${timeInterval}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig1}
    ${bpsSessionsComp1}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    ${bpsSessionsComp3}  getStatItem  ${bpsComponentsStats['appsim_3']}  ${bpsSessions}
    ${bpsSessionsChina}  Evaluate  ${bpsSessionsComp1} + ${bpsSessionsComp3}
    #Verify Top Filters window
    checkAtipTopStatsTotalSessions  ${atipSession}  ${multipleGeo}  ${bpsSessionsChina}  ${timeInterval}



Filter - single app condition - static
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    #Create filter with App Condition Netflix
    ${filterConfig}=  createFilter  ${atipSession}  ${singleApp}
    rulePush  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    checkFilterExistsInTopStats  ${atipSession}  ${singleApp}  ${timeInterval}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig1}
    ${stat}  PieStats.getTopStats  ${atipSession}  ${statsTypeApp}  ${timeInterval}
    #Validate traffic ic correctly processed by ATIP
    checkSpecificFieldStat  ${stat}  ${bpsTestConfig1}  ${bpsComponentsStats}  ${atipSession}  ${statsTypeApp}  ${atipSessions}
    #Compare Facebook BPS sessions with Top Filters stats
    ${bpsSessionsComp3}  getStatItem  ${bpsComponentsStats['appsim_3']}  ${bpsSessions}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${singleApp}  ${bpsSessionsComp3}  ${timeInterval}
    #Update filter with App Condition Apple
    resetConditions  ${filterConfig}
    addAppCondition  ${filterConfig}  ${Apple}
    updateFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}
    resetStats  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    checkFilterDoesNotExistsInTopStats  ${atipSession}  ${filterConfig}  ${timeInterval}



Filter - single app condition - dynamic
    resetStats  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig4}
    deleteAllFilters  ${atipSession}
    #Create filter with a dynamic app
    ${filterConfig}=  createFilter  ${atipSession}  ${dynApp}
    rulePush  ${atipSession}
    resetStats  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig44}
    checkFilterExistsInTopStats  ${atipSession}  ${dynApp}  ${timeInterval}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig44}
    ${statsApp}  PieStats.getTopStats  ${atipSession}  ${statsTypeApp}  ${timeInterval}
    ${statsDyn}  ListStats.getTopStats  ${atipSession}  ${statsTypeDyn}  ${timeInterval}
    ${statsGeo}  ListStats.getTopStats  ${atipSession}  ${statsTypeGeo}  ${timeInterval}
    #Validate traffic ic correctly processed by ATIP - check pie apps, top countries, dynamic apps window stats
    checkSpecificFieldStat  ${statsApp}  ${bpsTestConfig44}  ${bpsComponentsStats}  ${atipSession}  ${statsTypeApp}  ${atipSessions}
    checkSpecificFieldStat  ${statsDyn}  ${bpsTestConfig44}  ${bpsComponentsStats}  ${atipSession}  ${statsTypeDyn}  ${atipSessions}
    #Compare dynamic app 1 BPS sessions with Top Filters stats
    ${bpsSessionsComp1}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${dynApp}  ${bpsSessionsComp1}  ${timeInterval}




Filter - multiple app conditions
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    #Create filter with App Condition Netflix and Amazon
    ${filterConfig}=  createFilter  ${atipSession}  ${multipleApps}
    rulePush  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    checkFilterExistsInTopStats  ${atipSession}  ${multipleApps}  ${timeInterval}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig1}
    ${stat}  PieStats.getTopStats  ${atipSession}  ${statsTypeApp}  ${timeInterval}
    #Validate traffic ic correctly processed by ATIP
    checkSpecificFieldStat  ${stat}  ${bpsTestConfig1}  ${bpsComponentsStats}  ${atipSession}  ${statsTypeApp}  ${atipSessions}
    #Compare Netflix and Amazon BPS sessions with Top Filters stats
    ${bpsSessionsComp1}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    ${bpsSessionsComp2}  getStatItem  ${bpsComponentsStats['appsim_2']}  ${bpsSessions}
    ${totalSessions}  Evaluate  ${bpsSessionsComp1} + ${bpsSessionsComp2}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${multipleApps}  ${totalSessions}  ${timeInterval}
    resetConditions  ${filterConfig}
    ${apps}  create list  ${Apple}  ${Netflix}
    addAppCondition  ${filterConfig}  ${apps}
    updateFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}
    resetStats  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig1}
    ${bpsSessionsComp2}  getStatItem  ${bpsComponentsStats['appsim_2']}  ${bpsSessions}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${multipleApps}  ${bpsSessionsComp2}  ${timeInterval}


Filter - app single action
    #BUG1473820: 	Top Filter stats are not accurate for a filter with an app and action condition.
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    ${appAndActionFilterNew}=  updateFiltersActions  ${atipSession}  ${appAndActionFilter}
    ${filterConfig}=  createFilter  ${atipSession}  ${appAndActionFilterNew}
    rulePush  ${atipSession}
    checkNpRules  ${atipSession}  2  ${npId0}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig6}
    checkFilterExistsInTopStats  ${atipSession}  ${appAndActionFilterNew}  ${timeInterval}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig6}
    ${totalSessions}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${appAndActionFilterNew}  ${totalSessions}  ${timeInterval}
    #Run traffic without an action
    resetStats  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    checkFilterDoesNotExistsInTopStats  ${atipSession}  ${filterConfig}  ${timeInterval}



Filter - app multiple actions
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    ${appAndActionFilterNew}=  updateFiltersActions  ${atipSession}  ${multipleActions}
    createFilter  ${atipSession}  ${multipleActions}
    rulePush  ${atipSession}
    checkNpRules  ${atipSession}  2  ${npId0}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig6}
    checkFilterExistsInTopStats  ${atipSession}  ${multipleActions}  ${timeInterval}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig6}
    ${bpsSessionsComp1}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    ${bpsSessionsComp2}  getStatItem  ${bpsComponentsStats['appsim_1_1']}  ${bpsSessions}
    ${bpsSessions}  Evaluate  ${bpsSessionsComp1} + ${bpsSessionsComp2}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${multipleActions}  ${bpsSessions}  ${timeInterval}

Filter - single app group
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    createFilter  ${atipSession}  ${singleGroup}
    rulePush  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig3}
    checkFilterExistsInTopStats  ${atipSession}  ${singleGroup}  ${timeInterval}

Filter - multiple app groups
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    createFilter  ${atipSession}  ${multipleGroups}
    rulePush  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig3}
    checkFilterExistsInTopStats  ${atipSession}  ${multipleGroups}  ${timeInterval}

Filter - dynamic app group
    resetStats  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig4}
    deleteAllFilters  ${atipSession}
    createFilter  ${atipSession}  ${dynGroup}
    rulePush  ${atipSession}
    resetStats  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig44}
    checkFilterExistsInTopStats  ${atipSession}  ${dynGroup}  ${timeInterval}
    ${bpsSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTestConfig44}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${dynGroup}  ${bpsSessions}  ${timeInterval}


Filter - geo and multiple app condition
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    createFilter  ${atipSession}  ${geoMultipleApp}
    rulePush  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    checkFilterExistsInTopStats  ${atipSession}  ${geoMultipleApp}  ${timeInterval}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig1}
    ${stat}  PieStats.getTopStats  ${atipSession}  ${statsTypeApp}  ${timeInterval}
    #Validate traffic ic correctly processed by ATIP
    checkSpecificFieldStat  ${stat}  ${bpsTestConfig1}  ${bpsComponentsStats}  ${atipSession}  ${statsTypeApp}  ${atipSessions}
    #Compare BPS sessions with Top Filters stats
    ${bpsSessionsComp1}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    ${bpsSessionsComp3}  getStatItem  ${bpsComponentsStats['appsim_3']}  ${bpsSessions}
    ${bpsSessions}  Evaluate  ${bpsSessionsComp1} + ${bpsSessionsComp3}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${geoMultipleApp}  ${bpsSessions}  ${timeInterval}

Filter - geo and group condition
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    createFilter  ${atipSession}  ${geoAndAppGroup}
    rulePush  ${atipSession}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    checkFilterExistsInTopStats  ${atipSession}  ${geoAndAppGroup}  ${timeInterval}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig1}
    ${stat}  PieStats.getTopStats  ${atipSession}  ${statsTypeApp}  ${timeInterval}
    #Validate traffic ic correctly processed by ATIP
    checkSpecificFieldStat  ${stat}  ${bpsTestConfig1}  ${bpsComponentsStats}  ${atipSession}  ${statsTypeApp}  ${atipSessions}
    #Compare Netflix BPS sessions with Top Filters stats
    ${bpsSessionsComp2}  getStatItem  ${bpsComponentsStats['appsim_2']}  ${bpsSessions}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${geoAndAppGroup}  ${bpsSessionsComp2}  ${timeInterval}

Filter - custom app condition
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    uploadCustomAppFile  ${atipSession}  ${EXECDIR}/../../../frwk/src/atipAutoFrwk/data/custapps/ixiacom.com.xml
    createFilter  ${atipSession}  ${customFilter}
    rulePush  ${atipSession}
    checkNpRules  ${atipSession}  2  ${envVars.atipSlotNumber[0]}
    GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig7}
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${statsTypeDyn}  ${timeInterval}
    checkTargetDoesNotExistInTopStats  ${atipStats}  ${strIxiaCom}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig7}
    ${totalSessions}  getStatItem  ${bpsComponentsStats['appsim_7']}  ${bpsSessions}
    ${stat}  PieStats.getTopStats  ${atipSession}  ${statsTypeApp}  ${timeInterval}
    #Validate traffic ic correctly processed by ATIP
    checkSpecificFieldStat  ${stat}  ${bpsTestConfig7}  ${bpsComponentsStats}  ${atipSession}  ${statsTypeApp}  ${atipSessions}
    checkFilterExistsInTopStats  ${atipSession}  ${customFilter}  ${timeInterval}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${customFilter}  ${totalSessions}  ${timeInterval}
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}


Filter - app and group condition - negative
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    ${response}=  createFilter  ${atipSession}  ${appAndAppGroup}  ${expectedStatus}
    Log  ${response}
    rulePush  ${atipSession}

*** Keywords ***

Suite setup
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    configureAtip  ${atipSession}  ${atipConfig}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}





