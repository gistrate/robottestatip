from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations
import time
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.atip.Capture import Capture
from atipAutoFrwk.services.atip.CaptureService import CaptureService
from atipAutoFrwk.services.atip.FileService import FileService


class CaptureLib(object):
    def runBPSAndCapture(self, webApiSession, bpsSession, bpsConfig, env, filterId, captureTime):

        BpsLogin.login(bpsSession)
        PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)
        testId = BpsOperations.startBps(bpsSession, bpsConfig.testName, env.bpsPortGroup)

        #allPCAPS, path = Capture.getCaptureFileList(webApiSession)

        # delete capture files from ATIP
        Capture.deleteCaptureFiles(webApiSession)

        # wait for traffic to start
        bpsStatus = BpsOperations.getBpsTestProgress(bpsSession, testId)
        while bpsStatus == -1:
            print('Sleeping 1s')
            time.sleep(1)
            bpsStatus = BpsOperations.getBpsTestProgress(bpsSession, testId)

        # start capture
        Capture.controlCapture(webApiSession, filterId, True)

        # wait for capture to finish or stop it if capture time expired
        for i in range(int(captureTime)+1):
            status = Capture.captureRunningStatus(webApiSession)
            print('Waiting capture to finish for {}s'.format(i))
            if status and (i < int(captureTime)):
                time.sleep(1)
            elif i >= int(captureTime):
                Capture.controlCapture(webApiSession, filterId, False)
            else:
                break

        # stop traffic
        BpsOperations.forceStopBpsTestIfRunning(bpsSession)


    @classmethod
    def getLatestCaptureAndUnzip(cls, webApiSession):

        # get latest pcap zip file from ATIP
        zipLocation = CaptureService.getLatestCaptureFile(webApiSession)

        # unzip pcap file and delete zip file
        pcapFolder = FileService.unzipFileToFolder(zipLocation)

        return pcapFolder

    @classmethod
    def validatePCAPAddress(cls, webApiSession, filterIp):

        pcapFilter = 'not ip.host matches "{}"'.format(filterIp)
        pcapFolder = cls.getLatestCaptureAndUnzip(webApiSession)
        resultPCAP = FileService.analyzePCAP(pcapFolder, pcapFilter)

        # in Python 3, pyshark FileCapture length is not calculated correctly, in fact is always 0; here I validate that PCAP filtering contains elements or not, calculating length
        checkPCAP = len([packet for packet in resultPCAP])

        cls.removePcapAndFolder(webApiSession, pcapFolder)

        if checkPCAP:
            raise Exception('PCAP contains unexpected data.')


    @classmethod
    def validatePCAPHost(cls, webApiSession, filterHost):

        pcapFilter = 'http.host != "{}"'.format(filterHost)
        pcapFolder = cls.getLatestCaptureAndUnzip(webApiSession)
        resultPCAP = FileService.analyzePCAP(pcapFolder, pcapFilter)

        # in Python 3, pyshark FileCapture length is not calculated correctly, in fact is always 0; here I validate that PCAP filtering contains elements or not, calculating length
        checkPCAP = len([packet for packet in resultPCAP])

        cls.removePcapAndFolder(webApiSession, pcapFolder)

        if checkPCAP:
            raise Exception('PCAP contains unexpected data.')


    @classmethod
    def validateFileInfo(cls, webApiSession, setCaptureSize):
        pcapFolder = cls.getLatestCaptureAndUnzip(webApiSession)
        fileSize, fileName = FileService.getFileInfo(pcapFolder)
        print('Capture file name is {}.'.format(fileName))
        print('Capture length is {} bits.'.format(fileSize))

        # verify offset between capture file and capture size already set
        diff = abs(int(setCaptureSize) - fileSize)

        cls.removePcapAndFolder(webApiSession, pcapFolder)

        if diff > int(setCaptureSize)/2:
            raise Exception('PCAP size difference')
        else:
            print('Length difference of {} bits is in acceptable range (configured capture limit is {}).'.format(diff, setCaptureSize))

    @classmethod
    def removePcapAndFolder(cls, webApiSession, pcapFolder):

        # remove PCAP folder
        FileService.deleteFolder(pcapFolder)

        # delete capture files from ATIP
        Capture.deleteCaptureFiles(webApiSession)


