from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType

class SuiteConfig(object):

    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)

    def configureGeoFilter(self, webApiSession, country):

        Filters.deleteAllFilters(webApiSession)
        filterName = country + '_filter'

        filterConfig = FilterConfig()
        filterConfig.name = filterName
        for member in GeoLocation.__members__.values():
            if member.countryCode == country:
                FilterConditionConfig.addGeoCondition(filterConfig, member)
        Filters.createFilter(webApiSession, filterConfig)
        Filters.rulePush(webApiSession)

    def configureAppFilter(self, webApiSession, appName):

        Filters.deleteAllFilters(webApiSession)
        filterName = appName + '_filter'

        filterConfig = FilterConfig()
        filterConfig.name = filterName
        for member in ApplicationType.__members__.values():
            if member.appName == appName:
                FilterConditionConfig.addAppCondition(filterConfig, member)
        Filters.createFilter(webApiSession, filterConfig)
        Filters.rulePush(webApiSession)
