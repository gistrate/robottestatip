*** Settings ***
Documentation  Capture test suite.

Variables  SuiteConfig.py

Library    scripts.functional.capture.SuiteConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.Capture
Library    atipAutoFrwk.services.atip.CaptureService
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    BuiltIn
Library    CaptureLib.py

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP
Suite Teardown     atipLogout.Logout  ${atipSession}


*** Test Cases ***

# verify Geos   bpsTest                           country    filterIP       duration
TC000970218.1   Capture-Facebook-pakistan_10min     PK    14.192.128.*        600
    [Template]  Verify GEO
TC000970218.2   Capture-ESPN-greece_10min           GR    31.22.*.*           600
    [Template]  Verify GEO
TC000970218.3   Capture-Netflix-japan_10min         JP    103.241.104.*       600
    [Template]  Verify GEO

# verify Apps   bpsTest                           appName    filterHost     duration
TC000969871.1   Capture-Facebook-pakistan_10min   facebook   facebook.com     600
    [Template]  Verify APP
TC000969871.2   Capture-ESPN-greece_10min         espn       espn.com         600
    [Template]  Verify APP
TC000969871.3   Capture-Netflix-japan_10min       netflix    netflix.com      600
    [Template]  Verify APP

# verify Size   bpsTest                                                     duration   setCaptureSize
TC000970423     Capture_verifyPackets_5min                                    300        1000000
    [Template]  Verify SIZE

*** Keywords ***

Verify GEO
    [Arguments]  ${bpsFile}  ${country}  ${filterIP}  ${duration}

    atipLogin.Login  ${atipSession}

    configureGeoFilter  ${atipSession}  ${country}

    ${filter}=  Get Filter ID

    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  ${duration}

    runBPSAndCapture  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  ${envVars}  ${filter}  180

    # download capture file and validate content
    validatePCAPAddress  ${atipSession}  ${filterIP}


Verify APP
    [Arguments]  ${bpsFile}  ${appName}  ${filterHost}  ${duration}

    atipLogin.Login  ${atipSession}
    configureAppFilter  ${atipSession}  ${appName}

    # get filter id
    ${filter}=  Get Filter ID

    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  ${duration}

    runBPSAndCapture  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  ${envVars}  ${filter}  30

    # download capture file and validate content
    validatePCAPHost  ${atipSession}  ${filterHost}


Verify SIZE
    [Arguments]  ${bpsFile}  ${duration}  ${setCaptureSize}

    atipLogin.Login  ${atipSession}
    configureGeoFilter  ${atipSession}  PK

    # get filter id
    ${filter}=  Get Filter ID

    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  ${duration}

    # set capture size to 1MB
    editCaptureSize  ${atipSession}  ${filter}  1000000

    runBPSAndCapture  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  ${envVars}  ${filter}  180

    # verify difference between PCAP file and capture size set
    validateFileInfo  ${atipSession}  ${setCaptureSize}


Get Filter ID
    ${filterList}=  getAllFilters  ${atipSession}
    ${rule}=  Evaluate  ${filterList}.get('rule')[0]
    ${filterId}=  Evaluate  ${rule}.get('id')

    [return]  ${filterId}


Configure ATIP
    atipLogin.Login  ${atipSession}

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
