from http import HTTPStatus

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.DedupGlobalConfig import DedupGlobalConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.ApplicationGroupType import AppGroupType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.webApi.WebApiSession import WebApiSession



class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    timeInterval = TimeInterval.HOUR
    statsType = StatsType.APPS
    totalPkts = TopStats.TOTAL_PKTS
    totalSessions = TopStats.TOTAL_COUNT
    # ATIP config
    dedupFilter = FilterConfig("DedupFilter")
    FilterConditionConfig.addAppCondition(dedupFilter, ApplicationType.NETFLIX)
    dedupFilter.forward = True
    dedupFilter.transmitIds = [100]



    # Test: Validate Filter Name
    bpsTestConfig1 = BpsTestConfig('lowRate-bw-GET-response100_2CONNs', 80)

    atipConfig = AtipConfig()
    dedupConfig = DedupGlobalConfig("844600", True, "NONE")
    atipConfig.dedup = dedupConfig
    atipConfig.addFilters(dedupFilter)



