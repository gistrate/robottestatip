*** Settings ***
Documentation  Dedup basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.Dedup
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.webApi.atip.CustomAppConfig
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.Apps
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    atipAutoFrwk.config.atip.filter.FilterConditionConfig
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.atip.stats.ValidateDedupStatsService
Library    atipAutoFrwk.services.GeneralService  WITH NAME  GeneralService
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    Telnet   30 seconds
Resource   DedupConfigLib.robot


Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Dedup Setup
Suite Teardown     Dedup Teardown


*** Test Cases ***
TC000922719 Dedup - basic test
    ${testId} =  set variable  TC000922719
    atipAutoFrwk.webApi.atip.System.resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture traffic  ${testId}  ${bpsTestConfig1}
    ${noDedupPkts}=  Get duplicates count  ${testId}
    should be equal as integers  0  ${noDedupPkts}
    ${debugStats}=  getDedupStatsFromDebugPage  ${atipSession}
    ${stat}  ListStats.getTopStats  ${atipSession}  ${statsType}  ${timeInterval}
    ${totalPkts}  getAtipStat  ${stat}  ${totalPkts}
    should be equal as integers  ${debugStats['PacketsPassed']}  ${totalPkts}
    validateDedupVariation  ${atipSession}  0
    should be equal as integers  ${debugStats['UnhandledPackets']}  0

TC000922716 Dedup - negative test
    ${testId} =  set variable  TC000922716
    ${dedupConfig.enabled} =  set variable  ${false}
    updateDedup  ${atipSession}  ${dedupConfig}
    atipAutoFrwk.webApi.atip.System.resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture traffic  ${testId}  ${bpsTestConfig1}
    ${noDedupPkts}=  Get duplicates count  ${testId}
    ${stat}  ListStats.getTopStats  ${atipSession}  ${statsType}  ${timeInterval}
    ${atipSessions}  getAtipStat  ${stat}  ${totalSessions}
    ${expectedValue} =  evaluate  ${atipSessions}*2
    should be equal as integers  ${noDedupPkts}  ${expectedValue}





*** Keywords ***
Dedup Setup
    Create Dedup Config
    sshRemoveCoreFilesInNP  ${envVars}
    configureAtip  ${atipSession}  ${atipConfig}

Dedup Teardown
    Create NTO Config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    logout  ${atipSession}

Connect on Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}     prompt=$
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Write  su
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt

Run and Capture traffic
    [Arguments]  ${captureFileName}  ${bpsConfig}
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${captureFileName}.pcap -n -B 30720
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    Write Control Character     BRK
    Read Until Prompt

Get duplicates count
    [Arguments]  ${captureFileName}
    ${captureValue}=  Execute Command  tshark -r ${captureFileName}.pcap -q -z io,stat,30,\"COUNT(tcp.analysis.retransmission) tcp.analysis.retransmission\"
    Log  ${captureValue}
    ${matches}=  get regexp matches  ${captureValue}  \\S+\\s+<>\\s+\\S+\\s+\\|\\s+(\\d+)\\s+\\|  1
    ${dedupPkts}=  Convert To Integer  ${matches[0]}
    [return]  ${dedupPkts}









