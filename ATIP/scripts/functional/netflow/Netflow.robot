*** Settings ***
Documentation  Netflow Basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.services.atip.utility.IxFlowParsing
Library    atipAutoFrwk.services.atip.SystemService
Library    Collections
Library    Telnet   30 seconds
Library    BuiltIn
Library    atipAutoFrwk.webApi.atip.Netflow
Library    String
Library    atipAutoFrwk.services.TestCondition
Library    atipAutoFrwk.webApi.atip.EngineInfo
Resource    NetflowUtils.robot

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Suite Setup
Suite Teardown     Clean ATIP

*** Test Cases ***
TC000873556 Netflow filter with app condition

    [Setup]  Configure Netflow Filter  ${netflowAppFilter}
    ${testId} =  Set variable  TC000873556
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.l7-application-name"
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    dictionary Should Contain Key    ${dict}    amazon
    dictionary Should not Contain Key    ${dict}    facebook
    dictionary Should not Contain Key    ${dict}    netflix
    ${value1} = 	Get From Dictionary 	${dict}  amazon
    should be equal as integers  ${records}  ${value1}
    Execute Command  rm ${testId}*



TC000873554 Netflow filter with geo condition

    [Setup]  Configure Netflow Filter  ${netflowGeoFilter}
    ${testId} =  Set variable  TC000873554
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.source-ip-country-name,cflow.pie.ixia.source-ip-country-code"
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    ${expectedValue} =  evaluate  ${records}/2
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  France
    ${value2} = 	Get From Dictionary 	${dict}  FR
    should be equal as integers  ${value1}  ${expectedValue}
    should be equal as integers  ${value2}  ${expectedValue}
    Execute Command  rm ${testId}*


TC000873557 Netflow filter with geo and app condition

    [Setup]  Configure Netflow Filter  ${netflowGeoAppFilter}
    ${testId} =  Set variable  TC000873557
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.source-ip-country-name,cflow.pie.ixia.l7-application-name"
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    ${expectedValue} =  evaluate  ${records}/2
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  France
    ${value2} = 	Get From Dictionary 	${dict}  amazon
    should be equal as integers  ${expectedValue}  ${value1}
    should be equal as integers  ${records}  ${value2}
    Execute Command  rm ${testId}*



TC000873558 Netflow filter with geo and app condition - multiple collectors

    ${testId} =  Set variable  TC000873557
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.source-ip-country-name,cflow.pie.ixia.l7-application-name"
    enable collector  ${secondCollector}
    Configure Netflow Filter  ${netflowGeoAppFilter2}
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId}  ${bpsTestConfig}
    ${output1}=  Analyze traffic  ${testId}  source-ip-country-name  France  ${firstCollectorIP}
    ${output2}=  Analyze traffic  ${testId}  l7-application-name  amazon  ${firstCollectorIP}
    ${output3}=  Analyze traffic  ${testId}  source-ip-country-name  France  ${secondCollectorIP}
    ${output4}=  Analyze traffic  ${testId}  l7-application-name  amazon  ${secondCollectorIP}
    should be equal as integers    ${output1}  ${output3}
    should be equal as integers    ${output2}  ${output4}
    ${stats1} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    ${stats2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorIP}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} =  Get From Dictionary 	${dict}  France
    ${value2} = 	Get From Dictionary 	${dict}  amazon
    dictionary Should not Contain Key    ${dict}    facebook
    dictionary Should not Contain Key    ${dict}    netflix
    ${expectedValue1} =  evaluate  ${stats1}/2+${stats2}/2
    ${atipRecords} =  evaluate  ${stats1}+${stats2}
    should be equal as integers  ${atipRecords}  ${value2}
    should be equal as integers  ${expectedValue1}  ${value1}
    disable collector  ${secondCollector}
    Execute Command  rm ${testId}*

TC000873552 Netflow packets - no filter - one collector

    ${testId} =  Set variable  TC000873552
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.source-ip-country-name,cflow.pie.ixia.l7-application-name"
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId}  ${bpsTestConfig}
    ${records} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    ${expectedValue1} =  evaluate  ${records}/2
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} = 	Get From Dictionary 	${dict}  France
    ${app1} = 	Get From Dictionary 	${dict}  amazon
    ${app2} = 	Get From Dictionary 	${dict}  facebook
    ${app3} = 	Get From Dictionary 	${dict}  netflix
    ${expectedValue2} =  evaluate  ${app1}+${app2}+${app3}
    should be equal as integers  ${expectedValue1}  ${value1}
    should be equal as integers  ${records}  ${expectedValue2}
    Execute Command  rm ${testId}*

TC000873553 Netflow records - no filter - multiple collectors

    ${testId} =  Set variable  TC000873553
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.source-ip-country-name,cflow.pie.ixia.l7-application-name"
    resetStats  ${atipSession}
    deleteAllFilters  ${atipSession}
    enable collector  ${secondCollector}
    Connect on Linux Box
    Run and Capture Traffic  ${testId}  ${bpsTestConfig}
    ${output1}=  Analyze traffic  ${testId}  source-ip-country-name  France  ${firstCollectorIP}
    ${output2}=  Analyze traffic  ${testId}  l7-application-name  amazon  ${firstCollectorIP}
    ${output3}=  Analyze traffic  ${testId}  source-ip-country-name  France  ${secondCollectorIP}
    ${output4}=  Analyze traffic  ${testId}  l7-application-name  amazon  ${secondCollectorIP}
    should be equal as integers    ${output1}  ${output3}
    should be equal as integers    ${output2}  ${output4}
    ${stats1} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    ${stats2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorIP}
    ${response}=  Execute Command  python parsing.py ${testId}.pcap ${testId}.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    Log  ${dict}
    ${value1} =  Get From Dictionary 	${dict}  France
    ${app1} = 	Get From Dictionary 	${dict}  amazon
    ${app2} = 	Get From Dictionary 	${dict}  facebook
    ${app3} = 	Get From Dictionary 	${dict}  netflix
    ${expectedValue1} =  evaluate  ${stats1}/2+${stats2}/2
    ${expectedValue2} =  evaluate  ${app1}+${app2}+${app3}
    ${atipRecords} =  evaluate  ${stats1}+${stats2}
    should be equal as integers  ${expectedValue2}  ${atipRecords}
    should be equal as integers  ${expectedValue1}  ${value1}
    disable collector  ${secondCollector}
    Execute Command  rm ${testId}*

TC000969052 Netflow rate sampling
    [Tags]  hardware
    ${testId} =  Set variable  TC000969052
    deleteAllFilters  ${atipSession}
    Update Active Flow Timeout  ${netflowGlobalConfig}
    enable collector  ${secondCollector}
    Configure Rate Sampling  ${firstCollector}
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture traffic  ${testId}  ${bpsTestConfig2}
    ${records1} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    ${records2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorIP}
    ${atipStats}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}
    ${stat} =  getAtipStatsValues  ${atipStats}  ${strFacebook}  ${atipSessions}
    Log   ${records1}
    Log   ${records2}
    ${sessions} =  Evaluate   2 * ${stat}
    Validate Sampled Records  30  ${sessions}  ${records1}
    disable collector  ${secondCollector}
    Execute Command  rm ${testId}*

TC000969052 Netflow rate sampling - less connections
    [Tags]  virtual
    ${testId} =  Set variable  TC000969052
    deleteAllFilters  ${atipSession}
    Update Active Flow Timeout  ${netflowGlobalConfig}
    enable collector  ${secondCollector}
    Configure Rate Sampling  ${firstCollector}
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture traffic  ${testId}  ${bpsTestConfig3}
    ${records1} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorIP}
    ${records2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorIP}
    ${atipStats}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}
    ${stat} =  getAtipStatsValues  ${atipStats}  ${httpService}  ${atipSessions}
    Log   ${records1}
    Log   ${records2}
    ${sessions} =  Evaluate   2 * ${stat}
    Validate Sampled Records  30  ${sessions}  ${records1}
    disable collector  ${secondCollector}
    Execute Command  rm ${testId}*

*** Keywords ***
Suite Setup
    Configure Netfow on ATIP
    Connect on Linux Box
    ${command} =  copyFileToLinux
    Write  ${command}

Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    logout  ${atipSession}





















