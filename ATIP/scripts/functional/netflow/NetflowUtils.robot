*** Keywords ***
Configure Netfow on ATIP
    ConfigureAtip  ${atipSession}  ${atipConfig}

Configure Netflow Filter
    [Arguments]  ${filterObject}
    deleteAllFilters  ${atipSession}
    createFilter  ${atipSession}  ${filterObject}
    rulePush  ${atipSession}

Connect on Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}     prompt=$
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Write  su
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt

Run and Capture traffic
    [Arguments]  ${captureFileName}  ${bpsConfig}
    Write  tcpdump -U -i ${envVars.netflowCollectorConnectionConfig.port} -w ${captureFileName}.pcap -n -G 300 -W 1 -B 30720
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    Set timeout  5min
    Read Until Prompt

Analyze traffic
    [Arguments]  ${testId}  ${filterType}  ${filterValue}  ${collectorIp}
    Filter Capture IxFlow  ${testId}  ${filterType}  ${filterValue}  ${collectorIp}  ${testId}_${collectorIp}
    ${output}=  Extract Capture Matches  ${testId}_${collectorIp}
    [return]  ${output}

Validate Information Elements
    [Arguments]  ${testId}  ${collectorIp}  ${informElemToContain}

    @{informElemList} =  create list  srcCountryCode  srcCountryName  srcRegionCode  srcRegionName  srcCityName
                 ...    srcLatitude  srcLongitude  dstCountryCode  dstCountryName  dstRegionCode
                 ...    dstRegionName  dstCityName  dstLatitude  dstLongitude  deviceId  deviceName
                 ...    browserId  browserName  l7appID  l7appName  connEncryptType  encryptionCipher
                 ...    encryptionKeyLen  userAgent  httpHostName  dnsTxt  uri  dnsQuestionNames  dnsClasses


    Log  ${informElemList}
    Log  ${informElemToContain}

    :FOR    ${element}    IN    @{informElemToContain}
        \  Log  ${element}
        \  Filter Capture Information Element  ${testId}  ${element}  ${collectorIp}  ${testId}_${collectorIp}_${element}
        \  ${output}=  Extract Capture Matches  ${testId}_${collectorIp}_${element}
        \  should not be equal as integers  0  ${output}
        \  Remove Values From List  ${informElemList}  ${element}


    :FOR    ${element}    IN    @{informElemList}
        \  Log  ${element}
        \  Filter Capture Information Element  ${testId}  ${element}  ${collectorIp}  ${testId}_${collectorIp}_${element}
        \  ${output}=  Extract Capture Matches  ${testId}_${collectorIp}_${element}
        \  should be equal as integers  0  ${output}
    [return]  ${output}




Filter Capture IxFlow
    [Arguments]  ${testId}  ${filterType}  ${filterValue}  ${collectorIp}  ${resultFileName}  ${useString}= True
    ${tsharkCommand} =  Set Variable  ${EMPTY}
    ${tfilter} =  Set Variable  ${EMPTY}
    ${tfilter}=  Set Variable if  ${useString} == True    cflow and ip.addr == ${collectorIp} and cflow.pie.ixia.${filterType} == \"${filterValue}\" and !(icmp)  cflow and ip.addr == ${collectorIp} and cflow.pie.ixia.${filterType} == ${filterValue} and !(icmp)
    ${tsharkCommand} =  Catenate  tshark -Y ' ${tfilter}  ' -r ${testId}.pcap -T fields -e frame.number -e frame.len -e eth.src -e ip.src > ${resultFileName}.result
    Log  ${tsharkCommand}
    ${filterCapture} =  Execute Command  ${tsharkCommand}
    [return]  ${filterCapture}

Filter Capture Information Element
    [Arguments]  ${testId}  ${element}  ${collectorIp}  ${resultFileName}
    ${tsharkCommand} =  Set Variable  ${EMPTY}
    ${tfilter} =  Set Variable  ${EMPTY}
    ${tfilter}=  Set Variable  cflow and ip.addr == ${collectorIp} and cflow.information_element_name == ${element} and !(icmp)
    ${tsharkCommand} =  Catenate  tshark -Y ' ${tfilter}  ' -r ${testId}.pcap -T fields -e frame.number -e frame.len -e eth.src -e ip.src > ${resultFileName}.result
    Log  ${tsharkCommand}
    ${filterCapture} =  Execute Command  ${tsharkCommand}
    [return]  ${filterCapture}

Extract Capture Matches
    [Arguments]  ${resultFileName}
    ${captureValue}=  Execute Command  wc -l < ${resultFileName}.result
    ${matches}=  get regexp matches  ${captureValue}  \\d+
    Log  ${matches}
    ${capturedPkts}=  Convert To Integer  ${matches[0]}
    [return]  ${capturedPkts}

Configure Rate Sampling
    [Arguments]     ${collectorToUpdate}
    ${collectorToUpdate.unit}=   set variable  Samples
    ${collectorToUpdate.rate}=   set variable  ${30}
    updateCollectorConfig  ${atipSession}  ${collectorToUpdate}

Update Active Flow Timeout
    [Arguments]     ${netflowGlobalConfig}
    ${netflowGlobalConfig.timeout}=   set variable  ${0}
    updateGlobalConfig  ${atipSession}  ${netflowGlobalConfig}  ${httpStatusOK}

Enable Combine Bidirectional
    [Arguments]     ${netflowGlobalConfig}
    ${netflowGlobalConfig.biFlows}=   set variable  True
    updateGlobalConfig  ${atipSession}  ${netflowGlobalConfig}  ${httpStatusOK}

Disable Combine Bidirectional
    [Arguments]     ${netflowGlobalConfig}
    ${netflowGlobalConfig.biFlows}=   set variable  False
    updateGlobalConfig  ${atipSession}  ${netflowGlobalConfig}  ${httpStatusOK}

Validate Sampled Records
     [Arguments]     ${sampling}  ${expectedValue}  ${records}
     ${result} =  Evaluate  round(${sampling} * ${expectedValue} / 100)
     ${result1} =  CONVERT TO NUMBER  ${result}
     Should be true  ${records} <= ${result1}

Disable collector
    [Arguments]     ${collectorToUpdate}
    ${collectorToUpdate.enabled}=   set variable  False
    updateCollectorConfig  ${atipSession}  ${collectorToUpdate}

Enable collector
    [Arguments]     ${collectorToUpdate}
    ${collectorToUpdate.enabled}=   set variable  True
    updateCollectorConfig  ${atipSession}  ${collectorToUpdate}

Extract capture Per NP Exporter IP
    [Arguments]     ${testId}  ${exporterIP}
    ${tsharkCommand} =  Set Variable  ${EMPTY}
    ${tfilter} =  Set Variable  ${EMPTY}
    ${tfilter}=  Set Variable  ip.addr == ${exporterIP}
    ${outCap}=  set variable  ${testId}_${exporterIP}
    ${tsharkCommand} =  Catenate  tshark -Y ' ${tfilter}  ' -r ${testId}.pcap -w '${outCap}.pcap'
    Log  ${tsharkCommand}
    ${filterCapture} =  Execute Command  ${tsharkCommand}
    [return]  ${filterCapture}





