from http import HTTPStatus

import jsonpickle

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.Collector import Collector
from atipAutoFrwk.config.atip.NetflowCardConfig import NetflowCardConfig
from atipAutoFrwk.config.atip.NetflowCollectorConfig import NetflowCollectorConfig
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.NetflowStats import NetflowStats
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.services.atip.NetflowService import NetflowService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Netflow import Netflow
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
import ipaddress


class SuiteConfig(StatsDefaultConfig):
    httpService = ApplicationType.HTTP_80.appName
    maximizedDashboard = DashboardType.MAXIMIZED
    netflowPkts = NetflowStats.TOTAL_PKTS
    netflowRecords = NetflowStats.DATA_RECORDS
    statsType = StatsType.APPS
    timeInterval = TimeInterval.HOUR
    bpsDuration = 400
    firstCollectorIP = StatsDefaultConfig.envVars.netflowCollectorConnectionConfig.host
    secondCollectorIP = str(ipaddress.IPv4Address(firstCollectorIP) + 1)
    # ATIP config
    atipConfig = AtipConfig()
    netflowGlobalConfig = NetflowConfig()
    netflowGlobalConfig.enabled = True
    netflowGlobalConfig.version = 10
    atipConfig.setNetflowGlobal(netflowGlobalConfig)
    netflowCards = NetflowCardConfig()
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].enabled = True
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].odid = 100
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].ipMethod = 'STATIC'
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].dns = '8.8.8.8'
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].netmask = '255.255.255.0'
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].gw = '192.168.30.100'
    netflowCards.cardList[StatsDefaultConfig.envVars.atipSlotNumber[0]].ip_addr = '192.168.30.1'
    atipConfig.setNetflowCards(netflowCards)
    firstCollector = Collector(0, 100, 'Samples', True, 'UDP', 4739, firstCollectorIP)
    secondCollector = Collector(1, 100, 'Samples', True, 'UDP', 4739, secondCollectorIP)
    netflowCollectors = NetflowCollectorConfig()
    netflowCollectors.collectorList[0]=firstCollector
    atipConfig.setNetflowCollectors(netflowCollectors)
    netflowAppFilter = FilterConfig("testAppFilter")
    FilterConditionConfig.addAppCondition(netflowAppFilter, ApplicationType.AMAZON)
    netflowAppFilter.netflow=True
    netflowAppFilter.collectors=[0]
    netflowGeoFilter = FilterConfig("testGeoFilter")
    FilterConditionConfig.addGeoCondition(netflowGeoFilter, GeoLocation.FRANCE)
    netflowGeoFilter.netflow = True
    netflowGeoFilter.collectors = [0]
    netflowGeoAppFilter = FilterConfig("testGeoAppFilter")
    FilterConditionConfig.addAppCondition(netflowGeoAppFilter, ApplicationType.AMAZON)
    FilterConditionConfig.addGeoCondition(netflowGeoAppFilter, GeoLocation.FRANCE)
    netflowGeoAppFilter.netflow = True
    netflowGeoAppFilter.collectors = [0]
    netflowGeoAppFilter2 = FilterConfig("testGeoAppMoreCollectors")
    FilterConditionConfig.addAppCondition(netflowGeoAppFilter2, ApplicationType.AMAZON)
    FilterConditionConfig.addGeoCondition(netflowGeoAppFilter2, GeoLocation.FRANCE)
    netflowGeoAppFilter2.netflow = True
    netflowGeoAppFilter2.collectors = [0,1]
    bpsTestConfig = BpsTestConfig('automation_3_static_apps', 80)
    bpsTestConfig2 = BpsTestConfig('Netflow_TCP_100K_CPS_static', 80)
    bpsTestConfig3 = BpsTestConfig('NetFlow_TCP_1KCPS_v2_5min', 80)






