*** Settings ***

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.services.GeneralService  WITH NAME  General
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.atip.stats.ValidateSSLStatsService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    Collections
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.webApi.atip.ConnTableGlobalConfig
Library    atipAutoFrwk.webApi.atip.ConfigureStats
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.webApi.atip.stats.Filters
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    atipAutoFrwk.webApi.atip.PacketBufferGlobalConfig
Library    atipAutoFrwk.webApi.atip.MplsParsing
Library    atipAutoFrwk.webApi.atip.TcpMode
Library    atipAutoFrwk.services.atip.NPService




Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Clear Config

*** Test Cases ***

Test_HashDistribution (without SSL) - ip_port
    [Tags]  hardware
    Restart ATIP
    ${globalConfig0}=  getConnTableGlobalConfig  ${atipSession}  ${defaultGroupId}
    ${globalConfig0.tuple_hash_method}=  set variable  ip_port
    updateConnTableGlobalConfig  ${atipSession}  ${globalConfig0}
    Log  Run BPS traffic
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigHash}
    Log  ${rxPkts}
    ${rxPktsNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${rxPkts}
    should not be equal as integers  ${rxPktsNP1}  0
    ${newConnNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${newConn}
    should not be equal as integers  ${newConnNP1}  0
    ${qDepthNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${rxQueueDepth}
    should be equal as integers  ${qDepthNP1}  0
    ${qDropsNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${rxQueueDrops}
    should be equal as integers  ${qDropsNP1}  0
    resetConnTableGlobalConfig  ${atipSession}

Test_HashDistribution (without SSL) - ip
    [Tags]  hardware
    Restart ATIP
    ${globalConfig0}=  getConnTableGlobalConfig  ${atipSession}  ${defaultGroupId}
    ${globalConfig0.tuple_hash_method}=  set variable  ip
    updateConnTableGlobalConfig  ${atipSession}  ${globalConfig0}
    ${dropPcnt}  ${rxDropPkts}  ${rxDropBytes}=  getDropPercentFromDebugPage  ${atipSession}  ${envVars}  ${index}
    Log  Run BPS traffic
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigHash}
    ${dropPcnt}  ${rxDropPkts}  ${rxDropBytes}=  getDropPercentFromDebugPage  ${atipSession}  ${envVars}  ${index}
    Log  ${dropPcnt}
    should not be equal as integers  ${dropPcnt}  0
    Log  ${rxDropPkts}
    should not be equal as integers  ${rxDropPkts}  0
    Log  ${rxDropBytes}
    should be equal as integers  ${rxDropBytes}  0
    ${rxPktsNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${rxPkts}
    should not be equal as integers  ${rxPktsNP1}  0
    ${newConnNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${newConn}
    should not be equal as integers  ${newConnNP1}  0
    ${qDepthNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${rxQueueDepth}
    should be equal as integers  ${qDepthNP1}  0
    ${qDropsNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${rxQueueDrops}
    should not be equal as integers  ${qDropsNP1}  0
    resetConnTableGlobalConfig  ${atipSession}

Test_HashDistribution (with SSL) - ip_port
    [Tags]  hardware
    Restart ATIP
    resetConnTableGlobalConfig  ${atipSession}
    deleteAllCerts  ${atipSession}
    updateSslSettings  ${atipSession}  True  ${defaultGroupId}
    updateSslSettings  ${atipSession}  True  ${defaultGroupId}
    ${globalConfig0}=  getConnTableGlobalConfig  ${atipSession}  ${defaultGroupId}
    ${globalConfig0.tuple_hash_method}=  set variable  ip_port
    updateConnTableGlobalConfig  ${atipSession}  ${globalConfig0}
    Log  Run BPS traffic
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigHash}
    ${rxPktsNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${rxPkts}
    should not be equal as integers  ${rxPktsNP1}  0
    ${newConnNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${newConn}
    should not be equal as integers  ${newConnNP1}  0
    ${qDepthNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${rxQueueDepth}
    should be equal as integers  ${qDepthNP1}  0
    ${qDropsNP1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${rxQueueDrops}
    should be equal as integers  ${qDropsNP1}  0
    resetConnTableGlobalConfig  ${atipSession}
    deleteAllCerts  ${atipSession}
    updateSslSettings  ${atipSession}  False  ${defaultGroupId}

Test_QuickResumes (ip_port)  ip_port
    [Tags]  hardware
    [Template]  Validate QuickResumes

Test_QuickResumes (ip)  ip
    [Tags]  hardware
    [Template]  Validate QuickResumes


TC001025649 Connection table timeout - TCP  ${bpsTestConfigConnTCP}  50  TCP
    [Template]  Validate ConnTimeouts

TC001025652 Connection table timeout - UDP  ${bpsTestConfigConnUDP}  40  UDP
    [Template]  Validate ConnTimeouts

*** Keywords ***

Restart ATIP
    resetAtipAndWaitUntilNPReady  ${atipSession}  ${envVars}

Clear Config
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}



Validate ConnTimeouts
    [Arguments]  ${bpsTest}  ${expireTime}  ${protocol}
    resetStats  ${atipSession}
    ${newConn1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${newConn}
    ${timeouts1}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${timeouts}
    resetConnTableGlobalConfig  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest}
    ${newConn2}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${newConn}
    ${timeouts2}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${timeouts}
    ${valueTCP}=  Evaluate  (${newConn2}-${newConn1})/2
    ${valueUDP}=  Evaluate  (${newConn2}-${newConn1})
    ${expectedConn} =    Set Variable If    "${protocol}" == "TCP"    ${valueTCP}    ${valueUDP}
    ${expectedTimeouts}=  Evaluate  ${timeouts2}-${timeouts1}
    should be equal as integers  ${expectedConn}  ${expectedTimeouts}
    ${globalConfig0}=  getConnTableGlobalConfig  ${atipSession}  ${defaultGroupId}
    ${globalConfig0.inactive_expire_time_sec_tcp} =    Set Variable If    "${protocol}" == "TCP"    ${expireTime}    30
    ${globalConfig0.inactive_expire_time_sec_nontcp} =    Set Variable If    "${protocol}" == "UDP"    ${expireTime}    10
    updateConnTableGlobalConfig  ${atipSession}  ${globalConfig0}
    waitUntilATIPReady  ${atipSession}
    waitUntilSystemReady  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest}
    ${newConn3}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${newConn}
    ${timeouts3}=  getProcStatValueFromDebugPage  ${atipSession}  ${npId0}  ${timeouts}
    should be equal as integers  ${timeouts3}  ${timeouts2}
    resetConnTableGlobalConfig  ${atipSession}

Validate QuickResumes
    [Arguments]  ${hashType}
    updateSslSettings  ${atipSession}  True  ${defaultGroupId}
    deleteAllCerts  ${atipSession}
    uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert1024}  ${fileKey1024}  ${defaultGroupId}
    ${globalConfig0}=  getConnTableGlobalConfig  ${atipSession}  ${defaultGroupId}
    ${globalConfig0.tuple_hash_method}=  set variable  ${hashType}
    updateConnTableGlobalConfig  ${atipSession}  ${globalConfig0}  ${defaultGroupId}
    waitUntilATIPReady  ${atipSession}
    waitUntilSystemReady  ${atipSession}
    Log  Run BPS traffic
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigResume}
    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTestConfigResume}
    ${stats}=  getSSLStatsFromDebugPage  ${atipSession}
    Log  ${stats}
    ${sslKeys}=  get from dictionary  ${stats}  SslKeysUnmatched
    should be equal as integers  ${sslKeys}  0
    ${newConn}=  get from dictionary  ${stats}  SslNewEncryptedConns
    should not be equal as integers  ${newConn}  0
    ${resumeConns}=  get from dictionary  ${stats}  SslResumedConns
    should not be equal as integers  ${resumeConns}  0
    ${standardConns}=  get from dictionary  ${stats}  SslStandardConns
    should not be equal as integers  ${standardConns}  0
    ${resumeFails}=  get from dictionary  ${stats}  SslResumeFails
    Run Keyword If   "${hashType}" == "ip_port"
    ...   should not be equal as integers  ${resumeFails}  0
    ...   ELSE
    ...   should be equal as integers  ${resumeFails}  0

    ${expectedValue}=  Evaluate  ${resumeConns} + ${resumeFails} + ${standardConns}
    should be equal as integers  ${newConn}  ${bpsTotalSessions}
    should be equal as integers  ${newConn}  ${expectedValue}
    deleteAllCerts  ${atipSession}
    updateSslSettings  ${atipSession}  False  ${defaultGroupId}
    resetConnTableGlobalConfig  ${atipSession}

