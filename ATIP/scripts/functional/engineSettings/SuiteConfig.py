from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.ProcStats import ProcStats
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.WebApiSession import WebApiSession



class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    httpStatusOK = HTTPStatus.OK
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    appStatsType = StatsType.APPS
    atipSessions = TopStats.TOTAL_COUNT
    timeInterval = TimeInterval.HOUR
    minimizedDashboard = DashboardType.MINIMIZED
    rxPkts = ProcStats.RX_PKTS.statType
    newConn = ProcStats.NEW_CONN.statType
    timeouts = ProcStats.CONN_TIMEOUTS.statType
    rxQueueDepth = ProcStats.RX_QUEUE_DEPTH.statType
    rxQueueDrops = ProcStats.RX_QUEUE_DROPS.statType
    bpsTestConfigHash = BpsTestConfig('Test_HashDistribution', 40)
    bpsTestConfigResume = BpsTestConfig('Test_quickResumes', 60)
    bpsTestConfigDeep = BpsTestConfig('Recreate_Long_TCP_Stream_netflix',40)
    bpsTestConfigConnUDP = BpsTestConfig('DNS_Req_Resp_Delay30s',60)
    bpsTestConfigConnTCP = BpsTestConfig('http_get_delay45s', 70)
    bpsTestConfigReass = BpsTestConfig('RECREATE-evbrite', 40)

    name='cert1024'

    npId0 = int(envVars.atipSlotNumber[0])
    index = 0
    defaultGroupId = 0

    bpsKeysPath = "../../../frwk/src/atipAutoFrwk/data/bpskeys"
    fileCert1024 = bpsKeysPath + '/BreakingPoint_serverA_1024.crt'
    fileKey1024 = bpsKeysPath + '/BreakingPoint_serverA_1024.key'




