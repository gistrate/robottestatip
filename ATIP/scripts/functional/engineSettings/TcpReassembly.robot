*** Settings ***

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.services.GeneralService  WITH NAME  General
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.atip.stats.ValidateSSLStatsService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    Collections
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.webApi.atip.ConnTableGlobalConfig
Library    atipAutoFrwk.webApi.atip.ConfigureStats
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.webApi.atip.stats.Filters
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    atipAutoFrwk.webApi.atip.MplsParsing
Library    atipAutoFrwk.webApi.atip.TcpMode
Library    atipAutoFrwk.webApi.atip.MatchEngineGlobal




Default Tags  ATIP  functional  hardware

Suite Setup        Clear Config

*** Test Cases ***
TC000991682 TCP Reassembly
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigReass}
    waitTargetExistsInPieStats  ${atipSession}  ${appStatsType}  ${timeInterval}  https (TCP:443)
    ${tcpConfig0}=  getTcpConfig  ${atipSession}  ${defaultGroupId}
    ${tcpConfig0.enabledAlways}=  set variable  True
    updateTcpConfig  ${atipSession}  ${tcpConfig0}  ${defaultGroupId}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigReass}
    waitTargetExistsInPieStats  ${atipSession}  ${appStatsType}  ${timeInterval}  Eventbrite




*** Keywords ***

Clear Config
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}





