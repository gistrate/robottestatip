*** Settings ***

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.services.GeneralService  WITH NAME  General
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.atip.stats.ValidateSSLStatsService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    Collections
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.webApi.atip.ConnTableGlobalConfig
Library    atipAutoFrwk.webApi.atip.ConfigureStats
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.webApi.atip.stats.Filters
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    atipAutoFrwk.webApi.atip.MplsParsing
Library    atipAutoFrwk.webApi.atip.TcpMode
Library    atipAutoFrwk.webApi.atip.MatchEngineGlobal




Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Clear Config

*** Test Cases ***
TC001025653 Match Engine Max DPI Pkts
    ${detailedStats0}=  getConfig  ${atipSession}
    ${detailedStats0.enableTurboMode}=  set variable  False
    changeConfig  ${atipSession}  ${detailedStats0}
    resetStats  ${atipSession}
    Reset DPI Max Packets Config
    Log  Run BPS traffic
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigDeep}
    waitTargetExistsInPieStats  ${atipSession}  ${appStatsType}  ${timeInterval}  TCP:2000
    Update DPI Max Packets Config  ${False}  60
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigDeep}
    waitTargetExistsInPieStats  ${atipSession}  ${appStatsType}  ${timeInterval}  Netflix
    Reset DPI Max Packets Config


*** Keywords ***

Clear Config
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}


Reset DPI Max Packets Config
    ${response}  resetMatchEngineGlobalConfig  ${atipSession}
    Log  ${response}
    should be equal  ${response.buffer_packets}  ${False}
    should be equal as integers  ${response.max_inspect_packets}    30

Update DPI Max Packets Config
    [Arguments]  ${bufferPkts}  ${maxPkts}
    ${config}  getMatchEngineGlobalConfig  ${atipSession}
    ${config.buffer_packets}=  set variable  ${bufferPkts}
    ${config.max_inspect_packets}=  set variable  ${maxPkts}
    ${response}  updateMatchEngineGlobalConfig  ${atipSession}  ${config}
    should be equal  ${response.buffer_packets}  ${bufferPkts}
    should be equal as integers  ${response.max_inspect_packets}  ${maxPkts}



