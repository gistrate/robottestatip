*** Settings ***

#Libraries
Library  ./NTO/frwk/Utils.py
Library  ./NTO/frwk/NTOPortGroup.py
Library  Collections
Library  String

#Resource files
Resource  ./NTO/frwk/RobotUtils.robot

*** Variables ***
${RESOURCETIMEOUT}=  10

*** Test Cases ***
Create NTO Config
    import library  atipAutoFrwk.config.Environment  WITH NAME  environment
	${env}=  get library instance  environment

	import library  atipAutoFrwk.config.nto.NTOConfig  WITH NAME  ntoConfigFile
	${ntoConfig}=  get library instance  ntoConfigFile

    AddNTO  nto1  ${env.ntoConnectionConfig.host}
    Log To Console  Destroying NTO Configuration !
    nto1.clearFiltersAndPorts
    Sleep  2m
    Log To Console  NTO Configuration Started !
    ${myPortDef}=   Evaluate  ''
    ${index}=  Evaluate  1
    :FOR    ${key}    IN    @{ntoConfig.trafficPorts}
        \  ${myPortDef}=  Catenate  'np${index}': {'properties': {'ID': '${key}',
            ...                                               'ConnectionType': 'standalone',
            ...                                               'MediaType': 'SFP_PLUS_10G'}},    ${myPortDef}
        \  ${index}=  evaluate  ${index}+1
    ${myTrafficPorts}=  catenateToDict  {${myPortDef}}
    Log  ${myTrafficPorts}

    ${myPortDef}=   Evaluate  ''
    ${index}=  Evaluate  1
    :FOR    ${key}    IN    @{ntoConfig.appForwardingPorts}
        \  ${myPortDef}=  Catenate  'tp${index}': {'properties': {'ID': '${key}',
            ...                                               'ConnectionType': 'standalone',
            ...                                               'MediaType': 'SFP_PLUS_10G'}},    ${myPortDef}
        \  ${index}=  evaluate  ${index}+1
    ${myAppFwdPorts}=  catenateToDict  {${myPortDef}}
    Log  ${myAppFwdPorts}

    ${myPortDef}=   Evaluate  ''
    ${index}=  Evaluate  1
    :FOR    ${key}    IN    @{ntoConfig.netflowPG}
        \  ${myPortDef}=  Catenate  'npg${index}': {'properties': {'ID': '${key}',
            ...                                               'ConnectionType': 'standalone',
            ...                                               'MediaType': 'SFP_PLUS_10G'}},    ${myPortDef}
        \  ${index}=  evaluate  ${index}+1
    ${myNPGPorts}=  catenateToDict  {${myPortDef}}
    Log  ${myNPGPorts}

    #Add atip resources
    :FOR    ${atipId}    IN    @{env.atipSlotNumber}
        \  AddATIP  ${nto1}  atip${atipId}   L${atipId}-ATIP

    # Add traffic ports
    ${npId} =  set variable  1
    :FOR    ${key}    IN    @{ntoConfig.trafficPorts}
        \  AddPort  ${nto1}  np${npId}  BIDIRECTIONAL  ${myTrafficPorts}
        \  ${npId}=  evaluate  ${npId}+1
        \  Log  Crt np id:
        \  Log  ${npId}

    ${npgId} =  set variable  1
    # Add NetFlow Port Group
      ${pg1}=  Catenate  {'type': 'NETFLOW', 'mode': 'BIDIRECTIONAL', 'failover_mode': 'REBALANCE'}
      :FOR    ${key}    IN    @{ntoConfig.netflowPG}
        \  AddPort  ${nto1}  npg${npgId}  BIDIRECTIONAL  ${myNPGPorts}
        \  ${npg}=  set variable  ${npg${npgId}}
        \  AddPortGrp  ${nto1}  NetFlow  ${pg1}  ${npg}
        \  ${npgId}=  evaluate  ${npgId}+1
        \  Log  Crt np id:
        \  Log  ${npgId}

    # Add forward tool ports
    ${tpId} =  set variable  1
    :FOR    ${key}    IN    @{ntoConfig.appForwardingPorts}
        \  AddPort  ${nto1}  tp${tpId}  TOOL  ${myAppFwdPorts}
        \  ${tpId}=  evaluate  ${tpId}+1
        \  Log  Crt tp id:
        \  Log  ${tpId}

    #Add Filters
    ${portLenght} =  get length  ${ntoConfig.trafficPorts}
    ${atipLenght} =  get length  ${env.atipSlotNumber}
    ${totalFilters} =  evaluate  ${portLenght}+${atipLenght}

    :FOR    ${filterId}    IN RANGE  ${totalFilters}
        \  ${filterId}=  evaluate  ${filterId}+1
        \  AddFilter  ${nto1}  f${filterId}  PASS_ALL
        \  Log  Crt Filter id:
        \  Log  ${filterId}

    #Add bi-di Connetions between traffic ports and filters
    ${zero} =  set variable  0
    ${connId} =  set variable  0
    ${connId2} =  set variable  0
    :FOR    ${connId}    IN RANGE  ${portLenght}
        \  ${connId}=  evaluate  ${connId}+1
        \  ${par}=  evaluate  ${connId}%2
        \  ${eval}=    set variable if    ${par} == ${zero}  ${True}  ${False}
        \  Log  ${eval}
        \  run keyword if  ${eval} == ${False}  CustomForLoop  ${connId}  ${portLenght}
        ...  ELSE  Log  Odd index!

    #Add Connetions from traffic ports and atip dynamic filter
    :FOR    ${resFilterId}    IN RANGE  ${atipLenght}
        \  ${resFilterId}=  evaluate  ${portLenght}+${resFilterId}+1
        \  Log  ${resFilterId}
        \  ResourcesForLoop  ${resFilterId}  ${portLenght}

    #Add Connetions forward ports and atip dynamic filters
    ${fwdPortLenght} =  get length  ${ntoConfig.appForwardingPorts}
    :FOR    ${fwpId}    IN RANGE  ${fwdPortLenght}
        \  ${fwpId}=  evaluate  ${fwpId}+1
        \  ${fwf1}=  set variable  f${npId}
        \  ${connId3}=  evaluate  ${portLenght}*${portLenght}+${fwpId}
        \  ${fwf2}=  evaluate  ${portLenght}+1
        \  Log  ${fwf2}
        \  ${fwf3}=  set variable  ${f${fwf2}}
        \  ${fwf5}=  set variable  ${tp${fwpId}}
        \  Log  ${fwf3}
        \  Log  ${fwf5}
        \  AddConn    ${nto1}  c${connId3}  None  ${fwf3}  ${fwf5}

    nto1.getATIPResources
    nto1.setupNVSDevices
    nto1.initAllPortsAndFilters
    NetFlow.createPortGrp
    ${id}=  set variable  1
    Log  ${id}
    :FOR    ${resId}    IN    @{env.atipSlotNumber}
        \  ${ffId1}=  evaluate  ${portLenght}+${id}
        \  ${id}=  evaluate  ${id}+1
        \  ${ffId2}=  set variable  ${f${ffId1}}
        \  ${ffId3}=  set variable  atip${resId}
        \  Log  ${ffId2}
        \  Log  ${resId}
        \  Log  ${ffId3}
        \  run keyword  ${ffId3}.attachResource  ${ffId2}

    @{dL1}=  Create List  ${tp1.id}
    Log  ${tp1.id}
    @{dL2}=  Create List  ${tp2.id}


    &{appFwd1}=  Create Dictionary  application_name=default                        dest_port_list=${dL1}
    &{appFwd2}=  Create Dictionary  application_name=appForward   vlan_id=100,200,300,400,500       dest_port_list=${dL2}

    f9.configureAppForwardingMaps  ${appFwd1}  ${appFwd2}
    f10.configureAppForwardingMaps  ${appFwd1}  ${appFwd2}
    f11.configureAppForwardingMaps  ${appFwd1}  ${appFwd2}

*** Keywords ***

CustomForLoop
    [Arguments]  ${connId}  ${portLenght}


    ${connId2}=  evaluate  ${connId}+1
    Log  ${connId}
    Log  ${connId2}
    ${ntp1}=  set variable  ${np${connId}}
    ${ntp2}=  set variable  ${np${connId2}}
    ${tf1}=  set variable  ${f${connId}}
    ${tf2}=  set variable  ${f${connId2}}
    AddConn    ${nto1}  c${connId}  ${ntp1}  ${tf1}  ${ntp2}
    AddConn    ${nto1}  c${connId2}  ${ntp2}  ${tf2}  ${ntp1}

ResourcesForLoop
    [Arguments]  ${resFilterId}  ${portLenght}

    :FOR    ${conntId}    IN RANGE  ${portLenght}
        \  ${conntId}=  evaluate  ${conntId}+1
        \  ${atippId}=  evaluate  ${resFilterId}-${portLenght}
        \  Log  ${atippId}
        \  ${trfcId}=  evaluate  ${portLenght}*${atippId}+${conntId}
        \  ${trfpf3}=  set variable  ${f${resFilterId}}
        \  ${trfpf4}=  set variable  ${np${conntId}}
        \  AddConn    ${nto1}  c${trfcId}  ${trfpf4}  ${trfpf3}  None