from http import HTTPStatus

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.WebApiSession import WebApiSession


class SuiteConfig(object):
    envVars = Environment()
    webApiSession = WebApiSession(envVars.atipConnectionConfig)
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
