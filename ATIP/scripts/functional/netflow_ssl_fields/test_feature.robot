*** Settings ***

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.Login   WITH NAME   atLogin
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.config.atip.NetflowCollectorConfig
Library    atipAutoFrwk.config.traffic.BpsTestConfig  a  1
Library    atipAutoFrwk.services.atip.NetflowService    WITH NAME    serNetflowConfig
Library    atipAutoFrwk.webApi.atip.Netflow             WITH NAME    Netflow
Library    atipAutoFrwk.services.GeneralService
Library    Telnet   30 seconds     WITH NAME   Tnet
Library    Collections
Library    String


Suite Setup        Setup Netflow SSL Encrypt ATIP
Suite Teardown     Teardown Netflow SSL Encrypt ATIP


Default Tags  ${hardware}   ${virtual}

*** Test Cases ***
Netflow SSL Enc Fields Encrypted passiveDecrypt Disabled Cipher 1
    # Test variables
    ${bptFileName}=     set variable    ATIP_SSL-HTTP1_0-keySize512
    ${pcapFileName}=    set variable    /tmp/netflowssl.pcapng
    ${csvFileName}=     set variable    /tmp/netflowssl.csv
    ${encryptType}=     set variable    Encrypted
    ${cipherName}=      set variable    TLS_RSA_WITH_RC4_128_MD5
    ${keyLength}=       set variable    128
    ${passiveDecrypt}=  set variable    False

    #  \n
    #  Open Telnet Session
    ${session_telnet}=   Open Connection     ${envVars.linuxConnectionConfig.host}     prompt=$

    #  Telnet to the Linux Box to capture Netflow
    Tnet.Login  ${envVars.linuxConnectionConfig.username}   ${envVars.linuxConnectionConfig.password}

    #  Enable Netflow Global, Fields, Card, Collector
    Enable Netflow Global Config
    Enable All SSL Fields
    Enable Service Netflow Card Config
    Config First Collector  ${envVars.netflowCollectorConnectionConfig.host}

    #  Start Capture Using Tshark

    Execute Command  rm ${pcapFileName}
    Execute Command  rm ${csvFileName}
    Write   tshark -i ${envVars.linuxConnectionConfig.port} -w ${pcapFileName}
    Read Until  ${envVars.linuxConnectionConfig.port}

    #  Run BPS Test File
    Run BpsTestConfig   ${bptFileName}   400

    #  Stop Capture
    Write Control Character     BRK
    Read Until Prompt

    #  Process the captured File
    ${result}=  Execute Command     python nf_pcap2csv.py ${pcapFileName} ${csvFileName}
    ${result}=  Execute Command     cat ${csvFileName}
    log  ${result}
    #  \n
    #  ${result}
    ${result}=  Replace String Using Regexp  ${result}   \r\n    ${\n}
    @{lines}=  Split To Lines  ${result}    ${EMPTY}    -1
    #log to console      \n
    :FOR    ${line}     IN  @{lines}
    #\       log to console  ${line}
    #:FOR    ${index}    IN RANGE    2  10  2
    #\       ${line}=    get from list  ${lines}  ${index}
    \       @{encryptFields}=   Split String    ${line}     ,
    \       ${testKeyLen}=  get from list  ${encryptFields}   -1
    \       ${testCipher}=  get from list  ${encryptFields}   -2
    \       ${testType}=    get from list  ${encryptFields}   -3
    \       ${found}=  Run Keyword If  '${testType}'=='${encryptType}' and '${testCipher}'=='${cipherName}' and '${testKeyLen}'=='${keyLength}'  set variable    True
    \       ...                  ELSE   set variable    False
    \       exit for loop if  '${found}'=='True'
    should be true  ${found}

Netflow SSL Enc Fields Encrypted passiveDecrypt Disabled Cipher 2
    # Test variables
    ${bptFileName}=     set variable    ATIP_SSL-HTTP1_0-keySize2048-AES
    ${pcapFileName}=    set variable    /tmp/netflowssl.pcapng
    ${csvFileName}=     set variable    /tmp/netflowssl.csv
    ${encryptType}=     set variable    Encrypted
    ${cipherName}=      set variable    TLS_RSA_WITH_AES_256_CBC_SHA
    ${keyLength}=       set variable    256
    ${passiveDecrypt}=  set variable    False

    #  \n
    #  Open Telnet Session
    ${session_telnet}=   Open Connection     ${envVars.linuxConnectionConfig.host}     prompt=$

    #  Telnet to the Linux Box to capture Netflow
    Tnet.Login  ${envVars.linuxConnectionConfig.username}   ${envVars.linuxConnectionConfig.password}

    #  Enable Netflow Global, Fields, Card, Collector
    Enable Netflow Global Config
    Enable All SSL Fields
    Enable Service Netflow Card Config
    Config First Collector  ${envVars.netflowCollectorConnectionConfig.host}

    #  Start Capture Using Tshark
    Execute Command  rm ${pcapFileName}
    Execute Command  rm ${csvFileName}
    Write   tshark -i ${envVars.linuxConnectionConfig.port} -w ${pcapFileName}
    Read Until  ${envVars.linuxConnectionConfig.port}

    #  Run BPS Test File
    Run BpsTestConfig   ${bptFileName}   400

    #  Stop Capture
    Write Control Character     BRK
    Read Until Prompt

    #  Process the captured File
    ${result}=  Execute Command     python nf_pcap2csv.py ${pcapFileName} ${csvFileName}
    ${result}=  Execute Command     sed -n 1,10p ${csvFileName}
    log  ${result}
    #  \n
    #  ${result}
    ${result}=  Replace String Using Regexp  ${result}   \r\n    ${\n}
    @{lines}=  Split To Lines  ${result}    ${EMPTY}    -1
    #log to console      \n
    :FOR    ${line}     IN  @{lines}
    #\       log to console  ${line}
    #:FOR    ${index}    IN RANGE    2  10  2
    #\       ${line}=    get from list  ${lines}  ${index}
    \       @{encryptFields}=   Split String    ${line}     ,
    \       ${testKeyLen}=  get from list  ${encryptFields}   -1
    \       ${testCipher}=  get from list  ${encryptFields}   -2
    \       ${testType}=    get from list  ${encryptFields}   -3
    \       ${found}=  Run Keyword If  '${testType}'=='${encryptType}' and '${testCipher}'=='${cipherName}' and '${testKeyLen}'=='${keyLength}'  set variable    True
    \       ...                  ELSE   set variable    False
    \       exit for loop if  '${found}'=='True'
    should be true  ${found}



Netflow SSL Enc Fields Encrypted passiveDecrypt Enabled no Cert
    [Tags]  ${hardware}
    # Test variables
    ${bptFileName}=     set variable    ATIP_SSL-HTTP1_0-keySize512
    ${pcapFileName}=    set variable    /tmp/netflowssl.pcapng
    ${csvFileName}=     set variable    /tmp/netflowssl.csv
    ${encryptType}=     set variable    Encrypted
    ${cipherName}=      set variable    TLS_RSA_WITH_RC4_128_MD5
    ${keyLength}=       set variable    128
    ${passiveDecrypt}=  set variable    False

    #  \n
    #  Open Telnet Session
    ${session_telnet}=   Open Connection     ${envVars.linuxConnectionConfig.host}     prompt=$

    #  Telnet to the Linux Box to capture Netflow
    Tnet.Login  ${envVars.linuxConnectionConfig.username}   ${envVars.linuxConnectionConfig.password}

    #  Enable Netflow Global, Fields, Card, Collector
    Enable Netflow Global Config
    Enable All SSL Fields
    Enable Service Netflow Card Config
    Config First Collector  ${envVars.netflowCollectorConnectionConfig.host}
    Enabled SSL Passive Decryption Config

    #  Start Capture Using Tshark
    Execute Command  rm ${pcapFileName}
    Execute Command  rm ${csvFileName}
    Write   tshark -i ${envVars.linuxConnectionConfig.port} -w ${pcapFileName}
    Read Until  ${envVars.linuxConnectionConfig.port}

    #  Run BPS Test File
    Run BpsTestConfig   ${bptFileName}   400

    #  Stop Capture
    Write Control Character     BRK
    Read Until Prompt

    #  Process the captured File
    ${result}=  Execute Command     python nf_pcap2csv.py ${pcapFileName} ${csvFileName}
    ${result}=  Execute Command     sed -n 1,10p ${csvFileName}
    log  ${result}
    #  \n
    #  ${result}
    ${result}=  Replace String Using Regexp  ${result}   \r\n    ${\n}
    @{lines}=  Split To Lines  ${result}    ${EMPTY}    -1
    #log to console      \n
    :FOR    ${line}     IN  @{lines}
    #\       log to console  ${line}
    #:FOR    ${index}    IN RANGE    2  10  2
    #\       ${line}=    get from list  ${lines}  ${index}
    \       @{encryptFields}=   Split String    ${line}     ,
    \       ${testKeyLen}=  get from list  ${encryptFields}   -1
    \       ${testCipher}=  get from list  ${encryptFields}   -2
    \       ${testType}=    get from list  ${encryptFields}   -3
    \       ${found}=  Run Keyword If  '${testType}'=='${encryptType}' and '${testCipher}'=='${cipherName}' and '${testKeyLen}'=='${keyLength}'  set variable    True
    \       ...                  ELSE   set variable    False
    \       exit for loop if  '${found}'=='True'
    should be true  ${found}


Netflow SSL Enc Fields Decrypted passiveDecrypt Enabled and Cert
    [Tags]  ${hardware}
    # Test variables
    ${bptFileName}=     set variable    ATIP_SSL-HTTP1_0-keySize512
    ${pcapFileName}=    set variable    /tmp/netflowssl.pcapng
    ${csvFileName}=     set variable    /tmp/netflowssl.csv
    ${encryptType}=     set variable    Decrypted
    ${cipherName}=      set variable    TLS_RSA_WITH_RC4_128_MD5
    ${keyLength}=       set variable    128

    #  \n
    #  Open Telnet Session
    ${session_telnet}=   Open Connection     ${envVars.linuxConnectionConfig.host}     prompt=$

    #  Telnet to the Linux Box to capture Netflow
    Tnet.Login  ${envVars.linuxConnectionConfig.username}   ${envVars.linuxConnectionConfig.password}

    #  Enable Netflow Global, Fields, Card, Collector
    Enable Netflow Global Config
    Enable All SSL Fields
    Enable Service Netflow Card Config
    Config First Collector  ${envVars.netflowCollectorConnectionConfig.host}
    Enabled SSL Passive Decryption Config
    Upload Separate Files Test

    #  Start Capture Using Tshark

    Execute Command  rm ${pcapFileName}
    Execute Command  rm ${csvFileName}
    Write   tshark -i ${envVars.linuxConnectionConfig.port} -w ${pcapFileName}
    Read Until  ${envVars.linuxConnectionConfig.port}

    #  Run BPS Test File
    Run BpsTestConfig   ${bptFileName}    400

    #  Stop Capture
    Write Control Character     BRK
    Read Until Prompt

    #  Process the captured File
    ${result}=  Execute Command     python nf_pcap2csv.py ${pcapFileName} ${csvFileName}
    ${result}=  Execute Command     sed -n 1,10p ${csvFileName}
    log  ${result}
    ${result}=  Replace String Using Regexp  ${result}   \r\n    ${\n}
    @{lines}=  Split To Lines  ${result}    ${EMPTY}    -1
    #log to console      \n
    :FOR    ${line}     IN  @{lines}
    #\       log to console  ${line}
    #:FOR    ${index}    IN RANGE    2  10  2
    #\       ${line}=    get from list  ${lines}  ${index}
    \       @{encryptFields}=   Split String    ${line}     ,
    \       ${testKeyLen}=  get from list  ${encryptFields}   -1
    \       ${testCipher}=  get from list  ${encryptFields}   -2
    \       ${testType}=    get from list  ${encryptFields}   -3
    \       ${found}=  Run Keyword If  '${testType}'=='${encryptType}' and '${testCipher}'=='${cipherName}' and '${testKeyLen}'=='${keyLength}'  set variable    True
    \       ...                  ELSE   set variable    False
    \       exit for loop if  '${found}'=='True'
    should be true  ${found}



*** Keywords ***

Run BpsTestConfig
    [Arguments]  ${bptFile}     ${testTime}
    ${bpsTestConfigInit}=    create BpsTestConfig   ${bptFile}  ${testTime}
    runBpsTest  ${bpsSession}   ${envVars}  ${bpsTestConfigInit}

Run BpsTestConfig youku
    ${bpsTestConfigInit}=    create BpsTestConfig   neo RECREATE youku  400
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigInit}

Run BpsTestConfig aolmail
    ${bpsTestConfigInit}=    create BpsTestConfig    neo RECREATE aolmailnew  400
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigInit}

Disable Netflow Global Config
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    #log to console  \n${objNetflowGlobalConfig.enabled}
    ${objNetflowGlobalConfig.enabled}=  Set Variable    False
    #log to console  ${objNetflowGlobalConfig.enabled}
    Netflow.updateGlobalConfig  ${webApiSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    #log to console  ${objNetflowGlobalConfig.enabled}
    should not be true  ${objNetflowGlobalConfig.enabled}

Enable Netflow Global Config
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    #log to console  \n${objNetflowGlobalConfig.enabled}
    ${objNetflowGlobalConfig.enabled}=  Set Variable    True
    #log to console  ${objNetflowGlobalConfig.enabled}
    Netflow.updateGlobalConfig  ${webApiSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    #log to console  ${objNetflowGlobalConfig.enabled}
    should be true  ${objNetflowGlobalConfig.enabled}

Get SSL Fields
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    #log to console  \n${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

Disable All SSL Fields
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    ${objNetflowGlobalConfig.netflowL7ConnEncryptType}=     Set Variable    False
    ${objNetflowGlobalConfig.netflowL7EncryptionCipher}=    set variable    False
    ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}=    set variable    False

    Netflow.updateGlobalConfig  ${webApiSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}

    #log to console  \n${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

    should not be true  ${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    should not be true  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    should not be true  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

Enable All SSL Fields
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    ${objNetflowGlobalConfig.netflowL7ConnEncryptType}=     Set Variable    True
    ${objNetflowGlobalConfig.netflowL7EncryptionCipher}=    set variable    True
    ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}=    set variable    True

    Netflow.updateGlobalConfig  ${webApiSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}

    #log to console  \n${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

    should be true  ${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    should be true  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    should be true  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

Get Netflow Card Config
    ${jsonNetflowCardConfig}=   Netflow.getCardConfig  ${webApiSession}     ${httpStatusOK}
    #log to console  \n${jsonNetflowCardConfig}

Disable Service Netflow Card Config
    [Tags]  ${hardware}
    ${objNetflowCardConfig}=   serNetflowConfig.Get Card Config      ${webApiSession}     ${httpStatusOK}
    #${cardToUpdate}=    Get From List   ${objNetflowCardConfig.cardList}    ${envVars.atipSlotNumber}
    ##log to console  \n${cardToUpdate.enabled}
    #${objNetflowCardConfig.enabled}=    set variable    False
    #updateCardConfig    ${webApiSession}    ${objNetflowCardConfig}     key=enabled     value=False
    updateCard    ${webApiSession}    ${objNetflowCardConfig}     currentID=${envVars.atipSlotNumber}     key=enabled     value=False
    ${objNetflowCardConfig}=   serNetflowConfig.Get Card Config      ${webApiSession}     ${httpStatusOK}
    #log to console  ${objNetflowCardConfig.printConfig()}
    ${cardToUpdate}=    Get From List   ${objNetflowCardConfig.cardList}    ${envVars.atipSlotNumber}
    #log to console  \n${cardToUpdate.enabled}

Enable Service Netflow Card Config
    [Tags]  ${hardware}
    ${objNetflowCardConfig}=   serNetflowConfig.Get Card Config      ${webApiSession}     ${httpStatusOK}
    #log to console  ${objNetflowCardConfig.printConfig()}
    ${cardToUpdate}=    Get From List   ${objNetflowCardConfig.cardList}    ${envVars.atipSlotNumber}
    ${cardToUpdate.enabled}=    set variable    True
    ${cardToUpdate.gw}=    set variable    192.168.30.100
    ${cardToUpdate.dns}=    set variable    8.8.8.8
    ${cardToUpdate.odid}=    set variable    100
    ${cardToUpdate.ipMethod}=    set variable    STATIC
    ${cardToUpdate.ip_addr}=    set variable    192.168.30.1
    ${cardToUpdate.netmask}=    set variable    255.255.255.0
    #log to console  ${objNetflowCardConfig.printConfig()}
    updateCard    ${webApiSession}    ${objNetflowCardConfig}   currentID=${envVars.atipSlotNumber}  key=enabled     value=True
    #Netflow.updateCardConfig    ${webApiSession}    ${objNetflowCardConfig}
    ${objNetflowCardConfig}=   serNetflowConfig.Get Card Config      ${webApiSession}     ${httpStatusOK}
    #log to console  ${objNetflowCardConfig.printConfig()}

Get Netflow Collector Config
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
    #log to console  \n
    #log to console  ${listNetflowCollectorConfig.printConfig()}

Get First Available Collector Index
    ${nFirstAvailableCollector}=    getFirstAvailable      ${webApiSession}
    #log to console  \n${nFirstAvailableCollector}

#Config First Avaialbe Collector
#    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
#    ${nFirstAvailableCollector}=    getFirstAvailable      ${webApiSession}
#    ${collectorToUpdate}=   Get From List   ${listNetflowCollectorConfig.collectorList}   ${nFirstAvailableCollector}
#    ${collectorToUpdate.unit}=   set variable  Samples
#    ${collectorToUpdate.port}=   set variable  ${4739}
#    ${collectorToUpdate.rate}=   set variable  ${100}
#    ${collectorToUpdate.proto}=   set variable  UDP
#    ${collectorToUpdate.collector}=   set variable  192.168.30.100
#    ${collectorToUpdate.enabled}=   set variable  True
#    #    def updateCollectorConfig(cls, webApiSession, netflowCollectorConfig, expectedStatus, currentID, key = None, value = None):
#    updateCollectorConfig  ${webApiSession}    ${listNetflowCollectorConfig}   ${httpStatusOK}  ${nFirstAvailableCollector}
#    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
#    log to console  \n
#    log to console  ${listNetflowCollectorConfig.printConfig()}

Config First Collector
    [Arguments]     ${ipCollector}
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
    ${collectorToUpdate}=   Get From List   ${listNetflowCollectorConfig.collectorList}   ${0}
    ${collectorToUpdate.unit}=   set variable  Samples
    ${collectorToUpdate.port}=   set variable  ${4739}
    ${collectorToUpdate.rate}=   set variable  ${100}
    ${collectorToUpdate.proto}=   set variable  UDP
    #${collectorToUpdate.collector}=   set variable  192.168.30.100
    ${collectorToUpdate.collector}=   set variable  ${ipCollector}
    ${collectorToUpdate.enabled}=   set variable  True
    #    def updateCollectorConfig(cls, webApiSession, netflowCollectorConfig, expectedStatus, currentID, key = None, value = None):
    #updateCollectorConfig  ${webApiSession}    ${listNetflowCollectorConfig}   ${httpStatusOK}  ${0}
    updateCollectorConfig  ${webApiSession}  ${collectorToUpdate}
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
    #log to console  \n
    #log to console  ${listNetflowCollectorConfig.printConfig()}

Disable the Collector
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
    ${collectorToUpdate}=   Get From List   ${listNetflowCollectorConfig.collectorList}   ${0}
    ${collectorToUpdate.enabled}=   set variable  False
    #updateCollectorConfig  ${webApiSession}    ${listNetflowCollectorConfig}   ${httpStatusOK}  ${0}    key=enabled    value=False
    updateCollectorConfig  ${webApiSession}  ${collectorToUpdate}
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
    #log to console  \n
    #log to console  ${listNetflowCollectorConfig.printConfig()}




Reset SSL Passive Decryption Config
    ${response}  updateSslSettings  ${webApiSession}    False
    should be equal  ${response.passiveSSLDecryption}  ${False}

Enabled SSL Passive Decryption Config
    ${response}  updateSslSettings  ${webApiSession}    True
    should be equal  ${response.passiveSSLDecryption}  ${True}

Get SSL Passive Decryption Config
    ${response}  getSslSettings  ${webApiSession}
    should be equal  ${response.passiveSSLDecryption}  ${True}


Upload Separate Files Test
    ${fileCert}=  Set variable  ../../../frwk/src/atipAutoFrwk/data/bpskeys/BreakingPoint_serverA_512.crt
    ${fileKey}=  Set variable   ../../../frwk/src/atipAutoFrwk/data/bpskeys/BreakingPoint_serverA_512.key
    ${name}=  Set variable  BreakingPoint_serverA_512

    ${response}  uploadSeparateFiles  ${webApiSession}  ${name}  ${fileCert}  ${fileKey}
    #log to console  \n Response is ${response}
    #log to console  ${response.certificates}
    #should be equal as integers  ${response.status_code}  200

Delete All SSL Certificates Separate File
    ${response}  deleteAllCerts  ${webApiSession}
    #log to console  \n Response is ${response}

Upload Single File Test
    ${fileCombined}=  Set variable  ../../../frwk/src/atipAutoFrwk/data/sslkeys/deep_0001.two
    ${response}  uploadSingleFile  ${webApiSession}  ${fileCombined}
    #log to console  \n Response.txt is ${response}
    #should be equal as integers  ${response.status_code}  200


Delete All SSL Certificates Single File
    ${response}  BulkdeleteAllCerts  ${webApiSession}
    #log to console  \n Response is ${response}

Upload Single File Test Two of Them One by One
    ${fileCombined}=  Set variable  ../../../frwk/src/atipAutoFrwk/data/sslkeys/deep_0001.two
    ${response}  uploadSingleFile  ${webApiSession}  ${fileCombined}
    ${fileCombined}=  Set variable  ../../../frwk/src/atipAutoFrwk/data/sslkeys/deep_0002.two
    ${response}  uploadSingleFile  ${webApiSession}  ${fileCombined}
#
Bulk Delete All SSL Certificates Test Two of Them
    ${response}  BulkdeleteAllCerts  ${webApiSession}

Bulk Upload SSL Certificates Test Two of Them
    @{twoCerts}=  create list
    ...             ../../../frwk/src/atipAutoFrwk/data/sslkeys/deep_0001.two
    ...             ../../../frwk/src/atipAutoFrwk/data/sslkeys/deep_0002.two
    ${response}     bulkUploadSingleFile  ${webApiSession}     ${twoCerts}

Bulk Delete All SSL Certificates Test Bulk Upload Two of Them
    ${response}  BulkdeleteAllCerts  ${webApiSession}

Bulk Upload Max SSL Certificates Test
    @{twoCerts}=  create list
    ...             ../../../frwk/src/atipAutoFrwk/data/sslkeys/deep_0001.two
    ...             ../../../frwk/src/atipAutoFrwk/data/sslkeys/deep_0002.two
    ${response}     bulkUploadSingleFile  ${webApiSession}     ${twoCerts}

Retrieve All SSL Certificates Test
    ${response}  getSslCerts  ${webApiSession}
    log  \n Response is ${response}

Bulk Delete All SSL Certificates Test Max
    ${response}  BulkdeleteAllCerts  ${webApiSession}

Disable SSL Passive Decryption Config
    ${response}  updateSslSettings  ${webApiSession}    False
    should be equal  ${response.passiveSSLDecryption}  ${False}



Setup Netflow SSL Encrypt ATIP
    clearSystemAndWaitUntilNPReady  ${webApiSession}  ${envVars}
    ${response}  atLogin.login  ${webApiSession}
    should be equal as integers  ${response.status_code}  ${httpStatusOK}  msg=${response.text}
    should contain  ${response.text}  ${loginSuccessful}

Teardown Netflow SSL Encrypt ATIP
    Run keyword if  '${envVars.atipType}'=='${hardware}'  deleteAllCerts  ${webApiSession}
    Run keyword if  '${envVars.atipType}'=='${hardware}'  Reset SSL Passive Decryption Config
    logout  ${webApiSession}
