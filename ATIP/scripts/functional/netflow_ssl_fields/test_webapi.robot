*** Settings ***

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.SslConfig
#Library    atipAutoFrwk.config.atip.Card
#Library    atipAutoFrwk.config.atip.Collector
#Library    atipAutoFrwk.config.atip.NetflowAccelerationConfig
#Library    atipAutoFrwk.config.atip.NetflowCardConfig
Library    atipAutoFrwk.config.atip.NetflowCollectorConfig
#Library    atipAutoFrwk.config.atip.NetflowConfig
Library    atipAutoFrwk.config.traffic.BpsTestConfig  a  1
Library    atipAutoFrwk.services.atip.NetflowService    WITH NAME    serNetflowConfig
Library    atipAutoFrwk.webApi.atip.Netflow             WITH NAME    Netflow
Library    Collections
Library    atipAutoFrwk.services.GeneralService

Suite Setup        Login ATIP
Suite Teardown     logout  ${webApiSession}


Default Tags  ${hardware}   ${virtual}
*** Test Cases ***

Disable Netflow Global Config
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    #log to console  \n${objNetflowGlobalConfig.enabled}
    ${objNetflowGlobalConfig.enabled}=  Set Variable    False
    #log to console  ${objNetflowGlobalConfig.enabled}
    Netflow.updateGlobalConfig  ${webApiSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    #log to console  ${objNetflowGlobalConfig.enabled}
    should not be true  ${objNetflowGlobalConfig.enabled}

Enable Netflow Global Config
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    #log to console  \n${objNetflowGlobalConfig.enabled}
    ${objNetflowGlobalConfig.enabled}=  Set Variable    True
    #log to console  ${objNetflowGlobalConfig.enabled}
    Netflow.updateGlobalConfig  ${webApiSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    #log to console  ${objNetflowGlobalConfig.enabled}
    should be true  ${objNetflowGlobalConfig.enabled}

Get SSL Fields
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    #log to console  \n${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

Disable All SSL Fields
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    ${objNetflowGlobalConfig.netflowL7ConnEncryptType}=     Set Variable    False
    ${objNetflowGlobalConfig.netflowL7EncryptionCipher}=    set variable    False
    ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}=    set variable    False

    Netflow.updateGlobalConfig  ${webApiSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}

    #log to console  \n${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

    should not be true  ${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    should not be true  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    should not be true  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

Enable All SSL Fields
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}
    ${objNetflowGlobalConfig.netflowL7ConnEncryptType}=     Set Variable    True
    ${objNetflowGlobalConfig.netflowL7EncryptionCipher}=    set variable    True
    ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}=    set variable    True

    Netflow.updateGlobalConfig  ${webApiSession}     ${objNetflowGlobalConfig}    ${httpStatusOK}
    ${objNetflowGlobalConfig}=    Netflow.getGlobalConfig  ${webApiSession}

    #log to console  \n${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    #log to console  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

    should be true  ${objNetflowGlobalConfig.netflowL7ConnEncryptType}
    should be true  ${objNetflowGlobalConfig.netflowL7EncryptionCipher}
    should be true  ${objNetflowGlobalConfig.netflowL7EncryptionKeyLen}

Get Netflow Card Config
    ${jsonNetflowCardConfig}=   Netflow.getCardConfig  ${webApiSession}     ${httpStatusOK}
    #log to console  \n${jsonNetflowCardConfig}

Disable Service Netflow Card Config
    [Tags]  ${hardware}
    ${objNetflowCardConfig}=   serNetflowConfig.Get Card Config      ${webApiSession}     ${httpStatusOK}
    #${cardToUpdate}=    Get From List   ${objNetflowCardConfig.cardList}    ${envVars.atipSlotNumber}
    ##log to console  \n${cardToUpdate.enabled}
    #${objNetflowCardConfig.enabled}=    set variable    False
    #updateCardConfig    ${webApiSession}    ${objNetflowCardConfig}     key=enabled     value=False
    updateCard    ${webApiSession}    ${objNetflowCardConfig}     currentID=${envVars.atipSlotNumber}     key=enabled     value=False
    ${objNetflowCardConfig}=   serNetflowConfig.Get Card Config      ${webApiSession}     ${httpStatusOK}
    #log to console  ${objNetflowCardConfig.printConfig()}
    ${cardToUpdate}=    Get From List   ${objNetflowCardConfig.cardList}    ${envVars.atipSlotNumber}
    #log to console  \n${cardToUpdate.enabled}

Enable Service Netflow Card Config
    [Tags]  ${hardware}
    ${objNetflowCardConfig}=   serNetflowConfig.Get Card Config      ${webApiSession}     ${httpStatusOK}
    #log to console  ${objNetflowCardConfig.printConfig()}
    ${cardToUpdate}=    Get From List   ${objNetflowCardConfig.cardList}    ${envVars.atipSlotNumber}
    ${cardToUpdate.enabled}=    set variable    True
    ${cardToUpdate.gw}=    set variable    192.168.30.100
    ${cardToUpdate.dns}=    set variable    8.8.8.8
    ${cardToUpdate.odid}=    set variable    100
    ${cardToUpdate.ipMethod}=    set variable    STATIC
    ${cardToUpdate.ip_addr}=    set variable    192.168.30.1
    ${cardToUpdate.netmask}=    set variable    255.255.255.0
    #log to console  ${objNetflowCardConfig.printConfig()}
    updateCard    ${webApiSession}    ${objNetflowCardConfig}   currentID=${envVars.atipSlotNumber}  key=enabled     value=True
    #Netflow.updateCardConfig    ${webApiSession}    ${objNetflowCardConfig}
    ${objNetflowCardConfig}=   serNetflowConfig.Get Card Config      ${webApiSession}     ${httpStatusOK}
    #log to console  ${objNetflowCardConfig.printConfig()}

Get Netflow Collector Config
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
    #log to console  \n
    #log to console  ${listNetflowCollectorConfig.printConfig()}

Get First Available Collector Index
    ${nFirstAvailableCollector}=    getFirstAvailable      ${webApiSession}
    #log to console  \n${nFirstAvailableCollector}

#Config First Avaialbe Collector
#    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
#    ${nFirstAvailableCollector}=    getFirstAvailable      ${webApiSession}
#    ${collectorToUpdate}=   Get From List   ${listNetflowCollectorConfig.collectorList}   ${nFirstAvailableCollector}
#    ${collectorToUpdate.unit}=   set variable  Samples
#    ${collectorToUpdate.port}=   set variable  ${4739}
#    ${collectorToUpdate.rate}=   set variable  ${100}
#    ${collectorToUpdate.proto}=   set variable  UDP
#    ${collectorToUpdate.collector}=   set variable  192.168.30.100
#    ${collectorToUpdate.enabled}=   set variable  True
#    #    def updateCollectorConfig(cls, webApiSession, netflowCollectorConfig, expectedStatus, currentID, key = None, value = None):
#    updateCollectorConfig  ${webApiSession}    ${listNetflowCollectorConfig}   ${httpStatusOK}  ${nFirstAvailableCollector}
#    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
#    log to console  \n
#    log to console  ${listNetflowCollectorConfig.printConfig()}

Config First Collector
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
    ${collectorToUpdate}=   Get From List   ${listNetflowCollectorConfig.collectorList}   ${0}
    ${collectorToUpdate.unit}=   set variable  Samples
    ${collectorToUpdate.port}=   set variable  ${4739}
    ${collectorToUpdate.rate}=   set variable  ${100}
    ${collectorToUpdate.proto}=   set variable  UDP
    ${collectorToUpdate.collector}=   set variable  192.168.30.100
    ${collectorToUpdate.enabled}=   set variable  True
    #    def updateCollectorConfig(cls, webApiSession, netflowCollectorConfig, expectedStatus, currentID, key = None, value = None):
#    updateCollectorConfig  ${webApiSession}    ${listNetflowCollectorConfig}   ${httpStatusOK}  ${0}
    updateCollectorConfig  ${webApiSession}  ${collectorToUpdate}
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
    #log to console  \n
    #log to console  ${listNetflowCollectorConfig.printConfig()}

Disable the Collector
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
    ${collectorToUpdate}=   Get From List   ${listNetflowCollectorConfig.collectorList}   ${0}
#    updateCollectorConfig  ${webApiSession}    ${listNetflowCollectorConfig}   ${httpStatusOK}  ${0}    key=enabled    value=False
    ${collectorToUpdate.enabled}=   set variable  False
    updateCollectorConfig  ${webApiSession}  ${collectorToUpdate}
    ${listNetflowCollectorConfig}=  serNetflowConfig.getCollectorConfig     ${webApiSession}    ${httpStatusOK}
    #log to console  \n
    #log to console  ${listNetflowCollectorConfig.printConfig()}


*** Keywords ***

Login ATIP
    clearSystemAndWaitUntilNPReady  ${webApiSession}  ${envVars}
    ${response}  login  ${webApiSession}
    should be equal as integers  ${response.status_code}  ${httpStatusOK}  msg=${response.text}
    should contain  ${response.text}  ${loginSuccessful}
