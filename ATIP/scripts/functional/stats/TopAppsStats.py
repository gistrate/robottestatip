from cmath import isclose
from http import HTTPStatus

import jsonpickle
import logging
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.stats.StatsService import StatsService
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.services.traffic.BpsComponentStatsService import BpsComponentStatsService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.webApi.atip.stats.Lists import Lists
from atipAutoFrwk.webApi.traffic.BpsComponentStats import BpsComponentStats
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations
from lib.ValidateStatsService import ValidateStatsService



########## Helper Methods ##########

########## Test Suite Setup ##########
# Define test config
# Define ATIP config
atipConfig = AtipConfig()
# Define bps config
bpsTestConfig = BpsTestConfig('TopGeos_auto', 65)
bpsTestConfig.addAppSim(AppSimConfig('appsim_1', ApplicationType.AOL, OSType.Linux, BrowserType.BonEcho, GeoLocation.CANADA, GeoLocation.UNITED_STATES, PacketSizes(70,364,450,66)))
bpsTestConfig.addAppSim(AppSimConfig('appsim_2', ApplicationType.NETFLIX, OSType.Linux, BrowserType.SeaMonkey, GeoLocation.UNITED_STATES, GeoLocation.CANADA, PacketSizes(70,517,450,66)))
bpsTestConfig.addAppSim(AppSimConfig('appsim_3', ApplicationType.FACEBOOK, OSType.MacOS, BrowserType.SeaMonkey, GeoLocation.AUSTRALIA, GeoLocation.AUSTRALIA, PacketSizes(70,643,450,66)))
bpsTestConfig.addAppSim(AppSimConfig('appsim_4', ApplicationType.LINKEDIN, OSType.Linux, BrowserType.BonEcho, GeoLocation.CANADA, GeoLocation.UNITED_STATES, PacketSizes(70,464,450,66)))


# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
bpsSession = WebApiSession(env.bpsConnectionConfig)

# Configure ATIP
Login.login(atipSession)
AtipConfigService.createConfig(atipSession, atipConfig)
# Configure BPS
BpsLogin.login(bpsSession)
PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)

########## Test Setup ##########
# Clear stats on ATIP
System.resetStats(atipSession)
# Send traffic
bpsTestConfig.testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup,
                                                bpsTestConfig.testDuration)

bpsComponentsStats = BpsComponentStats.getAllComponentsStats(bpsSession, bpsTestConfig)


atipStatsMax = Lists.getTopStats(atipSession, StatsType.APPS.statsName, timeInterval=TimeInterval.FIVEMIN.intervalName, limit='50', appType=None, expectedStatus=HTTPStatus.OK)

print("Verify all app names are present in maximized window")
ValidateStatsService.checkTargetExistsInTopStats(atipStatsMax,'AOL')
ValidateStatsService.checkTargetExistsInTopStats(atipStatsMax,'Facebook')
ValidateStatsService.checkTargetExistsInTopStats(atipStatsMax,'LinkedIn')
ValidateStatsService.checkTargetExistsInTopStats(atipStatsMax,'Netflix')

print("Session stats validation")
isItOK = ValidateStatsService.checkSpecificAppStat(atipStatsMax, bpsTestConfig, bpsComponentsStats, atipSession,  StatsType.APPS, TopStats.TOTAL_COUNT.statType)
print("Test: {}".format(isItOK))

print("Compute Total packets")
isItOK = ValidateStatsService.checkSpecificAppStat(atipStatsMax, bpsTestConfig, bpsComponentsStats, atipSession,  StatsType.APPS, TopStats.TOTAL_PKTS.statType)
print("Test: {}".format(isItOK))

print("Client packets stats validation")
isItOK = ValidateStatsService.checkSpecificAppStat(atipStatsMax, bpsTestConfig, bpsComponentsStats, atipSession,  StatsType.APPS, TopStats.CLIENT_PKTS.statType)
print("Test: {}".format(isItOK))

print("Server packets stats validation")
isItOK = ValidateStatsService.checkSpecificAppStat(atipStatsMax, bpsTestConfig, bpsComponentsStats, atipSession,  StatsType.APPS, TopStats.SERVER_PKTS.statType)
print("Test: {}".format(isItOK))

print("Verify total bytes")
isItOK = ValidateStatsService.checkSpecificAppStat(atipStatsMax, bpsTestConfig, bpsComponentsStats, atipSession,  StatsType.APPS, TopStats.TOTAL_BYTES.statType)
print("Test: {}".format(isItOK)
      )
print("Verify client bytes")
isItOK = ValidateStatsService.checkSpecificAppStat(atipStatsMax, bpsTestConfig, bpsComponentsStats, atipSession,  StatsType.APPS, TopStats.CLIENT_BYTES.statType)
print("Test: {}".format(isItOK))

print("Verify server bytes")
isItOK = ValidateStatsService.checkSpecificAppStat(atipStatsMax, bpsTestConfig, bpsComponentsStats, atipSession,  StatsType.APPS, TopStats.SERVER_BYTES.statType)
print("Test: {}".format(isItOK))

print("Verify share window")
ValidateStatsService.checkAtipTopStatsTotalShare(atipStatsMax)




########## Test Suite Teardown ##########
Logout.logout(atipSession)
