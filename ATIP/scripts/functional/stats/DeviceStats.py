
from cmath import isclose
from http import HTTPStatus
import jsonpickle
import logging

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.RealTimeStats import RealTimeStats
from atipAutoFrwk.data.traffic.stats.BpsComponentStatsContainer import BpsComponentStatsContainer
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.LogoutService import LogoutService
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.services.atip.stats.StatsService import StatsService
from atipAutoFrwk.services.atip.stats.TopFiltersService import TopFiltersService
from atipAutoFrwk.services.traffic.BpsComponentStatsService import BpsComponentStatsService
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Configuration import Configuration
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.webApi.atip.TopFilters import TopFilters
from atipAutoFrwk.webApi.atip.stats.Lists import Lists
from atipAutoFrwk.webApi.atip.stats.Pie import Pie
from atipAutoFrwk.webApi.traffic.BpsComponentStats import BpsComponentStats
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations


########## Helper Methods ##########
def checkPieStatsValues(statsType, bpsBytesStats, totalBytes):
    atipStats = Pie.getTopStats(atipSession, statsType)
    for atipStat in atipStats.data:
        share = bpsBytesStats[atipStat.name] / totalBytes
        return isclose(atipStat.y, share, rel_tol=0.1)

def checkPieStats(statsType, bpsBytesStats, totalBytes):
    checkStatValue = lambda : checkPieStatsValues(statsType, bpsBytesStats, totalBytes)
    logActualValue = lambda : logging.exception('Found incorrect values for {}. Actual: {} expected: {}'.format(
        statsType,
        jsonpickle.encode(Pie.getTopStats(atipSession, statsType)),
        jsonpickle.encode(bpsBytesStats)))
    TestCondition.waitUntilTrue(checkStatValue, 
                                'Found incorrect values for {}. Check logs for actual values'.format(statsType),
                                onFailure=logActualValue)

def checkListStatsValues(statsType, crtApp, appSimConfig, bpsSessions, bpsPackets, bpsBytes):
    atipStatsByApp = Lists.getTopStats(atipSession, statsType, appType=crtApp)
    print("{} STATS BY APP {}: {}".format(statsType, crtApp.appName, jsonpickle.encode(atipStatsByApp)))
    
    for atipStat in atipStatsByApp.getList():
        crtKeyName = appSimConfig.getKeyName(statsType)
        print('{} {} {}'.format(atipStat.msg, atipStat.totalBytes, bpsBytes[crtKeyName]))
        print('{} {} {}'.format(atipStat.msg, atipStat.totalCount, bpsSessions[crtKeyName]))
        print('{} {} {}'.format(atipStat.msg, atipStat.totalPkts, bpsPackets[crtKeyName]))
        
        expectedBytes = StatsService.getBytesForAckPacketLoss(bpsBytes[crtKeyName], bpsPackets[crtKeyName], atipStat.totalPkts)
        sessionsOK = isclose(atipStat.totalCount, bpsSessions[crtKeyName], rel_tol=0.1)
        bytesOK = isclose(atipStat.totalBytes, expectedBytes, rel_tol=0.1)
        return sessionsOK and bytesOK

def checkListStats(statsType, crtApp, appSimConfig, bpsSessions, bpsPackets, bpsBytes):
    checkStatValue = lambda : checkListStatsValues(statsType, crtApp, appSimConfig, bpsSessions, bpsPackets, bpsBytes)
    logActualValue = lambda : logging.exception('Found incorrect values for {}. Actual: {}. Bps sessions: {}. Bps packets: {}. Bps bytes: {}'.format(
        statsType,
        jsonpickle.encode(Lists.getTopStats(atipSession, statsType, appType=crtApp)),
        jsonpickle.encode(bpsSessions),
        jsonpickle.encode(bpsPackets),
        jsonpickle.encode(bpsBytes)))
        
    TestCondition.waitUntilTrue(checkStatValue, 
                                'Found incorrect values for {}. Check logs for actual values'.format(statsType),
                                onFailure=logActualValue)

########## Test Suite Setup ##########
# Define test config
# Define ATIP config
atipConfig = AtipConfig()
# Define bps config
# bpsTestConfig = BpsTestConfig('TopBrowserTopDevices_15s', 15)
bpsTestConfig = BpsTestConfig('TopBrowserTopDevices_auto', 80)
bpsTestConfig.addAppSim(AppSimConfig('appsim_1', ApplicationType.AOL, OSType.Linux, BrowserType.BonEcho))
bpsTestConfig.addAppSim(AppSimConfig('appsim_2', ApplicationType.NETFLIX, OSType.Linux, BrowserType.SeaMonkey))
bpsTestConfig.addAppSim(AppSimConfig('appsim_3', ApplicationType.FACEBOOK, OSType.MacOS, BrowserType.SeaMonkey))

# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
bpsSession = WebApiSession(env.bpsConnectionConfig)

# Configure ATIP
Login.login(atipSession)
AtipConfigService.createConfig(atipSession, atipConfig)
# Configure BPS
BpsLogin.login(bpsSession)
PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)

########## Test Setup ##########
# Clear stats on ATIP
System.resetStats(atipSession)
# Send traffic
bpsTestConfig.testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup, bpsTestConfig.testDuration)

bpsComponentsStats = BpsComponentStats.getAllComponentsStats(bpsSession, bpsTestConfig)
totalBytes = BpsComponentStatsService.sumStatItem(bpsTestConfig, bpsComponentsStats, BpsStatsType.BYTES)
bytesByDevices = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsComponentsStats, BpsStatsType.BYTES, StatsType.DEVICES)
bytesByBrowsers = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsComponentsStats, BpsStatsType.BYTES, StatsType.BROWSERS)


print('bps stats: {}'.format(jsonpickle.encode(bpsComponentsStats)))
print('totalBytes: {}'.format(totalBytes))
print('by devices: {}'.format(bytesByDevices))
print('by browsers: {}'.format(bytesByBrowsers))


# Check Dashboard > TOP Browsers
checkPieStats(StatsType.BROWSERS, bytesByBrowsers, totalBytes)

# Check Dashboard > TOP Devices by OS
checkPieStats(StatsType.DEVICES, bytesByDevices, totalBytes)

# Check Installed Apps: TOP Devices
for component, appSimConfig in bpsTestConfig.appSimComponents.items():
    crtApp = appSimConfig.appType
    # compute bps values
    bpsStatsByApp = BpsComponentStatsService.filterStatsByApp(bpsTestConfig, bpsComponentsStats, crtApp)
    bpsSessionsByDevices = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp, BpsStatsType.SESSIONS, StatsType.DEVICES)
    bpsPacketsByDevices = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp, BpsStatsType.PACKETS, StatsType.DEVICES)
    bpsBytesByDevices = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp, BpsStatsType.BYTES, StatsType.DEVICES)
    # check the values
    checkListStats(StatsType.DEVICES, crtApp, appSimConfig, bpsSessionsByDevices, bpsPacketsByDevices, bpsBytesByDevices)


# Check Installed Apps: TOP Browsers
for component, appSimConfig in bpsTestConfig.appSimComponents.items():
    crtApp = appSimConfig.appType
    # compute bps values
    bpsStatsByApp = BpsComponentStatsService.filterStatsByApp(bpsTestConfig, bpsComponentsStats, crtApp)
    bpsSessionsByBrowsers = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp, BpsStatsType.SESSIONS, StatsType.BROWSERS)
    bpsPacketsByBrowsers = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp, BpsStatsType.PACKETS, StatsType.BROWSERS)
    bpsBytesByBrowsers = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp, BpsStatsType.BYTES, StatsType.BROWSERS)
    # check the values
    checkListStats(StatsType.BROWSERS, crtApp, appSimConfig, bpsSessionsByBrowsers, bpsPacketsByBrowsers, bpsBytesByBrowsers)


########## Test Suite Teardown ##########
Logout.logout(atipSession)
