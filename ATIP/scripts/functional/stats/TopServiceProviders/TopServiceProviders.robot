*** Settings ***
Documentation  Top Service Providers basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Lists
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    ../lib/StatsDefaultConfig.py

Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Run BPS Test
Suite Teardown     logout  ${atipSession}

*** Test Cases ***
Validate Service Provider Name
    :FOR    ${key}    IN    @{atipStatsMin.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
        \  checkTargetExistsInTopStats  ${atipStat}  ${strProvider}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
        \  checkTargetExistsInTopStats  ${atipStat}  ${strProvider}


Validate Session Statistics
    :FOR    ${key}    IN    @{atipStatsMin.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipSessions}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipSessions}

Validate Total Packets Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipTotalPkts}

Validate Total Client Packets Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipClientPkts}

Validate Total Server Packets Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipServerPkts}

Validate Total Bytes Statistics
    :FOR    ${key}    IN    @{atipStatsMin.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipTotalBytes}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipTotalBytes}


Validate Client Bytes Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipClientBytes}

Validate Server Bytes Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipServerBytes}

Validate Shared Statistics
    :FOR    ${key}    IN    @{atipStatsMin.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipShare}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipShare}




*** Keywords ***
Configure ATIP And Run BPS Test
    configureAtip  ${atipSession}  ${atipConfig}
    resetStats  ${atipSession}
    Log  ${testType}
    Log  ${validate}
    ${bpsConfig} = 	getBpsTestConfig  ${bpsTestConfig}
    ${timeIntervalList} = 	getTimeIntervals
    set suite variable  ${bpsConfig}   ${bpsConfig}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    login  ${atipSession}
    ${atipStatsMin} =  create dictionary
    ${atipStatsMax} =  create dictionary
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  Log  ${timeInterval}
        \  ${statsMin}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}  ${minimizedDashboard}
        \  ${statsMax}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}  ${maximizedDashboard}
        \  ${atipStatsMin}  setAtipStats  ${timeInterval}  ${statsMin}  ${atipStatsMin}
        \  ${atipStatsMax}  setAtipStats  ${timeInterval}  ${statsMax}  ${atipStatsMax}
    Log  ${atipStatsMin}
    Log  ${atipStatsMax}
    set suite variable  ${atipStatsMin}  ${atipStatsMin}
    set suite variable  ${atipStatsMax}  ${atipStatsMax}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsConfig}
    set suite variable  ${bpsComponentsStats}  ${bpsComponentsStats}
    Log  ${bpsComponentsStats}