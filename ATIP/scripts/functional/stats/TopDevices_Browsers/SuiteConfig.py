from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip import Login
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig


class SuiteConfig(StatsDefaultConfig):
    typeBrowsers = StatsType.BROWSERS
    typeDevices = StatsType.DEVICES

    # Define BPS config
    bpsTestConfig1 = BpsTestConfig('TopBrowserTopDevices_auto_1min', 80)
    appsim1=AppSimConfig('appsim_1', ApplicationType.AOL, OSType.Linux, BrowserType.BonEcho,clientLocation=GeoLocation.UNITED_KINGDOM,
                                         serverLocation=GeoLocation.UNITED_KINGDOM)
    appsim2=AppSimConfig('appsim_2', ApplicationType.NETFLIX, OSType.Linux, BrowserType.SeaMonkey,clientLocation=GeoLocation.UNITED_KINGDOM,
                                         serverLocation=GeoLocation.UNITED_KINGDOM)
    appsim3=AppSimConfig('appsim_3', ApplicationType.FACEBOOK, OSType.MacOS, BrowserType.SeaMonkey,clientLocation=GeoLocation.UNITED_KINGDOM,
                                         serverLocation=GeoLocation.UNITED_KINGDOM)

    bpsTestConfig2 = BpsTestConfig('TopBrowserTopDevices_auto_40min', 2500)
    bpsTestConfig1.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfig2.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfigHW = [bpsTestConfig1, bpsTestConfig2]
    bpsTestConfigVirtual = [bpsTestConfig1, bpsTestConfig2]

    if StatsDefaultConfig.envVars.atipType == StatsDefaultConfig.virtual:
        bpsTestConfig = bpsTestConfigVirtual
    else:
        bpsTestConfig = bpsTestConfigHW




