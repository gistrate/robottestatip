*** Settings ***
Documentation  Top devices and browsers basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    ../lib/StatsDefaultConfig.py

Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Run BPS Test
Suite Teardown     logout  ${atipSession}

*** Test Cases ***
Validate Device Stats
    [Setup]  Setup Validate Device Statistics
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  checkPieStats  ${atipSession}  ${typeDevices}  ${bytesByDevices}  ${bpsTotalBytes}  ${timeInterval}

Validate Browser Stats
    [Setup]  Setup Validate Browser Statistics
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  checkPieStats  ${atipSession}  ${typeBrowsers}  ${bytesByBrowsers}  ${bpsTotalBytes}  ${timeInterval}








*** Keywords ***
Configure ATIP And Run BPS Test
    configureAtip  ${atipSession}  ${atipConfig}
    resetStats  ${atipSession}
    Log  ${testType}
    Log  ${validate}
    ${bpsConfig} = 	getBpsTestConfig  ${bpsTestConfig}
    ${timeIntervalList} = 	getTimeIntervals
    set suite variable  ${timeIntervalList}  ${timeIntervalList}
    set suite variable  ${bpsConfig}   ${bpsConfig}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    login  ${atipSession}

Setup Validate Device Statistics
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsConfig}
    ${totalBytes}  sumStatItem  ${bpsComponentsStats}  ${bpsBytes}
    ${bytesByDevices}  sumStatItemByType  ${bpsConfig}  ${bpsComponentsStats}  ${bpsBytes}  ${typeDevices}
    Set Test Variable  ${bpsTotalBytes}  ${totalBytes}
    Set Test Variable  ${bytesByDevices}  ${bytesByDevices}

Setup Validate Browser Statistics
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsConfig}
    ${totalBytes}  sumStatItem  ${bpsComponentsStats}  ${bpsBytes}
    ${bytesByBrowsers}  sumStatItemByType  ${bpsConfig}  ${bpsComponentsStats}  ${bpsBytes}  ${typeBrowsers}
    Set Test Variable  ${bpsTotalBytes}  ${totalBytes}
    Set Test Variable  ${bytesByBrowsers}  ${bytesByBrowsers}





