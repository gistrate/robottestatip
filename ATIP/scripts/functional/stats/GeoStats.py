from cmath import isclose
from http import HTTPStatus

import jsonpickle
import logging
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.stats.StatsService import StatsService
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.services.traffic.BpsComponentStatsService import BpsComponentStatsService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.webApi.atip.stats.Lists import Lists
from atipAutoFrwk.webApi.traffic.BpsComponentStats import BpsComponentStats
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations
from lib.ValidateStatsService import ValidateStatsService

########## Test Suite Setup ##########
# Define test config
strAustralia = GeoLocation.AUSTRALIA.countryName
strCanada = GeoLocation.CANADA.countryName
strUS = GeoLocation.UNITED_STATES.countryName

# Define ATIP config
atipConfig = AtipConfig()
# Define bps config
bpsTestConfig = BpsTestConfig('TopGeos_auto', 65)
bpsTestConfig.addAppSim(AppSimConfig('appsim_1', ApplicationType.AOL, OSType.Linux, BrowserType.BonEcho, GeoLocation.CANADA, GeoLocation.UNITED_STATES, PacketSizes(70,364,450,66)))
bpsTestConfig.addAppSim(AppSimConfig('appsim_2', ApplicationType.NETFLIX, OSType.Linux, BrowserType.SeaMonkey, GeoLocation.UNITED_STATES, GeoLocation.CANADA, PacketSizes(70,517,450,66)))
bpsTestConfig.addAppSim(AppSimConfig('appsim_3', ApplicationType.FACEBOOK, OSType.MacOS, BrowserType.SeaMonkey, GeoLocation.AUSTRALIA, GeoLocation.AUSTRALIA, PacketSizes(70,643,450,66)))
bpsTestConfig.addAppSim(AppSimConfig('appsim_4', ApplicationType.LINKEDIN, OSType.Linux, BrowserType.BonEcho, GeoLocation.CANADA, GeoLocation.UNITED_STATES, PacketSizes(70,464,450,66)))


# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
bpsSession = WebApiSession(env.bpsConnectionConfig)

# Configure ATIP
Login.login(atipSession)
AtipConfigService.createConfig(atipSession, atipConfig)
# Configure BPS
BpsLogin.login(bpsSession)
PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)

########## Test Setup ##########
# Clear stats on ATIP
System.resetStats(atipSession)
# Send traffic
bpsTestConfig.testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup,
                                                bpsTestConfig.testDuration)

bpsComponentsStats = BpsComponentStats.getAllComponentsStats(bpsSession, bpsTestConfig)


atipStatsMax = Lists.getTopStats(atipSession, StatsType.GEO, timeInterval=TimeInterval.FIVEMIN, limit='50')
atipStatsMin = Lists.getTopStats(atipSession, StatsType.GEO, timeInterval=TimeInterval.FIVEMIN, limit='10')

print("Verify all country names are present in maximized window")
ValidateStatsService.checkTargetExistsInTopStats(atipStatsMax, strAustralia)
ValidateStatsService.checkTargetExistsInTopStats(atipStatsMax, strCanada)
ValidateStatsService.checkTargetExistsInTopStats(atipStatsMax, strUS)

print("Verify all country names are present in minimized window")
ValidateStatsService.checkTargetExistsInTopStats(atipStatsMin,strAustralia)
ValidateStatsService.checkTargetExistsInTopStats(atipStatsMin, strCanada)
ValidateStatsService.checkTargetExistsInTopStats(atipStatsMin, strUS)



print("Session stats validation")
print("Compute stats for Australia (both client and server)")
totalSessions = BpsComponentStatsService.sumStatItem(bpsComponentsStats, BpsStatsType.SESSIONS)
bpsSessionsAustralia = getattr(bpsComponentsStats['appsim_3'], BpsStatsType.SESSIONS.statsName)
ValidateStatsService.checkAtipListStats(atipStatsMin,'Australia', TopStats.TOTAL_COUNT, bpsSessionsAustralia)
ValidateStatsService.checkAtipListStats(atipStatsMax,'Australia', TopStats.TOTAL_COUNT, bpsSessionsAustralia)


print("Compute stats for Canada (client for linkedin and aol; server for netflix)")
bpsSessionsCanada = totalSessions - bpsSessionsAustralia
ValidateStatsService.checkAtipListStats(atipStatsMin,'Canada', TopStats.TOTAL_COUNT, bpsSessionsCanada)
ValidateStatsService.checkAtipListStats(atipStatsMax,'Canada', TopStats.TOTAL_COUNT, bpsSessionsCanada)

print("Compute stats for US (client for netflix; server for linkedin and aol)")
bpsSessionsUS = totalSessions - bpsSessionsAustralia
ValidateStatsService.checkAtipListStats(atipStatsMin,'United States', TopStats.TOTAL_COUNT, bpsSessionsUS)
ValidateStatsService.checkAtipListStats(atipStatsMax,'United States', TopStats.TOTAL_COUNT, bpsSessionsUS)

print("Total packets stats validation")
ValidateStatsService.checkAtipTopStatsTotalPkts(atipStatsMax, bpsTestConfig, 'Canada', bpsSessionsCanada, statsType = StatsType.GEO)
ValidateStatsService.checkAtipTopStatsTotalPkts(atipStatsMax, bpsTestConfig, 'Australia', bpsSessionsAustralia,statsType = StatsType.GEO)
ValidateStatsService.checkAtipTopStatsTotalPkts(atipStatsMax, bpsTestConfig, 'United States',bpsSessionsUS, statsType = StatsType.GEO)

print("Client packets stats validation")
ValidateStatsService.checkAtipTopStatsClientPkts(atipStatsMax, bpsTestConfig, bpsComponentsStats,'Canada')
ValidateStatsService.checkAtipTopStatsClientPkts(atipStatsMax, bpsTestConfig, bpsComponentsStats,'Australia')
ValidateStatsService.checkAtipTopStatsClientPkts(atipStatsMax, bpsTestConfig, bpsComponentsStats,'United States')

print("Server packets stats validation")
ValidateStatsService.checkAtipTopStatsServerPkts(atipStatsMax, bpsTestConfig, bpsComponentsStats,'United States')
ValidateStatsService.checkAtipTopStatsServerPkts(atipStatsMax, bpsTestConfig, bpsComponentsStats,'Canada')
ValidateStatsService.checkAtipTopStatsServerPkts(atipStatsMax, bpsTestConfig, bpsComponentsStats,'Australia')

print("Verify total bytes minimized window")
ValidateStatsService.checkAtipTopStatsTotalBytes(atipStatsMin, bpsTestConfig, bpsComponentsStats,'Canada')
ValidateStatsService.checkAtipTopStatsTotalBytes(atipStatsMin, bpsTestConfig, bpsComponentsStats,'United States')
ValidateStatsService.checkAtipTopStatsTotalBytes(atipStatsMin, bpsTestConfig, bpsComponentsStats,'Australia')

print("Verify total bytes maximized window")
ValidateStatsService.checkAtipTopStatsTotalBytes(atipStatsMax, bpsTestConfig, bpsComponentsStats,'Canada')
ValidateStatsService.checkAtipTopStatsTotalBytes(atipStatsMax, bpsTestConfig, bpsComponentsStats,'United States')
ValidateStatsService.checkAtipTopStatsTotalBytes(atipStatsMax, bpsTestConfig, bpsComponentsStats,'Australia')

print("Verify client bytes")
ValidateStatsService.checkAtipTopStatsClientBytes(atipStatsMax, bpsTestConfig, bpsComponentsStats,'Canada')
ValidateStatsService.checkAtipTopStatsClientBytes(atipStatsMax, bpsTestConfig, bpsComponentsStats,'United States')
ValidateStatsService.checkAtipTopStatsClientBytes(atipStatsMax, bpsTestConfig, bpsComponentsStats,'Australia')

print("Verify server bytes")
ValidateStatsService.checkAtipTopStatsServerBytes(atipStatsMax, bpsTestConfig, bpsComponentsStats,'Canada')
ValidateStatsService.checkAtipTopStatsServerBytes(atipStatsMax, bpsTestConfig, bpsComponentsStats,'United States')
ValidateStatsService.checkAtipTopStatsServerBytes(atipStatsMax, bpsTestConfig, bpsComponentsStats,'Australia')


print("Verify share minimized window")
ValidateStatsService.checkAtipTopStatsTotalShare(atipStatsMin)
print("Verify share maximized window")
ValidateStatsService.checkAtipTopStatsTotalShare(atipStatsMax)




########## Test Suite Teardown ##########
Logout.logout(atipSession)
