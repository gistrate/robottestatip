from cmath import isclose
from http import HTTPStatus
import jsonpickle
import logging

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.RealTimeStats import RealTimeStats
from atipAutoFrwk.data.traffic.stats.BpsComponentStatsContainer import BpsComponentStatsContainer
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.LogoutService import LogoutService
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.services.atip.stats.StatsService import StatsService
from atipAutoFrwk.services.atip.stats.TopFiltersService import TopFiltersService
from atipAutoFrwk.services.traffic.BpsComponentStatsService import BpsComponentStatsService
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Configuration import Configuration
from atipAutoFrwk.webApi.atip.ConfigureStats import ConfigureStats
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.Status import Status
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.webApi.atip.TopFilters import TopFilters
from atipAutoFrwk.webApi.atip.stats.Lists import Lists
from atipAutoFrwk.webApi.atip.stats.Pie import Pie
from atipAutoFrwk.webApi.traffic.BpsComponentStats import BpsComponentStats
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations
from datetime import datetime

########## Helper Methods ##########

def checkListStatsValues(statsType, crtApp, appSimConfig, bpsSessions, bpsPackets, bpsBytes):
    atipStatsByApp = Lists.getTopStats(atipSession, statsType, appType=crtApp)
    print("{} STATS BY APP {}: {}".format(statsType, crtApp.appName, jsonpickle.encode(atipStatsByApp)))
    print("{} BPS STATS BY APP {}: {}".format(statsType, crtApp.appName, jsonpickle.encode(bpsBytes)))

    for atipStat in atipStatsByApp.list:
        crtKeyName = appSimConfig.getKeyName(statsType)
        print('{} {} {}'.format(atipStat.msg, atipStat.totalBytes, bpsBytes[crtKeyName]))
        print('{} {} {}'.format(atipStat.msg, atipStat.totalCount, bpsSessions[crtKeyName]))
        print('{} {} {}'.format(atipStat.msg, atipStat.totalPkts, bpsPackets[crtKeyName]))

        expectedBytes = StatsService.getBytesForAckPacketLoss(bpsBytes[crtKeyName], bpsPackets[crtKeyName],
                                                              atipStat.totalPkts)
        sessionsOK = isclose(atipStat.totalCount, bpsSessions[crtKeyName], rel_tol=0.1)
        bytesOK = isclose(atipStat.totalBytes, expectedBytes, rel_tol=0.1)
        return sessionsOK and bytesOK


def checkListStats(statsType, crtApp, appSimConfig, bpsSessions, bpsPackets, bpsBytes):
    checkStatValue = lambda: checkListStatsValues(statsType, crtApp, appSimConfig, bpsSessions, bpsPackets, bpsBytes)
    logActualValue = lambda: logging.exception(
        'Found incorrect values for {}. Actual: {}. Bps sessions: {}. Bps packets: {}. Bps bytes: {}'.format(
            statsType,
            jsonpickle.encode(Lists.getTopStats(atipSession, statsType, appType=crtApp)),
            jsonpickle.encode(bpsSessions),
            jsonpickle.encode(bpsPackets),
            jsonpickle.encode(bpsBytes)))

    TestCondition.waitUntilTrue(checkStatValue,
                                'Found incorrect values for {}. Check logs for actual values'.format(statsType),
                                onFailure=logActualValue)


def checkSessions(currentAtipApp, currentBPSStats):
    print("The sessions are: {} vs. {}".format(currentAtipApp.totalCount, getattr(currentBPSStats, BpsStatsType.SESSIONS.statsName)))
    if currentAtipApp.totalCount != getattr(currentBPSStats, BpsStatsType.SESSIONS.statsName):
        raise ValueError("Sessions differ: {} vs. {}".format(currentAtipApp.totalCount, getattr(currentBPSStats, BpsStatsType.SESSIONS.statsName)))
    return True

def checkAtipDynamicAppSideBytes(currentAtipApp, currentBPSStats, result, component, side):
    ':type currentAtipApp TopStatsItem'

    sessions = getattr(currentBPSStats, BpsStatsType.SESSIONS.statsName)
    sideBytes = result[component.name][side]
    expectedValue = sessions * sideBytes

    if (side == 'client'):
        atipValue = currentAtipApp.clientBytes
    else:
        atipValue = currentAtipApp.serverBytes
    print(
        "The total {} bytes are: {} vs. {}".format(side, atipValue, expectedValue))
    if expectedValue != atipValue:
        raise ValueError("Total bytes differ: {} vs. {}".format(atipValue, expectedValue))
    return True

def checkTotalBytes(currentAtipApp, currentBPSStats):
    ':type currentAtipApp TopStatsItem'


    bpsBytes = getattr(currentBPSStats, BpsStatsType.BYTES.statsName)
    bpsPackets = getattr(currentBPSStats, BpsStatsType.PACKETS.statsName)
    atipPackets = currentAtipApp.totalPkts
    expectedBytes = StatsService.getBytesForAckPacketLoss(bpsBytes, bpsPackets, atipPackets)
    print(
        "The total bytes are: {} vs. {}".format(currentAtipApp.totalBytes, expectedBytes))
    if expectedBytes != currentAtipApp.totalBytes:
        raise ValueError("Total bytes differ: {} vs. {}".format(expectedBytes, currentAtipApp.totalBytes))
    return True

def checkTotalPkts(currentAtipApp, currentBPSStats):
    ':type currentAtipApp TopStatsItem'


    bpsBytes = getattr(currentBPSStats, BpsStatsType.BYTES.statsName)
    bpsPackets = getattr(currentBPSStats, BpsStatsType.PACKETS.statsName)
    atipPackets = currentAtipApp.totalPkts
    expectedPackets = 6 * getattr(currentBPSStats, BpsStatsType.SESSIONS.statsName)
    print(
        "The total packets are: {} vs. {}".format(atipPackets, expectedPackets))
    if expectedPackets != atipPackets:
        raise ValueError("Total bytes differ: {} vs. {}".format(atipPackets, expectedPackets))
    return True

def checkDate(currentAtipApp, theRealDate):
    ':type currentAtipApp TopStatsItem'


    atipDate = int(currentAtipApp.ts // 1000) # cutting the last 3 digits from timestamp as they represent miliseconds
    print(
        "The dates are: {} vs. {}".format(atipDate, theRealDate))
    if not isclose(atipDate, theRealDate, abs_tol=600):
        print("Dates differ: {} vs. {}".format(atipDate, theRealDate))
        return False
    return True


def checkAtipDynamicAppSidePkts(currentAtipApp, currentBPSStats, side):

    bpsSession = getattr(currentBPSStats, BpsStatsType.SESSIONS.statsName)
    count = 3
    expectedValue = bpsSession * count
    if (side == 'client'):
        atipValue = currentAtipApp.clientPkts
    else:
        atipValue = currentAtipApp.serverPkts
    print(
        "The total {} packets are: {} vs. {}".format(side, atipValue, expectedValue))
    if expectedValue != atipValue:
        raise ValueError("Total bytes differ: {} vs. {}".format(atipValue, expectedValue))
    return True

def computeClientBytes(appSimConfig):

    return appSimConfig.requestPacketValues.synSize + appSimConfig.requestPacketValues.getSize + \
           appSimConfig.requestPacketValues.finAckSize

def computeServerBytes(appSimConfig):

    return appSimConfig.requestPacketValues.synSize + appSimConfig.requestPacketValues.okSize + \
           appSimConfig.requestPacketValues.finAckSize

def checkSpecificAppStat(bpsTestConfig, bpsComponentsStats, atipSession, statType, realDate, interval, limit, stat):
    atipStatsByApp = Lists.getTopStats(atipSession, statType, timeInterval=interval, limit=limit)

    result = dict()
    finalResult = dict()
    for component in bpsTestConfig.appSimComponents.values():
        print("The appsim config name and type: {}, {}".format(component.name, component.appType.appName))
        # The following line returns the first occurence of the atip dynamic app that matches the name
        # in the bpsConfig, or None if no such app was found

        currentAtipApp = next((x for x in atipStatsByApp.list if x.msg == component.appType.appName), None)
        #print("ATIP: {}, \n BPS: {}".format(currentAtipApp, bpsComponentsStats[component.name]))
        if (stat == TopStats.CLIENT_PKTS.statType):
            clientPktsOK = checkAtipDynamicAppSidePkts(currentAtipApp, bpsComponentsStats[component.name], 'client')
            finalResult[component.name] = clientPktsOK
        elif (stat == TopStats.SERVER_PKTS.statType):
            serverPktsOK = checkAtipDynamicAppSidePkts(currentAtipApp, bpsComponentsStats[component.name], 'server')
            finalResult[component.name] = serverPktsOK
        elif (stat == TopStats.TOTAL_BYTES.statType):
            totalBytesOK = checkTotalBytes(currentAtipApp, bpsComponentsStats[component.name])
            finalResult[component.name] = totalBytesOK
        elif (stat == TopStats.TOTAL_PKTS.statType):
            totalPktsOK = checkTotalPkts(currentAtipApp, bpsComponentsStats[component.name])
            finalResult[component.name] = totalPktsOK
        elif (stat == TopStats.TOTAL_COUNT.statType):
            totalCountOK = checkSessions(currentAtipApp, bpsComponentsStats[component.name])
            finalResult[component.name] = totalCountOK
        elif (stat == TopStats.CLIENT_BYTES.statType):
            result[component.name] = dict()
            result[component.name]['client'] = computeClientBytes(component)
            print("Client bytes: {}".format(result[component.name]['client']))
            clientBytesOK = checkAtipDynamicAppSideBytes(currentAtipApp, bpsComponentsStats[component.name], result,
                                                         component, 'client')
            finalResult[component.name] = clientBytesOK
        elif (stat == TopStats.SERVER_BYTES.statType):
            result[component.name] = dict()
            result[component.name]['server'] = computeServerBytes(component)
            print("Server bytes: {}".format(result[component.name]['server']))
            serverBytesOK = checkAtipDynamicAppSideBytes(currentAtipApp, bpsComponentsStats[component.name], result,
                                                     component, 'server')
            finalResult[component.name] = serverBytesOK
        elif (stat == TopStats.DISCOVERY.statType):
            discoveryOK = checkDate(currentAtipApp, realDate)
            result[component.name] = discoveryOK
    return sum (x == True for x in finalResult.values()) == len (finalResult)

def checkAppStats(bpsTestConfig, bpsComponentsStats, atipSession, statType, realDate, interval, limit):
    atipStatsByApp = Lists.getTopStats(atipSession, statType, timeInterval=interval, limit=limit)

    result = dict()
    finalResult = dict()
    for component in bpsTestConfig.appSimComponents.values():
        print("The appsim config name and type: {}, {}".format(component.name, component.appType.appName))
        # The following line returns the first occurence of the atip dynamic app that matches the name
        # in the bpsConfig, or None if no such app was found
        currentAtipApp = next((x for x in atipStatsByApp.list if x.msg == component.appType.appName), None)
        #print("ATIP: {}, \n BPS: {}".format(currentAtipApp, bpsComponentsStats[component.name]))
        clientPktsOK = checkAtipDynamicAppSidePkts(currentAtipApp, bpsComponentsStats[component.name], 'client')
        serverPktsOK = checkAtipDynamicAppSidePkts(currentAtipApp, bpsComponentsStats[component.name], 'server')
        totalBytesOK = checkTotalBytes(currentAtipApp, bpsComponentsStats[component.name])

        totalPktsOK = checkTotalPkts(currentAtipApp, bpsComponentsStats[component.name])
        totalCountOK = checkSessions(currentAtipApp, bpsComponentsStats[component.name])
        result[component.name] = dict()
        result[component.name]['client'] = computeClientBytes(component)
        print("Client bytes: {}".format(result[component.name]['client']))
        result[component.name]['server'] = computeServerBytes(component)
        print("Server bytes: {}".format(result[component.name]['server']))
        serverBytesOK = checkAtipDynamicAppSideBytes(currentAtipApp, bpsComponentsStats[component.name], result,
                                                     component, 'server')
        clientBytesOK = checkAtipDynamicAppSideBytes(currentAtipApp, bpsComponentsStats[component.name], result,
                                                     component, 'client')
        discoveryOK = checkDate(currentAtipApp, realDate)
        print("{} {} {} {} {} {} {} {}".format(totalBytesOK, totalCountOK, totalPktsOK, clientPktsOK, serverBytesOK, serverPktsOK, discoveryOK, clientBytesOK))
        finalResult[component.name] = totalBytesOK and totalCountOK and totalPktsOK and clientPktsOK and serverPktsOK and discoveryOK and \
                serverBytesOK and clientBytesOK
    print("The final result is: {}".format(finalResult))
    return sum (x == True for x in finalResult.values()) == len (finalResult)

        #return clientBytesOK and serverBytesOK and clientPktsOK and serverPktsOK and totalBytesOK and shareOK and \
        #       totalPktsOK and discoveryOK
########## Test Suite Setup ##########
# Define test config
# Define ATIP config
atipConfig = AtipConfig()
# Define bps config



bpsTestConfig = BpsTestConfig('LatestDyn_auto', 62)
bpsTestConfig.addAppSim(AppSimConfig('appsim_3', ApplicationType.TECHCRUNCH, OSType.Linux, BrowserType.BonEcho, clientLocation="C", serverLocation="S", requestPacketValues=PacketSizes(okSize=410, getSize=478)))
bpsTestConfig.addAppSim(AppSimConfig('appsim_2', ApplicationType.BUSINESSINSIDER, OSType.Linux, BrowserType.BonEcho, clientLocation="C", serverLocation="S", requestPacketValues=PacketSizes(okSize=510, getSize=483)))
bpsTestConfig.addAppSim(AppSimConfig('appsim_1', ApplicationType.BOREDPANDA, OSType.Linux, BrowserType.BonEcho, clientLocation="C", serverLocation="S", requestPacketValues=PacketSizes(okSize=460, getSize=526)))

# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
bpsSession = WebApiSession(env.bpsConnectionConfig)

# Configure ATIP
Login.login(atipSession)
AtipConfigService.createConfig(atipSession, atipConfig)
# Configure BPS
BpsLogin.login(bpsSession)
PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)

########## Test Setup ##########
# Clear stats on ATIP
# SystemService.clearSystemAndWaitUntilNPReady(atipSession, env)

System.resetStats(atipSession)
# Send traffic

atipconf = AtipConfig()

theRealDate = Status.getStatus(atipSession)
theRealDate = theRealDate['status']['currentTime']
theRealDate = theRealDate // 1000

bpsTestConfig.testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup,
                                                bpsTestConfig.testDuration)




bpsComponentsStats = BpsComponentStats.getAllComponentsStats(bpsSession, bpsTestConfig)
print('bps stats: {}'.format(jsonpickle.encode(bpsComponentsStats)))
totalBytes = BpsComponentStatsService.sumStatItem(bpsComponentsStats, BpsStatsType.BYTES)
print('totalBytes: {}'.format(totalBytes))
print(type(bpsComponentsStats))

isOK = checkAppStats(bpsTestConfig, bpsComponentsStats, atipSession, StatsType.DYNAMIC, theRealDate, TimeInterval.FIVEMIN, DashboardType.MAXIMIZED.stringLimit)
isItOK = checkSpecificAppStat (bpsTestConfig, bpsComponentsStats, atipSession, StatsType.DYNAMIC, theRealDate, TimeInterval.FIVEMIN, DashboardType.MAXIMIZED.stringLimit, TopStats.TOTAL_PKTS.statType)
print("Test: {}".format(isItOK))
if not isOK:
    raise ValueError("Different values found!")
print("Verification finished.")


########## Test Suite Teardown ##########
Logout.logout(atipSession)
