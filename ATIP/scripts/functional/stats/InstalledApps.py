from cmath import isclose
from http import HTTPStatus
import jsonpickle
import logging

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.stats.StatsService import StatsService
from atipAutoFrwk.services.traffic.BpsComponentStatsService import BpsComponentStatsService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.webApi.atip.stats.Lists import Lists
from atipAutoFrwk.webApi.atip.stats.Pie import Pie
from atipAutoFrwk.webApi.atip.stats.Ports import Ports
from atipAutoFrwk.webApi.traffic.BpsComponentStats import BpsComponentStats
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.traffic.BpsStats import BpsStats
from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations
from datetime import datetime

########## Helper Methods ##########
from scripts.functional.stats import AppDistrStats
from atipAutoFrwk.services.atip.stats.ValidateStatsService import ValidateStatsService




########## Helper Methods ##########
def checkPieStatsValues(statsType, bpsBytesStats, totalBytes):
    atipStats = Pie.getTopStats(atipSession, statsType)
    for atipStat in atipStats.data:
        share = bpsBytesStats[atipStat.name] / totalBytes
        return isclose(atipStat.y, share, rel_tol=0.1)

def checkGeosInstalledApp(bpsTestConfig, atipSession, bpsComponentsStats):

    for component, appSimConfig in bpsTestConfig.appSimComponents.items():
        crtApp = appSimConfig.appType
        crtAppName = crtApp.appName
        checkListStats(StatsType.GEO, crtApp, appSimConfig, atipSession=atipSession, bpsTestConfig=bpsTestConfig,
                       bpsComponentsStats=bpsComponentsStats)

def checkSpecificInstalledApp(atipStatsType, bpsTestConfig):
    '''
    :param atipStatsType: the ATIP window stats type to be validated
    e.g. StatsType.BROWSER
    '''
    print("Check specific installed apps")
    for component, appSimConfig in bpsTestConfig.appSimComponents.items():
        crtApp = appSimConfig.appType
        crtAppName = crtApp.appName
        # compute bps values

        bpsStatsByApp = BpsComponentStatsService.filterStatsByType(bpsTestConfig, bpsComponentsStats, StatsType.APPS,
                                                                   crtAppName)

        bpsSessionsByStat = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp,
                                                                          BpsStatsType.SESSIONS, atipStatsType)

        bpsPacketsByStat = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp,
                                                                         BpsStatsType.PACKETS,
                                                                         atipStatsType)
        print("!{} {}".format(bpsPacketsByStat, bpsSessionsByStat))
        bpsBytesByStat = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp, BpsStatsType.BYTES,
                                                                       atipStatsType)
        print("BPS Bytes by stat: {}".format(bpsBytesByStat))
        # check the values
        print("A intrat!")
        checkListStats(atipStatsType, crtApp, appSimConfig, bpsSessionsByStat, bpsPacketsByStat,
                       bpsBytesByStat)
        print("A iesit!")


def checkPieStats(statsType, bpsBytesStats, totalBytes):
    checkStatValue = lambda: checkPieStatsValues(statsType, bpsBytesStats, totalBytes)
    logActualValue = lambda: logging.exception('Found incorrect values for {}. Actual: {} expected: {}'.format(
        statsType,
        jsonpickle.encode(Pie.getTopStats(atipSession, statsType)),
        jsonpickle.encode(bpsBytesStats)))
    TestCondition.waitUntilTrue(checkStatValue,
                                'Found incorrect values for {}. Check logs for actual values'.format(statsType),
                                onFailure=logActualValue)

def checkListStatsValues(statsType, crtApp, appSimConfig, bpsSessions, bpsPackets, bpsBytes):
    atipStatsByApp = Lists.getTopStats(atipSession, statsType, appType=crtApp)
    print("{} STATS BY APP {}: {}".format(statsType, crtApp.appName, jsonpickle.encode(atipStatsByApp)))
    print("BPS BYtes: {}".format(bpsBytes))
    for atipStat in atipStatsByApp.getList():
        print("Current country: {}".format(atipStat.msg))
        crtKeyName = appSimConfig.getKeyName(statsType)
        print("Current key name: {}".format(crtKeyName))
        print("Key: {}".format(crtKeyName))
        print('{} {} {}'.format(atipStat.msg, atipStat.totalBytes, bpsBytes[crtKeyName]))
        print('{} {} {}'.format(atipStat.msg, atipStat.totalCount, bpsSessions[crtKeyName]))
        print('{} {} {}'.format(atipStat.msg, atipStat.totalPkts, bpsPackets[crtKeyName]))

        if atipStat.totalCount == None:
            atipStat.totalCount = 0

        expectedBytes = StatsService.getBytesForAckPacketLoss(bpsBytes[crtKeyName], bpsPackets[crtKeyName],
                                                              atipStat.totalPkts)

        sessionsOK = isclose(atipStat.totalCount, bpsSessions[crtKeyName], rel_tol=0.1)
        bytesOK = isclose(atipStat.totalBytes, expectedBytes, rel_tol=0.1)
        return sessionsOK and bytesOK


def checkListGeoValues(crtApp, appSimConfig, atipSession, bpsTestConfig, bpsComponentsStats):

    atipStatsByApp = Lists.getTopStats(atipSession, StatsType.GEO, appType=crtApp)
    clientLocation = appSimConfig.clientLocation.countryName
    serverLocation = appSimConfig.serverLocation.countryName
    currentApp = appSimConfig.appType.appName
    ValidateStatsService.checkTargetExistsInTopStats(atipStatsByApp, clientLocation)
    ValidateStatsService.checkTargetExistsInTopStats(atipStatsByApp, serverLocation)



    for country in atipStatsByApp.getList():
        atipCountryName = getattr(country, TopStats.MESSAGE.statType)

        if atipCountryName == appSimConfig.clientLocation.countryName:

            result = dict()
            result[appSimConfig.name] = dict()
            result[appSimConfig.name][TopStats.CLIENT_BYTES] = ValidateStatsService.computeClientBytesPerItem(
                appSimConfig)

            sessions = getattr(bpsComponentsStats[appSimConfig.name], BpsStatsType.SESSIONS.statsName)
            totalCountOK = ValidateStatsService.checkAtipListStats(atipStatsByApp, appSimConfig.clientLocation.countryName,
                                                                   TopStats.TOTAL_COUNT,
                                                                   sessions)
            bytesOK = ValidateStatsService.checkTopStatsSideBytesPerItem(country, bpsComponentsStats[appSimConfig.name],
                                                                         result,
                                                                         appSimConfig, TopStats.CLIENT_BYTES)

        elif atipCountryName == appSimConfig.serverLocation.countryName:

            result = dict()
            result[appSimConfig.name] = dict()
            result[appSimConfig.name][TopStats.SERVER_BYTES] = ValidateStatsService.computeServerBytesPerItem(appSimConfig)

            bytesOK = ValidateStatsService.checkTopStatsSideBytesPerItem(country, bpsComponentsStats[appSimConfig.name],
                                                              result,
                                                              appSimConfig, TopStats.SERVER_BYTES)
            totalCountOK = ValidateStatsService.checkAtipListStats(atipStatsByApp, appSimConfig.serverLocation.countryName,
                                                                   TopStats.TOTAL_COUNT,
                                                                   0)

    return bytesOK and totalCountOK

def checkListStats(statsType, crtApp, appSimConfig, bpsSessions = -1, bpsPackets = -1,
                   bpsBytes = -1, atipSession = None, bpsTestConfig = None, bpsComponentsStats = None):

    if statsType == StatsType.GEO:
        checkStatValue = lambda: checkListGeoValues(crtApp, appSimConfig, atipSession, bpsTestConfig, bpsComponentsStats)
    else:
        checkStatValue = lambda: checkListStatsValues(statsType, crtApp, appSimConfig, bpsSessions, bpsPackets, bpsBytes)
    logActualValue = lambda: logging.exception(
        'Found incorrect values for {}. Actual: {}. Bps sessions: {}. Bps packets: {}. Bps bytes: {}'.format(
            statsType,
            jsonpickle.encode(Lists.getTopStats(atipSession, statsType, appType=crtApp)),
            jsonpickle.encode(bpsSessions),
            jsonpickle.encode(bpsPackets),
            jsonpickle.encode(bpsBytes)))

    TestCondition.waitUntilTrue(checkStatValue,
                                'Found incorrect values for {}. Check logs for actual values'.format(statsType),
                                onFailure=logActualValue)


def checkTrafficInstalledApp(atipSession, bpsSession, testId, bpsComponentsStats, bpsTestConfig):
    bpsContainer = BpsStats.getBpsRealTimeStatsContainer(bpsSession, testId)
    print("BPS Container: {}".format(bpsContainer.getList(bpsContainer)))
    (isOKS, isOKB) = ValidateStatsService.compareSessionRate(atipSession, bpsContainer, bpsComponentsStats, bpsTestConfig)
    print("{} {}".format(isOKS, isOKB))
    if (isOKB, isOKS) != (True, True):
        raise Exception("Different values!")

########## Test Suite Setup ##########
# Define test config
# Define ATIP config
atipConfig = AtipConfig()
# Define bps config
bpsTestConfig = BpsTestConfig('TopBrowserTopDevices_auto', 80)
# bpsTestConfig = BpsTestConfig('TopBrowserTopDevices_auto', 80)

bpsTestConfig.addAppSim(AppSimConfig('appsim_1', ApplicationType.AOL, OSType.Linux, BrowserType.BonEcho,
                                     GeoLocation.CANADA,
                                     GeoLocation.UNITED_STATES, PacketSizes(70, 364, 450, 66)
                                     ))

bpsTestConfig.addAppSim(AppSimConfig('appsim_2', ApplicationType.NETFLIX, OSType.Linux, BrowserType.SeaMonkey,
                                     GeoLocation.CANADA,
                                     GeoLocation.UNITED_STATES, PacketSizes(70, 517, 450, 66)
                                     ))
bpsTestConfig.addAppSim(AppSimConfig('appsim_3', ApplicationType.FACEBOOK, OSType.MacOS, BrowserType.SeaMonkey,
                                     GeoLocation.CANADA,
                                     GeoLocation.UNITED_STATES, PacketSizes(70, 643, 450, 66)
                                     ))

# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
bpsSession = WebApiSession(env.bpsConnectionConfig)

# Configure ATIP
Login.login(atipSession)
AtipConfigService.createConfig(atipSession, atipConfig)
# Configure BPS
BpsLogin.login(bpsSession)
PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)

########## Test Setup ##########
# Clear stats on ATIP
System.resetStats(atipSession)
# Send traffic
bpsTestConfig.testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup,
                                                bpsTestConfig.testDuration)

bpsComponentsStats = BpsComponentStats.getAllComponentsStats(bpsSession, bpsTestConfig)
totalBytes = BpsComponentStatsService.sumStatItem(bpsComponentsStats, BpsStatsType.BYTES)
bytesByDevices = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsComponentsStats, BpsStatsType.BYTES,
                                                            StatsType.DEVICES)
bytesByBrowsers = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsComponentsStats, BpsStatsType.BYTES,
                                                             StatsType.BROWSERS)

print('bps stats: {}'.format(jsonpickle.encode(bpsComponentsStats)))
print('totalBytes: {}'.format(totalBytes))
print('by devices: {}'.format(bytesByDevices))
print('by browsers: {}'.format(bytesByBrowsers))

# Check Dashboard > TOP Browsers
checkPieStats(StatsType.BROWSERS, bytesByBrowsers, totalBytes)

# Check Dashboard > TOP Devices by OS
checkPieStats(StatsType.DEVICES, bytesByDevices, totalBytes)

atipStatsByApp = Ports.getTopStats(atipSession, StatsType.TRAFFIC, timeInterval=TimeInterval.HOUR, limit=DashboardType.MINIMIZED.stringLimit)

checkSpecificInstalledApp(StatsType.DEVICES, bpsTestConfig)
checkSpecificInstalledApp(StatsType.BROWSERS, bpsTestConfig)
checkGeosInstalledApp(bpsTestConfig, atipSession, bpsComponentsStats)
checkTrafficInstalledApp(atipSession, bpsSession, bpsTestConfig.testId, bpsComponentsStats, bpsTestConfig)


########## Test Suite Teardown ##########
Logout.logout(atipSession)
