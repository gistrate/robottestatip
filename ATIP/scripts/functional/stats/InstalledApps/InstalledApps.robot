*** Settings ***
Documentation  Installed apps basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Lists
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.webApi.traffic.BpsStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    ../lib/StatsDefaultConfig.py

Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Run BPS Test
Suite Teardown     logout  ${atipSession}

*** Test Cases ***
Validate Top Geos
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkGeosInstalledApp  ${atipSession}  ${timeInterval}  ${bpsConfig}  ${bpsComponentsStats}

Validate Top Devices
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkSpecificInstalledApp  ${atipSession}  ${timeInterval}  ${typeDevices}  ${bpsConfig}  ${bpsComponentsStats}

Validate Top Browsers
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkSpecificInstalledApp  ${atipSession}  ${timeInterval}  ${typeBrowsers}  ${bpsConfig}  ${bpsComponentsStats}

Validate Bandwidh
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  checkTrafficInstalledApp  ${atipSession}  ${bpsSession}  ${bpsConfig.testId}  ${bpsComponentsStats}  ${bpsConfig}






*** Keywords ***
Configure ATIP And Run BPS Test
    configureAtip  ${atipSession}  ${atipConfig}
    resetStats  ${atipSession}
    Log  ${testType}
    Log  ${validate}
    ${bpsConfig} = 	getBpsTestConfig  ${bpsTestConfig}
    set suite variable  ${timeIntervalList}  ${defaultTimeInterval}
    set suite variable  ${bpsConfig}   ${bpsConfig}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    login  ${atipSession}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsConfig}
    set suite variable  ${bpsComponentsStats}  ${bpsComponentsStats}
    Log  ${bpsComponentsStats}
    ${bpsContainer}  getBpsRealTimeStatsContainer  ${bpsSession}  ${bpsConfig.testId}
    set suite variable  ${bpsContainer}  ${bpsContainer}
    Log  ${bpsContainer}
