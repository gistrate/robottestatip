from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from robot.libraries.BuiltIn import BuiltIn
import sys
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig


class SuiteConfig(StatsDefaultConfig):

    statsType = StatsType.APPS
    # Define BPS config

    bpsTestConfig1 = BpsTestConfig('TopGeos_auto', 80)
    bpsTestConfig2 = BpsTestConfig('TopGeos_auto_40min', 2500)
    bpsTestConfig3 = BpsTestConfig('TopGeos_auto_HW', 80)
    bpsTestConfig4 = BpsTestConfig('TopGeos_auto_40min_HW', 2500)
    appsim1=AppSimConfig('appsim_1', ApplicationType.AMAZON, OSType.Linux, BrowserType.BonEcho, GeoLocation.CANADA,
                     GeoLocation.UNITED_STATES, PacketSizes(70, 362, 450, 66))
    appsim2=AppSimConfig('appsim_2', ApplicationType.NETFLIX, OSType.Linux, BrowserType.SeaMonkey,
                                         GeoLocation.UNITED_STATES, GeoLocation.CANADA, PacketSizes(70, 517, 450, 66))
    appsim3=AppSimConfig('appsim_3', ApplicationType.FACEBOOK, OSType.MacOS, BrowserType.SeaMonkey, GeoLocation.AUSTRALIA,
                     GeoLocation.AUSTRALIA, PacketSizes(70, 643, 450, 66))
    appsim4=AppSimConfig('appsim_4', ApplicationType.LINKEDIN, OSType.Linux, BrowserType.BonEcho, GeoLocation.CANADA,
                     GeoLocation.UNITED_STATES, PacketSizes(70, 464, 450, 66))


    bpsTestConfig1.addAppSim([appsim1, appsim2, appsim3, appsim4])
    bpsTestConfig2.addAppSim([appsim1, appsim2, appsim3, appsim4])
    bpsTestConfig3.addAppSim([appsim1, appsim2, appsim3, appsim4])
    bpsTestConfig4.addAppSim([appsim1, appsim2, appsim3, appsim4])
    bpsTestConfigHW = [bpsTestConfig3, bpsTestConfig4]
    bpsTestConfigVirtual = [bpsTestConfig1, bpsTestConfig2]

    if  StatsDefaultConfig.envVars.atipType == StatsDefaultConfig.virtual:
        bpsTestConfig = bpsTestConfigVirtual
    else:
        bpsTestConfig = bpsTestConfigHW









