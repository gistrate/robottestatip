*** Settings ***
Documentation  App distribution basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.SystemService
Library    ../lib/StatsDefaultConfig.py
Library    Collections

Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Run BPS Test
Suite Teardown     logout  ${atipSession}

*** Test Cases ***
Validate Session Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipSessions}

Validate Total Packets Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipTotalPkts}

Validate Total Bytes Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipTotalBytes}

Validate Share Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipShare}




*** Keywords ***
Configure ATIP And Run BPS Test
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    ${response} =  readDebug  ${atipSession}
    Log  ${response}
    ConfigureAtip  ${atipSession}  ${atipConfig}
    resetStats  ${atipSession}
    Log  ${testType}
    Log  ${validate}
    ${atipStats} =  create dictionary
    ${bpsConfig} = 	getBpsTestConfig  ${bpsTestConfig}
    Log  ${bpsConfig}
    ${timeIntervalList} = 	getTimeIntervals
    set suite variable  ${bpsConfig}   ${bpsConfig}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    login  ${atipSession}
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  Log  ${timeInterval}
        \  ${stat}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}
        \  ${atipStats}  setAtipStats  ${timeInterval}  ${stat}  ${atipStats}
    Log  ${atipStats}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsConfig}
    set suite variable  ${atipStats}   ${atipStats}
    set suite variable  ${bpsComponentsStats}   ${bpsComponentsStats}
    ${response} =  readDebug  ${atipSession}
    Log  ${response}






