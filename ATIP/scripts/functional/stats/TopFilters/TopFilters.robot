*** Settings ***
Documentation  TopFilters basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    ../lib/StatsDefaultConfig.py



Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Run BPS Test
Suite Teardown     Clear Filter Config


*** Test Cases ***
Validate Filter Name
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkFilterExistsInTopStats  ${atipSession}  ${facebookFilter}  ${timeInterval}
    \   checkFilterExistsInTopStats  ${atipSession}  ${facebookFilter}  ${timeInterval}  ${maximizedDashboard}

Validate Session Statistics
    [Setup]  Setup Validate Session Statistics
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkAtipTopStatsTotalSessions  ${atipSession}  ${tcpFilter}  ${bpsTotalSessions}  ${timeInterval}
    \   checkAtipTopStatsTotalSessions  ${atipSession}  ${tcpFilter}  ${bpsTotalSessions}  ${timeInterval}  0  ${maximizedDashboard}

Validate Share Statistics
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkAtipTopStatsTotalShare  ${atipSession}  0  ${timeInterval}
    \   checkAtipTopStatsTotalShare  ${atipSession}  0  ${timeInterval}  ${maximizedDashboard}

Validate Total Packets Statistics
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkAtipTopStatsTotalPkts  ${atipSession}  ${tcpFilter}  ${timeInterval}

Validate Total Client Packets Statistics
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkAtipTopStatsTotalClientPkts  ${atipSession}  ${tcpFilter}  ${timeInterval}  ${maximizedDashboard}

Validate Total Server Packets Statistics
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkAtipTopStatsTotalServerPkts  ${atipSession}  ${tcpFilter}  ${timeInterval}  ${maximizedDashboard}

Validate Total Bytes Statistics
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkAtipTopStatsTotalBytes  ${atipSession}  ${tcpFilter}  ${bpsSession}  ${bpsConfig}  ${timeInterval}
    \   checkAtipTopStatsTotalBytes  ${atipSession}  ${tcpFilter}  ${bpsSession}  ${bpsConfig}  ${timeInterval}  ${maximizedDashboard}

Validate Client Bytes Statistics
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkAtipTopStatsTotalClientBytes  ${atipSession}  ${tcpFilter}  ${bpsConfig}  ${timeInterval}  ${maximizedDashboard}

Validate Server Bytes Statistics
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
    \   checkAtipTopStatsTotalServerBytes  ${atipSession}  ${tcpFilter}  ${bpsConfig}  ${timeInterval}  ${maximizedDashboard}

*** Keywords ***
Configure ATIP And Run BPS Test
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    configureAtip  ${atipSession}  ${atipConfig}  ${envVars}
    resetStats  ${atipSession}
    Log  ${testType}
    Log  ${validate}
    ${bpsConfig} = 	getBpsTestConfig  ${bpsTestConfig}
    ${timeIntervalList} = 	getTimeIntervals
    set suite variable  ${bpsConfig}   ${bpsConfig}
    set suite variable  ${timeIntervalList}  ${timeIntervalList}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    login  ${atipSession}
    
Setup Validate Session Statistics
    ${totalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsConfig}
    Set Test Variable  ${bpsTotalSessions}  ${totalSessions}

Clear Filter Config
    deleteAllFilters  ${atipSession}
    logout  ${atipSession}

