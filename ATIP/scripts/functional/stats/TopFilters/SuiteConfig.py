from http import HTTPStatus

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig


class SuiteConfig(StatsDefaultConfig):

    maximizedDashboard = DashboardType.MAXIMIZED
    # ATIP config
    facebookFilter = FilterConfig("Facebook_Test")
    FilterConditionConfig.addAppCondition(facebookFilter, ApplicationType.FACEBOOK)
    tcpFilter = FilterConfig("TCP_Test")
    FilterConditionConfig.addProtocolCondition(tcpFilter, NetworkProtocol.TCP)
    atipConfig = AtipConfig()
    atipConfig.addFilters([facebookFilter, tcpFilter])
    # Test: Validate Filter Name
    bpsTestConfig1 = BpsTestConfig('TopFilters_auto', 15)
    bpsTestConfig2 = BpsTestConfig('TopFilters_auto_40min', 2500)
    bpsTestConfig3 = BpsTestConfig('TopFilters_auto_HW', 70)
    bpsTestConfig4 = BpsTestConfig('TopFilters_auto_40min_HW', 2500)
    bpsTestConfigHW = [bpsTestConfig3, bpsTestConfig4]
    bpsTestConfigVirtual = [bpsTestConfig1, bpsTestConfig2]

    if StatsDefaultConfig.envVars.atipType == StatsDefaultConfig.virtual:
        bpsTestConfig = bpsTestConfigVirtual
    else:
        bpsTestConfig = bpsTestConfigHW
