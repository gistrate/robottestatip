from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.ServiceProvider import ServiceProvider
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig


class SuiteConfig(StatsDefaultConfig):

    statsType = StatsType.TRAFFIC
    tolerance = 0.15

    # Define BPS config
    bpsTestConfig1 = BpsTestConfig('ASName_usa_facebook_auto', 70)
    appsim1= AppSimConfig('appsim_1', ApplicationType.FACEBOOK, OSType.MacOS, BrowserType.Chrome,
                                         clientLocation=GeoLocation.UNITED_STATES,
                                         serverLocation=GeoLocation.UNITED_STATES,
                                         requestPacketValues=PacketSizes(getSize=643, okSize=450),
                                         serviceProvider=ServiceProvider.FACEBOOK)

    bpsTestConfig2 = BpsTestConfig('ASName_usa_facebook_auto_2h30min', 9100)
    bpsTestConfig3 = BpsTestConfig('ASName_usa_facebook_auto_HW', 70)
    bpsTestConfig4 = BpsTestConfig('ASName_usa_facebook_auto_2h30min_HW', 9100)
    bpsTestConfig1.addAppSim(appsim1)
    bpsTestConfig2.addAppSim(appsim1)
    bpsTestConfig3.addAppSim(appsim1)
    bpsTestConfig4.addAppSim(appsim1)
    bpsTestConfigHW = [bpsTestConfig3, bpsTestConfig4]
    bpsTestConfigVirtual = [bpsTestConfig1, bpsTestConfig2]

    if StatsDefaultConfig.envVars.atipType == StatsDefaultConfig.virtual:
        bpsTestConfig = bpsTestConfigVirtual
    else:
        bpsTestConfig = bpsTestConfigHW





