*** Settings ***
Documentation  Traffic window basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Ports
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.webApi.traffic.BpsStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    ../lib/StatsDefaultConfig.py

Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Run BPS Test
Suite Teardown     logout  ${atipSession}

*** Test Cases ***
Validate Traffic Window Minimized
    :FOR    ${key}    IN    @{atipStatsMin.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
        \  compareSessionRate  ${atipStat}  ${bpsContainer}  ${bpsComponentsStats}  ${tolerance}

Validate Traffic Window Maximized
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
        \  compareSessionRate  ${atipStat}  ${bpsContainer}  ${bpsComponentsStats}  ${tolerance}




*** Keywords ***
Configure ATIP And Run BPS Test
    configureAtip  ${atipSession}  ${atipConfig}
    resetStats  ${atipSession}
    Log  ${testType}
    Log  ${validate}
    ${bpsConfig} = 	getBpsTestConfig  ${bpsTestConfig}
    ${timeIntervalList} = 	getTimeIntervals
    set suite variable  ${bpsConfig}   ${bpsConfig}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    login  ${atipSession}
    ${atipStatsMin} =  create dictionary
    ${atipStatsMax} =  create dictionary
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  Log  ${timeInterval}
        \  ${statsMin}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}  ${minimizedDashboard}
        \  ${statsMax}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}  ${maximizedDashboard}
        \  ${atipStatsMin}  setAtipStats  ${timeInterval}  ${statsMin}  ${atipStatsMin}
        \  ${atipStatsMax}  setAtipStats  ${timeInterval}  ${statsMax}  ${atipStatsMax}
    Log  ${atipStatsMin}
    Log  ${atipStatsMax}
    set suite variable  ${atipStatsMin}  ${atipStatsMin}
    set suite variable  ${atipStatsMax}  ${atipStatsMax}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsConfig}
    set suite variable  ${bpsComponentsStats}  ${bpsComponentsStats}
    Log  ${bpsComponentsStats}
    ${bpsContainer}  getBpsRealTimeStatsContainer  ${bpsSession}  ${bpsConfig.testId}
    set suite variable  ${bpsContainer}  ${bpsContainer}
    Log  ${bpsContainer}
