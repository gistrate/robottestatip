from cmath import isclose
from http import HTTPStatus

import jsonpickle
import logging
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.stats.StatsService import StatsService
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.services.traffic.BpsComponentStatsService import BpsComponentStatsService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.webApi.atip.stats.Maps import Maps
from atipAutoFrwk.webApi.atip.stats.Lists import Lists
from atipAutoFrwk.webApi.traffic.BpsComponentStats import BpsComponentStats
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations
from lib.ValidateStatsService import ValidateStatsService


########## Helper Methods ##########


def checkGeoExistsInTopStats(countryName, timeInterval=TimeInterval.HOUR, expectedStatus=HTTPStatus.OK):
    checkAtipExists = lambda: countryName in str(
        jsonpickle.encode(Maps.getTopStats(atipSession, timeInterval, expectedStatus)))
    TestCondition.waitUntilTrue(checkAtipExists, 'Couldn\'t find geo: {}'.format(countryName),
                                totalTime=StatsTimeout.TOP_FILTERS.waitTime,
                                iterationTime=StatsTimeout.ITERATION_TIME.waitTime)
    print('Found country {} in Top Countries dashboard'.format(countryName))


########## Test Suite Setup ##########
# Define test config
# Define ATIP config
atipConfig = AtipConfig()
# Define bps config
bpsTestConfig = BpsTestConfig('TopGeos_auto', 65)
bpsTestConfig.addAppSim(AppSimConfig('appsim_1', ApplicationType.AOL, OSType.Linux, BrowserType.BonEcho, GeoLocation.CANADA, GeoLocation.UNITED_STATES, PacketSizes(70,364,450,66)))
bpsTestConfig.addAppSim(AppSimConfig('appsim_2', ApplicationType.NETFLIX, OSType.Linux, BrowserType.SeaMonkey, GeoLocation.UNITED_STATES, GeoLocation.CANADA, PacketSizes(70,517,450,66)))
bpsTestConfig.addAppSim(AppSimConfig('appsim_3', ApplicationType.FACEBOOK, OSType.MacOS, BrowserType.SeaMonkey, GeoLocation.AUSTRALIA, GeoLocation.AUSTRALIA, PacketSizes(70,643,450,66)))
bpsTestConfig.addAppSim(AppSimConfig('appsim_4', ApplicationType.LINKEDIN, OSType.Linux, BrowserType.BonEcho, GeoLocation.CANADA, GeoLocation.UNITED_STATES, PacketSizes(70,464,450,66)))


# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
bpsSession = WebApiSession(env.bpsConnectionConfig)

# Configure ATIP
Login.login(atipSession)
AtipConfigService.createConfig(atipSession, atipConfig)
# Configure BPS
BpsLogin.login(bpsSession)
PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)

########## Test Setup ##########
# Clear stats on ATIP
System.resetStats(atipSession)
# Send traffic
bpsTestConfig.testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup, bpsTestConfig.testDuration)

bpsComponentsStats = BpsComponentStats.getAllComponentsStats(bpsSession, bpsTestConfig)

atipStats = Maps.getTopStats(atipSession, timeInterval=TimeInterval.FIVEMIN, limit='50', appType=None, expectedStatus=HTTPStatus.OK)

print("Verify all country names are present in maximized window")
checkGeoExistsInTopStats('Australia', timeInterval=TimeInterval.FIVEMIN)
checkGeoExistsInTopStats('Canada', timeInterval=TimeInterval.FIVEMIN)
checkGeoExistsInTopStats('United States', timeInterval=TimeInterval.FIVEMIN)

print("Session stats validation")
print("Compute stats for Australia (both client and server)")
totalSessions = BpsComponentStatsService.sumStatItem(bpsComponentsStats, BpsStatsType.SESSIONS)
bpsSessionsAustralia = getattr(bpsComponentsStats['appsim_3'], BpsStatsType.SESSIONS.statsName)
ValidateStatsService.checkAtipListStats(atipStats,'Australia', TopStats.TOTAL_COUNT, bpsSessionsAustralia)

print("Compute stats for Canada (client for linkedin and aol; server for netflix)")
bpsSessionsCanada = totalSessions - bpsSessionsAustralia
ValidateStatsService.checkAtipListStats(atipStats,'Canada', TopStats.TOTAL_COUNT, bpsSessionsCanada)

print("Compute stats for US (client for netflix; server for linkedin and aol)")
bpsSessionsUS = totalSessions - bpsSessionsAustralia
ValidateStatsService.checkAtipListStats(atipStats, 'United States', TopStats.TOTAL_COUNT, bpsSessionsUS)


print("Client packets stats validation")
ValidateStatsService.checkAtipTopStatsClientPkts(atipStats, bpsTestConfig, bpsComponentsStats, 'Canada')
ValidateStatsService.checkAtipTopStatsClientPkts(atipStats, bpsTestConfig, bpsComponentsStats, 'Australia')
ValidateStatsService.checkAtipTopStatsClientPkts(atipStats, bpsTestConfig, bpsComponentsStats, 'United States')

print("Server packets stats validation")
ValidateStatsService.checkAtipTopStatsServerPkts(atipStats,bpsTestConfig, bpsComponentsStats,'United States')
ValidateStatsService.checkAtipTopStatsServerPkts(atipStats,bpsTestConfig, bpsComponentsStats,'Canada')
ValidateStatsService.checkAtipTopStatsServerPkts(atipStats,bpsTestConfig, bpsComponentsStats,'Australia')


print("Verify client bytes")
ValidateStatsService.checkAtipTopStatsClientBytes(atipStats,bpsTestConfig, bpsComponentsStats,'Canada')
ValidateStatsService.checkAtipTopStatsClientBytes(atipStats,bpsTestConfig, bpsComponentsStats,'United States')
ValidateStatsService.checkAtipTopStatsClientBytes(atipStats,bpsTestConfig, bpsComponentsStats,'Australia')

print("Verify server bytes")
ValidateStatsService.checkAtipTopStatsServerBytes(atipStats,bpsTestConfig, bpsComponentsStats,'Canada')
ValidateStatsService.checkAtipTopStatsServerBytes(atipStats,bpsTestConfig, bpsComponentsStats,'United States')
ValidateStatsService.checkAtipTopStatsServerBytes(atipStats,bpsTestConfig, bpsComponentsStats,'Australia')





########## Test Suite Teardown ##########
Logout.logout(atipSession)
