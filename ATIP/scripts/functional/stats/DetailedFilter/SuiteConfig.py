from http import HTTPStatus

from robot.libraries.BuiltIn import BuiltIn

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.services.IPService import IPService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.data.atip.ServiceProvider import ServiceProvider
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol


class SuiteConfig(StatsDefaultConfig):

    typeServers = StatsType.SERVERS
    typeClients = StatsType.CLIENTS
    typeDevice = StatsType.DEVICE
    typeTraffic = StatsType.TRAFFIC

    # ATIP config
    facebookFilter = FilterConfig("Facebook_Test")
    FilterConditionConfig.addAppCondition(facebookFilter, ApplicationType.FACEBOOK)
    tcpFilter = FilterConfig("TCP_Test")
    FilterConditionConfig.addProtocolCondition(tcpFilter, NetworkProtocol.TCP)
    atipConfig = AtipConfig()
    atipConfig.addFilters([facebookFilter, tcpFilter])

    # Test: Validate Filter Name
    bpsTestConfig1 = BpsTestConfig('DetailedFilters_auto', 70)
    # bpsTestConfig2 = BpsTestConfig('DetailedFilters_auto_40min', 2500)
    bpsTestConfig2 = BpsTestConfig('DetailedFilters_auto_2h30min', 9100)
    bpsTestConfig3 = BpsTestConfig('DetailedFilters_auto_HW', 70)
    # bpsTestConfig4 = BpsTestConfig('DetailedFilters_auto_40min_HW', 2500)
    bpsTestConfig4 = BpsTestConfig('DetailedFilters_auto_2h30min_HW', 9100)

    appsim3 = AppSimConfig('appsim_4', ApplicationType.AOL, OSType.Linux, BrowserType.BonEcho,
                                         GeoLocation.CHINA,
                                         GeoLocation.CHINA, PacketSizes(70, 364, 450, 66)
                                         )

    appsim2 = AppSimConfig('appsim_3', ApplicationType.NETFLIX, OSType.Linux, BrowserType.SeaMonkey,
                                         GeoLocation.CHINA,
                                         GeoLocation.CHINA, PacketSizes(70, 517, 450, 66)
                                         )
    appsim1 = AppSimConfig('appsim_1', ApplicationType.FACEBOOK, OSType.MacOS, BrowserType.SeaMonkey,
                                         GeoLocation.CHINA,
                                         GeoLocation.CHINA, PacketSizes(70, 534, 450, 66)
                                         )
    bpsTestConfig1.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfig2.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfig3.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfig4.addAppSim([appsim1, appsim2, appsim3])
    appSimList = bpsTestConfig1.getAppSimsFromAppFilter(facebookFilter)
    devices = []
    clientIPs = IPService.genIpRange("1.80.0.2", "1.80.0.11")
    serverIPs = IPService.genIpRange("1.80.1.2", "1.80.1.11")
    devices.append('MacOS')

    # Test: Validate Filter Name
    bpsTestConfigHW = [bpsTestConfig3, bpsTestConfig4]
    bpsTestConfigVirtual = [bpsTestConfig1, bpsTestConfig2]

    if StatsDefaultConfig.envVars.atipType == StatsDefaultConfig.virtual:
        bpsTestConfig = bpsTestConfigVirtual
    else:
        bpsTestConfig = bpsTestConfigHW




