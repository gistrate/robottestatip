*** Settings ***
Documentation  Detailed Filter Stats basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Filters  WITH NAME  StatsFilters
Library    atipAutoFrwk.webApi.atip.stats.Ports  WITH NAME  Ports
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    ../lib/StatsDefaultConfig.py
Library    Collections

Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Run BPS Test
Suite Teardown     Perform cleanup

*** Test Cases ***
Validate Top Clients
    :FOR    ${key}    IN    @{clientsMin.keys()}
        \  Log  ${key}
        \  ${clientStat}  getAtipStats  ${key}  ${clientsMin}
        \  Log  ${clientStat}
        \  ${totalClientCount}  getAtipTotalCount  ${clientStat}
        \  Log  ${totalClientCount}
        \  checkFilterStat  ${totalClientCount}  ${bpsComponentsStats}  ${appSimList}  ${bpsSessions}
        \  checkClientPackets  ${clientStat}
        \  checkFilterClientBytes  ${clientStat}  ${bpsConfig}  ${appSimList}

    :FOR    ${key}    IN    @{clientsMax.keys()}
        \  Log  ${key}
        \  ${clientStat}  getAtipStats  ${key}  ${clientsMax}
        \  Log  ${clientStat}
        \  ${totalClientCount}  getAtipTotalCount  ${clientStat}
        \  Log  ${totalClientCount}
        \  checkFilterStat  ${totalClientCount}  ${bpsComponentsStats}  ${appSimList}  ${bpsSessions}
        \  checkClientPackets  ${clientStat}
        \  checkFilterClientBytes  ${clientStat}  ${bpsConfig}  ${appSimList}

Validate Top Servers
    :FOR    ${key}    IN    @{serversMin.keys()}
        \  Log  ${key}
        \  ${serverStat}  getAtipStats  ${key}  ${serversMin}
        \  Log  ${serverStat}
        \  ${totalServerCount}  getAtipTotalCount  ${serverStat}
        \  checkFilterStat  ${totalServerCount}  ${bpsComponentsStats}  ${appSimList}  ${bpsSessions}
        \  checkServerPackets  ${serverStat}
        \  checkFilterServerBytes  ${serverStat}  ${bpsConfig}  ${appSimList}

    :FOR    ${key}    IN    @{serversMax.keys()}
        \  Log  ${key}
        \  ${serverStat}  getAtipStats  ${key}  ${serversMax}
        \  Log  ${serverStat}
        \  ${totalServerCount}  getAtipTotalCount  ${serverStat}
        \  checkFilterStat  ${totalServerCount}  ${bpsComponentsStats}  ${appSimList}  ${bpsSessions}
        \  checkServerPackets  ${serverStat}
        \  checkFilterServerBytes  ${serverStat}  ${bpsConfig}  ${appSimList}

Validate Top Devices
    :FOR    ${key}    IN    @{devicesMin.keys()}
        \  Log  ${key}
        \  ${deviceStat}  getAtipStats  ${key}  ${devicesMin}
        \  ${serverStat}  getAtipStats  ${key}  ${devicesMin}
        \  ${clientStat}  getAtipStats  ${key}  ${devicesMin}
        \  checkTopFilteredDevices  ${deviceStat}  ${appSimList}  ${bpsConfig}  ${bpsComponentsStats}  ${serverStat}  ${clientStat}

    :FOR    ${key}    IN    @{devicesMax.keys()}
        \  Log  ${key}
        \  ${deviceStat}  getAtipStats  ${key}  ${devicesMax}
        \  ${serverStat}  getAtipStats  ${key}  ${devicesMax}
        \  ${clientStat}  getAtipStats  ${key}  ${devicesMax}
        \  checkTopFilteredDevices  ${deviceStat}  ${appSimList}  ${bpsConfig}  ${bpsComponentsStats}  ${serverStat}  ${clientStat}

Validate Filtered Window
     [Tags]  skip
     # BPS is not consistent when sending sequential IPs, so we will skip the test until we upgrade to latest BPS build
     :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  checkAllDetailedFilters  ${clientIPs}  ${serverIPs}  ${devices}  ${facebookFilter}  ${atipSession}  ${bpsConfig}  ${time}  ${bpsComponentsStats}  ${bpsSession}  ${timeInterval}

Validate Traffic Window

    :FOR    ${key}    IN    @{trafficMin.keys()}
        \  Log  ${key}
        \  ${trafficStat}  getAtipStats  ${key}  ${trafficMin}
        \  checkTrafficForFilterApps  ${facebookFilter}  ${bpsConfig}  ${bpsComponentsStats}  ${trafficStat}  ${bpsSession}

    :FOR    ${key}    IN    @{trafficMax.keys()}
        \  Log  ${key}
        \  ${trafficStat}  getAtipStats  ${key}  ${trafficMax}
        \  checkTrafficForFilterApps  ${facebookFilter}  ${bpsConfig}  ${bpsComponentsStats}  ${trafficStat}  ${bpsSession}








*** Keywords ***
Configure ATIP And Run BPS Test
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    configureAtip  ${atipSession}  ${atipConfig}  ${envVars}
    resetStats  ${atipSession}
    Log  ${testType}
    Log  ${validate}
    ${bpsConfig} = 	getBpsTestConfig  ${bpsTestConfig}
    set suite variable  ${bpsConfig}   ${bpsConfig}
    ${timeIntervalList} =  getTimeIntervals  True
    set suite variable  ${timeIntervalList}  ${timeIntervalList}
    ${time} =  getCurrentTime
    set suite variable  ${time}  ${time}
    Log  ${time}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    login  ${atipSession}
    ${trafficMin} =  create dictionary
    ${trafficMax} =  create dictionary
    ${serversMin} =  create dictionary
    ${serversMax} =  create dictionary
    ${clientsMin} =  create dictionary
    ${clientsMax} =  create dictionary
    ${devicesMin} =  create dictionary
    ${devicesMax} =  create dictionary

    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  Log  ${timeInterval}
        \  ${atipTrafficStatsMin}  Ports.getTopStats  ${atipSession}  ${typeTraffic}  ${timeInterval}  ${minimizedDashboard}  ${null}  ${facebookFilter.id}
        \  ${atipTrafficStatsMax}  Ports.getTopStats  ${atipSession}  ${typeTraffic}  ${timeInterval}  ${maximizedDashboard}  ${null}  ${facebookFilter.id}
        \  ${atipServerStatsMin}  StatsFilters.getTopStats  ${atipSession}  ${typeServers}  ${timeInterval}  ${minimizedDashboard}  ${facebookFilter.id}
        \  ${atipServerStatsMax}  StatsFilters.getTopStats  ${atipSession}  ${typeServers}  ${timeInterval}  ${maximizedDashboard}  ${facebookFilter.id}
        \  ${atipClientStatsMin}  StatsFilters.getTopStats  ${atipSession}  ${typeClients}  ${timeInterval}  ${minimizedDashboard}  ${facebookFilter.id}
        \  ${atipClientStatsMax}  StatsFilters.getTopStats  ${atipSession}  ${typeClients}  ${timeInterval}  ${maximizedDashboard}  ${facebookFilter.id}
        \  ${atipDeviceStatsMin}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${facebookFilter.id}
        \  ${atipDeviceStatsMax}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${maximizedDashboard}  ${facebookFilter.id}
        \  ${trafficMin}  setAtipStats  ${timeInterval}  ${atipTrafficStatsMin}  ${trafficMin}
        \  ${trafficMax}  setAtipStats  ${timeInterval}  ${atipTrafficStatsMax}  ${trafficMax}
        \  ${serversMin}  setAtipStats  ${timeInterval}  ${atipServerStatsMin}  ${serversMin}
        \  ${serversMax}  setAtipStats  ${timeInterval}  ${atipServerStatsMax}  ${serversMax}
        \  ${clientsMin}  setAtipStats  ${timeInterval}  ${atipClientStatsMin}  ${clientsMin}
        \  ${clientsMax}  setAtipStats  ${timeInterval}  ${atipClientStatsMax}  ${clientsMax}
        \  ${devicesMin}  setAtipStats  ${timeInterval}  ${atipDeviceStatsMin}  ${devicesMin}
        \  ${devicesMax}  setAtipStats  ${timeInterval}  ${atipDeviceStatsMax}  ${devicesMax}


    set suite variable  ${serversMin}  ${serversMin}
    set suite variable  ${serversMax}  ${serversMax}
    set suite variable  ${clientsMin}  ${clientsMin}
    set suite variable  ${clientsMax}  ${clientsMax}
    set suite variable  ${devicesMin}  ${devicesMin}
    set suite variable  ${devicesMax}  ${devicesMax}
    set suite variable  ${trafficMin}  ${trafficMin}
    set suite variable  ${trafficMax}  ${trafficMax}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsConfig}
    set suite variable  ${bpsComponentsStats}  ${bpsComponentsStats}
    ${totalBytes}  sumStatItem  ${bpsComponentsStats}  ${bpsBytes}
    set suite variable  ${totalBytes}  ${totalBytes}



Perform cleanup
    deleteAllFilters  ${atipSession}
    logout  ${atipSession}
