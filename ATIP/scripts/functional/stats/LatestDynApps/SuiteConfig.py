from http import HTTPStatus

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip import Login
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig


class SuiteConfig(StatsDefaultConfig):

    statsType = StatsType.DYNAMIC

    # Define BPS config
    bpsTestConfigInit = BpsTestConfig('LatestDyn_auto_6min', 400)
    appsim3=AppSimConfig('appsim_3', ApplicationType.TECHCRUNCH, OSType.Linux, BrowserType.BonEcho, clientLocation=GeoLocation.FRANCE,
                                         serverLocation=GeoLocation.UNITED_KINGDOM, requestPacketValues=PacketSizes(okSize=410, getSize=478))
    appsim2=AppSimConfig('appsim_2', ApplicationType.BUSINESSINSIDER, OSType.Linux, BrowserType.BonEcho, clientLocation=GeoLocation.FRANCE,
                                         serverLocation=GeoLocation.UNITED_KINGDOM, requestPacketValues=PacketSizes(okSize=510, getSize=483))
    appsim1=AppSimConfig('appsim_1', ApplicationType.BOREDPANDA, OSType.Linux, BrowserType.BonEcho, clientLocation=GeoLocation.FRANCE,
                                         serverLocation=GeoLocation.UNITED_KINGDOM, requestPacketValues=PacketSizes(okSize=460, getSize=526))
    bpsTestConfigInit.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfig1 = BpsTestConfig('LatestDyn_auto', 62)
    bpsTestConfig1.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfig2 = BpsTestConfig('LatestDyn_auto_40min', 2500)
    bpsTestConfig2.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfig3 = BpsTestConfig('LatestDyn_auto_HW', 62)
    bpsTestConfig3.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfig4 = BpsTestConfig('LatestDyn_auto_40min_HW', 2500)
    bpsTestConfig4.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfigHW = [bpsTestConfig3, bpsTestConfig4]
    bpsTestConfigVirtual = [bpsTestConfig1, bpsTestConfig2]

    if StatsDefaultConfig.envVars.atipType == StatsDefaultConfig.virtual:
        bpsTestConfig = bpsTestConfigVirtual
    else:
        bpsTestConfig = bpsTestConfigHW






