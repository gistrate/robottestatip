from cmath import isclose
from http import HTTPStatus
import jsonpickle
import logging

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.ServiceProvider import ServiceProvider
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.RealTimeStats import RealTimeStats
from atipAutoFrwk.data.traffic.stats.BpsComponentStatsContainer import BpsComponentStatsContainer
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.LogoutService import LogoutService
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.services.atip.stats.StatsService import StatsService
from atipAutoFrwk.services.atip.stats.TopFiltersService import TopFiltersService
from atipAutoFrwk.services.traffic.BpsComponentStatsService import BpsComponentStatsService
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Configuration import Configuration
from atipAutoFrwk.webApi.atip.ConfigureStats import ConfigureStats
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.Status import Status
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.webApi.atip.TopFilters import TopFilters
from atipAutoFrwk.webApi.atip.stats.Lists import Lists
from atipAutoFrwk.webApi.atip.stats.Maps import Maps
from atipAutoFrwk.webApi.atip.stats.Pie import Pie
from atipAutoFrwk.webApi.traffic.BpsComponentStats import BpsComponentStats
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations
from datetime import datetime

########## Helper Methods ##########
from scripts.functional.stats import AppDistrStats
from atipAutoFrwk.services.atip.stats.ValidateStatsService import ValidateStatsService




########## Test Suite Setup ##########
# Define test config
# Define ATIP config


atipConfig = AtipConfig()
# Define bps config


bpsTestConfig = BpsTestConfig('ASName_usa_facebook_15sec', 62)
bpsTestConfig.addAppSim(AppSimConfig('appsim_1', ApplicationType.FACEBOOK, OSType.MacOS, BrowserType.Chrome,
                                     clientLocation=GeoLocation.UNITED_STATES,
                                     serverLocation=GeoLocation.UNITED_STATES,
                                     requestPacketValues=PacketSizes(getSize=643, okSize=450),
                                     serviceProvider=ServiceProvider.FACEBOOK))

# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
bpsSession = WebApiSession(env.bpsConnectionConfig)

# Configure ATIP
Login.login(atipSession)
AtipConfigService.createConfig(atipSession, atipConfig)
# Configure BPS
BpsLogin.login(bpsSession)
PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)

########## Test Setup ##########
# Clear stats on ATIP
# SystemService.clearSystemAndWaitUntilNPReady(atipSession, env)

System.resetStats(atipSession)
# Send traffic

atipconf = AtipConfig()
AtipConfigService.createConfig(atipSession, atipconf)


bpsTestConfig.testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup,
                                                bpsTestConfig.testDuration)
print(bpsTestConfig.testId)

bpsComponentsStats = BpsComponentStats.getAllComponentsStats(bpsSession, bpsTestConfig)
totalBytes = BpsComponentStatsService.sumStatItem(bpsComponentsStats, BpsStatsType.BYTES)

atipStatsByApp = Lists.getTopStats(atipSession, StatsType.PROVIDER, timeInterval=TimeInterval.FIVEMIN, limit=DashboardType.MINIMIZED.stringLimit)

totalAtipBytes = ValidateStatsService.getAtipTotalBytes(atipStatsByApp)


isOKMinimizedTP = ValidateStatsService.checkSpecificFieldStat(atipStatsByApp, bpsTestConfig, bpsComponentsStats, atipSession,
                                                              StatsType.PROVIDER,
                                                              stat=TopStats.TOTAL_PKTS)

isOKMinimizedTC = ValidateStatsService.checkSpecificFieldStat(atipStatsByApp, bpsTestConfig, bpsComponentsStats, atipSession,
                                                              StatsType.PROVIDER,
                                                              stat=TopStats.TOTAL_COUNT)

isOKMinimizedCP = ValidateStatsService.checkSpecificFieldStat(atipStatsByApp, bpsTestConfig, bpsComponentsStats, atipSession,
                                                              StatsType.PROVIDER,
                                                              stat=TopStats.CLIENT_PKTS)

isOKMinimizedTB = ValidateStatsService.checkSpecificFieldStat(atipStatsByApp, bpsTestConfig, bpsComponentsStats, atipSession,
                                                              StatsType.PROVIDER,
                                                              stat=TopStats.TOTAL_BYTES)

isOKMinimizedSB = ValidateStatsService.checkSpecificFieldStat(atipStatsByApp, bpsTestConfig, bpsComponentsStats, atipSession,
                                                              StatsType.PROVIDER,
                                                              stat=TopStats.SERVER_BYTES)

isOKMinimizedSP = ValidateStatsService.checkSpecificFieldStat(atipStatsByApp, bpsTestConfig, bpsComponentsStats, atipSession,
                                                              StatsType.PROVIDER,
                                                              stat=TopStats.SERVER_PKTS)

atipStatsByApp = Lists.getTopStats(atipSession, StatsType.PROVIDER, timeInterval=TimeInterval.FIVEMIN, limit=DashboardType.MAXIMIZED.stringLimit)

isOKMinimizedSH = ValidateStatsService.checkSpecificFieldStat(atipStatsByApp, bpsTestConfig, bpsComponentsStats, atipSession,
                                                              StatsType.PROVIDER,
                                                              stat=TopStats.SHARE)






print("1. Is OK minimized TP: {}".format(isOKMinimizedTP))
print("2. Is OK minimized TC: {}".format(isOKMinimizedTC))
print("3. Is OK minimized CP: {}".format(isOKMinimizedCP))
print("4. Is OK minimized TB : {}".format(isOKMinimizedTB))
print("5. Is OK minimized SB : {}".format(isOKMinimizedSB))
print("6. Is OK minimized SP: {}".format(isOKMinimizedSP))
print("7. Is OK minimized SH: {}".format(isOKMinimizedSH))



########## Test Suite Teardown ##########
Logout.logout(atipSession)
