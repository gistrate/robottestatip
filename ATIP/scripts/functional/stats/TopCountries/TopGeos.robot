*** Settings ***
Documentation  Top Geos basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Maps
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    ../lib/StatsDefaultConfig.py

Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Run BPS Test
Suite Teardown     logout  ${atipSession}

*** Test Cases ***
Validate Country Name
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkTargetExistsInTopStats  ${atipStat}  ${strAustralia}
        \  checkTargetExistsInTopStats  ${atipStat}  ${strUS}
        \  checkTargetExistsInTopStats  ${atipStat}  ${strCanada}


Validate Session Statistics
    [Setup]  Setup Validate Session Statistics
        :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkAtipListStats  ${atipStat}  ${strCanada}  ${atipSessions}  ${bpsSessionsCanada}
        \  checkAtipListStats  ${atipStat}  ${strAustralia}  ${atipSessions}  ${bpsSessionsAustralia}
        \  checkAtipListStats  ${atipStat}  ${strUS}  ${atipSessions}  ${bpsSessionsUS}

Validate Total Client Packets Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkAtipTopStatsClientPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strCanada}
        \  checkAtipTopStatsClientPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strUS}
        \  checkAtipTopStatsClientPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strAustralia}

Validate Total Server Packets Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkAtipTopStatsServerPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strCanada}
        \  checkAtipTopStatsServerPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strUS}
        \  checkAtipTopStatsServerPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strAustralia}

Validate Client Bytes Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkAtipTopStatsClientBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strCanada}
        \  checkAtipTopStatsClientBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strUS}
        \  checkAtipTopStatsClientBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strAustralia}

Validate Server Bytes Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkAtipTopStatsServerBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strCanada}
        \  checkAtipTopStatsServerBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strUS}
        \  checkAtipTopStatsServerBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strAustralia}


*** Keywords ***
Configure ATIP And Run BPS Test
    configureAtip  ${atipSession}  ${atipConfig}
    resetStats  ${atipSession}
    Log  ${testType}
    Log  ${validate}
    ${bpsConfig} = 	getBpsTestConfig  ${bpsTestConfig}
    ${timeIntervalList} = 	getTimeIntervals
    set suite variable  ${bpsConfig}   ${bpsConfig}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    login  ${atipSession}
    ${atipStats} =  create dictionary
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  Log  ${timeInterval}
        \  ${stat}  getTopStats  ${atipSession}  ${timeInterval}  ${maximizedDashboard}
        \  ${atipStats}  setAtipStats  ${timeInterval}  ${stat}  ${atipStats}
    Log  ${atipStats}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsConfig}
    set suite variable  ${atipStats}   ${atipStats}
    set suite variable  ${bpsComponentsStats}   ${bpsComponentsStats}

Setup Validate Session Statistics

    ${totalSessions}  sumStatItem  ${bpsComponentsStats}  ${bpsSessions}
    ${bpsSessionsComp3}  getStatItem  ${bpsComponentsStats['appsim_3']}  ${bpsSessions}
    ${bpsSessionsComp124}  Evaluate  ${totalSessions} - ${bpsSessionsComp3}
    Set Test Variable  ${bpsSessionsAustralia}  ${bpsSessionsComp3}
    Set Test Variable  ${bpsSessionsCanada}  ${bpsSessionsComp124}
    Set Test Variable  ${bpsSessionsUS}  ${bpsSessionsComp124}



