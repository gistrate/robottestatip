*** Settings ***
Documentation  Top Country basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Lists
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    ../lib/StatsDefaultConfig.py

Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Run BPS Test
Suite Teardown     logout  ${atipSession}

*** Test Cases ***
Validate Country Name

    :FOR    ${key}    IN    @{atipStatsMin.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
    \   checkTargetExistsInTopStats  ${atipStat}  ${strAustralia}
    \   checkTargetExistsInTopStats  ${atipStat}  ${strUS}
    \   checkTargetExistsInTopStats  ${atipStat}  ${strCanada}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \   checkTargetExistsInTopStats  ${atipStat}  ${strAustralia}
    \   checkTargetExistsInTopStats  ${atipStat}  ${strUS}
    \   checkTargetExistsInTopStats  ${atipStat}  ${strCanada}



Validate Session Statistics
    [Setup]  Setup Validate Session Statistics
    :FOR    ${key}    IN    @{atipStatsMin.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
    \  checkAtipListStats  ${atipStat}  ${strAustralia}  ${atipSessions}  ${bpsSessionsAustralia}
    \  checkAtipListStats  ${atipStat}  ${strUS}  ${atipSessions}  ${bpsSessionsUS}
    \  checkAtipListStats  ${atipStat}  ${strCanada}  ${atipSessions}  ${bpsSessionsCanada}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  checkAtipListStats  ${atipStat}  ${strAustralia}  ${atipSessions}  ${bpsSessionsAustralia}
    \  checkAtipListStats  ${atipStat}  ${strUS}  ${atipSessions}  ${bpsSessionsUS}
    \  checkAtipListStats  ${atipStat}  ${strCanada}  ${atipSessions}  ${bpsSessionsCanada}


Validate Total Packets Statistics
    [Setup]  Setup Validate Session Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  checkAtipTopStatsTotalPkts  ${atipStat}  ${bpsConfig}  ${strCanada}  ${bpsSessionsCanada}  ${statsType}
    \  checkAtipTopStatsTotalPkts  ${atipStat}  ${bpsConfig}  ${strUS}  ${bpsSessionsUS}  ${statsType}
    \  checkAtipTopStatsTotalPkts  ${atipStat}  ${bpsConfig}  ${strAustralia}  ${bpsSessionsAustralia}  ${statsType}


Validate Total Client Packets Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  checkAtipTopStatsClientPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strCanada}
    \  checkAtipTopStatsClientPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strUS}
    \  checkAtipTopStatsClientPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strAustralia}

Validate Total Server Packets Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  checkAtipTopStatsServerPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strCanada}
    \  checkAtipTopStatsServerPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strUS}
    \  checkAtipTopStatsServerPkts  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strAustralia}

Validate Total Bytes Statistics
    :FOR    ${key}    IN    @{atipStatsMin.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
    \  checkAtipTopStatsTotalBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strCanada}
    \  checkAtipTopStatsTotalBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strUS}
    \  checkAtipTopStatsTotalBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strAustralia}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  checkAtipTopStatsTotalBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strCanada}
    \  checkAtipTopStatsTotalBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strUS}
    \  checkAtipTopStatsTotalBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strAustralia}

Validate Client Bytes Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  checkAtipTopStatsClientBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strCanada}
    \  checkAtipTopStatsClientBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strUS}
    \  checkAtipTopStatsClientBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strAustralia}

Validate Server Bytes Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  checkAtipTopStatsServerBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strCanada}
    \  checkAtipTopStatsServerBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strUS}
    \  checkAtipTopStatsServerBytes  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${strAustralia}

Validate Share Statistics
    :FOR    ${key}    IN    @{atipStatsMin.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
    \  checkAtipTopStatsTotalShare  ${atipStat}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  checkAtipTopStatsTotalShare  ${atipStat}





*** Keywords ***
Configure ATIP And Run BPS Test
    configureAtip  ${atipSession}  ${atipConfig}
    resetStats  ${atipSession}
    Log  ${testType}
    Log  ${validate}
    ${bpsConfig} = 	getBpsTestConfig  ${bpsTestConfig}
    ${timeIntervalList} = 	getTimeIntervals
    set suite variable  ${bpsConfig}   ${bpsConfig}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    login  ${atipSession}
    ${atipStatsMin} =  create dictionary
    ${atipStatsMax} =  create dictionary
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  Log  ${timeInterval}
        \  ${statsMin}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}  ${minimizedDashboard}
        \  ${statsMax}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}  ${maximizedDashboard}
        \  ${atipStatsMin}  setAtipStats  ${timeInterval}  ${statsMin}  ${atipStatsMin}
        \  ${atipStatsMax}  setAtipStats  ${timeInterval}  ${statsMax}  ${atipStatsMax}
    Log  ${atipStatsMin}
    Log  ${atipStatsMax}
    set suite variable  ${atipStatsMin}  ${atipStatsMin}
    set suite variable  ${atipStatsMax}  ${atipStatsMax}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsConfig}
    set suite variable  ${bpsComponentsStats}  ${bpsComponentsStats}
    Log  ${bpsComponentsStats}


Setup Validate Session Statistics

    ${totalSessions}  sumStatItem  ${bpsComponentsStats}  ${bpsSessions}
    ${bpsSessionsComp3}  getStatItem  ${bpsComponentsStats['appsim_3']}  ${bpsSessions}
    ${bpsSessionsComp124}  Evaluate  ${totalSessions} - ${bpsSessionsComp3}
    Set Test Variable  ${bpsSessionsAustralia}  ${bpsSessionsComp3}
    Set Test Variable  ${bpsSessionsCanada}  ${bpsSessionsComp124}
    Set Test Variable  ${bpsSessionsUS}  ${bpsSessionsComp124}



