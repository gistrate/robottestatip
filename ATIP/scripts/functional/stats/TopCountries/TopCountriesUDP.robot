*** Settings ***
Documentation  Top Country basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Lists
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    ../lib/StatsDefaultConfig.py

Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Run BPS Test
Suite Teardown     logout  ${atipSession}

*** Test Cases ***
Validate Country Name

    :FOR    ${key}    IN    @{atipStatsMin.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
    \   checkTargetExistsInTopStats  ${atipStat}  ${strUS}
    \   checkTargetExistsInTopStats  ${atipStat}  ${strCanada}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \   checkTargetExistsInTopStats  ${atipStat}  ${strUS}
    \   checkTargetExistsInTopStats  ${atipStat}  ${strCanada}



Validate Session Statistics
    :FOR    ${key}    IN    @{atipStatsMin.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
    \  ${sessions}  convert to number  100
    \  checkAtipListStats  ${atipStat}  ${strUS}  ${atipSessions}  ${sessions}
    \  checkAtipListStats  ${atipStat}  ${strCanada}  ${atipSessions}  ${sessions}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  ${sessions}  convert to number  100
    \  checkAtipListStats  ${atipStat}  ${strUS}  ${atipSessions}  ${sessions}
    \  checkAtipListStats  ${atipStat}  ${strCanada}  ${atipSessions}  ${sessions}


Validate Total Packets Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${bpsPackets}  getBpsTotalTxPackets  ${bpsSession}  ${bpsConfig}
    \  Log  ${bpsPackets}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  ${pcktsUS}  getAtipStatsValues  ${atipStat}  ${strUS}  ${atipTotalPkts}
    \  ${pcktsCanada}  getAtipStatsValues  ${atipStat}  ${strCanada}  ${atipTotalPkts}
    \  should be equal  ${bpsPackets}  ${pcktsCanada}
    \  should be equal as integers  0  ${pcktsUS}



Validate Total Client Packets Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${bpsPackets}  getBpsTotalTxPackets  ${bpsSession}  ${bpsConfig}
    \  Log  ${bpsPackets}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  ${pcktsUS}  getAtipStatsValues  ${atipStat}  ${strUS}  ${atipClientPkts}
    \  ${pcktsCanada}  getAtipStatsValues  ${atipStat}  ${strCanada}  ${atipClientPkts}
    \  should be equal  ${bpsPackets}  ${pcktsCanada}
    \  should be equal as integers  0  ${pcktsUS}

Validate Total Server Packets Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  ${pcktsUS}  getAtipStatsValues  ${atipStat}  ${strUS}  ${atipServerPkts}
    \  ${pcktsCanada}  getAtipStatsValues  ${atipStat}  ${strCanada}  ${atipServerPkts}
    \  should be equal as integers  0  ${pcktsCanada}
    \  should be equal as integers  0  ${pcktsUS}

Validate Total Bytes Statistics
    :FOR    ${key}    IN    @{atipStatsMin.keys()}
    \  Log  ${key}
    \  ${bpsBytes}  getBpsTotalBytes  ${bpsSession}  ${bpsConfig}
    \  Log  ${bpsBytes}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
    \  ${bytesUS}  getAtipStatsValues  ${atipStat}  ${strUS}  ${atipTotalBytes}
    \  ${bytesCanada}  getAtipStatsValues  ${atipStat}  ${strCanada}  ${atipTotalBytes}
    \  should be equal  ${bpsBytes}  ${bytesCanada}
    \  should be equal as integers  0  ${bytesUS}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${bpsBytes}  getBpsTotalBytes  ${bpsSession}  ${bpsConfig}
    \  Log  ${bpsBytes}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
    \  ${bytesUS}  getAtipStatsValues  ${atipStat}  ${strUS}  ${atipTotalBytes}
    \  ${bytesCanada}  getAtipStatsValues  ${atipStat}  ${strCanada}  ${atipTotalBytes}
    \  should be equal  ${bpsBytes}  ${bytesCanada}
    \  should be equal as integers  0  ${bytesUS}

Validate Client Bytes Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${bpsBytes}  getBpsTotalBytes  ${bpsSession}  ${bpsConfig}
    \  Log  ${bpsBytes}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
    \  ${bytesUS}  getAtipStatsValues  ${atipStat}  ${strUS}  ${atipClientBytes}
    \  ${bytesCanada}  getAtipStatsValues  ${atipStat}  ${strCanada}  ${atipClientBytes}
    \  should be equal  ${bpsBytes}  ${bytesCanada}
    \  should be equal as integers  0  ${bytesUS}


Validate Server Bytes Statistics
    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
    \  ${bytesUS}  getAtipStatsValues  ${atipStat}  ${strUS}  ${atipServerBytes}
    \  ${bytesCanada}  getAtipStatsValues  ${atipStat}  ${strCanada}  ${atipServerBytes}
    \  should be equal as integers  0  ${bytesCanada}
    \  should be equal as integers  0  ${bytesUS}

Validate Share Statistics
    :FOR    ${key}    IN    @{atipStatsMin.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMin}
    \  checkAtipTopStatsTotalShare  ${atipStat}

    :FOR    ${key}    IN    @{atipStatsMax.keys()}
    \  Log  ${key}
    \  ${atipStat}  getAtipStats  ${key}  ${atipStatsMax}
    \  checkAtipTopStatsTotalShare  ${atipStat}





*** Keywords ***
Configure ATIP And Run BPS Test
    configureAtip  ${atipSession}  ${atipConfig}
    resetStats  ${atipSession}
    Log  ${testType}
    Log  ${validate}
    ${timeIntervalList} = 	getTimeIntervals
    set suite variable  ${bpsConfig}   ${bpsTestConfigUDP}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    sleep  10s
    login  ${atipSession}
    ${atipStatsMin} =  create dictionary
    ${atipStatsMax} =  create dictionary
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  Log  ${timeInterval}
        \  ${statsMin}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}  ${minimizedDashboard}
        \  ${statsMax}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}  ${maximizedDashboard}
        \  Log  ${statsMin}
        \  Log  ${statsMax}
        \  ${atipStatsMin}  setAtipStats  ${timeInterval}  ${statsMin}  ${atipStatsMin}
        \  ${atipStatsMax}  setAtipStats  ${timeInterval}  ${statsMax}  ${atipStatsMax}
    Log  ${atipStatsMin}
    Log  ${atipStatsMax}
    set suite variable  ${atipStatsMin}  ${atipStatsMin}
    set suite variable  ${atipStatsMax}  ${atipStatsMax}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsConfig}
    set suite variable  ${bpsComponentsStats}  ${bpsComponentsStats}
    Log  ${bpsComponentsStats}






