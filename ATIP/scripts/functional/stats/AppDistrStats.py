from cmath import isclose
from http import HTTPStatus
import jsonpickle
import logging

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.RealTimeStats import RealTimeStats
from atipAutoFrwk.data.traffic.stats.BpsComponentStatsContainer import BpsComponentStatsContainer
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.LogoutService import LogoutService
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.services.atip.stats.StatsService import StatsService
from atipAutoFrwk.services.atip.stats.TopFiltersService import TopFiltersService
from atipAutoFrwk.services.traffic.BpsComponentStatsService import BpsComponentStatsService
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Configuration import Configuration
from atipAutoFrwk.webApi.atip.ConfigureStats import ConfigureStats
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.Status import Status
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.webApi.atip.TopFilters import TopFilters
from atipAutoFrwk.webApi.atip.stats.Lists import Lists
from atipAutoFrwk.webApi.atip.stats.Pie import Pie
from atipAutoFrwk.webApi.traffic.BpsComponentStats import BpsComponentStats
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations
from datetime import datetime

########## Helper Methods ##########
from atipAutoFrwk.services.atip.stats.ValidateStatsService import ValidateStatsService


def checkSessions(currentAtipApp, currentBPSStats):
    print("The sessions are: {} vs. {}".format(currentAtipApp.totalCount, getattr(currentBPSStats, BpsStatsType.SESSIONS.statsName)))
    if currentAtipApp.totalCount != getattr(currentBPSStats, BpsStatsType.SESSIONS.statsName):
        raise ValueError("Sessions differ: {} vs. {}".format(currentAtipApp.totalCount, getattr(currentBPSStats, BpsStatsType.SESSIONS.statsName)))
    return True


def checkShare(currentAtipApp, currentBPSStats, totalBytes):

    share = currentAtipApp.totalBytes / totalBytes
    print("BPS BYTES: {}".format(totalBytes))
    print("atip bytes: {}".format(currentAtipApp.totalBytes))
    print("Share of {}: {} ".format(currentAtipApp.name, currentAtipApp.y))
    print("Computed share: {}".format(share))
    return isclose(currentAtipApp.y, share, rel_tol = 0.05)


def main():
    ########## Test Suite Setup ##########
    # Define test config
    # Define ATIP config
    atipConfig = AtipConfig()
    # Define bps config


    bpsTestConfig = BpsTestConfig('LatestDyn_auto', 62)
    bpsTestConfig.addAppSim(AppSimConfig('appsim_3', ApplicationType.TECHCRUNCH, OSType.Linux, BrowserType.BonEcho, clientLocation=GeoLocation.FRANCE, serverLocation=GeoLocation.UNITED_KINGDOM, requestPacketValues=PacketSizes(okSize=410, getSize=478)))
    bpsTestConfig.addAppSim(AppSimConfig('appsim_2', ApplicationType.BUSINESSINSIDER, OSType.Linux, BrowserType.BonEcho, clientLocation=GeoLocation.FRANCE, serverLocation=GeoLocation.UNITED_KINGDOM, requestPacketValues=PacketSizes(okSize=510, getSize=483)))
    bpsTestConfig.addAppSim(AppSimConfig('appsim_1', ApplicationType.BOREDPANDA, OSType.Linux, BrowserType.BonEcho, clientLocation=GeoLocation.FRANCE, serverLocation=GeoLocation.UNITED_KINGDOM, requestPacketValues=PacketSizes(okSize=460, getSize=526)))

    # Get environment config
    env = Environment()
    # Create webApi sessions
    atipSession = WebApiSession(env.atipConnectionConfig)
    bpsSession = WebApiSession(env.bpsConnectionConfig)

    # Configure ATIP
    Login.login(atipSession)
    AtipConfigService.createConfig(atipSession, atipConfig)
    # Configure BPS
    BpsLogin.login(bpsSession)
    PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)

    ########## Test Setup ##########
    # Clear stats on ATIP
    # SystemService.clearSystemAndWaitUntilNPReady(atipSession, env)

    System.resetStats(atipSession)
    # Send traffic

    atipconf = AtipConfig()
    AtipConfigService.createConfig(atipSession, atipconf)

    bpsTestConfig.testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup,
                                                    bpsTestConfig.testDuration)




    bpsComponentsStats = BpsComponentStats.getAllComponentsStats(bpsSession, bpsTestConfig)
    print('bps stats: {}'.format(jsonpickle.encode(bpsComponentsStats)))
    totalBytes = BpsComponentStatsService.sumStatItem(bpsComponentsStats, BpsStatsType.BYTES)
    print('totalBytes: {}'.format(totalBytes))
    print(type(bpsComponentsStats))


    def getAtipTotalBytes(atipSession, statType, interval):
        atipStats = Pie.getTopStats(atipSession, statType, timeInterval=interval)
        totalBytes = 0
        for app in atipStats.data:
            totalBytes += app.totalBytes
        return totalBytes


    totalAtipBytes = getAtipTotalBytes(atipSession, StatsType.APPS, TimeInterval.HOUR)
    print("Total bytes are {}:".format(totalAtipBytes))

    atipStatsByApp = Pie.getTopStats(atipSession, StatsType.APPS, timeInterval=TimeInterval.HOUR)
    isReallyOK = ValidateStatsService.checkSpecificFieldStat(atipStatsByApp, bpsTestConfig, bpsComponentsStats, atipSession,
                                                            StatsType.APPS,
                                                           TopStats.SHARE)

    print("Is really OK: {}".format(isReallyOK))

    if not isReallyOK:
        raise ValueError("Different values found!")
    print("Verification finished.")
    ########## Test Suite Teardown ##########
    Logout.logout(atipSession)

if __name__ == "__main__":
    main()


