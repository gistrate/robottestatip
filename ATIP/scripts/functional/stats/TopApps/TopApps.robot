*** Settings ***
Documentation  Top Apps basic tests.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.stats.Lists
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    ../lib/StatsDefaultConfig.py

Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Run BPS Test
Suite Teardown     logout  ${atipSession}

*** Test Cases ***
Validate App Name
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkTargetExistsInTopStats  ${atipStat}  ${strAmazon}
        \  checkTargetExistsInTopStats  ${atipStat}  ${strNetflix}
        \  checkTargetExistsInTopStats  ${atipStat}  ${strLinkedin}
        \  checkTargetExistsInTopStats  ${atipStat}  ${strFacebook}


Validate Session Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipSessions}

Validate Total Packets Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipTotalPkts}

Validate Total Client Packets Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipClientPkts}

Validate Total Server Packets Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipServerPkts}

Validate Total Bytes Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipTotalBytes}

Validate Client Bytes Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipClientBytes}

Validate Server Bytes Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkSpecificFieldStat  ${atipStat}  ${bpsConfig}  ${bpsComponentsStats}  ${atipSession}  ${statsType}  ${atipServerBytes}

Validate Share Statistics
    :FOR    ${key}    IN    @{atipStats.keys()}
        \  Log  ${key}
        \  ${atipStat}  getAtipStats  ${key}  ${atipStats}
        \  checkAtipTopStatsTotalShare  ${atipStat}




*** Keywords ***
Configure ATIP And Run BPS Test
    configureAtip  ${atipSession}  ${atipConfig}
    resetStats  ${atipSession}
    Log  ${testType}
    Log  ${validate}
    ${bpsConfig} = 	getBpsTestConfig  ${bpsTestConfig}
    ${timeIntervalList} = 	getTimeIntervals
    set suite variable  ${bpsConfig}   ${bpsConfig}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsConfig}
    login  ${atipSession}
    ${atipStats} =  create dictionary
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  Log  ${timeInterval}
        \  ${stat}  getTopStats  ${atipSession}  ${statsType}  ${timeInterval}  ${maximizedDashboard}
        \  ${atipStats}  setAtipStats  ${timeInterval}  ${stat}  ${atipStats}
    Log  ${atipStats}
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsConfig}
    set suite variable  ${atipStats}   ${atipStats}
    set suite variable  ${bpsComponentsStats}   ${bpsComponentsStats}




