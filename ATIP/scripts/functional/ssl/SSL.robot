*** Settings ***
Documentation  SSL Basic tests: creating a ssl filter and port mapping
Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.services.GeneralService  WITH NAME  General
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    Collections
Library    atipAutoFrwk.services.atip.stats.TopFiltersService

Suite Setup        Create Filter Config
Suite Teardown     Clean ATIP

Force Tags  ATIP  functional  hardware
*** Test Cases ***

TC1 Create SSL filter - decrypt static traffic
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    Log  Looking for application in top stats...  console=true
    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTestConfig1}
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}
    checkFilterExistsInTopStats  ${atipSession}  ${sslFilter}  ${timeInterval}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${sslFilter}  ${bpsTotalSessions}  ${timeInterval}
    checkTargetExistsInTopStats  ${atipStats}  ${strNetflix}

TC2 Create SSL filter - decrypt dynamic traffic
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig2}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig2}
    Log  Looking for application in top stats...  console=true
    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTestConfig2}
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}
    checkFilterExistsInTopStats  ${atipSession}  ${sslFilter}  ${timeInterval}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${sslFilter}  ${bpsTotalSessions}  ${timeInterval}
    checkTargetExistsInTopStats  ${atipStats}  ${strDynamic}

TC000873871 Create Port Mapping
    addSslPortMap  ${atipSession}  ${myPortMap}

TC000873876 Update Ssl Port Mapping
    addSslPortMap  ${atipSession}  ${oldPortMap}
    ${currentId}=  getSslPortMapId  ${atipSession}  ${oldPortMap}
    ${newPortMap.id}=  Set Variable  ${currentId}
    updateSslPortMap  ${atipSession}  ${newPortMap}

TC000873875 Delete Port Mapping
    ${currentId}=  getSslPortMapId  ${atipSession}  ${myPortMap}
    ${myPortMap.id}=  Set Variable  ${currentId}
    deleteSslPortMap  ${atipSession}  ${myPortMap}


*** Keywords ***

Create Filter Config
    General.configureAtip  ${atipSession}  ${atipConfig}
    ${response}  updateSslSettings  ${atipSession}    True
    should be equal  ${response.passiveSSLDecryption}  ${True}
    ${fileCert}=  Set variable  ${bpsKeysPath}/BreakingPoint_serverA_512.crt
    ${fileKey}=  Set variable   ${bpsKeysPath}/BreakingPoint_serverA_512.key
    ${name}=  Set variable  BreakingPoint_serverA_512
    ${response}  updateSslSettings  ${atipSession}    True
    should be equal  ${response.passiveSSLDecryption}  ${True}
    ${response}  deleteAllCerts  ${atipSession}
    ${response}  uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert}  ${fileKey}


Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    logout  ${atipSession}