from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.SslPortMapping import SslPortMapping
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.WebApiSession import WebApiSession



class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    appStatsType = StatsType.APPS
    dynAppStatsType = StatsType.DYNAMIC
    bpsTestConfig1 = BpsTestConfig('ATIP_SSL-HTTPS1-0_NetflixActionPakistan', 80)
    bpsTestConfig2 = BpsTestConfig('ATIP_SSL-HTTPS1_0-topsecretsite-Greece', 80)
    sslFilter = FilterConfig("SSL_Test")
    sslFilter.decrypted= True
    atipConfig = AtipConfig()
    atipConfig.addFilters(sslFilter)
    timeInterval=TimeInterval.HOUR
    minimizedDashboard = DashboardType.MINIMIZED.statsLimit
    strNetflix = 'Netflix'
    strDynamic = 'topsecretsite.com'
    myPortMap = SslPortMapping(-1, True, 900, 'some random service', 80)
    oldPortMap = SslPortMapping(-1, False, 1, 'some random service', 65535)
    newPortMap = SslPortMapping(-1, True, 65535, 'automation test update sll port map', 1)
    bpsKeysPath = "../../../frwk/src/atipAutoFrwk/data/bpskeys"
    sslKeysPath = "../../../frwk/src/atipAutoFrwk/data/sslkeys"
    fileNameList = ["../../../frwk/src/atipAutoFrwk/data/sslkeys"+"/deep_"+"%04d" % x for x in range(1,1025)]
    maxCertList = [x + ".two" for x in fileNameList]


