*** Settings ***

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    Collections

Suite Setup        Login ATIP
Suite Teardown     logout  ${atipSession}


Force Tags  ATIP  functional  hardware
*** Test Cases ***

Reset SSL Passive Decryption Config
    ${response}  updateSslSettings  ${atipSession}    False
    should be equal  ${response.passiveSSLDecryption}  ${False}

Enabled SSL Passive Decryption Config
    ${response}  updateSslSettings  ${atipSession}    True
    should be equal  ${response.passiveSSLDecryption}  ${True}

Get SSL Passive Decryption Config
    ${response}  getSslSettings  ${atipSession}
    should be equal  ${response.passiveSSLDecryption}  ${True}


Upload Separate Files Test
    ${fileCert}=  Set variable  ${bpsKeysPath}/BreakingPoint_serverA_1024.crt
    ${fileKey}=  Set variable   ${bpsKeysPath}/BreakingPoint_serverA_1024.key
    ${name}=  Set variable  BreakingPoint_serverA_1024

    ${response}  uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert}  ${fileKey}

Delete All SSL Certificates Separate File
    ${response}  deleteAllCerts  ${atipSession}

Upload Single File Test
    ${fileCombined}=  Set variable  ${sslKeysPath}/deep_0001.two
    ${response}  uploadSingleFile  ${atipSession}  ${fileCombined}

Delete All SSL Certificates Single File
    ${response}  bulkDeleteAllCerts  ${atipSession}

Upload Single File Test Two of Them One by One
    ${fileCombined}=  Set variable  ${sslKeysPath}/deep_0001.two
    ${response}  uploadSingleFile  ${atipSession}  ${fileCombined}
    ${fileCombined}=  Set variable  ${sslKeysPath}/deep_0002.two
    ${response}  uploadSingleFile  ${atipSession}  ${fileCombined}
#
Bulk Delete All SSL Certificates Test Two of Them
    ${response}  bulkDeleteAllCerts  ${atipSession}

Bulk Upload SSL Certificates Test Two of Them
    @{twoCerts}=  create list
    ...             ${sslKeysPath}/deep_0001.two
    ...             ${sslKeysPath}/deep_0002.two
    ${response}     bulkUploadSingleFile  ${atipSession}     ${twoCerts}

Bulk Delete All SSL Certificates Test Bulk Upload Two of Them
    ${response}  bulkDeleteAllCerts  ${atipSession}

Bulk Upload Max SSL Certificates Test

    ${response}     bulkUploadSingleFile  ${atipSession}     ${maxCertList}

Retrieve All SSL Certificates Test
    ${response}  getSslCerts  ${atipSession}
    log  \n Response is ${response}

Bulk Delete All SSL Certificates Test Max
    ${response}  bulkDeleteAllCerts  ${atipSession}

Disable SSL Passive Decryption Config
    ${response}  updateSslSettings  ${atipSession}    False
    should be equal  ${response.passiveSSLDecryption}  ${False}

*** Keywords ***

Login ATIP
    ${response}  login  ${atipSession}
    should be equal as integers  ${response.status_code}  ${httpStatusOK}  msg=${response.text}
    should contain  ${response.text}  ${loginSuccessful}