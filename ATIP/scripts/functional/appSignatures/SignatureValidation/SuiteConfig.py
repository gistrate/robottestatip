#from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
from atipAutoFrwk.data.atip.StatsType import StatsType

class SuiteConfig(StatsDefaultConfig):

    # Instantiating this derived class creates other pre-requisite objects needed for test execution
    appStatsType = StatsType.APPS
    dynAppStatsType = StatsType.DYNAMIC
