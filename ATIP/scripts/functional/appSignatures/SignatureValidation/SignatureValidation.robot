*** Settings ***
Documentation  Application signature detection test cases.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig

Force Tags  ATIP  functional  ${hardware}  ${virtual}  apps

Suite Setup        Reset ATIP
Suite Teardown     logout  ${atipSession}

Test Template  Validate Signature

*** Test Cases ***

sig_AlJazeera	    Al Jazeera	    	neo RECREATE aljazeeranew		TC000603809  #1
    [Tags]  onetest
sig_Amazon	        Amazon	        	neo RECREATE amazonnew	    	TC000603810  #2
sig_AOL	            AOL	            	sy_RECREATE_aol_155         	TC000603811  #3
sig_Apple	        Apple	        	neo RECREATE apple          	TC000603813  #4
sig_AresGalaxy	    Ares galaxy	    	neo RECREATE aresgalaxynew		TC000603814  #5
    [Tags]  skip
sig_Ask.Com			Ask.com         	neo RECREATE asknew         	TC000603815  #6
sig_Baidu           Baidu           	neo RECREATE baidunew       	TC000603816  #7
sig_BBC             BBC             	neo RECREATE bbcnew         	TC000603817  #8
sig_Bing            Bing            	neo RECREATE bingnew        	TC000603821  #9
sig_DCE/RPC2        DCE/RPC         	neo RECREATE dcerpc2new     	TC000603826  #10
sig_BitTorrent      BitTorrent      	neo RECREATE bittorrentnew  	TC000603822  #11
sig_DCE/RPC         DCE/RPC         	neo RECREATE dcerpcnew      	TC000603827  #12
sig_Dropbox         Dropbox         	neo RECREATE dropboxnew     	TC000603831  #13
sig_eBay            eBay            	neo RECREATE ebaynew        	TC000603832  #14
sig_ESPN			ESPN				neo RECREATE espnnew			TC000603834  #15
sig_Evernote		Evernote			neo RECREATE evernotenew		TC000603835  #16
sig_FIXSession		FIX					neo RECREATE fixsessionnew		TC000609475  #17
sig_FIXOrder		FIX					neo RECREATE fixordernew		TC000603837  #18
sig_FoxNewsChannel	Fox News Channel	neo RECREATE foxnewschannelnew	TC000603839  #19
sig_GaduGadu		Gadu Gadu			neo RECREATE gadugadunew		TC000603841  #20
sig_Gnutella		Gnutella			neo RECREATE gnutellanew		TC000603845  #21
sig_HDFS			HDFS				neo RECREATE hdfsnew			TC000603872  #22
sig_HDFSJobTracker	HDFS JobTracker		neo RECREATE hdfsjobtrackernew	TC000603873  #23
sig_Hotmail			Windows Live Hotmail	neo RECREATE hotmailnew		TC000603875  #24
sig_hulu			hulu				neo RECREATE hulunew			TC000603876  #25
sig_IMDB			IMDB				neo RECREATE imdbnew			TC000603881  #26
sig_IPFIX			Ipfix				neo RECREATE ipfixnew			TC000603883  #27
    [Tags]  skip
sig_ipp				ipp					neo RECREATE ippnew				TC000603885  #28
sig_iTunes			Apple				neo RECREATE itunesnew			OUT			 #29
    [Tags]  skip
sig_KakaoTalk		KakaoTalk			neo RECREATE kakaotalknew		TC000603889  #30
sig_LaPoste			La Poste			neo RECREATE lapostenew			TC000603890  #31
sig_LinkedIn		LinkedIn			neo RECREATE linkedinnew		TC000603892  #32
sig_Mail.ru			Mail.ru				sy_RECREATE_mailru_155			TC000603894  #33
sig_MSN				MSN					neo RECREATE msnnew				TC000603896  #34
sig_Okazii			Okazii				neo RECREATE okaziinew			TC000603901  #35
sig_OpenTable 		OpenTable			neo RECREATE opentablenew		TC000603902  #36
sig_Pinterest		Pinterest			neo RECREATE pinterestnew		TC000603903  #37
sig_QQ 				QQ					neo RECREATE qqnew				TC000603906  #38
sig_Reddit 			Reddit				neo RECREATE redditnew			TC000603910  #39
sig_Rediffmail		Rediffmail			sy_RECREATE_rediffmail_155		TC000603911  #40
sig_Skype2			Skype				neo RECREATE skype2new			TC000603917  #41
sig_Spotify 		Spotify				neo RECREATE spotifynew			TC000603920  #42
sig_Tumblr 			Tumblr				neo RECREATE tumblrnew			TC000603929  #43
sig_USAToday 		USA Today			neo RECREATE usatodaynew		TC000603930  #44
sig_Vudu 			Vudu				neo RECREATE vudunew			TC000603931  #45
sig_WikiLeaks		WikiLeaks			neo RECREATE wikileaksnew		TC000603932  #46
sig_Wikipedia		Wikipedia			neo RECREATE wikipedianew		TC000603933  #47
sig_WordPress 		WordPress   		sy_RECREATE_wordpress_155		TC000603935  #48
sig_Yahoo!			Yahoo!				neo RECREATE yahoonew			TC000603936  #49
sig_AIMKeyServer	AIM					neo RECREATE aim6-keyservernew	TC000609381  #50
    [Tags]  skip
sig_AIMSwitchBoard	AIM					neo RECREATE aim6-switchboardnew  TC000609382  #51
sig_aol			    aol					neo RECREATE aolmailnew			TC000609385  #52
    [Tags]  skip
sig_9p				9p					neo RECREATE 9pnew				N/A          #53	
    [Tags]  skip
sig_BBCiPlayer		BBC iPlayer			neo RECREATE bbciplayer			N/A			 #54
sig_BBCiPlayerRadio  BBC iPlayer Radio  neo RECREATE bbciplayerradio	N/A			 #55
    [Tags]  skip
sig_CNN				CNN					neo RECREATE cnnnew				N/A			 #56	
sig_FCIP			FCIP				neo RECREATE fcipnew			N/A			 #57	
sig_feedly			feedly				neo RECREATE feedlynew			N/A			 #58
    [Tags]  skip
sig_Flickr 			Flickr				neo RECREATE flickrnew			N/A			 #59
sig_GoogleAuthenticator  Google Authenticator  neo RECREATE googleauthenticatornew  N/A  #60	
    [Tags]  skip
sig_GoogleKeepNew	Google				neo RECREATE googlekeepnew		N/A			 #61	
    [Tags]  skip
sig_GoogleNewstand	Google				neo RECREATE googlenewstandnew	N/A			 #62	
sig_Gryphon 		Gryphon				neo RECREATE gryphonnew			N/A			 #63	
sig_hangouts2		hangouts			neo RECREATE hangouts2new		N/A			 #64	
    [Tags]  skip
sig_Hangouts		Hangouts			neo RECREATE hangoutsnew		N/A			 #65	
sig_hike			hike				neo RECREATE hikemobilenew		N/A			 #66	
sig_KakaoTalkMobile  KakaoTalk Mobile	neo RECREATE kakaotalkmobilenew  N/A		 #67
sig_Kik 			Kik					neo RECREATE kikmobilenew		N/A			 #68	
sig_LINEApp 		LINE App			neo RECREATE linemobilenew		N/A			 #69	
sig_MessageMe		MessageMe			neo RECREATE messagemobilenew	N/A			 #70	
sig_modbus			modbus				neo RECREATE modbusnew			N/A			 #71	
    [Tags]  skip
sig_Nimbuzz2		Nimbuzz				neo RECREATE nimbuzzmobile2new	N/A			 #72	
    [Tags]  skip
sig_Nimbuzz 		Nimbuzz				neo RECREATE nimbuzzmobilenew	N/A			 #73	
sig_ocsp 			ocsp				neo RECREATE ocspnew			N/A			 #74	
sig_OWA				Outlook Web Access	neo RECREATE owanew				N/A			 #75	
sig_Pandora			Pandora Media		neo RECREATE pandoranew			N/A			 #76	
sig_portmap 		portmap				neo RECREATE portmappernew		N/A			 #77	
    [Tags]  skip
sig_PPTP 			PPTP				neo RECREATE pptpnew			N/A			 #78	
sig_Shazam			Shazam Entertainment  neo RECREATE shazamnew		N/A			 #79
sig_snmp 			snmp				neo RECREATE snmpusmnew			N/A			 #80	
    [Tags]  skip
sig_stat			STAT				neo RECREATE statnew			N/A			 #81	
    
sig_Tango2			Tango				neo RECREATE tangomobile2new	N/A			 #82	
    
sig_Tango 			Tango				neo RECREATE tangomobilenew		N/A			 #83	
    [Tags]  skip
sig_Tango3			Tango				neo RECREATE tangomobile3new	N/A			 #84	
    [Tags]  skip
sig_TPNCP			TPNCP				neo RECREATE tpncptcpnew		N/A			 #85
    
sig_TPNCPUDP		TPNCP				neo RECREATE tpncpudpnew		N/A			 #86
    
sig_Twitter			Twitter Mobile		neo RECREATE twittermobilenew	N/A			 #87
    [Tags]  skip
    
sig_whatsapp		whatsapp			neo RECREATE whatsappnew		N/A			 #88
    [Tags]  skip
sig_WindowsLiveMail  Windows Live Mail  neo RECREATE windowslivemailnew  N/A		 #89	
    
sig_hulu2			hulu				neo RECREATE hulu2new			N/A			 #90
    [Tags]  skip
sig_hulu3			hulu				neo RECREATE hulu3new			N/A			 #91
    [Tags]  skip
sig_SkydriveLogin	live.com			neo RECREATE skydriveloginnew	N/A			 #92
    [Tags]  skip
sig_SkydriveOpen	live.com			neo RECREATE skydriveopennew	N/A			 #93
    [Tags]  skip
sig_SkydriveUpload	Microsoft SkyDrive	neo RECREATE skydriveuploadnew	N/A			 #94
    
sig_Skype			Skype				neo RECREATE skypenew			N/A			 #95
    
sig_Yelp			Yelp				neo RECREATE yelpnew			N/A			 #96
    
sig_HBO			    hbo					pcap_tom-hbo					N/A			 #97
    [Tags]  skip
sig_any			    any					ATIP-Perf_RR-ICMP-20Gbps		N/A			 #98
    [Tags]  skip
sig_DCERPC-EPM		DCE/RPC-EPM			neo RECREATE dcerpc-epm			N/A			 #99	
    
sig_DesktopNotification  Desktop Notification  neo RECREATE desktop-notification  N/A  #100
    
sig_Flipboard		Flipboard Mobile	neo RECREATE flipboard			N/A			 #101
    
sig_GoogleNewstand2  Google				neo RECREATE google-newstand	N/A			 #102	
    
sig_GoogleTranslate  google-translate  neo RECREATE google-translate	N/A		 	 #103
    [Tags]  skip
sig_HBOGO			hbogo				neo RECREATE hbogo				N/A			 #104 - dupe of #122
    [Tags]  skip
sig_HLS				HLS					neo RECREATE hls				N/A			 #105
    
sig_HSRP			Hot Standby Router Protocol  neo RECREATE hsrp		N/A			 #106
    [Tags]  skip
sig_iCloudWeb		iCloud Web			neo RECREATE icloudweb			N/A			 #107
    
sig_iTunes			iTunes				neo RECREATE itunesnew			N/A			 #108
    
sig_megaco			megaco				neo RECREATE megaco				N/A			 #109
    [Tags]  skip
sig_Pocket			Pocket				neo RECREATE pocket				N/A			 #110
    
sig_TFTP			TFTP				neo RECREATE tftp				N/A			 #111
    
sig_TeamSpeak2		TeamSpeak			neo RECREATE teamspeak2			N/A			 #112
    
sig_YouTube			YouTube				neo RECREATE youtube			N/A			 #113
    
sig_GoogleDNS		Google				neo RECREATE googledns			TC000688150  #114
    [Tags]  skip
sig_H245			H.245				neo RECREATE h245				TC000688156  #115
    [Tags]  skip
sig_GitHub			GitHub				neo RECREATE githttps			TC000688146  #116
    
sig_AppleAppStore	Apple				neo RECREATE appstorenew		TC000688127  #117
    [Tags]  skip
sig_HLS			    HLS					neo RECREATE hls				TC000688160  #118
    [Tags]  skip
sig_NicoNico		Nico Nico Douga		neo RECREATE niconicoloadvideo	TC000688176  #119
sig_SkySports		sky sports			neo RECREATE skysports			N/A			 #120
sig_SunRPC			SunRPC				neo RECREATE sunrpc				N/A			 #121
sig_HBOGO			HBO					neo RECREATE hbogo				N/A			 #122
sig_GoogleTalk		Google Talk			neo RECREATE googletalk			N/A			 #123
sig_Yammer			Yammer				neo RECREATE yammernew			TC000603940  #124
sig_9GAG			9GAG				neo RECREATE 9gag-mobile		TC000735757  #125
sig_AmazonKindle	Amazon Kindle		neo RECREATE amazon-kindle-mobile  TC000735768  #126
sig_Concur			Concur Technologies  neo RECREATE concur-mobile		TC000735770  #127
sig_ConnectBot		ConnectBot			neo RECREATE connectbot-mobile	TC000735771  #128
sig_Coursera		Coursera Mobile		neo RECREATE coursera-mobile	TC000735772  #129
sig_Eventbrite		Eventbrite			neo RECREATE eventbrite-mobile	TC000735774  #130
sig_Feedly			Feedly				neo RECREATE feedly				TC000664641  #131
sig_Fitocracy		Fitocracy			neo RECREATE fitocracy-mobile	TC000735776  #132
sig_FTP				FTP					neo RECREATE ftp				TC000735777  #133
sig_Goodreads		Goodreads			neo RECREATE goodreads-mobile	TC000735778  #134
sig_GoogleKeep		Google Keep			neo RECREATE google-keep		TC000664643  #135
sig_QuizUp			QuizUp				neo RECREATE quizup				TC000735784  #136
sig_GoogleNow		Google Now			neo RECREATE google-now			TC000735780  #137
sig_Grooveshark		Grooveshark			neo RECREATE grooveshark-mobile  TC000735781  #138
sig_Kismet			Kismet				neo RECREATE kismet				TC000735782  #139
sig_LiveScore		LiveScore Mobile	neo RECREATE livescore-mobile	TC000735783  #140
sig_SoundCloud		SoundCloud			neo RECREATE soundcloud-mobile	TC000735786  #141
sig_SoundHound		SoundHound Mobile	neo RECREATE soundhound			TC000735787  #142
sig_SpeedTest		SpeedTest Mobile	neo RECREATE speedtest-mobile	TC000735788  #143
sig_TED				TED					neo RECREATE ted-mobile			TC000735789  #144
sig_Tor				Tor					neo RECRATE tor-ip				TC000735790  #145
sig_TripAdvisorCityGuidesCtlg  TripAdvisor City Guides Catalog  neo RECREATE tripadvisor-city-guides-catalog  TC000735791  #146
sig_TripAdvisor		TripAdvisor			sy_RECREATE_tripadvisor_155     TC000735792  #147
sig_WeHeartIt		We Heart It			neo RECREATE weheartit-mobile	TC000735794  #148
sig_YouTubeRemote	YouTube Remote		neo RECREATE youtube-remote		TC000735795  #149
sig_ZDNet			ZDNet				neo RECREATE zdnet-mobile		TC000735796  #150
sig_ZDNetWeb		ZDNet				neo RECREATE zdnet-web			TC000735797  #151
sig_WebEx			WebEx				neo RECREATE webex				TC000741996  #152
sig_050plus			050plus				neo RECREATE 050plus			TC000754514  #153
sig_4shared			4shared				neo RECREATE 4shared-mobile		TC000774430  #154
sig_Ask.fm		    Ask.fm				neo RECREATE askfm-mobile		TC000774432  #155
sig_Badoo			Badoo				neo RECREATE badoo-mobile		TC000774433  #156
sig_Blogger			Blogger				neo RECREATE blogger-mobile		TC000774434  #157
sig_Booking.com		Booking.com			neo RECREATE booking-mobile		TC000774435  #158
sig_CNET			CNET				neo RECREATE cnet-mobile		TC000774436  #159
sig_DailyMail		Daily Mail			sy_RECREATE_dailymail_155   	TC000774437  #160
sig_DailyMotion		Dailymotion			neo RECREATE dailymotion-mobile  TC000774438  #161
sig_DepositFiles	DepositFiles		neo RECREATE depositfiles-mobile  TC000774440  #162
sig_GoogleAnalytics  Google Analytics	neo RECREATE google-analytics	TC000774531  #163 - dupe of #177
    [Tags]  skip
sig_GStatic			GStatic				neo RECREATE gstatic			TC000774441  #164
sig_iMesh			iMesh				neo RECREATE imesh				TC000774443  #165
sig_KaperskyLab		Kaspersky Lab		neo RECREATE kaspersky-mobile	TC000774445  #166
sig_LeagueOfLegends  League of Legends	neo RECREATE leagueoflegends	TC000774446  #167
sig_Maaii			Maaii Mobile		neo RECREATE maaii-mobile		TC000774447  #168
sig_Silverlight		Microsoft Silverlight  neo RECREATE microsoft-silverlight  TC000774448  #169
sig_MiTalk			MiTalk				neo RECREATE mitalk-mobile		TC000774449  #170
sig_mypeople		mypeople			neo RECREATE mypeople-mobile	TC000774450  #171
sig_Odnoklassniki	Odnoklassniki		neo RECREATE odnoklassniki-mobile  TC000774451  #172
sig_SIPr2			SIP Call			neo RECREATE sip-r2				TC000774452  #173
sig_AOLMail			AOL Mail			sy_RECREATE_aolmail_155			N/A			 #174
sig_Gmail			Gmail				neo RECREATE gmail				N/A			 #175
sig_GoogleMaps		Google Maps			neo RECREATE google-maps		N/A			 #176
sig_GoogleAnalytics  Google Analytics	neo RECREATE google-analytics	N/A			 #177
sig_Ident			Ident				neo RECREATE ident				N/A			 #178
sig_IMAP			IMAP				neo RECREATE imap				N/A			 #179
sig_LDAP			LDAP				neo RECREATE ldap				N/A			 #180
sig_PayPal			PayPal				sy_RECREATE_paypal_155			N/A			 #181
sig_Photobucket		Photobucket			neo RECREATE photobucket-mobile  N/A		 #182
sig_Plurk			Plurk				neo RECREATE plurk-mobile		N/A			 #183
sig_SMTP			SMTP				neo RECREATE smtp				N/A			 #184
sig_Yahoo!Mail		Yahoo! Mail			neo RECREATE yahoo-mail			N/A			 #185
sig_Yahoo!Messenger  Yahoo! Messenger	neo RECREATE yahoo-messenger	N/A			 #186
sig_AirDroid		AirDroid			neo RECREATE airdroid			N/A			 #187
sig_BGP				BGP					neo RECREATE bgp2				N/A			 #188
sig_Deezer			Deezer				neo RECREATE deezer-mobile		N/A			 #189
sig_Twitch			Twitch				neo RECREATE twitchtv			N/A			 #190
sig_GoogleEarth		Google Earth		neo RECREATE googleearthnew		N/A			 #191
sig_Instagram		Instagram			neo RECREATE instagram			N/A			 #192
sig_SMTPS			SMTPS				neo RECREATE smtps				N/A			 #193
sig_LLRP			LLRP				neo RECREATE llrp				TC000864011  #194
sig_SMPP			SMPP				neo RECREATE smpp				N/A			 #195 - dupe of #217
    [Tags]  skip
sig_Modbus			Modbus				neo RECREATE modbus				N/A			 #196
    [Tags]  skip
sig_PANA			PANA				neo RECREATE pana-rfc5191		N/A			 #197 - dupe of #222
    [Tags]  skip
sig_iSCSI			iSCSI				neo RECREATE iscsi				N/A			 #198
    [Tags]  skip
sig_NNTP			NNTP				neo RECREATE nntp				N/A			 #199
sig_POP3			POP3				neo RECREATE pop3				N/A			 #200
sig_RIPv1			RIP					neo RECREATE rip_v1				N/A			 #201
    [Tags]  skip
sig_Manolito		Manolito			neo RECREATE manolito2			TC000864012  #202
    [Tags]  skip
sig_Netflix			Netflix				neo RECREATE netflixnew			N/A			 #203
sig_Facetime		Facetime/iMessage	neo RECREATE facetimenew		N/A			 #204
sig_Facebook		Facebook			sy_RECREATE_facebook_155		N/A			 #205
sig_FacebookMobile	Facebook Mobile		neo RECREATE facebookmobilenew	N/A			 #206
    [Tags]  skip
sig_Viber			Viber Media			neo RECREATE vibernew			N/A			 #207
sig_Gopher			Gopher				neo RECREATE gopher				TC000863891  #208
sig_LDPLPR			LPD/LPR				neo RECREATE lpd				TC000864001  #209
sig_MSExcel			Microsoft Excel		neo RECREATE msexcel			TC000864002  #210
sig_MSPowerPoint	Microsoft PowerPoint  neo RECREATE mspowerpoint		TC000864003  #211
sig_MSWord			Microsoft Word		neo RECREATE msword				TC000864004  #212
sig_Office365		Office 365			neo RECREATE office365			TC000864005  #213
sig_POP3			POP3				neo RECREATE pop3				TC000864013  #214 - dupe of #200
    [Tags]  skip
sig_PVFS			PVFS				neo RECREATE pvfs				TC000864006  #215
sig_RIP			    RIP					neo RECREATE rip				TC000864015  #216
    [Tags]  skip
sig_SMPP			SMPP				neo RECREATE smpp				TC000864010  #217
sig_SNMPUSM			SNMP				neo RECREATE snmpusmnew			TC000864016  #218
sig_SRVLOC			SRVLOC				neo RECREATE srvloc				TC000864007  #219
sig_MOUNT			MOUNT				neo RECREATE mount				TC000864008  #220
sig_MSSQLServer	Microsoft SQL Server  neo RECREATE mssql			TC000864018  #221
    [Tags]  skip
sig_PANA			PANA				neo RECREATE pana-rfc5191		TC000864020  #222
sig_Portmap			Portmap				neo RECREATE portmap			TC000864022  #223
sig_RQUOTA			RQUOTA				neo RECREATE rquota				TC000864009  #224
sig_EPMD			EPMD				neo RECREATE epmd				TC000900947  #225
sig_PPS				PPS					neo RECREATE pps				N/A			 #226
sig_TV4Play			TV4 Play (Sweden)	neo RECREATE tv4playmobile		N/A			 #227
sig_Ustream		    Ustream				neo RECREATE ustreammobile		N/A			 #228
sig_AresGalaxy		Ares Galaxy			neo RECREATE ares-galaxy		N/A			 #229
    [Tags]  skip
sig_SSL			    SSL					neo RECREATE ssl				N/A			 #230
    [Tags]  skip
sig_MGCP			MGCP				neo RECREATE mgcp				N/A			 #231
sig_AmazonVideo	    Amazon Video		neo RECREATE amazon-video		N/A			 #232
sig_Steam			Steam				neo RECREATE steam				N/A			 #233
sig_eDonkey		    eDonkey				neo RECREATE edonkey			N/A			 #234
    [Tags]  skip
sig_MSNMessenger	MSN Messenger Service  neo RECREATE msnms			N/A			 #235
sig_IRC			    Internet Relay Chat	neo RECREATE irc				N/A			 #236
sig_TLS			    TLS					neo RECREATE tls				N/A			 #237
    [Tags]  skip
sig_Snapchat		Snapchat			neo RECREATE snapchat			N/A			 #238
sig_RTMP			RTMP				neo RECREATE rtmp				N/A			 #239
sig_MPEG-TS		    MPEG-TS				neo RECREATE mpeg_ts			N/A			 #240
sig_Zookeeper		Apache ZooKeeper	neo RECREATE zookeeper			N/A			 #241
sig_Vine			Vine				neo RECREATE vine				N/A			 #242
sig_Xunlei			Xunlei				neo RECREATE xunlei				N/A			 #243
sig_QUIC			QUIC				neo RECREATE quic				N/A			 #244
sig_Salesforce		Salesforce			neo RECREATE salesforce			N/A			 #245
sig_SalesforceAppExchange  Salesforce AppExchange  neo RECREATE salesforceAppexchange  N/A  #246
sig_SalesforceDesk  Salesforce Desk	neo RECREATE salesforceDesk		N/A			 #247
sig_BaiduTieba		Baidu Tieba			neo RECREATE baidutieba			N/A			 #248
sig_Profinet		Profinet/PNIO-CM	neo RECREATE profinet			N/A			 #249
sig_Zendesk		    Zendesk				neo RECREATE zendesk			N/A			 #250
sig_PokemonGO		Pokemon GO			neo RECREATE pokemonGo			N/A			 #251
sig_Vimeo			Vimeo				neo RECREATE vimeoMobile		N/A			 #252
sig_DICOM			DICOM				neo RECREATE dicom				N/A			 #253
sig_YouTube143		YouTube				neo RECREATE youtube143			N/A			 #254
sig_GoogleCloudStorage  Google Cloud Storage  neo RECREATE googleCloudStorage  N/A  #255
sig_Workday		    Workday				neo RECREATE workday			N/A			 #256
sig_Box			    Box					neo RECREATE box				N/A			 #257
sig_TwitPic		    TwitPic				neo RECREATE twitpic			N/A			 #258
sig_Hightail		Hightail			neo RECREATE hightail			N/A			 #259
sig_Youku			Youku				neo RECREATE youku				N/A			 #260
sig_Snapdeal		Snapdeal			neo RECREATE snapdeal			N/A			 #261
sig_VKontakte		VKontakte			neo RECREATE vkontakte			N/A			 #262
sig_Zedge			Zedge				neo RECREATE zedge				N/A			 #263
sig_SASOnDemand	    SAS OnDemand		neo RECREATE SASonDemand		N/A			 #264
sig_Retrica		    Retrica				neo RECREATE retricaMobile		N/A			 #265
sig_WindowsPhoneMarketplace  Windows Phone Marketplace  neo RECREATE windowsMarketplaceMobile  N/A  #266
sig_TeamSpeak		TeamSpeak			neo RECREATE teamspeak			N/A			 #267
sig_SRVLOCNEW		SRVLOC				neo RECREATE srvlocnew			N/A			 #268
sig_IPMI			ipmi				neo RECREATE ipmi				N/A			 #269
sig_9PFS			9PFS (Plan9)		neo RECREATE 9pfs				N/A			 #270
sig_RDP			    Remote Desktop Protocol  neo RECREATE rdp			N/A			 #271
sig_RDP10			Remote Desktop Protocol  neo RECREATE rdp10			N/A			 #272
    

*** Keywords ***

Validate Signature
    [Arguments]  ${appName}  ${bpsTestName}  ${tcID}
    Log  \nValidate application signature: ${appName}(${tcID})  console=true
    Log  Resetting statistics...  console=true
    resetStats  ${atipSession}
    # Define BPS config
    Log  Running BPS test: ${bpsTestName}  console=true
	import library  atipAutoFrwk.config.traffic.BpsTestConfig  ${bpsTestName}  400  WITH NAME  bpsTestConfigInit
	${myClassInstance}=  get library instance  bpsTestConfigInit
	Set Test Variable  ${bpsTestConfigInit}  ${myClassInstance}
	bpsTestConfigInit.reinit  ${bpsTestName}  400
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigInit}
    Log  Looking for application in top stats...  console=true
    ${timeIntervalList} = 	getTimeIntervals
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  Log  ${timeInterval}
        ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}
        checkTargetExistsInTopStats  ${atipStats}  ${appName}
        ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}
        checkTargetDoesNotExistInTopStats  ${atipStats}  ${appName}
    
Reset ATIP
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    configureAtip  ${atipSession}  ${atipConfig}
    