*** Settings ***
Documentation  Application signature detection test cases.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.webApi.atip.CustomAppConfig
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig

Force Tags  ATIP  functional  ${hardware}  ${virtual}  apps

Test Setup        Reset ATIP
Test Teardown     logout  ${atipSession}

Test Template  Validate Custom Signature

*** Test Cases ***

sig_SIPVoIP	        SIP VoIP		   1    neo RECREATE sipvoip			sip-voip.xml
    [Tags]  onetest
sig_SIPPSTN         SIP PSTN           1    neo RECREATE sippstn            sip-pstn.xml
sig_CDKDesking      CDK Desking        2    neo RECREATE desking_main       cdk-desking.xml
sig_CDKDrive        CDK Drive          23   neo RECREATE cdk_drive          cdk-drive.xml
sig_CDKServiceEdge  CDK Service Edge   3    neo RECREATE cdk_service_edge   cdk-service-edge.xml
sig_AndorraTelcoDNS  Andorra Telecom DNS  1  neo RECREATE dns_andorra_telekom  andorra-telecom-dns.xml
sig_Office365SSL    Office365 SSL      1    Custom-SNI                      sni_sig_office365.xml
sig_SharePointSSL   SharePoint SSL     1    Custom-SNI                      sni_sig_sharepoint.xml
sig_EricssonVXLAN	Ericsson VXLAN     1    neo RECREATE vxlan              ericsson-vxlan.xml
    
*** Keywords ***

Validate Custom Signature
    [Arguments]  ${appName}  ${numSigs}  ${bpsTestName}  ${sigFile}
    Log  \nValidate custom application signature: ${appName}  console=true
    Log  Uploading custom application signature...  console=true
    uploadCustomAppFile  ${atipSession}  ${EXECDIR}/../../../frwk/src/atipAutoFrwk/data/custapps/${sigFile}
    Log  Validating custom application signature upload...  console=true
    validateCustomApp  ${atipSession}  ${appName}  ${numSigs}
    Log  Resetting statistics...  console=true
    resetStats  ${atipSession}
    Log  Running BPS test: ${bpsTestName}  console=true
	import library  atipAutoFrwk.config.traffic.BpsTestConfig  ${bpsTestName}  400  WITH NAME  bpsTestConfigInit
	${myClassInstance}=  get library instance  bpsTestConfigInit
	Set Test Variable  ${bpsTestConfigInit}  ${myClassInstance}
	bpsTestConfigInit.reinit  ${bpsTestName}  400
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigInit}
    Log  Looking for application in top stats...  console=true
    ${timeIntervalList} = 	getTimeIntervals
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  Log  ${timeInterval}
        ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}
        checkTargetExistsInTopStats  ${atipStats}  ${appName}
        ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}
        checkTargetDoesNotExistInTopStats  ${atipStats}  ${appName}
    
Reset ATIP
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    #configureAtip  ${atipSession}  ${atipConfig}
