*** Settings ***
Documentation  Application signature detection test cases.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System  WITH NAME  AtipSystem
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    Telnet   60 seconds     WITH NAME   Tnet

Resource    scripts/functional/appSignatures/pcapSigValidation/NTOConfigPcapLib.robot

Force Tags  ATIP  functional  ${hardware}  ${virtual}  apps

Suite Setup        Setup PCAP ATIP Env
Suite Teardown     Recover ATIP Env

Test Template  Validate Signature

*** Variables ***
${topStatsLimit}    ${50}


*** Test Cases ***

050plus-mobile.xml  050plus  050plus-mobile.pcap
4shared-mobile.xml  4shared  4shared-mobile.pcap
7shifts.xml  7SHIFTS  7shifts.pcap
9gag-mobile.xml  9GAG  9gag-mobile.pcap
9p.xml  9PFS (Plan9)  9p.pcap
aim6-switchboard.xml  AIM  aim6-switchboard.pcap
airdroid.xml  AirDroid  airdroid.pcap
al-jazeera.xml  Al Jazeera  al-jazeera.pcap
amazon-awsconsole.xml  Amazon Web Services Management Console  amazon-awsconsole.pcap
amazon-ec2.xml  Amazon EC2  amazon-ec2.pcap
amazon-kindle-mobile.xml  Amazon Kindle  amazon-kindle-mobile.pcap
amazon-s3.xml  Amazon S3  amazon-s3.pcap
amazon-video.xml  Amazon Video  amazon-video.pcap
amazon.xml  Amazon  amazon.pcap
aolmail.xml  AOL Mail  aolmail.pcap
aol.xml  AOL  aol.pcap
apache-zookeeper.xml  Apache ZooKeeper  apache-zookeeper.pcap
apple.xml  Apple  apple.pcap
appogee.xml  Appogee  appogee.pcap
appointy.xml  Appointy  appointy.pcap
askfm-mobile.xml  Ask.fm  askfm-mobile.pcap
ask.xml  Ask.com  ask.pcap
badoo-mobile.xml  Badoo  badoo-mobile.pcap
baidutieba.xml  Baidu Tieba  baidutieba.pcap
baidu.xml  Baidu  baidu.pcap
bbciplayer.xml  BBC iPlayer  bbciplayer.pcap
bbc.xml  BBC  bbc.pcap
bgp2.xml  BGP  bgp2.pcap
bing.xml  Bing  bing.pcap
bittorrent.xml  BitTorrent  bittorrent.pcap
blogger-mobile.xml  Blogger  blogger-mobile.pcap
booking.com-mobile.xml  Booking.com  booking.com-mobile.pcap
box.xml  Box  box.pcap
certify.xml  Certify  certify.pcap
cnet-mobile.xml  CNET  cnet-mobile.pcap
cnn-com.xml  CNN  cnn-com.pcap
concur-mobile.xml  Concur Technologies  concur-mobile.pcap
connectbot-mobile.xml  ConnectBot  connectbot-mobile.pcap
coursera-mobile.xml  Coursera Mobile  coursera-mobile.pcap
dailymail-mobile.xml  Daily Mail  dailymail-mobile.pcap
dailymotion-mobile.xml  Dailymotion  dailymotion-mobile.pcap
dcerpc_epm.xml  DCE/RPC-EPM  dcerpc_epm.pcap
dcerpc.xml  DCE/RPC  dcerpc.pcap
deezer-mobile.xml  Deezer  deezer-mobile.pcap
depositfiles-mobile.xml  DepositFiles  depositfiles-mobile.pcap
desktop-notification.xml  Desktop Notification  desktop-notification.pcap
dhcp.xml  DHCP  dhcp.pcap
dicom.xml  DICOM  dicom.pcap
discord.xml  Discord  discord.pcap
docusign.xml  DocuSign  docusign.pcap
dropbox_paper.xml  Dropbox Paper  dropbox_paper.pcap
dropbox.xml  Dropbox  dropbox.pcap
ebay.xml  eBay  ebay.pcap
epmd.xml  EPMD  epmd.pcap
espn.xml  ESPN  espn.pcap
eventbrite-mobile.xml  Eventbrite  eventbrite-mobile.pcap
evernote.xml  Evernote  evernote.pcap
facebook.xml  Facebook  facebook.pcap
facetime.xml  Facetime/iMessage  facetime.pcap
fcip.xml  FCIP  fcip.pcap
feedly.xml  Feedly  feedly.pcap
fitocracy-mobile.xml  Fitocracy  fitocracy-mobile.pcap
fix.xml  FIX  fix.pcap
flickr.xml  Flickr  flickr.pcap
flipboard-mobile.xml  Flipboard Mobile  flipboard-mobile.pcap
fox-news-channel.xml  Fox News Channel  fox-news-channel.pcap
ftp.xml  FTP  ftp.pcap
gadu-gadu.xml  Gadu Gadu  gadu-gadu.pcap
githttps.xml  GitHub  githttps.pcap
gmail.xml  Gmail  gmail.pcap
gnutella.xml  Gnutella  gnutella.pcap
godaddy.xml  GoDaddy  godaddy.pcap
goodreads-mobile.xml  Goodreads  goodreads-mobile.pcap
google-analytics.xml  Google Analytics  google-analytics.pcap
google-cloud-storage.xml  Google Cloud Storage  google-cloud-storage.pcap
google-docs.xml  Google Docs  google-docs.pcap
google-drive.xml  Google Drive  google-drive.pcap
google-earth.xml  Google Earth  google-earth.pcap
google-keep.xml  Google Keep  google-keep.pcap
google-maps.xml  Google Maps  google-maps.pcap
google-now.xml  Google Now  google-now.pcap
google-photos.xml  Google Photos  google-photos.pcap
google-talk.xml  Google Talk  google-talk.pcap
google.xml  Google  google.pcap
gopher.xml  Gopher  gopher.pcap
grooveshark-mobile.xml  Grooveshark  grooveshark-mobile.pcap
gryphon.xml  Gryphon  gryphon.pcap
gstatic.xml  GStatic  gstatic.pcap
h225ras.xml  H.225.0 RAS  h225ras.pcap
hangouts.xml  Hangouts  hangouts.pcap
hbogo.xml  HBO  hbogo.pcap
hdfsjobtracker.xml  HDFS JobTracker  hdfsjobtracker.pcap
hdfs.xml  HDFS  hdfs.pcap
hightail.xml  Hightail  hightail.pcap
hike-mobile.xml  hike  hike-mobile.pcap
hls.xml  HLS  hls.pcap
hotmail.xml  Windows Live Hotmail  hotmail.pcap
hulu.xml  hulu  hulu.pcap
icloudweb.xml  iCloud Web  icloudweb.pcap
ident.xml  Ident  ident.pcap
imap.xml  IMAP  imap.pcap
imdb.xml  IMDB  imdb.pcap
imesh.xml  iMesh  imesh.pcap
instagram.xml  Instagram  instagram.pcap
ipmi.xml  ipmi  ipmi.pcap
ipp.xml  ipp  ipp.pcap
irc.xml  Internet Relay Chat  irc.pcap
itunes.xml  iTunes  itunes.pcap
jira-service-desk.xml  Jira Service Desk  jira-service-desk.pcap
kakaotalk-mobile.xml  KakaoTalk Mobile  kakaotalk-mobile.pcap
kakaotalk.xml  KakaoTalk  kakaotalk.pcap
kaspersky-mobile.xml  Kaspersky Lab  kaspersky-mobile.pcap
kelihos.xml  Kelihos botnet  kelihos.pcap
kik-mobile.xml  Kik  kik-mobile.pcap
kismet.xml  Kismet  kismet.pcap
klm.xml  KLM  klm.pcap
kodi.xml  Kodi  kodi.pcap
kpasswd.xml  KPASSWD  kpasswd.pcap
laposte.xml  La Poste  laposte.pcap
ldap.xml  LDAP  ldap.pcap
leagueoflegends.xml  League of Legends  leagueoflegends.pcap
line-mobile.xml  LINE App  line-mobile.pcap
linkedin.xml  LinkedIn  linkedin.pcap
livescore-mobile.xml  LiveScore Mobile  livescore-mobile.pcap
llrp.xml  LLRP  llrp.pcap
lpd.xml  LPD/LPR  lpd.pcap
maaii-mobile.xml  Maaii Mobile  maaii-mobile.pcap
mailru.xml  Mail.ru  mailru.pcap
meraki.xml  Meraki  meraki.pcap
messageme-mobile.xml  MessageMe  messageme-mobile.pcap
mgcp.xml  MGCP  mgcp.pcap
microsoftazure.xml  Microsoft Azure  microsoftazure.pcap
microsoft-silverlight.xml  Microsoft Silverlight  microsoft-silverlight.pcap
mitalk-mobile.xml  MiTalk  mitalk-mobile.pcap
mount.xml  MOUNT  mount.pcap
mpeg-ts.xml  MPEG-TS  mpeg-ts.pcap
msexcel.xml  Microsoft Excel  msexcel.pcap
msnms.xml  MSN Messenger Service  msnms.pcap
msn.xml  MSN  msn.pcap
mspowerpoint.xml  Microsoft PowerPoint  mspowerpoint.pcap
msword.xml  Microsoft Word  msword.pcap
mypeople-mobile.xml  mypeople  mypeople-mobile.pcap
mysql.xml  MySQL Server  mysql.pcap
netflix.xml  Netflix  netflix.pcap
nico-nico-douga.xml  Nico Nico Douga  nico-nico-douga.pcap
nimbuzz-mobile.xml  Nimbuzz  nimbuzz-mobile.pcap
nntp.xml  NNTP  nntp.pcap
ocsp.xml  ocsp  ocsp.pcap
odnoklassniki-mobile.xml  Odnoklassniki  odnoklassniki-mobile.pcap
office365_onenote.xml  Microsoft OneNote  office365_onenote.pcap
office365.xml  Office 365  office365.pcap
okazii.xml  Okazii  okazii.pcap
opentable.xml  OpenTable  opentable.pcap
owa.xml  Outlook Web Access  owa.pcap
pana.xml  PANA  pana.pcap
pandora.xml  Pandora Media  pandora.pcap
paypal-mobile.xml  PayPal  paypal-mobile.pcap
photobucket-mobile.xml  Photobucket  photobucket-mobile.pcap
pinterest.xml  Pinterest  pinterest.pcap
plurk-mobile.xml  Plurk  plurk-mobile.pcap
pocket.xml  Pocket  pocket.pcap
pokemon_go.xml  Pokemon GO  pokemon_go.pcap
pop3.xml  POP3  pop3.pcap
portmap.xml  Portmap  portmap.pcap
pps-mobile.xml  PPS  pps-mobile.pcap
pptp.xml  PPTP  pptp.pcap
profinet.xml  Profinet/PNIO-CM  profinet.pcap
pvfs.xml  PVFS  pvfs.pcap
qq.xml  QQ  qq.pcap
quic.xml  QUIC  quic.pcap
quizup.xml  QuizUp  quizup.pcap
quora.xml  Quora  quora.pcap
rdp.xml  Remote Desktop Protocol  rdp.pcap
reddit.xml  Reddit  reddit.pcap
rediffmail.xml  Rediffmail  rediffmail.pcap
retrica-mobile.xml  Retrica  retrica-mobile.pcap
rpcap.xml  RPCAP  rpcap.pcap
rquota.xml  RQUOTA  rquota.pcap
rtmp.xml  RTMP  rtmp.pcap
rtps.xml  RTPS  rtps.pcap
salesforce-desk.xml  Salesforce Desk  salesforce-desk.pcap
salesforce.xml  Salesforce  salesforce.pcap
sas-ondemand.xml  SAS OnDemand  sas-ondemand.pcap
sharefile.xml  ShareFile  sharefile.pcap
shazam.xml  Shazam Entertainment  shazam.pcap
sip.xml  SIP Call  sip.pcap
skype-for-business.xml  Skype for Business  skype-for-business.pcap
skype.xml  Skype  skype.pcap
sky-sports.xml  sky sports  sky-sports.pcap
slack.xml  Slack  slack.pcap
smpp.xml  SMPP  smpp.pcap
smtps.xml  SMTPS  smtps.pcap
smtp.xml  SMTP  smtp.pcap
snapchat-mobile.xml  Snapchat  snapchat-mobile.pcap
snapdeal.xml  Snapdeal  snapdeal.pcap
snmp.xml  SNMP  snmp.pcap
soundcloud-mobile.xml  SoundCloud  soundcloud-mobile.pcap
soundhound-mobile.xml  SoundHound Mobile  soundhound-mobile.pcap
speedtest-mobile.xml  SpeedTest Mobile  speedtest-mobile.pcap
spotify.xml  Spotify  spotify.pcap
srvloc.xml  SRVLOC  srvloc.pcap
stat.xml  STAT  stat.pcap
steam.xml  Steam  steam.pcap
stratum.xml  Stratum  stratum.pcap
sunrpc.xml  SunRPC  sunrpc.pcap
tango-mobile.xml  Tango  tango-mobile.pcap
ted-mobile.xml  TED  ted-mobile.pcap
tftp.xml  TFTP  tftp.pcap
tor-ip.xml  Tor  tor-ip.pcap
tpncp.xml  TPNCP  tpncp.pcap
tripadvisor-city-guides-catalog.xml  TripAdvisor City Guides Catalog  tripadvisor-city-guides-catalog.pcap
tripadvisor-mobile.xml  TripAdvisor  tripadvisor-mobile.pcap
ts2.xml  TeamSpeak  ts2.pcap
tumblr.xml  Tumblr  tumblr.pcap
tv4play-mobile.xml  TV4 Play (Sweden)  tv4play-mobile.pcap
twitchtv.xml  Twitch  twitchtv.pcap
twitpic-mobile.xml  TwitPic  twitpic-mobile.pcap
twitter.xml  Twitter  twitter.pcap
usa-today.xml  USA Today  usa-today.pcap
ustream-mobile.xml  Ustream  ustream-mobile.pcap
viber-media.xml  Viber Media  viber-media.pcap
vimeo-mobile.xml  Vimeo  vimeo-mobile.pcap
vine-mobile.xml  Vine  vine-mobile.pcap
vk-mobile.xml  VKontakte  vk-mobile.pcap
vnc.xml  VNC  vnc.pcap
vudu.xml  Vudu  vudu.pcap
webex.xml  WebEx  webex.pcap
wechat.xml  WeChat  wechat.pcap
weheartit-mobile.xml  We Heart It  weheartit-mobile.pcap
wikileaks.xml  WikiLeaks  wikileaks.pcap
wikipedia.xml  Wikipedia  wikipedia.pcap
windows-live-mail.xml  Windows Live Mail  windows-live-mail.pcap
windows-live-skydrive.xml  Microsoft SkyDrive  windows-live-skydrive.pcap
windows-phone-marketplace-mobile.xml  Windows Phone Marketplace  windows-phone-marketplace-mobile.pcap
wordpress-com.xml  WordPress  wordpress-com.pcap
workday.xml  Workday  workday.pcap
xunlei.xml  Xunlei  xunlei.pcap
yahoo-mail.xml  Yahoo! Mail  yahoo-mail.pcap
yahoo-messenger.xml  Yahoo! Messenger  yahoo-messenger.pcap
yahoo.xml  Yahoo!  yahoo.pcap
yammer.xml  Yammer  yammer.pcap
yelp.xml  Yelp  yelp.pcap
youku-mobile.xml  Youku  youku-mobile.pcap
youtube-remote.xml  YouTube Remote  youtube-remote.pcap
youtube.xml  YouTube  youtube.pcap
zalo.xml  Zalo  zalo.pcap
zdnet.xml  ZDNet  zdnet.pcap
zedge-mobile.xml  Zedge  zedge-mobile.pcap
zendesk.xml  Zendesk  zendesk.pcap



*** Keywords ***

Setup PCAP ATIP Env
    Create NTO Config AppFwd Bidi for Pcap App Sig
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    Ifconfig MTU

Recover ATIP Env
    Create NTO Config
    #clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    logout  ${atipSession}

Validate Signature
    [Arguments]  ${appName}  ${pcapName}
    Log  \nValidate application signature: ${appName}  console=true
    Log  Resetting statistics...  console=true
    AtipSystem.resetStats  ${atipSession}

    Send Pcap  ${pcapName}

    Log  Looking for application in top stats...  console=true
    ${timeIntervalList} = 	getTimeIntervals
    :FOR    ${timeInterval}    IN    @{timeIntervalList}
        \  Log  ${timeInterval}
        # Discussion with Deep to inspect top 50 list instead of the default (top 10)
        ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  ${topStatsLimit}
        # In the future we will collect the number of session information & app percentage for now just log stats for the place holder.
        # Action Item: AppSig Improvement
        Log  ${atipStats}
        checkTargetExistsInTopStats  ${atipStats}  ${appName}
        ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}
        # In the future we will collect the number of session information & app percentage for now just log stats for the place holder.
        # Action Item: AppSig Improvement
        Log  ${atipStats}
        checkTargetDoesNotExistInTopStats  ${atipStats}  ${appName}

Send Pcap
    [Arguments]  ${pcapName}

    #BPS setup equivalent sleep
    sleep   10s
    #  Open Telnet Session
    ${session_telnet}=   Open Connection     ${envVars.linuxConnectionConfig.host}     prompt=$
    #  Telnet to the Linux Box to capture Netflow
    Tnet.Login  ${envVars.linuxConnectionConfig.username}   ${envVars.linuxConnectionConfig.password}
    Log  Running pcap file: ${pcapName}  console=true
    #  Run pcap file
    # 03/14 reverting back to sudo with password so that the script can run on the current setup
    #Execute Command  sudo tcpreplay -t -i ${envVars.linuxConnectionConfig.port} ~/final/${pcapName}
    Write  sudo tcpreplay -t -i ${envVars.linuxConnectionConfig.port} ~/final/${pcapName}
    Read Until  ${envVars.linuxConnectionConfig.username}:${SPACE}
    Write   ${envVars.linuxConnectionConfig.password}
    Read Until Prompt
    Log  Done pcap file: ${pcapName}  console=true
    #BPS teardown equivalent sleep
    Log  waiting until the statistics settles down     console=true
    sleep   50s
    Close Connection

Ifconfig MTU
    [Arguments]  ${MTU}=9000

     ${session_telnet}=   Open Connection     ${envVars.linuxConnectionConfig.host}     prompt=$
    #  Telnet to the Linux Box to capture Netflow
    Tnet.Login  ${envVars.linuxConnectionConfig.username}   ${envVars.linuxConnectionConfig.password}
    Write  sudo ifconfig ${envVars.linuxConnectionConfig.port} mtu ${MTU}
    Read Until  ${envVars.linuxConnectionConfig.username}:${SPACE}
    Write   ${envVars.linuxConnectionConfig.password}
    Read Until Prompt
    Close Connection