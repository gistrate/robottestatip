from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.data.atip.StatsType import StatsType


class SuiteConfig(object):

    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)

    appStatsType = StatsType.APPS
    dynAppStatsType = StatsType.DYNAMIC

    minimizedDashboard = DashboardType.MINIMIZED.statsLimit
    maximizedDashboard = DashboardType.MAXIMIZED.statsLimit

    # expected values
    httpStatusOK = HTTPStatus.OK.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
