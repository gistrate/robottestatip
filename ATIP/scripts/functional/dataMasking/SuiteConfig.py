import jsonpickle
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.DataMaskingConfig import DataMaskingConfig
from atipAutoFrwk.config.atip.MplsParsingConfig import MplsParsingConfig
from atipAutoFrwk.config.atip.HeaderMask import HeaderMask
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.webApi.atip.PayloadMasks import PayloadMasks
from atipAutoFrwk.config.atip.PayloadMask import PayloadMask


class SuiteConfig(StatsDefaultConfig):

    bpsTestConfig = BpsTestConfig('DM_L3VPN', 15)
    maxHdrMasks = 500
    maxRgxMasks = 512
    predefRgxMasks = 8


    def configureHeaderMaskWithFwdFilter(self, webApiSession, character, mplsType, maskName, length, header, startOffset, filterName, vlanId=100):

        atipConfig = AtipConfig()

        dataMasking = DataMaskingConfig(True, character)
        atipConfig.setDataMasking(dataMasking)

        mplsParsing = MplsParsingConfig(True, mplsType, False)
        atipConfig.setMplsParsing(mplsParsing)


        headerMask = HeaderMask(startOffset, 0, maskName, header, length)

        filterConfig = FilterConfig(filterName)
        filterConfig.forward = True
        filterConfig.addHeaderMaskUUIDS(headerMask.name)

        if self.envVars.atipType == AtipType.HARDWARE:
            filterConfig.addTransmitIds(vlanId)
        atipConfig.addHeaderMask(headerMask)
        atipConfig.addFilters(filterConfig)


        return atipConfig


    def configurePayloadMaskWithFwdFilter(self, webApiSession, character, offsetEnd, regex, maskName, offsetStart, filterName, creditCard, vlanId=100):

        atipConfig = AtipConfig()

        dataMasking = DataMaskingConfig(True, character)
        atipConfig.setDataMasking(dataMasking)

        payloadMask = PayloadMask(regex, 0, offsetStart, maskName, offsetEnd, False, 0, creditCard)

        filterConfig = FilterConfig(filterName)
        filterConfig.forward = True
        if StatsDefaultConfig.envVars.atipType == AtipType.HARDWARE:
            filterConfig.addTransmitIds(vlanId)

        if regex != 'None':
            filterConfig.addRegexMaskUUIDS(payloadMask.name)
            atipConfig.addPayloadMask(payloadMask)
            atipConfig.addFilters(filterConfig)
        else:
            AtipConfigService.createDataMasking(webApiSession, atipConfig)
            PayloadMasks.updatePayloadConfig(webApiSession, payloadMask)

            filterID = PayloadMasks.getPayloadConfigByName(webApiSession, maskName)
            filterConfig.addRegexMaskUUIDS(filterID['masks'][0]['id'])
            atipConfig.addFilters(filterConfig)
            # AtipConfigService.createFilters(webApiSession, atipConfig)

        return atipConfig

