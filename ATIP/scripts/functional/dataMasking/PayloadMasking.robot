*** Settings ***
Documentation  Payload masking test suite.

Variables  SuiteConfig.py

Library    scripts/functional/dataMasking/SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.DataMaskingService
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    ../../frwk/src/atipAutoFrwk/webApi/atip/PayloadMasks.py
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    ../../../scripts/functional/stats/lib/StatsDefaultConfig.py
Library    Telnet  timeout=30  prompt=$
Library    String
Library    BuiltIn

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Config ATIP
Suite Teardown     Clean ATIP

Test Template  Configure Device and Run BPS Test

*** Test Cases ***

TC000894711  C  3  \\([0-9]{3}\\)[0-9]{3}-[0-9]{4}  phone       5     myfilter         DM_gmail_phone                        eth0    )CCCCC             7    14   None   (994)946-0114      false
TC000894706  Q  0  None                           Visa        0     visa             DM_visa_amex_discover_mastercard      eth0    QQQQQQQQQQQQQQQQ   34   34   None   4696257730186563   true
    [Tags]  hardware
TC000899704  X  0  None                           Discover    0     discover         DM_discover                           eth0    XXXXXXXXXXXXXXX    160  160  None   6011649900252105   true
TC001006003  ?  0  None                           Visa        4     visa_cc          DM_visa_ccvalidation_auto             eth0    4916????????????   4    4    None    4916629783970107  true
TC001006003b  ?  0  None                           Visa        4     visa_cc_off      DM_visa_ccvalidation_auto             eth0    4916????????????   6    6    None    4916629783970107  false
TC001006004  ?  2  None                           Mastercard  2     mc_cc            DM_mastercard_ccvalidation_auto       eth0    53????????????60   4    4    None    5379789757342860  true
TC001006005  ?  0  None                           Amex        0     amex_cc          DM_amex_ccvalidation_auto             eth0    ???????????????    4    4    None    376026881881891   true
TC001006006  *  0  None                           DinersClub  4     dinersclub_cc    DM_dinersclub_ccvalidation_auto       eth0    3037**********     10   10   None    30373592003441    true
TC001006007  C  4  None                           Discover    0     discover_cc      DM_discover_ccvalidation_auto         eth0    CCCCCCCCCCCC5296   6    6    None    6011187183755296  true
TC001006008  *  0  None                           JCB         10    jcb_cc           DM_jcb_ccvalidation_auto              eth0    3528519198******   4    4    None    3528519198876514  true
TC001006009  $  4  5020[0-9]{12}                  maestro     4     maestro_cc_off   DM_custom_maestro_ccvalidation_auto   eth0    5020$$$$$$$$654    9    9    None    5020772875016544  false
TC001006009b  $  4  5020[0-9]{12}                  maestro     4     maestro_cc       DM_custom_maestro_ccvalidation_auto   eth0    5020$$$$$$$$6544   7    7    None    5020772875016544  true


*** Keywords ***

Configure Device and Run BPS Test
    [Arguments]  ${character}  ${offsetEnd}  ${regex}  ${maskName}  ${offsetStart}  ${filterName}  ${bpsScript}  ${eth}  ${niddle}  ${niddleMatches}  ${expectedStatsValue}  ${appName}  ${sample}  ${creditCard}

    ${atipConfig}=  configurePayloadMaskWithFwdFilter  ${atipSession}  ${character}  ${offsetEnd}  ${regex}  ${maskName}  ${offsetStart}  ${filterName}  ${creditCard}
    createConfig  ${atipSession}  ${atipConfig}  ${envVars}

    #Reset Stats
    resetStats  ${atipSession}

    #Connect to Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Write  su
    sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt

    #Start traffic capture

    Write  tcpdump -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap
    Read Until  ${envVars.linuxConnectionConfig.port}

    #Start sending traffic
    import library  ../../frwk/src/atipAutoFrwk/config/traffic/BpsTestConfig.py  ${bpsScript}  100  WITH NAME  bpsTestConfigInit
	${myClassInstance}=  get library instance  bpsTestConfigInit
	Set Test Variable  ${bpsTestConfigInit}  ${myClassInstance}
	bpsTestConfigInit.reinit  ${bpsScript}  100
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigInit}
    Sleep  13s

    #Stop capture on Linux Box
    Write Control Character     BRK
    Read Until Prompt

    #Filter capture file and export matching packets to a result file
    ${tsharkCommand}=  filterCapture  ${niddle}  ${TEST NAME}
    Execute Command  ${tsharkCommand}

    #Validate result file content against expected result
    Matches Validation  ${TEST NAME}  ${niddleMatches}

    ${tsharkCommand}=  filterCapture  ${sample}  ${TEST NAME}
    Execute Command  ${tsharkCommand}
    Matches Validation  ${TEST NAME}  0

    Wait Until Keyword Succeeds  15s  3s  Validate Payload Mask  ${maskName}  ${expectedStatsValue}

    #delete capture and result file from Linux Box
    Execute Command  rm ${TEST NAME}*

Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [return]  ${output}

Matches Validation
    [Arguments]  ${STRING}  ${expectedValue}
    ${output}=  Execute Command  wc -l < ${STRING}.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Be Equal As Integers  ${statsCaptureLB}  ${expectedValue}

Validate Payload Mask
    [Arguments]  ${maskName}  ${expectedValue}
    ${pmStats}=  getPayloadConfigByName  ${atipSession}  ${maskName}
    ${item}=  Get From Dictionary  ${pmStats}  masks
    ${el}=  Get From List  ${item}  0
    ${rptCount}=  Get From Dictionary  ${el}  rpt_count
    Should Be Equal As Integers  ${rptCount}  ${expectedValue}

Config ATIP
    atipLogin.login  ${atipSession}
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}

Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    atipLogout.logout  ${atipSession}


