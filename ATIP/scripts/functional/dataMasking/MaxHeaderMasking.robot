*** Settings ***
Documentation  Maximum header masks test.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.DataMaskingService
Library    atipAutoFrwk.services.atip.SystemService.SystemService

Default Tags  ATIP  functional  ${hardware}  ${virtual}

Suite Setup        Configure ATIP  C
Suite Teardown     Clean ATIP

*** Test Cases ***
TC000894737
    Configure Device and Run BPS Test  ${atipSession}  4  L3

*** Keywords ***

Configure Device and Run BPS Test
    [Arguments]  ${atipSession}  ${length}  ${header}

    #create maximum number of header masks
    :FOR  ${i}  IN RANGE  ${maxHdrMasks}
    \    createHeaderMask  ${atipSession}  hdrMask${i}  ${length}  ${header}  ${i}

    #negative testing: create one more header mask; this should fail
    ${testStatus}=  Run Keyword And Return Status  createHeaderMask  ${atipSession}  hdrMask${maxHdrMasks}  ${length}  ${header}  ${maxHdrMasks}
    Should Be Equal As Strings  ${testStatus}  ${False}

Configure ATIP
    [Arguments]  ${character}
    atipLogin.login  ${atipSession}
    #enable data masking
    configureDataMasking  ${atipSession}  ${True}  ${character}

Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    atipLogout.logout  ${atipSession}

