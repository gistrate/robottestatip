*** Settings ***
Documentation  Header masking test suite.

Variables  SuiteConfig.py

Library    scripts/functional/dataMasking/SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    Collections
Library    atipAutoFrwk.services.atip.DataMaskingService
Library    atipAutoFrwk.webApi.atip.DataMasking.DataMasking
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    atipAutoFrwk.services.GeneralService
Library    ../../../scripts/functional/stats/lib/StatsDefaultConfig.py
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    Telnet  timeout=30   prompt=$
Library    String
Library    BuiltIn

Default Tags  ATIP  functional  ${hardware}  ${virtual}

Suite Setup        Clean ATIP
Suite Teardown     Clean ATIP

Test Template  Configure Device and Run BPS Test

*** Test Cases ***
# testID     char  mplsType   maskName  length     header      startOffset   niddle   niddleMatches   expectedStatsValue   filterName     sample            sampleCount
TC000894718   O    L3_VPN      l3vpn      4     L2_VLAN_MPLS        8         OOOO        30000             30000             l3vpn     00:01:00:40              0
TC000894712   P    L3_VPN      l2mac      6          L2             6       PPPPPP        30000             30000             l2mac     00:00:01:00:00:01      10000
TC000894714   Q    L3_VPN      vlan       4       L2_VLAN           4         QQQQ        30000             30000             l2vlan    00:C8:88:47              0
TC000894720   R    L3_VPN      sip        4          L3            12         RRRR        30000             30000            sourceIp   01:01:01:16              1
TC000894721   W    L3_VPN     dport       2          L4             2           WW        30000             30000            destPort   00:50:57:57            10000


*** Keywords ***

Configure Device and Run BPS Test
    [Arguments]  ${character}  ${mplsType}  ${maskName}  ${length}  ${header}  ${startOffset}  ${niddle}  ${niddleMatches}  ${expectedStatsValue}  ${filterName}  ${sample}  ${sampleCount}

    ${atipConfig}=  configureHeaderMaskWithFwdFilter  ${atipSession}  ${character}  ${mplsType}  ${maskName}  ${length}  ${header}  ${startOffset}  ${filterName}
    createConfig  ${atipSession}  ${atipConfig}  ${envVars}

    #Reset Stats
    resetStats  ${atipSession}

    #Connect to Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Write  su
    Sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set Prompt  \#
    Read Until Prompt

    #Start traffic capture

    Write  tcpdump -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap -c 30000 -B 30720 not multicast
    Read Until  ${envVars.linuxConnectionConfig.port}

    #Start sending traffic
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    # wait for capture to receive all files
    Sleep  5s

    #Stop capture on Linux Box
    Write Control Character     BRK
    Read Until Prompt

    #Filter capture file and export matching packets to a result file
    ${tsharkCommand}=  filterCapture  ${niddle}  ${TEST NAME}
    Execute Command  ${tsharkCommand}

    #Validate result file content against expected result
    Matches Validation  ${TEST NAME}  ${niddleMatches}

    ${tsharkCommand}=  filterCapture  ${sample}  ${TEST NAME}  hex
    Execute Command  ${tsharkCommand}
    Matches Validation  ${TEST NAME}  ${sampleCount}

    Wait Until Keyword Succeeds  20s  5s  Validate Header Mask  ${maskName}  ${expectedStatsValue}

    #delete capture and result file from Linux Box
    Execute Command  rm ${TEST NAME}*

Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [return]  ${output}

Matches Validation
    [Arguments]  ${STRING}  ${expectedValue}
    ${output}=  Execute Command  wc -l < ${STRING}.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Be Equal As Integers  ${statsCaptureLB}  ${expectedValue}

Validate Header Mask
    [Arguments]  ${maskName}  ${expectedValue}
    ${hmStats}=  getHeaderMaskByName  ${atipSession}  ${maskName}
    ${item}=  Get From Dictionary  ${hmStats}  masks
    ${el}=  Get From List  ${item}  0
    ${rptCount}=  Get From Dictionary  ${el}  rpt_count
    Should Be Equal As Integers  ${rptCount}  ${expectedValue}

Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}


