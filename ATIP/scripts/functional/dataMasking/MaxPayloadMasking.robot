*** Settings ***
Documentation  Maximum payload masks test.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.DataMaskingService
Library    atipAutoFrwk.services.atip.SystemService.SystemService

Default Tags  ATIP  functional  ${hardware}  ${virtual}

Suite Setup        Configure ATIP  C
Suite Teardown     Clean ATIP

*** Test Cases ***
Create maximum number of payload masks
    Configure Device and Run BPS Test  ${atipSession}  3  False

*** Keywords ***

Configure Device and Run BPS Test
    [Arguments]  ${atipSession}  ${offsetEnd}  ${readOnly}

    ${maxCustomRgxMasks}=  Set Variable  ${maxRgxMasks} - ${predefRgxMasks}

    #create maximum number of payload masks
    :FOR  ${i}  IN RANGE  ${maxCustomRgxMasks}
    \    createCustomPayloadMask  ${atipSession}  rgxMask${i}  p${i}  ${i}  ${offsetEnd}  ${readOnly}

    #negative testing: create one more payload mask; this should fail
    ${testStatus}=  Run Keyword And Return Status  createCustomPayloadMask  ${atipSession}  rgxMask${maxRgxMasks}  p${maxRgxMasks}  ${maxRgxMasks}  ${offsetEnd}  ${readOnly}
    Should Be Equal As Strings  ${testStatus}  ${False}

Configure ATIP
    [Arguments]  ${character}
    atipLogin.login  ${atipSession}
    #enable data masking
    configureDataMasking  ${atipSession}  ${True}  ${character}

Clean ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    atipLogout.logout  ${atipSession}

