from atipAutoFrwk.config.atip.ReleaseFileConfig import ReleaseFileConfig
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval

class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    signatureFile = ReleaseFileConfig(envVars.buildServer, 'atie-signature.bin')
    customSignatureFile = '../../../frwk/src/atipAutoFrwk/data/atip/bin/atie-signature_lamborghini.bin'
    customSoftwareFile = '../../../frwk/src/atipAutoFrwk/data/atip/bin/atip-upgrade_1.3.3-263785.bin'
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    intervalHour = TimeInterval.HOUR