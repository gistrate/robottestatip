import re
from atipAutoFrwk.webApi.atip.Update import Update

class SystemSettingsLib(object):
    @classmethod
    def uploadSoftware(cls, webApiSession, pathFile):
        # upload software to ATIP and returns error message
        status = ''
        try:
            Update.updateSoftware(webApiSession, pathFile)
        except Exception as e:
            status = str(e)

        return status

    @classmethod
    def getStatusMessage(cls, message):
        # checks and returns, if present, desired error message
        regex = re.compile('.*(Software Update Failed)')
        res = regex.search(message)

        return res.group(1)