*** Settings ***
Documentation  System Settings test suite.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.webApi.atip.Update
Library    atipAutoFrwk.services.UrlFileDownloadService
Library    atipAutoFrwk.services.atip.FileService
Library    atipAutoFrwk.webApi.atip.InstalledApps
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.webApi.atip.stats.Ports  WITH NAME  ports
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  lists
Library    atipAutoFrwk.webApi.atip.ConfigureStats
Library    BuiltIn
Library    String
Library    SystemSettingsLib.py

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP
Suite Teardown     atipLogout.Logout  ${atipSession}


*** Test Cases ***
# testId                           fileToUpload
updateSoftwareVersion    bin\atip-upgrade_1.3.3-263785.bin
    [Template]  UpdateSoftwareVersion
    [Tags]  hardware

# testId              application
TC001018532           lamborghini
    [Template]  UpdateAtiVersion
    [Tags]  hardware

# testId          bpsScript
TC001018525      DEVICE_nokia
    [Template]  AtipRestartVerify

# testId            bpsScript               application    country    device    browser
TC001018558   20Gbps-HTTP-netflix_5min        netflix       china      macos     chrome
    [Template]  Clear Statistics
    [Tags]  hardware
TC001018558b  1Gbps-HTTP-netflix_5min         netflix       china      macos     chrome
    [Tags]  virtual
    [Template]  Clear Statistics

*** Keywords ***
UpdateSoftwareVersion
    [Arguments]  ${fileToUpload}

    ${status}=  uploadSoftware  ${atipSession}  ${customSoftwareFile}
    ${statusMessage}=  getStatusMessage  ${status}

    Should Be Equal As Strings  ${statusMessage}  Software Update Failed


UpdateAtiVersion
    [Arguments]  ${application}

    #atipLogin.Login  ${atipSession}

    # update ATI with custom signatures file
    ${status}=  updateAti  ${atipSession}  ${customSignatureFile}

    # validate signature file has been successfuly installed
    Validate ATI Signature Update  ${status}

    # validate custom application is found in installed applications list
    ${search}=  searchInstalledApps  ${atipSession}  ${application}
    Should Not Be Empty  ${search}

    # download latest signature file
    ${signatureFile}=  getLatestBuildUrlLocation  ${envVars}  ${signatureFile}
    ${signatureFileLocalPath}=  getFileFromUrlLocation  ${signatureFile}  atie-signature.bin

    # update ATI with most recent signatures file
    ${status}=  updateAti  ${atipSession}  ${signatureFileLocalPath}

    # validate signature file has been successfuly installed
    Validate ATI Signature Update  ${status}

    # validate custom application is removed from installed applications list
    ${search}=  searchInstalledApps  ${atipSession}  ${application}
    Should Be Empty  ${search}

    # delete local downloaded signatures file
    deleteFile  ${signatureFileLocalPath}

Atip Restart Verify
    [Arguments]  ${bpsTest}

    resetStats  ${atipSession}

    # disable netflow acceleration
    ${detailedStats}=  getConfigureStatsObj  ${FALSE}
    changeConfig  ${atipSession}  ${detailedStats}

    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTest}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    resetAtip  ${atipSession}

    waitUntilATIPReady  ${atipSession}
    atipLogin.Login  ${atipSession}

    ${stats}=  ports.getTopStats  ${atipSession}  ${intervalHour}
    ${statsBytes}=  call method  ${stats}  getBytes
    ${statsSessions}=  call method  ${stats}  getSessions

    Should Not Be Equal As Numbers  ${statsBytes}  0
    Should Not Be Equal As Numbers  ${statsSessions}  0

    ${topDevice}=  Get Top Device
    ${topBrowser}=  Get Top Browser

    Should Be Equal As Strings  ${topDevice}  nokia
    Should Be Equal As Strings  ${topBrowser}  browserng

Clear Statistics
    [Arguments]  ${bpsTest}  ${application}  ${country}  ${device}  ${browser}
    atipLogin.Login  ${atipSession}

    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTest}  300
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    ${appDistribution}=  Get App Distribution Item
    ${topBrowser}=  Get Top Browser
    ${topDevice}=  Get Top Device
    ${topCountries}=  Get Top Country Item

    Should Be Equal As Strings  ${appDistribution}  ${application}
    Should Be Equal As Strings  ${topBrowser}  ${browser}
    Should Be Equal As Strings  ${topDevice}  ${device}
    Should Be Equal As Strings  ${topCountries}  ${TRUE}

    resetStats  ${atipSession}

    ${appDistribution}=  Get App Distribution Item
    ${topBrowser}=  Get Top Browser
    ${topDevice}=  Get Top Device
    ${topCountries}=  Get Top Country Item

    Should Be Equal  ${appDistribution}  ${None}
    Should Be Equal  ${topBrowser}  ${None}
    Should Be Equal  ${topDevice}  ${None}
    Should Be Equal  ${topCountries}  ${None}

Get Top Device
    ${pieTopDevices}=  getTopDevices  ${atipSession}
    ${pieTopDevicesList}=  call method  ${pieTopDevices}  getList
    ${pieTopDeviceName}=  Run Keyword If  '${pieTopDevicesList}' != '[]'   Get Stat Name    ${pieTopDevicesList}
    ...    ELSE    Set Variable  ${None}
    [Return]  ${pieTopDeviceName}

Get Top Browser
    ${pieTopBrowsers}=  getTopBrowsers  ${atipSession}
    ${pieTopBrowsersList}=  call method  ${pieTopBrowsers}  getList
    ${pieTopBrowserName}=  Run Keyword If  '${pieTopBrowsersList}' != '[]'   Get Stat Name    ${pieTopBrowsersList}
    ...    ELSE    Set Variable  ${None}
    [Return]  ${pieTopBrowserName}

Get App Distribution Item
    ${pieAppDistribution}=  getAppDistribution  ${atipSession}
    ${pieAppDistributionList}=  call method  ${pieAppDistribution}  getList
    ${pieAppDistributionName}=  Run Keyword If  '${pieAppDistributionList}' != '[]'   Get Stat Name    ${pieAppDistributionList}
    ...    ELSE    Set Variable  ${None}
    [Return]  ${pieAppDistributionName}

Get Top Country Item
    ${topCountries}=  lists.getTopStats  ${atipSession}
    ${topCountriesList}=  call method  ${topCountries}  getList
    ${statusCountry}=  Set Variable  ${None}
    ${statusCountry}=  Run Keyword If  '${topCountriesList}' != '[]'   Verify Country    ${topCountriesList}
    [Return]  ${statusCountry}

Verify Country
    [Arguments]  ${topCountriesList}
    ${found}=  Set Variable  ${FALSE}
    :FOR  ${countryStat}   IN  @{topCountriesList}
    \    ${found}=    Set Variable If    "${countryStat.msg}" == "China"    ${TRUE}
    \    Run Keyword If    '${found}' == '${TRUE}'    Exit For Loop
    [Return]  ${found}

Get Stat Name
    [Arguments]  ${distributionList}
    ${distributionItem}=  Get From List  ${distributionList}  0
    ${distributionItemName}=  Call Method  ${distributionItem}  getName
    [Return]  ${distributionItemName}

Validate ATI Signature Update
    [Arguments]  ${response}

    ${string}=  decode bytes to string   ${response}  UTF-8
    log  ${string}
    ${string1}=  Strip String  ${string}  characters={}
    ${string}=  fetch from right  ${string1}  :
    ${string1}=  Strip String  ${string}  characters="
    Should Be Equal As Strings  ${string1}  ATI Update Successful

Configure ATIP
    atipLogin.Login  ${atipSession}

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}