*** Settings ***
Documentation  Application signature detection test cases.

Variables  SuiteConfig.py

Library    ATIP.frwk.src.atipAutoFrwk.services.atip.SystemService
Library    Selenium2Library  run_on_failure=Nothing  implicit_wait=0

Force Tags  ATIP  functional  ${hardware}  ${virtual}  GUI  settings

Suite Setup        OpenATIP
Suite Teardown     ExitATIP

*** Test Cases ***

CreateCustomApplication
    loadCustomApp  ${EXECDIR}/../../../frwk/src/atipAutoFrwk/data/custapps/cdk-service-edge.xml
    verifyCustomApp  CDK Service Edge  3
    
*** Keywords ***

OpenATIP
    log  Performing suite setup for GUI automation...  console=true
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    import library  ATIP.frwk.src.atipAutoFrwk.gui.ATIPWebUI  WITH NAME  atipGUI
    ${myClassInstance}=  Get Library Instance  atipGUI
    Set Suite Variable  ${atipGUI}  ${myClassInstance}
    Login
    gotoSettingsPage

ExitATIP
    log  Performing suite teardown after GUI automation...  console=true
    Logout
    ExitBrowser
    