from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig

class SuiteConfig(StatsDefaultConfig):

    # Instantiating this derived class creates other pre-requisite objects needed for test execution
    print('Setting up for test execution...')
