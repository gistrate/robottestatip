from http import HTTPStatus
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.MaxItemsPerCategory import MaxItemsPerCategory


class SuiteConfig(StatsDefaultConfig):
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    appStatsType = StatsType.APPS
    dynAppStatsType = StatsType.DYNAMIC
    timeInterval = TimeInterval.FIVEMIN
    dashBoardType = DashboardType.MINIMIZED
    minimizedDashboard = DashboardType.MINIMIZED.statsLimit

    g1 = GroupConfig(name='Test1')
    g2 = GroupConfig(name='Test2')
    g3 = GroupConfig(name='Test3')

    atipConfig = AtipConfig()

    atipConfig.addGroup(g1)
    atipConfig.addGroup(g2)
    atipConfig.addGroup(g3)

    npId0 = str(StatsDefaultConfig.envVars.atipSlotNumber[0])
    if StatsDefaultConfig.envVars.atipType == hardware:
        npId1 = str(StatsDefaultConfig.envVars.atipSlotNumber[1])
        npId2 = str(StatsDefaultConfig.envVars.atipSlotNumber[2])

    appFilter1 = FilterConfig("skysports", groupId=0)
    FilterConditionConfig.addAppCondition(appFilter1, ApplicationType.SKYSPORTS)
    appFilter2 = FilterConfig("gopher", groupId=0)
    FilterConditionConfig.addAppCondition(appFilter2, ApplicationType.GOPHER)
    appFilter3 = FilterConfig("Vimeo", groupId=0)
    FilterConditionConfig.addAppCondition(appFilter3, ApplicationType.VIMEOMOBILE)

    bpsTestConfig1 = BpsTestConfig('neo RECREATE skysports',400)
    bpsTestConfig2 = BpsTestConfig('neo RECREATE gopher',400)
    bpsTestConfig3 = BpsTestConfig('neo RECREATE vimeoMobile',400)

    maxStaticApps = MaxItemsPerCategory.MAX_STATIC_APPS.maxNumber