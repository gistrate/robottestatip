*** Settings ***
Documentation  Installed Application NP Group

Variables  SuiteConfig.py

Library    StaticApp.py
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.config.atip.filter.FilterConditionConfig
Library    atipAutoFrwk.config.atip.filter.FilterConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    scripts.functional.NP_Group.Custom_Application.CustomApp
Library    atipAutoFrwk.services.atip.NPService

Force Tags  hardware
Suite Setup        Configure ATIP And Login
Suite Teardown     Clean ATIP

*** Test Cases ***
TC001161056_Installed_Application_NP_Group1_MoveNP_Configure
    [Tags]
    #Create new NP Group: Test1
    ${gid1} =  getGroupId  ${atipSession}  Test1
    log  ${gid1}

    #Move NP 2 to Group Test1
    changeNPGroup  ${atipSession}  ${npId1}  ${gid1}
    waitUntilGroupReady  ${atipSession}  ${gid1}

    #Check if sky-sports is in installed apps datbase
    checkForInstalledApp  ${atipSession}  sky sports  ${gid1}

    #Add app filter to group Test1 Only if the app exists
    ${appFilter1.groupId} =  set variable  ${gid1}
     createFilter  ${atipSession}  ${appFilter1}
     rulePush  ${atipSession}  ${gid1}

    #Reset Stats
    resetStats  ${atipSession}

    Log  Running BPS test1: ${bpsTestConfig1.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}

    Log  Looking for application in top stats...  console=true
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid1}
    checkTargetExistsInTopStats  ${atipStats}  sky sports
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid1}
    checkTargetDoesNotExistInTopStats  ${atipStats}  sky sports

    Log  Checking if sky-sports exists in Top Filters Widget : ${appFilter1.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${appFilter1}  ${timeInterval}  ${dashBoardType}

    #Expecting 240 static apps to be installed for Group1
    Log   Validating the correct number of static apps installed....  console=true
    checkNumberStaticApps  ${atipSession}  ${maxStaticApps}  ${npId1}  60  6


ITC001161056_Installed_Application_NP_Group2_Configure_MoveNP
    [Tags]
    #Create new NP Group: Test2
    ${gid2} =  getGroupId  ${atipSession}  Test2
    log  ${gid2}

    #Check if gopher is in installed apps datbase
    checkForInstalledApp  ${atipSession}  Gopher  ${gid2}

    #Add app filter to group Test2 Only if the app exists
    ${appFilter2.groupId} =  set variable  ${gid2}
     createFilter  ${atipSession}  ${appFilter2}
     rulePush  ${atipSession}  ${gid2}

    #Move NP 3 to Group Test2
    changeNPGroup  ${atipSession}  ${npId2}  ${gid2}
    waitUntilGroupReady  ${atipSession}  ${gid2}

    #Reset Stats
    resetStats  ${atipSession}

    Log  Running BPS test2: ${bpsTestConfig2.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig2}

    Log  Looking for application in top stats...  console=true
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid2}
    checkTargetExistsInTopStats  ${atipStats}  Gopher
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid2}
    checkTargetDoesNotExistInTopStats  ${atipStats}  Gopher

    Log  Checking if gopher exists in Top Filters Widget : ${appFilter2.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${appFilter2}  ${timeInterval}  ${dashBoardType}

    #Expecting 240 static apps to be installed for Group2
    Log   Validating the correct number of static apps installed....  console=true
    checkNumberStaticApps  ${atipSession}  ${maxStaticApps}  ${npId2}  60  6


TC001170499_Installed_Application_NP_Group3_vATIP
    [Tags]  virtual
    #Create new NP Group: Test3
    ${gid3} =  getGroupId  ${atipSession}  Test3
    log  ${gid3}

    #Check if vimeo-mobile is in installed apps datbase
    checkForInstalledApp  ${atipSession}  Vimeo  ${gid3}

     #Add app filter to group Test3 Only if the app exists
    ${appFilter3.groupId} =  set variable  ${gid3}
     createFilter  ${atipSession}  ${appFilter3}
     rulePush  ${atipSession}  ${gid3}

    #Move NP 1 to Group Test3
    changeNPGroup  ${atipSession}  ${npId0}  ${gid3}
    waitUntilGroupReady  ${atipSession}  ${gid3}

    #Reset Stats
    resetStats  ${atipSession}

    Log  Running BPS test3: ${bpsTestConfig3.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig3}

    Log  Looking for application in top stats...  console=true
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid3}
    checkTargetExistsInTopStats  ${atipStats}  Vimeo
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid3}
    checkTargetDoesNotExistInTopStats  ${atipStats}  Vimeo

    Log  Checking if vimeo exists in Top Filters Widget : ${appFilter3.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${appFilter3}  ${timeInterval}  ${dashBoardType}

    #Expecting 240 static apps to be installed for Group3
    Log   Validating the correct number of static apps installed....  console=true
    checkNumberStaticApps  ${atipSession}  ${maxStaticApps}  ${npId0}  60  6

*** Keywords ***
Configure ATIP And Login
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    configureAtip  ${atipSession}   ${atipConfig}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}

Clean ATIP
    logout  ${atipSession}