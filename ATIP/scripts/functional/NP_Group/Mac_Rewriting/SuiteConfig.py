from http import HTTPStatus

import jsonpickle
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.MplsParsingConfig import MplsParsingConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.webApi.atip.MacRewrites import MacRewrites
from atipAutoFrwk.data.atip.AtipType import AtipType

class SuiteConfig(object):

    bpsTestConfig1 = BpsTestConfig('DM_L3VPN', 15)
    bpsTestConfig2 = BpsTestConfig('MR_L3VPN_t2', 15)

    maxHdrMasks = 500
    maxRgxMasks = 512
    predefRgxMasks = 8
    envVars = Environment()
    hardware = AtipType.HARDWARE

    npId0 = str(envVars.atipSlotNumber[0])
    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[1])
        npId2 = str(envVars.atipSlotNumber[2])

    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)

    # expected values
    httpStatusOK = HTTPStatus.OK.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL

    gDefaultId = 0

    def configFilterMacRewrite(self, webApiSession, mplsType, filterName, macRewrite, groupId=0, vlanId=100):

        atipConfig = AtipConfig()

        filterConfig = FilterConfig(filterName, id=-1, groupId=groupId)
        filterConfig.forward = True

        mplsParsing = MplsParsingConfig(True, mplsType, False, groupId=groupId)
        atipConfig.setMplsParsing(mplsParsing)
        AtipConfigService.createMplsParsing(webApiSession, atipConfig)
        MacRewrites.getMacRewritesItems(webApiSession, groupId=groupId)
        config = MacRewrites.getMacRewritesItems(webApiSession,groupId)

        MacRewriteId = config.macRewriteList.get(macRewrite.name).id
        filterConfig.addMacRewriteUUIDS(MacRewriteId)

        if self.envVars.atipType == AtipType.HARDWARE:
            filterConfig.addTransmitIds(vlanId)

        atipConfig.addFilters(filterConfig)
        AtipConfigService.updateFiltersConfig(webApiSession, atipConfig)
        AtipConfigService.createFilters(webApiSession, atipConfig, self.envVars)
        Filters.rulePush(webApiSession, groupId)
