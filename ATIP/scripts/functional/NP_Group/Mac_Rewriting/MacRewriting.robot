*** Settings ***
Documentation    Mac Rewriting test suite
Variables  SuiteConfig.py

Library    scripts/functional/NP_Group/Mac_Rewriting/SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    Collections
Library    atipAutoFrwk.webApi.atip.DataMasking.DataMasking
Library    atipAutoFrwk.webApi.atip.PayloadMasks.PayloadMasks
Library    atipAutoFrwk.webApi.atip.MacRewrites.MacRewrites
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    Telnet  timeout=30   prompt=$
Library    String
Library    BuiltIn
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.webApi.atip.Groups

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        atipLogin.login  ${atipSession}
Test Setup         Clear Config
Test Teardown      Close All Connections
Suite Teardown     logout  ${atipSession}

*** Test Cases ***

TC001198138
    [Tags]  hardware
    # Default Group and 1 New Group with different Mac Rewriting Configuration (Config_MoveNP)

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${gDefaultId}

    ${macConfig}  addMac  ${atipSession}  both  MacMaskBoth  0002F425C139

    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterBoth  ${macConfig}  ${gDefaultId}

    Run Mac Mask Test and Validate  00:02:F4:25:C1:39  30000   60000  MacMaskBoth  00:00:01:00:00:01  0  ${gDefaultId}  ${bpsTestConfig1}  30000

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${g1id}
    ${macConfig2}  addMac  ${atipSession}  src  MacMaskSrc  0007E465C159  ${g1id}
    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterSrc  ${macConfig2}  ${g1id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run Mac Mask Test and Validate  00:07:E4:65:C1:59  30000  30000  MacMaskSrc  00:00:01:00:00:01  10000  ${g1id}  ${bpsTestConfig1}  30000

TC001198139
    [Tags]  hardware
    # Default Group and 1 New Group with different Mac Rewriting Configuration (MoveNP_Config)

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${gDefaultId}

    ${macConfig}  addMac  ${atipSession}  dst  MacMaskDst  0007E963CE53

    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterDst  ${macConfig}  ${gDefaultId}

    Run Mac Mask Test and Validate  00:07:E9:63:CE:53  30000   30000  MacMaskDst  00:00:01:00:00:01  20000  ${gDefaultId}  ${bpsTestConfig1}  30000

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${g1id}

    ${macConfig}  addMac  ${atipSession}  both  MacMaskBoth  0007E962C453  ${g1id}

    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterBoth  ${macConfig}  ${g1id}

    Run Mac Mask Test and Validate  00:07:E9:62:C4:53  30000   60000  MacMaskBoth  00:00:01:00:00:01  0  ${g1Id}  ${bpsTestConfig1}  30000

TC001198140
    [Tags]  hardware
    # 2xNew Groups with multiple NPs per group with different Mac Rewriting Configuration(Config_MoveNP)

    createNewGroup  ${atipSession}  Group1
    createNewGroup  ${atipSession}  Group2
    ${g1id}  getGroupId  ${atipSession}  Group1
    ${g2id}  getGroupId  ${atipSession}  Group2

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${g1id}
    ${macConfig}  addMac  ${atipSession}  src  MacMaskSrc  0007F93BDF53  ${g1id}

    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterSrc  ${macConfig}  ${g1id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run Mac Mask Test and Validate  00:07:F9:3B:DF:53  30000  30000  MacMaskSrc  00:00:01:00:00:01  10000  ${g1id}  ${bpsTestConfig1}  30000

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${g2id}
    ${macConfig}  addMac  ${atipSession}  dst  MacMaskDst  0007C72E2B53  ${g2id}

    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterDst  ${macConfig}  ${g2id}

    moveNPsToGroup  ${atipSession}  Group2  ${npId1}
    moveNPsToGroup  ${atipSession}  Group2  ${npId2}

    Run Mac Mask Test and Validate  00:07:C7:2E:2B:53  60000  60000  MacMaskDst  00:00:01:00:00:01  40000  ${g2id}  ${bpsTestConfig2}  60000

TC001198141
    [Tags]  hardware
    # Default Group and 1 New Group with different Data Masking Configuration (MoveNP_Config)

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${g1id}
    ${macConfig}  addMac  ${atipSession}  src  MacMaskSrc  0007F93C2A73  ${g1id}  \\x00\\x00\\x01\\x00\\x00\\x01

    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterSrc  ${macConfig}  ${g1id}

    Run Mac Mask Test and Validate  00:07:F9:3C:2A:73  20000  20000  MacMaskSrc  00:00:01:00:00:01  10000  ${g1id}  ${bpsTestConfig1}  30000

    createNewGroup  ${atipSession}  Group2
    ${g2id}  getGroupId  ${atipSession}  Group2

    moveNPsToGroup  ${atipSession}  Group2  ${npId1}
    moveNPsToGroup  ${atipSession}  Group2  ${npId2}

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${g2id}
    ${macConfig}  addMac  ${atipSession}  dst  MacMaskDst  0007E3BD9A52  ${g2id}  \\x00\\x10\\x94\\x00\\x00\\x02

    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterDst  ${macConfig}  ${g2id}

    Run Mac Mask Test and Validate  00:07:E3:BD:9A:52  40000  40000  MacMaskDst  00:10:94:00:00:02  20000  ${g2id}  ${bpsTestConfig2}  60000

TC001198144
    [Tags]  hardware
    # [VisionOne] Default Group and one new group with one NP with different MR settings (Config_MoveNP)

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${gDefaultId}

    ${macConfig}  addMac  ${atipSession}  both  MacMaskBoth  0002F425C139

    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterBoth  ${macConfig}  ${gDefaultId}

    Run Mac Mask Test and Validate  00:02:F4:25:C1:39  30000   60000  MacMaskBoth  00:00:01:00:00:01  0  ${gDefaultId}  ${bpsTestConfig1}  30000

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${g1id}
    ${macConfig2}  addMac  ${atipSession}  both  MacMaskBoth  0007E465C159  ${g1id}  \\x00\\x00\\x01\\x00\\x00\\x01
    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterBoth  ${macConfig2}  ${g1id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run Mac Mask Test and Validate  00:07:E4:65:C1:59  30000  30000  MacMaskBoth  00:00:01:00:00:01  0  ${g1id}  ${bpsTestConfig1}  30000

    createNewGroup  ${atipSession}  Group2
    ${g2id}  getGroupId  ${atipSession}  Group2

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${g2id}
    ${macConfig2}  addMac  ${atipSession}  dst  MacMaskDst  0007E3BD9A52  ${g2id}  \\x00\\x10\\x94\\x00\\x00\\x02
    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterDst  ${macConfig2}  ${g2id}

    moveNPsToGroup  ${atipSession}  Group2  ${npId0}

    Run Mac Mask Test and Validate  00:07:E3:BD:9A:52  20000  20000  MacMaskDst  00:10:94:00:00:02  10000  ${g2id}  ${bpsTestConfig1}  30000

TC001198145
    [Tags]  virtual
    # [vATIP] 2xNew Groups with multiple NPs per group with different Mac Rewriting Configuration (MoveNP_Config)

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${gDefaultId}

    ${macConfig}  addMac  ${atipSession}  dst  MacMaskDst  0007E963CE53

    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterDst  ${macConfig}  ${gDefaultId}

    Run Mac Mask Test and Validate  00:07:E9:63:CE:53  30000   30000  MacMaskDst  00:00:01:00:00:01  20000  ${gDefaultId}  ${bpsTestConfig1}  30000

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    updateMacRewriteSettings  ${atipSession}  enable  ${httpStatusOK}  ${g1id}

    ${macConfig}  addMac  ${atipSession}  both  MacMaskBoth  0007E962C453  ${g1id}

    configFilterMacRewrite  ${atipSession}  L3_VPN  macFilterBoth  ${macConfig}  ${g1id}

    Run Mac Mask Test and Validate  00:07:E9:62:C4:53  30000   60000  MacMaskBoth  00:00:01:00:00:01  0  ${g1Id}  ${bpsTestConfig1}  30000

*** Keywords ***

Run Mac Mask Test and Validate
    [Arguments]  ${niddle}  ${niddleMatches}  ${expectedStatsValue}  ${maskName}  ${sample}  ${sampleCount}  ${gId}  ${test}  ${packetNr}

    #Reset Stats
    resetStats  ${atipSession}

    #Connect to Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Write  sudo su
    Sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set Prompt  \#
    Read Until Prompt

    #Start traffic capture

    Write  tcpdump -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap -c ${packetNr} -B 30720 not multicast
    Read Until  ${envVars.linuxConnectionConfig.port}

    #Start sending traffic
    runBpsTest  ${bpsSession}  ${envVars}  ${test}

    #Stop capture on Linux Box
    Write Control Character     BRK
    Read Until Prompt

    #Filter capture file and export matching packets to a result file
    ${tsharkCommand}=  filterCapture  ${niddle}  ${TEST NAME}  hex
    Execute Command  ${tsharkCommand}

    #Validate result file content against expected result
    Matches Validation  ${TEST NAME}  ${niddleMatches}

    ${tsharkCommand}=  filterCapture  ${sample}  ${TEST NAME}  hex
    Execute Command  ${tsharkCommand}
    Matches Validation  ${TEST NAME}  ${sampleCount}

    Wait Until Keyword Succeeds  20s  5s  Validate Mac Mask  ${maskName}  ${expectedStatsValue}  ${gId}

    #delete capture and result file from Linux Box
    Execute Command  rm ${TEST NAME}*

Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [return]  ${output}

Matches Validation
    [Arguments]  ${STRING}  ${expectedValue}
    ${output}=  Execute Command  wc -l < ${STRING}.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Be Equal As Integers  ${statsCaptureLB}  ${expectedValue}

Validate Mac Mask
    [Arguments]  ${macMaskName}  ${expectedValue}  ${gId}
    ${rptCount}  getMatchCountByName  ${atipSession}  ${macMaskName}  ${gId}
    Should Be Equal As Integers  ${rptCount}  ${expectedValue}


Login ATIP
    login  ${atipSession}

Clear Config
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}