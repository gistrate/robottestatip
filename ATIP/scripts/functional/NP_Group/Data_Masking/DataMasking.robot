*** Settings ***
Documentation  Data masking test suite.

Variables  SuiteConfig.py

Library    scripts/functional/NP_Group/Data_Masking/SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    Collections
Library    atipAutoFrwk.webApi.atip.DataMasking.DataMasking
Library    atipAutoFrwk.webApi.atip.PayloadMasks.PayloadMasks
Library    atipAutoFrwk.services.atip.DataMaskingService
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    Telnet  timeout=30   prompt=$
Library    String
Library    BuiltIn
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.webApi.atip.Groups

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        atipLogin.login  ${atipSession}
Test Setup         Clear Config
Test Teardown      Close All Connections
Suite Teardown     logout  ${atipSession}

*** Test Cases ***

TC001179666
    [Tags]  hardware
    #Defalut Group and 1 New Group with different Data Masking Configuration (Config_MoveNP)

    configureDataMasking  ${atipSession}  ${True}  P  ${gDefaultId}
    ${headerMask}  configureHeaderMask  ${atipSession}  L3_VPN  l2mac  6  L2  6  ${gDefaultId}

    configFilterHeaderMask  ${atipSession}  l2mac  ${headerMask}  ${gDefaultId}

    Run Header Test and Validate  PPPPPP  30000   30000  l2mac  00:00:01:00:00:01  10000  ${gDefaultId}  ${bpsTestConfig1}  30000

    ${HeaderConfig}  getHeaderMaskByName  ${atipSession}  l2mac  ${gDefaultId}
    ${MatchNumber}=  set variable  ${HeaderConfig.get('masks')[0].get('rpt_count')}
    should be equal as integers  ${MatchNumber}  30000

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    configureDataMasking  ${atipSession}  ${True}  O  ${g1id}
    ${headerMask}  configureHeaderMask  ${atipSession}  L3_VPN  l3vpn  4  L2_VLAN_MPLS  8  ${g1id}
    configFilterHeaderMask  ${atipSession}  l3vpn  ${headerMask}  ${g1id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run Header Test and Validate  OOOO  30000   30000  l3vpn  00:01:00:40  0  ${g1id}  ${bpsTestConfig1}  30000

TC001179667
    [Tags]  hardware
    #Default Group and 1 New Group with different Data Masking Configuration (MoveNP_Config)

    configureDataMasking  ${atipSession}  ${True}  Q  ${gDefaultId}
    ${headerMask}  configureHeaderMask  ${atipSession}  L3_VPN  vlan  4  L2_VLAN  4  ${gDefaultId}
    configFilterHeaderMask  ${atipSession}  l2vlan  ${headerMask}  ${gDefaultId}

    Run Header Test and Validate  QQQQ  30000   30000  vlan  00:C8:88:47  0  ${gDefaultId}  ${bpsTestConfig1}  30000

    ${HeaderConfig}  getHeaderMaskByName  ${atipSession}  vlan  ${gDefaultId}
    ${MatchNumber}=  set variable  ${HeaderConfig.get('masks')[0].get('rpt_count')}
    should be equal as integers  ${MatchNumber}  30000

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}
    moveNPsToGroup  ${atipSession}  Group1  ${npId1}

    configureDataMasking  ${atipSession}  ${True}  R  ${g1id}

    ${headerMask}  configureHeaderMask  ${atipSession}  L3_VPN  sip  4  L3  12  ${g1id}

    configFilterHeaderMask  ${atipSession}  sourceIp  ${headerMask}  ${g1id}
    Run Header Test and Validate  RRRR  60000   60000  sip  01:01:01:16  2  ${g1id}  ${bpsTestConfig2}  60000

TC001179668
    [Tags]  hardware
    #2xNew Groups with multiple NPs per group with different Data Masking Configuration(Config_MoveNP)

    createNewGroup  ${atipSession}  Group1
    createNewGroup  ${atipSession}  Group2

    ${g1id}  getGroupId  ${atipSession}  Group1
    ${g2id}  getGroupId  ${atipSession}  Group2

    configureDataMasking  ${atipSession}  ${True}  W  ${g1id}

    ${headerMask}  configureHeaderMask  ${atipSession}  L3_VPN  dport  2  L4  2  ${g1id}
    configFilterHeaderMask  ${atipSession}  destPort  ${headerMask}  ${g1id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run Header Test and Validate  WW  30000   30000  dport  00:50:57:57  10000  ${g1id}  ${bpsTestConfig1}  30000

    ${HeaderConfig}  getHeaderMaskByName  ${atipSession}  dport  ${g1id}
    ${MatchNumber}=  set variable  ${HeaderConfig.get('masks')[0].get('rpt_count')}
    should be equal as integers  ${MatchNumber}  30000

    configureDataMasking  ${atipSession}  ${True}  C  ${g2id}


    ${payloadmask}  configurePayloadMask  ${atipSession}  3  \\([0-9]{3}\\)[0-9]{3}-[0-9]{4}  phone  5  false  ${g2id}

    configFilterPayloadMask  ${atipSession}  myfilter  ${payloadmask}  ${g2id}


    moveNPsToGroup  ${atipSession}  Group2  ${npId1}
    moveNPsToGroup  ${atipSession}  Group2  ${npId2}

    Run Payload Test and Validate  DM_gmail_phone2NPs  )CCCCC  14  28  phone  None  (994)946-0114  ${g2id}

TC001179669
    [Tags]  hardware
    #2xNew Groups with multiple NPs per group with different Data Masking Configuration (MoveNP_Config)

    createNewGroup  ${atipSession}  Group1

    ${g1id}  getGroupId  ${atipSession}  Group1

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    configureDataMasking  ${atipSession}  ${True}  X  ${g1id}

    ${payloadmask}  configurePayloadMask  ${atipSession}  0  ${None}  Discover  0  true  ${g1id}

    configFilterPayloadMask  ${atipSession}  discover  ${payloadmask}  ${g1id}

    Run Payload Test and Validate  DM_discover  XXXXXXXXXXXXXXX  160  160  Discover  ${None}  6011649900252105  ${g1id}

    createNewGroup  ${atipSession}  Group2

    ${g2id}  getGroupId  ${atipSession}  Group2

    moveNPsToGroup  ${atipSession}  Group2  ${npId1}
    moveNPsToGroup  ${atipSession}  Group2  ${npId2}

    configureDataMasking  ${atipSession}  ${True}  Q  ${g2id}

    ${payloadmask}  configurePayloadMask  ${atipSession}  0  ${None}  Visa  0  true  ${g2id}
    configFilterPayloadMask  ${atipSession}  visa  ${payloadmask}  ${g2id}

    Run Payload Test and Validate  DM_visa_amex_discover_mastercard2NPs  QQQQQQQQQQQQQQQQ  68  68  Visa  ${None}  4696257730186563  ${g2id}

TC001179670
    [Tags]  hardware
    #Default Group and one new group with one NP, both with max payload masks (Config_MoveNP)

    configureDataMasking  ${atipSession}  ${True}  Q  ${gDefaultId}

    Configure Max Payload Masks  ${atipSession}  3  False  ${gDefaultId}

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    configureDataMasking  ${atipSession}  ${True}  M  ${g1id}

    Configure Max Payload Masks  ${atipSession}  3  False  ${g1id}

TC001179671
    [Tags]  hardware
    #2xNew Groups with multiple NPs both with max header masks (MoveNP_Config)

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    createNewGroup  ${atipSession}  Group2
    ${g2id}  getGroupId  ${atipSession}  Group2

    createNewGroup  ${atipSession}  Group3
    ${g3id}  getGroupId  ${atipSession}  Group3

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    configureDataMasking  ${atipSession}  ${True}  M  ${g1id}
    Configure Header Masks  ${atipSession}  4  L3  ${maxHdrMasks}  ${g1id}

    moveNPsToGroup  ${atipSession}  Group2  ${npId1}

    configureDataMasking  ${atipSession}  ${True}  S  ${g2id}
    Configure Header Masks  ${atipSession}  4  L3  ${maxHdrMasks}  ${g2id}

    moveNPsToGroup  ${atipSession}  Group3  ${npId2}

    configureDataMasking  ${atipSession}  ${True}  T  ${g3id}
    Configure Header Masks  ${atipSession}  4  L3  ${maxHdrMasks}  ${g3id}

    moveNPsToGroup  ${atipSession}  Default  ${npId0}
    deleteGroup  ${atipSession}  ${g1id}

    createNewGroup  ${atipSession}  Group4
    ${g4id}  getGroupId  ${atipSession}  Group4
    moveNPsToGroup  ${atipSession}  Group4  ${npId0}

    configureDataMasking  ${atipSession}  ${True}  M  ${g4id}
    Configure Header Masks  ${atipSession}  4  L3  ${maxHdrMasks}  ${g4id}

TC001179672
    [Tags]  hardware
    #One group other than default with one NP and maximum number of regex rules per filter

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1
    moveNPsToGroup  ${atipSession}  Group1  ${npId0}
    configureDataMasking  ${atipSession}  ${True}  M  ${g1id}

    Configure Header Masks  ${atipSession}  4  L3  49  ${g1id}

    ${hdrMaskConfig}  getHeaderMasks  ${atipSession}  ${g1id}
    configFilterHeaderMask  ${atipSession}  myfilter  ${hdrMaskConfig.headerMaskList}  ${g1id}

    createNewGroup  ${atipSession}  Group2
    ${g2id}  getGroupId  ${atipSession}  Group2
    moveNPsToGroup  ${atipSession}  Group2  ${npId1}
    configureDataMasking  ${atipSession}  ${True}  M  ${g2id}

    Configure Header Masks  ${atipSession}  4  L3  50  ${g2id}
    ${hdrMaskConfig}  getHeaderMasks  ${atipSession}  ${g2id}
    ${testStatus}=  Run Keyword And Return Status  configFilterHeaderMask  ${atipSession}  myfilter  ${hdrMaskConfig.headerMaskList}  ${g2id}
    Should Be Equal As Strings  ${testStatus}  ${False}

TC001179673
    [Tags]  hardware
    #Default Group and one new group with one NP with different DM settings (Config_MoveNP)

    configureDataMasking  ${atipSession}  ${True}  P  ${gDefaultId}

    ${headerMask}  configureHeaderMask  ${atipSession}  L3_VPN  l2mac  6  L2  6  ${gDefaultId}

    configFilterHeaderMask  ${atipSession}  l2mac  ${headerMask}  ${gDefaultId}

    Run Header Test and Validate  PPPPPP  30000   30000  l2mac  00:00:01:00:00:01  10000  ${gDefaultId}  ${bpsTestConfig1}  30000

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    configureDataMasking  ${atipSession}  ${True}  O  ${g1id}
    ${headerMask}  configureHeaderMask  ${atipSession}  L3_VPN  l3vpn  4  L2_VLAN_MPLS  8  ${g1id}
    configFilterHeaderMask  ${atipSession}  l3vpn  ${headerMask}  ${g1id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run Header Test and Validate  OOOO  30000   30000  l3vpn  00:01:00:40  0  ${g1id}  ${bpsTestConfig1}  30000

TC001179674
    [Tags]  virtual
    #Default Group and 1 New Group with different Data Masking Configuration (MoveNP_Config)

    configureDataMasking  ${atipSession}  ${True}  Q  ${gDefaultId}
    ${headerMask}  configureHeaderMask  ${atipSession}  L3_VPN  vlan  4  L2_VLAN  4  ${gDefaultId}
    configFilterHeaderMask  ${atipSession}  l2vlan  ${headerMask}  ${gDefaultId}

    Run Header Test and Validate  QQQQ  30000   30000  vlan  00:C8:88:47  0  ${gDefaultId}  ${bpsTestConfig1}  30000

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    configureDataMasking  ${atipSession}  ${True}  R  ${g1id}

    ${headerMask}  configureHeaderMask  ${atipSession}  L3_VPN  sip  4  L3  12  ${g1id}

    configFilterHeaderMask  ${atipSession}  sourceIp  ${headerMask}  ${g1id}
    Run Header Test and Validate  RRRR  30000   30000  sip  01:01:01:16  1  ${g1id}  ${bpsTestConfig1}  30000

*** Keywords ***

Run Header Test and Validate
    [Arguments]  ${niddle}  ${niddleMatches}  ${expectedStatsValue}  ${maskName}  ${sample}  ${sampleCount}  ${gId}  ${test}  ${packetNr}

    #Reset Stats
    resetStats  ${atipSession}

    #Connect to Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Write  sudo su
    Sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set Prompt  \#
    Read Until Prompt

    #Start traffic capture

    Write  tcpdump -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap -c ${packetNr} -B 30720 not multicast
    Read Until  ${envVars.linuxConnectionConfig.port}

    #Start sending traffic
    runBpsTest  ${bpsSession}  ${envVars}  ${test}

    #Stop capture on Linux Box
    Write Control Character     BRK
    Read Until Prompt

    #Filter capture file and export matching packets to a result file
    ${tsharkCommand}=  filterCapture  ${niddle}  ${TEST NAME}
    Execute Command  ${tsharkCommand}

    #Validate result file content against expected result
    Matches Validation  ${TEST NAME}  ${niddleMatches}

    ${tsharkCommand}=  filterCapture  ${sample}  ${TEST NAME}  hex
    Execute Command  ${tsharkCommand}
    Matches Validation  ${TEST NAME}  ${sampleCount}

    Wait Until Keyword Succeeds  20s  5s  Validate Header Mask  ${maskName}  ${expectedStatsValue}  ${gId}

    #delete capture and result file from Linux Box
    Execute Command  rm ${TEST NAME}*


Run Payload Test and Validate
    [Arguments]  ${bpsScript}  ${niddle}  ${niddleMatches}  ${expectedStatsValue}  ${maskName}  ${appName}  ${sample}  ${gId}


    #Reset Stats
    resetStats  ${atipSession}

    #Connect to Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Write  sudo su
    sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt

    #Start traffic capture

    Write  tcpdump -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap
    Read Until  ${envVars.linuxConnectionConfig.port}

    #Start sending traffic
    import library  ../../frwk/src/atipAutoFrwk/config/traffic/BpsTestConfig.py  ${bpsScript}  100  WITH NAME  bpsTestConfigInit
	${myClassInstance}=  get library instance  bpsTestConfigInit
	Set Test Variable  ${bpsTestConfigInit}  ${myClassInstance}
	bpsTestConfigInit.reinit  ${bpsScript}  100
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigInit}
    Sleep  13s

    #Stop capture on Linux Box
    Write Control Character     BRK
    Read Until Prompt

    #Filter capture file and export matching packets to a result file
    ${tsharkCommand}=  filterCapture  ${niddle}  ${TEST NAME}
    Execute Command  ${tsharkCommand}

    #Validate result file content against expected result
    Matches Validation  ${TEST NAME}  ${niddleMatches}

    ${tsharkCommand}=  filterCapture  ${sample}  ${TEST NAME}
    Execute Command  ${tsharkCommand}
    Matches Validation  ${TEST NAME}  0

    Wait Until Keyword Succeeds  15s  3s  Validate Payload Mask  ${maskName}  ${expectedStatsValue}  ${gId}

    #delete capture and result file from Linux Box
    Execute Command  rm ${TEST NAME}*

Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [return]  ${output}

Matches Validation
    [Arguments]  ${STRING}  ${expectedValue}
    ${output}=  Execute Command  wc -l < ${STRING}.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Be Equal As Integers  ${statsCaptureLB}  ${expectedValue}

Validate Header Mask
    [Arguments]  ${maskName}  ${expectedValue}  ${gId}
    ${hmStats}=  getHeaderMaskByName  ${atipSession}  ${maskName}  ${gId}
    ${item}=  Get From Dictionary  ${hmStats}  masks
    ${el}=  Get From List  ${item}  0
    ${rptCount}=  Get From Dictionary  ${el}  rpt_count
    Should Be Equal As Integers  ${rptCount}  ${expectedValue}

Validate Payload Mask
    [Arguments]  ${maskName}  ${expectedValue}  ${gId}
    ${pmStats}=  getPayloadConfigByName  ${atipSession}  ${maskName}  ${gId}
    ${item}=  Get From Dictionary  ${pmStats}  masks
    ${el}=  Get From List  ${item}  0
    ${rptCount}=  Get From Dictionary  ${el}  rpt_count
    Should Be Equal As Integers  ${rptCount}  ${expectedValue}

Configure Header Masks

    [Arguments]  ${atipSession}  ${length}  ${header}  ${HdrMasksNr}  ${gId}

    #create maximum number of header masks
    :FOR  ${i}  IN RANGE  ${HdrMasksNr}
    \    createHeaderMask  ${atipSession}  hdrMask${i}  ${length}  ${header}  ${i}  ${gId}

    #negative testing: create one more header mask; this should fail
    ${testStatus}  Run Keyword And Return Status  createHeaderMask  ${atipSession}  hdrMask${HdrMasksNr}  ${length}  ${header}  ${HdrMasksNr}  ${gId}
    Run keyword if  '${HdrMasksNr}'=='${MaxHdrMasks}'
    ...  Should Be Equal As Strings  ${testStatus}  ${False}

Configure Max Payload Masks
    [Arguments]  ${atipSession}  ${offsetEnd}  ${readOnly}  ${gId}

    ${maxCustomRgxMasks}=  Set Variable  ${maxRgxMasks} - ${predefRgxMasks}

    #create maximum number of payload masks
    :FOR  ${i}  IN RANGE  ${maxCustomRgxMasks}
    \    createCustomPayloadMask  ${atipSession}  rgxMask${i}  p${i}  ${i}  ${offsetEnd}  ${readOnly}  ${gId}

    #negative testing: create one more payload mask; this should fail
    ${testStatus}=  Run Keyword And Return Status  createCustomPayloadMask  ${atipSession}  rgxMask${maxRgxMasks}  p${maxRgxMasks}  ${maxRgxMasks}  ${offsetEnd}  ${readOnly}  ${gId}
    Should Be Equal As Strings  ${testStatus}  ${False}

Login ATIP
    login  ${atipSession}

Clear Config
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}

