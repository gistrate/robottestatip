from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.webApi.atip.PayloadMasks import PayloadMasks
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.data.atip.AtipType import AtipType

class SuiteConfig(object):

    bpsTestConfig1 = BpsTestConfig('DM_L3VPN', 15)
    bpsTestConfig2 = BpsTestConfig('DM_L3VPN_t2', 15)

    maxHdrMasks = 500
    maxRgxMasks = 512
    predefRgxMasks = 8
    envVars = Environment()
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    httpStatusOK = HTTPStatus.OK.value
    hardware = AtipType.HARDWARE

    npId0 = str(envVars.atipSlotNumber[0])
    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[1])
        npId2 = str(envVars.atipSlotNumber[2])

    gDefaultId = 0


    def configFilterHeaderMask(self, webApiSession, filterName, headerMask, groupId=0, vlanId=100):

        atipConfig = AtipConfig()

        filterConfig = FilterConfig(filterName, id=-1,groupId=groupId)
        filterConfig.forward = True
        if type(headerMask) == dict:
            for hdrMask in headerMask.values():
                atipConfig.addHeaderMask(hdrMask)
                filterConfig.addHeaderMaskUUIDS(hdrMask.name)
        else:
            atipConfig.addHeaderMask(headerMask)
            filterConfig.addHeaderMaskUUIDS(headerMask.name)

        if self.envVars.atipType == AtipType.HARDWARE:
            filterConfig.addTransmitIds(vlanId)

        atipConfig.addFilters(filterConfig)
        AtipConfigService.updateFiltersConfig(webApiSession,atipConfig)
        AtipConfigService.createFilters(webApiSession,atipConfig, self.envVars)
        Filters.rulePush(webApiSession, groupId)

    def configFilterPayloadMask(self, webApiSession, filterName, payloadMask, groupId=0, vlanId=100):

        atipConfig = AtipConfig()
        atipConfig.addPayloadMask(payloadMask)

        filterConfig = FilterConfig(filterName, id=-1, groupId=groupId)
        filterConfig.addRegexMaskUUIDS(payloadMask.name)
        if self.envVars.atipType == AtipType.HARDWARE:
            filterConfig.addTransmitIds(vlanId)

        filterID = PayloadMasks.getPayloadConfigByName(webApiSession, payloadMask.name, groupId=groupId)
        filterConfig.addRegexMaskUUIDS(filterID['masks'][0]['id'])
        filterConfig.forward = True
        atipConfig.addFilters(filterConfig)

        AtipConfigService.updateFiltersConfig(webApiSession, atipConfig)
        AtipConfigService.createFilters(webApiSession, atipConfig, self.envVars)
        Filters.rulePush(webApiSession, groupId)