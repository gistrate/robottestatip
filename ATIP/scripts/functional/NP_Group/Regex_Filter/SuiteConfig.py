import ipaddress
from http import HTTPStatus

from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.Card import Card
from atipAutoFrwk.config.atip.Collector import Collector
from atipAutoFrwk.config.atip.NetflowCardConfig import NetflowCardConfig
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.AppSimConfig import AppSimConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.BrowserType import BrowserType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.atip.OSType import OSType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Groups import Groups
from atipAutoFrwk.data.atip.NetflowStats import NetflowStats
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.data.atip.DashboardType import DashboardType


class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    timeInterval = TimeInterval.HOUR
    typeDevice = StatsType.DEVICE
    deviceLinux = OSType.Linux.osName
    deviceMac = OSType.MacOS.osName
    deviceNokia = OSType.Nokia.osName
    totalSessions = TopStats.TOTAL_COUNT
    minimizedDashboard = DashboardType.MINIMIZED.statsLimit
    minDashboard = DashboardType.MINIMIZED
    expectedStatus = HTTPStatus.OK
    expAppFacebook = 'facebook'
    expAppEspn = 'espn'
    expAppAol = 'aol'
    expAppAmazon = 'amazon'
    expAppNetflix = 'netflix'

    gName1 = 'Group1'
    gName2 = 'Group2'
    gName3 = 'Group3'
    defaultG = 'Default'
    expRules2 = '2'
    expRules3 = '3'

    bpsTestConfig1 = BpsTestConfig('NP_Groups_Regex_FB_ESPN_AOL_i1i2_i3i14', 310)
    bpsTestConfig2 = BpsTestConfig('NP_Groups_Regex_FB_ESPN_AOL_i3i4_i5i16', 310)
    bpsTestConfig3 = BpsTestConfig('NP_Groups_Regex_FB_ESPN_AOL_Amazon_Netflix_i1i2_i3i14', 310)
    bpsTestConfig4 = BpsTestConfig('NP_Groups_Regex_FB_ESPN_AOL_Amazon_Netflix_i3i4_i5i16', 310)
    bpsTestConfig5 = BpsTestConfig('NP_Groups_Regex_FB_ESPN_AOL_none_Netflix_i3i4_i5i16', 310)
    bpsTestConfig6 = BpsTestConfig('NP_Groups_Regex_FB_ESPN_AOL_Amazon_none_i1i2_i5i16', 310)
    bpsTestConfig7 = BpsTestConfig('NP_Groups_Regex_FB_ESPN_AOL_none_Netflix_i1i2_i5i16', 310)
    bpsTestConfig8 = BpsTestConfig('NP_Groups_Regex_FB_ESPN_AOL_i1i2', 310)
    bpsTestConfig9 = BpsTestConfig('NP_Groups_Regex_FB_ESPN_AOL_none_Netflix_i1i2', 310)
    bpsTestConfig10 = BpsTestConfig('NP_Groups_Regex_FB_ESPN_AOL_Amazon_none_i1i2', 310)

    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[0])
        npId2 = str(envVars.atipSlotNumber[1])
        npId3 = str(envVars.atipSlotNumber[2])
        npIndexId1 = int(0)
        npIndexId2 = int(1)
        npIndexId3 = int(2)
    else:
        npId1 = str(envVars.atipSlotNumber[0])
        npIndexId1 = int(0)

    regexFacebook = 'facebook.*'
    filterRegexFacebook = FilterConfig("filterRegexFacebook")
    FilterConditionConfig.addCondition(filterRegexFacebook, FilterConditionType.RGX, regexFacebook)

    regexAol = 'aol.*'
    filterRegexAol = FilterConfig("filterRegexAol")
    FilterConditionConfig.addCondition(filterRegexAol, FilterConditionType.RGX, regexAol)

    regexAmazon = 'amazon.*'
    regexNetFlix = 'netflix.*'
    filterRegexAmazonNetflix = FilterConfig("filterRegexAmazonNetflix")
    FilterConditionConfig.addCondition(filterRegexAmazonNetflix, FilterConditionType.RGX, [regexAmazon, regexNetFlix])

    regexEspn = 'espn.*'
    filterRegexFacebookEspn = FilterConfig("filterRegexFacebookEspn")
    FilterConditionConfig.addCondition(filterRegexFacebookEspn, FilterConditionType.RGX, [regexFacebook, regexEspn])

    filterRegexEspn = FilterConfig("filterRegexEspn")
    FilterConditionConfig.addCondition(filterRegexEspn, FilterConditionType.RGX, regexEspn)

    filterRegexEspnDuplicate = FilterConfig("filterRegexEspnDuplicate")
    FilterConditionConfig.addCondition(filterRegexEspnDuplicate, FilterConditionType.RGX, regexEspn)

    filterRegexAmazon = FilterConfig("filterRegexAmazon")
    FilterConditionConfig.addCondition(filterRegexAmazon, FilterConditionType.RGX, regexAmazon)

    appsim1 = AppSimConfig('appsim_1', ApplicationType.FACEBOOK, OSType.Linux, BrowserType.BonEcho, GeoLocation.INDIA,
                           GeoLocation.INDIA, PacketSizes(70, 362, 450, 66))
    appsim2 = AppSimConfig('appsim_2', ApplicationType.ESPN, OSType.MacOS, BrowserType.Chrome,
                           GeoLocation.INDIA, GeoLocation.INDIA, PacketSizes(70, 643, 450, 66))
    appsim3 = AppSimConfig('appsim_3', ApplicationType.AOL, OSType.Linux, BrowserType.SeaMonkey,
                           GeoLocation.THAILAND, GeoLocation.THAILAND, PacketSizes(70, 517, 450, 66))
    appsim4 = AppSimConfig('appsim_4', ApplicationType.AMAZON, OSType.Nokia, BrowserType.BrowserNG,
                           GeoLocation.CHINA, GeoLocation.CHINA, PacketSizes(70, 517, 450, 66))
    appsim5 = AppSimConfig('appsim_5', ApplicationType.NETFLIX, OSType.MacOS, BrowserType.Chrome,
                          GeoLocation.INDIA, GeoLocation.INDIA, PacketSizes(70, 517, 450, 66))

    bpsTestConfig1.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfig2.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfig3.addAppSim([appsim1, appsim2, appsim3, appsim4, appsim5])
    bpsTestConfig4.addAppSim([appsim1, appsim2, appsim3, appsim4, appsim5])
    bpsTestConfig5.addAppSim([appsim1, appsim2, appsim3, appsim4, appsim5])
    bpsTestConfig6.addAppSim([appsim1, appsim2, appsim3, appsim4, appsim5])
    bpsTestConfig7.addAppSim([appsim1, appsim2, appsim3, appsim4, appsim5])
    bpsTestConfig8.addAppSim([appsim1, appsim2, appsim3])
    bpsTestConfig9.addAppSim([appsim1, appsim2, appsim3, appsim4, appsim5])
    bpsTestConfig10.addAppSim([appsim1, appsim2, appsim3, appsim4, appsim5])

    strFacebook = ApplicationType.FACEBOOK.appName
    strAOL = ApplicationType.AOL.appName
    strAmazon = ApplicationType.AMAZON.appName
    strNetflix = ApplicationType.NETFLIX.appName
    strEspn = ApplicationType.ESPN.appName
    bpsSessions = BpsStatsType.SESSIONS


