*** Settings ***
Documentation  Test groups functionality

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    Collections
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    BuiltIn
Library    String
Library    atipAutoFrwk.services.TestCondition
Library    atipAutoFrwk.webApi.atip.EngineInfo
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.webApi.atip.stats.Filters  WITH NAME  StatsFilters
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.services.atip.stats.DetailedFiltersService
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.webApi.atip.ConfigureStats

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP And Login
Suite Teardown     logout  ${atipSession}


*** Test Cases ***

TC001198421 [NP_Groups_7300][Regex]Defalut Group and 1 New Group with different Regex Filter Configuration (Config_MoveNP)
    [Tags]  hardware
    [Setup]  Clear Config

    log to console  TC001198421 Configuring regex Filter on Default Group
    Create Filters  ${atipSession}  ${filterRegexFacebook}  ${defaultG}

    log to console  TC001198421 Verifying per NP rules on Default Group
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId1}
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId2}
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId3}

    log to console  TC001198421 Running BPS traffic for Default Group
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}

    log to console  TC001198421 Verifying Filter existis in Top Filters Dashboard Window on Default Group
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexFacebook}  ${timeInterval}

    log to console  TC001198421 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on Default Group
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig1}
    ${bpsSessionsFacebook}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    Log  ${bpsSessionsFacebook}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexFacebook}  ${bpsSessionsFacebook}  ${timeInterval}

    log to console  TC001198421 Read all stats from Filter Detailed Stats - Top Devices on Default Group
    ${facebookDeviceStats}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexFacebook.id}
    checkAtipStatsValues  ${facebookDeviceStats}  ${deviceLinux}  ${totalSessions}  ${bpsSessionsFacebook}

    log to console  TC001198421 Read all stats from Filter Detailed Stats - App in Filtered Traffic on Default Group
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexFacebook}  ${deviceLinux}  ${expAppFacebook}  ${timeInterval}


    log to console  TC001198421 Configuring aol Regex Filter New Group1
    createNewGroup  ${atipSession}  ${gName1}
    Create Filters  ${atipSession}  ${filterRegexAol}  ${gName1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId2}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId3}

    log to console  TC001198421 Verifying per NP rules on New Group1
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId2}
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId3}

    log to console  TC001198421 Running BPS traffic for New Group1
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig2}

    log to console  TC001198421 Verifying Filter existis in Top Filters Dashboard Window on New Group1
    ${group1Id}=  getGroupId  ${atipSession}  ${gName1}
    Log  ${group1Id}
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexAol}  ${timeInterval}

    log to console  TC001198421 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on New Group1
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig2}
    ${bpsSessionsAol}  getStatItem  ${bpsComponentsStats['appsim_3']}  ${bpsSessions}
    Log  ${bpsSessionsAol}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexAol}  ${bpsSessionsAol}  ${timeInterval}  ${group1Id}

    log to console  TC001198421 Read all stats from Filter Detailed Stats - Top Devices on on New Group1
    ${aolDeviceStats}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexAol.id}  ${expectedStatus}  ${group1Id}
    checkAtipStatsValues  ${aolDeviceStats}  ${deviceLinux}  ${totalSessions}  ${bpsSessionsAol}

    log to console  TC001198421 Read all stats from Filter Detailed Stats - App in Filtered Traffic on New Group1 (group id will be read from given filter object)
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexAol}  ${deviceLinux}  ${expAppAol}  ${timeInterval}


TC001198422 [NP_Groups_7300][Regex]Default Group and 1 New Group with different Regex Filter Configuration (MoveNP_Config)
    [Tags]  hardware
    [Setup]  Clear Config

    log to console  TC001198422 Configuring regex Filter on Default Group
    Create Filters  ${atipSession}  ${filterRegexAmazonNetflix}  ${defaultG}

    log to console  TC001198422 Verifying per NP rules on Default Group
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId1}
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId2}
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId3}

    log to console  TC001198422 Running BPS traffic for Default Group
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig3}

    log to console  TC001198422 Verifying Filter existis in Top Filters Dashboard Window on Default Group
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexAmazonNetflix}  ${timeInterval}

    log to console  TC001198422 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on Default Group
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig3}
    ${bpsSessionsAmazon}  getStatItem  ${bpsComponentsStats['appsim_4']}  ${bpsSessions}
    Log  ${bpsSessionsAmazon}
    ${bpsSessionsNetflix}  getStatItem  ${bpsComponentsStats['appsim_5']}  ${bpsSessions}
    Log  ${bpsSessionsNetflix}
    ${bpsSessionsAmazonNetflix}=  evaluate  ${bpsSessionsAmazon}+${bpsSessionsNetflix}
    Log  ${bpsSessionsAmazonNetflix}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexAmazonNetflix}  ${bpsSessionsAmazonNetflix}  ${timeInterval}

    log to console  TC001198422 Read all stats from Filter Detailed Stats - Top Devices on Default Group
    ${amazonNetflixDeviceStats}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexAmazonNetflix.id}
    checkAtipStatsValues  ${amazonNetflixDeviceStats}  ${deviceNokia}  ${totalSessions}  ${bpsSessionsAmazon}
    checkAtipStatsValues  ${amazonNetflixDeviceStats}  ${deviceMac}  ${totalSessions}  ${bpsSessionsNetflix}

    log to console  TC001198422 Read all stats from Filter Detailed Stats - App in Filtered Traffic on Default Group
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexAmazonNetflix}  ${deviceNokia}  ${expAppAmazon}  ${timeInterval}
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexAmazonNetflix}  ${deviceMac}  ${expAppNetflix}  ${timeInterval}


    log to console  TC001198422 Configuring Regex Filter New Group1
    createNewGroup  ${atipSession}  ${gName1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId2}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId3}
    Create Filters  ${atipSession}  ${filterRegexFacebookEspn}  ${gName1}

    log to console  TC001198422 Verifying per NP rules on New Group1
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId2}
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId3}

    log to console  TC001198422 Running BPS traffic for New Group1
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig4}

    log to console  TC001198422 Verifying Filter existis in Top Filters Dashboard Window on New Group1
    ${group1Id}=  getGroupId  ${atipSession}  ${gName1}
    Log  ${group1Id}
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexFacebookEspn}  ${timeInterval}

    log to console  TC001198422 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on New Group1
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig4}
    ${bpsSessionsFacebook}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    Log  ${bpsSessionsFacebook}
    ${bpsSessionsEspn}  getStatItem  ${bpsComponentsStats['appsim_2']}  ${bpsSessions}
    Log  ${bpsSessionsEspn}
    ${bpsSessionsFacebookEspn}=  evaluate  ${bpsSessionsFacebook}+${bpsSessionsEspn}
    Log  ${bpsSessionsFacebookEspn}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexFacebookEspn}  ${bpsSessionsFacebookEspn}  ${timeInterval}  ${group1Id}

    log to console  TC001198422 Read all stats from Filter Detailed Stats - Top Devices on on New Group1
    ${facebookEspnDeviceStats}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexFacebookEspn.id}  ${expectedStatus}  ${group1Id}
    checkAtipStatsValues  ${facebookEspnDeviceStats}  ${deviceLinux}  ${totalSessions}  ${bpsSessionsFacebook}
    checkAtipStatsValues  ${facebookEspnDeviceStats}  ${deviceMac}  ${totalSessions}  ${bpsSessionsEspn}

    log to console  TC001198422 Read all stats from Filter Detailed Stats - App in Filtered Traffic on New Group1 (group id will be read from given filter object)
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexFacebookEspn}  ${deviceLinux}  ${expAppFacebook}  ${timeInterval}
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexFacebookEspn}  ${deviceMac}  ${expAppEspn}  ${timeInterval}


TC001198423 [NP_Groups_7300][Regex]2xNew Groups with multiple NPs per group with different Regex Filter Configuration(Config_MoveNP)
    [Tags]  hardware
    [Setup]  Clear Config

    log to console  TC001198423 Configuring Regex Filter New Group1
    createNewGroup  ${atipSession}  ${gName1}
    Create Filters  ${atipSession}  ${filterRegexEspn}  ${gName1}
    Create Filters  ${atipSession}  ${filterRegexAmazon}  ${gName1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId2}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId3}

    log to console  TC001198423 Verifying per NP rules on New Group1
    Validate Rules Number Per NP  ${atipSession}  ${expRules3}  ${npIndexId2}
    Validate Rules Number Per NP  ${atipSession}  ${expRules3}  ${npIndexId3}

    log to console  TC001198423 Running BPS traffic for New Group1
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig5}

    log to console  TC001198423 Verifying Filter existis in Top Filters Dashboard Window on New Group1
    ${group1Id}=  getGroupId  ${atipSession}  ${gName1}
    Log  ${group1Id}
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexEspn}  ${timeInterval}

    log to console  TC001198423 Verifying Filter does not existis in Top Filters Dashboard Window on on New Group1
    checkFilterDoesNotExistsInTopStats  ${atipSession}  ${filterRegexAmazon}  ${timeInterval}

    log to console  TC001198423 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on New Group1
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig5}
    ${bpsSessionsEspn}  getStatItem  ${bpsComponentsStats['appsim_2']}  ${bpsSessions}
    Log  ${bpsSessionsEspn}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexEspn}  ${bpsSessionsEspn}  ${timeInterval}  ${group1Id}

    log to console  TC001198423 Read all stats from Filter Detailed Stats - Top Devices on on New Group1
    ${espnDeviceStats}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexEspn.id}  ${expectedStatus}  ${group1Id}
    checkAtipStatsValues  ${espnDeviceStats}  ${deviceMac}  ${totalSessions}  ${bpsSessionsEspn}

    log to console  TC001198423 Read all stats from Filter Detailed Stats - App in Filtered Traffic on New Group1 (group id will be read from given filter object)
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexEspn}  ${deviceMac}  ${expAppEspn}  ${timeInterval}

    log to console  TC001198423 Configuring Regex Filter New Group2
    createNewGroup  ${atipSession}  ${gName2}
    Create Filters  ${atipSession}  ${filterRegexFacebook}  ${gName2}
    moveNPsToGroup  ${atipSession}  ${gName2}  ${npId1}
    moveNPsToGroup  ${atipSession}  ${gName2}  ${npId3}

    log to console  TC001198423 Verifying per NP rules on New Group2
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId1}
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId3}

    log to console  TC001198423 Running BPS traffic for New Group2
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig6}

    log to console  TC001198423 Verifying Filter existis in Top Filters Dashboard Window on New Group2
    ${group2Id}=  getGroupId  ${atipSession}  ${gName2}
    Log  ${group2Id}
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexFacebook}  ${timeInterval}

    log to console  TC001198423 Verifying Filter does not existis in Top Filters Dashboard Window on on New Group1 (filters are not mismatched between Groups)
    checkFilterDoesNotExistsInTopStats  ${atipSession}  ${filterRegexAmazon}  ${timeInterval}

    log to console  TC001198423 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on New Group2
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig6}
    ${bpsSessionsFacebook}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    Log  ${bpsSessionsFacebook}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexFacebook}  ${bpsSessionsFacebook}  ${timeInterval}  ${group2Id}

    log to console  TC001198423 Read all stats from Filter Detailed Stats - Top Devices on on New Group2
    ${facebookDeviceStats}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexFacebook.id}  ${expectedStatus}  ${group2Id}
    checkAtipStatsValues  ${facebookDeviceStats}  ${deviceLinux}  ${totalSessions}  ${bpsSessionsFacebook}

    log to console  TC001198423 Read all stats from Filter Detailed Stats - App in Filtered Traffic on New Group2 (group id will be read from given filter object)
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexFacebook}  ${deviceLinux}  ${expAppFacebook}  ${timeInterval}


TC001198427 [NP_Groups_7300][Regex]2xNew Groups with multiple NPs per group with same Regex Filter Configuration (MoveNP_Config)
    [Tags]  hardware
    [Setup]  Clear Config

    log to console  TC001198427 Configuring Regex Filter New Group1
    createNewGroup  ${atipSession}  ${gName1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId2}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId3}
    Create Filters  ${atipSession}  ${filterRegexEspn}  ${gName1}

    log to console  TC001198427 Verifying per NP rules on New Group1
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId2}
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId3}

    log to console  TC001198427 Running BPS traffic for New Group1
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig5}

    log to console  TC001198427 Verifying Filter existis in Top Filters Dashboard Window on New Group1
    ${group1Id}=  getGroupId  ${atipSession}  ${gName1}
    Log  ${group1Id}
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexEspn}  ${timeInterval}

    log to console  TC001198427 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on New Group1
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig5}
    ${bpsSessionsEspn}  getStatItem  ${bpsComponentsStats['appsim_2']}  ${bpsSessions}
    Log  ${bpsSessionsEspn}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexEspn}  ${bpsSessionsEspn}  ${timeInterval}  ${group1Id}

    log to console  TC001198427 Read all stats from Filter Detailed Stats - Top Devices on on New Group1
    ${espnDeviceStats}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexEspn.id}  ${expectedStatus}  ${group1Id}
    checkAtipStatsValues  ${espnDeviceStats}  ${deviceMac}  ${totalSessions}  ${bpsSessionsEspn}

    log to console  TC001198427 Read all stats from Filter Detailed Stats - App in Filtered Traffic on New Group1 (group id will be read from given filter object)
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexEspn}  ${deviceMac}  ${expAppEspn}  ${timeInterval}

    log to console  TC001198427 Configuring Regex Filter New Group2
    createNewGroup  ${atipSession}  ${gName2}
    moveNPsToGroup  ${atipSession}  ${gName2}  ${npId1}
    moveNPsToGroup  ${atipSession}  ${gName2}  ${npId3}
    Create Filters  ${atipSession}  ${filterRegexEspnDuplicate}  ${gName2}

    log to console  TC001198427 Verifying per NP rules on New Group2
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId1}
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId3}

    log to console  TC001198427 Running BPS traffic for New Group2
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig7}

    log to console  TC001198427 Verifying Filter existis in Top Filters Dashboard Window on New Group2
    ${group2Id}=  getGroupId  ${atipSession}  ${gName2}
    Log  ${group2Id}
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexEspnDuplicate}  ${timeInterval}

    log to console  TC001198427 Verifying Filter does not existis in Top Filters Dashboard Window on on New Group1 (filters are not mismatched between Groups)
    checkFilterDoesNotExistsInTopStats  ${atipSession}  ${filterRegexEspn}  ${timeInterval}

    log to console  TC001198427 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on New Group2
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig7}
    ${bpsSessionsEspn2}  getStatItem  ${bpsComponentsStats['appsim_2']}  ${bpsSessions}
    Log  ${bpsSessionsEspn2}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexEspnDuplicate}  ${bpsSessionsEspn2}  ${timeInterval}  ${group2Id}

    log to console  TC001198427 Read all stats from Filter Detailed Stats - Top Devices on on New Group2
    ${espnDeviceStats2}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexEspnDuplicate.id}  ${expectedStatus}  ${group2Id}
    checkAtipStatsValues  ${espnDeviceStats2}  ${deviceMac}  ${totalSessions}  ${bpsSessionsEspn2}

    log to console  TC001198427 Read all stats from Filter Detailed Stats - App in Filtered Traffic on New Group2 (group id will be read from given filter object)
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexEspnDuplicate}  ${deviceMac}  ${expAppEspn}  ${timeInterval}


TC001198426 [NP_Groups_V1][Regex]Defalut Group and 1 New Group with different Regex Filter Configuration (Config_MoveNP)
    [Tags]  hardware
    [Setup]  Clear Config

    log to console  TC001198426 Configuring regex Filter on Default Group
    Create Filters  ${atipSession}  ${filterRegexFacebook}  ${defaultG}

    log to console  TC001198426 Verifying per NP rules on Default Group
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId1}

    log to console  TC001198426 Running BPS traffic for Default Group
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig8}

    log to console  TC001198426 Verifying Filter existis in Top Filters Dashboard Window on Default Group
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexFacebook}  ${timeInterval}

    log to console  TC001198426 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on Default Group
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig8}
    ${bpsSessionsFacebook}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    Log  ${bpsSessionsFacebook}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexFacebook}  ${bpsSessionsFacebook}  ${timeInterval}

    log to console  TC001198426 Read all stats from Filter Detailed Stats - Top Devices on Default Group
    ${facebookDeviceStats}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexFacebook.id}
    checkAtipStatsValues  ${facebookDeviceStats}  ${deviceLinux}  ${totalSessions}  ${bpsSessionsFacebook}

    log to console  TC001198426 Read all stats from Filter Detailed Stats - App in Filtered Traffic on Default Group
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexFacebook}  ${deviceLinux}  ${expAppFacebook}  ${timeInterval}


    log to console  TC001198426 Configuring aol Regex Filter New Group1
    createNewGroup  ${atipSession}  ${gName1}
    Create Filters  ${atipSession}  ${filterRegexAol}  ${gName1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId1}


    log to console  TC001198426 Verifying per NP rules on New Group1
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId1}

    log to console  TC001198426 Running BPS traffic for New Group1
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig8}

    log to console  TC001198426 Verifying Filter existis in Top Filters Dashboard Window on New Group1
    ${group1Id}=  getGroupId  ${atipSession}  ${gName1}
    Log  ${group1Id}
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexAol}  ${timeInterval}

    log to console  TC001198426 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on New Group1
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig8}
    ${bpsSessionsAol}  getStatItem  ${bpsComponentsStats['appsim_3']}  ${bpsSessions}
    Log  ${bpsSessionsAol}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexAol}  ${bpsSessionsAol}  ${timeInterval}  ${group1Id}

    log to console  TC001198426 Read all stats from Filter Detailed Stats - Top Devices on on New Group1
    ${aolDeviceStats}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexAol.id}  ${expectedStatus}  ${group1Id}
    checkAtipStatsValues  ${aolDeviceStats}  ${deviceLinux}  ${totalSessions}  ${bpsSessionsAol}

    log to console  TC001198426 Read all stats from Filter Detailed Stats - App in Filtered Traffic on New Group1 (group id will be read from given filter object)
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexAol}  ${deviceLinux}  ${expAppAol}  ${timeInterval}

TC001198424[NP_Groups_vATIP][Regex]2xNew Groups with multiple NPs per group with different Regex Filter Configuration(Config_MoveNP)
    [Setup]  Clear Config

    log to console  TC001198424 Configuring Regex Filter New Group1
    createNewGroup  ${atipSession}  ${gName1}
    Create Filters  ${atipSession}  ${filterRegexEspn}  ${gName1}
    Create Filters  ${atipSession}  ${filterRegexAmazon}  ${gName1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId1}
    ${group1Id}=  getGroupId  ${atipSession}  ${gName1}
    ${detailedStats1}=  getConfig  ${atipSession}  ${group1Id}
    ${detailedStats1.enableTurboMode}=  set variable  False
    changeConfig  ${atipSession}  ${detailedStats1}  ${group1Id}

    log to console  TC001198424 Verifying per NP rules on New Group1
    Validate Rules Number Per NP  ${atipSession}  ${expRules3}  ${npIndexId1}

    log to console  TC001198424 Running BPS traffic for New Group1
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig9}

    log to console  TC001198424 Verifying Filter existis in Top Filters Dashboard Window on New Group1
    ${group1Id}=  getGroupId  ${atipSession}  ${gName1}
    Log  ${group1Id}
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexEspn}  ${timeInterval}

    log to console  TC001198424 Verifying Filter does not existis in Top Filters Dashboard Window on on New Group1
    checkFilterDoesNotExistsInTopStats  ${atipSession}  ${filterRegexAmazon}  ${timeInterval}

    log to console  TC001198424 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on New Group1
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig9}
    ${bpsSessionsEspn}  getStatItem  ${bpsComponentsStats['appsim_2']}  ${bpsSessions}
    Log  ${bpsSessionsEspn}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexEspn}  ${bpsSessionsEspn}  ${timeInterval}  ${group1Id}

    log to console  TC001198424 Read all stats from Filter Detailed Stats - Top Devices on on New Group1
    ${espnDeviceStats}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexEspn.id}  ${expectedStatus}  ${group1Id}
    checkAtipStatsValues  ${espnDeviceStats}  ${deviceMac}  ${totalSessions}  ${bpsSessionsEspn}

    log to console  TC001198424 Read all stats from Filter Detailed Stats - App in Filtered Traffic on New Group1 (group id will be read from given filter object)
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexEspn}  ${deviceMac}  ${expAppEspn}  ${timeInterval}

    log to console  TC001198424 Configuring Regex Filter New Group2
    createNewGroup  ${atipSession}  ${gName2}
    Create Filters  ${atipSession}  ${filterRegexFacebook}  ${gName2}
    moveNPsToGroup  ${atipSession}  ${gName2}  ${npId1}
    ${group2Id}=  getGroupId  ${atipSession}  ${gName2}
    ${detailedStats2}=  getConfig  ${atipSession}  ${group2Id}
    ${detailedStats2.enableTurboMode}=  set variable  False
    changeConfig  ${atipSession}  ${detailedStats2}  ${group2Id}

    log to console  TC001198424 Verifying per NP rules on New Group2
    Validate Rules Number Per NP  ${atipSession}  ${expRules2}  ${npIndexId1}

    log to console  TC001198424 Running BPS traffic for New Group2
    resetStats  ${atipSession}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig10}

    log to console  TC001198424 Verifying Filter existis in Top Filters Dashboard Window on New Group2
    ${group2Id}=  getGroupId  ${atipSession}  ${gName2}
    Log  ${group2Id}
    checkFilterExistsInTopStats  ${atipSession}  ${filterRegexFacebook}  ${timeInterval}

    log to console  TC001198424 Verifying Filter does not existis in Top Filters Dashboard Window on on New Group1 (filters are not mismatched between Groups)
    checkFilterDoesNotExistsInTopStats  ${atipSession}  ${filterRegexAmazon}  ${timeInterval}

    log to console  TC001198424 Verifying number of Session For Filter from Top Filters Dashboard Window VS BPS Sessions Stats on New Group2
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig10}
    ${bpsSessionsFacebook}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsSessions}
    Log  ${bpsSessionsFacebook}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${filterRegexFacebook}  ${bpsSessionsFacebook}  ${timeInterval}  ${group2Id}

    log to console  TC001198424 Read all stats from Filter Detailed Stats - Top Devices on on New Group2
    ${facebookDeviceStats}  StatsFilters.getTopStats  ${atipSession}  ${typeDevice}  ${timeInterval}  ${minimizedDashboard}  ${filterRegexFacebook.id}  ${expectedStatus}  ${group2Id}
    checkAtipStatsValues  ${facebookDeviceStats}  ${deviceLinux}  ${totalSessions}  ${bpsSessionsFacebook}

    log to console  TC001198424 Read all stats from Filter Detailed Stats - App in Filtered Traffic on New Group2 (group id will be read from given filter object)
    checkAppInFilterDetailedStats  ${atipSession}  ${filterRegexFacebook}  ${deviceLinux}  ${expAppFacebook}  ${timeInterval}


*** Keywords ***
Configure ATIP And Login
    login  ${atipSession}

Clear Config
   clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
   ${npId1}=  updateNpIdVirtual  ${atipSession}  ${envVars}
   set suite variable  ${npId1}  ${npId1}

Create Filters
    [Arguments]  ${atipSession}  ${filterObject}  ${groupName}
    ${groupId}=  getGroupId  ${atipSession}  ${groupName}
    ${filterObject.groupId}=  set variable  ${groupId}
    ${filterObject.id}=  set variable  -1
    createFilter  ${atipSession}  ${filterObject}
    rulePush  ${atipSession}  ${groupId}
    waitUntilSystemReady  ${atipSession}

Get GroupId from GroupName
    [Arguments]  ${atipSession}  ${netflowObject}  ${groupName}
    ${groupId}=  getGroupId  ${atipSession}  ${groupName}

Validate Rules Number Per NP
    [Arguments]  ${atipSession}  ${expectedRules}  ${networkProcessorId}
    checkNpRules  ${atipSession}  ${expectedRules}  ${envVars.atipSlotNumber[${networkProcessorId}]}
