
from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage
from atipAutoFrwk.services.TestCondition import TestCondition


class CustomApp(object):

    @classmethod
    def checkNumberStaticApps(cls, webApiSession, apps, npId=0, timeout=0, iterations=0):
        checkStaticApps = lambda: int(DebugPage.getNpStaticAppsDebugPage(webApiSession, npId)) == int(apps)
        TestCondition.waitUntilTrue(checkStaticApps, 'Expected Number of Static Apps:{}'.format(apps), totalTime=int(timeout), iterationTime=int(iterations))
        return None

