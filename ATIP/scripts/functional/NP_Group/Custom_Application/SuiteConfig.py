from http import HTTPStatus
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.MaxItemsPerCategory import MaxItemsPerCategory


class SuiteConfig(StatsDefaultConfig):
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    appStatsType = StatsType.APPS
    dynAppStatsType = StatsType.DYNAMIC
    timeInterval = TimeInterval.FIVEMIN
    dashBoardType = DashboardType.MINIMIZED
    minimizedDashboard = DashboardType.MINIMIZED.statsLimit

    g1 = GroupConfig(name='Test1')
    g2 = GroupConfig(name='Test2')
    g3 = GroupConfig(name='Test3')

    atipConfig = AtipConfig()

    atipConfig.addGroup(g1)
    atipConfig.addGroup(g2)
    atipConfig.addGroup(g3)

    npId0 = str(StatsDefaultConfig.envVars.atipSlotNumber[0])
    if StatsDefaultConfig.envVars.atipType == hardware:
        npId1 = str(StatsDefaultConfig.envVars.atipSlotNumber[1])
        npId2 = str(StatsDefaultConfig.envVars.atipSlotNumber[2])

    bpsTestConfig1 = BpsTestConfig('neo RECREATE vxlan',400)
    bpsTestConfig2 = BpsTestConfig('neo RECREATE dns_andorra_telekom',400)
    bpsTestConfig3 = BpsTestConfig('neo RECREATE desking_main',400)

    maxStaticApps = MaxItemsPerCategory.MAX_STATIC_APPS.maxNumber