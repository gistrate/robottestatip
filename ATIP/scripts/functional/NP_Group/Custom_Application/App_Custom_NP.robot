*** Settings ***
Documentation  Custom Application NP Group

Variables  SuiteConfig.py
Library    CustomApp.py
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.webApi.atip.CustomAppConfig
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.services.atip.NPService

Force Tags  hardware
Suite Setup        Configure ATIP And Login
Suite Teardown     Clean ATIP

*** Test Cases ***
TC001164803_Custom_Application_NP_Group_Test1_MoveNP_Configure
    [Tags]
    #Create new NP Group: Test1
    ${gid1} =  getGroupId  ${atipSession}  Test1
    log  ${gid1}
    #Move NP 3 to Group Test1
    changeNPGroup  ${atipSession}  ${npId2}  ${gid1}
    waitUntilGroupReady  ${atipSession}  ${gid1}

    Log  Uploading custom application signature to Test1...  console=true
    uploadCustomAppFile  ${atipSession}  ../../../frwk/src/atipAutoFrwk/data/custapps/ericsson-vxlan.xml  ${gid1}
    Log  Validating custom application signature upload...  console=true
    validateCustomApp  ${atipSession}  Ericsson VXLAN  1  ${gid1}

    Log  Running BPS test1: ${bpsTestConfig1.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}

    Log  Looking for application in top stats...  console=true
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}   ${timeInterval}   ${minimizedDashboard}  ${gid1}
    checkTargetExistsInTopStats  ${atipStats}  Ericsson VXLAN
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}   ${gid1}
    checkTargetDoesNotExistInTopStats  ${atipStats}  Ericsson VXLAN

     #Expecting 241 static apps to be installed after upload
    Log   Validating the correct number of static apps installed....  console=true
    ${maxapp} =  evaluate  ${maxStaticApps} + 1
    checkNumberStaticApps  ${atipSession}  ${maxapp}  ${npId2}  60  6


TC001164806_Custom_Application_NP_Group_Test2_Configure_MoveNP
    [Tags]
    #Create new NP Group: Test2
    ${gid2} =  getGroupId  ${atipSession}  Test2
    log  ${gid2}
    Log  Uploading custom application signature to Test2...  console=true
    uploadCustomAppFile  ${atipSession}  ../../../frwk/src/atipAutoFrwk/data/custapps/andorra-telecom-dns.xml  ${gid2}
    Log  Validating custom application signature upload...  console=true
    validateCustomApp  ${atipSession}  Andorra Telecom DNS  1  ${gid2}

    #Move NP 1 to Group Test2
    changeNPGroup  ${atipSession}  ${npId1}   ${gid2}
    waitUntilGroupReady  ${atipSession}  ${gid2}

    Log  Running BPS test2: ${bpsTestConfig2.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig2}

    Log  Looking for application in top stats...  console=true
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid2}
    checkTargetExistsInTopStats  ${atipStats}  Andorra Telecom DNS
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid2}
    checkTargetDoesNotExistInTopStats  ${atipStats}  Andorra Telecom DNS

    #Expecting 241 static apps to be installed after upload
    Log   Validating the correct number of static apps installed....  console=true
    ${maxapp} =  evaluate  ${maxStaticApps} + 1
    checkNumberStaticApps  ${atipSession}  ${maxapp}  ${npId1}  60  6


TC001161059_Custom_Application_NP_Group_Test3_MoveNP_Configure_DeleteApp_HW_vATIP
    [Tags]  virtual
    #Create new NP Group: Test1
    ${gid3} =  getGroupId  ${atipSession}  Test3
    log  ${gid3}

     #Move NP 1 to Group Test3
    changeNPGroup  ${atipSession}  ${npId0}   ${gid3}

    waitUntilGroupReady  ${atipSession}  ${gid3}

    Log  Uploading custom application signature to Test3...  console=true
    uploadCustomAppFile  ${atipSession}  ../../../frwk/src/atipAutoFrwk/data/custapps/cdk-desking.xml  ${gid3}
    Log  Validating custom application signature upload...  console=true
    validateCustomApp  ${atipSession}  CDK Desking  2  ${gid3}

    Log  Running BPS test3: ${bpsTestConfig3.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig3}

    Log  Looking for application in top stats...  console=true
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid3}
    checkTargetExistsInTopStats  ${atipStats}  CDK Desking
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid3}
    checkTargetDoesNotExistInTopStats  ${atipStats}  CDK Desking

    #Expecting 241 static apps to be installed after upload
    Log   Validating the correct number of static apps installed....  console=true
    ${maxapp} =  evaluate  ${maxStaticApps} + 1
    checkNumberStaticApps  ${atipSession}  ${maxapp}  ${npId0}  60  6

    log  Deleting custom App cdk-desking...  console=true
    deleteCustomApp  ${atipSession}  CDK Desking  2  ${gid3}
    #Reset Stats
    resetStats  ${atipSession}

    Log  Running BPS test3 again....: ${bpsTestConfig3.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig3}

    Log  Looking for application in top stats...  console=true
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid3}
    checkTargetDoesNotExistInTopStats  ${atipStats}  CDK Desking

    #Expecting 240 static apps to be installed after app is deleted
    Log   Validating the correct number of static apps installed....  console=true
    checkNumberStaticApps  ${atipSession}  ${maxStaticApps}  ${npId0}  60  6


*** Keywords ***
Configure ATIP And Login
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    configureAtip  ${atipSession}   ${atipConfig}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}

Clean ATIP
    logout  ${atipSession}
