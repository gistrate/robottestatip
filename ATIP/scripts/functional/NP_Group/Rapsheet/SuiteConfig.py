from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.data.atip.ThreatType import ThreatType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.TopStats import TopStats


class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    httpStatusNoContent = HTTPStatus.NO_CONTENT
    httpStatusServError = HTTPStatus.INTERNAL_SERVER_ERROR
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    rapsheetStatsType = StatsType.RAPSHEET
    timeInterval = TimeInterval.FIVEMIN
    dashBoardType = DashboardType.MINIMIZED
    minimizedDashboard = DashboardType.MINIMIZED.statsLimit
    Hijacked = ThreatType.HIJACKED
    Malware = ThreatType.MALWARE
    Phishing = ThreatType.PHISHING
    Botnet = ThreatType.BOTNET
    Exploit = ThreatType.EXPLOIT
    Sessions = TopStats.TOTAL_COUNT

    # Define groups and add them to the config
    g1 = GroupConfig(name='Test1')

    atipConfig = AtipConfig()

    atipConfig.addGroup(g1)
    npId0 = str(envVars.atipSlotNumber[0])
    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[1])
        npId2 = str(envVars.atipSlotNumber[2])

    bpsTestConfig1 = BpsTestConfig('mtest3',80)
    bpsTestConfig2 = BpsTestConfig('mtest3_multipleIPThreats', 80)