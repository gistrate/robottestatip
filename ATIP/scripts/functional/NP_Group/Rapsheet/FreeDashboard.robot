*** Settings ***
Documentation  Rapsheet FreeDashboard NP Group

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.config.atip.filter.FilterConditionConfig
Library    atipAutoFrwk.config.atip.filter.FilterConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.services.atip.stats.RapSheetService
Library    atipAutoFrwk.services.atip.NPService


Force Tags  hardware
Suite Setup        Configure ATIP And Login
Suite Teardown     Clean ATIP



*** Test Cases ***
TC001212083_Rapsheet_All_ThreatTypes
    [Tags]  virtual
    Log  Looking that Raphseet DB is present...  console=true
    verifyRapsheetDbExists  ${atipSession}  0

    Log  Running BPS test1: ${bpsTestConfig1.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}

    Log  Looking for threats in latest Raphseet widget...  console=true
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${rapsheetStatsType}  ${timeInterval}  ${minimizedDashboard}
    ${totalSessions} =  getAtipStatsValues  ${atipStats}  ${Hijacked.value}  ${Sessions}
    run keyword if  '${totalSessions}' == '0'  fail  Couldn\'t find threat type Hijacked
    ${totalSessions} =  getAtipStatsValues  ${atipStats}  ${Phishing.value}  ${Sessions}
    run keyword if  '${totalSessions}' == '0'  fail  Couldn\'t find threat type Phishing
    ${totalSessions} =  getAtipStatsValues  ${atipStats}  ${Exploit.value}  ${Sessions}
    run keyword if  '${totalSessions}' == '0'  fail  Couldn\'t find threat type Exploit
    ${totalSessions} =  getAtipStatsValues  ${atipStats}  ${Malware.value}  ${Sessions}
    run keyword if  '${totalSessions}' == '0'  fail  Couldn\'t find threat type Malware
    ${totalSessions} =  getAtipStatsValues  ${atipStats}  ${Botnet.value}  ${Sessions}
    run keyword if  '${totalSessions}' == '0'  fail  Couldn\'t find threat type Botnet


TC001212084_Rapsheet_MultipleIP_Threats
    [Tags]  virtual
    #Create new NP Group: Test3
    ${gid1} =  getGroupId  ${atipSession}  Test1
    log  ${gid1}

    #Move NP 1 to Group Test1
    changeNPGroup  ${atipSession}  ${npId0}  ${gid1}
    waitUntilGroupReady  ${atipSession}  ${gid1}


    Log  Running BPS test1: ${bpsTestConfig2.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig2}

    Log  Looking for threats in latest Raphseet widget...  console=true
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${rapsheetStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid1}
    ${totalSessions} =  getAtipStatsValues  ${atipStats}  ${Exploit.value}  ${Sessions}
    run keyword if  '${totalSessions}' == '0'  fail  Couldn\'t find threat type Exploit
    ${TotalSessions} =  getAtipStatsValues  ${atipStats}  ${Malware.value}  ${Sessions}
    run keyword if  '${totalSessions}' == '0'  fail  Couldn\'t find threat type Malware


*** Keywords ***
Configure ATIP And Login
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    configureAtip  ${atipSession}   ${atipConfig}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}


Clean ATIP
    logout  ${atipSession}