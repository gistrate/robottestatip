from http import HTTPStatus

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.config.atip.DedupGlobalConfig import DedupGlobalConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.data.atip.DashboardType import DashboardType



class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    hardware = AtipType.HARDWARE
    timeInterval = TimeInterval.HOUR
    statsType = StatsType.APPS
    totalPkts = TopStats.TOTAL_PKTS
    totalSessions = TopStats.TOTAL_COUNT
    minimizedDashboard = DashboardType.MINIMIZED.statsLimit
    # ATIP config
    dedupFilter1 = FilterConfig("DedupFilter")
    FilterConditionConfig.addAppCondition(dedupFilter1, ApplicationType.NETFLIX)
    dedupFilter1.forward = True
    dedupFilter1.transmitIds = [100]

    dedupFilter2 = FilterConfig("DedupFilter")
    FilterConditionConfig.addAppCondition(dedupFilter2, ApplicationType.NETFLIX)
    dedupFilter2.forward = True
    dedupFilter2.transmitIds = [100]

    # Define groups and add them to the config
    g1 = GroupConfig(name='Test1')
    g2 = GroupConfig(name='Test2')

    npId0 = str(envVars.atipSlotNumber[0])
    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[1])
        npId2 = str(envVars.atipSlotNumber[2])


    # Test: Validate Filter Name
    bpsTestConfig1 = BpsTestConfig('lowRate-bw-GET-response100_2CONNs', 80)

    atipConfig = AtipConfig()
    dedupConfig = DedupGlobalConfig("844600", True, "NONE")
    atipConfig.addGroup(g1)
    atipConfig.addGroup(g2)



