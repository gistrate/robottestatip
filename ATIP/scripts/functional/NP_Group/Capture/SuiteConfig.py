from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.AtipType import AtipType

class SuiteConfig(object):

    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    captureLocation = '/home/atip/captures/'
    gDefaultId = 0
    timeInterval = TimeInterval.HOUR
    HTTPStatus = HTTPStatus.OK
    fbApp = ApplicationType.FACEBOOK.appName
    netflixApp = ApplicationType.NETFLIX.appName
    hardware = AtipType.HARDWARE
    rel_tol = 0.5
    chinaGeoName = GeoLocation.CHINA.countryName
    chinaGeoCode = GeoLocation.CHINA.countryCode
    indiaGeoName = GeoLocation.INDIA.countryName
    indiaGeoCode = GeoLocation.INDIA.countryCode

    bpsConfig1 = 'TC001202950_pcap'
    bpsConfig2 = 'TC001202951_pcap'
    bpsConfig3 = 'TC001202952_pcap'
    bpsConfig4 = 'TC001202955_pcap'
    bpsConfig5 = 'TC001202956_pcap'
    bpsConfig6 = 'TC001202956_pcap2'

    npId0 = str(envVars.atipSlotNumber[0])
    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[1])
        npId2 = str(envVars.atipSlotNumber[2])