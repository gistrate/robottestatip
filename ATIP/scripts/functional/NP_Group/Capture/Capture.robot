*** Settings ***
Documentation  Capture test suite.

Variables  SuiteConfig.py

Library    CaptureLibrary.py
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.atip.FileService.FileService
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices.FilterCaptureServices
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.Capture
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.services.atip.CaptureService
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.webApi.atip.TopFilters.TopFilters
Library    Telnet  timeout=30  prompt=$
Library    BuiltIn
Library    String

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP
Test Setup         Clear ATIP
Test Teardown      Close All Connections
Suite Teardown     Clear configuration

*** Test Cases ***

TC001202950
    [Tags]  hardware
    #[NP_Groups_7300][PCAP]Default Group and 1 New Group with the same Configuration (MoveNP_Config)

    ${filterConfig}=  configureFilter  ${chinaGeoName}  ${None}  ${None}  ${chinaGeoCode}  1  ${gDefaultId}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${gDefaultId}

    Run traffic and capture  ${bpsConfig1}  ${chinaGeoName}  ${gDefaultId}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${gDefaultId}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${chinaGeoName}  totalPkts

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  1.45.1  ${None}  ${filterPackets}  ${gDefaultId}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1
    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    ${filterConfig}=  configureFilter  ${chinaGeoName}  ${None}  ${None}  ${chinaGeoCode}  1  ${g1id}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${g1id}

    Run traffic and capture  ${bpsConfig1}  ${chinaGeoName}  ${g1id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g1id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${chinaGeoName}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${fbApp}  ${filterPackets}  ${g1id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

TC001202951
    [Tags]  hardware
    #[NP_Groups_7300][PCAP]Default Group and 1 New Group with the same Configuration (MoveNP_Config)

    ${filterConfig}=  configureFilter  ${chinaGeoName}  ${None}  ${None}  ${chinaGeoCode}  1  ${gDefaultId}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${gDefaultId}

    Run traffic and capture  ${bpsConfig2}  ${chinaGeoName}  ${gDefaultId}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${gDefaultId}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${chinaGeoName}  totalPkts

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  1.45.1  ${None}  ${filterPackets}  ${gDefaultId}
    ${SecondPCAPStats}  Get 2nd PCAP Stats  ${NamesList}  1.45.1  ${None}  ${filterPackets}  ${gDefaultId}

    Validate Captures  ${FirstPCAPStats}  ${SecondPCAPStats}  ${filterPackets}

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    ${filterConfig}=  configureFilter  ${fbApp}  ${fbApp}  ${None}  ${None}  1  ${g1id}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${g1id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run traffic and capture  ${bpsConfig1}  ${fbApp}  ${g1id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g1id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${fbApp}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${fbApp}  ${filterPackets}  ${g1id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}


    ${filterId}=  getFilterId  ${atipSession}  ${fbApp}
    editCaptureSize  ${atipSession}  ${filterId}  1000000
    Run traffic and capture  ${bpsConfig1}  ${fbApp}  ${g1id}
    validateFileInfo  ${atipSession}  1000000

TC001202952
    [Tags]  hardware
    #[NP_Groups_7300][PCAP]2xNew Groups with different Filter settings per group(MoveNP_Config)

    createNewGroup  ${atipSession}  Group1
    createNewGroup  ${atipSession}  Group2
    ${g1id}  getGroupId  ${atipSession}  Group1
    ${g2id}  getGroupId  ${atipSession}  Group2

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    ${filterConfig}=  configureFilter  ${chinaGeoName}  ${None}  ${None}  ${chinaGeoCode}  1  ${g1id}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${g1id}

    Run traffic and capture  ${bpsConfig3}  ${chinaGeoName}  ${g1id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g1id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${chinaGeoName}  totalPkts

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  1.45.1  ${None}  ${filterPackets}  ${g1id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

    moveNPsToGroup  ${atipSession}  Group2  ${npId0}

    ${filterConfig}=  configureFilter  ${indiaGeoName}  ${None}  ${None}  ${indiaGeoCode}  1  ${g2id}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${g2id}

    Run traffic and capture  ${bpsConfig3}  ${indiaGeoName}  ${g2id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g2id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${indiaGeoName}  totalPkts

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  1.38.1  ${None}  ${filterPackets}  ${g2id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

    ${filterId}=  getFilterId  ${atipSession}  ${indiaGeoName}
    editCaptureSize  ${atipSession}  ${filterId}  1000000
    Run traffic and capture  ${bpsConfig3}  ${indiaGeoName}  ${g2id}
    validateFileInfo  ${atipSession}  1000000

TC001202953
    [Tags]  hardware
    #[NP_Groups_7300][PCAP]2xNew Groups with different Filter Configuration (Config_MoveNP)

    createNewGroup  ${atipSession}  Group1
    createNewGroup  ${atipSession}  Group2
    ${g1id}  getGroupId  ${atipSession}  Group1
    ${g2id}  getGroupId  ${atipSession}  Group2

    ${filterConfig}=  configureFilter  ${fbApp}  ${fbApp}  ${None}  ${None}  1  ${g1id}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${g1id}

    ${filterConfig}=  configureFilter  ${indiaGeoName}  ${None}  ${None}  ${indiaGeoCode}  1  ${g2id}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${g2id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run traffic and capture  ${bpsConfig3}  ${fbApp}  ${g1id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g1id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${fbApp}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${fbApp}  ${filterPackets}  ${g1id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

    moveNPsToGroup  ${atipSession}  Group2  ${npId0}

    Run traffic and capture  ${bpsConfig3}  ${indiaGeoName}  ${g2id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g2id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${indiaGeoName}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${fbApp}  ${filterPackets}  ${g2id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

    ${filterId}=  getFilterId  ${atipSession}  ${indiaGeoName}
    editCaptureSize  ${atipSession}  ${filterId}  1000000
    Run traffic and capture  ${bpsConfig3}  ${indiaGeoName}  ${g2id}
    validateFileInfo  ${atipSession}  1000000

TC001202955
    [Tags]  hardware
    #[NP_Groups_7300][PCAP]2xNew groups with 2 different filters per group

    createNewGroup  ${atipSession}  Group1
    createNewGroup  ${atipSession}  Group2
    ${g1id}  getGroupId  ${atipSession}  Group1
    ${g2id}  getGroupId  ${atipSession}  Group2

    ${filterConfig1}=  configureFilter  ${fbApp}  ${fbApp}  ${None}  ${None}  1  ${g1id}
    createFilter  ${atipSession}  ${filterConfig1}
    ${filterConfig2}=  configureFilter  ${netflixApp}  ${netflixApp}  ${None}  ${None}  1  ${g1id}
    createFilter  ${atipSession}  ${filterConfig2}

    rulePush  ${atipSession}  ${g1id}

    ${filterConfig1}=  configureFilter  ${indiaGeoName}  ${None}  ${None}  ${indiaGeoCode}  1  ${g2id}
    createFilter  ${atipSession}  ${filterConfig1}
    ${filterConfig2}=  configureFilter  ${chinaGeoName}  ${None}  ${None}  ${chinaGeoCode}  1  ${g2id}
    createFilter  ${atipSession}  ${filterConfig2}

    rulePush  ${atipSession}  ${g2id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run traffic and capture  ${bpsConfig4}  ${fbApp}  ${g1id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g1id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${fbApp}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${fbApp}  ${filterPackets}  ${g1id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

    resetStats  ${atipSession}

    Run traffic and capture  ${bpsConfig4}  ${netflixApp}  ${g1id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g1id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${netflixApp}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${netflixApp}  ${filterPackets}  ${g1id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}


    moveNPsToGroup  ${atipSession}  Group2  ${npId0}

    Run traffic and capture  ${bpsConfig3}  ${indiaGeoName}  ${g2id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g2id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${indiaGeoName}  totalPkts

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  1.38.1  ${None}  ${filterPackets}  ${g2id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

    resetStats  ${atipSession}


    Run traffic and capture  ${bpsConfig3}  ${chinaGeoName}  ${g2id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g2id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${chinaGeoName}  totalPkts

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  1.45.1  ${None}  ${filterPackets}  ${g2id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}


TC001202956
    [Tags]  hardware
    #[NP_Groups_7300][PCAP]2xNew groups with 2 different filters per group and multiple NPs

    createNewGroup  ${atipSession}  Group1
    createNewGroup  ${atipSession}  Group2
    ${g1id}  getGroupId  ${atipSession}  Group1
    ${g2id}  getGroupId  ${atipSession}  Group2

    ${filterConfig1}=  configureFilter  ${fbApp}  ${fbApp}  ${None}  ${None}  1  ${g1id}
    createFilter  ${atipSession}  ${filterConfig1}
    ${filterConfig2}=  configureFilter  ${netflixApp}  ${netflixApp}  ${None}  ${None}  1  ${g1id}
    createFilter  ${atipSession}  ${filterConfig2}

    rulePush  ${atipSession}  ${g1id}

    ${filterConfig1}=  configureFilter  ${indiaGeoName}  ${None}  ${None}  ${indiaGeoCode}  1  ${g2id}
    createFilter  ${atipSession}  ${filterConfig1}
    ${filterConfig2}=  configureFilter  ${chinaGeoName}  ${None}  ${None}  ${chinaGeoCode}  1  ${g2id}
    createFilter  ${atipSession}  ${filterConfig2}

    rulePush  ${atipSession}  ${g2id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}
    moveNPsToGroup  ${atipSession}  Group1  ${npId1}

    Run traffic and capture  ${bpsConfig5}  ${fbApp}  ${g1id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g1id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${fbApp}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${fbApp}  ${filterPackets}  ${g1id}
    ${SecondPCAPStats}  Get 2nd PCAP Stats  ${NamesList}  ${None}  ${fbApp}  ${filterPackets}  ${g1id}

    Validate Captures  ${FirstPCAPStats}  ${SecondPCAPStats}  ${filterPackets}

    resetStats  ${atipSession}

    Run traffic and capture  ${bpsConfig5}  ${netflixApp}  ${g1id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g1id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${netflixApp}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${netflixApp}  ${filterPackets}  ${g1id}
    ${SecondPCAPStats}  Get 2nd PCAP Stats  ${NamesList}  ${None}  ${netflixApp}  ${filterPackets}  ${g1id}

    Validate Captures  ${FirstPCAPStats}  ${SecondPCAPStats}  ${filterPackets}

    moveNPsToGroup  ${atipSession}  Group2  ${npId0}
    moveNPsToGroup  ${atipSession}  Group2  ${npId1}

    Run traffic and capture  ${bpsConfig6}  ${indiaGeoName}  ${g2id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g2id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${indiaGeoName}  totalPkts

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  1.38.1  ${None}  ${filterPackets}  ${g2id}
    ${SecondPCAPStats}  Get 2nd PCAP Stats  ${NamesList}  1.38.1  ${None}  ${filterPackets}  ${g2id}

    Validate Captures  ${FirstPCAPStats}  ${SecondPCAPStats}  ${filterPackets}

    resetStats  ${atipSession}


    Run traffic and capture  ${bpsConfig6}  ${chinaGeoName}  ${g2id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g2id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${chinaGeoName}  totalPkts

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  1.45.1  ${None}  ${filterPackets}  ${g2id}
    ${SecondPCAPStats}  Get 2nd PCAP Stats  ${NamesList}  1.45.1  ${None}  ${filterPackets}  ${g2id}

    Validate Captures  ${FirstPCAPStats}  ${SecondPCAPStats}  ${filterPackets}

TC001202957
    [Tags]  hardware
    #[NP_Groups_V1][PCAP]2xNew groups with 2 different filters per group

    createNewGroup  ${atipSession}  Group1
    createNewGroup  ${atipSession}  Group2
    ${g1id}  getGroupId  ${atipSession}  Group1
    ${g2id}  getGroupId  ${atipSession}  Group2

    ${filterConfig1}=  configureFilter  ${fbApp}  ${fbApp}  ${None}  ${None}  1  ${g1id}
    createFilter  ${atipSession}  ${filterConfig1}
    ${filterConfig2}=  configureFilter  ${netflixApp}  ${netflixApp}  ${None}  ${None}  1  ${g1id}
    createFilter  ${atipSession}  ${filterConfig2}

    rulePush  ${atipSession}  ${g1id}

    ${filterConfig1}=  configureFilter  ${indiaGeoName}  ${None}  ${None}  ${indiaGeoCode}  1  ${g2id}
    createFilter  ${atipSession}  ${filterConfig1}
    ${filterConfig2}=  configureFilter  ${chinaGeoName}  ${None}  ${None}  ${chinaGeoCode}  1  ${g2id}
    createFilter  ${atipSession}  ${filterConfig2}

    rulePush  ${atipSession}  ${g2id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run traffic and capture  ${bpsConfig4}  ${fbApp}  ${g1id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g1id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${fbApp}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${fbApp}  ${filterPackets}  ${g1id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

    resetStats  ${atipSession}

    Run traffic and capture  ${bpsConfig4}  ${netflixApp}  ${g1id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g1id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${netflixApp}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${netflixApp}  ${filterPackets}  ${g1id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}


    moveNPsToGroup  ${atipSession}  Group2  ${npId0}

    Run traffic and capture  ${bpsConfig3}  ${indiaGeoName}  ${g2id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g2id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${indiaGeoName}  totalPkts

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  1.38.1  ${None}  ${filterPackets}  ${g2id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

    resetStats  ${atipSession}


    Run traffic and capture  ${bpsConfig3}  ${chinaGeoName}  ${g2id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g2id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${chinaGeoName}  totalPkts

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  1.45.1  ${None}  ${filterPackets}  ${g2id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

    ${filterId}=  getFilterId  ${atipSession}  ${chinaGeoName}
    editCaptureSize  ${atipSession}  ${filterId}  1000000
    Run traffic and capture  ${bpsConfig3}  ${chinaGeoName}  ${g2id}
    validateFileInfo  ${atipSession}  1000000

TC001202958
    [Tags]  virtual
    #[NP_Groups_vATIP][PCAP]Default Group and one new group with different filter priority configuration

    createNewGroup  ${atipSession}  Group1
    createNewGroup  ${atipSession}  Group2
    ${g1id}  getGroupId  ${atipSession}  Group1
    ${g2id}  getGroupId  ${atipSession}  Group2

    ${filterConfig}=  configureFilter  ${fbApp}  ${fbApp}  ${None}  ${None}  1  ${g1id}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${g1id}

    ${filterConfig}=  configureFilter  ${indiaGeoName}  ${None}  ${None}  ${indiaGeoCode}  1  ${g2id}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${g2id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run traffic and capture  ${bpsConfig3}  ${fbApp}  ${g1id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g1id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${fbApp}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${fbApp}  ${filterPackets}  ${g1id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

    ${filterId}=  getFilterId  ${atipSession}  ${fbApp}

    editCaptureSize  ${atipSession}  ${filterId}  1000000
    Run traffic and capture  ${bpsConfig3}  ${fbApp}  ${g1id}
    validateFileInfo  ${atipSession}  1000000

    moveNPsToGroup  ${atipSession}  Group2  ${npId0}

    Run traffic and capture  ${bpsConfig3}  ${indiaGeoName}  ${g2id}

    ${NamesList}  Transfer PCAP

    ${topFilters}=  getTopFiltersStats  ${atipSession}  ${g2id}

    ${filterPackets}=  getFilterStatsActualValue  ${topFilters}  ${indiaGeoName}  totalCount

    ${FirstPCAPStats}  Get 1st PCAP Stats  ${NamesList}  ${None}  ${fbApp}  ${filterPackets}  ${g2id}

    Validate Captures  ${FirstPCAPStats}  ${None}  ${filterPackets}

*** Keywords ***

Run traffic and capture
    [Arguments]  ${bpsFile}  ${filter}  ${gId}

    # perform captures
    # get filter id
    ${filterId}=  getFilterId  ${atipSession}  ${filter}

    # delete capture files from ATIP
    deleteCaptureFiles  ${atipSession}

    # start capture on ATIP
    controlCapture  ${atipSession}  ${filterId}  ${True}

    #run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Connect on Linux Box
    Write  cd captures
    Become SU

    controlCapture  ${atipSession}  ${filterId}  ${False}

Transfer PCAP
    # download last PCAP and analyze

    ${localPcapFolder}=  getLatestCaptureAndUnzip  ${atipSession}
    Connect on Linux Box
    Write  cd captures
    Become SU

    ${NamesList}  transferAtipCapture  ${atipSession}  ${envVars}  ${localPcapFolder}  ${captureLocation}${TEST NAME}

    [Return]  ${NamesList}

Get 1st PCAP Stats
    [Arguments]  ${NamesList}  ${IpSubnet}  ${hostname}  ${expectedStats}  ${gId}

    ${displayFilter}=  set variable if  $IpSubnet!=$None   ip.host matches ${IpSubnet}  http.host contains "${hostname}"

    ${filteringCommand}=  configFilterCommand  ${NamesList[0]}  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_1
    Execute Command  ${filteringCommand}
    ${output}=  Execute Command  wc -l < ${TEST NAME}_1.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !

    [return]  ${statsCaptureLB}

Get 2nd PCAP Stats
    [Arguments]  ${NamesList}  ${IpSubnet}  ${hostname}  ${expectedStats}  ${gId}

    ${displayFilter}=  set variable if  $IpSubnet!=$None   ip.host matches ${IpSubnet}  http.host contains "${hostname}"

    ${filteringCommand}=  configFilterCommand  ${NamesList[1]}  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_1
    Execute Command  ${filteringCommand}
    ${output}=  Execute Command  wc -l < ${TEST NAME}_1.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !

    [return]  ${statsCaptureLB}

Validate Captures
    [Arguments]  ${FirstPCAP}  ${SecondPCAP}  ${expectedStats}

    ${SecondPCAP}=  set variable if  $SecondPCAP==$None  0  ${SecondPCAP}
    ${PCAPSum}=  Evaluate  ${FirstPCAP}+${SecondPCAP}
    convert to integer  ${PCAPSum}
    ${Validation}=  validatePackets  ${expectedStats}  ${PCAPSum}  ${rel_tol}
    Should Be True  ${Validation}

Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [return]  ${output}

Connect on Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Read
    [return]  ${openTelnet}

Become SU
    Write  sudo su
    sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt

Configure ATIP
    atipLogin.Login  ${atipSession}

    Connect on Linux Box
    #create temp folder for testing captures
    Write  mkdir captures

    Close Connection

Clear ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}
    Connect on Linux Box
    Write  cd captures
    Become SU
    Write  rm TC*
    Close Connection

Clear configuration
    Connect on Linux Box
    Execute Command  rm -rf captures
    Close Connection
    atipLogout.Logout  ${atipSession}