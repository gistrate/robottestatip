from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.services.atip.CaptureService import CaptureService
from atipAutoFrwk.services.atip.FileService import FileService
from math import isclose
import warnings


class CaptureLibrary(object):


    def configureFilter(self, filterName, appName=None, hdrMaskId=None, geo=None, priority=1000, groupId=0):
        filterConfig = FilterConfig(filterName)
        filterConfig.groupId = groupId
        if hdrMaskId != None:
            filterConfig.addHeaderMaskUUIDS(hdrMaskId)

        filterConfig.priority = priority

        if not isinstance(appName, list):
            for member in ApplicationType.__members__.values():
                if member.appName == appName:
                    FilterConditionConfig.addAppCondition(filterConfig, member)
        else:
            # build list of AppType objects corresponding to appList
            appType = []
            for app in appName:
                for member in ApplicationType.__members__.values():
                    if member.appName == app:
                        appType.append(member)

            FilterConditionConfig.addAppCondition(filterConfig, appType)

        if geo:
            for member in GeoLocation.__members__.values():
                if member.countryCode == geo:
                    FilterConditionConfig.addGeoCondition(filterConfig, member)

        return filterConfig

    @classmethod
    def getFilterStatsActualValue(cls, topFiltersStats, filterName, statType):
        '''
        Return counter for specified statistic
        :param topFiltersStats: top 10 filter statistics
        :param filterName: name of filter to get statistics for
        :param statType: type of statistic:   serverBytes, totalBytes, serverPkts, clientPkts, clientBytes, ruleId,
                                              totalCount, totalPkts, asNumber, discovery
        :return: specific statistic value
        '''

        actualValue = 0
        if len(topFiltersStats['stats'][0]) == 1:
            # empty TopFilter statistics contains {"stats": {"name": "rules"}}, so not really empty, but not having "list" attribute which contains actual statistics
            warnings.warn('TopFilter statistics are empty.')
            return False
        elif not isinstance(topFiltersStats['stats'][0]['list'], list):
            topFiltersStats['stats'][0]['list'] = [topFiltersStats['stats'][0]['list']]

        for statsItem in topFiltersStats['stats'][0]['list']:
            if statsItem['msg'] == filterName:
                actualValue = statsItem[statType]
                break
        return actualValue

    @classmethod
    def validateFileInfo(cls, webApiSession, setCaptureSize):
        pcapFolder = CaptureService.getLatestCaptureAndUnzip(webApiSession)
        fileSize, fileName = FileService.getFileInfo(pcapFolder)
        print('Capture file name is {}.'.format(fileName))
        print('Capture length is {} bits.'.format(fileSize))

        # verify offset between capture file and capture size already set
        diff = abs(int(setCaptureSize) - fileSize)

        FileService.removePcapAndFolder(webApiSession, pcapFolder)

        if diff > int(setCaptureSize) / 2:
            raise Exception('PCAP size difference')
        else:
            print('Length difference of {} bits is in acceptable range (configured capture limit is {}).'.format(diff, setCaptureSize))

    @classmethod
    def validatePackets(cls, expectedValue, actualValue, relative_tolerance):
        if isclose(expectedValue, actualValue, rel_tol=relative_tolerance):
            return True
        else:
            return False