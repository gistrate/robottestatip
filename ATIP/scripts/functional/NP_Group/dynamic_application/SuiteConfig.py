from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.MaxItemsPerCategory import MaxItemsPerCategory


class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    httpStatusNoContent = HTTPStatus.NO_CONTENT
    httpStatusServError = HTTPStatus.INTERNAL_SERVER_ERROR
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    appStatsType = StatsType.APPS
    dynAppStatsType = StatsType.DYNAMIC
    timeInterval = TimeInterval.FIVEMIN
    dashBoardType = DashboardType.MINIMIZED
    minimizedDashboard = DashboardType.MINIMIZED.statsLimit
    Techcrunch = ApplicationType.TECHCRUNCH
    Businessinsider = ApplicationType.BUSINESSINSIDER
    Boredpanda = ApplicationType.BOREDPANDA

    # Define groups and add them to the config
    g1 = GroupConfig(name='Test1')
    g2 = GroupConfig(name='Test2')
    g3 = GroupConfig(name='Test3')
    g4 = GroupConfig(name='Test4')

    atipConfig = AtipConfig()

    atipConfig.addGroup(g1)
    atipConfig.addGroup(g2)
    atipConfig.addGroup(g3)
    atipConfig.addGroup(g4)

    npId0 = str(envVars.atipSlotNumber[0])
    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[1])
        npId2 = str(envVars.atipSlotNumber[2])

    appFilter2 = FilterConfig(Techcrunch.value, groupId=0)
    FilterConditionConfig.addAppCondition(appFilter2, Techcrunch)
    appFilter3 = FilterConfig(Businessinsider.value, groupId=0)
    FilterConditionConfig.addAppCondition(appFilter3, Businessinsider)

    bpsTestConfig1 = BpsTestConfig('automation_5dynamic_apps_NP',400)

    maxStaticApps = MaxItemsPerCategory.MAX_STATIC_APPS.maxNumber