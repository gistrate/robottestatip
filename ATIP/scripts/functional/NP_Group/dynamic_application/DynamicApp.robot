*** Settings ***
Documentation  Dynamic Application NP Group

Variables  SuiteConfig.py

Library    DynamicAppLib.py
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.config.atip.filter.FilterConditionConfig
Library    atipAutoFrwk.config.atip.filter.FilterConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.webApi.atip.CustomAppConfig
Library    atipAutoFrwk.webApi.atip.DynamicApps
Library    atipAutoFrwk.services.atip.NPService

Force Tags  hardware
Suite Setup        Configure ATIP And Login
Suite Teardown     Clean ATIP



*** Test Cases ***
TC001167209_Dynamic_App_NP_Group_DownloadUpload_7300
    [Tags]
    #Create new NP Group: Test1
    ${gid1} =  getGroupId  ${atipSession}  Test1
    log  ${gid1}

    #Move NP 3 to Group Test1
    changeNPGroup  ${atipSession}  ${npId2}  ${gid1}
    waitUntilGroupReady  ${atipSession}  ${gid1}

    Log  Running BPS test1: ${bpsTestConfig1.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}

    Log  Looking for application in latest dynamic apps...  console=true
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid1}
    checkTargetExistsInTopStats  ${atipStats}  ${Boredpanda.value}

    #Download dynamic app
    downloadDynamicApp  ${atipSession}  ${Boredpanda.value}  boredpanda.xml  ${gid1}

    Log  Uploading dynamic app as custom application to Test1...  console=true
    uploadCustomAppFile  ${atipSession}  boredpanda.xml  ${gid1}
    Log  Validating custom application signature upload...  console=true
    validateCustomApp  ${atipSession}  ${Boredpanda.value}  1  ${gid1}

    #Reset Stats
    resetStats  ${atipSession}

    Log  Running BPS test1: ${bpsTestConfig1.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}

    Log  Looking for application in top stats...  console=true
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid1}
    checkTargetExistsInTopStats  ${atipStats}  ${Boredpanda.value}
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid1}
    checkTargetDoesNotExistInTopStats  ${atipStats}  ${Boredpanda.value}


    #Expecting 0 Dynamic apps to be installed for Group1
    Log   Validating the correct number of dynamic apps installed....  console=true
    checkNumberDynamicApps  ${atipSession}  0  ${npId0}  60  6


TC001167208_Dynamic_App_NP_Group_DeleteApp_7300
    [Tags]
    #Create new NP Group: Test2
    ${gid2} =  getGroupId  ${atipSession}  Test2
    log  ${gid2}

    #Move NP 2 to Group Test1
    changeNPGroup  ${atipSession}  ${npId1}  ${gid2}
    waitUntilGroupReady  ${atipSession}  ${gid2}

    Log  Running BPS test2: ${bpsTestConfig1.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}

    Log  Looking for application in latest dynamic apps...  console=true
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid2}
    checkTargetExistsInTopStats  ${atipStats}  ${Techcrunch.value}

    #Add app filter to group Test2
    ${appFilter2.groupId} =  set variable  ${gid2}
     createFilter  ${atipSession}  ${appFilter2}
     rulePush  ${atipSession}  ${gid2}

    Log  Can not delete dynamic app...  console=true
    deleteDynamicApp  ${atipSession}  ${Techcrunch}  ${gid2}  ${httpStatusServError}

    Log  Running BPS test2: ${bpsTestConfig1.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}

    Log  Checking if Test2 filter exists in Top Filters Widget : ${appFilter2.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${appFilter2}  ${timeInterval}  ${dashBoardType}

    Log  Deleting Filter : ${appFilter2.name}  console=true
    deleteFilterByName  ${atipSession}  ${appFilter2.name}

    Log  Looking for application in installed apps...  console=true
    dynamicAppPresent  ${atipSession}  ${Techcrunch.value}  ${gid2}  ${True}

    Log  Deleting dynamic app...  console=true
    deleteDynamicApp  ${atipSession}  ${Techcrunch.value}  ${gid2}

    Log  Looking for application in installed apps...  console=true
    dynamicAppPresent  ${atipSession}  ${Techcrunch.value}  ${gid2}  ${False}  60


TC001167211_Dynamic_App_NP_Group_7300_vATIP
    [Tags]  virtual
    #Create new NP Group: Test3
    ${gid3} =  getGroupId  ${atipSession}  Test3
    log  ${gid3}

    #Move NP 1 to Group Test3
    changeNPGroup  ${atipSession}  ${npId0}  ${gid3}
    waitUntilGroupReady  ${atipSession}  ${gid3}

    Log  Running BPS test3: ${bpsTestConfig1.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}

    Log  Looking for application in latest dynamic apps...  console=true
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid3}
    checkTargetExistsInTopStats  ${atipStats}  ${Businessinsider.value}

    #Create new NP Group: Test4
    ${gid4} =  getGroupId  ${atipSession}  Test4
    log  ${gid4}

    #Move NP 1 to Group Test4
    changeNPGroup  ${atipSession}  ${npId0}  ${gid4}
    waitUntilGroupReady  ${atipSession}  ${gid4}

    #Download dynamic app from Test3
    downloadDynamicApp  ${atipSession}  ${Businessinsider.value}  businessinsider.xml  ${gid3}

    Log  Deleting dynamic app from Test3...  console=true
    deleteDynamicApp  ${atipSession}  ${Businessinsider.value}  ${gid3}

    Log  Looking for application in installed apps...  console=true
    dynamicAppPresent  ${atipSession}  businessinsider.xml  ${gid3}  ${false}  120

    Log  Uploading dynamic app as custom application to Test3...  console=true
    uploadCustomAppFile  ${atipSession}  businessinsider.xml  ${gid3}
    Log  Validating custom application signature upload...  console=true
    validateCustomApp  ${atipSession}  ${Businessinsider.value}  1  ${gid3}


     #Add app filter to group Test3 Only if the app exists
    ${appFilter3.groupId} =  set variable  ${gid3}
     createFilter  ${atipSession}  ${appFilter3}
     rulePush  ${atipSession}  ${gid3}

    #Move NP 1 to Group Test3
    changeNPGroup  ${atipSession}  ${npId0}  ${gid3}
    waitUntilGroupReady  ${atipSession}  ${gid3}

    #Reset Stats
    resetStats  ${atipSession}

    Log  Running BPS test3: ${bpsTestConfig1.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}

    Log  Looking for application in top stats...  console=true
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid3}
    checkTargetExistsInTopStats  ${atipStats}  ${Businessinsider.value}
    ${atipStats}  ListStats.getTopStats  ${atipSession}  ${dynAppStatsType}  ${timeInterval}  ${minimizedDashboard}  ${gid3}
    checkTargetDoesNotExistInTopStats  ${atipStats}  ${Businessinsider.value}

     Log  Checking if Test3 filter exists in Top Filters Widget : ${appFilter3.name}  console=true
     checkFilterExistsInTopStats  ${atipSession}  ${appFilter3}  ${timeInterval}  ${dashBoardType}


*** Keywords ***
Configure ATIP And Login
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    configureAtip  ${atipSession}   ${atipConfig}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}

Clean ATIP
    logout  ${atipSession}