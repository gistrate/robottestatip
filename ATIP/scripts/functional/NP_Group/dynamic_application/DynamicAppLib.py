from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.StaticAppService import StaticApp


class DynamicAppLib(object):

    @classmethod
    def dynamicAppPresent(cls, webApiSession, appName, groupId=0, condition= False, timeout=10, iterations=1):

        if condition == True:
            checkDynApp = lambda: StaticApp.checkForInstalledApp(webApiSession, appName, groupId)
            TestCondition.waitUntilTrue(checkDynApp, 'Could not find Dynamic App:{}'.format(appName),
                                    totalTime=int(timeout), iterationTime=int(iterations))
        else:
            checkDynApp = lambda: StaticApp.checkForInstalledApp(webApiSession, appName, groupId)
            TestCondition.waitUntilFalse(checkDynApp, 'Found Dynamic App:{}'.format(appName),
                                        totalTime=int(timeout), iterationTime=int(iterations))

        return None