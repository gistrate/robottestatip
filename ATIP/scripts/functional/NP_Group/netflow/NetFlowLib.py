from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage
from atipAutoFrwk.services.TestCondition import TestCondition


class NetFlowLib(object):

    def verifyNpRules(self, webSession, npId, expRules):
        checkRulesLambda = lambda: int(DebugPage.getNpRulesFromDebugPage(webSession, npId)) == int(expRules)
        TestCondition.waitUntilTrue(checkRulesLambda, errorMessage="Installed rules number: {} not equal with expected rules number :{} ".format(DebugPage.getNpRulesFromDebugPage(webSession, npId), expRules), totalTime=300, iterationTime=5)
        return 0

