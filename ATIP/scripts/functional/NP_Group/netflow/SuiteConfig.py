import ipaddress
from http import HTTPStatus

from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.Card import Card
from atipAutoFrwk.config.atip.Collector import Collector
from atipAutoFrwk.config.atip.NetflowCardConfig import NetflowCardConfig
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Groups import Groups
from atipAutoFrwk.data.atip.NetflowStats import NetflowStats
from atipAutoFrwk.data.atip.StatsType import StatsType


class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)

    gName1 = 'Group1'
    gName2 = 'Group2'
    gName3 = 'Group3'
    defaultG = 'Default'
    slotId1 = '1'

    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[0])
        npId2 = str(envVars.atipSlotNumber[1])
        npId3 = str(envVars.atipSlotNumber[2])
    else:
        npId1 = str(envVars.atipSlotNumber[0])

    expRules = 2
    expRules3 = 3
    bpsTestConfig = BpsTestConfig('TC001160851_DefaultGroup_NP4NP2', 310)
    bpsTestConfig2 = BpsTestConfig('TC001160851_Group1_NP2NP3', 310)
    bpsTestConfig3 = BpsTestConfig('TC001160852_DefaultGroup_NP4NP2', 310)
    bpsTestConfig4 = BpsTestConfig('TC001160859', 310)
    bpsTestConfig5 = BpsTestConfig('TC001160859_Group1', 310)

    netflowRecords = NetflowStats.DATA_RECORDS
    appStatsType = StatsType.APPS

    # NetFlow Configuration for Default Group
    netflowdefaultcfg = NetflowConfig()
    netflowdefaultcfg.groupId = 0
    netflowdefaultcfg.netflowL7ImsiMapping = False
    netflowdefaultcfg.netflowL7SrcLongitude = False
    netflowdefaultcfg.netflowL7DNSClass = True
    netflowdefaultcfg.netflowL7SrcRegionName = False
    netflowdefaultcfg.netflowL7AppID = True
    netflowdefaultcfg.netflowL7SrcCityName = False
    netflowdefaultcfg.netflowL7DNSHostname = False
    netflowdefaultcfg.enabled = True
    netflowdefaultcfg.netflow_l7 = True
    netflowdefaultcfg.netflowL7DeviceId = False
    netflowdefaultcfg.netflowL7EncryptionCipher = False
    netflowdefaultcfg.netflowL7SrcCountryCode = False
    netflowdefaultcfg.netflowL7SrcRegionCode = False
    netflowdefaultcfg.netflowL7URI = True
    netflowdefaultcfg.netflowL7EncryptionKeyLen = False
    netflowdefaultcfg.netflowL7DstRegionName = False
    netflowdefaultcfg.netflowL7DstCityName = False
    netflowdefaultcfg.netflowL7DstLatitude = False
    netflowdefaultcfg.netflowL7DstCountryName = False
    netflowdefaultcfg.netflowL7Domain = True
    netflowdefaultcfg.netflowL7BrowserId = False    
    netflowdefaultcfg.netflowL7SrcLatitude = False
    netflowdefaultcfg.netflowL7DstLongitude = False
    netflowdefaultcfg.netflowL7DstRegionCode = False
    netflowdefaultcfg.netflowL7BrowserName = False
    netflowdefaultcfg.netflowL7DeviceName = False
    netflowdefaultcfg.netflowL7SrcCountryName = False
    netflowdefaultcfg.netflowL7AppName = True
    netflowdefaultcfg.netflowL7ConnEncryptType = False
    netflowdefaultcfg.netflowL7Latency = False
    netflowdefaultcfg.netflowL7DNSText = True
    netflowdefaultcfg.netflowL7DstCountryCode = False
    netflowdefaultcfg.netflowL7UserAgent = True
    netflowdefaultcfg.netflowL7DNSHostnameQuery = True
    netflowdefaultcfg.netflowL7DNSHostnameResponse = True

    if envVars.atipType == hardware:
        firstCardDefault = Card(id=envVars.atipSlotNumber[0],
                         gw = '192.168.30.200',
                         odid = '100',
                         netmask = '255.255.255.0',
                         ipMethod = 'STATIC',
                         dns = '8.8.8.8',
                         ip_addr = '192.168.30.1',
                         enabled=True, )

        secondCardDefault = Card(id=envVars.atipSlotNumber[1],
                         gw='192.168.30.200',
                         odid='100',
                         netmask='255.255.255.0',
                         ipMethod='STATIC',
                         dns='8.8.8.8',
                         ip_addr='192.168.30.2',
                         enabled=True, )
    else:
        firstCardDefault = Card(id=envVars.atipSlotNumber[0],
                                gw='192.168.30.200',
                                odid='100',
                                netmask='255.255.255.0',
                                ipMethod='STATIC',
                                dns='8.8.8.8',
                                ip_addr='192.168.30.1',
                                enabled=True, )


    firstCollectorDefaultIP = envVars.netflowCollectorConnectionConfig.host
    secondCollectorDefaultIP = str(ipaddress.IPv4Address(firstCollectorDefaultIP) + 1)
    thirdCollectorDefaultIP = str(ipaddress.IPv4Address(firstCollectorDefaultIP) + 2)
    firstCollectorDefault = Collector(0, 100, 'Samples', True, 'UDP', 4739, firstCollectorDefaultIP)
    secondCollectorDefault = Collector(1, 100, 'Samples', True, 'UDP', 4739, secondCollectorDefaultIP)
    thirdCollectorDefault = Collector(2, 100, 'Samples', True, 'UDP', 4739, thirdCollectorDefaultIP)

    # firstCollectorDefaultIPvATIP = '10.38.185.45'
    firstCollectorDefaultIPvATIP = envVars.netflowCollectorConnectionConfig.host
    secondCollectorDefaultIPvATIP = str(ipaddress.IPv4Address(firstCollectorDefaultIPvATIP) + 1)
    firstCollectorDefaultvATIP = Collector(0, 100, 'Samples', True, 'UDP', 4739, firstCollectorDefaultIPvATIP)
    secondCollectorDefaultvATIP = Collector(1, 100, 'Samples', True, 'UDP', 4739, secondCollectorDefaultIPvATIP)

    filter1DefaultGroup = FilterConfig("filter1DefaultGroup")
    FilterConditionConfig.addAppCondition(filter1DefaultGroup, ApplicationType.FACEBOOK)
    filter1DefaultGroup.netflow = True
    filter1DefaultGroup.collectors = [0]

    filter1DefaultGroupAol = FilterConfig("filter1DefaultGroupAol")
    FilterConditionConfig.addAppCondition(filter1DefaultGroupAol, ApplicationType.AOL)
    filter1DefaultGroupAol.netflow = True
    filter1DefaultGroupAol.collectors = [1]

    netflowgroupcfg = NetflowConfig()
    netflowgroupcfg.enabled = True
    #GEO
    netflowgroupcfg.netflowL7SrcLongitude = True
    netflowgroupcfg.netflowL7SrcCityName = True
    netflowgroupcfg.netflowL7SrcRegionName = True
    netflowgroupcfg.netflowL7SrcCountryCode = True
    netflowgroupcfg.netflowL7SrcRegionCode = True
    netflowgroupcfg.netflowL7SrcASName = False
    netflowgroupcfg.netflowL7DstRegionName = True
    netflowgroupcfg.netflowL7DstCityName = True
    netflowgroupcfg.netflowL7DstLatitude = True
    netflowgroupcfg.netflowL7DstCountryName = True
    netflowgroupcfg.netflowL7SrcLatitude = True
    netflowgroupcfg.netflowL7DstLongitude = True
    netflowgroupcfg.netflowL7DstRegionCode = True
    netflowgroupcfg.netflowL7SrcCountryName = True
    netflowgroupcfg.netflowL7DstCountryCode = True
    netflowgroupcfg.netflowL7DstASName = False
    #APP
    netflowgroupcfg.netflowL7URI = False
    netflowgroupcfg.netflowL7DNSClass = False
    netflowgroupcfg.netflowL7AppID = False
    netflowgroupcfg.netflowL7DNSHostname = False
    netflowgroupcfg.netflow_l7 = True
    netflowgroupcfg.netflowL7Domain = False
    netflowgroupcfg.netflowL7AppName = False
    netflowgroupcfg.netflowL7Latency = False
    netflowgroupcfg.netflowL7DNSText = False
    netflowgroupcfg.netflowL7UserAgent = False
    netflowgroupcfg.netflowL7DNSHostnameQuery = False
    netflowgroupcfg.netflowL7DNSHostnameResponse = False
    #DEVICE
    netflowgroupcfg.netflowL7DeviceId = False
    netflowgroupcfg.netflowL7BrowserId = False
    netflowgroupcfg.netflowL7BrowserName = False
    netflowgroupcfg.netflowL7DeviceName = False
    #SSL
    netflowgroupcfg.netflowL7EncryptionCipher = False
    netflowgroupcfg.netflowL7EncryptionKeyLen = False
    netflowgroupcfg.netflowL7ConnEncryptType = False

    if envVars.atipType == hardware:
        firstCardGroup = Card(id=envVars.atipSlotNumber[1],
                                gw='192.168.31.200',
                                odid='11',
                                netmask='255.255.255.0',
                                ipMethod='STATIC',
                                dns='8.8.8.8',
                                ip_addr='192.168.31.1',
                                enabled=True, )

        secondCardGroup = Card(id=envVars.atipSlotNumber[2],
                                 gw='192.168.31.200',
                                 odid='11',
                                 netmask='255.255.255.0',
                                 ipMethod='STATIC',
                                 dns='8.8.8.8',
                                 ip_addr='192.168.31.2',
                                 enabled=True, )

        firstCardGroup1 = Card(id=envVars.atipSlotNumber[0],
                               gw='192.168.31.200',
                               odid='11',
                               netmask='255.255.255.0',
                               ipMethod='STATIC',
                               dns='8.8.8.8',
                               ip_addr='192.168.31.3',
                               enabled=True, )
    else:
        firstCardGroup1 = Card(id=envVars.atipSlotNumber[0],
                               gw='192.168.31.200',
                               odid='11',
                               netmask='255.255.255.0',
                               ipMethod='STATIC',
                               dns='8.8.8.8',
                               ip_addr='192.168.31.3',
                               enabled=True, )

    # firstCollectorGroupIP = envVars.netflowCollectorConnectionConfig.host
    firstCollectorGroupIP = '192.168.31.100'
    secondCollectorGroupIP = str(ipaddress.IPv4Address(firstCollectorGroupIP) + 1)
    thirdCollectorGroupIP = str(ipaddress.IPv4Address(firstCollectorGroupIP) + 2)
    firstCollectorGroup = Collector(0, 100, 'Samples', True, 'UDP', 4739, firstCollectorGroupIP)
    secondCollectorGroup = Collector(1, 100, 'Samples', True, 'UDP', 4739, secondCollectorGroupIP)
    thirdCollectorGroup = Collector(2, 100, 'Samples', True, 'UDP', 4739, thirdCollectorGroupIP)

    #firstCollectorGroupIPvATIP = '192.168.31.100'
    firstCollectorGroupIPvATIP = envVars.netflowCollectorConnectionConfig.host
    secondCollectorGroupIPvATIP = str(ipaddress.IPv4Address(firstCollectorGroupIPvATIP) + 1)
    thirdCollectorGroupIPvATIP = str(ipaddress.IPv4Address(firstCollectorGroupIPvATIP) + 3)
    firstCollectorGroupvATIP = Collector(0, 100, 'Samples', True, 'UDP', 4739, firstCollectorGroupIPvATIP)
    secondCollectorGroupvATIP = Collector(1, 100, 'Samples', True, 'UDP', 4739, secondCollectorGroupIPvATIP)
    thirdCollectorGroupvATIP = Collector(2, 100, 'Samples', True, 'UDP', 4739, thirdCollectorGroupIPvATIP)

    Filter1Group = FilterConfig("Filter1Group")
    FilterConditionConfig.addGeoCondition(Filter1Group, GeoLocation.CHINA)
    Filter1Group.netflow = True
    Filter1Group.collectors = [2]


    netflowGroup1CfgDevices= NetflowConfig()
    netflowGroup1CfgDevices.enabled = True
    # GEO
    netflowGroup1CfgDevices.netflowL7SrcLongitude = False
    netflowGroup1CfgDevices.netflowL7SrcCityName = False
    netflowGroup1CfgDevices.netflowL7SrcRegionName = False
    netflowGroup1CfgDevices.netflowL7SrcCountryCode = False
    netflowGroup1CfgDevices.netflowL7SrcRegionCode = False
    netflowGroup1CfgDevices.netflowL7SrcASName = False
    netflowGroup1CfgDevices.netflowL7DstRegionName = False
    netflowGroup1CfgDevices.netflowL7DstCityName = False
    netflowGroup1CfgDevices.netflowL7DstLatitude = False
    netflowGroup1CfgDevices.netflowL7DstCountryName = False
    netflowGroup1CfgDevices.netflowL7SrcLatitude = False
    netflowGroup1CfgDevices.netflowL7DstLongitude = False
    netflowGroup1CfgDevices.netflowL7DstRegionCode = False
    netflowGroup1CfgDevices.netflowL7SrcCountryName = False
    netflowGroup1CfgDevices.netflowL7DstCountryCode = False
    netflowGroup1CfgDevices.netflowL7DstASName = False
    # APP
    netflowGroup1CfgDevices.netflowL7URI = False
    netflowGroup1CfgDevices.netflowL7DNSClass = False
    netflowGroup1CfgDevices.netflowL7AppID = False
    netflowGroup1CfgDevices.netflowL7DNSHostname = False
    netflowGroup1CfgDevices.netflow_l7 = True
    netflowGroup1CfgDevices.netflowL7Domain = False
    netflowGroup1CfgDevices.netflowL7AppName = False
    netflowGroup1CfgDevices.netflowL7Latency = False
    netflowGroup1CfgDevices.netflowL7DNSText = False
    netflowGroup1CfgDevices.netflowL7UserAgent = False
    netflowGroup1CfgDevices.netflowL7DNSHostnameQuery = False
    netflowGroup1CfgDevices.netflowL7DNSHostnameResponse = False
    # DEVICE
    netflowGroup1CfgDevices.netflowL7DeviceId = True
    netflowGroup1CfgDevices.netflowL7BrowserId = True
    netflowGroup1CfgDevices.netflowL7BrowserName = True
    netflowGroup1CfgDevices.netflowL7DeviceName = True
    # SSL
    netflowGroup1CfgDevices.netflowL7EncryptionCipher = False
    netflowGroup1CfgDevices.netflowL7EncryptionKeyLen = False
    netflowGroup1CfgDevices.netflowL7ConnEncryptType = False

    FilterEspnCollecotr2 = FilterConfig("FilterEspnCollecotr2")
    FilterConditionConfig.addAppCondition(FilterEspnCollecotr2, ApplicationType.ESPN)
    FilterEspnCollecotr2.netflow = True
    FilterEspnCollecotr2.collectors = [1]

    netflowgroup2cfgv9 = NetflowConfig()
    netflowgroup2cfgv9.enabled = True
    netflowgroup2cfgv9.version = 9

    FilterChinaCollecotr1 = FilterConfig("FilterChinaCollecotr1")
    FilterConditionConfig.addGeoCondition(FilterChinaCollecotr1, GeoLocation.CHINA)
    FilterChinaCollecotr1.netflow = True
    FilterChinaCollecotr1.collectors = [0]

    FilterItalyCollecotr2 = FilterConfig("FilterItalyCollecotr2")
    FilterConditionConfig.addGeoCondition(FilterItalyCollecotr2, GeoLocation.ITALY)
    FilterItalyCollecotr2.netflow = True
    FilterItalyCollecotr2.collectors = [1]

























