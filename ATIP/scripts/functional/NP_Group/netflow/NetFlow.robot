*** Settings ***
Documentation  Test groups functionality

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.webApi.atip.Netflow
Library	   NetFlowLib.py
Library    Telnet   30 seconds
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    Collections
Library    atipAutoFrwk.services.atip.utility.IxFlowParsing
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    BuiltIn
Library    atipAutoFrwk.webApi.atip.Netflow
Library    String
Library    atipAutoFrwk.services.TestCondition
Library    atipAutoFrwk.webApi.atip.EngineInfo
Library    atipAutoFrwk.services.atip.utility.IxFlowParsing
Library    atipAutoFrwk.config.atip.NetflowConfig
Library    atipAutoFrwk.services.atip.NetflowService
Library    atipAutoFrwk.webApi.atip.Netflow
Library    atipAutoFrwk.services.atip.NPService


Resource   scripts/functional/netflow/NetflowUtils.robot



Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP And Login
Test Teardown      Close All Connections
Suite Teardown     logout  ${atipSession}


*** Test Cases ***

TC001160851 Defalut Group and 1 New Group with different Netflow Configuration and Filters (Config_MoveNP)
    [Tags]  hardware
    [Setup]  Clear Config

    log to console  TC001160851 Configuring NetFLow on Default Group
    Add GroupId to NetFlow Config  ${atipSession}  ${netflowdefaultcfg}  ${defaultG}
    updateGlobalConfig  ${atipSession}  ${netflowdefaultcfg}
    Configure NetFlow Exporter Cards  ${atipSession}  ${firstCardDefault}  ${defaultG}
    Configure NetFlow Exporter Cards  ${atipSession}  ${secondCardDefault}  ${defaultG}
    Configure NetFlow Collectors  ${atipSession}  ${firstCollectorDefault}  ${defaultG}
    Configure NetFlow Collectors  ${atipSession}  ${secondCollectorDefault}  ${defaultG}
    Create Filters  ${atipSession}  ${filter1DefaultGroup}  ${defaultG}

    log to console  TC001160851 Verifying per NP rules on Default Group
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules}
    Validate Rules Number Per NP  ${atipSession}  ${npId2}  ${expRules}

    log to console  TC001160851 Running BPS raffic and capturing netflow for Default Group
    ${testId1} =  Set variable  TC001160851_default
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId1}  ${bpsTestConfig}

    log to console  TC001160851 Validating that only enabled ixflow fields are present in the capture for Default Group
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.hostname,cflow.pie.ixia.l7-application-id,cflow.pie.ixia.dns-question-names,cflow.pie.ixia.http-uri,cflow.pie.ixia.l7-application-name,cflow.pie.ixia.dns-question-names,cflow.pie.ixia.http-user-agent,cflow.pie.ixia.dns-txt,cflow.pie.ixia.dns-classes"
    @{informElemToContain} =  CREATE LIST  httpHostName  l7appID  dnsQuestionNames  uri  l7appName  dnsQuestionNames  userAgent  dnsTxt  dnsClasses
    Validate Information Elements  ${testId1}  ${firstCollectorDefaultIP}  ${informElemToContain}
    Validate Information Elements  ${testId1}  ${secondCollectorDefaultIP}  ${informElemToContain}

    log to console  TC001160851 Capture Split based on the NP Exporter IP for Default Group
    Extract capture Per NP Exporter IP  ${testId1}  ${firstCardDefault.ip_addr}
    Extract capture Per NP Exporter IP  ${testId1}  ${secondCardDefault.ip_addr}
    Log  ${firstCardDefault.ip_addr}

    log to console  TC001160851 Reading records stats per NP slot and collector for Default Group
    ${recordsFirstNPCollecotr1Default} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorDefaultIP}  ${npId1}
    Log  ${recordsFirstNPCollecotr1Default}
    ${recordsFirstNPCollecotr2Default} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorDefaultIP}  ${npId1}
    Log  ${recordsFirstNPCollecotr2Default}
    ${recordsSecondNPCollecotr1Default} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorDefaultIP}  ${npId2}
    Log  ${recordsSecondNPCollecotr1Default}
    ${recordsSecondNPCollecotr2Default} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorDefaultIP}  ${npId2}
    Log  ${recordsSecondNPCollecotr2Default}

    log to console  TC001160851 Comparing capture values with records stats for Exporter 1 for Default Group
    ${responseFirstCardDefault}=  Execute Command  python parsing.py ${testId1}_${firstCardDefault.ip_addr}.pcap ${testId1}_${firstCardDefault.ip_addr}.csv ${ixFlowFilters}
    ${dict1}=  parseOutput  ${responseFirstCardDefault}
    Log  ${dict1}
    ${expectedValue1} =  evaluate  ${recordsFirstNPCollecotr1Default}*2
    Log  ${expectedValue1}
    ${value1} = 	Get From Dictionary 	${dict1}  facebook
    Log  ${value1}
    should be equal as integers  ${expectedValue1}  ${value1}
    ${expectedValue2} =  evaluate  ${recordsFirstNPCollecotr2Default}-${recordsFirstNPCollecotr1Default}
    Log  ${expectedValue2}
    ${value2} = 	Get From Dictionary 	${dict1}  espn
    Log  ${value2}
    should be equal as integers  ${expectedValue2}  ${value2}

    log to console  TC001160851 Comparing capture values with records stats for Exporter 2 for Default Group
    ${responseSecondCardDefault}=  Execute Command  python parsing.py ${testId1}_${secondCardDefault.ip_addr}.pcap ${testId1}_${secondCardDefault.ip_addr}.csv ${ixFlowFilters}
    ${dict2}=  parseOutput  ${responseSecondCardDefault}
    Log  ${dict2}
    ${value3} = 	Get From Dictionary 	${dict2}  aol
    Log  ${value3}
    should be equal as integers  ${recordsSecondNPCollecotr1Default}  0
    should be equal as integers  ${recordsSecondNPCollecotr2Default}  ${value3}

    log to console   TC001160851 removing files from Linux Box
    Execute Command  rm ${testId1}*

    log to console  TC001160851 Configuring NetFLow on Group1
    createNewGroup  ${atipSession}  ${gName1}
    Add GroupId to NetFlow Config  ${atipSession}  ${netflowgroupcfg}  ${gName1}
    updateGlobalConfig  ${atipSession}  ${netflowgroupcfg}
    Configure NetFlow Collectors  ${atipSession}  ${firstCollectorGroup}  ${gName1}
    Configure NetFlow Collectors  ${atipSession}  ${secondCollectorGroup}  ${gName1}
    Configure NetFlow Collectors  ${atipSession}  ${thirdCollectorGroup}  ${gName1}
    Create Filters  ${atipSession}  ${Filter1Group}  ${gName1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId2}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId3}
    Configure NetFlow Exporter Cards  ${atipSession}  ${firstCardGroup}  ${gName1}
    Configure NetFlow Exporter Cards  ${atipSession}  ${secondCardGroup}  ${gName1}

    log to console  TC001160851 Verifying per NP rules on Group1
    Validate Rules Number Per NP  ${atipSession}  ${npId2}  ${expRules}
    Validate Rules Number Per NP  ${atipSession}  ${npId3}  ${expRules}

    log to console  TC001160851 Running BPS raffic and capturing netflow for New Group
    ${testId2} =  Set variable  TC001160851_Group1
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId2}  ${bpsTestConfig2}

    log to console  TC001160851 Validating that only enabled ixflow fields are present in the capture for Group1
    ${ixFlowFiltersGroup} =  Set variable  "cflow.pie.ixia.source-ip-country-code,cflow.pie.ixia.source-ip-country-name,cflow.pie.ixia.source-ip-region-code,cflow.pie.ixia.source-ip-region-name,cflow.pie.ixia.source-ip-city-name,cflow.pie.ixia.source-ip-latitude,cflow.pie.ixia.source-ip-longitude,cflow.pie.ixia.destination-ip-country-code,cflow.pie.ixia.destination-ip-country-name,cflow.pie.ixia.source-ip-region-code,cflow.pie.ixia.source-ip-region-name,cflow.pie.ixia.destination-ip-city-name,cflow.pie.ixia.destination-ip-latitude,cflow.pie.ixia.destination-ip-longitude"
    @{informElemToContainGroup} =  CREATE LIST  srcCountryCode  srcCountryName  srcRegionCode  srcRegionName  srcCityName  srcLatitude  srcLongitude  dstCountryCode  dstCountryName  dstRegionCode  dstRegionName  dstCityName  dstLatitude  dstLongitude
    Validate Information Elements  ${testId2}  ${firstCollectorGroupIP}  ${informElemToContainGroup}
    Validate Information Elements  ${testId2}  ${secondCollectorGroupIP}  ${informElemToContainGroup}
    Validate Information Elements  ${testId2}  ${thirdCollectorGroupIP}  ${informElemToContainGroup}

    log to console  TC001160851 Capture Split based on the NP Exporter IP for Group1
    Extract capture Per NP Exporter IP  ${testId2}  ${firstCardGroup.ip_addr}
    Extract capture Per NP Exporter IP  ${testId2}  ${secondCardGroup.ip_addr}

    log to console  TC001160851 Reading records stats per NP slot and collector for Group1
    ${groupId}=  getGroupId  ${atipSession}  ${gName1}
    ${recordsFirstNPCollecotr1Group} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorGroupIP}  ${npId2}  ${groupId}
    Log  ${recordsFirstNPCollecotr1Group}
    ${recordsFirstNPCollecotr2Group} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorGroupIP}  ${npId2}  ${groupId}
    Log  ${recordsFirstNPCollecotr2Group}
    ${recordsFirstNPCollecotr3Group} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${thirdCollectorGroupIP}  ${npId2}  ${groupId}
    Log  ${recordsFirstNPCollecotr3Group}
    ${recordsSecondNPCollecotr1Group} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorGroupIP}  ${npId3}  ${groupId}
    Log  ${recordsSecondNPCollecotr1Group}
    ${recordsSecondNPCollecotr2Group} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorGroupIP}  ${npId3}  ${groupId}
    Log  ${recordsSecondNPCollecotr2Group}
     ${recordsSecondNPCollecotr3Group} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${thirdCollectorGroupIP}  ${npId3}  ${groupId}
    Log  ${recordsSecondNPCollecotr3Group}

    log to console  TC001160851 Comparing capture values with records stats for Exporter 1 for Group1
    ${responseFirstCardGroup}=  Execute Command  python parsing.py ${testId2}_${firstCardGroup.ip_addr}.pcap ${testId2}_${firstCardGroup.ip_addr}.csv ${ixFlowFiltersGroup}
    ${dict3}=  parseOutput  ${responseFirstCardGroup}
    Log  ${dict3}
    ${value5} = 	Get From Dictionary 	${dict3}  Italy
    Log  ${value5}
    ${value6} = 	Get From Dictionary 	${dict3}  China
    Log  ${value6}
    ${italyRecords}=  evaluate  ${value5}/2
    ${chinaRecord}=  evaluate  ${value6}/3
    ${chinaItalySum}=  evaluate  ${italyRecords}+${chinaRecord}
    should be equal as integers  ${recordsFirstNPCollecotr1Group}  ${chinaItalySum}
    should be equal as integers  ${recordsFirstNPCollecotr2Group}  ${chinaItalySum}
    should be equal as integers  ${recordsFirstNPCollecotr3Group}  ${chinaRecord}

    log to console  TC001160851 Comparing capture values with records stats for Exporter 2 for Group1
    ${responseSecondCardGroup}=  Execute Command  python parsing.py ${testId2}_${secondCardGroup.ip_addr}.pcap ${testId2}_${secondCardGroup.ip_addr}.csv ${ixFlowFiltersGroup}
    ${dict4}=  parseOutput  ${responseSecondCardGroup}
    Log  ${dict4}
    ${value7} = 	Get From Dictionary 	${dict4}  Thailand
    ${thailandRecords}=  evaluate  ${value7}/2
    Log  ${value7}
    should be equal as integers  ${recordsSecondNPCollecotr1Group}  ${thailandRecords}
    should be equal as integers  ${recordsSecondNPCollecotr2Group}  ${thailandRecords}
    should be equal as integers  ${recordsSecondNPCollecotr3Group}  0

    log to console   TC001160851 removing files from Linux Box
    Execute Command  rm ${testId2}*

    log to console  TC001160851 Revalidate steps for the Default Group with the remaining NP.
    ${testId3} =  Set variable  TC001160851_default_bis
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId3}  ${bpsTestConfig}

    log to console  TC001160851 Verifying per NP rules on Default Group
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules}

    log to console  TC001160851 Validate enabled ixflow exists in capture per collector for all NPs on Default Group
    Validate Information Elements  ${testId3}  ${firstCollectorDefaultIP}  ${informElemToContain}
    Validate Information Elements  ${testId3}  ${secondCollectorDefaultIP}  ${informElemToContain}

    log to console  TC001160851 Split capture per NP Exporter IP  on Default Group
    Extract capture Per NP Exporter IP  ${testId3}  ${firstCardDefault.ip_addr}
    Log  ${firstCardDefault.ip_addr}

    log to console  TC001160851 Reading records stats per NP slot and collector for Default Group
    ${recordsFirstNPCollecotr1Default1} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorDefaultIP}  ${npId1}
    Log  ${recordsFirstNPCollecotr1Default1}
    ${recordsFirstNPCollecotr2Default1} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorDefaultIP}  ${npId1}
    Log  ${recordsFirstNPCollecotr2Default1}

    log to console  TC001160851 Comparing capture values with records stats for Exporter 1 Default Group
    ${responseFirstCardDefault1}=  Execute Command  python parsing.py ${testId3}_${firstCardDefault.ip_addr}.pcap ${testId3}_${firstCardDefault.ip_addr}.csv ${ixFlowFilters}
    ${dict5}=  parseOutput  ${responseFirstCardDefault1}
    Log  ${dict5}
    ${expectedValue5} =  evaluate  ${recordsFirstNPCollecotr1Default1}*2
    Log  ${expectedValue5}
    ${value8} = 	Get From Dictionary 	${dict5}  facebook
    Log  ${value8}
    should be equal as integers  ${expectedValue5}  ${value8}
    ${expectedValue6} =  evaluate  ${recordsFirstNPCollecotr2Default1}-${recordsFirstNPCollecotr1Default1}
    Log  ${expectedValue6}
    ${value9} = 	Get From Dictionary 	${dict5}  espn
    Log  ${value9}
    should be equal as integers  ${expectedValue6}  ${value9}

    log to console   TC001160851 removing files from Linux Box
    Execute Command  rm ${testId3}*

TC001160852 Defalut Group and 1 New Group with different Netflow Configuration and Filters (MoveNP_Config)
    [Tags]  hardware
    [Setup]  Clear Config

    log to console  TC001160852 Configuring NetFLow on Default Group
    Add GroupId to NetFlow Config  ${atipSession}  ${netflowdefaultcfg}  ${defaultG}
    updateGlobalConfig  ${atipSession}  ${netflowdefaultcfg}
    Configure NetFlow Exporter Cards  ${atipSession}  ${firstCardDefault}  ${defaultG}
    Configure NetFlow Exporter Cards  ${atipSession}  ${secondCardDefault}  ${defaultG}
    Configure NetFlow Collectors  ${atipSession}  ${firstCollectorDefault}  ${defaultG}
    Configure NetFlow Collectors  ${atipSession}  ${secondCollectorDefault}  ${defaultG}
    Create Filters  ${atipSession}  ${filter1DefaultGroupAol}  ${defaultG}

    log to console  TC001160852 Verifying per NP rules on Default Group
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules}
    Validate Rules Number Per NP  ${atipSession}  ${npId2}  ${expRules}

    log to console  TC001160852 Running BPS raffic and capturing netflow for Default Group
    ${testId4} =  Set variable  TC001160852_default
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId4}  ${bpsTestConfig3}

    log to console  TC001160852 Validating that only enabled ixflow fields are present in the capture for Default Group
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.hostname,cflow.pie.ixia.l7-application-id,cflow.pie.ixia.dns-question-names,cflow.pie.ixia.http-uri,cflow.pie.ixia.l7-application-name,cflow.pie.ixia.dns-question-names,cflow.pie.ixia.http-user-agent,cflow.pie.ixia.dns-txt,cflow.pie.ixia.dns-classes"
    @{informElemToContain} =  CREATE LIST  httpHostName  l7appID  dnsQuestionNames  uri  l7appName  dnsQuestionNames  userAgent  dnsTxt  dnsClasses
    Validate Information Elements  ${testId4}  ${firstCollectorDefaultIP}  ${informElemToContain}
    Validate Information Elements  ${testId4}  ${secondCollectorDefaultIP}  ${informElemToContain}

    log to console  TC001160852 Capture Split based on the NP Exporter IP for Default Group
    Extract capture Per NP Exporter IP  ${testId4}  ${firstCardDefault.ip_addr}
    Extract capture Per NP Exporter IP  ${testId4}  ${secondCardDefault.ip_addr}

    log to console  TC001160852 Reading records stats per NP slot and collector for Default Group
    ${recordsFirstNPCollecotr1Default2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorDefaultIP}  ${npId1}
    Log  ${recordsFirstNPCollecotr1Default2}
    ${recordsFirstNPCollecotr2Default2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorDefaultIP}  ${npId1}
    Log  ${recordsFirstNPCollecotr2Default2}
    ${recordsSecondNPCollecotr1Default2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorDefaultIP}  ${npId2}
    Log  ${recordsSecondNPCollecotr1Default2}
    ${recordsSecondNPCollecotr2Default2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorDefaultIP}  ${npId2}
    Log  ${recordsSecondNPCollecotr2Default2}

    log to console  TC001160852 Comparing capture values with records stats for Exporter 1 for Default Group
    ${responseFirstCardDefault2}=  Execute Command  python parsing.py ${testId4}_${firstCardDefault.ip_addr}.pcap ${testId4}_${firstCardDefault.ip_addr}.csv ${ixFlowFilters}
    ${dict6}=  parseOutput  ${responseFirstCardDefault2}
    Log  ${dict6}
    ${value10} = 	Get From Dictionary 	${dict6}  facebook
    Log  ${value10}
    should be equal as integers  ${recordsFirstNPCollecotr1Default2}  ${value10}
    should be equal as integers  ${recordsFirstNPCollecotr2Default2}  0

    log to console  TC001160852 Comparing capture values with records stats for Exporter 2 for Default Group
    ${responseSecondCardDefault2}=  Execute Command  python parsing.py ${testId4}_${secondCardDefault.ip_addr}.pcap ${testId4}_${secondCardDefault.ip_addr}.csv ${ixFlowFilters}
    ${dict7}=  parseOutput  ${responseSecondCardDefault2}
    Log  ${dict6}
    ${value11} = 	Get From Dictionary 	${dict7}  aol
    ${aolRec}=  evaluate  ${value11}/2
    Log  ${aolRec}
    ${espnRec} = 	Get From Dictionary 	${dict7}  espn
    Log  ${espnRec}
    ${espnaolRec}=  evaluate  ${aolRec}+${espnRec}
    should be equal as integers  ${recordsSecondNPCollecotr1Default2}  ${espnaolRec}
    should be equal as integers  ${recordsSecondNPCollecotr2Default2}  ${aolRec}

    log to console   TC001160852 removing files from Linux Box
    Execute Command  rm ${testId4}*

    log to console  TC001160852 Configuring NetFLow on Group1
    createNewGroup  ${atipSession}  ${gName1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId2}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId3}
    Add GroupId to NetFlow Config  ${atipSession}  ${netflowgroupcfg}  ${gName1}
    updateGlobalConfig  ${atipSession}  ${netflowgroupcfg}
    Configure NetFlow Collectors  ${atipSession}  ${firstCollectorGroup}  ${gName1}
    Configure NetFlow Collectors  ${atipSession}  ${secondCollectorGroup}  ${gName1}
    Configure NetFlow Collectors  ${atipSession}  ${thirdCollectorGroup}  ${gName1}
    Configure NetFlow Exporter Cards  ${atipSession}  ${firstCardGroup}  ${gName1}
    Configure NetFlow Exporter Cards  ${atipSession}  ${secondCardGroup}  ${gName1}
    Create Filters  ${atipSession}  ${Filter1Group}  ${gName1}

    log to console  TC001160852 Verifying per NP rules on Group1
    Validate Rules Number Per NP  ${atipSession}  ${npId2}  ${expRules}
    Validate Rules Number Per NP  ${atipSession}  ${npId3}  ${expRules}

    log to console  TC001160852 Running BPS raffic and capturing netflow for Group1
    ${testId5} =  Set variable  TC001160852_Group1
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId5}  ${bpsTestConfig2}

    log to console  TC001160852 Validating that only enabled ixflow fields are present in the capture for Group1
    ${ixFlowFiltersGroup} =  Set variable  "cflow.pie.ixia.source-ip-country-code,cflow.pie.ixia.source-ip-country-name,cflow.pie.ixia.source-ip-region-code,cflow.pie.ixia.source-ip-region-name,cflow.pie.ixia.source-ip-city-name,cflow.pie.ixia.source-ip-latitude,cflow.pie.ixia.source-ip-longitude,cflow.pie.ixia.destination-ip-country-code,cflow.pie.ixia.destination-ip-country-name,cflow.pie.ixia.source-ip-region-code,cflow.pie.ixia.source-ip-region-name,cflow.pie.ixia.destination-ip-city-name,cflow.pie.ixia.destination-ip-latitude,cflow.pie.ixia.destination-ip-longitude"
    @{informElemToContainGroup} =  CREATE LIST  srcCountryCode  srcCountryName  srcRegionCode  srcRegionName  srcCityName  srcLatitude  srcLongitude  dstCountryCode  dstCountryName  dstRegionCode  dstRegionName  dstCityName  dstLatitude  dstLongitude
    Validate Information Elements  ${testId5}  ${firstCollectorGroupIP}  ${informElemToContainGroup}
    Validate Information Elements  ${testId5}  ${secondCollectorGroupIP}  ${informElemToContainGroup}
    Validate Information Elements  ${testId5}  ${thirdCollectorGroupIP}  ${informElemToContainGroup}

    log to console  TC001160852 Capture Split based on the NP Exporter IP for for Group1
    Extract capture Per NP Exporter IP  ${testId5}  ${firstCardGroup.ip_addr}
    Extract capture Per NP Exporter IP  ${testId5}  ${secondCardGroup.ip_addr}

    log to console  TC001160852 Reading records stats per NP slot and collector for Group1
    ${groupId}=  getGroupId  ${atipSession}  ${gName1}
    ${recordsFirstNPCollecotr1Group2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorGroupIP}  ${npId2}  ${groupId}
    Log  ${recordsFirstNPCollecotr1Group2}
    ${recordsFirstNPCollecotr2Group2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorGroupIP}  ${npId2}  ${groupId}
    Log  ${recordsFirstNPCollecotr2Group2}
    ${recordsFirstNPCollecotr3Group2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${thirdCollectorGroupIP}  ${npId2}  ${groupId}
    Log  ${recordsFirstNPCollecotr3Group2}
    ${recordsSecondNPCollecotr1Group2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorGroupIP}  ${npId3}  ${groupId}
    Log  ${recordsSecondNPCollecotr1Group2}
    ${recordsSecondNPCollecotr2Group2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorGroupIP}  ${npId3}  ${groupId}
    Log  ${recordsSecondNPCollecotr2Group2}
     ${recordsSecondNPCollecotr3Group2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${thirdCollectorGroupIP}  ${npId3}  ${groupId}
    Log  ${recordsSecondNPCollecotr3Group2}

    log to console  TC001160852 Comparing capture values with records stats for Exporter 1 for Group1
    ${responseFirstCardGroup2}=  Execute Command  python parsing.py ${testId5}_${firstCardGroup.ip_addr}.pcap ${testId5}_${firstCardGroup.ip_addr}.csv ${ixFlowFiltersGroup}
    ${dict8}=  parseOutput  ${responseFirstCardGroup2}
    Log  ${dict8}
    ${value10} = 	Get From Dictionary 	${dict8}  Italy
    Log  ${value10}
    ${value11} = 	Get From Dictionary 	${dict8}  China
    Log  ${value11}
    ${italyRecords}=  evaluate  ${value10}/2
    ${chinaRecord}=  evaluate  ${value11}/3
    ${chinaItalySum}=  evaluate  ${italyRecords}+${chinaRecord}
    should be equal as integers  ${recordsFirstNPCollecotr1Group2}  ${chinaItalySum}
    should be equal as integers  ${recordsFirstNPCollecotr2Group2}  ${chinaItalySum}
    should be equal as integers  ${recordsFirstNPCollecotr3Group2}  ${chinaRecord}

    log to console  TC001160852 Comparing capture values with records stats for Exporter 2 for Group1
    ${responseSecondCardGroup2}=  Execute Command  python parsing.py ${testId5}_${secondCardGroup.ip_addr}.pcap ${testId5}_${secondCardGroup.ip_addr}.csv ${ixFlowFiltersGroup}
    ${dict9}=  parseOutput  ${responseSecondCardGroup2}
    Log  ${dict9}
    ${value13} = 	Get From Dictionary 	${dict9}  Thailand
    ${thailandRecords2}=  evaluate  ${value13}/2
    Log  ${value13}
    should be equal as integers  ${recordsSecondNPCollecotr1Group2}  ${thailandRecords2}
    should be equal as integers  ${recordsSecondNPCollecotr2Group2}  ${thailandRecords2}
    should be equal as integers  ${recordsSecondNPCollecotr3Group2}  0

    log to console   TC001160852 removing files from Linux Box
    Execute Command  rm ${testId4}*

    log to console  TC001160852 Re-validate steps for the Default Group with the remaining NP.
    ${testId6} =  Set variable  TC001160852_default_bis
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId6}  ${bpsTestConfig3}

    log to console  TC001160852  Validate Rules Number Per NP for the Default Group
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules}

    log to console  TC001160852 Validating that only enabled ixflow fields are present in the capture for Default Group
    Validate Information Elements  ${testId6}  ${firstCollectorDefaultIP}  ${informElemToContain}
    Validate Information Elements  ${testId6}  ${secondCollectorDefaultIP}  ${informElemToContain}

    log to console  TC001160852 Capture Split based on the NP Exporter IP for Default Group
    Extract capture Per NP Exporter IP  ${testId6}  ${firstCardDefault.ip_addr}
    Log  ${firstCardDefault.ip_addr}

    log to console  TC001160852 Reading records stats per NP slot and collector for Default Group
    ${recordsFirstNPCollecotr1Default3} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorDefaultIP}  ${npId1}
    Log  ${recordsFirstNPCollecotr1Default3}
    ${recordsFirstNPCollecotr2Default3} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorDefaultIP}  ${npId1}
    Log  ${recordsFirstNPCollecotr2Default3}

    log to console  TC001160852 Comparing capture values with records stats for Exporter 1 for Default Group
    ${responseFirstCardDefault3}=  Execute Command  python parsing.py ${testId6}_${firstCardDefault.ip_addr}.pcap ${testId6}_${firstCardDefault.ip_addr}.csv ${ixFlowFilters}
    ${dict10}=  parseOutput  ${responseFirstCardDefault3}
    Log  ${dict10}
    ${value12} = 	Get From Dictionary 	${dict10}  facebook
    Log  ${value12}
    should be equal as integers  ${recordsFirstNPCollecotr1Default3}  ${value12}
    should be equal as integers  ${recordsFirstNPCollecotr2Default3}  0

    log to console   TC001160852 removing files from Linux Box
    Execute Command  rm ${testId6}*

TC001160853 2xNew Groups with multiple NPs per group with different Netflow Configuration and Filters (Config_MoveNP)
    [Tags]  hardware
    [Setup]  Clear Config

    log to console  TC001160853 Configuring NetFLow on Group1
    createNewGroup  ${atipSession}  ${gName1}
    Add GroupId to NetFlow Config  ${atipSession}  ${netflowGroup1CfgDevices}  ${gName1}
    updateGlobalConfig  ${atipSession}  ${netflowGroup1CfgDevices}
    Configure NetFlow Collectors  ${atipSession}  ${firstCollectorDefault}  ${gName1}
    Configure NetFlow Collectors  ${atipSession}  ${secondCollectorDefault}  ${gName1}
    Configure NetFlow Collectors  ${atipSession}  ${thirdCollectorDefault}  ${gName1}
    Create Filters  ${atipSession}  ${FilterEspnCollecotr2}  ${gName1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId2}
    Configure NetFlow Exporter Cards  ${atipSession}  ${firstCardDefault}  ${gName1}
    Configure NetFlow Exporter Cards  ${atipSession}  ${secondCardDefault}  ${gName1}

    log to console  TC001160853 Verifying per NP rules on Group1
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules}
    Validate Rules Number Per NP  ${atipSession}  ${npId2}  ${expRules}

    log to console  TC001160853 Run BPS and capture traffic on Group1
    ${testId8} =  Set variable  TC001160853_Group1
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId8}  ${bpsTestConfig}

    log to console  TC001160853 Validating that only enabled ixflow fields are present in the capture for Group1
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.os-device-id,cflow.pie.ixia.os-device-name,cflow.pie.ixia.browser-id,cflow.pie.ixia.browser-name"
    @{informElemToContain} =  CREATE LIST  deviceId  deviceName  browserId  browserName
    Validate Information Elements  ${testId8}  ${firstCollectorDefaultIP}  ${informElemToContain}
    Validate Information Elements  ${testId8}  ${secondCollectorDefaultIP}  ${informElemToContain}

    log to console  TC001160853 Capture Split based on the NP Exporter IP for Group1
    Extract capture Per NP Exporter IP  ${testId8}  ${firstCardDefault.ip_addr}
    Extract capture Per NP Exporter IP  ${testId8}  ${secondCardDefault.ip_addr}

    log to console  TC001160853 Reading records stats per NP slot and collector for Group1
    ${groupId4}=  getGroupId  ${atipSession}  ${gName1}
    ${recordsFirstNPCollecotr1Default4} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorDefaultIP}  ${npId1}  ${groupId4}
    Log  ${recordsFirstNPCollecotr1Default4}
    ${recordsFirstNPCollecotr2Default4} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorDefaultIP}  ${npId1}  ${groupId4}
    Log  ${recordsFirstNPCollecotr2Default4}
    ${recordsFirstNPCollecotr3Default4} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${thirdCollectorDefaultIP}  ${npId1}  ${groupId4}
    Log  ${recordsFirstNPCollecotr3Default4}
    ${recordsSecondNPCollecotr1Default4} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorDefaultIP}  ${npId2}  ${groupId4}
    Log  ${recordsSecondNPCollecotr1Default4}
    ${recordsSecondNPCollecotr2Default4} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorDefaultIP}  ${npId2}  ${groupId4}
    Log  ${recordsSecondNPCollecotr2Default4}
    ${recordsSecondNPCollecotr3Default4} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${thirdCollectorDefaultIP}  ${npId2}  ${groupId4}
    Log  ${recordsSecondNPCollecotr3Default4}

    log to console  TC001160853 Comparing capture values with records stats for Exporter 1 for Group1
    ${responseFirstCardDefault4}=  Execute Command  python parsing.py ${testId8}_${firstCardDefault.ip_addr}.pcap ${testId8}_${firstCardDefault.ip_addr}.csv ${ixFlowFilters}
    ${dict14}=  parseOutput  ${responseFirstCardDefault4}
    Log  ${dict14}
    ${macosRec} = 	Get From Dictionary 	${dict14}  MacOS
    Log  ${macosRec}
    ${linuxRec} = 	Get From Dictionary 	${dict14}  Linux
    Log  ${linuxRec}
    ${chromeRec} = 	Get From Dictionary 	${dict14}  Chrome
    Log  ${chromeRec}
    ${bonechoRec} = 	Get From Dictionary 	${dict14}  BonEcho
    Log  ${bonechoRec}
    ${expvalueos}=  evaluate  ${macosRec}/3+${linuxRec}/2
    Log  ${expvalueos}
    ${expvaluebw}=  evaluate  ${chromeRec}/3+${bonechoRec}/2
    Log  ${expvaluebw}
    ${expvaluemac}=  evaluate  ${macosRec}/3
    Log  ${expvaluebw}
    ${expvaluechrome}=  evaluate  ${chromeRec}/3
    Log  ${expvaluebw}
    should be equal as integers  ${recordsFirstNPCollecotr1Default4}  ${expvalueos}
    should be equal as integers  ${recordsFirstNPCollecotr1Default4}  ${expvaluebw}
    should be equal as integers  ${recordsFirstNPCollecotr2Default4}  ${expvaluemac}
    should be equal as integers  ${recordsFirstNPCollecotr2Default4}  ${expvaluechrome}
    should be equal as integers  ${recordsFirstNPCollecotr3Default4}  ${expvalueos}
    should be equal as integers  ${recordsFirstNPCollecotr3Default4}  ${expvaluebw}

    log to console  TC001160853 Comparing capture values with records stats for Exporter 2 for Group1
    ${responseSecondCardDefault4}=  Execute Command  python parsing.py ${testId8}_${secondCardDefault.ip_addr}.pcap ${testId8}_${secondCardDefault.ip_addr}.csv ${ixFlowFilters}
    ${dict15}=  parseOutput  ${responseSecondCardDefault4}
    Log  ${dict15}
    ${aolLinux} = 	Get From Dictionary 	${dict15}  Linux
    Log  ${aolLinux}
    ${aolSeamonkey} = 	Get From Dictionary 	${dict15}  SeaMonkey
    Log  ${aolSeamonkey}
    ${expAolLinux}=  evaluate  ${aolLinux}/2
    ${expAolSeaM}=  evaluate  ${aolSeamonkey}/2
    should be equal as integers  ${recordsSecondNPCollecotr1Default4}  ${expAolLinux}
    should be equal as integers  ${recordsSecondNPCollecotr1Default4}  ${expAolSeaM}
    should be equal as integers  ${recordsSecondNPCollecotr2Default4}  0
    should be equal as integers  ${recordsSecondNPCollecotr2Default4}  0
    should be equal as integers  ${recordsSecondNPCollecotr3Default4}  ${expAolLinux}
    should be equal as integers  ${recordsSecondNPCollecotr3Default4}  ${expAolSeaM}

    log to console   TC001160853 removing files from Linux Box
    Execute Command  rm ${testId8}*

    log to console  TC001160853 Configuring NetFLow on Group2
    createNewGroup  ${atipSession}  ${gName2}
    Add GroupId to NetFlow Config  ${atipSession}  ${netflowgroup2cfgv9}  ${gName2}
    updateGlobalConfig  ${atipSession}  ${netflowgroup2cfgv9}
    Configure NetFlow Collectors  ${atipSession}  ${firstCollectorGroup}  ${gName2}
    Configure NetFlow Collectors  ${atipSession}  ${secondCollectorGroup}  ${gName2}
    Configure NetFlow Exporter Cards  ${atipSession}  ${firstCardGroup}  ${gName2}
    Configure NetFlow Exporter Cards  ${atipSession}  ${secondCardGroup}  ${gName2}
    Create Filters  ${atipSession}  ${FilterChinaCollecotr1}  ${gName2}
    Create Filters  ${atipSession}  ${FilterItalyCollecotr2}  ${gName2}
    moveNPsToGroup  ${atipSession}  ${gName2}  ${npId2}
    moveNPsToGroup  ${atipSession}  ${gName2}  ${npId3}

    log to console  TC001160853 Verifying per NP rules on Group2
    Validate Rules Number Per NP  ${atipSession}  ${npId2}  ${expRules3}
    Validate Rules Number Per NP  ${atipSession}  ${npId3}  ${expRules3}

    log to console  TC001160853 Run BPS and capture traffic on Group2
    ${testId7} =  Set variable  TC001160853_Group2
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId7}  ${bpsTestConfig2}

    ${ixFlowFiltersGroupv9} =  Set variable  "cflow.protocol,cflow.inputint,cflow.outputint,cflow.srcas,cflow.dstas"

    log to console  TC001160853 Capture Split based on the NP Exporter IP on Group2
    Extract capture Per NP Exporter IP  ${testId7}  ${firstCardGroup.ip_addr}
    Extract capture Per NP Exporter IP  ${testId7}  ${secondCardGroup.ip_addr}

    log to console  TC001160853 Reading records stats per NP slot and collector for Group2
    ${groupId2}=  getGroupId  ${atipSession}  ${gName2}
    ${recordsFirstNPCollecotr1Group3} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorGroupIP}  ${npId2}  ${groupId2}
    Log  ${recordsFirstNPCollecotr1Group3}
    ${recordsFirstNPCollecotr2Group3} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorGroupIP}  ${npId2}  ${groupId2}
    Log  ${recordsFirstNPCollecotr2Group3}
    ${recordsSecondNPCollecotr1Group3} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorGroupIP}  ${npId3}  ${groupId2}
    Log  ${recordsSecondNPCollecotr1Group3}
    ${recordsSecondNPCollecotr2Group3} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorGroupIP}  ${npId3}  ${groupId2}
    Log  ${recordsSecondNPCollecotr2Group3}

    log to console  TC001160853 Comparing capture values with records stats for Exporter 1
    ${responseFirstCardDefault4}=  Execute Command  python parsing.py ${testId7}_${firstCardGroup.ip_addr}.pcap ${testId7}_${firstCardGroup.ip_addr}.csv ${ixFlowFiltersGroupv9}
    ${dict11}=  parseOutput  ${responseFirstCardDefault4}
    Log  ${dict11}
    ${italyAS} = 	Get From Dictionary 	${dict11}  30722
    Log  ${italyAS}
    ${chinaAS} = 	Get From Dictionary 	${dict11}  45083
    Log  ${chinaAS}
    ${tcpv9Rec} = 	Get From Dictionary 	${dict11}  6
    Log  ${tcpv9Rec}
    ${totalv9RecStats}=  evaluate  ${recordsFirstNPCollecotr1Group3}+${recordsFirstNPCollecotr2Group3}
    should be equal as integers  ${recordsFirstNPCollecotr1Group3}  ${chinaAS}
    should be equal as integers  ${recordsFirstNPCollecotr2Group3}  ${italyAS}
    should be equal as integers  ${recordsSecondNPCollecotr1Group3}  0
    should be equal as integers  ${recordsSecondNPCollecotr2Group3}  0
    should be equal as integers  ${totalv9RecStats}  ${tcpv9Rec}

    log to console   TC001160853 removing files from Linux Box
    Execute Command  rm ${testId7}*

TC001160859 Defalut Group and 1 New Group with different Netflow Configuration and Filters (Config_MoveNP) V1
    [Tags]  hardware
    [Setup]  Clear Config

    log to console  TC001160859 Configuring NetFLow on Default Group
    Add GroupId to NetFlow Config  ${atipSession}  ${netflowdefaultcfg}  ${defaultG}
    updateGlobalConfig  ${atipSession}  ${netflowdefaultcfg}
    Configure NetFlow Exporter Cards  ${atipSession}  ${firstCardDefault}  ${defaultG}
    Configure NetFlow Collectors  ${atipSession}  ${firstCollectorDefault}  ${defaultG}
    Configure NetFlow Collectors  ${atipSession}  ${secondCollectorDefault}  ${defaultG}
    Create Filters  ${atipSession}  ${filter1DefaultGroup}  ${defaultG}

    log to console  TC001160859 Verifying per NP rules on Default Group
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules}

    log to console  TC001160859 Running BPS raffic and capturing netflow for Default Group
    ${testId9} =  Set variable  TC001160859_Default
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId9}  ${bpsTestConfig4}

    log to console  TC001160859 Validating that only enabled ixflow fields are present in the capture for Default Group
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.hostname,cflow.pie.ixia.l7-application-id,cflow.pie.ixia.dns-question-names,cflow.pie.ixia.http-uri,cflow.pie.ixia.l7-application-name,cflow.pie.ixia.dns-question-names,cflow.pie.ixia.http-user-agent,cflow.pie.ixia.dns-txt,cflow.pie.ixia.dns-classes"
    @{informElemToContain} =  CREATE LIST  httpHostName  l7appID  dnsQuestionNames  uri  l7appName  dnsQuestionNames  userAgent  dnsTxt  dnsClasses
    Validate Information Elements  ${testId9}  ${firstCollectorDefaultIP}  ${informElemToContain}
    Validate Information Elements  ${testId9}  ${secondCollectorDefaultIP}  ${informElemToContain}

    log to console  TC001160859 Capture Split based on the NP Exporter IP fro Default Group
    Extract capture Per NP Exporter IP  ${testId9}  ${firstCardDefault.ip_addr}

    log to console  TC001160859 Reading records stats per NP slot and collector for Default Group
    ${recordsFirstNPCollecotr1Default5} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorDefaultIP}  ${npId1}
    Log  ${recordsFirstNPCollecotr1Default5}
    ${recordsFirstNPCollecotr2Default5} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorDefaultIP}  ${npId1}
    Log  ${recordsFirstNPCollecotr2Default5}

    log to console  TC001160859 Comparing capture values with records stats for Exporter for Default Group
    ${responseFirstCardDefault5}=  Execute Command  python parsing.py ${testId9}_${firstCardDefault.ip_addr}.pcap ${testId9}_${firstCardDefault.ip_addr}.csv ${ixFlowFilters}
    ${dict16}=  parseOutput  ${responseFirstCardDefault5}
    Log  ${dict16}
    ${facebookRec5} = 	Get From Dictionary 	${dict16}  facebook
    Log  ${facebookRec5}
    ${aolRec5} = 	Get From Dictionary 	${dict16}  aol
    Log  ${aolRec5}
    ${espnRec5} = 	Get From Dictionary 	${dict16}  espn
    Log  ${espnRec5}
    ${expFacebookRec5}=  evaluate  ${facebookRec5}/2
    Log  ${expFacebookRec5}
    ${expaolespnfacebookRec5}=  evaluate  ${expFacebookRec5}+${aolRec5}+${espnRec5}
    Log  ${expaolespnfacebookRec5}
    should be equal as integers  ${recordsFirstNPCollecotr1Default5}  ${expFacebookRec5}
    should be equal as integers  ${recordsFirstNPCollecotr2Default5}  ${expaolespnfacebookRec5}

    log to console   TC001160859 removing files from Linux Box
    Execute Command  rm ${testId9}*

    log to console  TC001160859 Configuring NetFLow on Group1
    createNewGroup  ${atipSession}  ${gName1}
    Add GroupId to NetFlow Config  ${atipSession}  ${netflowgroupcfg}  ${gName1}
    updateGlobalConfig  ${atipSession}  ${netflowgroupcfg}
    Configure NetFlow Collectors  ${atipSession}  ${firstCollectorGroup}  ${gName1}
    Configure NetFlow Collectors  ${atipSession}  ${secondCollectorGroup}  ${gName1}
    Configure NetFlow Collectors  ${atipSession}  ${thirdCollectorGroup}  ${gName1}
    Create Filters  ${atipSession}  ${Filter1Group}  ${gName1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId1}
    Configure NetFlow Exporter Cards  ${atipSession}  ${firstCardGroup1}  ${gName1}

    log to console  TC001160859 Verifying per NP rules on Group1
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules}

    log to console  TC001160859 Running BPS raffic and capturing netflow for Group1
    ${testId10} =  Set variable  TC001160859_Group1
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId10}  ${bpsTestConfig5}

    log to console  TC001160859 Validating that only enabled ixflow fields are present in the capture for Group1
    ${ixFlowFiltersGroup} =  Set variable  "cflow.pie.ixia.source-ip-country-code,cflow.pie.ixia.source-ip-country-name,cflow.pie.ixia.source-ip-region-code,cflow.pie.ixia.source-ip-region-name,cflow.pie.ixia.source-ip-city-name,cflow.pie.ixia.source-ip-latitude,cflow.pie.ixia.source-ip-longitude,cflow.pie.ixia.destination-ip-country-code,cflow.pie.ixia.destination-ip-country-name,cflow.pie.ixia.source-ip-region-code,cflow.pie.ixia.source-ip-region-name,cflow.pie.ixia.destination-ip-city-name,cflow.pie.ixia.destination-ip-latitude,cflow.pie.ixia.destination-ip-longitude"
    @{informElemToContainGroup} =  CREATE LIST  srcCountryCode  srcCountryName  srcRegionCode  srcRegionName  srcCityName  srcLatitude  srcLongitude  dstCountryCode  dstCountryName  dstRegionCode  dstRegionName  dstCityName  dstLatitude  dstLongitude
    Validate Information Elements  ${testId10}  ${firstCollectorGroupIP}  ${informElemToContainGroup}
    Validate Information Elements  ${testId10}  ${secondCollectorGroupIP}  ${informElemToContainGroup}
    Validate Information Elements  ${testId10}  ${thirdCollectorGroupIP}  ${informElemToContainGroup}

    log to console  TC001160859 Capture Split based on the NP Exporter IP for Group1
    Extract capture Per NP Exporter IP  ${testId10}  ${firstCardGroup1.ip_addr}

    log to console  TC001160859 Reading records stats per NP slot and collector for Group1
    ${groupId1}=  getGroupId  ${atipSession}  ${gName1}
    ${recordsFirstNPCollecotr1Group5} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorGroupIP}  ${npId1}  ${groupId1}
    Log  ${recordsFirstNPCollecotr1Group5}
    ${recordsFirstNPCollecotr2Group5} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorGroupIP}  ${npId1}  ${groupId1}
    Log  ${recordsFirstNPCollecotr2Group5}
    ${recordsFirstNPCollecotr3Group5} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${thirdCollectorGroupIP}  ${npId1}  ${groupId1}
    Log  ${recordsFirstNPCollecotr3Group5}

    log to console  Comparing capture values with records stats for Exporter for Group1
    ${responseFirstCardGroup6}=  Execute Command  python parsing.py ${testId10}_${firstCardGroup1.ip_addr}.pcap ${testId10}_${firstCardGroup1.ip_addr}.csv ${ixFlowFiltersGroup}
    ${dict17}=  parseOutput  ${responseFirstCardGroup6}
    Log  ${dict17}
    ${italyAll} = 	Get From Dictionary 	${dict17}  Italy
    ${chinaAll} = 	Get From Dictionary 	${dict17}  China
    ${thailandAll} = 	Get From Dictionary 	${dict17}  Thailand
    ${itRec}=  evaluate  ${italyAll}/2
    ${chRec}=  evaluate  ${chinaAll}/3
    ${thRec}=  evaluate  ${thailandAll}/2
    ${thItChRec}=  evaluate  ${itRec}+${chRec}+${thRec}
    should be equal as integers  ${recordsFirstNPCollecotr1Group5}  ${thItChRec}
    should be equal as integers  ${recordsFirstNPCollecotr2Group5}  ${thItChRec}
    should be equal as integers  ${recordsFirstNPCollecotr3Group5}  ${chRec}

    log to console   TC001160859 removing files from Linux Box
    Execute Command  rm ${testId10}*


TC001160855 Defalut Group and 1 New Group with different Netflow Configuration and Filters (MoveNP_Config) vATIP
    [Tags]  virtual
    [Setup]  Clear Config

    log to console  TC001160855 Configuring NetFLow on Default Group
    Add GroupId to NetFlow Config  ${atipSession}  ${netflowdefaultcfg}  ${defaultG}
    updateGlobalConfig  ${atipSession}  ${netflowdefaultcfg}
    Configure NetFlow Collectors  ${atipSession}  ${firstCollectorDefaultvATIP}  ${defaultG}
    Configure NetFlow Collectors  ${atipSession}  ${secondCollectorDefaultvATIP}  ${defaultG}
    Create Filters  ${atipSession}  ${filter1DefaultGroupAol}  ${defaultG}

    Configure NetFlow Exporter Cards  ${atipSession}  ${firstCardDefault}  ${defaultG}

    log to console  TC001160855 Verifying per NP rules on Default Group
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules}

    log to console  TC001160855 Running BPS raffic and capturing netflow for Default Group
    ${testId11} =  Set variable  TC001160855_Default
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId11}  ${bpsTestConfig4}

    log to console  TC001160855 Validating that only enabled ixflow fields are present in the capture for Default Group
    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.hostname,cflow.pie.ixia.l7-application-id,cflow.pie.ixia.dns-question-names,cflow.pie.ixia.http-uri,cflow.pie.ixia.l7-application-name,cflow.pie.ixia.dns-question-names,cflow.pie.ixia.http-user-agent,cflow.pie.ixia.dns-txt,cflow.pie.ixia.dns-classes"
    @{informElemToContain} =  CREATE LIST  httpHostName  l7appID  dnsQuestionNames  uri  l7appName  dnsQuestionNames  userAgent  dnsTxt  dnsClasses
    Validate Information Elements  ${testId11}  ${firstCollectorDefaultIPvATIP}  ${informElemToContain}
    Validate Information Elements  ${testId11}  ${secondCollectorDefaultIPvATIP}  ${informElemToContain}

    log to console   TC001160855 Reading records stats per NP slot and collector for Default Group
    ${recordsFirstNPCollecotr1Default6} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorDefaultIPvATIP}  ${slotId1}
    Log  ${recordsFirstNPCollecotr1Default6}
    ${recordsFirstNPCollecotr2Default6} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorDefaultIPvATIP}  ${slotId1}
    Log  ${recordsFirstNPCollecotr2Default6}

    log to console  TC001160855 Comparing capture values with records stats for Exporter for Default Group
    ${responseFirstCardDefault6}=  Execute Command  python parsing.py ${testId11}.pcap ${testId11}.csv ${ixFlowFilters}
    ${dict18}=  parseOutput  ${responseFirstCardDefault6}
    Log  ${dict18}
    ${facebookRec6} = 	Get From Dictionary 	${dict18}  facebook
    ${espnRec6} = 	Get From Dictionary 	${dict18}  espn
    ${aolRec6} = 	Get From Dictionary 	${dict18}  aol
    ${expAolbookRec6}=  evaluate  ${aolRec6}/2
    ${expaolespnfacebookRec6}=  evaluate  ${facebookRec6}+${expAolbookRec6}+${espnRec6}
    should be equal as integers  ${recordsFirstNPCollecotr1Default6}  ${expaolespnfacebookRec6}
    should be equal as integers  ${recordsFirstNPCollecotr2Default6}  ${expAolbookRec6}

    log to console   TC001160855 removing files from Linux Box
    Execute Command  rm ${testId11}*


    log to console  TC001160855 Configuring NetFLow on Group1
    createNewGroup  ${atipSession}  ${gName1}
    Add GroupId to NetFlow Config  ${atipSession}  ${netflowgroupcfg}  ${gName1}
    updateGlobalConfig  ${atipSession}  ${netflowgroupcfg}
    Configure NetFlow Collectors  ${atipSession}  ${firstCollectorGroupvATIP}  ${gName1}
    Configure NetFlow Collectors  ${atipSession}  ${secondCollectorGroupvATIP}  ${gName1}
    Configure NetFlow Collectors  ${atipSession}  ${thirdCollectorGroupvATIP}  ${gName1}
    Create Filters  ${atipSession}  ${Filter1Group}  ${gName1}
    moveNPsToGroup  ${atipSession}  ${gName1}  ${npId1}

    Configure NetFlow Exporter Cards  ${atipSession}  ${firstCardGroup1}  ${gName1}


    log to console  TC001160855 Verifying per NP rules on Group1
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules}

    log to console  TC001160855 Running BPS raffic and capturing netflow for Group1
    ${testId12} =  Set variable  TC001160855_Group1
    resetStats  ${atipSession}
    Connect on Linux Box
    Run and Capture Traffic  ${testId12}  ${bpsTestConfig5}

    log to console  TC001160855 Validating that only enabled ixflow fields are present in the capture for Group1
    ${ixFlowFiltersGroup} =  Set variable  "cflow.pie.ixia.source-ip-country-code,cflow.pie.ixia.source-ip-country-name,cflow.pie.ixia.source-ip-region-code,cflow.pie.ixia.source-ip-region-name,cflow.pie.ixia.source-ip-city-name,cflow.pie.ixia.source-ip-latitude,cflow.pie.ixia.source-ip-longitude,cflow.pie.ixia.destination-ip-country-code,cflow.pie.ixia.destination-ip-country-name,cflow.pie.ixia.source-ip-region-code,cflow.pie.ixia.source-ip-region-name,cflow.pie.ixia.destination-ip-city-name,cflow.pie.ixia.destination-ip-latitude,cflow.pie.ixia.destination-ip-longitude"
    @{informElemToContainGroup} =  CREATE LIST  srcCountryCode  srcCountryName  srcRegionCode  srcRegionName
    ...  srcCityName  srcLatitude  srcLongitude  dstCountryCode  dstCountryName  dstRegionCode  dstRegionName
    ...  dstCityName  dstLatitude  dstLongitude
    Validate Information Elements  ${testId12}  ${firstCollectorGroupIPvATIP}  ${informElemToContainGroup}
    Validate Information Elements  ${testId12}  ${secondCollectorGroupIPvATIP}  ${informElemToContainGroup}
    Validate Information Elements  ${testId12}  ${thirdCollectorGroupIPvATIP}  ${informElemToContainGroup}

    log to console  TC001160855 Reading records stats per NP slot and collector for Group1
    ${groupId1}=  getGroupId  ${atipSession}  ${gName1}
    ${recordsFirstNPCollecotr1Group6} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${firstCollectorGroupIPvATIP}  ${slotId1}  ${groupId1}
    Log  ${recordsFirstNPCollecotr1Group6}
    ${recordsFirstNPCollecotr2Group6} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${secondCollectorGroupIPvATIP}  ${slotId1}  ${groupId1}
    Log  ${recordsFirstNPCollecotr2Group6}
    ${recordsFirstNPCollecotr3Group6} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${thirdCollectorGroupIPvATIP}  ${slotId1}  ${groupId1}
    Log  ${recordsFirstNPCollecotr3Group6}

    log to console  TC001160855 Comparing capture values with records stats for Exporter 1 for Group1
    ${responseFirstCardGroup7}=  Execute Command  python parsing.py ${testId12}.pcap ${testId12}.csv ${ixFlowFiltersGroup}
    ${dict19}=  parseOutput  ${responseFirstCardGroup7}
    Log  ${dict19}
    ${itAll} = 	Get From Dictionary 	${dict19}  Italy
    ${chAll} = 	Get From Dictionary 	${dict19}  China
    ${thAll} = 	Get From Dictionary 	${dict19}  Thailand
    ${italyRec}=  evaluate  ${itAll}/2
    ${chinaRec}=  evaluate  ${chAll}/3
    ${thailandRec}=  evaluate  ${thAll}/2
    ${thItChRecTotal}=  evaluate  ${italyRec}+${chinaRec}+${thailandRec}
    should be equal as integers  ${recordsFirstNPCollecotr1Group6}  ${thItChRecTotal}
    should be equal as integers  ${recordsFirstNPCollecotr2Group6}  ${thItChRecTotal}
    should be equal as integers  ${recordsFirstNPCollecotr3Group6}  ${chinaRec}

    log to console   TC001160855 removing files from Linux Box
    Execute Command  rm ${testId12}*

*** Keywords ***
Configure ATIP And Login
    login  ${atipSession}

Clear Config
   Connect on Linux Box
   ${command} =  copyFileToLinux
   Write  ${command}
   clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
   ${npId1}=  updateNpIdVirtual  ${atipSession}  ${envVars}
   set suite variable  ${npId1}  ${npId1}

Create Filters
    [Arguments]  ${atipSession}  ${filterObject}  ${groupName}
    ${groupId}=  getGroupId  ${atipSession}  ${groupName}
    ${filterObject.groupId}=  set variable  ${groupId}
    ${filterObject.id}=  set variable  -1
    createFilter  ${atipSession}  ${filterObject}
    rulePush  ${atipSession}  ${groupId}
    waitUntilSystemReady  ${atipSession}

Configure NetFlow Exporter Cards
    [Arguments]  ${atipSession}  ${exporterConfig}  ${groupName}
    ${groupId}=  getGroupId  ${atipSession}  ${groupName}
    updateCard  ${atipSession}  ${exporterConfig}  ${groupId}

Configure NetFlow Collectors
    [Arguments]  ${atipSession}  ${collecotrConfigObject}  ${groupName}
    ${groupId}=  getGroupId  ${atipSession}  ${groupName}
    updateCollectorConfig  ${atipSession}  ${collecotrConfigObject}  ${groupId}

Add GroupId to NetFlow Config
    [Arguments]  ${atipSession}  ${netflowObject}  ${groupName}
    ${groupId}=  getGroupId  ${atipSession}  ${groupName}
    ${netflowObject.groupId}=  set variable  ${groupId}

Validate Rules Number Per NP
    [Arguments]  ${atipSession}  ${networkProcessorId}  ${expectedRules}
    verifyNpRules  ${atipSession}  ${networkProcessorId}  ${expectedRules}

