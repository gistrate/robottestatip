
from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage

class FilterActions(object):

    @classmethod
    def checkTxPacketsForward(cls, webApiSession, npId=0):
        txPackets = DebugPage.getTxPacketsFromDebugPage(webApiSession, npId)
        if txPackets == 0:
            raise Exception('No Packets are being forwarded')
        else:
            print('tx: {}'.format(txPackets))
        return None

    @classmethod
    def checkTxPacketsDrop(cls, webApiSession, npId=0):
        txPackets = DebugPage.getTxPacketsFromDebugPage(webApiSession, npId)
        if txPackets != 0:
            raise Exception('Packets are still being forwarded')
        else:
            print('tx: {}'.format(txPackets))
        return None


