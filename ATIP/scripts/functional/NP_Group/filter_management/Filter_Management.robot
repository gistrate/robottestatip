*** Settings ***
Documentation  Filter Management functionality

Variables  SuiteConfig.py
Library    FilterActions.py
Library    Collections
Library    scripts.functional.NP_Group.Group_Management.GroupMgmtLib
Library    atipAutoFrwk.webApi.atip.Logout
Library	   atipAutoFrwk.webApi.atip.CustomIP
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.config.atip.filter.FilterConditionConfig
Library    atipAutoFrwk.config.atip.filter.FilterConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.services.atip.NPService
Library    BuiltIn

Force Tags  hardware
Suite Setup        Configure ATIP And Login
Suite Teardown     Clean ATIP

*** Test Cases ***
TC001178760_Filter_Management_NP_Group_Filter_Update_HW_vATIP
    [Tags]  virtual
    #Get groupId for NP Group: Test1
    ${gid1} =  getGroupId  ${atipSession}  Test1
    log  ${gid1}

    #Add IP Address filter to group Test1
    ${customIPFilter1.groupId} =  set variable  ${gid1}
    createFilter  ${atipSession}  ${customIPFilter1}
    rulePush  ${atipSession}  ${gid1}

    #Move NP 1 to Group Test1
    changeNPGroup  ${atipSession}  ${npId0}  ${gid1}
    waitUntilGroupReady  ${atipSession}  ${gid1}

    #Validate number of Rules present
    verifyNpRules  ${atipSession}  ${npId0}  2

    Log  Running BPS test: ${bpsTestConfig1.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig1}

    Log  Checking if Test1 filter exists in Top Filters Widget : ${customIPFilter1.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${customIPFilter1}  ${timeInterval}  ${dashBoardType}

    #Updating IP Address filter with a second IP Address range
    addIPRangeCondition  ${customIPFilter1}  ${updateIpRange}
    updateFilter  ${atipSession}  ${customIPFilter1}
    #updateFilterByName    ${atipSession}  IPRange  ${customIPFilter1}

    #Reset Stats
    resetStats  ${atipSession}

    Log  Running BPS test: ${bpsTestConfig2.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig2}

    Log  Checking if Test1 filter exists in Top Filters Widget : ${customIPFilter1.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${customIPFilter1}  ${timeInterval}  ${dashBoardType}


TC001178758_Filter_Management_NP_Group_Filter_Drop_Egress_Action_HW
    [Tags]
    #Get groupId for NP Group: Test2
    ${gid2} =  getGroupId  ${atipSession}  Test2
    log  ${gid2}

    #Add Protocol filter to group Test2
    ${protocolFilter2.groupId} =  set variable  ${gid2}
    createFilter  ${atipSession}  ${protocolFilter2}
    rulePush  ${atipSession}  ${gid2}

    #Add Application filter forward to group Test2
    ${appFilter3.groupId} =  set variable  ${gid2}
    createFilter  ${atipSession}  ${appFilter3}
    rulePush  ${atipSession}  ${gid2}

    #Move NP 2 to Group Test2
    changeNPGroup  ${atipSession}  ${npId1}  ${gid2}
    waitUntilGroupReady  ${atipSession}  ${gid2}

    #Validate number of Rules present
    verifyNpRules  ${atipSession}  ${npId1}  3

    Log  Running BPS test: ${bpsTestConfig3.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig3}

    Log  Checking if Test2 filter exists in Top Filters Widget : ${protocolFilter2.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${protocolFilter2}  ${timeInterval}  ${dashBoardType}

    Log  Checking if Test2 filter exists in Top Filters Widget : ${appFilter3.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${appFilter3}  ${timeInterval}  ${dashBoardType}

    #Check that packets are being transmitted
    checkTxPacketsForward  ${atipSession}  ${npId1}

    #Configure egress action of Protocol filter to drop
    ${protocolFilter2.dropTraffic} =  set variable  ${True}
    updateFilter  ${atipSession}  ${protocolFilter2}
    #updateFilterByName  ${atipSession}  ProtocolTCP  ${protocolFilter2}

     Log  Running BPS test: ${bpsTestConfig3.testName}  console=true
     atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig3}

    #Check that packets are being dropped
    checkTxPacketsDrop  ${atipSession}  ${npId1}


TC001178759_Filter_Management_NP_Group_unmatched_Filter_HW
    [Tags]
    #Get groupId for NP Group: Test3
    ${gid3} =  getGroupId  ${atipSession}  Test3
    log  ${gid3}

    #Move NP 3 to Group Test3
    changeNPGroup  ${atipSession}  ${npId2}  ${gid3}
    waitUntilGroupReady  ${atipSession}  ${gid3}

    #Enable Unmatched filter on group Test3
    ${umFilter} =  getFilterByName  ${atipSession}  Unmatched traffic  ${gid3}
    ${umFilter.deny} =   set variable  ${true}
    updateFilter  ${atipSession}  ${umFilter}
    rulePush  ${atipSession}  ${gid3}

    #Validate number of Rules present
    verifyNpRules  ${atipSession}  ${npId2}  1

    Log  Running BPS test: ${bpsTestConfig3.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig3}

    Log  Checking if Test3 filter exists in Top Filters Widget : ${umFilter.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${umFilter}  ${timeInterval}  ${dashBoardType}


TC001191466_ Filter_Management_NP_Group_Multiple_Conditions_HW_vATIP]
    [Tags]  virtual
    #Get groupId for NP Group: Test4
    ${gid4} =  getGroupId  ${atipSession}  Test4
    log  ${gid4}

    #Add app+ipAddress filter to group Test4
    ${filter4.groupId} =  set variable  ${gid4}
    createFilter  ${atipSession}  ${filter4}
    rulePush  ${atipSession}  ${gid4}

    #Move NP 1 to Group Test4
    changeNPGroup  ${atipSession}  ${npId0}  ${gid4}
    waitUntilGroupReady  ${atipSession}  ${gid4}

    #Enable Unmatched filter on group Test4
    ${umFilter} =  getFilterByName  ${atipSession}  Unmatched traffic  ${gid4}
    ${umFilter.deny} =  set variable  ${true}
    updateFilter  ${atipSession}  ${umFilter}
    rulePush  ${atipSession}  ${gid4}


    #Validate number of Rules present
    verifyNpRules  ${atipSession}  ${npId0}  2

    Log  Running BPS test: ${bpsTestConfig4.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig4}

    Log  Checking if Test4 filter exists in Top Filters Widget : ${umFilter.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${umFilter}  ${timeInterval}  ${dashBoardType}

    Log  Checking if Test4 filter exists in Top Filters Widget : ${filter4.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${filter4}  ${timeInterval}  ${dashBoardType}


*** Keywords ***
Configure ATIP And Login
     Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    configureAtip  ${atipSession}   ${atipConfig}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}

Clean ATIP
    logout  ${atipSession}