from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.IPAddressRange import IPAddressRange
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig

class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    timeInterval = TimeInterval.FIVEMIN
    dashBoardType = DashboardType.MINIMIZED

    # Define groups and add them to the config
    g1 = GroupConfig(name='Test1')
    g2 = GroupConfig(name='Test2')
    g3 = GroupConfig(name='Test3')
    g4 = GroupConfig(name='Test4')

    atipConfig = AtipConfig()

    atipConfig.addGroup(g1)
    atipConfig.addGroup(g2)
    atipConfig.addGroup(g3)
    atipConfig.addGroup(g4)

    npId0 = str(envVars.atipSlotNumber[0])
    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[1])
        npId2 = str(envVars.atipSlotNumber[2])

    #Create Filters
    customIPFilter1 = FilterConfig("IPRange", groupId= 0)
    protocolFilter2 = FilterConfig("ProtocolTCP", groupId=0)
    appFilter3 = FilterConfig("Facebook", groupId=0)
    appFilter3.forward = True
    filter4 = FilterConfig("AppAndIPrange", groupId=0)

    FilterConditionConfig.addIPRangeCondition(customIPFilter1, IPAddressRange("1.1.0.1", "1.1.0.50", "user"))
    FilterConditionConfig.addProtocolCondition(protocolFilter2, NetworkProtocol.TCP)
    FilterConditionConfig.addAppCondition(appFilter3, ApplicationType.FACEBOOK)
    FilterConditionConfig.addAppCondition(filter4, ApplicationType.AMAZON)
    FilterConditionConfig.addIPRangeCondition(filter4, IPAddressRange("1.1.1.10", "1.1.1.50", "user"))

    type = FilterConditionType.IP_RANGE
    updateIpRange = IPAddressRange("1.10.0.30", "1.10.0.40", "user")

    #specify BPS TestConfig
    bpsTestConfig1 = BpsTestConfig('Ip filtering test 1', 80)
    bpsTestConfig2 = BpsTestConfig('Ip filtering test 2', 80)
    bpsTestConfig3 = BpsTestConfig('adina_facebook', 80)
    bpsTestConfig4 = BpsTestConfig('NP_Group_Amazon', 80)