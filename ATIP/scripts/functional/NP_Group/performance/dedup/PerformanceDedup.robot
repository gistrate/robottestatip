*** Settings ***
Documentation  Dedup Performance test suite.

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    atipAutoFrwk.webApi.traffic.BpsOperations
Library    atipAutoFrwk.webApi.traffic.Login  WITH NAME  bpsLogin
Library    atipAutoFrwk.webApi.traffic.PortsOperations  WITH NAME  portsOperations
Library    atipAutoFrwk.services.atip.NPService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.services.atip.stats.PerformanceService
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.services.atip.stats.ValidateDedupStatsService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.webApi.atip.Dedup
Resource    scripts/functional/dedup/DedupConfigLib.robot

Default Tags  ATIP  performance  hardware  virtual
Suite Setup  Create Dedup Config
Test Template  Validate Dedup
Suite Teardown  Dedup Teardown

*** Test Cases ***
TC000922912  500Mbps-Netflix_30min  1800  60  0.02  5  Test1
TC000926602  2_5Gbps-Netflix_30min  1800  60  0.02  5  Test2
TC000926603  5Gbps-Netflix_30min  1800  60  0.02  10  Test3
TC000926604  10Gbps-Netflix_30min  1800  60  0.3  20  Test4



*** Keywords ***

Validate Dedup
    [Arguments]  ${bpsFile}  ${duration}  ${validateDropRateFrequency}  ${percent}  ${unhandled}  ${testId}
    # Reset Stats
    atipLogin.login  ${atipSession}
    atipAutoFrwk.webApi.atip.System.resetStats  ${atipSession}

    # Configure BPS
    bpslogin.Login  ${bpsSession}
    portsOperations.reserveBpsPorts  ${bpsSession}  ${envVars.bpsSlot}  ${envVars.bpsPortList}  ${envVars.bpsPortGroup}

    # Remove core files
	sshRemoveCoreFilesInNP  ${envVars}  ${envVars}

	# Configure Dedup
	configureAtip  ${atipSession}  ${atipConfig}  ${envVars}
	${npId}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId}  ${npId}

	#Get groupId for NP Group: testId
    ${gid} =  getGroupId  ${atipSession}  ${testId}
    log  ${gid}

    #Move NPId to NP Group
    changeNPGroup  ${atipSession}  ${npId}  ${gid}
    waitUntilGroupReady  ${atipSession}  ${gid}
    #Add app filter to NP Group
    ${dedupFilter1.groupId} =  set variable  ${gid}
    atipAutoFrwk.webApi.atip.Filters.createFilter  ${atipSession}  ${dedupFilter1}
    rulePush  ${atipSession}  ${gid}
    #Enable Dedup on group testId
    updateDedup  ${atipSession}  ${dedupConfig}  ${gid}
	# Send traffic and verify performance
	${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  ${duration}
	${initRx}=  getRxPacketsFromDebugPage  ${atipSession}  ${envVars}
	${verifyDedupVariation.groupId} =  set variable  ${gid}
	${verifyDedupUnhandledPkts.groupId} =  set variable  ${gid}
    addBpsTestAction  ${verifyDedupVariation}
    addBpsTestAction  ${verifyDedupUnhandledPkts}
    Call method    ${verifyDedupVariation}  setDropPercent  ${percent}
    Call method    ${verifyDedupUnhandledPkts}  setUnhandled  ${unhandled}
	performActionUntilBpsTestCompletion  ${envVars}  ${bpsSession}  ${bpsTestConfig}  ${validateDropRateFrequency}
	# Validate no crashes in core files
	validateNoCrashesInNPCoreFile  ${atipSession}  ${envVars}  ${bpsFile}
    checkHWDrop  ${envVars}  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  ${initRx}  0
    clearBpsTestAction

Dedup Teardown
    Create NTO Config
    logout  ${atipSession}