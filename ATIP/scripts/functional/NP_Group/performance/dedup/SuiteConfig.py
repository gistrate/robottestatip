from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.DedupGlobalConfig import DedupGlobalConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.services.atip.bpsTestActions.VerifyDedupDropAction import VerifyDedupDropAction, ChangeDedupState, ChangeExpectedDataRate, ChangeHeaderValue, VerifyDedupUnhandledPkts
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig




class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    timeInterval = TimeInterval.HOUR
    npId = str(envVars.atipSlotNumber[0])
    # ATIP config
    dedupFilter1 = FilterConfig("DedupFilter")
    FilterConditionConfig.addAppCondition(dedupFilter1, ApplicationType.NETFLIX)
    dedupFilter1.forward = True
    dedupFilter1.transmitIds = [100]

    dedupFilter2 = FilterConfig("DedupFilter")
    FilterConditionConfig.addAppCondition(dedupFilter2, ApplicationType.NETFLIX)
    dedupFilter2.forward = True
    dedupFilter2.transmitIds = [100]

    dedupFilter3 = FilterConfig("DedupFilter")
    FilterConditionConfig.addAppCondition(dedupFilter3, ApplicationType.NETFLIX)
    dedupFilter3.forward = True
    dedupFilter3.transmitIds = [100]

    dedupFilter4 = FilterConfig("DedupFilter")
    FilterConditionConfig.addAppCondition(dedupFilter4, ApplicationType.NETFLIX)
    dedupFilter4.forward = True
    dedupFilter4.transmitIds = [100]
    # Define groups and add them to the config
    g1 = GroupConfig(name='Test1')
    g2 = GroupConfig(name='Test2')
    g3 = GroupConfig(name='Test3')
    g4 = GroupConfig(name='Test4')

    atipConfig = AtipConfig()
    dedupConfig = DedupGlobalConfig("844600", True, "NONE")
    verifyDedupVariation = VerifyDedupDropAction(atipSession)
    verifyDedupUnhandledPkts = VerifyDedupUnhandledPkts(atipSession)
    changeDedupState = ChangeDedupState(atipSession)
    changeHeaderValue = ChangeHeaderValue(atipSession)
    changeExpectedDataRate = ChangeExpectedDataRate(atipSession)
    bpsTestConfigChange1 = BpsTestConfig('10Gbps-Netflix_30min', 1800)
    bpsTestConfigChange2 = BpsTestConfig('AppSimMix-2ports_30min', 1800)

    atipConfig.addGroup(g1)
    atipConfig.addGroup(g2)
    atipConfig.addGroup(g3)
    atipConfig.addGroup(g4)