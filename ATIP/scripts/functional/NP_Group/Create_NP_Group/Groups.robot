*** Settings ***
Documentation  Test groups functionality

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Groups
Library	   GroupsLib.py
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    ../../stats/lib/ValidateStatsService.py
Library    ../../stats/lib/GeneralService.py
Library    ../../stats/lib/StatsDefaultConfig.py


Default Tags  ${hardware}  ${virtual}

Suite Setup        Configure ATIP And Login
Suite Teardown     logout  ${atipSession}

*** Test Cases ***
Check Default Group
    testDefaultGroup  ${atipSession}

Check Delete Default Group
	${response}  deleteGroup  ${atipSession}    0  ${httpStatusError}

Check If Group Can Be Added
	testAddGroup  ${atipSession}

Check Duplicate Name On Add
	testDuplicateName  ${atipSession}

Check Duplicate Name On Update
	testDuplicateNameUpdate  ${atipSession}

Check NP Change Group
	testNPChangeGroup  ${atipSession}

*** Keywords ***
Configure ATIP And Login
    configureAtip  ${atipSession}   ${atipConfig}