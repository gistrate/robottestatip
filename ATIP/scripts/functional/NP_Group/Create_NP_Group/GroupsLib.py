from http import HTTPStatus
import json
from robot.variables.assigner import AssignmentValidator

from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.data.atip.ConfigTimeout import ConfigTimeout
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.webApi.atip.Groups import Groups
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.Status import Status
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.SystemService import SystemService

class GroupsLib(object):
    def __init__(self):
        pass

    def getGroupId(self, webApiSession, name):
        groups = Groups.getGroups(webApiSession)
        for item in groups.get('groups'):
            if item.get('name') == name:
                return item.get('id')
        return None

    def testDefaultGroup(self, webApiSession):
        groups = Groups.getGroups(webApiSession)

        if not groups.get('groups'):
            raise Exception('No groups defined...')
        else:
            if isinstance(groups.get('groups'), list):
                for item in groups.get('groups'):
                    if item.get('id') == 0:
                        return True
            else:
                if groups.get('groups').get('id') == 0:
                    return True
        raise Exception('Could not find default group')

    def testDeleteDefaultGroup(self, webApiSession):
        response = Groups.deleteGroup(webApiSession, 0, HTTPStatus.INTERNAL_SERVER_ERROR)

        if response.get("success") == "true":
            raise Exception('Default group deleted :(')

    def testAddGroup(self, webApiSession):
        gName = "gr12"
        Groups.addGroup(webApiSession, GroupConfig(name=gName))

        id = Groups.getGroupId(webApiSession, gName)

        if id is None:
            raise Exception('Group not found')

        Groups.deleteGroup(webApiSession, id)

    def testDuplicateName(self, webApiSession):
        gName = "gr16"

        Groups.addGroup(webApiSession, GroupConfig(name=gName))

        Groups.addGroup(webApiSession, GroupConfig(name=gName), HTTPStatus.INTERNAL_SERVER_ERROR)

        id = Groups.getGroupId(webApiSession, gName)

        Groups.deleteGroup(webApiSession, id)

    def testDuplicateNameUpdate(self, webApiSession):
        gName = "gr22"
        gDup = "gr23"

        Groups.addGroup(webApiSession, GroupConfig(name=gName))

        Groups.addGroup(webApiSession, GroupConfig(name=gDup))

        id = Groups.getGroupId(webApiSession, gDup)

        Groups.updateGroup(webApiSession, GroupConfig(id=id, name=gName), HTTPStatus.INTERNAL_SERVER_ERROR)
        Groups.deleteGroup(webApiSession, id)

        id = Groups.getGroupId(webApiSession, gName)
        Groups.deleteGroup(webApiSession, id)

    def testNPChangeGroup(self, webApiSession):
        nps = Groups.getNPList(webApiSession)

        npId = 0

        if isinstance(nps.get('np'), list):
            for item in nps.get('np'):
                npId = item.get('id')
                break
        else:
            npId = nps.get('np').get('id')

        gname = 'test'
        Groups.addGroup(webApiSession, GroupConfig(name=gname))
        gid = Groups.getGroupId(webApiSession, gname)

        Groups.changeNPGroup(webApiSession, npId, gid)

        nps = Groups.getNPsInGroup(webApiSession, gid)

        if isinstance(nps.get('np'), list):
            for item in nps.get('np'):
                if item.get('id') != npId:
                    raise Exception('NP not moved')
        else:
            print('before exception')
            if nps.get('nps').get('id') != npId:
                print('after exception')
                raise Exception('NP not moved')


