from http import HTTPStatus

from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Groups import Groups
from atipAutoFrwk.webApi.atip.Login import Login


class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL

    Filter1DefaultGroup = FilterConfig("Filter1DefaultGroup")
    FilterConditionConfig.addAppCondition(Filter1DefaultGroup, ApplicationType.AMAZON)

    Filter1Group1 = FilterConfig("Filter1Group1")
    FilterConditionConfig.addAppCondition(Filter1Group1, ApplicationType.NETFLIX)

    Filter1Group2 = FilterConfig("Filter1Group2")
    FilterConditionConfig.addGeoCondition(Filter1Group2, GeoLocation.FRANCE)

    Filter2Group2 = FilterConfig("Filter2Group2")
    FilterConditionConfig.addProtocolCondition(Filter2Group2, NetworkProtocol.TCP)

    Filter1Group3 = FilterConfig("Filter1Group3")
    FilterConditionConfig.addGeoCondition(Filter1Group3, GeoLocation.AUSTRALIA)

    Filter2Group3 = FilterConfig("Filter2Group3")
    FilterConditionConfig.addProtocolCondition(Filter2Group3, NetworkProtocol.UDP)

    Filter3Group3 = FilterConfig("Filter3Group3")
    FilterConditionConfig.addAppCondition(Filter3Group3, ApplicationType.FACEBOOK)

    gName1 = 'Group1'
    gName2 = 'Group2'
    gName3 = 'Group3'
    defaultG = 'Default'

    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[0])
        npId2 = str(envVars.atipSlotNumber[1])
        npId3 = str(envVars.atipSlotNumber[2])
        print("NP ids are {} ; {} ; {}".format(npId1, npId2, npId3))
        npIds = [npId1, npId2, npId3]
    else:
        npId1 = str(envVars.atipSlotNumber[0])

    gNames = ['Group1', 'Group2', 'Group3']

    expRules1 = '2'
    expRules2 = '3'
    expRules3 = '4'



