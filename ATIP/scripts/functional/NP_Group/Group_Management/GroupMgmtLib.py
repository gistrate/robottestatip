from http import HTTPStatus
import json
import time

from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage
from robot.variables.assigner import AssignmentValidator
from atipAutoFrwk.services.atip.GroupsService import GroupsService
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.data.atip.ConfigTimeout import ConfigTimeout
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.webApi.atip.Groups import Groups
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.Status import Status
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.SystemService import SystemService

class GroupMgmtLib(object):

    def validateAddGroup(self, webApiSession, gName):
        Groups.addGroup(webApiSession, GroupConfig(name=gName))
        id = Groups.getGroupId(webApiSession, gName)
        if id is None:
            raise Exception('Group not found')

    def validateDeleteGroup(self, webApiSession, gName):
        id = Groups.getGroupId(webApiSession, gName)
        if id is None:
            raise Exception('Group not found on ATIP setup')
        else:
             response = Groups.deleteGroup(webApiSession, id)
             if response.get("success") == "true":
                 raise Exception('Default group deleted! User should not be allowed to delete the default group !')

    def validateNPMoveGroup(self, webApiSession, gName, npId):

        nps = Groups.getNPList(webApiSession)
        self.checkNpExists(nps.get('np'), npId)

        gid = Groups.getGroupId(webApiSession, gName)
        Groups.changeNPGroup(webApiSession, npId, gid)
        nps = Groups.getNPsInGroup(webApiSession, gid)

        self.checkNpExists(nps.get('nps'), npId)

        GroupsService.waitUntilGroupReady(webApiSession, gid)


    def checkNpExists(self, npList, npId):
        if not isinstance(npList, list):
            npList = [npList]
        found = False
        for item in npList:
            if str(npId) == str(item.get('id')):
                found = True
                break
        if not found:
            raise Exception('NP id {} not in the current list'.format(npId))

    def validateDeleteDefaultGroup(self, webApiSession):
        response = Groups.deleteGroup(webApiSession, 0, HTTPStatus.INTERNAL_SERVER_ERROR)
        if response.get("success") == "true":
            raise Exception('Default group deleted !!! Should not be allowed !!!')

    def validateDuplicateName(self, webApiSession, gName):
        Groups.addGroup(webApiSession, GroupConfig(name=gName), HTTPStatus.INTERNAL_SERVER_ERROR)

    def validateDuplicateNameUpdate(self, webApiSession, gName1, gName2):
        Groups.addGroup(webApiSession, GroupConfig(name=gName1))
        Groups.addGroup(webApiSession, GroupConfig(name=gName2))
        id = Groups.getGroupId(webApiSession, gName2)
        Groups.updateGroup(webApiSession, GroupConfig(id=id, name=gName1), HTTPStatus.INTERNAL_SERVER_ERROR)

    def validateDefaultGroup(self, webApiSession):
        groups = Groups.getGroups(webApiSession)
        if not groups.get('groups'):
            raise Exception('No groups defined !!! Default Group should always be present !!!')
        else:
            if isinstance(groups.get('groups'), list):
                for item in groups.get('groups'):
                    if item.get('id') == 0:
                        return True
            else:
                if groups.get('groups').get('id') == 0:
                    return True
        raise Exception('Could not find default group !!!')


    def verifyNpRules(self, webSession, npId, expRules):
        SystemService.waitUntilSystemReady(webSession)
        rules = DebugPage.getNpRulesFromDebugPage(webSession, npId)
        print("Installed rules number is {} & expected rules number is {} ".format(rules, expRules))
        if rules != int(expRules):
            raise Exception("Installed rules number : {} not equal with expected rules number :{} ".format(rules, expRules))
        else:
            print("Installed rules number: {} is equal with expected rules number: {} ".format(rules, expRules))
        return 0


    def deleteGroupWithNP(self, webApiSession, gName1):
        response = Groups.deleteGroup(webApiSession, 0, HTTPStatus.INTERNAL_SERVER_ERROR)
        if response.get("success") == "true":
            raise Exception('Group with assigned NP deleted !!! Should not be allowed !!!')

    def createMultipeGroups(self, webApiSession, gNames):
        for gName in gNames:
            self.validateAddGroup(webApiSession, gName)

    def assignMultipeNpsToGroups(self, webApiSession, gNames, npIds):
        for npsId, gName  in zip(npIds,gNames) :
            print(npsId, gName)
            self.validateNPMoveGroup(webApiSession, gName, npsId)

