*** Settings ***
Documentation  Test groups functionality

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Groups
Library	   GroupMgmtLib.py
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    scripts.functional.stats.lib.StatsDefaultConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    Collections


Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP And Login
Suite Teardown     logout  ${atipSession}


*** Test Cases ***

TC001152524 Default Group Is Present 7300/vATIP
    [Setup]  Clear Config
    validateDefaultGroup  ${atipSession}

TC001152521 Delete Default Group 7300/vATIP
     validateDeleteDefaultGroup  ${atipSession}

TC001152535 Create One Group 7300/vATIP
    validateAddGroup  ${atipSession}  ${gName1}

TC001152535 Create Duplicate Group Name 7300/vATIP
    validateDuplicateName  ${atipSession}  ${gName1}

TC001152535 Add NP To Group 7300/vATIP
    validateNPMoveGroup  ${atipSession}  ${gName1}  ${npId1}

TC001152530 Delete Group With Assigned NP 7300/vATIP
    deleteGroupWithNP  ${atipSession}  ${gName1}

TC001152535 Move NP Back to Default Groups 7300/vATIP
    validateNPMoveGroup  ${atipSession}  ${defaultG}  ${npId1}

TC001152529 Delete Created Group 7300/vATIP
    validateDeleteGroup  ${atipSession}  ${gName1}

Duplicate Group Name If Updated 7300/vATIP
    validateDuplicateNameUpdate  ${atipSession}  ${gName1}  ${gName2}
    validateDeleteGroup  ${atipSession}  ${gName1}
    validateDeleteGroup  ${atipSession}  ${gName2}

TC001159690 Create Delete Multiple Groups with assigned NPs (assign NP + create filter) 7330 with 3NPs
    [Tags]  hardware
    [Setup]  Clear Config
    createMultipeGroups  ${atipSession}  ${gNames}
    assignMultipeNpsToGroups  ${atipSession}  ${gNames}  ${npIds}
    Create Filters Per Group Name From Dict  ${atipSession}  ${filtersToGroups}
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules1}
    Validate Rules Number Per NP  ${atipSession}  ${npId2}  ${expRules2}
    Validate Rules Number Per NP  ${atipSession}  ${npId3}  ${expRules3}


TC001152526 Create Delete Multiple Groups with assigned NPs (create filter + assign NP ) 7330 with 3NPs
    [Tags]  hardware
    [Setup]  Clear Config
    createMultipeGroups  ${atipSession}  ${gNames}
    Create Filters Per Group Name From Dict  ${atipSession}  ${filtersToGroups}
    assignMultipeNpsToGroups  ${atipSession}  ${gNames}  ${npIds}
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules1}
    Validate Rules Number Per NP  ${atipSession}  ${npId2}  ${expRules2}
    Validate Rules Number Per NP  ${atipSession}  ${npId3}  ${expRules3}

TC001153112 Create Delete Multiple Groups with same NP assigned (create filter + assign NP ) 7300/V1/ATIP
    [Setup]  Clear Config
    createMultipeGroups  ${atipSession}  ${gNames}
    Create Filters Per Group Name From Dict  ${atipSession}  ${filtersToGroups}
    validateNPMoveGroup  ${atipSession}  ${gName1}  ${npId1}
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules1}
    validateNPMoveGroup  ${atipSession}  ${gName2}  ${npId1}
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules2}
    validateNPMoveGroup  ${atipSession}  ${gName3}  ${npId1}
    Validate Rules Number Per NP  ${atipSession}  ${npId1}  ${expRules3}


*** Keywords ***
Configure ATIP And Login
    login  ${atipSession}
    &{filtersToGroups}=   Create Dictionary    ${Filter1DefaultGroup}=${defaultG}
    ...  ${Filter1Group1}=${gName1}
    ...  ${Filter1Group2}=${gName2}
    ...  ${Filter2Group2}=${gName2}
    ...  ${Filter1Group3}=${gName3}
    ...  ${Filter2Group3}=${gName3}
    ...  ${Filter3Group3}=${gName3}
    set suite variable  &{filtersToGroups}  &{filtersToGroups}

Clear Config
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    ${npId1}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId1}  ${npId1}

Create Filters Per Group Name
    [Arguments]  ${atipSession}  ${filterObject}  ${groupName}
    ${groupId}=  getGroupId  ${atipSession}  ${groupName}
    ${filterObject.groupId}=  set variable  ${groupId}
    createFilter  ${atipSession}  ${filterObject}
    rulePush  ${atipSession}  ${groupId}
    waitUntilSystemReady  ${atipSession}

Create Filters Per Group Name From Dict
    [Arguments]  ${atipSession}  ${filtersToGroups}
    :FOR    ${key}    IN    @{filtersToGroups.keys()}
        \  Log  ${key}
        \  ${value}=    Get From Dictionary    ${filtersToGroups}    ${key}
        \  Log  ${key}
        \  Log  ${value}
        \  ${groupId}=  getGroupId  ${atipSession}  ${value}
        \  Log  ${groupId}
        \  Add GroupId To Filter  ${key}  ${groupId}
        \  createFilter  ${atipSession}  ${key}
        \  rulePush  ${atipSession}  ${groupId}
        \  waitUntilSystemReady  ${atipSession}

Validate Rules Number Per NP
    [Arguments]  ${atipSession}  ${networkProcessorId}  ${expectedRules}
    verifyNpRules  ${atipSession}  ${networkProcessorId}  ${expectedRules}

Add GroupId To Filter
    [Arguments]  ${filterObject}  ${groupId}
    ${filterObject.groupId}=  set variable  ${groupId}
