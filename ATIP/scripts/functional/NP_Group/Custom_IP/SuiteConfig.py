from http import HTTPStatus
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig

class SuiteConfig(StatsDefaultConfig):
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    timeInterval = TimeInterval.FIVEMIN
    dashBoardType = DashboardType.MINIMIZED
    minimizedDashboard = DashboardType.MINIMIZED.statsLimit

    # Define groups and add them to the config
    g1 = GroupConfig(name='Test1')
    g2 = GroupConfig(name='Test2')
    g3 = GroupConfig(name='Test3')
    g4 = GroupConfig(name='Test4')
    atipConfig = AtipConfig()

    atipConfig.addGroup(g1)
    atipConfig.addGroup(g2)
    atipConfig.addGroup(g3)
    atipConfig.addGroup(g4)

    npId0 = str(StatsDefaultConfig.envVars.atipSlotNumber[0])
    if StatsDefaultConfig.envVars.atipType == hardware:
        npId1 = str(StatsDefaultConfig.envVars.atipSlotNumber[1])
        npId2 = str(StatsDefaultConfig.envVars.atipSlotNumber[2])
    
    #Create Geo Filters
    CustomGeoFilter1 = FilterConfig("Romania", groupId= 0)
    FilterConditionConfig.addGeoCondition(CustomGeoFilter1, GeoLocation.ROMANIA)
    CustomGeoFilter2 = FilterConfig("Malaysia", groupId=0)
    FilterConditionConfig.addGeoCondition(CustomGeoFilter2, GeoLocation.MALAYSIA)
    CustomGeoFilter3 = FilterConfig("Botswana", groupId=0)
    FilterConditionConfig.addGeoCondition(CustomGeoFilter3, GeoLocation.BOTSWANA)
    CustomGeoFilter4 = FilterConfig("Malaysia2", groupId=0)
    FilterConditionConfig.addGeoCondition(CustomGeoFilter4, GeoLocation.MALAYSIA)

    #specify BPS TestConfig
    bpsTestConfig = BpsTestConfig('IPCustomNP', 80)