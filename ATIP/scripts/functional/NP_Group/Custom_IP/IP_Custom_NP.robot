*** Settings ***
Documentation  Custom Geo/IP functionality

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.Logout

Library	   atipAutoFrwk.webApi.atip.CustomIP
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.config.atip.filter.FilterConditionConfig
Library    atipAutoFrwk.config.atip.filter.FilterConfig
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.services.atip.NPService
Library    BuiltIn

Force Tags  hardware
Suite Setup        Configure ATIP And Login
Suite Teardown     Clean ATIP

*** Test Cases ***
TC001167729_IP_Custom_NP_Group_Multiple
    [Tags]
    #Get groupId for NP Group: Test1
    ${gid1} =  getGroupId  ${atipSession}  Test1
    log  ${gid1}

    #Move NP 2 to Group Test1
    changeNPGroup  ${atipSession}  ${npId1}  ${gid1}
    waitUntilGroupReady  ${atipSession}  ${gid1}

    #Add customgeo filter to group Test1
    ${CustomGeoFilter1.groupId} =  set variable  ${gid1}
    createFilter  ${atipSession}  ${CustomGeoFilter1}
    rulePush  ${atipSession}  ${gid1}

    Log  Uploading Custom Geo file  console=true
    ${file}=  Set variable  ../../../frwk/src/atipAutoFrwk/data/atip/customgeo/IP_locations_restructured.csv
    uploadCustomIp  ${atipSession}  ${file}  ${gid1}

    #IP_Custom_NP_Group_Configure_Move
    #Get groupId for NP Group: Test2
    ${gid2} =  getGroupId  ${atipSession}  Test2
    log  ${gid2}

    #Add customgeo filter to group Test2
    ${CustomGeoFilter2.groupId} =  set variable  ${gid2}
    createFilter  ${atipSession}  ${CustomGeoFilter2}
    rulePush  ${atipSession}  ${gid2}

    Log  Uploading Custom Geo file  console=true
    ${file}=  Set variable  ../../../frwk/src/atipAutoFrwk/data/atip/customgeo/IP_locations_restructured1.csv
    uploadCustomIp  ${atipSession}  ${file}  ${gid2}

    #Move NP 3 to Group Test2
    changeNPGroup  ${atipSession}  ${npId2}   ${gid2}
    waitUntilGroupReady  ${atipSession}  ${gid2}

    #IP_Custom_NP_Group_Modify
    #Get groupId for NP Group: Test3
    ${gid3} =  getGroupId  ${atipSession}  Test3
    log  ${gid3}

    #Move NP 1 to Group Test3
    changeNPGroup  ${atipSession}  ${npId0}   ${gid3}
    waitUntilGroupReady  ${atipSession}  ${gid3}

    #Add customgeo filter to group Test1
    ${CustomGeoFilter3.groupId} =  set variable  ${gid3}
    createFilter  ${atipSession}  ${CustomGeoFilter3}
    rulePush  ${atipSession}  ${gid3}

    Log  Uploading Custom Geo file  console=true
    ${file}=  Set variable  ../../../frwk/src/atipAutoFrwk/data/atip/customgeo/IP_locations_restructured3.csv
    uploadCustomIp  ${atipSession}  ${file}  ${gid3}

    Log  Running BPS test: ${bpsTestConfig.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Log  Checking if Test1 filter exists in Top Filters Widget : ${CustomGeoFilter1.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${CustomGeoFilter1}  ${timeInterval}  ${dashBoardType}

    Log  Checking if Test2 filter exists in Top Filters Widget : ${CustomGeoFilter2.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${CustomGeoFilter2}  ${timeInterval}  ${dashBoardType}

    Log  Checking if Test3 filter exists in Top Filters Widget : ${CustomGeoFilter3.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${CustomGeoFilter3}  ${timeInterval}  ${dashBoardType}

    Log  Modifying Custom Geo file for group Test3  console=true
    ${file}=  Set variable  ../../../frwk/src/atipAutoFrwk/data/atip/customgeo/IP_locations_restructured.csv
    uploadCustomIp  ${atipSession}  ${file}  ${gid3}

    #Reset Stats
    resetStats  ${atipSession}

    Log  Running BPS test: ${bpsTestConfig.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Log  Checking if Test3 filter no longer exists in Top Filters Widget : ${CustomGeoFilter3.name}  console=true
    checkFilterDoesNotExistsInTopStats  ${atipSession}  ${CustomGeoFilter3}  ${timeInterval}  ${dashBoardType}

TC001167729_IP_Custom_NP_Group_MoveNP_Hw_vATIP
    [Tags]  virtual
    ${gid4} =  getGroupId  ${atipSession}  Test4
    log  ${gid4}

    #Add customgeo filter to group Test4
    ${CustomGeoFilter4.groupId} =  set variable  ${gid4}
    createFilter  ${atipSession}  ${CustomGeoFilter4}
    rulePush  ${atipSession}  ${gid4}

    Log  Uploading Custom Geo file  console=true
    ${file}=  Set variable  ../../../frwk/src/atipAutoFrwk/data/atip/customgeo/IP_locations_restructured1.csv
    uploadCustomIp  ${atipSession}  ${file}  ${gid4}

    #Move NP 1 to Group Test4
    changeNPGroup  ${atipSession}  ${npId0}   ${gid4}
    waitUntilGroupReady  ${atipSession}  ${gid4}

    Log  Running BPS test: ${bpsTestConfig.testName}  console=true
    atipAutoFrwk.services.GeneralService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Log  Checking if Test4 filter exists in Top Filters Widget : ${CustomGeoFilter4.name}  console=true
    checkFilterExistsInTopStats  ${atipSession}  ${CustomGeoFilter4}  ${timeInterval}  ${dashBoardType}


*** Keywords ***
Configure ATIP And Login
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    configureAtip  ${atipSession}   ${atipConfig}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}


Clean ATIP
    logout  ${atipSession}