from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.data.atip.NetflowStats import NetflowStats
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.data.atip.AtipType import AtipType

class SuiteConfig(object):
    netflowRecords = NetflowStats.DATA_RECORDS
    captureLocation = '/home/atip/captures/'
    envVars = Environment()

    atipSessions = TopStats.TOTAL_COUNT
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    hardware = AtipType.HARDWARE

    httpStatusOK = HTTPStatus.OK.value
    bpsConfig1 = "TC001201998_aol_facebook_netflix_china"
    bpsConfig12 = "TC001201999_aol_facebook_netflix_china2"
    bpsConfig2 = "TC001202000_4apps_china"
    bpsConfig22 = "TC001202001_facebook_china"
    bpsConfig23 = "TC004_facebook_china2"
    bpsConfig24 = "TC001202002_facebook_china_2NPs"

    gDefaultId = 0
    npId0 = str(envVars.atipSlotNumber[0])
    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[1])
        npId2 = str(envVars.atipSlotNumber[2])

