from atipAutoFrwk.config.atip.NetflowCollectorConfig import NetflowCollectorConfig
from atipAutoFrwk.webApi.atip.Netflow import Netflow
from scripts.functional.stats.lib.StatsDefaultConfig import StatsDefaultConfig
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.DataMaskingConfig import DataMaskingConfig
from atipAutoFrwk.config.atip.HeaderMask import HeaderMask
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig
from atipAutoFrwk.config.atip.Collector import Collector
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.webApi.atip.stats.Pie import Pie
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.services.atip.CaptureService import CaptureService
from atipAutoFrwk.services.atip.FileService import FileService
from atipAutoFrwk.webApi.atip.Capture import Capture
from atipAutoFrwk.config.atip.NetflowCardConfig import NetflowCardConfig
from atipAutoFrwk.services.atip.GroupsService import GroupsService
import ipaddress
import time


class FilterPriorityLib(object):

    def configureNetflow(self, webApiSession, netflowVersion, odid, ipMethod, dns, npId, groupId=0):

        npId = int(npId)
        netflowCards = [NetflowCardConfig()]
        netflowCollectors = [NetflowCollectorConfig()]
        netflowGlobalConfig = [NetflowConfig()]
        netflowGlobalConfig[0].enabled = True
        netflowGlobalConfig[0].version = netflowVersion
        netflowGlobalConfig[0].groupId = groupId

        # configure Netflow Card on ATIP HARDWARE
        if StatsDefaultConfig.envVars.atipType == AtipType.HARDWARE:
            netflowCards[0].cardList[npId].enabled = True
            netflowCards[0].cardList[npId].odid = odid
            netflowCards[0].cardList[npId].ipMethod = ipMethod
            netflowCards[0].cardList[npId].dns = dns
            netflowCards[0].cardList[npId].netmask = '255.255.255.0'
            netflowCards[0].cardList[npId].gw = '192.168.30.100'
            netflowCards[0].cardList[npId].ip_addr = '192.168.30.1'

        # configure Netflow Collectors
        firstCollectorIP = StatsDefaultConfig.envVars.netflowCollectorConnectionConfig.host
        secondCollectorIP = str(ipaddress.IPv4Address(firstCollectorIP) + 1)
        firstCollector = Collector(0, 100, 'Samples', True, 'UDP', 4739, firstCollectorIP)
        secondCollector = Collector(1, 100, 'Samples', True, 'UDP', 4739, secondCollectorIP)
        netflowCollectors[0].collectorList[0] = firstCollector
        netflowCollectors[0].collectorList[1] = secondCollector

        for netflowGlobal in netflowGlobalConfig:
            GroupsService.updateReference(netflowGlobal)
            if netflowGlobal.enabled == True:
                Netflow.updateGlobalConfig(webApiSession, netflowGlobal)

        for netflowCard in netflowCards:
            GroupsService.updateReference(netflowCard)
            if netflowCard.cardList[npId].enabled != '':
                Netflow.updateCard(webApiSession, netflowCard.cardList[npId],groupId=groupId)

        for netflowCollector in netflowCollectors:
            GroupsService.updateReference(netflowCollector)
            for crtCollector in netflowCollector.collectorList:
                if crtCollector.enabled:
                    Netflow.updateCollectorConfig(webApiSession, crtCollector, groupId=groupId)


    def configureFilterForward(self, filterName, forward, vlanId, appName=None, hdrMaskId=None, geo=None, priority=1000, netflow=False, collectors=None, groupId=0):
        filterConfig = FilterConfig(filterName)
        filterConfig.forward = forward
        filterConfig.groupId = groupId
        if hdrMaskId != None:
            filterConfig.addHeaderMaskUUIDS(hdrMaskId)
        if StatsDefaultConfig.envVars.atipType == AtipType.HARDWARE:
            filterConfig.addTransmitIds(vlanId)
        filterConfig.priority = priority
        filterConfig.netflow = netflow

        if not isinstance(appName, list):
            for member in ApplicationType.__members__.values():
                if member.appName == appName:
                    FilterConditionConfig.addAppCondition(filterConfig, member)
        else:
            # build list of AppType objects corresponding to appList
            appType = []
            for app in appName:
                for member in ApplicationType.__members__.values():
                    if member.appName == app:
                        appType.append(member)

            FilterConditionConfig.addAppCondition(filterConfig, appType)

        if geo:
            for member in GeoLocation.__members__.values():
                if member.countryCode == geo:
                    FilterConditionConfig.addGeoCondition(filterConfig, member)

        if collectors != None:
            filterConfig.addCollectors(collectors)

        return filterConfig

    @classmethod
    def getActualValue(cls, pieStatsContainer, app, typeStat):
        '''
        Return counter for specified statistic
        :param pieStatsContainer: all applications statistics
        :param app: application name
        :param typeStat: statistics field to get value for;
        :return: specific statistic value
        '''
        actualValue = 0
        pieData = pieStatsContainer.getList()
        for pieStatsItem in pieData:
            if pieStatsItem.getName() == app:
                if typeStat == TopStats.TOTAL_COUNT:
                    actualValue = pieStatsItem.getSessions()
                elif typeStat == TopStats.TOTAL_BYTES:
                    actualValue = pieStatsItem.getBytes()
                elif typeStat == TopStats.TOTAL_PKTS:
                    actualValue = pieStatsItem.getPkts()
                break

        return actualValue

    @classmethod
    def getAppTopStats(cls, webApiSession, applications, statType, testId, vlanId, filteringType, outFilename="", groupId=0):

        appStats = 0
        tsharkComm = ''

        topStats = Pie.getTopStats(webApiSession, StatsType.APPS, groupId=groupId)

        if not topStats:
            raise Exception('No statistics')

        if not isinstance(applications, list):
            applications = [applications]

        for item in topStats.getList():
            for app in applications:
                if item.getName() == app:
                    if app == 'aljazeera':
                        b = 'al-jazeera'
                        appStats = cls.getActualValue(topStats, b, statType)
                        tsharkComm = cls.filterPcap(app, testId, filteringType, vlanId, outFilename)
                    else:
                        appStats = cls.getActualValue(topStats, app, statType)
                        tsharkComm = cls.filterPcap(app, testId, filteringType, vlanId, outFilename)

        return [appStats, tsharkComm]

    @classmethod
    def filterPcap(cls, app, testId, filteringFormat, vlanId, outFilename=""):
        a = ''
        if app == 'owa':
            a = 'outlook'
            displayFilter = 'frame contains "{}"'.format(a)
            tsharkCommand = cls.configFilterCommand(testId, displayFilter, filteringFormat, vlanId, outFilename)
        else:
            displayFilter = 'frame contains "{}"'.format(app)
            tsharkCommand = cls.configFilterCommand(testId, displayFilter, filteringFormat, vlanId, outFilename)

        return tsharkCommand

    def filterCaptureNoTraffic(self, testId, vlanId="", outFilename=""):
        tsharkCommand = ""
        pcap = '{0}.pcap'.format(testId)
        resultFilename = testId
        if outFilename != "":
            resultFilename = outFilename

        if vlanId != "":
            tsharkCommand = "tshark -r {0} -Y 'vlan.id=={2}' > {1}.result".format(pcap, resultFilename, vlanId)
        else:
            tsharkCommand = "tshark -r {0} -Y 'ip' > {1}.result".format(pcap, resultFilename)

        return tsharkCommand

    # @classmethod
    # def getLatestCaptureAndUnzip(cls, webApiSession):
    #
    #     # get latest pcap zip file from ATIP
    #     zipLocation = CaptureService.getLatestCaptureFile(webApiSession)
    #
    #     # unzip pcap file and delete zip file
    #     pcapFolder = FileService.unzipFileToFolder(zipLocation)
    #
    #     return pcapFolder

    @classmethod
    def removePcapAndFolder(cls, webApiSession, pcapFolder):

        # remove PCAP folder
        FileService.deleteFolder(pcapFolder)

        # delete capture files from ATIP
        Capture.deleteCaptureFiles(webApiSession)

    def waitCaptureToFinish(self, webApiSession, filterId, captureTime):
        for i in range(int(captureTime)+1):
            status = Capture.captureRunningStatus(webApiSession)
            print('Waiting capture to finish for {}s'.format(i))
            if status and (i < int(captureTime)):
                time.sleep(1)
            elif i >= int(captureTime):
                Capture.controlCapture(webApiSession, filterId, False)
            else:
                break

    @classmethod
    def configFilterCommand(cls, testId, displayFilter, format="text", vlanId="", outFilename=""):
        tsharkCommand = ""
        pcap = '{0}.pcap'.format(testId)
        resultFilename = testId

        if outFilename != "":
            resultFilename = outFilename

        if format == "text":
            if vlanId != "":
                tsharkCommand = "tshark -r {0} -Y '{1} and vlan.id=={2}' > {3}.result".format(pcap, displayFilter, vlanId, resultFilename)
            else:
                tsharkCommand = "tshark -r {0} -Y '{1}' > {2}.result".format(pcap, displayFilter, resultFilename)
        elif format == "hex":
            tsharkCommand = "tshark -r {0} -Y '{1}' > {2}.result".format(pcap, displayFilter, resultFilename)
        else:
            raise Exception("Use correct filter capture parameter: text or hex value")

        return tsharkCommand

    @classmethod
    def transferAtipCapture(cls, webApiSession, envVars, localPcapFolder, remotePcap):

        files = FileService.getFolderFileList(localPcapFolder)
        localPcap = files[0]
        localPath = localPcapFolder + localPcap
        FileService.sftpCopyToLinuxBox(envVars, localPath, remotePcap, 'to')
        cls.removePcapAndFolder(webApiSession, localPcapFolder)












