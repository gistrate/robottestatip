*** Settings ***
Documentation  Filter Priority test suite.

Variables  SuiteConfig.py

Library    FilterPriorityLib.py
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.atip.DataMaskingService.DataMaskingService
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.webApi.atip.DataMasking
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.Netflow
Library    atipAutoFrwk.webApi.atip.stats.Pie
Library    atipAutoFrwk.webApi.atip.Capture
Library    atipAutoFrwk.services.atip.CaptureService
Library    atipAutoFrwk.services.atip.utility.IxFlowParsing
Library    atipAutoFrwk.webApi.atip.EngineInfo
Library    Telnet  timeout=30  prompt=$
Library    BuiltIn
Library    String

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP
Test Setup         Clear ATIP
Test Teardown      Close All Connections
Suite Teardown     Clear Linux conf

*** Test Cases ***

TC001201998
    [Tags]  hardware
    #[NP_Groups_7300][FP]Default Group and 1 New Group with the same Configuration (MoveNP_Config)

    configureDataMasking  ${atipSession}  ${True}  X  ${gDefaultId}
    ${headerMask}  configureHeaderMask  ${atipSession}  ${None}  myMask  6  L2  6  ${gDefaultId}

    configureNetflow  ${atipSession}  10  125  STATIC  8.8.8.8  ${npId0}  ${gDefaultId}

    ${hdrMaskId}=  Get Header Mask ID  myMask  ${gDefaultId}

    deleteAllFilters  ${atipSession}
    resetStats  ${atipSession}

    Configure Filters with Forward Data Masking and Netflow  facebook  aol  netflix  ${hdrMaskId}  CN  ${gDefaultId}
    Run traffic and validate Data Masking and Netflow  ${bpsConfig1}  facebook   aol    netflix    CN  1.8.*.*  ${gDefaultId}


    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    configureDataMasking  ${atipSession}  ${True}  X  ${g1id}
    ${headerMask}  configureHeaderMask  ${atipSession}  ${None}  myMask  6  L2  6  ${g1id}

    configureNetflow  ${atipSession}  10  125  STATIC  8.8.8.8  ${npId0}  ${g1id}

    ${hdrMaskId}=  Get Header Mask ID  myMask  ${g1id}

    Configure Filters with Forward Data Masking and Netflow  facebook  aol  netflix  ${hdrMaskId}  CN  ${g1id}
    Run traffic and validate Data Masking and Netflow  ${bpsConfig1}  facebook   aol    netflix    CN  1.8.*.*  ${g1id}

TC001201999
    [Tags]  hardware
    #[NP_Groups_7300][FP]2x New Groups with different NPs in each group (MoveNP_Config)
    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    createNewGroup  ${atipSession}  Group2
    ${g2id}  getGroupId  ${atipSession}  Group2

    moveNPsToGroup  ${atipSession}  Group2  ${npId1}

    configureDataMasking  ${atipSession}  ${True}  X  ${g1id}
    configureDataMasking  ${atipSession}  ${True}  S  ${g2id}
    ${headerMask}  configureHeaderMask  ${atipSession}  ${None}  MaskGroup1  6  L2  6  ${g1id}
    ${headerMask}  configureHeaderMask  ${atipSession}  ${None}  MaskGroup2  6  L2  6  ${g2id}

    configureNetflow  ${atipSession}  10  125  STATIC  8.8.8.8  ${npId0}  ${g1id}
    configureNetflow  ${atipSession}  10  200  STATIC  8.8.8.8  ${npId1}  ${g2id}

    ${hdrMask1Id}=  Get Header Mask ID  MaskGroup1  ${g1id}
    ${hdrMask2Id}=  Get Header Mask ID  MaskGroup2  ${g2id}

    Configure Filters with Forward Data Masking and Netflow  facebook  aol  netflix  ${hdrMask1Id}  CN  ${g1id}
    Configure Filters with Forward Data Masking and Netflow  facebook  aol  netflix  ${hdrMask2Id}  CN  ${g2id}
    Run traffic and validate Data Masking and Netflow  ${bpsConfig1}  facebook   aol    netflix    CN  1.8.*.*  ${g1id}
    Run traffic and validate Data Masking and Netflow  ${bpsConfig12}  facebook   aol    netflix    CN  1.8.*.*  ${g2id}

TC001202000
    [Tags]  hardware
    #[NP_Groups_7300][FP]2xNew Groups with different Filter Priority settings per group(MoveNP_Config)

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1
    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    configureDataMasking  ${atipSession}  ${True}  X  ${g1id}
    ${headerMask}  configureHeaderMask  ${atipSession}  ${None}  myMask  6  L2  6  ${g1id}
    ${hdrMaskId}=  Get Header Mask ID  myMask  ${g1id}

    deleteAllFilters  ${atipSession}
    Configure Filters with Forward and Data Masking  facebook   amazon     netflix  CN  ${hdrMaskId}  ${g1id}

    Run traffic and validate Forward and Data Masking  ${bpsConfig2}  facebook   amazon     netflix  ${g1id}

    createNewGroup  ${atipSession}  Group2
    ${g2id}  getGroupId  ${atipSession}  Group2
    moveNPsToGroup  ${atipSession}  Group2  ${npId1}

    configureDataMasking  ${atipSession}  ${True}  X  ${g2id}
    ${headerMask}  configureHeaderMask  ${atipSession}  ${None}  MaskGroup2  6  L2  6  ${g2id}

    configureNetflow  ${atipSession}  10  125  STATIC  8.8.8.8  ${npId1}  ${g2id}
    ${hdrMask2Id}=  Get Header Mask ID  MaskGroup2  ${g2id}

    Configure Filters with Forward Data Masking and Netflow  facebook  aol  netflix  ${hdrMask2Id}  CN  ${g2id}
    Run traffic and validate Data Masking and Netflow  ${bpsConfig12}  facebook   aol    netflix    CN  1.8.*.*  ${g2id}

TC001202001
    [Tags]  hardware
    #[NP_Groups_7300][FP]2xNew Groups with different Filter Priority Configuration (Config_MoveNP)

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    configureDataMasking  ${atipSession}  ${True}  X  ${g1id}
    ${headerMask}  configureHeaderMask  ${atipSession}  ${None}  MyMask  6  L2  6  ${g1id}
    ${hdrMaskId}=  Get Header Mask ID  MyMask  ${g1id}

    Configure Filters for PCAP  facebook  CN  ${hdrMaskId}  ${g1id}
    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    Run traffic and verify PCAP  ${bpsConfig22}  facebook  ${g1id}

    createNewGroup  ${atipSession}  Group2
    ${g2id}  getGroupId  ${atipSession}  Group2

    configureDataMasking  ${atipSession}  ${True}  S  ${g2id}
    ${headerMask}  configureHeaderMask  ${atipSession}  ${None}  MaskGroup2  6  L2  6  ${g2id}
    ${hdrMaskId}=  Get Header Mask ID  MaskGroup2  ${g2id}

    deleteAllFilters  ${atipSession}

    Configure Filters with Forward and Data Masking  facebook   amazon     netflix  CN  ${hdrMaskId}  ${g2id}

    moveNPsToGroup  ${atipSession}  Group2  ${npId0}

    Run traffic and validate Forward and Data Masking  ${bpsConfig2}  facebook   amazon     netflix  ${g2id}

TC001202002
     [Tags]  hardware
     #[NP_Groups_7300][FP]2xNew groups with multiple NPs per group, with different filter priority configuration

     createNewGroup  ${atipSession}  Group1
     ${g1id}  getGroupId  ${atipSession}  Group1
     moveNPsToGroup  ${atipSession}  Group1  ${npId0}
     moveNPsToGroup  ${atipSession}  Group1  ${npId1}

     configureDataMasking  ${atipSession}  ${True}  M  ${g1id}
     ${headerMask}  configureHeaderMask  ${atipSession}  ${None}  MyMask  6  L2  6  ${g1id}
     ${hdrMaskId}=  Get Header Mask ID  MyMask  ${g1id}

     Configure Filters for PCAP  facebook  CN  ${hdrMaskId}  ${g1id}

     Run traffic and verify PCAP  ${bpsConfig24}  facebook  ${g1id}

     createNewGroup  ${atipSession}  Group2
     ${g2id}  getGroupId  ${atipSession}  Group2

     moveNPsToGroup  ${atipSession}  Group2  ${npId0}
     moveNPsToGroup  ${atipSession}  Group2  ${npId1}

     configureDataMasking  ${atipSession}  ${True}  X  ${g2id}
     ${headerMask}  configureHeaderMask  ${atipSession}  ${None}  MyMask  6  L2  6  ${g2id}
     ${hdrMaskId}=  Get Header Mask ID  MyMask  ${g2id}

     configureNetflow  ${atipSession}  10  125  STATIC  8.8.8.8  ${npId0}  ${g2id}

     Configure Filters with Forward Data Masking and Netflow  facebook  aol  netflix  ${hdrMaskId}  CN  ${g2id}

     Run traffic and validate Data Masking and Netflow  ${bpsConfig1}  facebook   aol    netflix    CN  1.8.*.*  ${g2id}

     moveNPsToGroup  ${atipSession}  Group1  ${npId0}
     moveNPsToGroup  ${atipSession}  Group1  ${npId1}

     resetStats  ${atipSession}
     Run traffic and verify PCAP  ${bpsConfig24}  facebook  ${g1id}

TC001202004
    [Tags]  hardware
    #[NP_Groups_V1][FP]One new group with one NP and different filter priority configuration (MoveNP_Config)

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1
    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    configureDataMasking  ${atipSession}  ${True}  X  ${g1id}
    ${headerMask}  configureHeaderMask  ${atipSession}  ${None}  myMask  6  L2  6  ${g1id}
    ${hdrMaskId}=  Get Header Mask ID  myMask  ${g1id}

    deleteAllFilters  ${atipSession}
    Configure Filters with Forward and Data Masking  facebook   amazon     netflix  CN  ${hdrMaskId}  ${g1id}

    Run traffic and validate Forward and Data Masking  ${bpsConfig2}  facebook   amazon     netflix  ${g1id}

TC001202005
    [Tags]  virtual
    #NP_Groups_vATIP][FP]Default Group and one new group with different filter priority configuration

    configureDataMasking  ${atipSession}  ${TRUE}  X  ${gDefaultId}

    createHeaderMask  ${atipSession}  srcMac  6  L2  6  ${gDefaultId}
    createHeaderMask  ${atipSession}  dstMac  6  L2  0  ${gDefaultId}

    ${hdrMaskId1}=  Get Header Mask ID  srcMac  ${gDefaultId}
    ${hdrMaskId2}=  Get Header Mask ID  dstMac  ${gDefaultId}

    Configure Filters high priority vATIP  facebook  aol  CN  ${hdrMaskId1}  ${hdrMaskId2}  ${gDefaultId}

    Run traffic and verify high priority vATIP  ${bpsConfig1}  facebook  aol  netflix  ${gDefaultId}

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    configureDataMasking  ${atipSession}  ${TRUE}  R  ${g1id}

    createHeaderMask  ${atipSession}  srcMac  6  L2  6  ${g1id}
    createHeaderMask  ${atipSession}  dstMac  6  L2  0  ${g1id}

    ${hdrMaskId1}=  Get Header Mask ID  srcMac  ${g1id}
    ${hdrMaskId2}=  Get Header Mask ID  dstMac  ${g1id}

    Configure Filters high priority vATIP  facebook  aol  CN  ${hdrMaskId1}  ${hdrMaskId2}  ${g1id}

    Run traffic and verify high priority vATIP  ${bpsConfig1}  facebook  aol  netflix  ${g1id}


*** Keywords ***

Configure Filters for PCAP
    [Arguments]  ${app}  ${geo}  ${hdrMaskId}  ${gId}

    ${filterConfig}=  configureFilterForward  Filter11  ${False}  0  ${None}  ${None}  ${geo}  1  ${False}  ${None}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}
    ${filterConfig}=  configureFilterForward  Filter22  ${True}  100  ${app}  ${hdrMaskId}  ${None}  2  ${False}  ${None}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${gId}

Run traffic and verify PCAP
    [Arguments]  ${bpsFile}  ${app}  ${gId}

    # start capture on linux box
    ${index1}  Connect on Linux Box
    Write  cd captures
    Become SU 
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}_1.pcap -B 30720
    Read Until  ${envVars.linuxConnectionConfig.port}

    # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  ${index1}

    ${index2}  Connect on Linux Box
    Write  cd captures
    Become SU
    validate capture  ${app}  text  100  ${TEST NAME}_1  ${atipSessions}  ${TEST NAME}_1  ${gId}

    resetStats  ${atipSession}

    # perform captures
    # get filter id
    ${filter}=  getFilterId  ${atipSession}  Filter11

    # delete capture files from ATIP
    deleteCaptureFiles  ${atipSession}

    # start capture on ATIP
    controlCapture  ${atipSession}  ${filter}  ${True}

    # start capture on linux box
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}_2.pcap -B 30720
    Read Until  ${envVars.linuxConnectionConfig.port}

    # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  ${index2}

    Connect on Linux Box
    Write  cd captures
    Become SU
    Validate No Traffic In Capture  ${TEST NAME}_2  100  ${TEST NAME}_2
    # stop capture on ATIP
    controlCapture  ${atipSession}  ${filter}  ${False}

    # download last PCAP and analyze
    ${localPcapFolder}=  getLatestCaptureAndUnzip  ${atipSession}
    transferAtipCapture  ${atipSession}  ${envVars}  ${localPcapFolder}  ${captureLocation}${TEST NAME}_3.pcap
    ${displayFilter}=  catenate  frame contains  "${app}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}_3  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_3_1
    Execute Command  ${filteringCommand}
    ${output}=  Execute Command  wc -l < ${TEST NAME}_3_1.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Not Be Equal As Integers  ${statsCaptureLB}  0


Configure Filters with Forward and Data Masking
    [Arguments]  ${app1}  ${app2}  ${app3}  ${geo}  ${hdrMaskId}  ${gId}

    ${filterConfig}=  configureFilterForward  Filter1  ${True}  100  ${app1}  ${None}  ${None}  1  ${False}  ${None}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}

    @{appList}=  create list  ${app1}  ${app2}
    ${filterConfig}=  configureFilterForward  Filter2  ${True}  200  ${appList}  ${hdrMaskId}  ${None}  2  ${False}  ${None}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}

    ${filterConfig}=  configureFilterForward  Filter3  ${True}  300  ${None}  ${None}  ${geo}  3  ${False}  ${None}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}

    # push changes to NP
    rulePush  ${atipSession}  ${gId}

Run traffic and validate Forward and Data Masking
    [Arguments]  ${bpsFile}  ${app1}  ${app2}  ${app3}  ${gId}

    # Start traffic capture on linux box interface
    ${index1}  Connect on Linux Box
    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}_1.pcap
    Read Until  ${envVars.linuxConnectionConfig.port}

     # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  ${index1}

    ${index2}  Connect on Linux Box
    Become SU
    Write  cd captures

    validate capture  ${app1}  text  100  ${TEST NAME}_1  ${atipSessions}  ${TEST NAME}_1_1  ${gId}
    validate capture  ${app2}  text  200  ${TEST NAME}_1  ${atipSessions}  ${TEST NAME}_1_2  ${gId}
    validate capture  ${app3}  text  300  ${TEST NAME}_1  ${atipSessions}  ${TEST NAME}_1_3  ${gId}

    resetStats  ${atipSession}

    # change priorities
    updateFilterPriority  ${atipSession}  Filter1  2  ${gId}
    updateFilterPriority  ${atipSession}  Filter2  1  ${gId}
    updateFilterPriority  ${atipSession}  Filter3  3  ${gId}

    # start capture on linux box
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}_2.pcap
    Read Until  ${envVars.linuxConnectionConfig.port}

     # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  ${index2}

    ${index3}  Connect on Linux Box
    Write  cd captures
    Become SU
    validate capture  ${app1}  text  200  ${TEST NAME}_2  ${atipSessions}  ${TEST NAME}_2_1  ${gId}
    validate capture  ${app2}  text  200  ${TEST NAME}_2  ${atipSessions}  ${TEST NAME}_2_2  ${gId}
    Validate No Traffic In Capture  ${TEST NAME}_2  100  ${TEST NAME}_2_3
    validate capture  ${app3}  text  300  ${TEST NAME}_2  ${atipSessions}  ${TEST NAME}_2_4  ${gId}

    resetStats  ${atipSession}

    # change priorities
     updateFilterPriority  ${atipSession}  Filter1  2  ${gId}
     updateFilterPriority  ${atipSession}  Filter2  3  ${gId}
     updateFilterPriority  ${atipSession}  Filter3  1  ${gId}

    # start capture on linux box
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}_3.pcap
    Read Until  ${envVars.linuxConnectionConfig.port}

    # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  ${index3}

    Connect on Linux Box
    Write  cd captures
    Become SU
    validate capture  ${app1}  text  300  ${TEST NAME}_3  ${atipSessions}  ${TEST NAME}_3_1  ${gId}
    validate capture  ${app2}  text  300  ${TEST NAME}_3  ${atipSessions}  ${TEST NAME}_3_2  ${gId}
    validate capture  ${app3}  text  300  ${TEST NAME}_3  ${atipSessions}  ${TEST NAME}_3_3  ${gId}
    Validate No Traffic In Capture  ${TEST NAME}_3  100  ${TEST NAME}_3_4
    Validate No Traffic In Capture  ${TEST NAME}_3  200  ${TEST NAME}_3_5

Configure Filters with Forward Data Masking and Netflow
    [Arguments]  ${app1}  ${app2}  ${app3}  ${hdrMaskId}  ${geo}  ${gId}

    # get Netflow collectors IDs
    ${firstCollectorId}=  Get Netflow Collector ID  0  ${gId}
    ${secondCollectorId}=  Get Netflow Collector ID  1  ${gId}

    ${filterConfig}=  configureFilterForward  Filter1  ${True}  100  ${app1}  ${None}  ${None}  1  ${False}  ${None}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}

    ${filterConfig}=  configureFilterForward  Filter2  ${True}  200  ${app2}  ${hdrMaskId}  ${None}  2  ${False}  ${None}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}

    ${filterConfig}=  configureFilterForward  Filter3  ${True}  300  ${None}  ${None}  ${geo}  3  ${True}  ${firstCollectorId}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}

    ${filterConfig}=  configureFilterForward  Filter4  ${False}  400  ${app3}  ${None}  ${None}  4  ${False}  ${None}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}

    @{appList}=  create list  ${app1}  ${app2}  ${app3}
    ${filterConfig}=  configureFilterForward  Filter5  ${True}  500  ${appList}  ${None}  ${None}  5  ${True}  ${secondCollectorId}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}

    # push changes to NP
    rulePush  ${atipSession}  ${gId}

Run traffic and validate Data Masking and Netflow
    [Arguments]  ${bpsFile}  ${app1}  ${app2}  ${app3}  ${geo}  ${ip}  ${gId}

    # Start traffic capture on linux box interface
    ${index1}  Connect on Linux Box

    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap -B 30720
    Read Until  ${envVars.linuxConnectionConfig.port}

    # Start traffic capture on netflow collector interface
    ${index2}  Connect on Linux Box
    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.netflowCollectorConnectionConfig.port} -w ${TEST NAME}_netflow.pcap -B 30720
    Read Until  ${envVars.netflowCollectorConnectionConfig.port}

    # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    # stop captures manually on each telnet connection, configuring them to auto finish did not correlate well with bps traffic run each time
    Stop Capture  ${index1}
    Stop Capture  ${index2}

    # display debug page statistics
    ${debugStats}=  readDebug  ${atipSession}
    Log  ${debugStats}

    # Filter capture file and export matching packets to a result file for validation
    ${index1}  Connect on Linux Box
    Become SU
    Execute Command  mv ./parsing.py ./captures  strip_prompt=True
    Write  cd captures
    Read Until Prompt

    validate capture  ${app1}  text  100  ${TEST NAME}  ${atipSessions}  ${TEST NAME}_1  ${gId}
    validate capture  ${app2}  text  200  ${TEST NAME}  ${atipSessions}  ${TEST NAME}_2  ${gId}

    # validate ip in capture
    ${displayFilter}=  catenate  not ip.host matches ${ip}
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}  ${displayFilter}  text  300  ${TEST NAME}_3
    Execute Command  ${filteringCommand}
    matches validation  ${TEST NAME}_3  0

    validate capture  ${app3}  text  300  ${TEST NAME}  ${atipSessions}  ${TEST NAME}_4  ${gId}
    Validate No Traffic In Capture  ${TEST NAME}  400  ${TEST NAME}_5

    ${firstCollector}=  Get Netflow Collector IP  0  ${gId}
    ${secondCollector}=  Get Netflow Collector IP  1  ${gId}

    Validate Apps in Netflow Capture  ${firstCollector}  ${secondCollector}  ${TEST NAME}  ${app1}  ${app2}  ${app3}  China  ${gId}
    Write  rm TC*

Configure Filters high priority vATIP
    [Arguments]  ${app1}  ${app2}  ${geo}  ${hdrMask1}  ${hdrMask2}  ${gId}

    deleteAllFilters  ${atipSession}

    ${filterConfig}=  configureFilterForward  Filter1  ${False}  100  ${None}  ${None}  ${geo}  1  ${False}  ${None}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}
    ${filterConfig}=  configureFilterForward  Filter2  ${True}  200  ${app1}  ${hdrMask1}  ${None}  2  ${False}  ${None}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}
    ${filterConfig}=  configureFilterForward  Filter3  ${True}  300  ${app2}  ${hdrMask2}  ${None}  3  ${False}  ${None}  ${gId}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}  ${gId}

Run traffic and verify high priority vATIP
    [Arguments]  ${bpsFile}  ${app1}  ${app2}  ${app3}  ${gId}

    ${filter}=  getFilterId  ${atipSession}  Filter1

    ## delete capture files from ATIP
    deleteCaptureFiles  ${atipSession}

    ## start capture on ATIP
    controlCapture  ${atipSession}  ${filter}  ${True}

    ## start capture on linux box
    ${index1}  Connect on Linux Box
    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap
    Read Until  ${envVars.linuxConnectionConfig.port}

    ## run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    ## stop capture on ATIP and linux box
    controlCapture  ${atipSession}  ${filter}  ${False}
    Stop Capture  ${index1}

    ## wait capture to stop
    waitCaptureToFinish  ${atipSession}  ${filter}  180

    ${displayFilter}=  catenate  frame contains "${app1}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_1
    ${index2}  Connect on Linux Box
    Write  cd captures
    Become SU
    Execute Command  ${filteringCommand}
    matches validation  ${TEST NAME}_1  0

    ${displayFilter}=  catenate  frame contains "${app2}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_2
    Execute Command  ${filteringCommand}
    matches validation  ${TEST NAME}_2  0

    ## download last PCAP and analyze for each app
    ${localPcapFolder}=  getLatestCaptureAndUnzip  ${atipSession}
    transferAtipCapture  ${atipSession}  ${envVars}  ${localPcapFolder}  ${captureLocation}${TEST NAME}_1.pcap
    ${displayFilter}=  catenate  http.host contains "${app1}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}_1  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_1_1
    Execute Command  ${filteringCommand}
    ${output}=  Execute Command  wc -l < ${TEST NAME}_1_1.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Not Be Equal As Integers  ${statsCaptureLB}  0

    ${displayFilter}=  catenate  http.host contains "${app2}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}_1  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_1_2
    Execute Command  ${filteringCommand}
    ${output}=  Execute Command  wc -l < ${TEST NAME}_1_2.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Not Be Equal As Integers  ${statsCaptureLB}  0

    ${displayFilter}=  catenate  http.host contains "${app3}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}_1  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_1_3
    Execute Command  ${filteringCommand}
    ${output}=  Execute Command  wc -l < ${TEST NAME}_1_3.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Not Be Equal As Integers  ${statsCaptureLB}  0


Get Header Mask ID
    [Arguments]  ${maskName}  ${gId}
    ${hdrMaskConfig}=  getHeaderMaskByName  ${atipSession}  ${maskName}  ${gId}
    ${hdrMask}=  evaluate  ${hdrMaskConfig}.get('masks')
    ${hdrMaskId}=  evaluate  ${hdrMask[0]}.get('id')

    [return]  ${hdrMaskId}

Get Netflow Collector ID
    [Arguments]  ${index}  ${gId}
    ${netflowCollectors}=  getCollectorConfig  ${atipSession}  ${httpStatusOK}  ${gId}
    Log  ${netflowCollectors}
    ${collectors}=  evaluate  ${netflowCollectors}.get('collectors')
    ${collectorConfig}=  evaluate  ${collectors}[${index}]
    ${collectorId}=  evaluate  ${collectorConfig}.get('id')

    [return]  ${collectorId}

Get Netflow Collector IP
    [Arguments]  ${index}  ${gId}
    ${netflowCollectors}=  getCollectorConfig  ${atipSession}  ${httpStatusOK}  ${gId}
    Log  ${netflowCollectors}
    ${collectors}=  evaluate  ${netflowCollectors}.get('collectors')
    ${collectorConfig}=  evaluate  ${collectors}[${index}]
    ${collectorIp}=  evaluate  ${collectorConfig}.get('collector')

    [return]  ${collectorIp}

Validate Apps in Netflow Capture
    [Arguments]  ${ipCollector1}  ${ipCollector2}  ${STRING}  ${app1}  ${app2}  ${app3}  ${geo}  ${gId}

    ${records1} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${ipCollector1}  ${None}  ${gId}
    ${records2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${ipCollector2}  ${None}  ${gId}

    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.l7-application-name,cflow.pie.ixia.source-ip-country-name"

    ${response}=  Execute Command  python parsing.py ${STRING}_netflow.pcap ${STRING}_netflow.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    ${value1}=  Get From Dictionary  ${dict}  ${app1}
    ${value2}=  Get From Dictionary  ${dict}  ${app2}
    ${value3}=  Get From Dictionary  ${dict}  ${app3}
    ${value4}=  Get From Dictionary  ${dict}  ${geo}
    ${total}=  evaluate  ${value1}+${value2}+${value3}
    ${totalRecords}=  evaluate  ${records1}+${records2}
    should be equal as integers  ${value4}  ${total}
    should be equal as integers  ${value4}  ${totalRecords}

Validate Capture
    [Arguments]  ${app}  ${filteringType}  ${vlanId}  ${STRING}  ${statsType}  ${outFilename}  ${gId}

    # get top apps
    @{appStats}=  getAppTopStats  ${atipSession}  ${app}  ${statsType}  ${STRING}  ${vlanId}  ${filteringType}  ${outFilename}  ${gId}

    Execute Command  ${appStats[1]}  strip_prompt=True

    matches validation  ${outFilename}  ${appStats[0]}


Validate No Traffic In Capture
    [Arguments]  ${STRING}  ${vlanId}  ${outFilename}

    ${filteringCommand} =  filterCaptureNoTraffic  ${STRING}  ${vlanId}  ${outFilename}
    Execute Command  ${filteringCommand}
    matches validation  ${outFilename}  0

Matches Validation
    [Arguments]  ${STRING}  ${expectedValue}
    sleep  2
    ${output}=  Execute Command  wc -l < ${STRING}.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Be Equal As Integers  ${statsCaptureLB}  ${expectedValue}

Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [return]  ${output}

Stop Capture
    [Arguments]  ${index}
    Switch Connection  ${index}
    Write Control Character     BRK
    Read Until Prompt
    Close Connection

Connect on Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Read
    [return]  ${openTelnet}

Become SU
    Write  sudo su
    sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt

Configure ATIP
    atipLogin.Login  ${atipSession}

    Connect on Linux Box
    Become SU
    Write  rm -rf captures
    Read Until Prompt
    Write  exit
    Read Until  $
    Sleep  1s
    # create temp folder for testing captures; it should be owned by "atip" user
    Write  mkdir captures
    Read Until  $
    Sleep  1s
    Close Connection

Clear ATIP
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}
    Connect on Linux Box
    Write  cd captures
    Become SU
    Write  rm -rf TC*

    ${command} =  copyFileToLinux
    Write  ${command}

    Close Connection

Clear Linux conf
    Connect on Linux Box
    Become SU
    Execute Command  rm -rf captures
    Close Connection
    atipLogout.Logout  ${atipSession}