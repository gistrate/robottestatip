*** Settings ***

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.services.GeneralService  WITH NAME  General
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.atip.stats.ValidateSSLStatsService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.services.traffic.BpsStatsService.BpsStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    Collections
Library    atipAutoFrwk.services.atip.stats.TopFiltersService

Default Tags  ATIP  functional  hardware

Suite Setup        Login ATIP
Test Setup         Clear Config
Suite Teardown     logout  ${atipSession}

*** Test Cases ***

TC001166223 Default Group and 1 New Group with different SSL conf and filters

    ${name}=  Set variable  BreakingPoint_serverA

    updateSslSettings  ${atipSession}  True  ${group0Id}

    uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert512}  ${fileKey512}  ${group0Id}

    Create Filter on Group  ${Filter1Decrypted}  ${group0Id}

    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigSsl}

    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTestConfigSsl}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${bpsTotalSessions}

    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  10  ${group0Id}
    checkTargetExistsInTopStats  ${atipStats}  ${strFacebook}

    checkFilterExistsInTopStats  ${atipSession}  ${Filter1Decrypted}  ${timeInterval}  ${minDashboard}

    createNewGroup  ${atipSession}  Group1

    ${g1id}  getGroupId  ${atipSession}  Group1

    updateSslSettings  ${atipSession}  True  ${g1id}
    uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert1024}  ${fileKey1024}  ${g1id}

    ${Filter1Group1.groupId}=  Set variable  ${g1id}
    Create Filter on Group  ${Filter1Group1}  ${g1id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest1ConfigG1}

    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTest1ConfigG1}
    ${bpsTotalSessions}=  convert to integer  ${bpsTotalSessions}
    ${EncryptedSessions}=  Evaluate  ${bpsTotalSessions}/3
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${EncryptedSessions}  ${g1id}

    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  10  ${g1id}
    checkTargetExistsInTopStats  ${atipStats}  ${strESPN}
    checkFilterExistsInTopStats  ${atipSession}  ${Filter1Group1}  ${timeInterval}  ${minDashboard}

TC001166368 Default Group and 1 New Group with different SSL Configuration and Filters (MoveNP_Config)

    ${name}=  Set variable  BreakingPoint_serverA
    uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert2048}  ${fileKey2048}  ${group0Id}
    updateSslSettings  ${atipSession}  True

    Create Filter on Group  ${Filter2GroupDefault}  ${group0Id}

    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest2Config1}

    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  10  ${group0Id}
    checkTargetDoesNotExistInTopStats  ${atipStats}  ${strFacebook}
    checkTargetDoesNotExistInTopStats  ${atipStats}  ${strESPN}
    checkTargetExistsInTopStats  ${atipStats}  ${strNetflix}

    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTest2Config1}
    ${bpsTotalSessions}=  convert to integer  ${bpsTotalSessions}
    ${EncryptedSessions}=  Evaluate  ${bpsTotalSessions}/3
    ${EncryptedSessions}=  convert to integer  ${EncryptedSessions}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${EncryptedSessions}
    checkFilterExistsInTopStats  ${atipSession}  ${Filter2GroupDefault}  ${timeInterval}  ${minDashboard}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${Filter2GroupDefault}  ${EncryptedSessions}  ${timeInterval}  ${group0Id}  ${minDashboard}  ${relativeTolerance}

    createNewGroup  ${atipSession}  Group1
    ${g1id}  getGroupId  ${atipSession}  Group1
    ${Filter2Group1.groupId}=  Set variable  ${g1id}

    uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert512}  ${fileKey512}  ${g1id}
    updateSslSettings  ${atipSession}  True  ${g1id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId1}
    moveNPsToGroup  ${atipSession}  Group1  ${npId2}

    Create Filter on Group  ${Filter2Group1}  ${g1id}

    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest2ConfigG1}

    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  10  ${g1id}
    checkTargetDoesNotExistInTopStats  ${atipStats}  ${strFacebook}
    checkTargetDoesNotExistInTopStats  ${atipStats}  ${strNetflix}
    checkTargetExistsInTopStats  ${atipStats}  ${strESPN}

    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTest2ConfigG1}
    ${bpsTotalSessions}=  convert to integer  ${bpsTotalSessions}
    ${EncryptedSessions}=  Evaluate  ${bpsTotalSessions}/3
    ${EncryptedSessions}=  convert to integer  ${EncryptedSessions}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${EncryptedSessions}  ${g1id}
    checkFilterExistsInTopStats  ${atipSession}  ${Filter2Group1}  ${timeInterval}  ${minDashboard}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${Filter2Group1}  ${EncryptedSessions}  ${timeInterval}  ${g1id}  ${minDashboard}  ${relativeTolerance}
    resetStats  ${atipSession}
    modifyCert  ${atipSession}  ${g1id}  ${name}  ${False}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest2ConfigG1}

    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  10  ${g1id}

    checkTargetDoesNotExistInTopStats  ${atipStats}  ${strESPN}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  0  ${g1id}

TC001166369: 2xNew Groups with multiple NPs per group with different SSL Configuration and Filters (Config_MoveNP)


    createNewGroup  ${atipSession}  Group1
    createNewGroup  ${atipSession}  Group2

    ${g1id}  getGroupId  ${atipSession}  Group1
    ${g2id}  getGroupId  ${atipSession}  Group2

    ${Filter2Decrypted.groupId}=  Set variable  ${g1id}
    ${Filter1Group2.groupId}=  Set variable  ${g2id}
    ${Filter2Group2.groupId}=  Set variable  ${g2id}

    updateSslSettings  ${atipSession}  True  ${g1id}
    updateSslSettings  ${atipSession}  True  ${g2id}

    ${name}=  Set variable  BreakingPoint1
    ${name1}=  Set variable  BreakingPoint2

    uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert1024}  ${fileKey1024}  ${g1id}

    Create Filter on Group  ${Filter2Decrypted}  ${g1id}
    moveNPsToGroup  ${atipSession}  Group1  ${npId2}

    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest3ConfigG1}

    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  10  ${g1id}
    checkTargetDoesNotExistInTopStats  ${atipStats}  ${strFacebook}
    checkTargetDoesNotExistInTopStats  ${atipStats}  ${strNetflix}
    checkTargetExistsInTopStats  ${atipStats}  ${strESPN}

    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTest3ConfigG1}
    ${bpsTotalSessions}=  convert to integer  ${bpsTotalSessions}
    ${EncryptedSessions}=  Evaluate  ${bpsTotalSessions}/3
    ${EncryptedSessions}=  convert to integer  ${EncryptedSessions}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${EncryptedSessions}  ${g1id}
    checkFilterExistsInTopStats  ${atipSession}  ${Filter2Decrypted}  ${timeInterval}  ${minDashboard}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${Filter2Decrypted}  ${EncryptedSessions}  ${timeInterval}  ${g1id}  ${minDashboard}  ${relativeTolerance}
    deleteAllCerts  ${atipSession}  ${g1id}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest3ConfigG1}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  0  ${g1id}

    uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert512}  ${fileKey512}  ${g2id}
    uploadSeparateFiles  ${atipSession}  ${name1}  ${fileCert1024}  ${fileKey1024}  ${g2id}

    moveNPsToGroup  ${atipSession}  Group2  ${npId0}
    moveNPsToGroup  ${atipSession}  Group2  ${npId1}

    updateSslSettings  ${atipSession}  True  ${g2id}

    Create Filter on Group  ${Filter1Group2}  ${g2id}
    Create Filter on Group  ${Filter2Group2}  ${g2id}

    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest3ConfigG2}

    #Verify that each traffic is decrypted
    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTest3ConfigG2}
    ${bpsTotalSessions}=  convert to integer  ${bpsTotalSessions}
    ${EncryptedSessions}=  Evaluate  ${bpsTotalSessions}*2/3
    ${EncryptedSessions}=  convert to integer  ${EncryptedSessions}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${EncryptedSessions}  ${g2id}
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  10  ${g2id}
    checkFilterExistsInTopStats  ${atipSession}  ${Filter1Group2}  ${timeInterval}  ${minDashboard}
    checkFilterExistsInTopStats  ${atipSession}  ${Filter2Group2}  ${timeInterval}  ${minDashboard}
    checkTargetExistsInTopStats  ${atipStats}  ${strESPN}
    checkTargetExistsInTopStats  ${atipStats}  ${strFacebook}
    checkTargetDoesNotExistInTopStats  ${atipStats}  ${strNetflix}

    ${DecryptedSessions}=  Evaluate  ${bpsTotalSessions}/3
    ${DecryptedSessions}=  convert to integer  ${DecryptedSessions}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${Filter1Group2}  ${DecryptedSessions}  ${timeInterval}  ${g2id}  ${minDashboard}  ${relativeTolerance}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${Filter2Group2}  ${DecryptedSessions}  ${timeInterval}  ${g2id}  ${minDashboard}  ${relativeTolerance}
    modifyCert  ${atipSession}  ${g2id}  ${name}  ${False}
    modifyCert  ${atipSession}  ${g2id}  ${name1}  ${False}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest3ConfigG2}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  0  ${g2id}

    modifyCert  ${atipSession}  ${g2id}  ${name}
    modifyCert  ${atipSession}  ${g2id}  ${name1}

    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest3ConfigG2}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${EncryptedSessions}  ${g2id}

TC001166372:[V1] Default Group and 1 New Group with different SSL Configuration and Filters (Config_MoveNP)

    ${name}=  Set variable  BreakingPoint_serverA

    updateSslSettings  ${atipSession}  True

    uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert2048}  ${fileKey2048}

    ${Filter3Decrypted.groupId}=  Set variable  0
    Create Filter on Group  ${Filter3Decrypted}  ${group0Id}

    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigV1}

    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTestConfigV1}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${bpsTotalSessions}

    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  10  ${group0Id}
    checkTargetExistsInTopStats  ${atipStats}  ${strFacebook}
    checkTargetExistsInTopStats  ${atipStats}  ${strESPN}
    checkTargetExistsInTopStats  ${atipStats}  ${strNetflix}

    checkFilterExistsInTopStats  ${atipSession}  ${Filter3Decrypted}  ${timeInterval}  ${minDashboard}

    checkAtipTopStatsTotalSessions  ${atipSession}  ${Filter3Decrypted}  ${bpsTotalSessions}  ${timeInterval}  ${group0Id}  ${minDashboard}  ${relativeTolerance}
    createNewGroup  ${atipSession}  Group1

    ${name}=  Set variable  BreakingPoint_serverA

    ${g1id}  getGroupId  ${atipSession}  Group1

    updateSslSettings  ${atipSession}  True  ${g1id}
    uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert512}  ${fileKey512}  ${g1id}

    ${Filter2Group1.groupId}=  Set variable  ${g1id}

    Create Filter on Group  ${Filter2Group1}  ${g1id}

    moveNPsToGroup  ${atipSession}  Group1  ${npId0}

    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTest2ConfigV1}

    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTest2ConfigV1}
    ${bpsTotalSessions}=  convert to integer  ${bpsTotalSessions}
    ${EncryptedSessions}=  Evaluate  ${bpsTotalSessions}/3
    ${EncryptedSessions}=  convert to integer  ${EncryptedSessions}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${EncryptedSessions}  ${g1id}

    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  10  ${g1id}
    checkTargetExistsInTopStats  ${atipStats}  ${strESPN}
    checkFilterExistsInTopStats  ${atipSession}  ${Filter2Group1}  ${timeInterval}  ${minDashboard}
    checkAtipTopStatsTotalSessions  ${atipSession}  ${Filter2Group1}  ${EncryptedSessions}  ${timeInterval}  ${g1id}  ${minDashboard}  ${relativeTolerance}

*** Keywords ***

Login ATIP
    login  ${atipSession}

Clear Config
    clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}

Create Filter on Group
    [Arguments]     ${filterConf}  ${gId}
    createFilter  ${atipSession}  ${filterConf}
    rulePush  ${atipSession}  ${gId}
    waitUntilSystemReady  ${atipSession}