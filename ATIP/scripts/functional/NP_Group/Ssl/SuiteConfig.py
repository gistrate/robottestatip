from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.SslPortMapping import SslPortMapping
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation


class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    httpStatusOK = HTTPStatus.OK.value
    httpStatusError = HTTPStatus.INTERNAL_SERVER_ERROR.value
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    appStatsType = StatsType.APPS
    timeInterval = TimeInterval.HOUR
    minimizedDashboard = DashboardType.MINIMIZED.statsLimit
    minDashboard = DashboardType.MINIMIZED
    bpsKeysPath = "../../../frwk/src/atipAutoFrwk/data/bpskeys"
    sslKeysPath = "../../../frwk/src/atipAutoFrwk/data/sslkeys"

    fileCert512 = bpsKeysPath + '/BreakingPoint_serverA_512.crt'
    fileKey512 = bpsKeysPath + '/BreakingPoint_serverA_512.key'
    fileCert1024 = bpsKeysPath + '/BreakingPoint_serverA_1024.crt'
    fileKey1024 = bpsKeysPath + '/BreakingPoint_serverA_1024.key'
    fileCert2048 = bpsKeysPath + '/BreakingPoint_serverA_2048.crt'
    fileKey2048 = bpsKeysPath + '/BreakingPoint_serverA_2048.key'

    bpsTestConfigSsl = BpsTestConfig('fb_ssl_512', 80)
    bpsTest1ConfigG1 = BpsTestConfig('espn1024fbaolplain', 80)
    bpsTest2ConfigV1 = BpsTestConfig('espn512_fbplain_aolplain', 80)
    bpsTest2Config1 = BpsTestConfig('espn512_fb1024_netflix2048', 80)
    bpsTest2ConfigG1 = BpsTestConfig('espn512_fb512_netflix1024', 80)
    bpsTest3ConfigG1 = BpsTestConfig('espn1024_fb512_netflix2048', 80)
    bpsTest3ConfigG2 = BpsTestConfig('espn1024_fb512_netflix2048t2', 80)
    bpsTestConfigV1 = BpsTestConfig('espn2048_fb2048_netflix2048', 80)
    strFacebook = 'Facebook'
    strESPN = 'ESPN'
    strAOL = 'AOL'
    strNetflix = 'Netflix'
    npId0 = str(envVars.atipSlotNumber[0])
    npId1 = str(envVars.atipSlotNumber[1])
    npId2 = str(envVars.atipSlotNumber[2])
    group0Id = 0
    relativeTolerance = 0.05

    Filter1Decrypted = FilterConfig("Filter1Decrypted")
    Filter1Decrypted.decrypted = True
    Filter2Decrypted = FilterConfig("Filter2Decrypted")
    Filter2Decrypted.decrypted = True
    Filter3Decrypted = FilterConfig("Filter3Decrypted")
    Filter3Decrypted.decrypted = True

    Filter2GroupDefault = FilterConfig("Filter2GroupDefault", id=-1, groupId=0)
    Filter2GroupDefault.decrypted = True
    FilterConditionConfig.addAppCondition(Filter2GroupDefault, ApplicationType.NETFLIX)

    Filter1Group1 = FilterConfig("Filter1Group1")
    FilterConditionConfig.addAppCondition(Filter1Group1, ApplicationType.ESPN)
    Filter1Group1.decrypted = True

    Filter2Group1 = FilterConfig("Filter2Group1")
    FilterConditionConfig.addGeoCondition(Filter2Group1, GeoLocation.CHINA)
    Filter2Group1.decrypted = True

    Filter1Group2 = FilterConfig("Filter1Group2")
    FilterConditionConfig.addGeoCondition(Filter1Group2, GeoLocation.CHINA)
    Filter1Group2.decrypted = True

    Filter2Group2 = FilterConfig("Filter2Group2")
    FilterConditionConfig.addGeoCondition(Filter2Group2, GeoLocation.THAILAND)
    Filter2Group2.decrypted = True