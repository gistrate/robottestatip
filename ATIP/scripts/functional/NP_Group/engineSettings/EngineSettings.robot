*** Settings ***

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Groups
Library    atipAutoFrwk.config.atip.GroupConfig
Library    atipAutoFrwk.services.atip.GroupsService
Library    atipAutoFrwk.services.GeneralService  WITH NAME  General
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  ListStats
Library    atipAutoFrwk.webApi.atip.stats.Pie  WITH NAME  PieStats
Library    atipAutoFrwk.services.atip.stats.ValidateStatsService
Library    atipAutoFrwk.services.atip.stats.ValidateSSLStatsService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    Collections
Library    atipAutoFrwk.services.atip.stats.TopFiltersService
Library    atipAutoFrwk.webApi.atip.ConnTableGlobalConfig
Library    atipAutoFrwk.webApi.atip.ConfigureStats
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    atipAutoFrwk.webApi.atip.stats.Filters
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    atipAutoFrwk.webApi.atip.MplsParsing
Library    atipAutoFrwk.webApi.atip.TcpMode
Library    atipAutoFrwk.webApi.atip.MatchEngineGlobal
Library    atipAutoFrwk.services.atip.CaptureService
Library    atipAutoFrwk.webApi.atip.Capture
Library    Telnet  timeout=30   prompt=$
Library    BuiltIn
Library    String
Library    scripts.functional.NP_Group.Filter_Priority.FilterPriorityLib
Library    atipAutoFrwk.webApi.atip.IngressPacketBuffer
Library    atipAutoFrwk.services.atip.NPService

Default Tags  ATIP  functional  hardware

Suite Setup        Suite Setup
Suite Teardown     Clean ATIP

*** Test Cases ***

TC001191214: [NP_Groups_7300][Engine_Settings]Default Group and 1 New Group with different Disable Detailed Stats and Connection Table Settings Configuration (Config_MoveNP)
    [Tags]  hardware
    [Setup]  Clear Config
    ${name}=  Set variable  BreakingPoint_serverA
    #On Default group Configure: Enable the Disable Detailed Statistics option and set TCP Session Timeout to 60 seconds and Non TCP Session timeout to 60 seconds.
    ${detailedStats0}=  getConfig  ${atipSession}  ${defaultGroupId}
    ${detailedStats0.enableTurboMode}=  set variable  True
    changeConfig  ${atipSession}  ${detailedStats0}

    ${globalConfig0}=  getConnTableGlobalConfig  ${atipSession}  ${defaultGroupId}
    ${globalConfig0.inactive_expire_time_sec_tcp}=  set variable  60
    ${globalConfig0.inactive_expire_time_sec_nontcp}=  set variable  60
    updateConnTableGlobalConfig  ${atipSession}  ${globalConfig0}

    #Create an app filter
    Create Filter on Group  ${filterAppGroupDefault}  ${defaultGroupId}

    Log  Run TCP traffic
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigTCP}
    ${timeoutsTcpNP1}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId0}
    ${timeoutsTcpNP2}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId1}
    ${timeoutsTcpNP3}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId2}
    should be equal as integers  ${timeoutsTcpNP1}  0
    should be equal as integers  ${timeoutsTcpNP2}  0
    should be equal as integers  ${timeoutsTcpNP3}  0

    Log  Run UDP traffic
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigUDP}
    ${timeoutsUdpNP1}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId0}
    ${timeoutsUdpNP2}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId1}
    ${timeoutsUdpNP3}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId2}
    should be equal as integers  ${timeoutsUdpNP1}  0
    should be equal as integers  ${timeoutsUdpNP2}  0
    should be equal as integers  ${timeoutsUdpNP3}  0

    Log  Run Facebook traffic
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}
    checkFilterExistsInTopStats  ${atipSession}  ${filterAppGroupDefault}  ${timeInterval}  ${minimizedDashboard}
    ${detailedStatsDefault}=  getDetailedStats  ${atipSession}  ${None}  ${None}  ${timeInterval}  10  ${filterAppGroupDefault.id}  ${None}  ${httpStatusOK}  ${defaultGroupId}
    Log  ${detailedStatsDefault}


    #using this sleep since we need to make sure that the previous connections are timing out
    sleep  60

    ${valueNP1}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId0}
    ${valueNP2}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId1}
    ${valueNP3}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId2}

    createNewGroup  ${atipSession}  Group1

    ${gId1}  getGroupId  ${atipSession}  Group1
    ${detailedStats1}=  getConfig  ${atipSession}  ${gId1}
    ${detailedStats1.enableTurboMode}=  set variable  False
    changeConfig  ${atipSession}  ${detailedStats1}

    ${globalConfig1}=  getConnTableGlobalConfig  ${atipSession}  ${gId1}
    ${globalConfig1.inactive_expire_time_sec_tcp}=  set variable  100
    ${globalConfig1.inactive_expire_time_sec_nontcp}=  set variable  100
    ${globalConfig1.tuple_hash_method}=  set variable  ip
    updateConnTableGlobalConfig  ${atipSession}  ${globalConfig1}  ${gId1}
    updateSslSettings  ${atipSession}  True  ${gId1}
    uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert1024}  ${fileKey1024}  ${gId1}
    #Create an app filter
    ${filterAppGroup1.groupId}=  set variable  ${gId1}
    Create Filter on Group  ${filterAppGroup1}  ${gId1}
    moveNPsToGroup  ${atipSession}  Group1  ${npId1}
    moveNPsToGroup  ${atipSession}  Group1  ${npId2}



    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigTCPNew}
    ${timeoutsTcpNP1}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId0}
    ${timeoutsTcpNP2}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId1}
    ${timeoutsTcpNP3}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId2}
    should be equal as integers  ${timeoutsTcpNP2}  ${valueNP2}
    should be equal as integers  ${timeoutsTcpNP3}  ${valueNP3}
    ${expectedNP1}=  Evaluate  ${valueNP1} + 4
    should be equal as integers  ${timeoutsTcpNP1}  ${expectedNP1}


    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigUDPNew}
    ${timeoutsUdpNP1}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId0}
    ${timeoutsUdpNP2}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId1}
    ${timeoutsUdpNP3}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId2}
    ${expectedNP1}=  Evaluate  ${timeoutsTcpNP1} + 4
    should be equal as integers  ${timeoutsUdpNP1}  ${expectedNP1}
    should be equal as integers  ${timeoutsUdpNP2}  ${valueNP2}
    should be equal as integers  ${timeoutsUdpNP3}  ${valueNP3}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigResume}
    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTestConfigResume}
    ${bpsTotalSessions}=  convert to integer  ${bpsTotalSessions}
    ${decryptedSessions}=  Evaluate  ${bpsTotalSessions} * 2
    checkSslResumeFails  ${atipSession}  ${gId1}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${decryptedSessions}  ${gId1}

    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}
    checkFilterExistsInTopStats  ${atipSession}  ${filterAppGroup1}  ${timeInterval}  ${minimizedDashboard}
    ${detailedStats}=  getDetailedStats  ${atipSession}  ${None}  ${None}  ${timeInterval}  10  ${filterAppGroup1.id}  ${None}  ${httpStatusOK}  ${gId1}
    Log  ${detailedStats}



TC001191219: [NP_Groups_V1][Engine_Settings]Defalut Group and 1 New Group with different Disable Detailed Stats and Connection Table Settings Configuration (Config_MoveNP)
    [Tags]  hardware
    [Setup]  Clear Config
    ${name}=  Set variable  BreakingPoint_serverA
    #added reset atip to clean previous timeouts
    resetAtipAndWaitUntilNPReady  ${atipSession}  ${envVars}
    #On Default group Configure: Enable the Disable Detailed Statistics option and set TCP Session Timeout to 60 seconds and Non TCP Session timeout to 60 seconds.
    ${detailedStats0}=  getConfig  ${atipSession}  ${defaultGroupId}
    ${detailedStats0.enableTurboMode}=  set variable  True
    changeConfig  ${atipSession}  ${detailedStats0}

    ${globalConfig0}=  getConnTableGlobalConfig  ${atipSession}  ${defaultGroupId}
    ${globalConfig0.inactive_expire_time_sec_tcp}=  set variable  60
    ${globalConfig0.inactive_expire_time_sec_nontcp}=  set variable  60
    updateConnTableGlobalConfig  ${atipSession}  ${globalConfig0}

    #Create an app filter
    Create Filter on Group  ${filterAppGroupDefault}  ${defaultGroupId}
    Log  Run TCP traffic
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigTCP}
    ${timeoutsTcpNP1}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId0}
    should be equal as integers  ${timeoutsTcpNP1}  0

    Log  Run UDP traffic
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigUDP}
    ${timeoutsUdpNP1}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId0}
    should be equal as integers  ${timeoutsUdpNP1}  0


    Log  Run Facebook traffic
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}
    checkFilterExistsInTopStats  ${atipSession}  ${filterAppGroupDefault}  ${timeInterval}  ${minimizedDashboard}
    ${detailedStatsDefault}=  getDetailedStats  ${atipSession}  ${None}  ${None}  ${timeInterval}  10  ${filterAppGroupDefault.id}  ${None}  ${httpStatusOK}  ${defaultGroupId}
    Log  ${detailedStatsDefault}


    #using this sleep since we need to make sure that the previous connections are timing out
    sleep  60

    ${valueNP1}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId0}

    createNewGroup  ${atipSession}  Group1

    ${gId1}  getGroupId  ${atipSession}  Group1
    ${detailedStats1}=  getConfig  ${atipSession}  ${gId1}
    ${detailedStats1.enableTurboMode}=  set variable  False
    changeConfig  ${atipSession}  ${detailedStats1}

    ${globalConfig1}=  getConnTableGlobalConfig  ${atipSession}  ${gId1}
    ${globalConfig1.inactive_expire_time_sec_tcp}=  set variable  100
    ${globalConfig1.inactive_expire_time_sec_nontcp}=  set variable  100
    ${globalConfig1.tuple_hash_method}=  set variable  ip
    updateConnTableGlobalConfig  ${atipSession}  ${globalConfig1}  ${gId1}
    updateSslSettings  ${atipSession}  True  ${gId1}
    uploadSeparateFiles  ${atipSession}  ${name}  ${fileCert1024}  ${fileKey1024}  ${gId1}
    #Create an app filter
    ${filterAppGroup1.groupId}=  set variable  ${gId1}
    Create Filter on Group  ${filterAppGroup1}  ${gId1}
    moveNPsToGroup  ${atipSession}  Group1  ${npId0}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigTCPNew}
    ${timeoutsTcpNP1}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId0}

    should be equal as integers  ${timeoutsTcpNP1}  ${valueNP1}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigUDPNew}
    ${timeoutsUdpNP1}=  getConnTimeoutsFromDebugPage  ${atipSession}  ${npId0}
    should be equal as integers  ${timeoutsUdpNP1}  ${valueNP1}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigResume}
    ${bpsTotalSessions}  getBpsTotalSessions  ${bpsSession}  ${bpsTestConfigResume}
    ${bpsTotalSessions}=  convert to integer  ${bpsTotalSessions}
    checkSslResumeFails  ${atipSession}  ${gId1}
    validateSslTotalDecryptedSessionsByPercentage  ${atipSession}  ${bpsTotalSessions}  ${gId1}

    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}
    checkFilterExistsInTopStats  ${atipSession}  ${filterAppGroup1}  ${timeInterval}  ${minimizedDashboard}
    ${detailedStats}=  getDetailedStats  ${atipSession}  ${None}  ${None}  ${timeInterval}  10  ${filterAppGroup1.id}  ${None}  ${httpStatusOK}  ${gId1}
    Log  ${detailedStats}

TC001191215
#   [NP_Groups_7300][Engine_Settings]Default Group and 1 New Group with different Deep Packet Inspection and TCP Settings Configuration (MoveNP_Config)
    [Tags]  hardware
    [Setup]  Clear Config
    ${tcpConfig0}=  getTcpConfig  ${atipSession}  ${defaultGroupId}
    ${tcpConfig0.enabledAlways}=  set variable  True
    updateTcpConfig  ${atipSession}  ${tcpConfig0}  ${defaultGroupId}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigRecreate}
    waitTargetExistsInPieStats  ${atipSession}  ${appStatsType}  ${timeInterval}  Eventbrite
    ${config}  getMatchEngineGlobalConfig  ${atipSession}
    ${config.buffer_packets}=  set variable  ${False}
    ${config.max_inspect_packets}=  set variable  1
    ${response}  updateMatchEngineGlobalConfig  ${atipSession}  ${config}
    sleep  1m
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigRecreate}
    waitTargetExistsInPieStats  ${atipSession}  ${appStatsType}  ${timeInterval}  https (TCP:443)



    createNewGroup  ${atipSession}  Group1
    login  ${atipSession}
    ${gId1}  getGroupId  ${atipSession}  Group1
    moveNPsToGroup  ${atipSession}  Group1  ${npId1}
    moveNPsToGroup  ${atipSession}  Group1  ${npId2}

    #Create an app filter
    ${filterAppGroupFw1.groupId}=  set variable  ${gId1}
    ${filterConfig}=  Create Filter on Group  ${filterAppGroupFw1}  ${gId1}

    ${tcpConfig1}=  getTcpConfig  ${atipSession}  ${gId1}
    ${tcpConfig1.forwardEmptyAcks}=  set variable  True
    updateTcpConfig  ${atipSession}  ${tcpConfig1}  ${gId1}
    ${config}  getMatchEngineGlobalConfig  ${atipSession}  ${gId1}  ${httpStatusOK}
    ${config.buffer_packets}=  set variable  ${True}
    ${config.max_inspect_packets}=  set variable  1
    ${response}  updateMatchEngineGlobalConfig  ${atipSession}  ${config}

    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigBuffer}
    waitTargetExistsInPieStats  ${atipSession}  ${appStatsType}  ${timeInterval}  Facebook  ${gId1}
    Run traffic and capture  ${bpsTestConfigBuffer}  ${filterConfig.name}  ${gId1}
    Connect on Linux Box
    Write  cd captures
    #download last PCAP and analyze
    ${localPcapFolder}=  getLatestCaptureAndUnzip  ${atipSession}
    transferAtipCapture  ${atipSession}  ${envVars}  ${localPcapFolder}  ${captureLocation}${TEST NAME}_1.pcap
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}_1  ${pcap1}  text  ${EMPTY}  ${TEST NAME}_1_1
    Execute Command  ${filteringCommand}  strip_prompt=True
    ${output}=  Execute Command  wc -l < ${TEST NAME}_1_1.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Not Be Equal As Integers  ${statsCaptureLB}  0

TC001191216: [NP_Groups_7300][Engine_Settings]2xNew Groups with multiple NPs per group with different MPLS Parsing Configuration(Config_MoveNP)
    [Tags]  hardware
    [Setup]  Clear Config
    createNewGroup  ${atipSession}  Group1
    ${gId1}  getGroupId  ${atipSession}  Group1
    ${mplsConfig1}=  getMplsParsingConfig  ${atipSession}  ${gId1}
    ${mplsConfig1.enabled}=  set variable  True
    ${mplsConfig1.traffic_type}=  set variable  L2_VPN
    ${mplsConfig1.contains_pwcw}=  set variable  True
    ${mplsConfig1.groupId}=  set variable  ${gId1}
    updateMplsParsing  ${atipSession}  ${mplsConfig1}
    moveNPsToGroup  ${atipSession}  Group1  ${npId1}
    moveNPsToGroup  ${atipSession}  Group1  ${npId2}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigL2VPN}
    waitTargetExistsInPieStats  ${atipSession}  ${appStatsType}  ${timeInterval}  http (TCP:80)  ${gId1}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigL3VPN}
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}
    checkTargetDoesNotExistInTopStats  ${atipStats}  http (TCP:80)

    createNewGroup  ${atipSession}  Group2
    ${gId2}  getGroupId  ${atipSession}  Group2
    ${mplsConfig2}=  getMplsParsingConfig  ${atipSession}  ${gId2}
    ${mplsConfig2.enabled}=  set variable  True
    ${mplsConfig2.traffic_type}=  set variable  L3_VPN
    ${mplsConfig2.groupId}=  set variable  ${gId2}
    updateMplsParsing  ${atipSession}  ${mplsConfig2}
    moveNPsToGroup  ${atipSession}  Group2  ${npId0}

    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigL3VPN}
    waitTargetExistsInPieStats  ${atipSession}  ${appStatsType}  ${timeInterval}  http (TCP:80)  ${gId2}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigL2VPN}
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}
    checkTargetDoesNotExistInTopStats  ${atipStats}  http (TCP:80)

TC001191217: [NP_Groups_vATIP][Engine_Settings]2xNew Groups with multiple NPs per group with different MPLS Parsing Configuration(Config_MoveNP)
    [Tags]  virtual
    [Setup]  Clear Config
    createNewGroup  ${atipSession}  Group1
    ${gId1}  getGroupId  ${atipSession}  Group1
    ${mplsConfig1}=  getMplsParsingConfig  ${atipSession}  ${gId1}
    ${mplsConfig1.enabled}=  set variable  True
    ${mplsConfig1.traffic_type}=  set variable  L2_VPN
    ${mplsConfig1.contains_pwcw}=  set variable  True
    ${mplsConfig1.groupId}=  set variable  ${gId1}
    updateMplsParsing  ${atipSession}  ${mplsConfig1}
    moveNPsToGroup  ${atipSession}  Group1  ${npId0}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigL2VPN}
    waitTargetExistsInPieStats  ${atipSession}  ${appStatsType}  ${timeInterval}  http (TCP:80)  ${gId1}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigL3VPN}
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}
    checkTargetDoesNotExistInTopStats  ${atipStats}  http (TCP:80)

    createNewGroup  ${atipSession}  Group2
    ${gId2}  getGroupId  ${atipSession}  Group2
    ${mplsConfig2}=  getMplsParsingConfig  ${atipSession}  ${gId2}
    ${mplsConfig2.enabled}=  set variable  True
    ${mplsConfig2.traffic_type}=  set variable  L3_VPN
    ${mplsConfig2.groupId}=  set variable  ${gId2}
    updateMplsParsing  ${atipSession}  ${mplsConfig2}
    moveNPsToGroup  ${atipSession}  Group2  ${npId0}

    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigL3VPN}
    waitTargetExistsInPieStats  ${atipSession}  ${appStatsType}  ${timeInterval}  http (TCP:80)  ${gId2}
    resetStats  ${atipSession}
    General.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfigL2VPN}
    ${atipStats}  PieStats.getTopStats  ${atipSession}  ${appStatsType}  ${timeInterval}  ${gId2}
    checkTargetDoesNotExistInTopStats  ${atipStats}  http (TCP:80)

TC001210806 Ingress packet buffer size
    [Setup]  Clear Config

    Log  Check limits for Default NP Group
    Validate Ingress Packet Buffer limits for group  ${defaultGroupId}

    Log  Create NP Group1
    createNewGroup  ${atipSession}  Group1
    ${groupId1}  getGroupId  ${atipSession}  Group1

    Log  Check limits for NP Group 1
    Validate Ingress Packet Buffer limits for group  ${groupId1}

    Log  Check values are set per group and not globally
    ${ingressPacketBufferConfig}=  getIngressPacketBufferConfig  ${atipSession}  ${defaultGroupId}
    ${ingressPacketBufferConfig.softwareRxQueue}=  Set Variable  16500
    updateIngressPacketBufferConfig  ${atipSession}  ${ingressPacketBufferConfig}  ${defaultGroupId}
    ${ingressPacketBufferConfig}=  getIngressPacketBufferConfig  ${atipSession}  ${groupId1}
    ${ingressPacketBufferConfig.softwareRxQueue}=  Set Variable  26500
    updateIngressPacketBufferConfig  ${atipSession}  ${ingressPacketBufferConfig}  ${groupId1}

    ${ingressPacketBufferConfigDefaultGroup}=  getIngressPacketBufferConfig  ${atipSession}  ${defaultGroupId}
    should be equal as integers  ${ingressPacketBufferConfigDefaultGroup.softwareRxQueue}  16500
    ${ingressPacketBufferConfigGroup1}=  getIngressPacketBufferConfig  ${atipSession}  ${groupId1}
    should be equal as integers  ${ingressPacketBufferConfigGroup1.softwareRxQueue}  26500

*** Keywords ***




Become SU
    Write  su
    sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt



Clear Config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}

Create Filter on Group
    [Arguments]     ${filterConf}  ${gId}
    ${filterConfUpdated}=  createFilter  ${atipSession}  ${filterConf}
    rulePush  ${atipSession}  ${gId}
    waitUntilSystemReady  ${atipSession}
    [Return]  ${filterConfUpdated}




Run traffic and capture
    [Arguments]  ${bpsTestConfig}  ${filter}  ${gId}

    # perform captures
    # get filter id
    ${filterId}=  getFilterId  ${atipSession}  ${filter}
    Log  ${filterId}

    # delete capture files from ATIP
    deleteCaptureFiles  ${atipSession}

    # start capture on ATIP
    controlCapture  ${atipSession}  ${filterId}  ${True}
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Connect on Linux Box
    Write  cd captures
    Become SU

    controlCapture  ${atipSession}  ${filterId}  ${False}

Suite Setup

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    ${npId0}=  updateNpIdVirtual  ${atipSession}  ${envVars}
    set suite variable  ${npId0}  ${npId0}
    resetAtipAndWaitUntilNPReady  ${atipSession}  ${envVars}


    Connect on Linux Box
    # be sure that "captures" folder is deleted because it happened to remain created by root and had write protection against "atip" user; hence, no capture filter result file could be created by "atip" user when running tshark filtering command
    Become SU
    Write  rm -rf captures
    Write  exit
    Sleep  1s
    # create temp folder for testing captures; it should be owned by "atip" user
    Write  mkdir captures
    Sleep  1s
    Close Connection


Connect on Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Read
    [return]  ${openTelnet}


Clean ATIP
    Connect on Linux Box
    Become SU
    Execute Command  rm -rf captures
    Logout  ${atipSession}


Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [Return]  ${output}


Validate Ingress Packet Buffer limits for group
    [Arguments]  ${groupId}
    ${ingressPacketBufferConfig}=  getIngressPacketBufferConfig  ${atipSession}  ${groupId}
    should be equal as integers  ${ingressPacketBufferConfig.softwareRxQueue}  ${softwareRxQueueDefaultValue}
    should be equal as integers  ${ingressPacketBufferConfig.minSoftwareRxQueue}  ${minSoftwareRxQueueDefaultValue}
    should be equal as integers  ${ingressPacketBufferConfig.maxSoftwareRxQueue}  ${maxSoftwareRxQueueDefaultValue}
    should be equal as integers  ${ingressPacketBufferConfig.defaultSoftwareRxQueue}  ${softwareRxQueueDefaultValue}
    ${ingressPacketBufferConfig.softwareRxQueue}=  Set Variable  ${minSoftwareRxQueueDefaultValue - 1}
    ${msg}=  run keyword and expect error  *  updateIngressPacketBufferConfig  ${atipSession}  ${ingressPacketBufferConfig}  ${groupId}
    should contain  ${msg}  ${ipbExpectedErrorCodeMsg}
    should contain  ${msg}  ${ipbExpectedErrorMsgForTooLow}
    ${ingressPacketBufferConfig.softwareRxQueue}=  Set Variable  ${maxSoftwareRxQueueDefaultValue + 1}
    ${msg}=  run keyword and expect error  *  updateIngressPacketBufferConfig  ${atipSession}  ${ingressPacketBufferConfig}  ${groupId}
    should contain  ${msg}  ${ipbExpectedErrorCodeMsg}
    should contain  ${msg}  ${ipbExpectedErrorMsgForTooHigh}
    ${ingressPacketBufferConfig}=  getIngressPacketBufferConfig  ${atipSession}  ${groupId}
    should be equal as integers  ${ingressPacketBufferConfig.softwareRxQueue}  ${ingressPacketBufferConfig.defaultSoftwareRxQueue}
    ${ingressPacketBufferConfig.softwareRxQueue}=  Set Variable  ${minSoftwareRxQueueDefaultValue}
    updateIngressPacketBufferConfig  ${atipSession}  ${ingressPacketBufferConfig}  ${groupId}
    ${ingressPacketBufferConfig}=  getIngressPacketBufferConfig  ${atipSession}  ${groupId}
    should be equal as integers  ${ingressPacketBufferConfig.softwareRxQueue}  ${minSoftwareRxQueueDefaultValue}
    ${ingressPacketBufferConfig.softwareRxQueue}=  Set Variable  ${maxSoftwareRxQueueDefaultValue}
    updateIngressPacketBufferConfig  ${atipSession}  ${ingressPacketBufferConfig}  ${groupId}
    ${ingressPacketBufferConfig}=  getIngressPacketBufferConfig  ${atipSession}  ${groupId}
    should be equal as integers  ${ingressPacketBufferConfig.softwareRxQueue}  ${maxSoftwareRxQueueDefaultValue}
