from http import HTTPStatus
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.SslPortMapping import SslPortMapping
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.ProcStats import ProcStats
from atipAutoFrwk.data.atip.EngineSettings import IngressPacketBuffer


class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    httpStatusOK = HTTPStatus.OK
    loginSuccessful = ResponseMessage.LOGIN_SUCCESSFUL
    hardware = AtipType.HARDWARE
    virtual = AtipType.VIRTUAL
    appStatsType = StatsType.APPS
    timeInterval = TimeInterval.HOUR
    minimizedDashboard = DashboardType.MINIMIZED
    typeDevice = StatsType.DEVICE
    bpsTestConfig = BpsTestConfig('ATIP_Facebook_1min', 70)
    bpsTestConfigTCP = BpsTestConfig('http_get_delay45s', 80)
    bpsTestConfigUDP = BpsTestConfig('DNS_Req_Resp_Delay30s', 80)
    bpsTestConfigResume = BpsTestConfig('Test_quickResumes', 80)
    bpsTestConfigTCPNew = BpsTestConfig('http_get_delay95s', 100)
    bpsTestConfigUDPNew = BpsTestConfig('DNS_Req_Resp_Delay70s', 80)
    bpsTestConfigRecreate = BpsTestConfig('RECREATE-evbrite', 80)
    bpsTestConfigBuffer = BpsTestConfig('facebook_mihail', 80)
    bpsTestConfigL2VPN = BpsTestConfig('L2_VPN_CW', 80)
    bpsTestConfigL3VPN = BpsTestConfig('DM_L3VPN', 80)


    strFacebook = 'facebook'
    strESPN = 'espn'
    strAOL = 'aol'
    npId0 = str(envVars.atipSlotNumber[0])
    if envVars.atipType == hardware:
        npId1 = str(envVars.atipSlotNumber[1])
        npId2 = str(envVars.atipSlotNumber[2])
    defaultGroupId = 0

    bpsKeysPath = "../../../frwk/src/atipAutoFrwk/data/bpskeys"
    fileCert1024 = bpsKeysPath + '/BreakingPoint_serverA_1024.crt'
    fileKey1024 = bpsKeysPath + '/BreakingPoint_serverA_1024.key'
    captureLocation = '/home/atip/captures/'


    filterAppGroupDefault = FilterConfig("FilterGroupDefault", id=-1, groupId=0)
    FilterConditionConfig.addAppCondition(filterAppGroupDefault, ApplicationType.FACEBOOK)
    filterAppGroup1 = FilterConfig("FilterGroup1", id=-1)
    FilterConditionConfig.addAppCondition(filterAppGroup1, ApplicationType.FACEBOOK)
    filterAppGroupFw1 = FilterConfig("FilterGroupFw1", id=-1)
    FilterConditionConfig.addAppCondition(filterAppGroupFw1, ApplicationType.FACEBOOK)
    filterAppGroupFw1.forward = True
    filterAppGroupFw1.transmitIds = 100
    pcap1 = '!((http.request.method == "GET") || (tcp.flags.ack == 1 && tcp.flags.fin == 1) || (http.response.code == 200 && http.response.phrase == OK))'
    pcap2 = '!((http.request.method == "GET") || (tcp.flags.ack == 1 && tcp.flags.fin == 1) || (http.response.code == 200 && http.response.phrase == OK) || (tcp.flags.ack == 1 && tcp.flags.fin == 0))'
    pcap3 = '!((http.request.method == "GET") || (tcp.flags.ack == 0 && tcp.flags.syn == 1) || (tcp.flags.ack == 1 && tcp.flags.syn == 1) || (tcp.flags.ack == 1 && tcp.flags.fin == 1) || (http.response.code == 200 && http.response.phrase == OK) || (tcp.flags.ack == 1 && tcp.flags.fin == 0))'

    # Ingress Packet Buffer expected error messages
    ipbExpectedErrorCodeMsg = ResponseMessage.EXCEPTION_EXPECTED_200_ACTUAL_500
    ipbExpectedErrorMsgForTooLow = ResponseMessage.INGRESS_PACKET_BUFFER_EXPECTED_ERROR_FOR_TOO_LOW
    ipbExpectedErrorMsgForTooHigh = ResponseMessage.INGRESS_PACKET_BUFFER_EXPECTED_ERROR_FOR_TOO_HIGH

    # Ingress Packet Buffer expected values
    softwareRxQueueDefaultValue = IngressPacketBuffer.SOFTWARE_RX_QUEUE_DEFAULT_VALUE
    minSoftwareRxQueueDefaultValue = IngressPacketBuffer.MIN_SOFTWARE_RX_QUEUE_DEFAULT_VALUE
    maxSoftwareRxQueueDefaultValue = IngressPacketBuffer.MAX_SOFTWARE_RX_QUEUE_DEFAULT_VALUE