from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.PortRange import PortRange
from atipAutoFrwk.config.atip.filter.IpRange import IpRange
from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage
from atipAutoFrwk.webApi.atip.TopFilters import TopFilters
from atipAutoFrwk.webApi.atip.stats.Lists import Lists
from atipAutoFrwk.webApi.atip.stats.Filters import Filters as FilterStats
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.services.IPService import IPService
import warnings
import time

class IpFilteringLib(object):

    @classmethod
    def configureFilter(cls, filterConfig, filterName, proto=None, startIndex=None, endIndex=None, startIP=None, endIP=None):

        filterConfig.name = filterName
        if proto:
            FilterConditionConfig.addProtocolCondition(filterConfig, proto)
        if startIndex and endIndex:
            portRange = PortRange(startIndex, endIndex)
            FilterConditionConfig.addPortRangeCondition(filterConfig, portRange)

        if startIP and endIP:
            ipRange = IpRange(startIP,endIP,'user')
            FilterConditionConfig.addCondition(filterConfig, FilterConditionType.IP_RANGE, ipRange)

        return filterConfig

    @classmethod
    def getGeoTopStats(cls, webApiSession):
        geo = {}
        countryList = []
        topStats = Lists.getTopStats(webApiSession, StatsType.GEO, TimeInterval.FIVEMIN).getList()
        if topStats:
            if not isinstance(topStats, list):
                topStats = [topStats]
        for item in topStats:
            geo[item.msg] = item.totalPkts
            countryList.append(item.msg)

        return geo, countryList

    @classmethod
    def getIpFilterTopStats(cls, webApiSession, ruleID, statsType):
        ipList = []
        topStats = FilterStats.getTopStats(webApiSession, statsType, TimeInterval.FIVEMIN, '10', ruleID).getList()
        if topStats:
            if not isinstance(topStats, list):
                topStats = [topStats]
        else:
            warnings.warn('No IP filter statistics.')
        for item in topStats:
            ipList.append(item.msg)

        return ipList

    @classmethod
    def validateAtipStatistics(cls, webApiSession, envVars, filterName, xTimes, delay, ruleID, statsType, strictEvaluateTopFilter):
        oldHwRxDebugStats = 0
        oldTopFilterStat = 0
        oldGeo, country = cls.getGeoTopStats(webApiSession)
        filterIPStatsList = []

        # need to wait 5 secs in order for newGeo to get new statistics, other than those stored in oldGeo already
        time.sleep(5)

        for i in range(int(xTimes)):
            # validate debug statistics, test traffic is getting into Atip
            newHwRxDebugStats = DebugPage.getRxPacketsFromDebugPage(webApiSession, envVars)
            if not(oldHwRxDebugStats < newHwRxDebugStats):
                raise Exception('RX statistics on Atip are not incrementing.')
            oldHwRxDebugStats = newHwRxDebugStats

            # validate Geo statistics, test if Dashboard stats are really populated
            newGeo, countryList = cls.getGeoTopStats(webApiSession)

            for country in countryList:
                if not (oldGeo[country] < newGeo[country]):
                    raise Exception('Geo statistics on Atip are not incrementing.')
            oldGeo = newGeo

            # validate client IP statistics for filter
            ipStatsStatus, forcedTopFilters, oldTopFilterStat = cls.validateTopFilterAndIPStatistics(webApiSession, ruleID, filterName, oldTopFilterStat, statsType, strictEvaluateTopFilter)
            filterIPStatsList.append(ipStatsStatus)

            time.sleep(int(delay))

        return filterIPStatsList, forcedTopFilters

    @classmethod
    def validateTopFilterAndIPStatistics(cls, webApiSession, ruleID, filterName, oldTopFilterStat, statsType, strictEvaluateTopFilter=False):
        # boolean variable to test that filter IP statistics are populated and TopFilter statistics are increasing
        validTopFilterIPStats = True

        # boolean variable to test that filter IP statistics are not populated when traffic is not matching filter; because same method is used also when positive testing, this variable return status when negative scenario is forced
        forcedTopFilterStats = False

        # get filter IP statistics
        filterIpStats = cls.getIpFilterTopStats(webApiSession, ruleID, statsType)

        # get TopFilter statistics
        topFilters = TopFilters.getTopFiltersStats(webApiSession, 0, TimeInterval.FIVEMIN)
        newTopFilterStat = cls.getFilterStatsActualValue(topFilters, filterName, TopStats.TOTAL_PKTS)
        if strictEvaluateTopFilter:
            if newTopFilterStat == False:
                # this option is used when want to verify that TopFilter statistics are not populating when Atip is receiving TCP traffic that does not match filter; this is considered one Pass Test criterion
                forcedTopFilterStats = True
                validTopFilterIPStats = False
            else:
                raise Exception('Invalid TopFilter statistics.')
        elif not ((filterIpStats and len(filterIpStats) > 0) and (oldTopFilterStat < newTopFilterStat)):
            validTopFilterIPStats = False
        oldTopFilterStat = newTopFilterStat

        return validTopFilterIPStats, forcedTopFilterStats, oldTopFilterStat

    @classmethod
    def validateIpFilterStatsInRange(cls, webApiSession, ruleId, statsType, startIp, endIp, xTimes, delay):
        for i in range(int(xTimes)):
            filterIpStatsList = cls.getIpFilterTopStats(webApiSession, ruleId, statsType)
            filterIpRange = IPService.getIpRangeFromList(filterIpStatsList)
            if not((filterIpRange[0] >= startIp) and (filterIpRange[-1] <= endIp)):
                raise Exception('Filter IP statistics are not in filter configured IP range. Statistics IP range: {}, configured IP range: {}'.format(filterIpRange,tuple(startIp,endIp)))
            time.sleep(int(delay))

    @classmethod
    def getFilterStatsActualValue(cls, topFiltersStats, filterName, typeStat):
        '''
        Return counter for specified statistic
        :param topFiltersStats: top 10 filter statistics
        :param filterName: name of filter to get statistics for
        :param typeStat: statistics field to get value for
        :return: specific statistic value
        '''

        actualValue = 0
        if len(topFiltersStats['stats'][0]) == 1:
            # empty TopFilter statistics contains {"stats": {"name": "rules"}}, so not really empty, but not having "list" attribute which contains actual statistics
            warnings.warn('TopFilter statistics are empty.')
            return False
        elif not isinstance(topFiltersStats['stats'][0]['list'], list):
            topFiltersStats['stats'][0]['list'] = [topFiltersStats['stats'][0]['list']]

        for statsItem in topFiltersStats['stats'][0]['list']:
            if statsItem['msg'] == filterName:
                if typeStat == TopStats.TOTAL_COUNT:
                    actualValue = statsItem['totalCount']
                elif typeStat == TopStats.TOTAL_BYTES:
                    actualValue = statsItem['totalBytes']
                elif typeStat == TopStats.TOTAL_PKTS:
                    actualValue = statsItem['totalPkts']
                break

        return actualValue







