from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.config.Environment import Environment

class SuiteConfig(object):
    filterName = "IP Filtering Automation Test"
    bpsTestFile = "Ip filtering test 1"
    filterProtocolTCP = NetworkProtocol.TCP
    filterProtocolUDP = NetworkProtocol.UDP
    filterConfig = FilterConfig()
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    statsTypeServer = StatsType.SERVERS
    statsTypeClient = StatsType.CLIENTS