*** Settings ***
Documentation  Ip Filtering test suite.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    Collections
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.config.atip.filter.FilterConditionConfig
Library    atipAutoFrwk.services.IPService
Library    atipAutoFrwk.webApi.traffic.Login  WITH NAME  bpsLogin
Library    atipAutoFrwk.webApi.traffic.BpsOperations
Library    atipAutoFrwk.webApi.traffic.PortsOperations
Library    atipAutoFrwk.webApi.atip.ConfigureStats
Library    BuiltIn
Library    String
Library    IpFilteringLib.py

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP
Suite Teardown     atipLogout.Logout  ${atipSession}


*** Test Cases ***

IpFiltering_RunTraffic
    [Setup]  Create Config

	# create a TCP protocol filter with one ip address range
	configureFilter  ${filterConfig}  ${filterName}  ${filterProtocolTCP}  ${EMPTY}  ${EMPTY}  1.1.0.30  1.1.0.40
    createFilter  ${atipSession}  ${filterConfig}

    # push changes to NP
    rulePush  ${atipSession}

    # disable TurboMode
    Configure Detailed Statistics  ${atipSession}  ${FALSE}

    # run BPS test
    Start BPS Test  ${bpsTestFile}  ${bpsSession}

    ${filterID}=  getFilterId  ${atipSession}  ${filterName}

    ${ipStartRange}  ${ipEndRange}=  Get Filter IP Range  ${filterID}
    validateIpFilterStatsInRange  ${atipSession}  ${filterID}  ${statsTypeClient}  ${ipStartRange}  ${ipEndRange}  6  5

    # reset 'Conditions' attribute from original filter config; update existing filter ip address range with a new ip address range and chack statistics
    resetConditions  ${filterConfig}
    configureFilter  ${filterConfig}  ${filterName}  ${filterProtocolTCP}  ${EMPTY}  ${EMPTY}  1.1.0.40  1.1.0.50
    updateFilter  ${atipSession}  ${filterConfig}

    # push changes to NP
    rulePush  ${atipSession}

    # get updated filter ID
    ${filterID}=  getFilterId  ${atipSession}  ${filterName}

    ${ipStartRange}  ${ipEndRange}=  Get Filter IP Range  ${filterID}

    # wait minimum 5s in order for Atip Ip filter statistics to be incremented after filter update
    sleep  5s
    validateIpFilterStatsInRange  ${atipSession}  ${filterID}  ${statsTypeClient}  ${ipStartRange}  ${ipEndRange}  6  5

    forceStopBpsTestIfRunning  ${bpsSession}

PortFiltering
    [Setup]  Create Config

    # create TCP protocol filter with port ranges
	configureFilter  ${filterConfig}  ${filterName}  ${filterProtocolTCP}  10000  10010  ${EMPTY}  ${EMPTY}
	${newProtocolFilterConfig}=  configureFilter  ${filterConfig}  ${filterName}  ${EMPTY}  80  80  ${EMPTY}  ${EMPTY}
    createFilter  ${atipSession}  ${newProtocolFilterConfig}

    # push changes to NP
    rulePush  ${atipSession}

    # disable TurboMode
    Configure Detailed Statistics  ${atipSession}  ${FALSE}

    # run BPS test
    Start BPS Test  ${bpsTestFile}  ${bpsSession}

    ${filterID}=  getFilterId  ${atipSession}  ${filterName}
    ${validateResult}  ${noTraffic}=  validateAtipStatistics  ${atipSession}  ${envVars}  ${filterName}  6  5  ${filterID}  ${statsTypeServer}  ${FALSE}

    should not contain  ${validateResult}  ${FALSE}

    # reset 'Conditions' attribute from original filter config; delete previous created filter and create a TCP protocol filter with one port range only; check statistics again
    resetConditions  ${filterConfig}
    deleteAllFilters  ${atipSession}
    configureFilter  ${filterConfig}  ${filterName}_2  ${filterProtocolTCP}  1000  1100  ${EMPTY}  ${EMPTY}
	createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}

    resetStats  ${atipSession}

    # wait minimum 5s in order for Atip statistics to be incremented after filter update
    sleep  5s

    ${filterID}=  getFilterId  ${atipSession}  ${filterName}_2
    ${validateResult}  ${noTraffic}=  validateAtipStatistics  ${atipSession}  ${envVars}  ${filterName}_2  6  5  ${filterID}  ${statsTypeServer}  ${TRUE}

    forceStopBpsTestIfRunning  ${bpsSession}

    should not contain  ${validateResult}  ${TRUE}
    should be equal as strings  ${noTraffic}  ${TRUE}


ProtocolFiltering
    [Setup]  Create Config

    # create TCP protocol filter
	configureFilter  ${filterConfig}  ${filterName}  ${filterProtocolTCP}  ${EMPTY}  ${EMPTY}  ${EMPTY}  ${EMPTY}
    createFilter  ${atipSession}  ${filterConfig}

    # push changes to NP
    rulePush  ${atipSession}

    # disable TurboMode
    Configure Detailed Statistics  ${atipSession}  ${FALSE}

    # run BPS test
    Start BPS Test  ${bpsTestFile}  ${bpsSession}

    ${filterID}=  getFilterId  ${atipSession}  ${filterName}
    ${validateResult}  ${noTraffic}=  validateAtipStatistics  ${atipSession}  ${envVars}  ${filterName}  6  5  ${filterID}  ${statsTypeServer}  ${FALSE}

    should not contain  ${validateResult}  ${FALSE}

    # change protocol to UDP
    resetConditions  ${filterConfig}
    deleteAllFilters  ${atipSession}
    configureFilter  ${filterConfig}  ${filterName}_2  ${filterProtocolUDP}  ${EMPTY}  ${EMPTY}  ${EMPTY}  ${EMPTY}
	createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}

    resetStats  ${atipSession}

    # wait minimum 5s in order for Atip statistics to be incremented after filter update
    sleep  5s

    ${filterID}=  getFilterId  ${atipSession}  ${filterName}_2
    ${validateResult}  ${noTraffic}=  validateAtipStatistics  ${atipSession}  ${envVars}  ${filterName}_2  6  5  ${filterID}  ${statsTypeServer}  ${TRUE}
    Log  ${validateResult}

    forceStopBpsTestIfRunning  ${bpsSession}

    should not contain  ${validateResult}  ${TRUE}
    should be equal as strings  ${noTraffic}  ${TRUE}


*** Keywords ***

Create Config
    deleteAllFilters  ${atipSession}
    resetStats  ${atipSession}
    resetConditions  ${filterConfig}

Configure Detailed Statistics
    [Arguments]  ${atipSession}  ${status}
    ${detailedStats}=  getConfigureStatsObj   ${status}
    changeConfig  ${atipSession}  ${detailedStats}

Start BPS Test
    [Arguments]  ${bpsTestFile}  ${bpsSession}
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTestFile}  150
    bpsLogin.login  ${bpsSession}
    startAndWaitForTraffic  ${bpsSession}  ${bpsTestConfig.testName}  ${envVars.bpsPortGroup}

Get Filter IP Range
    [Arguments]  ${filterId}
    ${ipList}=  getFilterIpRangeById  ${atipSession}  ${filterId}
    ${ipListItem}=  get from list  ${ipList}  0
    ${ipStartRange}=  get from dictionary  ${ipListItem}  startRange
    ${ipEndRange}=  get from dictionary  ${ipListItem}  endRange

    [return]  ${ipStartRange}  ${ipEndRange}

Configure ATIP
    atipLogin.Login  ${atipSession}

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}