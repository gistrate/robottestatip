*** Settings ***
Documentation  Forward Empty Acks test suite.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    Collections
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    atipAutoFrwk.services.atip.TcpSettingsService
Library    atipAutoFrwk.webApi.atip.TcpMode
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.webApi.atip.Netflow
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.ConfigureStats
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.services.atip.stats.DebugPage
Library    BuiltIn
Library    String
Library    Telnet  timeout=30   prompt=$
Library    ForwardEmptyAcksLib.py

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP
Suite Teardown     atipLogout.Logout  ${atipSession}


*** Test Cases ***
TC001039189    TC001039189
    [Template]  ForwardEmptyAcksEnableDisable


# testId        captureTime     pcapArgument               bpsTest                      bpsNeighborhood       enableSSL   enableForwardEmptyAcks    enableTCPForwarding    jsonFilter      restartAtip
TC001033173         20           ${pcap1}    FEA_lowRate-bw-GET-response100_2CONNs    FEA-NN-Pakistan-ATIE    ${FALSE}           ${FALSE}                 ${FALSE}         ${feaNetflix}      ${FALSE}
    [Setup]  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    [Template]  ForwardEmptyAcksTraffic
TC001033174         10           ${pcap2}    FEA_lowRate-bw-GET-response100_2CONNs    FEA-NN-Pakistan-ATIE    ${FALSE}           ${TRUE}                  ${FALSE}         ${feaNetflix}      ${FALSE}
    [Template]  ForwardEmptyAcksTraffic
TC001033179         20           ${pcap2}    FEA_lowRate-bw-GET-response100_2CONNs    FEA-NN-Pakistan-ATIE    ${TRUE}            ${TRUE}                  ${FALSE}         ${feaNetflix}      ${FALSE}
    [Template]  ForwardEmptyAcksTraffic
TC001033179b        10           ${pcap2}    FEA_lowRate-bw-GET-response100_2CONNs    FEA-NN-Pakistan-ATIE    ${TRUE}            ${FALSE}                 ${FALSE}         ${feaNetflix}      ${FALSE}
    [Template]  ForwardEmptyAcksTraffic
TC001034388         15           ${pcap3}    FEA_lowRate-bw-GET-response100_2CONNs    FEA-NN-Pakistan-ATIE    ${TRUE}            ${TRUE}                  ${TRUE}          ${fea_pakistan}    ${FALSE}
    [Template]  ForwardEmptyAcksTraffic
TC001034388b        15           ${pcap2}    FEA_lowRate-bw-GET-response100_2CONNs    FEA-NN-Pakistan-ATIE    ${TRUE}            ${FALSE}                 ${FALSE}         ${feaNetflix}      ${TRUE}
    [Template]  ForwardEmptyAcksTraffic


# testId                      bpsTest                 testDuration   rxPkts   txPkts   certName   tcpReasembly   fwdEmptyAcks
TC001077176              RECREATE_AOL_8pkt                 16          8         8     ${EMPTY}     ${TRUE}       ${TRUE}
    [Setup]  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    [Template]  ForwardLastAckTraffic
TC001077180   RECREATE_AOL_9pkt_extra_retrans_ack          16          9         8     ${EMPTY}      ${TRUE}       ${TRUE}
    [Template]  ForwardLastAckTraffic
TC001077183   RECREATE_AOL_10pkt_ending_double_ACK         16         10        10     ${EMPTY}      ${TRUE}       ${TRUE}
    [Template]  ForwardLastAckTraffic
TC001077225            RECREATE_https_22pkts               16         22        16        512       ${TRUE}       ${TRUE}
    [Template]  ForwardLastAckTraffic
TC001077179            RECREATE_AOL_8pkt                   16          8         6     ${EMPTY}    ${FALSE}      ${FALSE}
    [Template]  ForwardLastAckTraffic
TC001077178            RECREATE_AOL_8pkt                   16          8         6     ${EMPTY}     ${TRUE}       ${FALSE}
    [Template]  ForwardLastAckTraffic
TC001077177            RECREATE_AOL_8pkt                   16          8         7     ${EMPTY}     ${FALSE}       ${TRUE}
    [Template]  ForwardLastAckTraffic


*** Keywords ***
ForwardEmptyAcksEnableDisable
    [Arguments]  ${testId}

    # disable SSL
    updateSslSettings  ${atipSession}  ${FALSE}

    # disable TCP Reassembly and forward empty acks and check result
    ${tcpModeConfig}=  setTcpModeConfig  ${tcpMode}  ${FALSE}  ${FALSE}
    updateTcpConfig  ${atipSession}  ${tcpModeConfig}  0  ${httpStatus}
    Verify TCP Settings  ${FALSE}  ${FALSE}  ${FALSE}

    # enable TCP Reassembly and forward empty acks and check result
    ${tcpModeConfig}=  setTcpModeConfig  ${tcpMode}  ${TRUE}  ${TRUE}
    updateTcpConfig  ${atipSession}  ${tcpModeConfig}  0  ${httpStatus}
    Verify TCP Settings  ${FALSE}  ${TRUE}  ${TRUE}

    # disable TCP Reassembly and forward empty acks and check result
    ${tcpModeConfig}=  setTcpModeConfig  ${tcpMode}  ${FALSE}  ${FALSE}
    updateTcpConfig  ${atipSession}  ${tcpModeConfig}  0  ${httpStatus}
    Verify TCP Settings  ${FALSE}  ${FALSE}  ${FALSE}

    # enable forward empty acks and check result
    ${tcpModeConfig}=  setTcpModeConfig  ${tcpMode}  ${TRUE}  ${FALSE}
    updateTcpConfig  ${atipSession}  ${tcpModeConfig}  0  ${httpStatus}
    Verify TCP Settings  ${FALSE}  ${TRUE}  ${FALSE}

    # disable forward empty acks and check result
    ${tcpModeConfig}=  setTcpModeConfig  ${tcpMode}  ${FALSE}  ${FALSE}
    updateTcpConfig  ${atipSession}  ${tcpModeConfig}  0  ${httpStatus}
    Verify TCP Settings  ${FALSE}  ${FALSE}  ${FALSE}

    # enable SSL
    updateSslSettings  ${atipSession}  ${TRUE}
    Verify TCP Settings  ${TRUE}  ${FALSE}  ${FALSE}

    # disable SSL
    updateSslSettings  ${atipSession}  ${FALSE}
    ${ssl}=  getSslSettings  ${atipSession}
    Verify TCP Settings  ${FALSE}  ${FALSE}  ${FALSE}


ForwardEmptyAcksTraffic
    [Arguments]  ${captureTime}  ${pcapArgument}  ${bpsTest}  ${bpsNeighborhood}  ${enableSSL}  ${enableForwardEmptyAcks}  ${enableTCPForwarding}  ${jsonFilter}  ${restartAtip}

    atipLogin.Login  ${atipSession}

    # disable netflow acceleration
    Configure Detailed Statistics  ${atipSession}  ${FALSE}

    # enable/disable SSL according to parameter value
    updateSslSettings  ${atipSession}  ${enableSSL}

    # enable/disable forward empty acks and TCP Reassembly according to parameters values and check status
    ${tcpModeConfig}=  setTcpModeConfig  ${tcpMode}  ${enableForwardEmptyAcks}  ${enableTCPForwarding}
    updateTcpConfig  ${atipSession}  ${tcpModeConfig}  0  ${httpStatus}
    Verify TCP Settings  ${enableSSL}  ${enableForwardEmptyAcks}  ${enableTCPForwarding}

    # delete all filters
    deleteAllFilters  ${atipSession}

    # reset ATIP if TRUE
    Run Keyword If  ${restartAtip} == ${TRUE}  resetAtipAndWaitUntilNPReady  ${atipSession}  ${envVars}

    # add new filter
    createFilter  ${atipSession}  ${jsonFilter}
    rulePush  ${atipSession}

    # send traffic and capture
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTest}  600
    ${filterId}=  getFilterId  ${atipSession}  ${jsonFilter.name}
    runBPSAndCapture  ${atipSession}  ${bpsSession}  ${bpsTestConfig}  ${envVars}  ${filterId}  ${captureTime}

    ${tsharkFilterCommand}=  downloadCaptureAndGenerateFilterCommand  ${atipSession}  ${envVars}  ${remotePcapLocation}  ${TEST NAME}  ${pcapArgument}  text  ${EMPTY}  ${EMPTY}

    # filter capture according to display filter tshark command
    Connect on Linux Box
    Write  cd captures
    Execute Command  ${tsharkFilterCommand}  strip_prompt=True

    # validate capture using entire pcap filter
    matches validation  ${TEST NAME}  0  ${TRUE}

    # split pcap filter in constituent groups and filter capture according to all these display filters
    ${pcapDisplayFilterList}=  getPcapGroups  ${pcapArgument}

    :FOR    ${displayFilter}    IN    @{pcapDisplayFilterList}
    \    ${filterCaptureCommand}=  configFilteringCommand  ${TEST NAME}  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_1
    \    Execute Command  ${filterCaptureCommand}  strip_prompt=True
    \    matches validation  ${TEST NAME}_1  0  ${FALSE}


ForwardLastAckTraffic
    [Arguments]  ${bpsTest}  ${testDuration}  ${rxPkts}  ${txPkts}  ${certName}  ${tcpReasembly}  ${fwdEmptyAcks}

    # get debug stats in order to reset pkt stats
    readDebug  ${atipSession}

    # delete all filters
    deleteAllFilters  ${atipSession}

    # enable/disable forward empty acks and TCP Reassembly according to parameters values and check status
    ${tcpModeConfig}=  setTcpModeConfig  ${tcpMode}  ${fwdEmptyAcks}  ${tcpReasembly}
    updateTcpConfig  ${atipSession}  ${tcpModeConfig}  0  ${httpStatus}
    Verify TCP Settings  ${FALSE}  ${fwdEmptyAcks}  ${tcpReasembly}

    Run Keyword If    '${certName}'=='${EMPTY}'  Run Keywords
    ...    updateSslSettings  ${atipSession}  False
    ...    AND  Verify TCP Settings  ${FALSE}  ${fwdEmptyAcks}  ${tcpReasembly}
    ...    ELSE  Run Keywords
    ...    updateSslSettings  ${atipSession}  True
    ...    AND  deleteAllCerts  ${atipSession}
    ...    AND  uploadSeparateFiles  ${atipSession}  cert${certName}  ${certFilename}  ${keyFilename}
    ...    AND  Verify TCP Settings  ${TRUE}  ${fwdEmptyAcks}  ${tcpReasembly}

    # create forwarding filter
    ${filterConfig}=  configureFilter  ${envVars}  ForwardAll  333
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}

    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTest}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Run Keyword Unless  '${certName}' == '${EMPTY}'  Validate SSL Stats  ${rxPkts}

    ${debugStats}=  readDebug  ${atipSession}
    ${txPktsDebugStats}  ${rxPktsProcDebugStats}=  getDebugStats  ${envVars}  ${debugStats}
    Should Be Equal As Integers  ${rxPktsProcDebugStats}  ${rxPkts}
    Should Be Equal As Integers  ${txPktsDebugStats}  ${txPkts}

Verify TCP Settings
    [Arguments]  ${enabledBySSLStatus}  ${forwardEmptyAcksStatus}  ${enabledAlwaysStatus}
    ${tcpSettingsCfg}=  getTcpConfig  ${atipSession}  0  ${httpStatus}
    Should Be Equal As Strings  ${tcpSettingsCfg.enabledBySSL}  ${enabledBySSLStatus}
    Should Be Equal As Strings  ${tcpSettingsCfg.enabledAlways}  ${enabledAlwaysStatus}
    Should Be Equal As Strings  ${tcpSettingsCfg.forwardEmptyAcks}  ${forwardEmptyAcksStatus}

Validate SSL Stats
    [Arguments]  ${rxPkts}
    &{sslDebugStats}=  Create Dictionary
    ${sslDebugStats}=  getSSLStatsFromDebugPage  ${atipSession}
    ${sslEnc}=  Get From Dictionary  ${sslDebugStats}  SslNewEncryptedConns
    ${sslStd}=  Get From Dictionary  ${sslDebugStats}  SslStandardConns
    ${sslRes}=  Get From Dictionary  ${sslDebugStats}  SslResumedConns
    ${encPkts}=  Get From Dictionary  ${sslDebugStats}  SslEncryptedPackets
    Should Be Equal As Integers  ${encPkts}  ${rxPkts}
    ${sum}=  Evaluate  ${sslStd}+${sslRes}
    Should Be Equal As Integers  ${sslEnc}  ${sum}
    updateSslSettings  ${atipSession}  False

Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [Return]  ${output}

Matches Validation
    [Arguments]  ${STRING}  ${expectedValue}  ${match}
    ${output}=  Execute Command  wc -l < ${STRING}.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    # ${match}, if True, will verify if ${expectedValue} matches ${statsCaptureLB}, if False, verify the values are unequal
    Run Keyword If  ${match} == ${TRUE}  Should Be Equal As Integers  ${statsCaptureLB}  ${expectedValue}
    ...    ELSE  Should Not Be Equal As Integers  ${statsCaptureLB}  ${expectedValue}

Connect on Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Read
    [return]  ${openTelnet}

Become SU
    Write  su
    sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt

Configure Detailed Statistics
    [Arguments]  ${atipSession}  ${status}

    ${detailedStats}=  getConfigureStatsObj  ${status}
    changeConfig  ${atipSession}  ${detailedStats}

Configure ATIP
    atipLogin.Login  ${atipSession}

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}

    Connect on Linux Box
    # be sure that "captures" folder is deleted because it happened to remain created by root and had write protection against "atip" user; hence, no capture filter result file could be created by "atip" user when running tshark filtering command
    Become SU
    Write  rm -rf captures
    Write  exit

    # create temp folder for testing captures; it should be owned by "atip" user
    Write  mkdir captures
    Sleep  1s
    Close Connection

Clean ATIP
    Connect on Linux Box
    Become SU
    Execute Command  rm -rf captures
    atipLogout.logout  ${atipSession}