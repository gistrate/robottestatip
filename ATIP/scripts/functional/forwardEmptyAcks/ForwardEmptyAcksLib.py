from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.services.GeneralService import GeneralService
from atipAutoFrwk.services.atip.CaptureService import CaptureService
from atipAutoFrwk.services.atip.FileService import FileService
from atipAutoFrwk.services.atip.utility.FilterCaptureServices import FilterCaptureServices
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.atip.Capture import Capture
import time
import re

class ForwardEmptyAcksLib(object):

    @classmethod
    def configureFilter(cls, envVars, filterName, vlanId=None):
        filterConfig = FilterConfig(filterName)
        filterConfig.forward = True
        if envVars.atipType == AtipType.HARDWARE:
            filterConfig.addTransmitIds(vlanId)

        return filterConfig

    def runBPSAndCapture(self, webApiSession, bpsSession, bpsConfig, env, filterId, captureTime):

        # delete capture files from ATIP
        Capture.deleteCaptureFiles(webApiSession)

        # start test and wait for traffic to start
        GeneralService.startBpsTestAndWaitForTraffic(bpsSession, env, bpsConfig)

        # start capture
        Capture.controlCapture(webApiSession, filterId, True)

        # wait for capture to finish or stop it if capture time expired
        for i in range(int(captureTime)+1):
            status = Capture.captureRunningStatus(webApiSession)
            print('Waiting capture to finish for {}s'.format(i))
            if status and (i < int(captureTime)):
                time.sleep(1)
            elif i >= int(captureTime):
                Capture.controlCapture(webApiSession, filterId, False)
            else:
                break

        # stop traffic
        BpsOperations.forceStopBpsTestIfRunning(bpsSession)


    @classmethod
    def downloadCaptureAndGenerateFilterCommand(cls, webApiSession, envVars, remotePath, testId, displayFilter, filteringFormat, vlanId, outFilename=''):

        pcapFolder = CaptureService.getLatestCaptureAndUnzip(webApiSession)
        files = FileService.getFolderFileList(pcapFolder)
        localPcap = files[0]
        localPath = pcapFolder + localPcap
        remoteFile = testId + '.pcap'
        FileService.sftpCopyToLinuxBox(envVars, localPath, remotePath+remoteFile, 'to')
        FileService.removePcapAndFolder(webApiSession, pcapFolder)

        if envVars.atipType == AtipType.VIRTUAL:
            vlanId = ''
        tsharkCommand = FilterCaptureServices.configFilteringCommand(testId, displayFilter, filteringFormat, vlanId, outFilename)

        return tsharkCommand

    @classmethod
    def getPcapGroups(cls, pcapDisplayFilter):
        # it is used to extract all display filters from a big compound display filter
        group = re.compile('\(\(?(.*?)\)', re.IGNORECASE)
        groupList = []
        for match in group.finditer(pcapDisplayFilter):
            print('\r\n{}'.format(match.group(1)))
            groupList.append(match.group(1))

        return groupList

    @classmethod
    def setTcpModeConfig(cls, tcpModeConfig, fwdEmptyAck, tcpReassembly):
        tcpModeConfig.forwardEmptyAcks = fwdEmptyAck
        tcpModeConfig.enabledAlways = tcpReassembly

        return tcpModeConfig

    @classmethod
    def getDebugStats(cls, envVars, debugStats, indexSlotNumber=0):
        # I've extracted these values here because Debug Page library does not contain a method to extract both these values from one debug stats read and adding a method to do this seemed too particular for Debug Page service
        txStats = re.findall(r'NP::{}[\S\s]+?(\d+)\s+(\d+)\s+tx\s'.format(envVars.atipSlotNumber[indexSlotNumber]), debugStats)
        txPkts = int(txStats[0][0])
        rxProc = re.findall(r'NP::{}[\S\s]+?(\d+)\s+(\d+)\s+rx_proc'.format(envVars.atipSlotNumber[indexSlotNumber]), debugStats)
        rxProcPkts = int(rxProc[0][0])

        return txPkts, rxProcPkts