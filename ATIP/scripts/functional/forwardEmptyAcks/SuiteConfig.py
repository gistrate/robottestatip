from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.TcpModeConfig import TcpModeConfig
from http import HTTPStatus


class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    remotePcapLocation = '/home/atip/captures/'
    feaNetflix = FilterConfig('Automation Netflix Filter')
    feaNetflix.forward = True
    FilterConditionConfig.addAppCondition(feaNetflix, ApplicationType.NETFLIX)
    feaPakistan = FilterConfig('Automation FEA Pakistan Filter')
    feaPakistan.forward = True
    FilterConditionConfig.addGeoCondition(feaPakistan,GeoLocation.PAKISTAN)
    tcpMode = TcpModeConfig()
    httpStatus = HTTPStatus.OK
    pcap1 = '!((http.request.method == "GET") || (tcp.flags.ack == 1 && tcp.flags.fin == 1) || (http.response.code == 200 && http.response.phrase == OK))'
    pcap2 = '!((http.request.method == "GET") || (tcp.flags.ack == 1 && tcp.flags.fin == 1) || (http.response.code == 200 && http.response.phrase == OK) || (tcp.flags.ack == 1 && tcp.flags.fin == 0))'
    pcap3 = '!((http.request.method == "GET") || (tcp.flags.ack == 0 && tcp.flags.syn == 1) || (tcp.flags.ack == 1 && tcp.flags.syn == 1) || (tcp.flags.ack == 1 && tcp.flags.fin == 1) || (http.response.code == 200 && http.response.phrase == OK) || (tcp.flags.ack == 1 && tcp.flags.fin == 0))'
    certFilename = '../../../frwk/src/atipAutoFrwk/data/sslCert/cert512/cert512.txt'
    keyFilename = '../../../frwk/src/atipAutoFrwk/data/sslCert/cert512/key512.txt'