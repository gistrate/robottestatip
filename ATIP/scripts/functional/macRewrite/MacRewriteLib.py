from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.services.atip.CaptureService import CaptureService
from atipAutoFrwk.services.atip.FileService import FileService
from atipAutoFrwk.services.atip.utility.FilterCaptureServices import FilterCaptureServices
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.data.atip.AtipType import AtipType


class MacRewriteLib(object):
    def configureAndCreateFilter(self, webApiSession, envVars, forward, filterName, macRewriteUUID, vlanId = None):
        filterConfig = FilterConfig()
        filterConfig.forward = forward
        filterConfig.addMacRewriteUUIDS(macRewriteUUID)

        if filterName:
            filterConfig.name = filterName

        if envVars.atipType == AtipType.HARDWARE:
            filterConfig.addTransmitIds(vlanId)

        Filters.createFilter(webApiSession, filterConfig)
        Filters.rulePush(webApiSession)

        return filterConfig

    def buildDisplayFilter(self, field, value, itShouldBe = 'not equal'):
        # convert string 112233445566 into mac address 11:22:33:44:55:66
        mac = ':'.join(value[i:i + 2] for i in range(0, 12, 2))
        if itShouldBe == 'not equal':
            macDstArg = '(!(eth.dst == {0}))'.format(mac) if field in ('both','dst') else ''
            macSrcArg = '(!(eth.src == {0}))'.format(mac) if field in('both','src') else ''
        else:
            macDstArg = '(eth.dst == {0})'.format(mac) if field in ('both', 'dst') else ''
            macSrcArg = '(eth.src == {0})'.format(mac) if field in ('both', 'src') else ''

        combineArgs = ' && ' if field == 'both' else ''
        displayFilter = macDstArg + combineArgs + macSrcArg

        return displayFilter

    def downloadCaptureAndGenerateFilterCommand(cls, webApiSession, envVars, remotePath, testId, displayFilter, filteringFormat, outFilename=''):

        pcapFolder = CaptureService.getLatestCaptureAndUnzip(webApiSession)
        files = FileService.getFolderFileList(pcapFolder)
        localPcap = files[0]
        localPath = pcapFolder + localPcap
        remoteFile = testId + '.pcap'
        FileService.sftpCopyToLinuxBox(envVars, localPath, remotePath+remoteFile, 'to')
        FileService.removePcapAndFolder(webApiSession, pcapFolder)

        if envVars.atipType == AtipType.VIRTUAL:
            vlanId = ''
        tsharkCommand = FilterCaptureServices.configFilteringCommand(testId, displayFilter, filteringFormat, '', outFilename)

        return tsharkCommand