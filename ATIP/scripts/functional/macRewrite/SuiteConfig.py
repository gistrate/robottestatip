from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.config.atip.MacRewriteConfig import MacRewriteConfig

class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    macRewriteConfig = MacRewriteConfig()
    remotePcapLocation = '/home/atip/captures/'