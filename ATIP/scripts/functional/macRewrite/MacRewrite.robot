*** Settings ***
Documentation  Mac Rewrite test suite.

Variables  SuiteConfig.py

Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.webApi.atip.MacRewrites
Library    atipAutoFrwk.webApi.atip.ConfigureStats
Library    Collections
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.services.atip.SystemService.SystemService
Library    atipAutoFrwk.services.GeneralService
Library    atipAutoFrwk.webApi.traffic.BpsOperations  WITH NAME  bpsOperations
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    atipAutoFrwk.services.atip.DataMaskingService
Library    atipAutoFrwk.services.TestCondition
Library    atipAutoFrwk.services.atip.CaptureService
Library    Telnet  timeout=30  prompt=$
Library    String
Library    BuiltIn
Library    MacRewriteLib.py

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP
#Suite Teardown     Clean ATIP

Test Template  Configure Device and Run BPS Test

*** Test Cases ***
# testID                        bpsFile            noOfMacRewrites
MacRewrite_BUG1417445a    Ip filtering test 2            10
    [Template]  MacRewriteBUG1417445
MacRewrite_BUG1417445b    Ip filtering test 2            2
    [Template]  MacRewriteBUG1417445

# testID        field                regex                                 replacement     captureTime               bpsFile
TC001020469     both    \\x02\\x1a\\xc5\\x05[\\x00-\\xFF][\\x00-\\xFF]     112233445566         20        Netflix_mac05-05_10min_TC001020469
    [Template]  Verify Mac Rewrite
TC001024163      src    \\x02\\x1a\\xc5\\x05[\\x00-\\xFF][\\x00-\\xFF]     112233445566         20        Netflix_mac05-05_10min_TC001020469
    [Template]  Verify Mac Rewrite
TC001024164      dst    \\x02\\x1a\\xc5\\x05[\\x00-\\xFF][\\x00-\\xFF]     112233445566         20        Netflix_mac05-05_10min_TC001020469
    [Template]  Verify Mac Rewrite

*** Keywords ***
MacRewriteBUG1417445
    [Arguments]  ${bpsFile}  ${noOfMacRewrites}

    atipLogin.Login  ${atipSession}

    # disable netflow acceleration
    ${detailedStats}=  getConfigureStatsObj  ${FALSE}
    changeConfig  ${atipSession}  ${detailedStats}

    # enable mac rewrite and delete all existing mac rewrite masks
    Update Mac Rewrite Config And Verify  enable
    deleteAllMacItems  ${atipSession}

    @{macRewriteUUIDS}=  Create List

    # create mac rewrite masks in specified range
    : FOR  ${item}  IN RANGE  ${noOfMacRewrites}
    \    ${regex}=  Set Variable If  ${item} < 10  \\x02\\x1a\\xc5\\x0${item}[\\x00-\\xFF][\\x00-\\xFF]    \\x02\\x1a\\xc5\\x${item}[\\x00-\\xFF][\\x00-\\xFF]
    \    addMac  ${atipSession}  both  MacRewrite${item}  112233445566  0  ${regex}
    \    ${macRewriteConfig}=  Get Mac Rewrite Details  MacRewrite${item}
    \    # create a list with all mac rewrite masks created
    \    Append To List  ${macRewriteUUIDS}  ${macRewriteConfig.id}

    deleteAllFilters  ${atipSession}

    # create a forwarding filter associated with all mac rewrite masks created in previous step
    ${filterConfig}=  configureAndCreateFilter  ${atipSession}  ${envVars}  ${TRUE}  Mac Test Automation Filter${TEST NAME}  ${macRewriteUUIDS}  100

    # enable Data Masking
    configureDataMasking  ${atipSession}

    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  1300
    ${testId}=  startBpsTestAndWaitForTraffic  ${bpsSession}  ${envVars}  ${bpsTestConfig}
    ${bpsTestConfig.testId}=  Set Variable  ${testId}

    ${macRewriteItems}=  getMacRewritesItems  ${atipSession}
    ${macRewriteNumber}=  Get Length  ${macRewriteItems.macRewriteList}
    ${totalNumberOfMatches}=  Get Total Number Of Matches MacRewriteList  ${macRewriteItems.macRewriteList}
    # verify total number of matches for all mac rewrite masks is incremented
    Should Be True  ${totalNumberOfMatches} > 0

    # disable Data Masking
    configureDataMasking  ${atipSession}  ${FALSE}
    Sleep  40s

    # verify total number of matches for all mac rewrite masks is incrementing
    ${macRewriteItems}=  getMacRewritesItems  ${atipSession}
    ${newTotalNoOfMatches}=  Get Total Number Of Matches MacRewriteList  ${macRewriteItems.macRewriteList}
    #Log  ${newTotalNoOfMatches}
    Should Be True  ${totalNumberOfMatches} < ${newTotalNoOfMatches}

    # enable Data Masking
    configureDataMasking  ${atipSession}
    Sleep  40s
    # verify total number of matches for all mac rewrite masks is still incrementing compared to previous step
    ${macRewriteItems}=  getMacRewritesItems  ${atipSession}
    ${latestTotalNoOfMatches}=  Get Total Number Of Matches MacRewriteList  ${macRewriteItems.macRewriteList}
    Should Be True  ${newTotalNoOfMatches} < ${latestTotalNoOfMatches}

    bpsOperations.stopBps  ${bpsSession}  ${bpsTestConfig.testId}

Verify Mac Rewrite
    [Arguments]  ${field}  ${regex}  ${replacement}  ${captureTime}  ${bpsFile}

    # disable netflow acceleration
    ${detailedStats}=  getConfigureStatsObj  ${FALSE}
    changeConfig  ${atipSession}  ${detailedStats}

    # enable mac rewrite and delete all existing mac rewrite masks
    Update Mac Rewrite Config And Verify  enable
    deleteAllMacItems  ${atipSession}

    # add mac rewrite mask according to current values
    addMac  ${atipSession}  ${field}  macTest${TEST NAME}  ${replacement}  0  ${regex}

    # get id for created mac rewrite mask
    ${macRewriteDetails}=  Get Mac Rewrite Details  macTest${TEST NAME}

    deleteAllFilters  ${atipSession}

    # create forwarding filter and assign created mac rewrite mask
    ${filterConfig}=  configureAndCreateFilter  ${atipSession}  ${envVars}  ${TRUE}  Mac Test Automation Filter${TEST NAME}  ${macRewriteDetails.id}  100

    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  1300
    ${testId}=  startBpsTestAndWaitForTraffic  ${bpsSession}  ${envVars}  ${bpsTestConfig}
    ${bpsTestConfig.testId}=  Set Variable  ${testId}

    # get two consecutive readings of mac rewrite statistics and compare
    Sleep  40s

    ${macRewriteDetails}=  Get Mac Rewrite Details  macTest${TEST NAME}
    ${macRewriteCountOld}=  Set Variable  ${macRewriteDetails.rpt_count}

    Sleep  40s

    ${macRewriteDetails}=  Get Mac Rewrite Details  macTest${TEST NAME}
    ${macRewriteCountNew}=  Set Variable  ${macRewriteDetails.rpt_count}

    Should Be True    ${macRewriteCountNew} > ${macRewriteCountOld}

    # start capture on filter and analize capture content to check for mac address masking
    ${filterId}=  getFilterId  ${atipSession}  ${filterConfig.name}
    startAndWaitForCaptureToFinish  ${atipSession}  ${bpsSession}  ${filterId}  ${captureTime}

    bpsOperations.stopBps  ${bpsSession}  ${bpsTestConfig.testId}

    ${macRewriteDetails}=  Get Mac Rewrite Details  macTest${TEST NAME}
    Log  ${macRewriteDetails.rpt_count}

    # verify capture does not contain any unmasked frame
    ${displayFilter}=  buildDisplayFilter  ${field}  ${replacement}
    ${tsharkFilterCommand}=  downloadCaptureAndGenerateFilterCommand  ${atipSession}  ${envVars}  ${remotePcapLocation}  ${TEST NAME}  ${displayFilter}  text  ${EMPTY}

    # filter capture according to display filter tshark command
    Connect on Linux Box
    Write  cd captures
    Execute Command  ${tsharkFilterCommand}  strip_prompt=True

    matches validation  ${TEST NAME}  0


Update Mac Rewrite Config And Verify
    [Arguments]  ${state}
    updateMacRewriteSettings  ${atipSession}  ${state}
    ${macRewriteSettings}=  getMacRewritesSettings  ${atipSession}
    ${macRewriteSettingsStatus}=  call method  ${macRewriteSettings}  __getstate__
    ${macRewriteSettingsEnabled}=  get from dictionary  ${macRewriteSettingsStatus}  enabled
    Run Keyword If  '${state}' == 'enable'    Should Be True  ${macRewriteSettings.enabled}
    ...    ELSE  Should Not Be True  ${macRewriteSettings.enabled}

Get Mac Rewrite Details
    [Arguments]  ${macRewriteName}
    ${macRewriteItem}=  getMacRewritesItems  ${atipSession}
    ${macRewriteDetails}=  get from dictionary  ${macRewriteItem.macRewriteList}  ${macRewriteName}
    [Return]    ${macRewriteDetails}

Get Total Number Of Matches MacRewriteList
    [Arguments]  ${macRewriteList}

    ${totalNoOfMatches}=  Set Variable  0
    ${macRewriteValues}=  Get Dictionary Values  ${macRewriteList}
    : FOR  ${macRewrite}  IN  @{macRewriteValues}
    \    ${temp}=  Evaluate  ${totalNoOfMatches} + ${macRewrite.rpt_count}
    \    Set Test Variable  ${totalNoOfMatches}  ${temp}

    [Return]  ${totalNoOfMatches}

Connect on Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Read
    [return]  ${openTelnet}

Become SU
    Write  su
    sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt

Matches Validation
    [Arguments]  ${STRING}  ${expectedValue}
    ${output}=  Execute Command  wc -l < ${STRING}.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Be Equal As Integers  ${statsCaptureLB}  ${expectedValue}

Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [Return]  ${output}

Configure ATIP
    atipLogin.Login  ${atipSession}

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}

    Connect on Linux Box
    # be sure that "captures" folder is deleted because it happened to remain created by root and had write protection against "atip" user; hence, no capture filter result file could be created by "atip" user when running tshark filtering command
    Become SU
    Write  rm -rf captures
    Write  exit

    # create temp folder for testing captures; it should be owned by "atip" user
    Write  mkdir captures
    Sleep  1s
    Close Connection


Clean ATIP
    Connect on Linux Box
    Become SU
    Write  rm -rf captures
    atipLogout.logout  ${atipSession}


