*** Settings ***

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Logout
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.webApi.atip.PacketBufferGlobalConfig
Library    atipAutoFrwk.webApi.atip.SslConfig
Library    Collections

Suite Setup        Login ATIP
Suite Teardown     logout  ${webApiSession}


Default Tags  ${hardware}
*** Test Cases ***

Reset Packet Buffer Config
    ${response}  updatePacketBufferSettings  ${webApiSession}  bBufferPackets=False  nMaxInspectPackets=30
    should be equal  ${response.buffer_packets}  ${False}
    should be equal as integers  ${response.max_inspect_packets}    30

Get Packet Buffer Config
    ${response}  getPacketBuffersSettings  ${webApiSession}
    should be equal  ${response.buffer_packets}  ${False}
    should be equal as integers  ${response.max_inspect_packets}    30

Update Packet Buffer Config Only Enable
    ${response}  updatePacketBufferSettings  ${webApiSession}  bBufferPackets=True
    should be equal  ${response.buffer_packets}  ${True}

Update Packet Buffer Only Size 20
    ${response}  updatePacketBufferSettings  ${webApiSession}  nMaxInspectPackets=20
    should be equal as integers  ${response.max_inspect_packets}    20

Update Packet Buffer Disable and Size 30
    ${response}  updatePacketBufferSettings  ${webApiSession}  bBufferPackets=False  nMaxInspectPackets=30
    should be equal  ${response.buffer_packets}  ${False}
    should be equal as integers  ${response.max_inspect_packets}    30

###########################################################################################################

Disable SSL Passive Decryption Config
    ${response}  updateSslSettings  ${webApiSession}    False
    should be equal  ${response.passiveSSLDecryption}  ${False}

Enabled SSL Passive Decryption Config
    ${response}  updateSslSettings  ${webApiSession}    True
    should be equal  ${response.passiveSSLDecryption}  ${True}

Get SSL Passive Decryption Config
    ${response}  getSslSettings  ${webApiSession}
    should be equal  ${response.passiveSSLDecryption}  ${True}

Reset SSL Passive Decryption Config
    ${response}  updateSslSettings  ${webApiSession}    False
    should be equal  ${response.passiveSSLDecryption}  ${False}

Upload Separate Files Test
    ${fileCert}=  Set variable  /home/sajang/PycharmProjects/Hackathon2017/bps/BreakingPoint_serverA_1024.crt
    ${fileKey}=  Set variable  /home/sajang/PycharmProjects/Hackathon2017/bps/BreakingPoint_serverA_1024.key
    ${name}=  Set variable  BreakingPoint_serverA_1024

    ${response}  uploadSeparateFiles  ${webApiSession}  ${name}  ${fileCert}  ${fileKey}
    #log to console  \n Response is ${response}
    #log to console  ${response.certificates}
    #should be equal as integers  ${response.status_code}  200

Delete All SSL Certificates Test 1
    ${response}  deleteAllCerts  ${webApiSession}
    #log to console  \n Response is ${response}

Upload Single File Test
    ${fileCombined}=  Set variable  /home/sajang/PycharmProjects/Hackathon2017/sslkeys/deep_0001.two
    ${response}  uploadSingleFile  ${webApiSession}  ${fileCombined}
    #log to console  \n Response.txt is ${response}
    #should be equal as integers  ${response.status_code}  200

Retrieve All SSL Certificates Test
    ${response}  getSslCerts  ${webApiSession}
    #log to console  \n Response is ${response}

Delete All SSL Certificates Test 2
    ${response}  deleteAllCerts  ${webApiSession}
    #log to console  \n Response is ${response}

Upload Single File Test 2
    ${fileCombined}=  Set variable  /home/sajang/PycharmProjects/Hackathon2017/sslkeys/deep_0001.two
    ${response}  uploadSingleFile  ${webApiSession}  ${fileCombined}
    #log to console  \n Response.txt is ${response}
    ${fileCombined}=  Set variable  /home/sajang/PycharmProjects/Hackathon2017/sslkeys/deep_0002.two
    ${response}  uploadSingleFile  ${webApiSession}  ${fileCombined}
    #log to console  \n Response.txt is ${response}
    #should be equal as integers  ${response.status_code}  200
#
Bulk Delete All SSL Certificates Test
    ${response}  BulkdeleteAllCerts  ${webApiSession}
    #log to console  \n Response is ${response}

Upload Single File Test 3
    ${fileCombined}=  Set variable  /home/sajang/PycharmProjects/Hackathon2017/sslkeys/deep_0001.two
    ${response}  uploadSingleFile  ${webApiSession}  ${fileCombined}
    #should be equal as integers  ${response.status_code}  200
#
Bulk Delete All SSL Certificates Test Single Certs
    ${response}  BulkdeleteAllCerts  ${webApiSession}
    #log to console  \n Response is ${response}

Bulk Upload two SSL Certificates Test
    @{twoCerts}=  create list
    ...             /home/sajang/PycharmProjects/Hackathon2017/sslkeys/deep_0001.two
    ...             /home/sajang/PycharmProjects/Hackathon2017/sslkeys/deep_0002.two
    ${response}     bulkUploadSingleFile  ${webApiSession}     ${twoCerts}

Bulk Delete All SSL Certificates Test Single Certs 2
    ${response}  BulkdeleteAllCerts  ${webApiSession}
*** Keywords ***

Login ATIP
    ${response}  login  ${webApiSession}
    should be equal as integers  ${response.status_code}  ${httpStatusOK}  msg=${response.text}
    should contain  ${response.text}  ${loginSuccessful}