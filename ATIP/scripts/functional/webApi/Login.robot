*** Settings ***
Documentation  Example for login webApi tests. The tests assume it isn't the first login performed after install.

Variables  SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.Login
Library    atipAutoFrwk.webApi.atip.Configuration
Library    atipAutoFrwk.services.atip.SystemService

Default Tags  ${hardware}  ${virtual}
*** Test Cases ***
Simple Login
    ${response}  login  ${webApiSession}
    should be equal as integers  ${response.status_code}  ${httpStatusOK}  msg=${response.text}
    should contain  ${response.text}  ${loginSuccessful}

Clear System and Login
    [Tags]  ${virtual}
    [Documentation]  'On vatip it changes the password'
    clearSystemAndWaitUntilNPReady  ${webApiSession}  ${envVars}



