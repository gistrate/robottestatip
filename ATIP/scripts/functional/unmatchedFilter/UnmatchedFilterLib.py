from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.webApi.atip.stats.Lists import Lists
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.webApi.atip.stats.Ports import Ports
from atipAutoFrwk.services.atip.TcpSettingsService import TcpSettingsService
from atipAutoFrwk.services.atip.utility.FilterCaptureServices import FilterCaptureServices
from cmath import isclose
import time
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations


class UnmatchedFilterLib(object):

    def configureAndCreateFilter(self, webApiSession, envVars, forward, vlanId, appName='', filterName='', deny=False):
        filterConfig = FilterConfig()
        filterConfig.forward = forward

        if filterName:
            filterConfig.name = filterName

        if appName:
            if not isinstance(appName, list):
                appName = [appName]
            for app in appName:
                appConf = ApplicationType(app)
                FilterConditionConfig.addAppCondition(filterConfig,appConf)

        if envVars.atipType == AtipType.HARDWARE:
            filterConfig.addTransmitIds(vlanId)

        if deny == True:
            # Unmatched Traffic filter is hardcoded with id=1000, needs to be referred with this ID
            filterConfig.id = 1000
            filterConfig.deny = True
            filterConfig.isUnmatched = True
            Filters.updateFilter(webApiSession, filterConfig)
            return

        Filters.createFilter(webApiSession, filterConfig)
        Filters.rulePush(webApiSession)

        return filterConfig

    @classmethod
    def verifySustainedTxRate(cls, webApiSession, bpsSession, envVars, expectedRate, duration, bpsTestId):
        '''
        Verify traffic is forwarded at expected rate; verify tx traffic each second for a specific period and compare with previous reading and with expectedRate
        :param webApiSession: current session
        :param bpsSession: current BPS session
        :param envVars: environment params
        :param expectedRate: rate at which traffic should be forwarded
        :param duration: how long analyze should be performed (in seconds)
        :param bpsTestId: test ID of BPS test run
        :return: True if forwarding rate is sustained compared to expectedRate; else, it should fail
        '''

        txBytesList = []

        resp = TcpSettingsService.enableEmptyPktsForward(webApiSession)
        bpsStatus = BpsOperations.getBpsTestProgress(bpsSession, bpsTestId)
        i = 0
        while (bpsStatus != 100) and (i < int(duration)):
            txBytesStat = DebugPage.getTxStatsFromDebugPage(webApiSession, envVars, 'bytes')
            txBytesList.append(txBytesStat)
            time.sleep(1)
            bpsStatus = BpsOperations.getBpsTestProgress(bpsSession, bpsTestId)
            i += 1

        # remove 0 values
        txBytesList = [y for y in txBytesList if y != 0]

        # remove first and last value from list because they may contain abnormal values that may affect result
        txBytesList = txBytesList[1:-1]

        # calculate average rate
        txBytesRate = sum(txBytesList) / len(txBytesList)
        print('TX rate (bytes): {}'.format(txBytesRate))

        close10 = isclose(txBytesRate, int(expectedRate)/8, rel_tol=0.1)

        if (txBytesRate >= int(expectedRate)/8) or close10:
            return True

    @classmethod
    def getTopAppsStats(cls, webApiSession, appName, statType):
        topApps = Lists.getTopStats(webApiSession, StatsType.APPS, TimeInterval.HOUR).getList()

        for item in topApps:
            if item.id == appName:
                if statType == TopStats.TOTAL_COUNT:
                    return item.totalCount
                elif statType == TopStats.TOTAL_PKTS:
                    return item.totalPkts
                else:
                    return item.totalBytes

    @classmethod
    def getTopFilterStatisticsByType(cls, webApiSession, filterName, statType):
        topFilters = Lists.getTopStats(webApiSession, StatsType.RULES, TimeInterval.DAY).getList()

        for item in topFilters:
            if item.msg == filterName:
                if statType == TopStats.TOTAL_COUNT:
                    return item.totalCount
                elif statType == TopStats.TOTAL_PKTS:
                    return item.totalPkts
                else:
                    return item.totalBytes


    @classmethod
    def validateRate(cls, webApiSession, ruleId, typeStat, expectedRate):
        '''
        Validate ATIP rx rate per filter is matching bps send rate per component
        :param webApiSession: current session
        :param ruleId: filter to get statistics for
        :param typeStat: statistics field to get value for
        :param expectedRate: rate to compare with
        :return: boolean to indicate rate is matching bps component send rate
        '''

        portStats = Ports.getTopStats(webApiSession, typeStat, TimeInterval.HOUR, 10, None, ruleId)

        for item in portStats.getList():
            if 'Bits' in item.name:
                bitsList = item.data

        # remove 0 values from list
        bitsList = [y for y in bitsList if y !=0]

        # remove first and last value from list because they may contain incorrect values that may affect result
        bitsList = bitsList[1:-1]

        # calculate average rate
        avgRate = sum(bitsList)/len(bitsList)

        return isclose(avgRate, int(expectedRate), rel_tol=0.05)


    @classmethod
    def getUnmatchedFilterSessions(cls, webApiSession, envVars, testId, filteringType, vlanId, unmatchedApp, outFilename=''):
        appSession = 0
        tsharkComm = ''

        if unmatchedApp == "aljazeera":
            b = "al-jazeera"
            appSession = cls.getTopAppsStats(webApiSession, b, TopStats.TOTAL_COUNT)
            tsharkComm = cls.filterPcap(envVars, unmatchedApp, testId, filteringType, vlanId, outFilename)
        else:
            appSession = cls.getTopAppsStats(webApiSession, unmatchedApp, TopStats.TOTAL_COUNT)
            tsharkComm = cls.filterPcap(envVars, unmatchedApp, testId, filteringType, vlanId, outFilename)

        return appSession, tsharkComm

    @classmethod
    def filterPcap(cls, envVars, app, testId, filteringFormat, vlanId, outFilename=""):
        a = ''
        # because same test is executed on both platforms, removing vlan ID condition from display filter of tshark command is needed
        if envVars.atipType == AtipType.VIRTUAL:
            vlanId = ''
        if app == 'owa':
            a = 'outlook'
            displayFilter = 'frame contains "{}"'.format(a)
            tsharkCommand = FilterCaptureServices.configFilteringCommand(testId, displayFilter, filteringFormat, vlanId, outFilename)
        else:
            displayFilter = 'frame contains "{}"'.format(app)
            tsharkCommand = FilterCaptureServices.configFilteringCommand(testId, displayFilter, filteringFormat, vlanId, outFilename)

        return tsharkCommand









