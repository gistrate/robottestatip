from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.RealTimeStats import RealTimeStats
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.data.atip.AtipType import AtipType

class SuiteConfig(object):
    envVars = Environment()
    atipSession = WebApiSession(envVars.atipConnectionConfig)
    bpsSession = WebApiSession(envVars.bpsConnectionConfig)
    statsType = StatsType.APPS
    bpsStatsType = BpsStatsType.SESSIONS
    bpsSessions = RealTimeStats.APP_SUCCESSFUL
    atipSessions = TopStats.TOTAL_COUNT
    atipVirt = AtipType.VIRTUAL
    atipHw = AtipType.HARDWARE
    timeInterval = TimeInterval.DAY
    dynAppsType = StatsType.DYNAMIC
    fiveMin = TimeInterval.FIVEMIN
