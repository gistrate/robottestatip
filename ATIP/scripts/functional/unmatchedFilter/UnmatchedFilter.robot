*** Settings ***
Documentation  Unmatched Filter test suite.

Variables  SuiteConfig.py

Library    SuiteConfig.py
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    Collections
Library    atipAutoFrwk.services.atip.DataMaskingService
Library    atipAutoFrwk.webApi.atip.DataMasking.DataMasking
Library    atipAutoFrwk.services.atip.SystemService
Library    atipAutoFrwk.services.atip.AtipConfigService
Library    atipAutoFrwk.services.GeneralService  WITH NAME  generalService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.webApi.traffic.Login  WITH NAME  bpsLogin
Library    atipAutoFrwk.webApi.traffic.BpsComponentStats
Library    atipAutoFrwk.webApi.traffic.BpsOperations  WITH NAME  bpsOperations
Library    atipAutoFrwk.services.atip.utility.FilterCaptureServices
Library    atipAutoFrwk.services.traffic.BpsComponentStatsService
Library    atipAutoFrwk.services.traffic.BpsStatsService
Library    atipAutoFrwk.services.atip.FileService
Library    atipAutoFrwk.webApi.traffic.PortsOperations  WITH NAME  portsOperations
Library    atipAutoFrwk.webApi.atip.TopFilters
Library    atipAutoFrwk.webApi.atip.stats.Lists  WITH NAME  lists
Library    Telnet  timeout=30   prompt=$
Library    String
Library    BuiltIn
Library    UnmatchedFilterLib.py

Default Tags  ATIP  functional  hardware  virtual

Suite Setup        Configure ATIP
Suite Teardown     Clean ATIP


*** Test Cases ***
# testID                 bpsTest                         app1     app2         expectedRate
TC000927482     deny_by_troughput_10Gbps_4Apps          netflix  facebook       10000000000
    [Tags]  hardware
    [Template]  Verify 1
TC000927482     deny_by_troughput_10Gbps_4Apps_vatip    netflix  facebook        500000000
    [Tags]  virtual
    [Setup]  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    [Template]  Verify 1

# testID                 bpsTest                                    filterName                    matchedAppsString                                          unmatchedAppsString
TC000927480      mihail_demo2_microsoft_2CPS                        microsoftDemo    microsoft.com;windowsupdate.com;OutlookWebAccess     netflix;facebook;hbo;yahoo;aol;amazon;aljazeera
    [Setup]  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
    [Template]  Verify 2

*** Keywords ***

Verify 1
    [Arguments]  ${bpsTest}  ${app1}  ${app2}  ${expectedRate}

    atipLogin.Login  ${atipSession}

    # create 2 app filters
    configureAndCreateFilter  ${atipSession}  ${envVars}  ${FALSE}  500  ${app1}  ${app1}  ${FALSE}
    configureAndCreateFilter  ${atipSession}  ${envVars}  ${FALSE}  500  ${app2}  ${app2}  ${FALSE}

    # configure unmatched traffic filter
    configureAndCreateFilter  ${atipSession}  ${envVars}  ${TRUE}  100  ${EMPTY}  Unmatched traffic  ${TRUE}

    # push changes to NP
    rulePush  ${atipSession}

    ${bpsTestConfig}=  createBpsTestConfig  deny_by_troughput_10Gbps_4Apps_10min  600
    ${testId}=  generalService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    ${debugStats}=  readDebug  ${atipSession}
    Log  ${debugStats}

    resetStats  ${atipSession}

    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTest}  3800

    ${testId}=  startAndWaitForTraffic  ${bpsSession}  ${bpsTestConfig.testName}  ${envVars.bpsPortGroup}
    ${bpsTestConfig.testId}=  set variable  ${testId}

    # verify ATIP is forwarding traffic at sustained rate
    ${okRate}=  verifySustainedTxRate  ${atipSession}  ${bpsSession}  ${envVars}  ${expectedRate}  ${bpsTestConfig.testDuration}  ${bpsTestConfig.testId}
    Should Be Equal As Strings  ${okRate}  ${TRUE}

    bpsOperations.stopBps  ${bpsSession}  ${bpsTestConfig.testId}

    # validate forwarding rate for Unmatched Traffic Filter against expectedRate
    atipLogin.Login  ${atipSession}
    ${checkRate}=  validateRate  ${atipSession}  1000   ${statsType}  ${expectedRate}
    Should Be Equal As Strings  ${checkRate}  ${TRUE}

    # validate Unmatched Traffic Filter sessions equals sum of BPS components corresponding to dynamic apps
    ${bpsComponentsStats}  getAllComponentsStats  ${bpsSession}  ${bpsTestConfig}
    ${bpsSessionsComp1}  getStatItem  ${bpsComponentsStats['appsim_1']}  ${bpsStatsType}
    ${bpsSessionsComp2}  getStatItem  ${bpsComponentsStats['appsim_2']}  ${bpsStatsType}
    ${bpsUnmatchedCompSessions}=  evaluate  ${bpsSessionsComp1} + ${bpsSessionsComp2}

    ${unmatchedFilterSessions}=  getTopFilterStatisticsByType  ${atipSession}  Unmatched traffic  ${atipSessions}

    Should Be Equal As Integers  ${unmatchedFilterSessions}  ${bpsUnmatchedCompSessions}

Verify 2
    [Arguments]  ${bpsTest}  ${filterName}  ${matchedAppsString}  ${unmatchedAppsString}

   # run traffic for MW to learn dynamic apps
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsTest}  360
    bpslogin.Login  ${bpsSession}
    portsOperations.reserveBpsPorts  ${bpsSession}  ${envVars.bpsSlot}  ${envVars.bpsPortList}  ${envVars.bpsPortGroup}
    ${testId}=  startAndWaitForTraffic  ${bpsSession}  ${bpsTestConfig.testName}  ${envVars.bpsPortGroup}
    ${bpsTestConfig.testId}=  set variable  ${testId}

    ${matchedAppList}=  Split String  ${matchedAppsString}  ;
    ${unmatchedAppList}=  Split String  ${unmatchedAppsString}  ;
    # only first two apps from matchedAppList are dynamic apps
    @{filterDynApps}=  Get Slice From List  ${matchedAppList}  0  2

    # repeatedly check until dynamic apps are learnt
    wait until keyword succeeds  10 min  1 sec  Validate Dynamic Apps  ${bpsSession}  ${bpsTestConfig}  ${bpsTestConfig.testId}  ${dynAppsType}  ${fiveMin}  ${filterDynApps}

    # create not forwarding filter with matching app list as condition
    configureAndCreateFilter  ${atipSession}  ${envVars}  ${FALSE}  ${EMPTY}  ${matchedAppList}  ${filterName}  ${FALSE}

    # configure Unmatched Traffic filter forwarding
    configureAndCreateFilter  ${atipSession}  ${envVars}  ${TRUE}  100  ${EMPTY}  Unmatched traffic  ${TRUE}

    # push changes to NP
    rulePush  ${atipSession}

    resetStats  ${atipSession}

    # start traffic capture on linux box interface
    Connect on Linux Box
    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap -B 30720
    Read Until  ${envVars.linuxConnectionConfig.port}

    # send traffic
    ${testId}=  generalService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    # display debug page statistics
    ${debugStats}=  readDebug  ${atipSession}
    Log  ${debugStats}

    Stop Capture  2

    # verify filter matched apps are not contained in capture
    :FOR    ${app}    IN    @{matchedAppList}
    \    ${filterCaptureCommand}=  filterPcap  ${envVars}  ${app}  ${TEST NAME}  text  ${EMPTY}
    \    Validate App In Capture  ${app}  ${TEST NAME}  ${filterCaptureCommand}  0

    # verify all other apps with didn't match filter are contained in linux box capture, forwarded by Unmatched Traffic filter
    :FOR    ${app}    IN    @{unmatchedAppList}
    \    ${appSession}  ${filterCaptureCommand}=  getUnmatchedFilterSessions  ${atipSession}  ${envVars}  ${TEST NAME}  text  100  ${app}  ${TEST NAME}_${app}
    \    Validate App In Capture  ${app}  ${TEST NAME}_${app}  ${filterCaptureCommand}  ${appSession}

Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [Return]  ${output}

Matches Validation
    [Arguments]  ${STRING}  ${expectedValue}
    ${output}=  Execute Command  wc -l < ${STRING}.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Be Equal As Integers  ${statsCaptureLB}  ${expectedValue}

Validate Dynamic Apps
    [Arguments]  ${bpsSession}  ${bpsTestConfig}  ${testId}  ${dynAppsType}  ${timeInterval}  ${filterDynApps}

    # because bps runs traffic for 2mins only, bps test needs to be restarted several times until dynamic apps are learnt
    ${bpsStatus}=  bpsOperations.getBpsTestProgress  ${bpsSession}  ${testId}
    run keyword if  ${bpsStatus} == 100  generalService.runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    # get dynamic apps list
    ${topStatsApps}=  lists.getTopStats  ${atipSession}  ${dynAppsType}  ${timeInterval}
    ${topStatsAppsList}=  call method  ${topStatsApps}  getList

    @{dynAppsList}=  create list
    :FOR    ${app}    IN    @{topStatsAppsList}
    \    ${appName}=  call method  ${app}  getName
    \    append to list  ${dynAppsList}  ${appName}

    # sort both expected and actual dynamic apps lists in order to compare them
    sort list  ${dynAppsList}
    sort list  ${filterDynApps}
    lists should be equal  ${dynAppsList}  ${filterDynApps}

Validate App In Capture
    [Arguments]  ${app}  ${STRING}  ${command}  ${expectedValue}

    Connect on Linux Box
    Write  cd captures

    # filter capture according to display filter tshark command
    Execute Command  ${command}  strip_prompt=True

    # verify number of app records from capture match number of app sessions from Atip
    matches validation  ${STRING}  ${expectedValue}

Connect on Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Read
    [return]  ${openTelnet}

Become SU
    Write  su
    sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt

Stop Capture
    [Arguments]  ${index}
    Switch Connection  ${index}
    Write Control Character     BRK
    Read Until Prompt
    Close Connection

Configure ATIP
    atipLogin.Login  ${atipSession}

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}

    Connect on Linux Box

    # create temp folder for testing captures
    Write  mkdir captures

    Close Connection

Clean ATIP
    Connect on Linux Box
    Execute Command  rm -rf captures
    atipLogout.logout  ${atipSession}


