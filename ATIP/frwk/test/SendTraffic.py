
from http import HTTPStatus

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.webApi.atip.TopFilters import TopFilters
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout

########## Test Suite Setup ##########
# Define test config
# Define filters
facebookFilter = FilterConfig("Facebook_Test")
FilterConditionConfig.addAppCondition(facebookFilter, ApplicationType.FACEBOOK)

allMatchingFilter = FilterConfig("allMatchingFilter")
FilterConditionConfig.addAppCondition(allMatchingFilter, [ApplicationType.NETFLIX, ApplicationType.FACEBOOK, ApplicationType.AOL])
FilterConditionConfig.addGeoCondition(allMatchingFilter, GeoLocation.CHINA)
tcpFilter = FilterConfig("TCP_Test")
FilterConditionConfig.addProtocolCondition(tcpFilter, NetworkProtocol.TCP)
# Define ATIP config
atipConfig = AtipConfig()
atipConfig.addFilters([facebookFilter, allMatchingFilter, tcpFilter])
# Define bps config
bpsTestConfig = BpsTestConfig('china_netflix_facebook_aol_15sec', 15)

# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
bpsSession = WebApiSession(env.bpsConnectionConfig)

# Configure ATIP
Login.login(atipSession)

########## Test Setup ##########
System.resetStats(atipSession)
Filters.deleteAllFilters(atipSession, HTTPStatus.OK)
AtipConfigService.createConfig(atipSession, atipConfig, env)

# Configure BPS and send traffic
BpsLogin.login(bpsSession)
PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)
testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup, bpsTestConfig.testDuration)

########## Test ##########
checkFilterExists = lambda : facebookFilter.name in str(TopFilters.getTopFiltersStats(atipSession))
TestCondition.waitUntilTrue(checkFilterExists, 'Couldn\'t find filter in top filter stats: {}'.format(tcpFilter.name),
                             totalTime=StatsTimeout.TOP_FILTERS.waitTime, iterationTime=StatsTimeout.ITERATION_TIME.waitTime)  # @UndefinedVariable

wrongName = "wrongName"
filterExistsWrongCondition = lambda : wrongName in str(TopFilters.getTopFiltersStats(atipSession))
TestCondition.waitUntilTrue(filterExistsWrongCondition, 'Couldn\'t find filter in top filter stats: {}'.format(wrongName),
                             totalTime=10, iterationTime=1)

