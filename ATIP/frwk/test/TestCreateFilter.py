from http import HTTPStatus

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.services.atip.FiltersService import FiltersService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout

env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
 
Login.login(atipSession)

Filters.deleteAllFilters(atipSession, HTTPStatus.OK)

facebookFilter = FilterConfig("facebook")
FilterConditionConfig.addAppCondition(facebookFilter, ApplicationType.FACEBOOK)

FiltersService.createFilter(atipSession, facebookFilter)

responseDuplicateFilter = Filters.createFilter(atipSession, facebookFilter, HTTPStatus.INTERNAL_SERVER_ERROR)

print("Got response from ATIP: {}".format(responseDuplicateFilter))

Logout.logout(atipSession)
