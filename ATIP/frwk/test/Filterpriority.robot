*** Settings ***
Documentation  Filter Priority test suite.

#Variables  SuiteConfig.py
#Variables  ../../scripts/functional/stats/lib/StatsDefaultConfig.py
#
#Library    FilterPriorityLib.py
Library    atipAutoFrwk.webApi.atip.System
Library    atipAutoFrwk.webApi.atip.Logout  WITH NAME  atipLogout
Library    atipAutoFrwk.webApi.atip.Login  WITH NAME  atipLogin
Library    atipAutoFrwk.services.atip.AtipConfigService
#Library    Collections
#Library    atipAutoFrwk.services.atip.SystemService.SystemService
#Library    atipAutoFrwk.services.atip.NPService
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.Capture
Library    atipAutoFrwk.services.atip.CaptureService
Library    atipAutoFrwk.services.traffic.BpsTestService
Library    atipAutoFrwk.services.GeneralService
#Library    atipAutoFrwk.webApi.atip.DataMasking
Library    atipAutoFrwk.webApi.atip.Filters
Library    atipAutoFrwk.webApi.atip.Netflow
Library    atipAutoFrwk.webApi.atip.stats.Pie
#Library    atipAutoFrwk.webApi.atip.Capture
#Library    atipAutoFrwk.services.atip.CaptureService
Library    atipAutoFrwk.services.atip.utility.IxFlowParsing
#Library    atipAutoFrwk.webApi.atip.EngineInfo
#Library    atipAutoFrwk.services.atip.DataMaskingService
#Library    Telnet  timeout=1000  prompt=$  default_log_level=DEBUG  telnetlib_log_level=DEBUG
Library    BuiltIn
Library    String

Default Tags  ATIP  functional  hardware  virtual

Test Setup        Configure ATIP
Suite Teardown     Clear ATIP


*** Test Cases ***

# higher priority match       bpsTest                                    app1     app2     app3     geo     ip     version     odid     dns     ipMethod
#TC001010128               TC001010128_aol_facebook_netflix_china      facebook   aol    netflix    CN    1.8.*.*    10        125    8.8.8.8    STATIC
#    [Teardown]  Close All Connections
#    [Template]  Verify high priority
#    [Tags]  hardware

# forward and data masking    bpsTest                                   app1     app2     app3     geo
#TC001011807               TC001010132_4apps_china                     facebook   amazon     netflix   CN
#    #[Setup]  Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
#    [Teardown]  Close All Connections
#    [Template]  Verify Forward and Data Masking
#    [Tags]  hardware

TC0001
    atipLogin.Login  ${atipSession}
    updateFilterPriority  ${atipSession}  Filter1  3
    updateFilterPriority  ${atipSession}  Filter2  2
    updateFilterPriority  ${atipSession}  Filter3  1
#
## PCAP                       bpsTest                                    app                        geo
#TC001011707               TC001011707_facebook_china                  facebook                     CN
#    [Setup]  Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}
#    [Teardown]  Close All Connections
#    [Template]  Verify PCAP
#    [Tags]  hardware
#
## vATIP                       bpsTest                                   app1     app2     app3     geo
#Scenario1               TC001010128_aol_facebook_netflix_china        facebook   aol    netflix    CN
#    [Template]  Verify filter priority vATIP
#    [Tags]  virtual

*** Keywords ***

Verify high priority
    [Arguments]  ${bpsFile}  ${app1}  ${app2}  ${app3}  ${geo}  ${ip}  ${netflowVersion}  ${odid}  ${dns}  ${ipMethod}

    ${atipConfig}=  FilterPriorityLib.configureHeaderMask  X  myMask  6  L2  6  ${FALSE}

    # configure Netflow
    ${newAtipConfig}=  configureNetflow  ${atipConfig}  ${netflowVersion}  ${odid}  ${ipMethod}  ${dns}

    createConfig  ${atipSession}  ${newAtipConfig}

    # get Header Mask ID
    ${hdrMaskId}=  Get Header Mask ID  myMask

    # get Netflow collectors IDs
    ${firstCollectorId}=  Get Netflow Collector ID  0
    ${secondCollectorId}=  Get Netflow Collector ID  1

    deleteAllFilters  ${atipSession}

    resetStats  ${atipSession}

    ${filterConfig}=  configureFilterForward  Filter1  ${True}  100  ${app1}  ${None}  ${None}  1  ${False}  ${None}
    createFilter  ${atipSession}  ${filterConfig}

    ${filterConfig}=  configureFilterForward  Filter2  ${True}  200  ${app2}  ${hdrMaskId}  ${None}  2  ${False}  ${None}
    createFilter  ${atipSession}  ${filterConfig}

    ${filterConfig}=  configureFilterForward  Filter3  ${True}  300  ${None}  ${None}  ${geo}  3  ${True}  ${firstCollectorId}
    createFilter  ${atipSession}  ${filterConfig}

    ${filterConfig}=  configureFilterForward  Filter4  ${False}  400  ${app3}  ${None}  ${None}  4  ${False}  ${None}
    createFilter  ${atipSession}  ${filterConfig}

    @{appList}=  create list  ${app1}  ${app2}  ${app3}
    ${filterConfig}=  configureFilterForward  Filter5  ${True}  500  ${appList}  ${None}  ${None}  5  ${True}  ${secondCollectorId}
    createFilter  ${atipSession}  ${filterConfig}

    # push changes to NP
    rulePush  ${atipSession}

    # Start traffic capture on linux box interface
    Connect on Linux Box
    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap -B 30720
    Read Until  ${envVars.linuxConnectionConfig.port}

    # Start traffic capture on netflow collector interface
    Connect on Linux Box
    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.netflowCollectorConnectionConfig.port} -w ${TEST NAME}_netflow.pcap -B 30720
    Read Until  ${envVars.netflowCollectorConnectionConfig.port}

    # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    # stop captures manually on each telnet connection, configuring them to auto finish did not correlate well with bps traffic run each time
    Stop Capture  2
    Stop Capture  3

    # display debug page statistics
    ${debugStats}=  readDebug  ${atipSession}
    Log  ${debugStats}

    # Filter capture file and export matching packets to a result file for validation
    Connect on Linux Box
    Become SU
    Execute Command  mv ./parsing.py ./captures  strip_prompt=True
    Write  cd captures
    Read Until Prompt

    #run keyword and return status  validate capture  ${app1}  text  100  ${TEST NAME}  ${atipSessions}  ${TEST NAME}_10
    validate capture  ${app1}  text  100  ${TEST NAME}  ${atipSessions}  ${TEST NAME}_1
    validate capture  ${app2}  text  200  ${TEST NAME}  ${atipSessions}  ${TEST NAME}_2

    # validate ip in capture
    ${displayFilter}=  catenate  not ip.host matches ${ip}
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}  ${displayFilter}  text  300  ${TEST NAME}_3
    Execute Command  ${filteringCommand}
    matches validation  ${TEST NAME}_3  0

    validate capture  ${app3}  text  300  ${TEST NAME}  ${atipSessions}  ${TEST NAME}_4
    Validate No Traffic In Capture  ${TEST NAME}  400  ${TEST NAME}_5

    ${firstCollector}=  Get Netflow Collector IP  0
    ${secondCollector}=  Get Netflow Collector IP  1

    Validate Apps in Netflow Capture  ${firstCollector}  ${secondCollector}  ${TEST NAME}  ${app1}  ${app2}  ${app3}  China


Verify Forward and Data Masking
    [Arguments]  ${bpsFile}  ${app1}  ${app2}  ${app3}  ${geo}

    ${hdrMaskId}=  Configure Header Mask And Reset Stats  X  MyMask  6  L2  6  ${FALSE}

    ${filterConfig}=  configureFilterForward  Filter1  ${True}  100  ${app1}  ${None}  ${None}  1  ${False}  ${None}
    createFilter  ${atipSession}  ${filterConfig}

    @{appList}=  create list  ${app1}  ${app2}
    ${filterConfig}=  configureFilterForward  Filter2  ${True}  200  ${appList}  ${hdrMaskId}  ${None}  2  ${False}  ${None}
    createFilter  ${atipSession}  ${filterConfig}

    ${filterConfig}=  configureFilterForward  Filter3  ${True}  300  ${None}  ${None}  ${geo}  3  ${False}  ${None}
    createFilter  ${atipSession}  ${filterConfig}

     # push changes to NP
     rulePush  ${atipSession}

    # Start traffic capture on linux box interface
    Connect on Linux Box
    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}_1.pcap
    Read Until  ${envVars.linuxConnectionConfig.port}

     # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  1

    Connect on Linux Box
    Become SU
    Write  cd captures

    validate capture  ${app1}  text  100  ${TEST NAME}_1  ${atipSessions}  ${TEST NAME}_1_1
    validate capture  ${app2}  text  200  ${TEST NAME}_1  ${atipSessions}  ${TEST NAME}_1_2
    validate capture  ${app3}  text  300  ${TEST NAME}_1  ${atipSessions}  ${TEST NAME}_1_3

    resetStats  ${atipSession}

    # change priorities
    updateFilterPriority  ${atipSession}  Filter1  2
    updateFilterPriority  ${atipSession}  Filter2  1
    updateFilterPriority  ${atipSession}  Filter3  3

    # start capture on linux box
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}_2.pcap
    Read Until  ${envVars.linuxConnectionConfig.port}

     # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  2

    Connect on Linux Box
    Write  cd captures
    validate capture  ${app1}  text  200  ${TEST NAME}_2  ${atipSessions}  ${TEST NAME}_2_1
    validate capture  ${app2}  text  200  ${TEST NAME}_2  ${atipSessions}  ${TEST NAME}_2_2
    Validate No Traffic In Capture  ${TEST NAME}_2  100  ${TEST NAME}_2_3
    validate capture  ${app3}  text  300  ${TEST NAME}_2  ${atipSessions}  ${TEST NAME}_2_4

    resetStats  ${atipSession}

    # change priorities
     updateFilterPriority  ${atipSession}  Filter1  2
     updateFilterPriority  ${atipSession}  Filter2  3
     updateFilterPriority  ${atipSession}  Filter3  1

    # start capture on linux box
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}_3.pcap
    Read Until  ${envVars.linuxConnectionConfig.port}

    # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  3

    Connect on Linux Box
    Write  cd captures
    validate capture  ${app1}  text  300  ${TEST NAME}_3  ${atipSessions}  ${TEST NAME}_3_1
    validate capture  ${app2}  text  300  ${TEST NAME}_3  ${atipSessions}  ${TEST NAME}_3_2
    validate capture  ${app3}  text  300  ${TEST NAME}_3  ${atipSessions}  ${TEST NAME}_3_3
    Validate No Traffic In Capture  ${TEST NAME}_3  100  ${TEST NAME}_3_4
    Validate No Traffic In Capture  ${TEST NAME}_3  200  ${TEST NAME}_3_5

Verify PCAP
    [Arguments]  ${bpsFile}  ${app}  ${geo}

    ${hdrMaskId}=  Configure Header Mask And Reset Stats  X  MyMask  6  L2  6  ${FALSE}

    ${filterConfig}=  configureFilterForward  Filter1  ${False}  0  ${None}  ${None}  ${geo}  1  ${False}  ${None}
    createFilter  ${atipSession}  ${filterConfig}
    ${filterConfig}=  configureFilterForward  Filter2  ${True}  100  ${app}  ${hdrMaskId}  ${None}  2  ${False}  ${None}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}

    # start capture on linux box
    Connect on Linux Box
    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}_1.pcap -B 30720
    Read Until  ${envVars.linuxConnectionConfig.port}

    # run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  1

    Connect on Linux Box
    Write  cd captures

    validate capture  ${app}  text  100  ${TEST NAME}_1  ${atipSessions}  ${TEST NAME}_1

    resetStats  ${atipSession}

    # perform captures
    ## get filter id
    ${filter}=  getFilterId  ${atipSession}  Filter1

    ## delete capture files from ATIP
    deleteCaptureFiles  ${atipSession}

    ## start capture on ATIP
    controlCapture  ${atipSession}  ${filter}  ${True}

    ## start capture on linux box
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}_2.pcap -B 30720
    Read Until  ${envVars.linuxConnectionConfig.port}

    ## run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    Stop Capture  2

    Connect on Linux Box
    Write  cd captures

    Validate No Traffic In Capture  ${TEST NAME}_2  100  ${TEST NAME}_2
    ## stop capture on ATIP
    controlCapture  ${atipSession}  ${filter}  ${False}

    ## download last PCAP and analyze
    ${localPcapFolder}=  getLatestCaptureAndUnzip  ${atipSession}
    transferAtipCapture  ${atipSession}  ${envVars}  ${localPcapFolder}  ${captureLocation}${TEST NAME}_3.pcap
    ${displayFilter}=  catenate  frame contains  "${app}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}_3  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_3_1
    Execute Command  ${filteringCommand}
    ${output}=  Execute Command  wc -l < ${TEST NAME}_3_1.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Not Be Equal As Integers  ${statsCaptureLB}  0

Verify filter priority vATIP
    [Arguments]  ${bpsFile}  ${app1}  ${app2}  ${app3}  ${geo}

    configureDataMasking  ${atipSession}  ${TRUE}  X
    createHeaderMask  ${atipSession}  srcMac  6  L2  6
    createHeaderMask  ${atipSession}  dstMac  6  L2  0

    atipLogin.Login  ${atipSession}

    # get Header Mask ID
    ${hdrMask1}=  Get Header Mask ID  srcMac
    ${hdrMask2}=  Get Header Mask ID  dstMac

    deleteAllFilters  ${atipSession}
    resetStats  ${atipSession}

    ${filterConfig}=  configureFilterForward  Filter1  ${False}  100  ${None}  ${None}  ${geo}  1  ${False}  ${None}
    createFilter  ${atipSession}  ${filterConfig}
    ${filterConfig}=  configureFilterForward  Filter2  ${True}  200  ${app1}  ${hdrMask1}  ${None}  2  ${False}  ${None}
    createFilter  ${atipSession}  ${filterConfig}
    ${filterConfig}=  configureFilterForward  Filter3  ${True}  300  ${app2}  ${hdrMask2}  ${None}  3  ${False}  ${None}
    createFilter  ${atipSession}  ${filterConfig}
    rulePush  ${atipSession}

    resetStats  ${atipSession}

    # do captures
    ## get filter id
    ${filter}=  getFilterId  ${atipSession}  Filter1

    ## delete capture files from ATIP
    deleteCaptureFiles  ${atipSession}

    ## start capture on ATIP
    controlCapture  ${atipSession}  ${filter}  ${True}

    ## start capture on linux box
    Connect on Linux Box
    Write  cd captures
    Become SU
    Write  tcpdump -U -i ${envVars.linuxConnectionConfig.port} -w ${TEST NAME}.pcap
    Read Until  ${envVars.linuxConnectionConfig.port}

    ## run BPS test
    ${bpsTestConfig}=  createBpsTestConfig  ${bpsFile}  120
    runBpsTest  ${bpsSession}  ${envVars}  ${bpsTestConfig}

    ## stop capture on ATIP and linux box
    controlCapture  ${atipSession}  ${filter}  ${False}
    Stop Capture  2

    ## wait capture to stop
    waitCaptureToFinish  ${atipSession}  ${filter}  180

    ${displayFilter}=  catenate  frame contains "${app1}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_1
    Connect on Linux Box
    Write  cd captures
    Execute Command  ${filteringCommand}
    matches validation  ${TEST NAME}_1  0

    ${displayFilter}=  catenate  frame contains "${app2}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_2
    Execute Command  ${filteringCommand}
    matches validation  ${TEST NAME}_2  0

    ## download last PCAP and analyze for each app
    ${localPcapFolder}=  getLatestCaptureAndUnzip  ${atipSession}
    transferAtipCapture  ${atipSession}  ${envVars}  ${localPcapFolder}  ${captureLocation}${TEST NAME}_1.pcap
    ${displayFilter}=  catenate  http.host contains "${app1}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}_1  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_1_1
    Execute Command  ${filteringCommand}
    ${output}=  Execute Command  wc -l < ${TEST NAME}_1_1.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Not Be Equal As Integers  ${statsCaptureLB}  0

    ${displayFilter}=  catenate  http.host contains "${app2}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}_1  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_1_2
    Execute Command  ${filteringCommand}
    ${output}=  Execute Command  wc -l < ${TEST NAME}_1_2.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Not Be Equal As Integers  ${statsCaptureLB}  0

    ${displayFilter}=  catenate  http.host contains "${app3}"
    ${filteringCommand}=  configFilterCommand  ${TEST NAME}_1  ${displayFilter}  text  ${EMPTY}  ${TEST NAME}_1_3
    Execute Command  ${filteringCommand}
    ${output}=  Execute Command  wc -l < ${TEST NAME}_1_3.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Not Be Equal As Integers  ${statsCaptureLB}  0

Configure Header Mask And Reset Stats
    [Arguments]  ${maskChar}  ${maskName}  ${hdrLength}  ${hdrHeader}  ${hdrStartOffset}  ${enableTurboMode}

    ${atipConfig}=  FilterPriorityLib.configureHeaderMask  ${maskChar}  ${maskName}  ${hdrLength}  ${hdrHeader}  ${hdrStartOffset}  ${enableTurboMode}
    createConfig  ${atipSession}  ${atipConfig}

    atipLogin.Login  ${atipSession}

    # get Header Mask ID
    ${hdrMaskId}=  Get Header Mask ID  ${maskName}

    deleteAllFilters  ${atipSession}
    resetStats  ${atipSession}

    [return]  ${hdrMaskId}

Get Header Mask ID
    [Arguments]  ${maskName}
    ${hdrMaskConfig}=  getHeaderMaskByName  ${atipSession}  ${maskName}
    ${hdrMask}=  evaluate  ${hdrMaskConfig}.get('masks')
    ${hdrMaskId}=  evaluate  ${hdrMask[0]}.get('id')

    [return]  ${hdrMaskId}

Get Netflow Collector ID
    [Arguments]  ${index}
    ${netflowCollectors}=  getCollectorConfig  ${atipSession}  ${httpStatusOK}
    Log  ${netflowCollectors}
    ${collectors}=  evaluate  ${netflowCollectors}.get('collectors')
    ${collectorConfig}=  evaluate  ${collectors}[${index}]
    ${collectorId}=  evaluate  ${collectorConfig}.get('id')

    [return]  ${collectorId}

Get Netflow Collector IP
    [Arguments]  ${index}
    ${netflowCollectors}=  getCollectorConfig  ${atipSession}  ${httpStatusOK}
    Log  ${netflowCollectors}
    ${collectors}=  evaluate  ${netflowCollectors}.get('collectors')
    ${collectorConfig}=  evaluate  ${collectors}[${index}]
    ${collectorIp}=  evaluate  ${collectorConfig}.get('collector')

    [return]  ${collectorIp}

Validate Apps in Netflow Capture
    [Arguments]  ${ipCollector1}  ${ipCollector2}  ${STRING}  ${app1}  ${app2}  ${app3}  ${geo}

    ${records1} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${ipCollector1}
    ${records2} =  getNetflowStatValue  ${atipSession}  ${netflowRecords}  ${ipCollector2}

    ${ixFlowFilters} =  Set variable  "cflow.pie.ixia.l7-application-name,cflow.pie.ixia.source-ip-country-name"

    ${response}=  Execute Command  python parsing.py ${STRING}_netflow.pcap ${STRING}_netflow.csv ${ixFlowFilters}
    ${dict}=  parseOutput  ${response}
    ${value1}=  Get From Dictionary  ${dict}  ${app1}
    ${value2}=  Get From Dictionary  ${dict}  ${app2}
    ${value3}=  Get From Dictionary  ${dict}  ${app3}
    ${value4}=  Get From Dictionary  ${dict}  ${geo}
    ${total}=  evaluate  ${value1}+${value2}+${value3}
    ${totalRecords}=  evaluate  ${records1}+${records2}
    should be equal as integers  ${value4}  ${total}
    should be equal as integers  ${value4}  ${totalRecords}

Validate Capture
    [Arguments]  ${app}  ${filteringType}  ${vlanId}  ${STRING}  ${statsType}  ${outFilename}

    # get top apps
    @{appStats}=  getAppTopStats  ${atipSession}  ${app}  ${statsType}  ${STRING}  ${vlanId}  ${filteringType}  ${outFilename}

    Execute Command  ${appStats[1]}  strip_prompt=True

    matches validation  ${outFilename}  ${appStats[0]}


Validate No Traffic In Capture
    [Arguments]  ${STRING}  ${vlanId}  ${outFilename}

    ${filteringCommand} =  filterCaptureNoTraffic  ${STRING}  ${vlanId}  ${outFilename}
    Execute Command  ${filteringCommand}
    matches validation  ${outFilename}  0

Matches Validation
    [Arguments]  ${STRING}  ${expectedValue}
    sleep  2
    ${output}=  Execute Command  wc -l < ${STRING}.result
    ${statsCaptureLB}=  Extract Result  ${output}  0  !
    Should Be Equal As Integers  ${statsCaptureLB}  ${expectedValue}

Extract Result
    [Arguments]  ${STRING}  ${lineNo}  ${controlWord}
    ${line}=  Get Line  ${STRING}  ${lineNo}
    ${output}=  Fetch From Left  ${line}  ${controlWord}
    Convert To Integer  ${output}
    [return]  ${output}

Stop Capture
    [Arguments]  ${index}
    Switch Connection  ${index}
    Write Control Character     BRK
    Read Until Prompt
    Close Connection

Connect on Linux Box
    ${openTelnet}=  Open Connection  ${envVars.linuxConnectionConfig.host}
    Telnet.Login  ${envVars.linuxConnectionConfig.username}  ${envVars.linuxConnectionConfig.password}
    Read
    [return]  ${openTelnet}

Become SU
    Write  sudo su
    sleep  1s
    Write  ${envVars.linuxConnectionConfig.password}
    Set prompt  \#
    Read Until Prompt

Configure ATIP
    atipLogin.Login  ${atipSession}

    # Clear ATIP config
    Run Keyword And Return Status  clearSystemAndWaitUntilNPReady  ${atipSession}  ${envVars}

#    Connect on Linux Box
#    # create temp folder for testing captures
#    Write  mkdir captures
#    ${command} =  copyFileToLinux
#    Write  ${command}
#    Close Connection

Clear ATIP
    #deleteHeaderMasks  ${atipSession}
    #deleteAllFilters  ${atipSession}
    #deleteCaptureFiles  ${atipSession}

    #Connect on Linux Box
    #Execute Command  rm -rf captures

    atipLogout.Logout  ${atipSession}