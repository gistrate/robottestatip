#
#
#
#       THIS IS A USAGE EXAMPLE OF SOME FEATURES OF THE FRAMEWORK
#
#
#
#
#
#






from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage

from atipAutoFrwk.webApi.WebApiSession import WebApiSession




# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
bpsSession = WebApiSession(env.bpsConnectionConfig)

# # Configure ATIP
# for i in range(0,5):
#     Login.login(atipSession)
#     SystemService.clearSystemAndWaitUntilNPReady(atipSession,env)
#     print(Status.getStatus(atipSession, HTTPStatus.OK))
#     Logout.logout(atipSession)
# SystemService.clearSystemAndWaitUntilNPReady(atipSession,env)
# Login.defaultLoginChangePassword(atipSession, env.atipDefaultPassword, env.atipConnectionConfig.password, env.atipConnectionConfig.username)

# print(["%04d" % x for x in range(1025)])

DebugPage.getSSLStatsFromDebugPage(atipSession)