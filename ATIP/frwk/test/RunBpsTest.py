
from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.RealTimeStats import RealTimeStats
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.services.atip.LogoutService import LogoutService
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.stats.TopFiltersService import TopFiltersService
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.webApi.atip.TopFilters import TopFilters
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations
from atipAutoFrwk.webApi.atip.Configuration import Configuration
from atipAutoFrwk.webApi.atip.Filters import Filters
from http import HTTPStatus
from atipAutoFrwk.webApi.atip.Logout import Logout

########## Test Suite Setup ##########
# Define test config
# Define filters
facebookFilter = FilterConfig("Facebook_Test")
FilterConditionConfig.addAppCondition(facebookFilter, ApplicationType.FACEBOOK)
allMatchingFilter = FilterConfig("allMatchingFilter")
FilterConditionConfig.addAppCondition(allMatchingFilter, [ApplicationType.NETFLIX, ApplicationType.FACEBOOK, ApplicationType.AOL])
FilterConditionConfig.addGeoCondition(allMatchingFilter, GeoLocation.CHINA)
tcpFilter = FilterConfig("TCP_Test")
FilterConditionConfig.addProtocolCondition(tcpFilter, NetworkProtocol.TCP)
# Define ATIP config
atipConfig = AtipConfig()
atipConfig.addFilters([facebookFilter, allMatchingFilter, tcpFilter])
# Define bps config
bpsTestConfig = BpsTestConfig('china_netflix_facebook_aol_15sec', 15)

# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
bpsSession = WebApiSession(env.bpsConnectionConfig)

# Configure ATIP
Login.login(atipSession)

Filters.deleteAllFilters(atipSession, HTTPStatus.OK)

AtipConfigService.createConfig(atipSession, atipConfig)
# Configure BPS
BpsLogin.login(bpsSession)
PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)

########## Test Setup ##########
# Clear stats on ATIP
System.resetStats(atipSession)
# Send traffic
testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup, bpsTestConfig.testDuration)
########## Test ##########
checkFilterExists = lambda : facebookFilter.name in str(TopFilters.getTopFiltersStats(atipSession))
TestCondition.waitUntilTrue(checkFilterExists, 'Couldn\'t find filter: {}'.format(tcpFilter.name),
                             totalTime=StatsTimeout.TOP_FILTERS.waitTime, iterationTime=StatsTimeout.ITERATION_TIME.waitTime)  # @UndefinedVariable

bpsTotalSessions = BpsStatsService.getRealTimeStatsByName(bpsSession, testId, RealTimeStats.APP_SUCCESSFUL)
allMatchingTotalSessions = TopFiltersService.checkTopFiltersStats(atipSession, TopStats.TOTAL_COUNT, allMatchingFilter.name, bpsTotalSessions)
tcpAtipTotalSessions = TopFiltersService.checkTopFiltersStats(atipSession, TopStats.TOTAL_COUNT, tcpFilter.name, bpsTotalSessions)

print('allMatchingFilter total sessions count: {}; expected: {}'.format(allMatchingTotalSessions, bpsTotalSessions))
print('tcpFilter total sessions count: {}; expected: {}'.format(tcpAtipTotalSessions, bpsTotalSessions))

Configuration.exportConfig(expectedStatus=HTTPStatus.OK, webApiSession=atipSession, host=env.atipConnectionConfig.host)
Configuration.clearSystem(atipSession, HTTPStatus.INTERNAL_SERVER_ERROR)

LogoutService.waitUntilLoggedOut(atipSession)

# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
# Changing credentials after the reset
Login.defaultLoginChangePassword(atipSession, 'admin', 'admin1234', 'admin', HTTPStatus.OK)
Filters.checkNoFilters(atipSession, HTTPStatus.OK)

Configuration.importConfig(atipSession, HTTPStatus.OK, host=env.atipConnectionConfig.host)
# Done importing and logging back in
# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
Login.login(atipSession)
SystemService.waitUntilSystemReady(atipSession)

# Send traffic
testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup, bpsTestConfig.testDuration)
########## Test ##########
checkFilterExists = lambda : facebookFilter.name in str(TopFilters.getTopFiltersStats(atipSession))
TestCondition.waitUntilTrue(checkFilterExists, 'Couldn\'t find filter: {}'.format(tcpFilter.name),
                             totalTime=StatsTimeout.TOP_FILTERS.waitTime, iterationTime=StatsTimeout.ITERATION_TIME.waitTime)  # @UndefinedVariable

bpsTotalSessions = BpsStatsService.getRealTimeStatsByName(bpsSession, testId, RealTimeStats.APP_SUCCESSFUL)
allMatchingTotalSessions = TopFiltersService.checkTopFiltersStats(atipSession, TopStats.TOTAL_COUNT, allMatchingFilter.name, bpsTotalSessions)
tcpAtipTotalSessions = TopFiltersService.checkTopFiltersStats(atipSession, TopStats.TOTAL_COUNT, tcpFilter.name, bpsTotalSessions)


# The check below will fail
#atipTotalSessions = TopFiltersService.checkTopFiltersStats(atipSession, TopStats.TOTAL_COUNT, facebookFilter.name, bpsTotalSessions)

########## Test Suite Teardown ##########
Logout.logout(atipSession)
