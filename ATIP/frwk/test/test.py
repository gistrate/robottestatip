#
#
#
#       THIS IS A USAGE EXAMPLE OF SOME FEATURES OF THE FRAMEWORK
#
#
#
#
#
#




from http import HTTPStatus
from pprint import pprint
import jsonpickle

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.Collector import Collector
from atipAutoFrwk.config.atip.HeaderMask import HeaderMask
from atipAutoFrwk.config.atip.MacRewrite import MacRewrite
from atipAutoFrwk.config.atip.MplsParsingConfig import MplsParsingConfig
from atipAutoFrwk.config.atip.NetflowCollectorConfig import NetflowCollectorConfig
from atipAutoFrwk.config.atip.PayloadMask import PayloadMask
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig
from atipAutoFrwk.data.atip.HeaderType import HeaderType
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.NetflowService import NetflowService
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.MacRewrites import MacRewrites
from atipAutoFrwk.webApi.atip.DataMasking import DataMasking
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.MacRewrites import MacRewrites
from atipAutoFrwk.webApi.atip.Netflow import Netflow
from atipAutoFrwk.webApi.atip.PayloadMasks import PayloadMasks
from atipAutoFrwk.webApi.atip.Status import Status
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.services.atip.GroupsService import GroupsService
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.webApi.atip.Groups import Groups
from atipAutoFrwk.config.atip.filter.FilterConditionConfig import FilterConditionConfig
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType

# Define ATIP config
atipConfig = AtipConfig()


# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
#bpsSession = WebApiSession(env.bpsConnectionConfig)

# Configure ATIP
Login.login(atipSession)

#Filters.deleteAllFilters(atipSession, HTTPStatus.OK)

AtipConfigService.createConfig(atipSession, atipConfig)
# Configure BPS

System.resetStats(atipSession)
# Send traffic

# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
# Changing credentials after the reset

env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
Login.login(atipSession)
SystemService.waitUntilSystemReady(atipSession)
print('*******************************************')
print(Status.getStatus(atipSession, HTTPStatus.OK))
###Netflow stuff now

### CAREFUL! MULTIPLE RUNS ON SAME IP WILL RETURN SERVER ERROR (unicity of IP/Ports combination)
#print("What I got:")
### Global settings stuff


#

# def testGlobalConfig():
#
#     print("1. Global config:")
#     netflowConfig = Netflow.getGlobalConfig(webApiSession=atipSession)
#     print(vars(netflowConfig))
#
#     netflowConfig.enabled = True
#     netflowConfig.timeout = 33
#     netflowConfig.version = 10
#     Netflow.updateGlobalConfig(atipSession, netflowConfig, HTTPStatus.OK)
#
#
# def testCollectorConfig():
#     print("2. Collector config:")
#     ### Collector config test
#     theCollectorConfig = NetflowService.getCollectorConfig(atipSession, HTTPStatus.OK)
#
#     theCollectorConfig.printConfig()
#     # Uncomment the following lines to add the collector:
#     myCollector = Collector(None, 101, 'Kbps', True, 'UDP', 4742, '124.3.3.18')
#     print("The first available is: " + str(NetflowCollectorConfig.FIRST_AVAILABLE))
#     Netflow.addCollector(atipSession, myCollector, HTTPStatus.OK)
#     Netflow.updateCollectorConfig(atipSession, theCollectorConfig, HTTPStatus.OK, 1, key='rate', value=105)
#
# def testCardConfig():
#     print("3.1 Card config JSON:")
#     hey = Netflow.getCardConfig(webApiSession=atipSession, expectedStatus=HTTPStatus.OK)
#     print("3.2 Card config object:")
#     theCardConfig = NetflowService.getCardConfig(atipSession, HTTPStatus.OK)
#     theCardConfig.printConfig()
#
#     theCardConfig.cardList[3].enabled = True
#     theCardConfig.cardList[3].odid = 2
#     theCardConfig.cardList[3].ipMethod = 'DHCP'
#
#     theCardConfig.cardList[4].enabled = True
#     theCardConfig.cardList[4].odid = 2
#     theCardConfig.cardList[4].ipMethod = 'DHCP'
#
#     Netflow.updateCard(atipSession, theCardConfig, 3, 'odid', 4, HTTPStatus.OK)
#     Netflow.updateCardConfig(atipSession, theCardConfig)
#     theCardConfig2 = NetflowService.getCardConfig(atipSession, HTTPStatus.OK)
#     for i in range(theCardConfig2.MAXIMUM_COLLECTORS):
#         if (vars(theCardConfig.cardList[i]) != vars(theCardConfig2.cardList[i])):
#             print('Different at card number %s' % i)

######## NP Groups ########

def testNPGroups():
    # ATIP config
    atipConfig = AtipConfig()
    
    # Define 2 groups and add them to the config
    g1 = GroupConfig(name='Test1')
    g2 = GroupConfig(name='Test2')
    atipConfig.addGroup([g1, g2])
    
    AtipConfigService.createConfig(atipSession, atipConfig)
    
    # Move NP2 to g2(Test2)
    Groups.changeNPGroup(atipSession, 2, g2.id)
    
    # Wait until Group2 finishes to init
    GroupsService.waitUntilGroupReady(atipSession, g2.id)
    
def testNPGroupsAtipCfg():
    # ATIP config
    atipConfig = AtipConfig()
    
    # Define 2 groups and add them to the config
    g1 = GroupConfig(name='Test1')
    g2 = GroupConfig(name='Test2')
    atipConfig.addGroup([g1, g2])
    
    filter1 = FilterConfig("test_filter")
    filter1.forward = True
    filter1.groupId = g2
    FilterConditionConfig.addAppCondition(filter1, ApplicationType.AOL)

    atipConfig.addFilters(filter1)

    
    AtipConfigService.createConfig(atipSession, atipConfig)

######## Data masking ########


def testDataMasking():

    print('################')
    # #t = Filters.getAllFilters(atipSession, expectedStatus=HTTPStatus.OK).get('rule')
    # filterList = [{"isExplore":False,"deny":False,"hdrMaskUUIDS":'l3vpn',"forward":True,"priority":1,"decrypted":False,"netflow":False,"encrypted":False,"lastUpdate":1507792931531,"name":"filter1","dropTraffic":False,"id":100003,"createDate":1507792931531},{"isExplore":False,"deny":False,"hdrMaskUUIDS":['l3vpn','l2'],"forward":True,"priority":2,"decrypted":False,"netflow":False,"encrypted":False,"lastUpdate":1507820846858,"name":"temp2","dropTraffic":False,"id":100014,"createDate":1507820846858}]
    # print('filterList : {}'.format(filterList))
    # path = 'headerMasks/'
    # headerMaskNameToId = {}
    # headerMasks = DataMasking.getJSON(atipSession, path).get('masks')
    # print('headerMasks : {}'.format(headerMasks))
    #
    # for i in range(len(headerMasks)):
    #     headerMaskNameToId[headerMasks[i]['name']] = headerMasks[i]['id']
    #
    # print('headerMaskNameToId : {}'.format(headerMaskNameToId))
    #
    # for i in filterList:
    #     if isinstance(i['hdrMaskUUIDS'], list):
    #         for j in range(len(i['hdrMaskUUIDS'])):
    #             if i['hdrMaskUUIDS'][j] in headerMaskNameToId:
    #                 i['hdrMaskUUIDS'][j] = headerMaskNameToId[i['hdrMaskUUIDS'][j]]
    #     else:
    #         i['hdrMaskUUIDS'] = headerMaskNameToId[i['hdrMaskUUIDS']]
    #
    # print('Updated filterList : {}'.format(filterList))

    atipConfig1 = AtipConfig()
    atipConfig1.dataMasking[0].maskChar = "C"
    atipConfig1.dataMasking[0].enabled = True

    headerMask1 = HeaderMask()
    headerMask1.header = 'L2_VLAN_MPLS'
    headerMask1.maskLength = '4'
    headerMask1.startOffset = '8'
    headerMask1.name = 'l2'

    headerMask2 = HeaderMask()
    headerMask2.header = 'L2_VLAN'
    headerMask2.maskLength = '4'
    headerMask2.startOffset = '8'
    headerMask2.name = 'l3vpn'

    atipConfig1.headerMasks.headerMaskList[headerMask1.name] = headerMask1
    atipConfig1.headerMasks.headerMaskList[headerMask2.name] = headerMask2


    # AtipConfigService.createDataMasking(atipSession, atipConfig1)
    # AtipConfigService.createHeaderMasks(atipSession, atipConfig1)
    #

    filter1 = FilterConfig("l3vpn")
    filter1.forward = True
    filter1.hdrMaskUUIDS = [headerMask1.name, headerMask2.name]
    #filter1.hdrMaskUUIDS = headerMask1.name
    print('filter1 : {}'.format(filter1.hdrMaskUUIDS))

    atipConfig1.addFilters(filter1)


    filter2 = FilterConfig("l2mac")
    filter2.forward = True
    filter2.hdrMaskUUIDS = headerMask1.name
    print('filter2 : {}'.format(filter2.hdrMaskUUIDS))
    atipConfig1.addFilters(filter2)
    #AtipConfigService.updateFiltersConfig(atipSession, atipConfig1)
    AtipConfigService.createConfig(atipSession, atipConfig1)
    DataMasking.deleteHeaderMasks(atipSession)


    #print('atipConfig : {}'.format(atipConfig1.__dict__))
    #print('atipConfig : {}'.format(atipConfig1.__dict__['filterList']))
    #AtipConfigService.updateFiltersConfig(atipSession, atipConfig1)
    #for i in range(len(atipConfig1.__dict__['filterList'])):
    #    print('sdvfsd : {}'.format(atipConfig1.__dict__['filterList'][i].hdrMaskUUIDS))
    #print('atipConfig1 : {}'.format(atipConfig1.__dict__['filterList'][0].hdrMaskUUIDS))
    #print('atipConfig1 : {}'.format(atipConfig1.__dict__['filterList'][1].hdrMaskUUIDS))
    #print('atipConfig : {}'.format(atipConfig1.__dict__['filterList']))

    #for i in range(len(atipConfig1.__dict__['filterList'])):
    #    print('sdvfsd : {}'.format(atipConfig1.__dict__['filterList'][i]))
    #print('atipConfig : {}'.format(dir(atipConfig1.__dict__['filterList'])))
    #print('atipConfig : {}'.format(vars(atipConfig1.__dict__['filterList'])))
    #print('atipConfig : {}'.format(atipConfig1.__dict__['filterList'].hdrMaskUUIDS for
    # AtipConfigService.updateFiltersConfig(atipSession, f

    # filterC = FilterConfig('l3vpn')
    # print(filterC)
    # print('Filter blank : {}'.format(filterC.__dict__))
    # #print('Filter stripped : {}'.format(filterC.__dict__['hdrMaskUUIDS']))
    # filterC.hdrMaskUUIDS = ['l3vpn', ['l3vpn', 'l2']]
    # print('Filter with names : {}'.format(filterC.__dict__))
    # print('Filter with UUIDS: {}'.format(filterC.__dict__['hdrMaskUUIDS']))
    # AtipConfigService.updateFiltersConfig(atipSession, filterC)
    # print('Updated Filter : {}'.format(filterC.__dict__))
    # AtipConfig.addFilters(atipConfig, filterC)
    # print('Done creating ATIP config: {}'.format(jsonpickle.encode(atipConfig.__dict__['filterList'])))
    #print('atipConfig : {}'.format(atipConfig.__dict__['filterList']))


def main():
    #pass
    #testGlobalConfig()
    #testCollectorConfig()
    #testCardConfig()
    #testDataMasking()
    #testNPGroups()
    testNPGroupsAtipCfg()

if(__name__ == '__main__'):
    main()

Logout.logout(atipSession)
