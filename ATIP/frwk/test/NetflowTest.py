#
#
#
#       THIS IS A USAGE EXAMPLE OF SOME FEATURES OF THE FRAMEWORK
#
#
#
#
#
#




from http import HTTPStatus
from pprint import pprint

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.config.atip.Collector import Collector
from atipAutoFrwk.config.atip.HeaderMask import HeaderMask
from atipAutoFrwk.config.atip.MacRewrite import MacRewrite
from atipAutoFrwk.config.atip.NetflowCollectorConfig import NetflowCollectorConfig
from atipAutoFrwk.config.atip.PayloadMask import PayloadMask
from atipAutoFrwk.data.atip.HeaderType import HeaderType
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.services.atip.NetflowService import NetflowService
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip import MacRewrites
from atipAutoFrwk.webApi.atip.DataMasking import DataMasking
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Logout import Logout
from atipAutoFrwk.webApi.atip.MacRewrites import MacRewrites
from atipAutoFrwk.webApi.atip.Netflow import Netflow
from atipAutoFrwk.webApi.atip.PayloadMasks import PayloadMasks
from atipAutoFrwk.webApi.atip.Status import Status
from atipAutoFrwk.webApi.atip.System import System

# Define ATIP config
atipConfig = AtipConfig()


# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
#bpsSession = WebApiSession(env.bpsConnectionConfig)

# Configure ATIP
Login.login(atipSession)

#Filters.deleteAllFilters(atipSession, HTTPStatus.OK)

AtipConfigService.createConfig(atipSession, atipConfig)
# Configure BPS

System.resetStats(atipSession)
# Send traffic

# Get environment config
env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
# Changing credentials after the reset

env = Environment()
# Create webApi sessions
atipSession = WebApiSession(env.atipConnectionConfig)
Login.login(atipSession)
SystemService.waitUntilSystemReady(atipSession)
print(Status.getStatus(atipSession, HTTPStatus.OK))
###Netflow stuff now

### CAREFUL! MULTIPLE RUNS ON SAME IP WILL RETURN SERVER ERROR (unicity of IP/Ports combination)
#print("What I got:")
### Global settings stuff


#

def testGlobalConfig():

    print("1. Global config:")
    netflowConfig = Netflow.getGlobalConfig(webApiSession=atipSession)
    print(vars(netflowConfig))

    netflowConfig.enabled = True
    netflowConfig.timeout = 33
    netflowConfig.version = 10
    Netflow.updateGlobalConfig(atipSession, netflowConfig, HTTPStatus.OK)


def testCollectorConfig():
    print("2. Collector config:")
    ### Collector config test
    theCollectorConfig = NetflowService.getCollectorConfig(atipSession, HTTPStatus.OK)

    theCollectorConfig.printConfig()
    # Uncomment the following lines to add the collector:
    myCollector = Collector(None, 101, 'Kbps', True, 'UDP', 4742, '124.3.3.18')
    print("The first available is: " + str(NetflowCollectorConfig.FIRST_AVAILABLE))
    Netflow.addCollector(atipSession, myCollector, HTTPStatus.OK)
    Netflow.updateCollectorConfig(atipSession, theCollectorConfig, HTTPStatus.OK, 1, key='rate', value=105)

def testCardConfig():
    print("3.1 Card config JSON:")
    hey = Netflow.getCardConfig(webApiSession=atipSession, expectedStatus=HTTPStatus.OK)
    print("3.2 Card config object:")
    theCardConfig = NetflowService.getCardConfig(atipSession, HTTPStatus.OK)
    theCardConfig.printConfig()

    theCardConfig.cardList[3].enabled = True
    theCardConfig.cardList[3].odid = 2
    theCardConfig.cardList[3].ipMethod = 'DHCP'

    theCardConfig.cardList[4].enabled = True
    theCardConfig.cardList[4].odid = 2
    theCardConfig.cardList[4].ipMethod = 'DHCP'

    Netflow.updateCard(atipSession, theCardConfig, 3, 'odid', 4, HTTPStatus.OK)
    Netflow.updateCardConfig(atipSession, theCardConfig)
    theCardConfig2 = NetflowService.getCardConfig(atipSession, HTTPStatus.OK)
    for i in range(theCardConfig2.MAXIMUM_COLLECTORS):
        if (vars(theCardConfig.cardList[i]) != vars(theCardConfig2.cardList[i])):
            print('Different at card number %s' % i)

######## Data masking ########


def testDataMasking():
    print("4.1 Data masking:")
    theOutput = DataMasking.getDataMasking(atipSession)

    dmConf = DataMasking.getDataMaskingConfig(atipSession)
    dmConf.enabled = True

    DataMasking.updateDataMasking(atipSession, dmConf)

    print("4.2. Header Masks:")
    theHeaderMaskConfig = DataMasking.getHeaderMasks(atipSession)
    print(type(theHeaderMaskConfig))
    myHeader = HeaderMask(2, 0, "QQA", HeaderType.L2_VLAN, 11)
    theHeaderMaskConfig = DataMasking.addHeader(atipSession, myHeader)


    DataMasking.updateHeaderConfig(atipSession, 'QQA', 'header', HeaderType.L4_PAYLOAD)
    theHeaderMaskConfig = DataMasking.deleteHeaderConfigByName(atipSession, "QQA")

    print("4.3. Payload Masks:")
    thePayloadMaskConfig = PayloadMasks.getPayloadMasks(atipSession)

    print("Initially, the list is: ")
    thePayloadMaskConfig.printConfig()
    myPayload = PayloadMask("4[0-7]{13}", 0, 0, "Cardul meu", 0, True, -1, True)
    thePayloadMaskConfig = PayloadMasks.addPayload(atipSession, myPayload)
    thePayloadMaskConfig = PayloadMasks.updatePayloadConfig(atipSession, 'Cardul meu', 'offsetBegin', 2)
    thePayloadMaskConfig = PayloadMasks.deletePayloadConfigByName(atipSession, "Cardul meu")

    print("4.4. Mac Rewrite:")
    theMacConfig = MacRewrites.getMacRewritesSettings(atipSession)
    print(theMacConfig)
    theMacConfig = MacRewrites.updateMacRewriteSettings(atipSession, "enable")
    theMacItems = MacRewrites.getMacRewritesItems(atipSession)
    myMac = MacRewrite(regex ="[\\x00-\\xFF][\\x00-\\xFF][\\x00-\\xFF][\\x00-\\xFF][\\x00-\\xFF][\\x00-\\xFE]", field="src", rpt_count=0, name="gigel", id=-1, replacement="021AC5010003")
    theMacItems = MacRewrites.addMac(atipSession, myMac)
    print(theMacItems)
    MacRewrites.updateMacItem(atipSession, "gigel", "field", "dst")
    MacRewrites.deleteMacConfigByName(atipSession, "gigel")
    theMacConfig = MacRewrites.getMacRewritesSettings(atipSession)
    print(theMacConfig)

def main():
    pass
    #testGlobalConfig()
    #testCollectorConfig()
    #testCardConfig()
    #testDataMasking()

if(__name__ == '__main__'):
    main()

Logout.logout(atipSession)
