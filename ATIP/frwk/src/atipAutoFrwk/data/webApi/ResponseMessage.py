class ResponseMessage(object):

    LOGIN_SUCCESSFUL = '{"success":true}'
    EXCEPTION_EXPECTED_200_ACTUAL_500 = 'RequestException: Expected status code 200. Actual status code 500.'
    INGRESS_PACKET_BUFFER_EXPECTED_ERROR_FOR_TOO_LOW = 'Response text: {\"type\":\"error\",\"messages\":[\"Ingress Packet Buffer cannot be smaller than 4000\"]}'
    INGRESS_PACKET_BUFFER_EXPECTED_ERROR_FOR_TOO_HIGH = 'Response text: {\"type\":\"error\",\"messages\":[\"Ingress Packet Buffer cannot be bigger than 32000\"]}'