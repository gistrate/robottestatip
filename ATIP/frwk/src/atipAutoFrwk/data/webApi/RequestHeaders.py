class RequestHeaders(object):

    CONTENT_TYPE = 'content-type'
    CONTENT_TYPE_JSON = 'application/json'
    CONTENT_TYPE_URLENCODED = 'application/x-www-form-urlencoded; charset=UTF-8'
    CONTENT_TYPE_XML = 'application/xml'