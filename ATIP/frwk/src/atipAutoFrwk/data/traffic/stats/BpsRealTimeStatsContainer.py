import jsonpickle


class BpsRealTimeStatsContainer(object):
    '''
    Contains the BPS stats for Real Time Statistics.
    '''

    def __init__(self, values):
        '''
        Expects a list of BpsRealTimeStatsItem objects.
        '''
        self.time = None
        self.progress = None
        self.values = values

    def getList(self):
        return self.values

    def __str__(self):
        return "Time: {}\n Progress: {}\n Stats: {}".format(self.time, self.progress, self.getList())
