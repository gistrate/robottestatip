import jsonpickle


class BpsRealTimeStatsItem(object):
    '''
    Item from the bps real time stats list.
    '''

    def __init__(self):
        self.ethTxFrames = None
        self.ethTxFrameRate = None
        self.ethTxFrameDataRate = None
        self.ethRxFrames = None
        self.ethRxFrameRate = None
        self.ethRxFrameDataRate = None
        self.tcpFlowsConcurrent = None
        self.udpFlowsConcurrent = None
        self.totalFlowsConcurrent = None
        self.superFlowsConcurrent = None
        self.sctpFlowsConcurrent = None
        self.concurrentAppFlows = None
        self.appAttempted = None
        self.appSuccessful = None
        self.appUnsuccessful = None
        self.appIncomplete = None
        self.appAttemptedRate = None
        self.appSuccessfulRate = None
        self.appUnsuccessfulRate = None
        self.txFrames = None
        self.rxFrames = None
        self.tcpAvgResponseTime = None
        self.tcpClientEstablished = None
        self.tcpClientEstablishRate = None
        self.tcpClientClosed = None
        self.tcpClientClosedByReset = None
        self.tcpClosedByReset = None
        self.tcpClientCloseRate = None
        self.tcpAttempted = None
        self.tcpAttemptRate = None
        self.tcpServerEstablished = None
        self.tcpServerEstablishRate = None
        self.tcpServerClosed = None
        self.tcpServerClosedByReset = None
        self.tcpServerCloseRate = None
        self.tcpAvgSetupTime = None
        self.tcpAvgCloseTime = None
        self.tcpAvgSessionDuration = None

    def __str__(self):
        return jsonpickle.encode(self)

