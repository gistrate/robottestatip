from enum import Enum

class BpsStatsType(Enum):
    SESSIONS = 'appSuccessful'
    PACKETS = 'appTxFrames'
    BYTES = 'appTxFrameData'

    def __init__(self, statsName):
        self.statsName = statsName
