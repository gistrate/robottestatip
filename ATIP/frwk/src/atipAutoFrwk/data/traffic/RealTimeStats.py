from enum import Enum


class RealTimeStats(Enum):
    # example for how to set a custom maxDiff value
    APP_SUCCESSFUL = ('appSuccessful', 0.01)
    APP_ATTEMPTED = 'appAttempted'
    ETH_TX_FRAMES = 'ethTxFrames'
    ETH_RX_FRAMES = 'ethRxFrames'
    APP_TX_FRAME_DATA = 'appTxFrameData'

    def __init__(self, statType, maxDiff=0.01):
        '''
        Keyword arguments:
        statType -- the name of the stat as it's returned from BPS
        maxDiff -- the maximum allowed difference between the actual and expected value (see rel_tol argument description from isclose method)  
        '''
        self.statType = statType
        self.maxDiff = maxDiff
        