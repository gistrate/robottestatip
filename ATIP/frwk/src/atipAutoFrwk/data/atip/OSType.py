from enum import Enum


class OSType(Enum):

    Linux = 'Linux'
    MacOS = 'MacOS'
    Nokia = 'Nokia'
   
    def __init__(self, osName):
        self.osName = osName

    @classmethod
    def toDict(cls, osType):
        ':type osType: OSType'

        osDict = {
                "id" : osType.value
            }
        return osDict
