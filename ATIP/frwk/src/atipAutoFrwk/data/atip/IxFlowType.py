from enum import Enum


class IxFlowType(Enum):
    clientIPCityName = ('netflowL7SrcCityName', 'cflow.pie.ixia.source-ip-city-name', 'srcCityName')
    clientIPCountryCode = ('netflowL7SrcCountryCode', 'cflow.pie.ixia.source-ip-country-code', 'srcCountryCode')
    clientIPCountryName = ('netflowL7SrcCountryName', 'cflow.pie.ixia.source-ip-country-name', 'srcCountryName')
    clientLatitude = ('netflowL7SrcLatitude', 'cflow.pie.ixia.source-ip-latitude', 'srcLatitude')
    clientLongitude = ('netflowL7SrcLongitude', 'cflow.pie.ixia.source-ip-longitude', 'srcLongitude')
    clientIPRegionCode = ('netflowL7SrcRegionCode', 'cflow.pie.ixia.source-ip-region-code', 'srcRegionCode')
    clientIPRegionName = ('netflowL7SrcRegionName', 'cflow.pie.ixia.source-ip-region-name', 'srcRegionName')
    serverIPCityName = ('netflowL7DstCityName', 'cflow.pie.ixia.destination-ip-city-name', 'dstCityName')
    serverIPCountryCode = ('netflowL7DstCountryCode', 'cflow.pie.ixia.destination-ip-country-code', 'dstCountryCode')
    serverIPCountryName = ('netflowL7DstCountryName', 'cflow.pie.ixia.destination-ip-country-name', 'dstCountryName')
    serverLatitude = ('netflowL7DstLatitude', 'cflow.pie.ixia.destination-ip-latitude', 'dstLatitude')
    serverLongitude = ('netflowL7DstLongitude', 'cflow.pie.ixia.destination-ip-longitude', 'dstLongitude')
    serverIPRegionCode = ('netflowL7DstRegionCode', 'cflow.pie.ixia.destination-ip-region-code', 'dstRegionCode')
    serverIPRegionName = ('netflowL7DstRegionName', 'cflow.pie.ixia.destination-ip-region-name', 'dstRegionName')
    applicationId =('netflowL7AppID', 'cflow.pie.ixia.l7-application-id', 'l7appID' )
    applicationName = ('netflowL7AppName', 'cflow.pie.ixia.l7-application-name', 'l7appName')
    browserName =('netflowL7BrowserName', 'cflow.pie.ixia.browser-name', 'browserName')
    browserId = ('netflowL7BrowserId', 'cflow.pie.ixia.browser-id', 'browserId')
    deviceId = ('netflowL7DeviceId', 'cflow.pie.ixia.os-device-id', 'deviceId')
    deviceName = ('netflowL7DeviceName', 'cflow.pie.ixia.os-device-name', 'deviceName')
    connEncryptType = ('netflowL7ConnEncryptType', 'cflow.pie.ixia.conn-encryption-type', 'connEncryptType')
    encryptionCipher = ('netflowL7EncryptionCipher', 'cflow.pie.ixia.encryption-cipher', 'encryptionCipher')
    encryptionKeyLen = ('netflowL7EncryptionKeyLen', 'cflow.pie.ixia.encryption-keylen', 'encryptionKeyLen')
    dnsClass = ('netflowL7DNSClass', 'cflow.pie.ixia.dns-classes', '')
    dnsHostnameQuery = ('netflowL7DNSHostnameQuery', 'cflow.pie.ixia.dns-question-names', '')
    dnsHostnameResponse = ('netflowL7DNSHostnameResponse', 'cflow.pie.ixia.dns-question-names', '')
    dnsText = ('netflowL7DNSText', 'cflow.pie.ixia.dns-txt', 'dnsTxt')
    domain = ('netflowL7Domain', 'cflow.pie.ixia.hostname', 'httpHostName')
    uri = ('netflowL7URI', 'cflow.pie.ixia.http-uri', 'uri')
    userAgent = ('netflowL7UserAgent', 'cflow.pie.ixia.http-user-agent', 'userAgent')

    def __init__(self, statType, filterType, infoElemType):
        '''
        Keyword arguments:
        statType -- the name of the stat as it's returned from MW
        filterType -- the name of the cflow filter associated to that specific stat
        infoElemType -- the name of the information element from the cflow packet
        '''
        self.statType= statType
        self.filterType = filterType
        self.infoElemType = infoElemType
























