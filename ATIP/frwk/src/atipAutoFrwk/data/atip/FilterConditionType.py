from enum import Enum


class FilterConditionType(Enum):

    GEO = ('geo', 'geos')
    APP = ('app', 'apps')
    RGX = ('regex', 'regex')
    PROTOCOL = ('protocols', 'protocols')
    PORT_RANGE = ('portRange', 'portRanges')
    IP_RANGE = ('ipAddressRange', 'ipAddressRanges')
    APPGROUP = ('tag','tags')

    def __init__(self, conditionType, listName):
        self.conditionType = conditionType
        self.listName = listName
        