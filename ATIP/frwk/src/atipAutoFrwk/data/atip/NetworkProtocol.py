from distutils.dist import warnings
from enum import Enum


class NetworkProtocol(Enum):
    TCP = ('tcp', 6)
    UDP = ('udp', 17)
    ESP_IPSEC = ('esp_ipsec', 50)
    HTTP = ('http', 0)
    HTTPS = ('https', 0)        

    def __init__(self, protocolName, protocolNumber):
        self.protocolName = protocolName
        self.protocolNumber = protocolNumber
        
    @classmethod
    def toDict(cls, networkProtocol):
        ':type networkProtocol: NetworkProtocol'

        if networkProtocol.protocolNumber == 0:
            warnings.warn('Protocol {} doesn\'t have a protocol number assigned!'.format(networkProtocol.protocolName))

        networkProtocolDict = {
                "protocol" : networkProtocol.protocolNumber
            }
        return networkProtocolDict
