from enum import Enum


class ApplicationType(Enum):

    AOL = 'aol'
    APPLE = 'apple'
    AMAZON = 'amazon'
    BBC = 'bbc'
    FACEBOOK = 'facebook'
    NETFLIX = 'netflix'
    LINKEDIN = 'linkedin'
    TECHCRUNCH = 'techcrunch.com'
    BUSINESSINSIDER = 'businessinsider.com'
    BOREDPANDA = 'boredpanda.com'
    TOPSECRET = ('topsecretsite.com', 'dynamic')
    HTTP_80 = 'http (tcp:80)'
    ESPN = 'espn'
    SKYSPORTS = 'sky-sports'
    GOPHER = 'gopher'
    VIMEOMOBILE = "vimeo-mobile"
    IXIACOM = 'ixiacom.com'
    HBO = 'hbo'
    DYNAMIC1 = 'mihaildynamic1.com'
    DYNAMIC2 = 'mihaildynamic2.com'
    DYNAMIC3 = 'mihaildynamic3.com'
    DYNAMIC4 = 'mihaildynamic4.com'
    MICROSOFT = 'microsoft.com'
    WINDOWSUPDATE = 'windowsupdate.com'
    OUTLOOKWEBACCESS = 'OutlookWebAccess'



    def __init__(self, appName, appType = "static", actions= []):
        self.appName = appName
        self.appType = appType


    @classmethod
    def toDict(cls, applicationType, actions=None):
        ':type applicationType: ApplicationType'

        appDict = {
                "id" : applicationType.value
            }
        if actions:
            appDict['actions'] = []
            for i in actions:
                appDict['actions'].append({"id": i})
        return appDict
