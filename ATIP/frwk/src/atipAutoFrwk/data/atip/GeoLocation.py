from enum import Enum


class GeoLocation(Enum):

    CHINA = ('CN', '', '', 'China')
    FRANCE = ('FR', '', '', 'France')
    GREECE = ('GR', '', '', 'Greece')
    JAPAN = ('JP', '', '', 'Japan')
    PAKISTAN = ('PK', '', '', 'Pakistan')
    ROMANIA = ('RO', '', '', 'Romania')
    UNITED_KINGDOM = ('GB', '', '', 'United Kingdom')
    UNITED_STATES = ('US', '', '', 'United States')
    CANADA = ('CA', '', '', 'Canada')
    AUSTRALIA = ('AU', '', '', 'Australia')
    MALAYSIA = ('MY', '', '', 'Malaysia')
    BOTSWANA = ('BW', '', '', 'Botswana')
    ITALY = ('IT', '', '', 'Italy')
    INDIA = ('IN', '', '', 'India')
    THAILAND = ('TH', '', '', 'Thailand')

    def __init__(self, countryCode, region, city, countryName):
        self.countryCode = countryCode
        self.region = region
        self.city = city
        self.countryName = countryName



    @classmethod
    def toDict(cls, geoLocation):
        ':type geoLocation: GeoLocation'

        geoDict = {
                "country" : geoLocation.countryCode,
                # removed "region" and "city" from this method because they caused some mismatch and result was not matching received data on China geo location
                # "countryName": geoLocation.countryName
            }
        return geoDict