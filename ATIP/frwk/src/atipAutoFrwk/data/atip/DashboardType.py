from enum import Enum


class DashboardType(Enum):

    MINIMIZED = 10
    MAXIMIZED = 50
    
    def __init__(self, statsLimit):
        self.statsLimit = statsLimit
        self.stringLimit = str(statsLimit)