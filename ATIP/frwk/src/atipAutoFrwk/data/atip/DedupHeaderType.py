from enum import Enum


class DedupHeaderType(Enum):

    NONE = 'NONE'
    MAC = 'MAC'
    MAC_VLAN = 'MAC_VLAN'
    MAC_VLAN_L3 = 'MAC_VLAN_L3'
    MAC_VLAN_L3_L4 = 'MAC_VLAN_L3_L4'
    MAC_VLAN_MPLS = 'MAC_VLAN_MPLS'
    MAC_VLAN_MPLS_L3 = 'MAC_VLAN_MPLS_L3'
    MAC_VLAN_MPLS_L3_L4 = 'MAC_VLAN_MPLS_L3_L4'

    def __init__(self, headerName):
        self.headerName = headerName
