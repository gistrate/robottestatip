from enum import Enum


class ServiceProvider(Enum):

    FACEBOOK = ('FACEBOOK - Facebook, Inc., US ', 32934)

    def __init__(self, ISPName, asNumber):
        self.ISPName = ISPName
        self.asNumber = asNumber

    @property
    def fullname(self):
        s = '{}(AS Number: {})'.format(self.ISPName, self.asNumber)
        return s
        
    @classmethod
    def toDict(cls, applicationType):
        ':type applicationType: ApplicationType'

        appDict = {
                "id" : applicationType.value
            }
        return appDict
