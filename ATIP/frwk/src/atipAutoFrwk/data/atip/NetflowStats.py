from enum import Enum


class NetflowStats(Enum):
    OPTION_TEMPLATES = 'option_templates_sent'
    OPTION_RECORDS = 'option_records_sent'
    DATA_TEMPLATES = 'data_templates_sent'
    DATA_RECORDS = 'data_records_sent'
    TOTAL_PKTS = 'total_pkts_sent'
    TOTAL_BYTES = 'total_bytes_sent'
    PKTS_RATE = 'pkts_per_second'
    BITS_RATE = 'bits_per_second'


    def __init__(self, statType):
        '''
        Keyword arguments:
        statType -- the name of the stat as it's returned from MW
        '''
        self.statType = statType
