class HeaderType(object):

    L2 = 'L2'
    L2_VLAN = 'L2_VLAN'
    L2_MPLS = 'L2_VLAN_MPLS'
    L3 = 'L3'
    L4 = 'L4'
    L4_PAYLOAD = 'RAW'