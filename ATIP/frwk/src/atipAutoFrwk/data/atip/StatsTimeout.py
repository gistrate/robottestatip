from enum import Enum


class StatsTimeout(Enum):
    '''
    Contains timeout values for checking the values on each stat type.
    Expects seconds.
    '''

    # TODO - see if different values are needed on different time intervals 
    TOP_FILTERS = 60
    
    # Interval at which to check the stats values again
    ITERATION_TIME = 5


    def __init__(self, waitTime):
        '''
        Keyword arguments:
        waitTime -- maximum time to wait for the actual value to match the expected value 
        '''
        self.waitTime = waitTime
        