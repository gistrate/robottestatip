class PortRange(object):

    def __init__(self, startIndex, stopIndex):
        self.startIndex = startIndex
        self.stopIndex = stopIndex
    
    def toDict(self):

        portRange = {
            'startIndex' : self.startIndex,
            'endIndex' : self.stopIndex
            }
        return portRange