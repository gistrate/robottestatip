from enum import Enum

class BuildType(Enum):
    RELEASE = 'releases'
    UNSTABLE = 'unstable'

    def __init__(self, buildType):
        '''
        Keyword arguments:
        buildType -- type of build file: "release" or "unstable"
        '''
        self.buildType = buildType