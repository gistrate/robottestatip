from enum import Enum


class BrowserType(Enum):

    SeaMonkey = 'SeaMonkey'
    BonEcho = 'BonEcho'
    Chrome = 'Chrome'
    BrowserNG = 'BrowserNG'

    def __init__(self, browserName):
        self.browserName = browserName

    @classmethod
    def toDict(cls, browserType):
        ':type browserType: BrowserType'

        browserDict = {
                "id" : browserType.value
            }
        return browserDict
