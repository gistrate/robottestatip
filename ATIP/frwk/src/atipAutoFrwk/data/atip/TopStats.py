from enum import Enum


class TopStats(Enum):
    # example for how to set a custom maxDiff value
    CLIENT_PKTS = ('clientPkts', 0.01)
    SERVER_PKTS = 'serverPkts'
    TOTAL_BYTES = 'totalBytes'
    SERVER_BYTES = 'serverBytes'
    SHARE = 'share'
    TOTAL_PKTS = 'totalPkts'
    TOTAL_COUNT = 'totalCount'
    RULE_ID = 'ruleId'
    CLIENT_BYTES = 'clientBytes'
    DISCOVERY = 'discovery'
    AS_NUMBER = 'asNumber'
    MESSAGE = 'msg'

    def __init__(self, statType, maxDiff=0.01):
        '''
        Keyword arguments:
        statType -- the name of the stat as it's returned from MW
        maxDiff -- the maximum allowed difference between the actual and expected value (i.e. atip and bps) (see rel_tol argument description from isclose method)  
        '''
        self.statType = statType
        self.maxDiff = maxDiff




