from enum import Enum

class StatsType(Enum):
    APPS = 'apps'
    APPTYPE = 'apptype'
    GEO = 'geo'
    BROWSERS = 'browsers'
    DEVICES = 'devices'
    DEVICE = 'device'
    CLIENTS = 'clientip'
    SERVERS = 'serverip'
    DYNAMIC = 'dynamic'
    PROVIDER = 'provider'
    WORLD = 'mapStats'
    TRAFFIC = 'portStats'
    RULES = "rules"
    RAPSHEET = "rapsheet"

    def __init__(self, statsName):
        self.statsName = statsName