from enum import Enum


class DedupPacketWindowType(Enum):
    LESS1GBPS = 42250
    LESS5GBPS = 211250
    LESS10GBPS = 422300
    OVER10GBPS = 844600


    def __init__(self, packetWindowValue):
        self.packetWindowValue = packetWindowValue






