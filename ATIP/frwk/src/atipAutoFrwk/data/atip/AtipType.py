from enum import Enum


class AtipType(Enum):
    HARDWARE = 1
    VIRTUAL = 2
    