from enum import Enum


class ProcStats(Enum):
    # example for how to set a custom maxDiff value
    RX_QUEUE_DEPTH = 'Rx Queue Depth'
    RX_QUEUE_DROPS = 'Rx Queue Drops'
    RX_PKTS = 'Rx Pkts'
    NEW_CONN = 'New Connections'
    CONN_TIMEOUTS = 'Connection Timeouts'
    SKIP_FULL = 'Skip (CT Full)'
    SKIP_NOMEN = 'Skip (CT NOMEM)'
    DEADCONN = 'Deadconn Queue Drops'
    TICKLES = 'Tickles'


    def __init__(self, statType):
        '''
        Keyword arguments:
        statType -- the name of the stat
        '''
        self.statType = statType
