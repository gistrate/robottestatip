from enum import Enum

class MaxItemsPerCategory(Enum):
    MAX_STATIC_APPS = 249
    MAX_DYNAMIC_APPS = 2000

    def __init__(self, maxNumber):
        '''
        Keyword arguments:
        maxNumber -- maximum number of items in specific category
        '''
        self.maxNumber = maxNumber