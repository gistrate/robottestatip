from enum import Enum



class ThreatType(Enum):

    HIJACKED = 'hijacked'
    PHISHING = 'phishing'
    MALWARE = 'malware'
    BOTNET = 'botnet'
    EXPLOIT = 'exploit'


    def __init__(self, threatName):
        self.threatName = threatName


    @classmethod
    def toDict(cls, threatType):
        ':type threatType: ThreatType'

        threatDict = {
                "id" : threatType.value
            }
        return threatDict