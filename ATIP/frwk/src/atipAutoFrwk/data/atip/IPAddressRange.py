class IPAddressRange(object):

    def __init__(self, startRange, endRange, source):
        self.startRange = startRange
        self.endRange = endRange
        self.source = source

    def toDict(self):

        ipAddressRange = {
            'startRange' : self.startRange,
            'endRange' : self.endRange,
            'source' : self.source
            }
        return ipAddressRange
