from enum import Enum

class SshNP(Enum):
    CORE_PATH = '/var/ipc/'
    CORE_FILES_PATH = '/mnt/tests/coreFiles/'
    DUMP_FILES_PATH = '/mnt/tests/dumpFiles/'
    SYSINFO_PATH = '/usr/scripts/sysinfo.sh'
    LOG_FILES_PATH = '/var/log/atip'
    LOG_FILES_PATH_NP = '/var/log'
    ATIP_HW_PKey = '../../../frwk/src/atipAutoFrwk/data/NPPrivateKeys/anue_id_rsa'
    ATIP_VIRT_PKey = '../../../frwk/src/atipAutoFrwk/data/NPPrivateKeys/vatip_ssh_rsa'

    def __init__(self, filePath):
        '''
        Keyword arguments:
        filePath -- the path to which files are located
        '''
        self.filePath = filePath