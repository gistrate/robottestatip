from enum import Enum


class ConfigTimeout(Enum):
    '''
    Contains values for maximum time for which it is considered OK to wait for the setup to be updated to the given configuration.
    Expects seconds.
    '''

    FILTERS_UPDATE = (180)
    CLEAR_STATS = (60)

    def __init__(self, waitTime):
        self.waitTime = waitTime
        