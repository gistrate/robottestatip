from enum import Enum


class AppGroupConditions(Enum):
    CATEGORY = 'Category'
    TRANSPORT = 'Transport'
    PROVIDER = 'Provider'
    OS = 'OS'
    DYNAMIC = 'Dynamic'


    def __init__(self, appGroupValue):
        self.appGroupValue = appGroupValue



class AppGroupType(Enum):
    STEAMING = (AppGroupConditions.CATEGORY, 'Streaming')
    NETFLIX = (AppGroupConditions.PROVIDER, 'Netflix')
    FACEBOOK = (AppGroupConditions.PROVIDER, 'Facebook')
    HTTP = (AppGroupConditions.TRANSPORT, 'http')
    DYNAMIC = (AppGroupConditions.DYNAMIC, 'Dynamic Application')

    def __init__(self, appGroupCondition, appGroupName):
        self.appGroupName = appGroupName
        self.appGroupCondition = appGroupCondition

    def toDict(self):

        appGroupDict = {
            "tag": self.appGroupName,
            "tagGroup": self.appGroupCondition.appGroupValue
        }
        return appGroupDict



