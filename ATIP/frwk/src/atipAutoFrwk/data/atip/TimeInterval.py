from enum import Enum


class TimeInterval(Enum):
 
    FIVEMIN = 'FIVEMIN'
    HOUR = 'HOUR'
    DAY = 'DAY'
    WEEK = 'WEEK'


    def __init__(self, intervalName):
        self.intervalName= intervalName
        