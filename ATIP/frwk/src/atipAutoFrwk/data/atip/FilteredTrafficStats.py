from enum import Enum


class FilteredTrafficStats(Enum):
    # example for how to set a custom maxDiff value
    APP = 'app'
    CLIENT_BYTES = 'clientBytes'
    CLIENT_CITY = 'clientCity'
    CLIENT_COUNTRY = 'clientCountry'
    CLIENT_IP = 'clientIP'
    CLIENT_PKTS = 'clientPkts'
    CLIENT_REGION = 'clientRegion'
    COUNT = 'count'
    DEVICE = 'device'
    LATENCY = '0'
    NAME = 'name'
    RX_IF = 'rxIf'
    SERVER_BYTES = 'serverBytes'
    SERVER_CITY = 'serverCity'
    SERVER_COUNTRY = 'serverCountry'
    SERVER_IP = 'serverIP'
    SERVER_PKTS = 'serverPkts'
    SERVER_REGION = 'serverRegion'
    TIME = 'ts'

    def __init__(self, statType):
        '''
        Keyword arguments:
        statType -- the name of the stat as it's returned from MW
        '''
        self.statType = statType




