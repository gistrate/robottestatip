from enum import Enum


class TimeValues(Enum):
 
    TEN_MINUTES = 600

    def __init__(self, time):
        self.time = time
        