import jsonpickle


class PieStatsItem(object):
    '''
    Item from the pie stats list.
    '''

    def __init__(self):
        self.name = None
        self.y = None
        self.totalBytes = None
        self.totalPkts = None
        self.totalCount = None

    def __str__(self):
        return jsonpickle.encode(self)

    def getName(self):
        return self.name.lower()

    def getShare(self):
        return self.y

    def getSessions(self):
        return self.totalCount

    def getPkts(self):
        return self.totalPkts

    def getBytes(self):
        return self.totalBytes