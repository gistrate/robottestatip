
class NetflowConnItem(object):
    '''
    Contains the description the a conn object.
    '''


    def __init__(self, conn=0):

        self._conn = conn
        self.option_records_sent = 0
        self.data_templates_sent = 0
        self.conn_status = None
        self.data_records_sent = 0
        self.option_templates_sent = 0
        self.enabled = False
        self.collector = ""
        self.connected = False
        self.total_pkts_sent = 0
        self._port= "4739"
        self.pkts_per_second = 0
        self.total_bytes_sent = 0
        self.proto= "TCP"
        self.bits_per_second = 0

    @property
    def port(self, port):
        return self._port

    @port.getter
    def port(self):
        return self._port

    @port.setter
    def port(self, port):
        self._port= int(port)

    @property
    def conn(self, conn):
        return self._conn

    @conn.getter
    def conn(self):
        return self._conn

    @conn.setter
    def conn(self, conn):
        self._conn = int(conn)