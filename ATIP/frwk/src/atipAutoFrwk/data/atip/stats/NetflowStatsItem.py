from atipAutoFrwk.data.atip.stats.NetflowConnItem import NetflowConnItem
from atipAutoFrwk.config.atip.NetflowCollectorConfig import NetflowCollectorConfig


class NetflowStatsItem(object):
    '''
    Contains the description for a netflow stats item.
    '''

    def __init__(self):
        self.biflows = None
        self.slotNum = None
        self.version = None
        self.enabled = None
        self.timeout = None
        self.conn = [NetflowConnItem(conn=i) for i in range(NetflowCollectorConfig.MAXIMUM_COLLECTORS)]
