import jsonpickle


class DetailedFilterItem(object):
    '''
    Item from the top stats list.
    '''

    def __init__(self):
        self.app = None
        self.clientBytes = None
        self.clientCity = None
        self.clientCountry = None
        self.clientIP = None
        self.clientPkts = None
        self.clientRegion = None
        self.count = None
        self.device = None
        self.latency = None
        self.name = None
        self.rxIf = None
        self.serverCity = None
        self.serverCountry = None
        self.serverIP = None
        self.serverPkts = None
        self.serverRegion = None
        self.serverBytes = None
        self.ts = None

    def __str__(self):
        return jsonpickle.encode(self)

    def getName(self):
        return self.name

    def getAppName(self):
        return self.app

    def getClientIP(self):
        return self.clientIP

    def getServerIP(self):
        return self.serverIP