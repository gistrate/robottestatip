import jsonpickle


class NetflowStatsContainer(object):
    '''
    Expects the Netflow stats.
    '''

    def __init__(self,statsItemsList):
        '''
        Expects a list of NetflowStatsItem objects
        '''
        self.status = statsItemsList

    def getList(self):
        return self.status

    def __str__(self):
        return jsonpickle.encode(self.getList())
