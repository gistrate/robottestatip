import jsonpickle


class MapsStatsContainer(object):
    '''
    Contains the ATIP stats for a world chart.
    '''

    def __init__(self, statsItemsList):
        '''
        Expects a list of MapsStatsItem objects.
        '''
        self.stats = statsItemsList
        self.name = 'geo'

    def getList(self):
        return self.stats

    def __str__(self):
        return "Name: {}\nStats: {}".format(self.name, jsonpickle.encode(self.stats))