import jsonpickle


class TrafficContainer(object):
    '''
    Contains the ATIP stats for Traffic stats chart.
    '''

    def __init__(self, statsItemsList):
        '''
        Expects a list of TrafficItem objects.
        '''

        self.list = statsItemsList

    def getList(self):
        return self.list

    def getItemByName(self, name):
        return list(filter(lambda x: x.name == name, self.list))

    def getSessions(self):
        goodItem = self.getItemByName('Sessions/s')[0]
        return goodItem.pointInterval / 1000 * sum(goodItem.data)

    def getBytes(self):
        goodItem = self.getItemByName('Bits/s')[0]
        return goodItem.pointInterval / 1000 * sum(goodItem.data)

    def __str__(self):
        return jsonpickle.encode(self.getList())
