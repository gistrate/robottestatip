import jsonpickle


class TrafficItem(object):
    '''
    Item from the top stats list.
    '''

    def __init__(self):

        self.pointStart = None
        self.data = None
        self.pointInterval = None
        self.name = None

    def __str__(self):
        return jsonpickle.encode(self)

    def getName(self):
        return self.name.lower()

