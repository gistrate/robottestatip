class PieStatsContainer(object):
    '''
    Contains the ATIP stats for a pie chart.
    '''

    def __init__(self, statsItemsList):
        '''
        Expects a list of PieStatsItem objects.
        '''
        self.data = statsItemsList

    def getList(self):
        return self.data
