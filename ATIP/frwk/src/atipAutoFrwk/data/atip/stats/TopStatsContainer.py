class TopStatsContainer(object):
    '''
    Contains the ATIP stats for top stats list chart.
    '''

    def __init__(self, name, statsItemsList):
        '''
        Expects a list of TopStatsItem objects.
        '''
        self.name = name
        self.list = statsItemsList

    def getList(self):
        return self.list
