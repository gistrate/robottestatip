
class DetailedFilterContainer(object):
    '''
    Contains the ATIP stats for detailed filter stats list chart.
    '''

    def __init__(self, name, statsItemsList):
        '''
        Expects a list of DetailedFilterItem objects.
        '''
        self.name = name
        self.list = statsItemsList

    def getList(self):
        return self.list
