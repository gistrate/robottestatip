import jsonpickle


class TopStatsItem(object):
    '''
    Item from the top stats list.
    '''

    def __init__(self):
        self.msg = None
        self.clientPkts = 0
        self.serverPkts = 0
        self.totalBytes = 0
        self.serverBytes = 0
        self.share = 0
        self.totalPkts = 0
        self.totalCount = 0
        self.clientBytes = 0
        self.asNumber = None
        self.ts = None

    def __str__(self):
        return jsonpickle.encode(self)

    def getName(self):
        return self.msg

    def getShare(self):
        return self.share