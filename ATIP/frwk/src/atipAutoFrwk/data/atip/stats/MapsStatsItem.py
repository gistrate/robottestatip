import jsonpickle


class MapsStatsItem(object):
    '''
    Item from the top stats list.
    '''

    def __init__(self):
        self.country = None
        self.clientPkts = None
        self.serverPkts = None
        self.totalBytes = None
        self.serverBytes = None
        self.links = list()
        self.countryName = None
        self.totalPkts = None
        self.totalCount = None
        self.percent = None
        self.clientBytes = None

    def getName(self):
        return self.countryName.lower()

    def __str__(self):
        return jsonpickle.encode(self)