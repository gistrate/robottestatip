class IngressPacketBuffer(object):
    # default values defined in software - change these if specs specs change
    SOFTWARE_RX_QUEUE_DEFAULT_VALUE = 8000
    MIN_SOFTWARE_RX_QUEUE_DEFAULT_VALUE = 4000
    MAX_SOFTWARE_RX_QUEUE_DEFAULT_VALUE = 32000