import jsonpickle


class BpsComponentStatsService(object):
    '''
    *** WARNING***
    All methods of this class assume that all app sim from the bps test have a SINGLE app, browser and device!
    '''

    @classmethod
    def sumStatItem(cls, bpsComponentsStats, bpsStatsType):
        '''
        Computes the sum of the given stat item by grouping the values according to the given attribute.
        :param bpsComponentsStats: dictionary containing the bps component stats by component (as they are returned by BpsComponentStats.getAllComponentsStats)
        :param bpsStatsType: the bps stat item for which the sum is computed
        '''
        ':type bpsTestConfig: BpsTestConfig'
        ':type bpsStatsType: BpsStatsType'

        statSum = 0
        for component, componentStats in bpsComponentsStats.items():
            statItemValue = getattr(componentStats, bpsStatsType.statsName)
            statSum += statItemValue
        return statSum

    @classmethod
    def getStatItem(cls, componentStats, bpsStatsType):
        '''
        Gets the value of the given stat item.
        :param componentStats: bps component stats
        :param bpsStatsType: the bps stat item for which the value is retrieved
        '''
        ':type bpsStatsType: BpsStatsType'


        statItemValue = getattr(componentStats, bpsStatsType.statsName)
        return statItemValue


    @classmethod
    def sumStatItemByType(cls, bpsTestConfig, bpsComponentsStats, bpsStatsType, statsType):
        '''
        Computes the sum of the given stat item by grouping the values according to the given attribute.
        :param bpsTestConfig: the configuration that contains the appSimComponents attributes
        :param bpsComponentsStats: dictionary containing the bps component stats by component (as they are returned by BpsComponentStats.getAllComponentsStats)
        :param bpsStatsType: the bps stat item for which the sum is computed
        '''
        ':type bpsTestConfig: BpsTestConfig'
        ':type bpsStatsType: BpsStatsType'
        ':type statsType: StatsType'

        results = dict()
        for component, appSimConfig in bpsTestConfig.appSimComponents.items():
            crtStatItem = appSimConfig.getKeyName(statsType)
            if component in bpsComponentsStats:
                statItemValue = getattr(bpsComponentsStats[component], bpsStatsType.statsName)
                if crtStatItem in results:
                    results[crtStatItem] += statItemValue
                else:
                    results[crtStatItem] = statItemValue
        return results

    @classmethod
    def filterStatsByType(cls, bpsTestConfig, bpsComponentsStats, statsType, name):
        '''
        Create a new dict that contains only the bps stats for which the component has the given type.
        '''
        filteredResult = dict()
        for component, appSimConfig in bpsTestConfig.appSimComponents.items():
            if appSimConfig.getKeyName(statsType) == name:
                filteredResult[component] = bpsComponentsStats[component]
        return filteredResult

    @classmethod
    def filterStatsByClientLocation(cls, bpsTestConfig, bpsComponentsStats, countryName):
        '''
        Create a new dict that contains only the bps stats for which the component has the given client location.
        '''
        filteredResult = dict()
        for component, appSimConfig in bpsTestConfig.appSimComponents.items():

            if appSimConfig.clientLocation.countryName == countryName:
                filteredResult[component] = bpsComponentsStats[component]
        return filteredResult

    @classmethod
    def filterStatsByServerLocation(cls, bpsTestConfig, bpsComponentsStats, countryName):
        '''
        Create a new dict that contains only the bps stats for which the component has the given client location.
        '''
        filteredResult = dict()
        for component, appSimConfig in bpsTestConfig.appSimComponents.items():
            if appSimConfig.serverLocation.countryName == countryName:
                filteredResult[component] = bpsComponentsStats[component]
        return filteredResult

    @classmethod
    def filterStatsByClientServerLocation(cls, bpsTestConfig, bpsComponentsStats, countryName):
        '''
        Create a new dict that contains only the bps stats for which the component has the given client location.
        '''
        filteredResult = dict()
        for component, appSimConfig in bpsTestConfig.appSimComponents.items():
            if appSimConfig.client.countryName == countryName or appSimConfig.serverLocation.countryName == countryName:
                filteredResult[component] = bpsComponentsStats[component]
        return filteredResult



