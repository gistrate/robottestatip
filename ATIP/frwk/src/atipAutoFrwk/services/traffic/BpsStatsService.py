import re

from atipAutoFrwk.webApi.traffic.BpsStats import BpsStats
from atipAutoFrwk.data.traffic.RealTimeStats import RealTimeStats


class BpsStatsService(object):
    
    @classmethod
    def getAllRealTimeStats(cls, webApiSession, testId, statsGroup, strict=True):
        '''
        Keyword arguments:
        strict -- this is considered only when the requested value cannot be returned; if True, an exception is raised; otherwise None is returned
        '''
        ':type webApiSession: WebApiSession'
        ':type statName: RealTimeStats'
        
        statsList = BpsStats.getBpsRtsStatsJSON(webApiSession, testId, statsGroup)
        if not statsList.get('rts'):
            if strict:
                raise Exception('No Real Time Statistics found.')
            else:
                return None

        values = re.search(r"\[(.*?)\]", statsList.get('rts'))       
        if values is None:
            if strict:
                raise Exception('No BPS stats available.')
            else:
                return None
        statsList = values.group(1).split()
        bpsStats = BpsStatsService.convertToDict(statsList)
        
        if not bpsStats:
            if strict:
                raise Exception('Couldn\'t get the values for Real Time Statistics.')
        
        return bpsStats


    @classmethod
    def getRealTimeStatsByName(cls, webApiSession, testId, rtsStat, statsGroup="summary", strict=True):
        '''
        Keyword arguments:
        strict -- this is considered only when the requested value cannot be returned; if True, an exception is raised; otherwise None is returned
        '''
        ':type webApiSession: WebApiSession'
        ':type rtsStat: RealTimeStats'
        
        rts = cls.getAllRealTimeStats(webApiSession, testId, statsGroup, strict)
        if strict:
            if not rts[rtsStat.statType]:
                raise Exception('Couldn\'t get the value for: {}'.format(rtsStat.statType))
        return rts[rtsStat.statType]

    @classmethod
    def getL7RealTimeStats(cls, webApiSession, testId, strict=True):
        '''
        Keyword arguments:
        strict -- this is considered only when the requested value cannot be returned; if True, an exception is raised; otherwise None is returned
        '''
        ':type webApiSession: WebApiSession'
        ':type statName: RealTimeStats'

        statsList = BpsStats.getBpsRtsStatsJSON(webApiSession, testId, "l7Stats")
        print(statsList)
        if not statsList.get('rts'):
            if strict:
                raise Exception('No Real Time Statistics found.')
            else:
                return None

        values = re.search(r"stats:\[(.*?)\]", statsList.get('rts'))
        print("values")
        print(values)
        if values is None:
            if strict:
                raise Exception('No BPS stats available.')
            else:
                return None
        statsList = values.group(1).split()
        bpsStats = BpsStatsService.convertToDict(statsList)

        if not bpsStats:
            if strict:
                raise Exception('Couldn\'t get the values for Real Time Statistics.')

        return bpsStats

    @classmethod
    def getL7RealTimeStatsByName(cls, webApiSession, testId, rtsStat, strict=True):
        '''
        Keyword arguments:
        strict -- this is considered only when the requested value cannot be returned; if True, an exception is raised; otherwise None is returned
        '''
        ':type webApiSession: WebApiSession'
        ':type rtsStat: RealTimeStats'

        rts = cls.getL7RealTimeStats(webApiSession, testId, strict)
        if strict:
            if not rts[rtsStat.statType]:
                raise Exception('Couldn\'t get the value for: {}'.format(rtsStat.statType))
        return rts[rtsStat.statType]

    @classmethod
    def convertToDict(cls, statsList):
        bpsStats = {}
        for stats in statsList:
            statsVal = stats.split("=")
            bpsStats[statsVal[0]] = float(statsVal[1].strip("'"))
        return bpsStats

    @classmethod
    def convertBytes(cls, bytes, to, bsize=1024):

        a = {'k': 1, 'm': 2, 'g': 3, 't': 4, 'p': 5, 'e': 6}
        r = float(bytes)
        for i in range(a[to]):
            r = r / bsize

        return "{0:0.1f}".format(r)

    @classmethod
    def compareConvertedBytes(self, actualValue, expectedValue):

        expectedValueConverted = BpsStatsService.convertBytes(expectedValue, 'k')
        actualValueConverted = BpsStatsService.convertBytes(actualValue, 'k')
        if expectedValueConverted != actualValueConverted:
            raise Exception(
                'The total number of bytes {} is not correct, expected values is {}.'.format(actualValueConverted,
                                                                                             expectedValueConverted))
        else:
            print('The total number of bytes {} is correct, expected values is {}.'.format(actualValueConverted, expectedValueConverted))
            return actualValueConverted


    def getBpsTotalSessions(self, bpsSession, bpsTestConfig):
        return BpsStatsService.getRealTimeStatsByName(bpsSession, bpsTestConfig.testId, RealTimeStats.APP_SUCCESSFUL)

    def getBpsTotalTxPackets(self, bpsSession, bpsTestConfig):
        return BpsStatsService.getRealTimeStatsByName(bpsSession, bpsTestConfig.testId, RealTimeStats.ETH_TX_FRAMES)

    def getBpsTotalBytes(self, bpsSession, bpsTestConfig):
        return BpsStatsService.getL7RealTimeStatsByName(bpsSession, bpsTestConfig.testId, RealTimeStats.APP_TX_FRAME_DATA)




