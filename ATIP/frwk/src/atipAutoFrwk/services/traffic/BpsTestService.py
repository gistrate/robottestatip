from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
import time

class BpsTestService(object):
    @classmethod
    def createBpsTestConfig(cls,testName, testDuration):
        return BpsTestConfig(testName, testDuration)

    @classmethod
    def startAndWaitForTraffic(cls, webApiSession, testName, group):
        testId = BpsOperations.startBps(webApiSession,testName,group)
        bpsStatus = BpsOperations.getBpsTestProgress(webApiSession, testId)
        i = 0
        while bpsStatus <= 1:
            print('Sleeping 1s, waited BPS traffic for {}s now.'.format(i))
            time.sleep(1)
            bpsStatus = BpsOperations.getBpsTestProgress(webApiSession, testId)
            i += 1
        return testId
