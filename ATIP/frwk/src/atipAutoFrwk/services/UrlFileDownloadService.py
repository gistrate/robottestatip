import urllib.request
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.BuildType import BuildType
import os.path
import shutil
import re

class UrlFileDownloadService:

    @classmethod
    def getLatestBuildUrlLocation(cls, env, latestFile, version=BuildType.UNSTABLE):
        '''
        Retrieve URL location of files from latest build version
        :type latestFile: ReleaseFileConfig
        :param env: environment variables
        :param latestFile: object containing build server url, ATIP type and file name to be downloaded; build server url can be e.g. http://build-filesrv-0.ann.is.keysight.com/builds/
        :param version: variable to indicate if file should be searched into "releases"(latest released build) or "unstable" branches
        :return: Url of requested file from latest build version location
        '''
        if env.atipType == AtipType.VIRTUAL:
            latestFile.buildServerLocation += 'vatip/' + version.buildType + '/'
        else:
            latestFile.buildServerLocation += 'atip/' + version.buildType + '/'

        locationLinks = cls.getLocationUrls(latestFile.buildServerLocation)
        latestBuildLocation = locationLinks[-1]
        # boolean variable to check if latest build folder URL was located in current URL or had to be further searched in subURLs
        searchedLatest = False

        while 'latest/' not in latestBuildLocation:
            latestFile.buildServerLocation += locationLinks[-1]
            locationLinks = cls.getLocationUrls(latestFile.buildServerLocation)
            latestBuildLocation = cls.checkLatestBuild(locationLinks)
            latestFile.buildServerLocation += latestBuildLocation
            searchedLatest = True

        if not searchedLatest:
            latestFile.buildServerLocation += latestBuildLocation

        # open latest build location and get requested filename url
        latestLocationLinks = cls.getLocationUrls(latestFile.buildServerLocation)
        for link in latestLocationLinks:
            if latestFile.fileName in link:
                latestFile.buildServerLocation += link

        return latestFile.buildServerLocation

    @classmethod
    def getLocationUrls(cls, url):
        '''
        Retrive all subURLs from specified parent URL
        :param url: URL to get subURLs
        :return: a list containing all subURLs of parent URL
        '''
        links = []
        resp = urllib.request.urlopen(url)
        html = resp.read()
        urls = re.findall('href="(.*?)"', str(html))
        for url in urls:
            links.append(url)

        return links

    @classmethod
    def checkLatestBuild(cls, links):
        '''
        Verify if current list of URLs contains "latest"
        :param links: curent list of URLs to search for "latest"
        :return: "latest/" or last URL from initial list if "latest" has not been found
        '''
        for link in links:
            if 'latest' in link:
                return link

        return links[-1]


    @classmethod
    def getFileFromUrlLocation(cls, fileUrlLocation, localFilename):
        '''
        Download file from specified URL location and saves local with specified file name
        :param fileUrlLocation: URL location of file to be downloaded
        :param localFilename: file name to save local
        :return: absolute local path of saved file
        '''
        print('\r\nDownloading {0} from {1}'.format(localFilename, fileUrlLocation))

        with urllib.request.urlopen(fileUrlLocation) as response, open(localFilename, 'wb') as out_file:
            shutil.copyfileobj(response, out_file)

        localPath = os.path.join(os.getcwd(), localFilename)
        print('\r\nFinished downloading {} to {}'.format(localFilename, localPath))

        return localPath