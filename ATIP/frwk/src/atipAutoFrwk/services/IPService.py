import re
import jsonpickle
from atipAutoFrwk.data.traffic.stats.BpsRealTimeStatsContainer import BpsRealTimeStatsContainer
from atipAutoFrwk.data.traffic.stats.BpsRealTimeStatsItem import BpsRealTimeStatsItem
import ipaddress


class IPService(object):
    '''
    The class contains methods that are related to IP generation and mapping.
    '''

    @staticmethod
    def genIpRange(startIp, endIp):
        '''
        :return: a list of IPs ranging from the startIP until endIP (both inclusive)
        '''
        startValues = list(map(int, startIp.split(".")))
        endValues = list(map(int, endIp.split(".")))

        current = startValues
        ipRange = []

        ipRange.append(startIp)
        while current != endValues:
            startValues[3] += 1
            for i in 3, 2, 1:
                if current[i] == 256:
                    current[i] = 0
                    current[i-1] += 1
            ipRange.append(".".join(map(str, current)))
        return ipRange

    @staticmethod
    def getIpRangeFromList(ipList):
        '''
        Get ip address range from a list of ip addresses
        :param ipList: list of ip addresses to get a range from
        :return: start ip and end ip addresses based on ip addresses list provided as input
        '''
        ipAddressList = []
        for ip in ipList:
            ipAddressList.append(ipaddress.IPv4Address(ip))
        ipAddressList.sort()

        return ipAddressList[0], ipAddressList[-1]
