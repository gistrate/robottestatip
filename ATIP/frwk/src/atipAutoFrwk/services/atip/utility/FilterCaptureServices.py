from atipAutoFrwk.services.atip.FileService import FileService

class FilterCaptureServices(object):

    def filterCapture(self, niddle, testId, format="text", vlanId="", out_filename=""):
        tsharkCommand = ""
        pcap = '{0}.pcap'.format(testId)
        result_filename = testId

        if format == "text":
            if vlanId != "":
                tsharkCommand = "tshark -r {0} -Y 'frame contains \"{1}\" and vlan.id=={3}' > {2}.result".format(pcap, niddle, result_filename, vlanId)
            else:
                tsharkCommand = "tshark -r {0} -Y 'frame contains \"{1}\"' > {2}.result".format(pcap, niddle, result_filename)
        elif format == "hex":
                tsharkCommand = 'tshark -r {0} -Y "frame contains {1}" > {2}.result'.format(pcap, niddle, result_filename)
        else:
                raise Exception("Use correct filter capture parameter: text or hex value")

        return tsharkCommand

    @classmethod
    def configFilterCommand(cls, testId, displayFilter, format="text", vlanId="", outFilename=""):
        resultFilename = testId

        if outFilename != "":
            resultFilename = outFilename

        if format == "text":
            if vlanId != "":
                tsharkCommand = "tshark -r {0} -Y '{1} and vlan.id=={2}' > {3}.result".format(testId, displayFilter,
                                                                                                vlanId, resultFilename)
            else:
                tsharkCommand = "tshark -r {0} -Y '{1}' > {2}.result".format(testId, displayFilter, resultFilename)
        elif format == "hex":
            tsharkCommand = "tshark -r {0} -Y '{1}' > {2}.result".format(testId, displayFilter, resultFilename)
        else:
            raise Exception("Use correct filter capture parameter: text or hex value")

        return tsharkCommand

    @staticmethod
    def configFilteringCommand(testId, displayFilter, format="text", vlanId="", outFilename=""):
        '''
        Create filtering command based on display filter provided
        :param testId: self explanatory
        :param displayFilter: tshark parameter based on that the capture will be filtered
        :param format: capture file format
        :param vlanId: vlan ID the traffic has been sent on
        :param outFilename: resulting file name containing filtered capture
        :return: tshark command to be used to filter capture file
        '''
        tsharkCommand = ""
        pcap = '{0}.pcap'.format(testId)
        resultFilename = testId

        if outFilename != "":
            resultFilename = outFilename

        if format == "text":
            if vlanId != "":
                tsharkCommand = "tshark -r {0} -Y '{1} and vlan.id=={2}' > {3}.result".format(pcap, displayFilter, vlanId, resultFilename)
            else:
                tsharkCommand = "tshark -r {0} -Y '{1}' > {2}.result".format(pcap, displayFilter, resultFilename)
        elif format == "hex":
            tsharkCommand = "tshark -r {0} -Y '{1}' > {2}.result".format(pcap, displayFilter, resultFilename)
        else:
            raise Exception("Use correct filter capture parameter: text or hex value")

        return tsharkCommand
 
    @classmethod
    def transferAtipCapture(cls, webApiSession, envVars, localPcapFolder, remotePcap):
        NamesList = []
        files = FileService.getFolderFileList(localPcapFolder)
        index = 0
        for Pcap in files:
            localPath = localPcapFolder + Pcap
            RemoteName = remotePcap + '_{}'.format(index) + '.pcap'
            index += 1
            NamesList.append(RemoteName)
            FileService.sftpCopyToLinuxBox(envVars, localPath, RemoteName, 'to')
        FileService.removePcapAndFolder(webApiSession, localPcapFolder)

        return NamesList