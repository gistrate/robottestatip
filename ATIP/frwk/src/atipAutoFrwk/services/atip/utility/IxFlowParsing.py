import telnetlib
import base64
import time
import os
import re
from pathlib import Path



class IxFlowParsing(object):


    def performTelnet(self, envVars):

        tn = telnetlib.Telnet('envVars.linuxConnectionConfig.host')
        user = envVars.linuxConnectionConfig.username
        password = envVars.linuxConnectionConfig.password
        tn.read_until(b'atip-virtual-machine login: ')
        tn.write(user.encode('ascii') + b'\n')
        tn.read_until(b'Password: ')
        tn.write(password.encode('ascii') + b'\n')
        return tn

    def copyFileToLinux(self):
        dir = os.path.dirname(__file__)
        path = os.path.join(dir,"nf_script.py")
        with open(path,'r') as content_file:
            contents = content_file.read()
        time.sleep(3)
        encoded = base64.b64encode(bytes(contents, 'UTF-8'))
        encoded = encoded.decode('UTF-8').strip('\'')
        cmd = "echo \"{}\" | base64 --decode > parsing.py".format(encoded)
        command = cmd.encode('UTF-8') + b'\n'
        return command

    def parseOutput(self, output):
        dict = {}
        result = re.findall(r'(.*?):(\d+)',output)
        for i in result:
            print(i)
            print(i[0])
            dict[i[0]]=i[1]

        return dict