#!/usr/bin/python
import sys, csv, os, subprocess, StringIO, datetime

SEP_CHAR = '@'
TSHARK_CMD = "tshark -T fields -E header=y -E separator=, -E quote=d -E occurrence=a -E aggregator=%s" % (SEP_CHAR)
TSHARK_CMD = TSHARK_CMD.split() + ["-Y", "cflow && !icmp"]
FIELDSv9 = [
    "frame.number",
    "cflow.od_id",
    "cflow.packets",
    "cflow.octets",
    # "cflow.protocol",
    # "cflow.srcport",
    # "cflow.srcaddr",
    # "cflow.inputint",
    # "cflow.dstport",
    # "cflow.dstaddr",
    # "cflow.outputint",
    # "cflow.srcas",
    # "cflow.dstas",
    "cflow.timestart",
    "cflow.timeend",
]

FIELDSv10 = [
    "frame.number",
    # "cflow.od_id",
    "cflow.packets",
    # "cflow.octets",
    # "cflow.protocol",
    "cflow.srcport",
    "cflow.srcaddr",
    # "cflow.inputint",
    "cflow.dstport",
    "cflow.dstaddr",
    # "cflow.outputint",
    # "cflow.srcas",
    # "cflow.dstas",
    # "cflow.abstimestart",
    # "cflow.abstimeend",
    # "cflow.pie.ixia.l7-application-id",
    # "cflow.pie.ixia.l7-application-name",
    # "cflow.pie.ixia.source-ip-country-code",
    # "cflow.pie.ixia.source-ip-country-name",
    # "cflow.pie.ixia.source-ip-region-code",
    # "cflow.pie.ixia.source-ip-region-name",
    # "cflow.pie.ixia.source-ip-city-name",
    # "cflow.pie.ixia.source-ip-latitude",
    # "cflow.pie.ixia.source-ip-longitude",
    # "cflow.pie.ixia.destination-ip-country-code",
    # "cflow.pie.ixia.destination-ip-country-name",
    # "cflow.pie.ixia.destination-ip-region-code",
    # "cflow.pie.ixia.destination-ip-region-name",
    # "cflow.pie.ixia.destination-ip-city-name",
    # "cflow.pie.ixia.destination-ip-latitude",
    # "cflow.pie.ixia.destination-ip-longitude",
    # "cflow.pie.ixia.os-device-id",
    # "cflow.pie.ixia.os-device-name",
    # "cflow.pie.ixia.browser-id",
    # "cflow.pie.ixia.browser-name",
    # "cflow.pie.ixia.reverse-octet-delta-count",
    # "cflow.pie.ixia.reverse-packet-delta-count",
    # "cflow.pie.ixia.conn-encryption-type",
    # "cflow.pie.ixia.encryption-cipher",
    # "cflow.pie.ixia.encryption-keylen",
    # "cflow.pie.ixia.imsi",
    # "cflow.pie.ixia.http-user-agent",
    # "cflow.pie.ixia.hostname",
    # "cflow.pie.ixia.http-uri",
    # "cflow.pie.ixia.dns-txt",
]

FIELDS = FIELDSv10

# Convert the date time stamp string to milliseconds since epoch
epoch_dt = datetime.datetime.fromtimestamp(0)


def date_norm(tshark_date):
    if not len(tshark_date):
        return ''
    delta = datetime.datetime.strptime(tshark_date, "%b %d, %Y %H:%M:%S.%f000") - epoch_dt
    return ((delta.days * 24 * 60 * 60 + delta.seconds) * 1000 + delta.microseconds / 1000)


def translate_row(inrow):
    elems = 1
    """
    if inrow[6]:
        inrow[6] = inrow[6].split(SEP_CHAR)[0]  # To get a single flowset id in case it has multiple for option templates
    """

    # Flowsets with multiple flow records have each field value of all the records in a single column seperated by SEP_CHAR
    # Splitting out those SEP_CHAR seperated column values into individual rows for each record.
    inrow2 = []
    for item in inrow:
        item = item.split(SEP_CHAR)
        item_elems = len(item)
        if 0 == item_elems:
            item = ['']
        elif item_elems > 1:
            if (elems > 1) and item_elems != elems:
                print "Bad row ", inrow
                return []
            elems = item_elems
        inrow2.append(item)

    inrow = []
    for item in inrow2:
        if len(item) == 1:  # Duplicate the single valued columns like od_id for all the new rows
            item *= elems
        inrow.append(item)

    return zip(*inrow)


def count_values_in_rows(file, colName):
    rows = list(csv.reader(open(file, 'r'), delimiter=','))
    cols = zip(*rows)
    row_to_count = cols[rows[0].index(colName)][1:]
    dict_to_count = {}
    for item in row_to_count:
        dict_to_count.setdefault(item, 0)
        dict_to_count[item] = dict_to_count[item] + 1

    for key, value in dict_to_count.items():
        print(str(key) + ':' + str(value))


def __main():
    if len(sys.argv) < 4:
        print "usage %s <pcap file> <output csv> <fields>" % sys.argv[0]
        sys.exit(1)

    if not os.path.isfile(sys.argv[1]):
        print "Can't open pcap file %s" % sys.argv[1]
        sys.exit(1)

    tshark_cmd = TSHARK_CMD
    print "Fields %s" % FIELDS
    for i in sys.argv[3].split(","):
        FIELDS.append(i)


    for field in FIELDS:
        tshark_cmd.extend(['-e', field])
    tshark_cmd.extend(['-r', sys.argv[1]])

    print "Running command...\n" + ' '.join(tshark_cmd)
    p = subprocess.Popen(tshark_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    if len(err):
        print
        err
        # sys.exit(1)

    # Write out the raw tshark output
    """
    with open("./tsout.csv", "wb") as f:
        f.write(out)
    """

    print "Preparing the final csv %s..." % sys.argv[2]
    data_csv = csv.reader(StringIO.StringIO(out))

    pkts = 0
    bytes = 0
    recs = 0
    with open(sys.argv[2], "wb") as out_csv:
        out_csvw = csv.writer(out_csv)
        out_csvw.writerow(data_csv.next())  # Write out the header
        for row in data_csv:
            new_rows = translate_row(row)
            for r in new_rows:
                if "" == r[2]:
                    continue
                out_csvw.writerow(r)
                recs += 1
                try:
                    pkts += long(r[2])
                    bytes += long(r[3])
                except:
                    pass

    for i in sys.argv[3].split(","):
        count_values_in_rows(sys.argv[2], i)
    print "Records:%d Pkts:%d Bytes:%d" % (recs, pkts, bytes)


__main()
