from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.LoginService import LoginService
from atipAutoFrwk.services.atip.LogoutService import LogoutService
from atipAutoFrwk.services.atip.NPService import NPService
from atipAutoFrwk.services.atip.stats.ValidateStatsService import ValidateStatsService
from atipAutoFrwk.webApi.atip.Configuration import Configuration
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.NP import NP
from atipAutoFrwk.webApi.atip.Status import Status
from atipAutoFrwk.webApi.atip.System import System
from atipAutoFrwk.webApi.atip.Users import Users
from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage


class SystemService(object):

    @classmethod
    def waitUntilSystemReady(cls, webApiSession, password=None):
        ':type webApiSession: WebApiSession'

        isReadyLambda = lambda: Status.isReady(webApiSession, password) is True
        TestCondition.waitUntilTrue(isReadyLambda, errorMessage="NP isn't in ready status in 300 seconds!", totalTime=300, iterationTime=5)
        
    @classmethod
    def waitUntilATIPReady(cls, webApiSession, password=None):
        ':type webApiSession: WebApiSession'

        isReadyLambda = lambda: Status.isATIPSystemReady(webApiSession, password) is True
        TestCondition.waitUntilTrue(isReadyLambda, errorMessage="System isn't in ready status in 300 seconds!", totalTime=300, iterationTime=5)

    @classmethod
    def clearSystemAndWaitUntilNPReady(cls, webApiSession, env):
        ':type webApiSession: WebApiSession'
        ':type env: Environment'

        Login.login(webApiSession)
        print('Start clear system...')
        Configuration.clearSystem(webApiSession)
        print('System configuration cleared')
        LogoutService.waitUntilLoggedOut(webApiSession)
        print('Confirmed Logout')
        LoginService.waitUntilLoggedIn(webApiSession, env.atipConnectionConfig.username, env.atipDefaultPassword)
        print('Confirmed Login Default')

        if env.atipType == AtipType.VIRTUAL:
            print('trying to change password')
            SystemService.waitUntilATIPReady(webApiSession, password=env.atipDefaultPassword)
            print('Confirmed ATIP System ready')
            SystemService.waitUntilSystemReady(webApiSession, password=env.atipDefaultPassword)
            Users.changePassword(webApiSession, env.atipDefaultPassword, env.atipConnectionConfig.password)
            print('the password changed')
            NPService.updateNpIdVirtual(webApiSession, env)
        else:
            SystemService.waitUntilATIPReady(webApiSession)
            print('Confirmed ATIP System ready')
            SystemService.waitUntilSystemReady(webApiSession)
        print('System is up and ready.')
        
    @classmethod
    def resetAtipAndWaitUntilNPReady(cls, webApiSession, env):
        ':type webApiSession: WebApiSession'
        ':type env: Environment'

        Login.login(webApiSession)
        System.resetAtip(webApiSession)
        LogoutService.waitUntilLoggedOut(webApiSession)
        LoginService.waitUntilLoggedIn(webApiSession)

        SystemService.waitUntilATIPReady(webApiSession)
        SystemService.waitUntilSystemReady(webApiSession)
        DebugPage.checkNpStaticApps(webApiSession, env.atipSlotNumber)

    @classmethod
    def resetStatsAndWaitUntilCleared(cls, webApiSession):
        ':type webApiSession: WebApiSession'

        System.resetStats(webApiSession)
        for statsType in [StatsType.GEO, StatsType.DYNAMIC, StatsType.PROVIDER,StatsType.RULES]:
            ValidateStatsService.checkListStatsEmpty(webApiSession,statsType)
        for statsType in [StatsType.APPS, StatsType.BROWSERS, StatsType.DEVICES]:
            ValidateStatsService.checkPieStatsEmpty(webApiSession,statsType)
        ValidateStatsService.checkTrafficStatsEmpty(webApiSession, StatsType.TRAFFIC)