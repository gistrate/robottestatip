from atipAutoFrwk.services.atip.FileService import FileService
from atipAutoFrwk.webApi.atip.Capture import Capture
from http import HTTPStatus
import time

from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations


class CaptureService:

    @classmethod
    def getCaptureFileByName(cls, webApiSession, captureFile):
        '''
        Save capture file to local user's home folder; will be created item <user_home_folder>/location/captureFile
        :type webApiSession: WebApiSession
        :param captureFile: name of capture file to be downloaded
        :return: nothing
        '''

        allCaptureFiles, path = Capture.getCaptureFileList(webApiSession)
        found = False

        if not allCaptureFiles:
            print('No capture file...')
        else:
            if not isinstance(allCaptureFiles['rulesFile'], list):
                allCaptureFiles['rulesFile'] = [allCaptureFiles['rulesFile']]

        for item in allCaptureFiles['rulesFile']:
            if item['fileName'] == captureFile:
                print('Save capture file {}'.format(captureFile))
                FileService.saveFile(webApiSession, path, captureFile)
                found = True

        if not found:
            raise Exception('Capture file {} not found.'.format(captureFile))

    @classmethod
    def getLatestCaptureFile(cls, webApiSession):
        '''
        Save latest capture file to local user's home folder; will be created item <user_home_folder>/location/captureFile
        :type webApiSession: WebApiSession
        :return: absolute path to file
        '''

        allCaptureFiles, path = Capture.getCaptureFileList(webApiSession)

        if not allCaptureFiles:
            print('No capture file...')
        else:
            if not isinstance(allCaptureFiles['rulesFile'], list):
                allCaptureFiles['rulesFile'] = [allCaptureFiles['rulesFile']]

        print('Save last capture file {}'.format(allCaptureFiles['rulesFile'][-1]['fileName']))
        fileLocation = FileService.saveFile(webApiSession, path, allCaptureFiles['rulesFile'][-1]['fileName'])

        return fileLocation

    @classmethod
    def getLatestCaptureAndUnzip(cls, webApiSession):
        '''
        Download latest capture file from ATIP (zip file), saves it to local and unzip
        :param webApiSession: current session
        :return: absolute path to unzipped PCAP file
        '''

        # get latest pcap zip file from ATIP
        zipLocation = cls.getLatestCaptureFile(webApiSession)

        # unzip pcap file and delete zip file
        pcapFolder = FileService.unzipFileToFolder(zipLocation)

        return pcapFolder

    @classmethod
    def getCaptureFileListByRuleId(cls, webApiSession, ruleId):
        '''
        Get capture file list by filter ID
        :type webApiSession: WebApiSession
        :param ruleId: filter ID for getting capture file list
        :returns capture file list for given ruleId
        '''

        found = False
        allCaptureFiles, path = Capture.getCaptureFileList(webApiSession)
        captureFileByRuleId = []

        if not allCaptureFiles:
            print('No capture file...')
        else:
            if not isinstance(allCaptureFiles['rulesFile'], list):
                if allCaptureFiles['rulesFile'].get('ruleId') == ruleId:
                    captureFileByRuleId.append(allCaptureFiles['rulesFile'])
                    found = True
            else:
                for index in range(len(allCaptureFiles['rulesFile'])):
                    if allCaptureFiles['rulesFile'][index].get('ruleId') == ruleId:
                        captureFileByRuleId.append(allCaptureFiles['rulesFile'][index])
                        found = True

        if not found:
            raise Exception('No capture file found for filter {}'.format(ruleId))

        return captureFileByRuleId

    def startAndWaitForCaptureToFinish(self, webApiSession, bpsSession, filterId, captureTime):

        # start capture
        Capture.controlCapture(webApiSession, filterId, True)

        # wait for capture to finish or stop it if capture time expired
        for i in range(int(captureTime) + 1):
            status = Capture.captureRunningStatus(webApiSession)
            print('Waiting capture to finish for {}s'.format(i))
            if status and (i < int(captureTime)):
                time.sleep(1)
                print('\r\nsleep 1s')
            elif i >= int(captureTime):
                print('Should stop capture, elapsed for {}s'.format(i))
                Capture.controlCapture(webApiSession, filterId, False)
            else:
                break

        # stop traffic
        BpsOperations.forceStopBpsTestIfRunning(bpsSession)