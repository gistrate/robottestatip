from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.webApi.atip.Groups import Groups


class FiltersService(object):
    
    @classmethod
    def createFilter(cls, webApiSession, filterConfigList, groupId=0):
        '''
        Create the given list of filters, call rule push and wait until the rule is learned by the NPs from the group.
        Can be called for a single FilterConfig.
        
        :type webApiSession: WebApiSession
        :type filterConfig: List[FilterConfig]
        '''

        if not isinstance(filterConfigList, list):
            filterConfigList = [filterConfigList]

        shouldCheckRulesNo = True
        
        npList = Groups.getGroupNpIdList(webApiSession, groupId)
        if not npList:
            shouldCheckRulesNo = False
        
        if shouldCheckRulesNo:
            # just assume we have the same number of apps on all the NPs from the group (use the first one)
            initialRulesNo = DebugPage.getNpRulesFromDebugPage(webApiSession, npList[0])
              
        for filterConfig in filterConfigList:
            Filters.createFilter(webApiSession, filterConfig)
        Filters.rulePush(webApiSession,groupId=groupId)

        if not shouldCheckRulesNo:
            return

        # Count the new rules for this group
        totalRulesNo = initialRulesNo
        for crtFilter in filterConfigList:
            if crtFilter.groupId == groupId:
                totalRulesNo += 1

        # make sure all NPs have the new rules
        for crtNp in npList:
            DebugPage.checkNpRules(webApiSession, totalRulesNo, crtNp)
        
