from http import HTTPStatus

import jsonpickle
from atipAutoFrwk.webApi.atip.Netflow import Netflow
from atipAutoFrwk.webApi.traffic.BpsOperations import IBpsTestAction
from atipAutoFrwk.services.atip.stats.PerformanceService import PerformanceService


class VerifyDropAction(IBpsTestAction):

    def __init__(self, atipSession, env, swDropPercent=0):
        self.atipSession = atipSession
        self.env = env
        self.swDropPercent = swDropPercent

    def doAction(self):

        PerformanceService.verifyDropPercent(self.atipSession, self.env, self.swDropPercent)


    def setSwDropPercent(self, swDropPercent):
        self.swDropPercent = swDropPercent





