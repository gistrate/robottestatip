import jsonpickle
from atipAutoFrwk.data.atip.DedupHeaderType import DedupHeaderType
from atipAutoFrwk.data.atip.DedupPacketWindowType import DedupPacketWindowType
from atipAutoFrwk.services.atip.stats.ValidateDedupStatsService import ValidateDedupStatsService
from atipAutoFrwk.webApi.atip.Dedup import Dedup
from atipAutoFrwk.webApi.traffic.BpsOperations import IBpsTestAction



class VerifyDedupDropAction(IBpsTestAction):

    def __init__(self, atipSession, dropPercent=0, groupId=0):
        self.atipSession = atipSession
        self.dropPercent = dropPercent
        self.groupId = groupId
    def doAction(self):

        ValidateDedupStatsService.validateDedupVariation(self.atipSession, self.dropPercent, self.groupId)


    def setDropPercent(self, dropPercent):
        self.dropPercent = dropPercent


class VerifyDedupUnhandledPkts(IBpsTestAction):

    def __init__(self, atipSession, unhandled=0, groupId=0):
        self.atipSession = atipSession
        self.unhandled = unhandled
        self.groupId = groupId
    def doAction(self):

        ValidateDedupStatsService.checkUnhandledPckts(self.atipSession, self.unhandled, self.groupId)


    def setUnhandled(self, unhandled):
        self.unhandled = unhandled


class ChangeDedupState(IBpsTestAction):

    def __init__(self, atipSession, groupId=0):
        self.atipSession = atipSession
        self.groupId = groupId

    def doAction(self):
        print("------------> switch dedup state")

        globalConfig = Dedup.getDedupGlobalConfig(self.atipSession, self.groupId)
        print(jsonpickle.encode(globalConfig))
        # switch
        globalConfig.enabled = self.switchDedupState(globalConfig.enabled)
        Dedup.updateDedup(self.atipSession, globalConfig)
        obj1 = Dedup.getDedupGlobalConfig(self.atipSession)
        print("current dedup state: {}".format(jsonpickle.encode(obj1.enabled)))

    def switchDedupState(self, dedupState):
        if dedupState == True:
            return False
        return True

class ChangeHeaderValue(IBpsTestAction):

    def __init__(self, atipSession, groupId=0):
        self.atipSession = atipSession
        self.groupId = groupId
        self.indexList = 0
        self.header = list(map(lambda crtHeader: crtHeader.headerName, DedupHeaderType))

    def doAction(self):
        print("------------> switch dedup header value")
        globalConfig = Dedup.getDedupGlobalConfig(self.atipSession, self.groupId)
        # switch
        globalConfig.headerToIgnore = self.switchHeader()
        Dedup.updateDedup(self.atipSession, globalConfig)
        obj1 = Dedup.getDedupGlobalConfig(self.atipSession)
        print("current dedup header value: {}".format(jsonpickle.encode(obj1.headerToIgnore)))


    def switchHeader(self):
        self.indexList= (self.indexList + 1) % len(self.header)
        return self.header[self.indexList]


class ChangeExpectedDataRate(IBpsTestAction):

    def __init__(self, atipSession, groupId=0):
        self.atipSession = atipSession
        self.groupId = groupId
        self.indexList = 0
        self.packetWindow = list(map(lambda crtWindow: crtWindow.packetWindowValue, DedupPacketWindowType))

    def doAction(self):
        print("------------> switch expected data rate value")

        globalConfig = Dedup.getDedupGlobalConfig(self.atipSession, self.groupId)
        # switch
        globalConfig.packetWindow = self.switchPacketWindow()
        Dedup.updateDedup(self.atipSession, globalConfig)
        obj1 = Dedup.getDedupGlobalConfig(self.atipSession)
        print("current dedup expected rate value: {}".format(jsonpickle.encode(obj1.packetWindow)))


    def switchPacketWindow(self):
        self.indexList = (self.indexList + 1) % len(self.packetWindow)
        return self.packetWindow[self.indexList]
