from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.webApi.atip.Groups import Groups
from atipAutoFrwk.services.TestCondition import TestCondition

class GroupsService(object):
    
    @classmethod
    def clearGroups(self, webApiSession):

        # move all nps to default group
        nps = Groups.getNPList(webApiSession)
      
        for item in nps.get('np'):
            if item.get('group') != 0:
                Groups.changeNPGroup(webApiSession, item.get('id'), 0)
                self.waitUntilGroupReady(webApiSession, 0)
      
        # remove all groups
        groups = Groups.getGroups(webApiSession)
        for item in groups.get('groups'):
            if item.get('id') != 0:
                Groups.deleteGroup(webApiSession, item.get('id'))
        
    @classmethod
    def isReady(self, webApiSession, id):
        ':type webApiSession: WebApiSession'

        try:
            theJSON = Groups.getStatus(webApiSession)
        except (ConnectionError):
            return False

        try:
            for group in theJSON.get('groups'):
                if group.get('id') == id:
                    for np in group.get('npStatus'):
                        if np.get('status') != 'ready':
                            return False
                    
        except Exception:
            return False

        return True
    
    @classmethod
    def waitUntilGroupReady(self, webApiSession, id):
        isReadyLambda = lambda: self.isReady(webApiSession, id) is True
        TestCondition.waitUntilTrue(isReadyLambda, errorMessage="Group isn't in ready status!", totalTime=300, iterationTime=5)

    @classmethod
    def createNewGroup(self, webApiSession, gName):
        Groups.addGroup(webApiSession, GroupConfig(name=gName))
        id = Groups.getGroupId(webApiSession, gName)
        if id is None:
            raise Exception('Group not found')

    @classmethod
    def moveNPsToGroup(self, webApiSession, gName, npId):
        nps = Groups.getNPList(webApiSession)
        self.checkNpExists(nps.get('np'), npId)
        gid = Groups.getGroupId(webApiSession, gName)
        Groups.changeNPGroup(webApiSession, npId, gid)
        nps = Groups.getNPsInGroup(webApiSession, gid)
        self.checkNpExists(nps.get('nps'), npId)
        GroupsService.waitUntilGroupReady(webApiSession, gid)

    @classmethod
    def checkNpExists(self, npList, npId):
        if not isinstance(npList, list):
            npList = [npList]
        found = False
        for item in npList:
            if str(npId) == item.get('id'):
                found = True
                break
        if not found:
            raise Exception('NP id {} not in the current list'.format(npId))
        
    @classmethod
    def updateReference(self, obj):
        if isinstance(obj.groupId, GroupConfig):
            obj.groupId = obj.groupId.id
