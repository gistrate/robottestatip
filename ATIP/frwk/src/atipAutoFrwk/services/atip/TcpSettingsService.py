from atipAutoFrwk.webApi.atip.TcpMode import TcpMode

class TcpSettingsService(object):
    '''
    Contains methods that can be used to configure Tcp Settings.
    '''

    @classmethod
    def enableEmptyPktsForward(cls, webApiSession):
        '''
        Enable forward packets with no payload
        :param webApiSession: current session
        '''
        cfg = {"tcpMode": {"enabledAlways": False, "forwardEmptyAcks": True}}
        resp = TcpMode.updateTcpConfig(webApiSession, cfg, 0)

        return resp
