from http import HTTPStatus

import jsonpickle
from atipAutoFrwk.config.atip.NetflowCardConfig import NetflowCardConfig
from atipAutoFrwk.config.atip.Card import Card
from atipAutoFrwk.config.atip.NetflowCollectorConfig import NetflowCollectorConfig
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.webApi.atip.Netflow import Netflow


class NetflowService(object):

    @classmethod
    def getGlobalConfig(cls, webApiSession, groupId = 0):
        '''
        :param webApiSession: current session
        :return: a class item of type NetflowConfig
        '''
        theJSON = Netflow.getGlobalConfig(webApiSession=webApiSession, groupId=groupId)
        netflowConfig = ConversionService.createBasicObject(theJSON['globalConfig'], NetflowConfig)
        return netflowConfig

    @classmethod
    def getCardConfig(cls, webApiSession, expectedStatus, groupId = 0):
        '''
        :param webApiSession: current session
        :return: a class item of type NetflowCardConfig
        '''
        theJSON = Netflow.getCardConfig(webApiSession=webApiSession, expectedStatus=HTTPStatus.OK, groupId=groupId)
        netflowCardConfig = NetflowCardConfig()
        if(type(theJSON['cards']) == list):
            for i in range(len(theJSON['cards'])):
                try:
                    netflowCardConfig.cardList[int(theJSON['cards'][i]['id'])] = ConversionService.createBasicObject(theJSON['cards'][i], Card)
                except ValueError:
                    netflowCardConfig.cardList[1] = ConversionService.createBasicObject(theJSON['cards'][i], Card)
        else:
            netflowCardConfig.cardList[1] = ConversionService.createBasicObject(theJSON['cards'], Card)
        return netflowCardConfig

    @classmethod
    def getCollectorConfig(cls, webApiSession, expectedStatus=HTTPStatus.OK, groupId = 0):
        '''
        :param webApiSession: current session
        :return: a class item of type NetflowCollectorConfig
        '''
        theJSON = Netflow.getCollectorConfig(webApiSession=webApiSession, expectedStatus=HTTPStatus.OK, groupId=groupId)
        netflowCollectorConfig = NetflowCollectorConfig()
        if(len(theJSON['collectors']) > 1):
            for i in range(netflowCollectorConfig.MAXIMUM_COLLECTORS):
                for names in theJSON['collectors'][i]:
                    netflowCollectorConfig.collectorList[i].__setattr__(names, theJSON['collectors'][i][names])
        else:
            for names in theJSON['collectors']:
                print(theJSON['collectors'])
                netflowCollectorConfig.collectorList[0].__setattr__(names, theJSON['collectors'][names])
        return netflowCollectorConfig



    @classmethod
    def printCollectorConfig(cls, netflowCollectorConfig):
        for i in range(netflowCollectorConfig.MAXIMUM_COLLECTORS):
            print(vars(netflowCollectorConfig.collectorList[i]))

#    @classmethod
#    def printCardConfig(cls, netflowCardConfig):
#        for i in range(1, len(netflowCardConfig.cardList)):
#            print(vars(netflowCardConfig.cardList[i]))

    @classmethod
    def enableIxFlowFields(self, atipSession, fields, reset = True, groupId = 0):
        netflowObj = Netflow.getGlobalConfig(atipSession, groupId=groupId)
        if reset:
            netflowObj.resetIxFlowFields(netflowObj)
        print("attribute {}".format(netflowObj.__dict__.keys()))
        for key in fields:
                setattr(netflowObj, key, True)
        print("netflowObj {}".format(jsonpickle.encode(netflowObj)))
        Netflow.updateGlobalConfig(atipSession,netflowObj)

    @classmethod
    def disableIxFlowFields(self, atipSession, fields, groupId = 0):
        netflowObj = Netflow.getGlobalConfig(atipSession, groupId=groupId)
        print("attribute {}".format(netflowObj.__dict__.keys()))
        for key in fields:
            setattr(netflowObj, key, False)
        print("netflowObj {}".format(jsonpickle.encode(netflowObj)))
        Netflow.updateGlobalConfig(atipSession, netflowObj)

    @classmethod
    def changeGlobalConfig(cls, atipSession, key, groupId = 0):
        '''
        :param webApiSession: current session
        :return: a class item of type NetflowConfig
        '''
        netflowObj = Netflow.getGlobalConfig(atipSession, groupId=groupId)
        print("attribute {}".format(netflowObj.__dict__.keys()))
        setattr(netflowObj, key, False)
        Netflow.updateGlobalConfig(atipSession, netflowObj)
        setattr(netflowObj, key, True)
        Netflow.updateGlobalConfig(atipSession, netflowObj)



