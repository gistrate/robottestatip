from http import HTTPStatus
import logging
import math
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.webApi.atip.TopFilters import TopFilters
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.traffic.RealTimeStats import RealTimeStats
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.webApi.atip.TopFilters import TopFilters


class TopFiltersService(object):

    @classmethod
    def checkTopFiltersStats(cls, webApiSession, topStat, filterName, expectedValue, timeInterval=TimeInterval.HOUR, groupId=0,limit='10', rel_tol=0):
        checkStatValue = lambda : math.isclose(expectedValue, (math.floor(cls.getTopFiltersStats(
            webApiSession, topStat, filterName, timeInterval, limit=limit, strict=False, groupId=groupId)*100))/100, rel_tol=rel_tol)
        logActualValue = lambda : logging.exception('Found incorrect values for {}. Actual: {} expected: {}'.format(
            topStat,
            (math.floor((cls.getTopFiltersStats(webApiSession, topStat, filterName, timeInterval, limit=limit, strict=False, groupId=groupId)*100))/100),
            expectedValue))

        TestCondition.waitUntilTrue(checkStatValue, 'Found incorrect values for {}. Check logs for actual values'.format(topStat),
                                    onFailure=logActualValue, totalTime=StatsTimeout.TOP_FILTERS.waitTime, iterationTime=StatsTimeout.ITERATION_TIME.waitTime)  # @UndefinedVariable

        return cls.getTopFiltersStats(webApiSession, topStat, filterName, timeInterval, limit=limit, groupId=groupId)

    @classmethod
    def getTopFiltersStats(cls, webApiSession, statName, filterName, timeInterval=TimeInterval.HOUR, limit='10', strict=True, expectedStatus=HTTPStatus.OK, groupId=0):
        '''
        Keyword arguments:
        strict -- this is considered only when the requested value cannot be returned; if True, an exception is raised; otherwise None is returned
        '''
        ':type webApiSession: WebApiSession'
        ':type timeInterval: TimeInterval'
        
        statsList = TopFilters.getTopFiltersStats(webApiSession,groupId, timeInterval, limit)
        if not statsList.get('stats')[0].get('list')[0]:
            if strict:
                raise Exception('ERROR: No stats present in the Top Filters window')
            else:
                return None
        else:
            if not isinstance(statsList.get('stats')[0].get('list'), list):
                if statsList.get('stats')[0].get('list')[0].get('msg') != filterName:
                    if strict:
                        raise Exception('Filter {} wasn\'t found in the Top Filters window.'.format(filterName))
                    else:
                        return None 
                else:
                    return statsList.get('stats')[0].get('list')[0].get(statName.statType)

            else:
                if len([element.get('msg') for element in statsList.get('stats')[0].get('list') if
                        element.get('msg') == filterName]) == 0:
                    if strict:
                        raise Exception('Filter {} wasn\'t found in the Top Filters window.'.format(filterName))
                    else:
                        return None
                else:
                    return [element.get(statName.statType) for element in statsList.get('stats')[0].get('list') if element.get('msg') == filterName][0]

    @classmethod
    def checkFilterExistsInTopStats(cls, atipSession, filterConfig, timeInterval,
                                    dashboardType=DashboardType.MINIMIZED):
        checkFilterExists = lambda: filterConfig.name in str(
            TopFilters.getTopFiltersStats(atipSession, timeInterval=timeInterval, limit=dashboardType.statsLimit,
                                          groupId=filterConfig.groupId))
        TestCondition.waitUntilTrue(checkFilterExists, 'Couldn\'t find filter: {}'.format(filterConfig.name),
                                    totalTime=StatsTimeout.TOP_FILTERS.waitTime,
                                    iterationTime=StatsTimeout.ITERATION_TIME.waitTime)  # @UndefinedVariable
        print('Found filter {} in Top Filters dashboard {}'.format(filterConfig.name, dashboardType))

    @classmethod
    def checkFilterDoesNotExistsInTopStats(cls, atipSession, filterConfig, timeInterval,
                                           dashboardType=DashboardType.MINIMIZED):
        checkFilterExists = lambda: filterConfig.name not in str(
            TopFilters.getTopFiltersStats(atipSession, timeInterval=timeInterval, limit=dashboardType.statsLimit,
                                          groupId=filterConfig.groupId))
        TestCondition.waitUntilTrue(checkFilterExists, 'Could find filter: {}'.format(filterConfig.name),
                                    totalTime=StatsTimeout.TOP_FILTERS.waitTime,
                                    iterationTime=StatsTimeout.ITERATION_TIME.waitTime)  # @UndefinedVariable
    @classmethod
    def checkAtipTopStatsTotalSessions(cls, atipSession, filterConfig, bpsTotalSessions, timeInterval, groupId=0,
                                       dashboardType=DashboardType.MINIMIZED, rel_tol=0):
        TopFiltersService.checkTopFiltersStats(atipSession, TopStats.TOTAL_COUNT, filterConfig.name, bpsTotalSessions,
                                               timeInterval, groupId=groupId, limit=dashboardType.statsLimit, rel_tol=rel_tol)

    @classmethod
    def checkAtipTopStatsShare(cls, atipSession, filterConfig, expectedValue, timeInterval,
                               dashboardType=DashboardType.MINIMIZED):
        TopFiltersService.checkTopFiltersStats(atipSession, TopStats.SHARE, filterConfig.name, expectedValue,
                                               timeInterval, limit=dashboardType.statsLimit)

    @classmethod
    def checkAtipTopStatsTotalShare(cls, atipSession, groupId=0, timeInterval=0,
                                    dashboardType=DashboardType.MINIMIZED):
        statsList = TopFilters.getTopFiltersStats(atipSession, groupId=groupId, timeInterval=timeInterval,
                                                  limit=dashboardType.statsLimit)
        myList = []
        for filter in statsList.get('stats')[0].get('list'):
            item = TopFiltersService.getTopFiltersStats(atipSession, TopStats.SHARE, filter.get('msg'),
                                                        limit=dashboardType.statsLimit)
            print(item)
            myList.append(float(item))
        TestCondition.computeShare(myList)

    @classmethod
    def checkAtipTopStatsTotalClientBytes(cls, atipSession, filterConfig, bpsTestConfig, timeInterval,
                                          dashboardType=DashboardType.MINIMIZED):
        clientBytes = TopFiltersService.getTopFiltersStats(atipSession, TopStats.CLIENT_BYTES, filterConfig.name,
                                                           timeInterval, limit=dashboardType.statsLimit)
        sessions = TopFiltersService.getTopFiltersStats(atipSession, TopStats.TOTAL_COUNT, filterConfig.name,
                                                        timeInterval, limit=dashboardType.statsLimit)
        expectedNoBytes = (
                          bpsTestConfig.requestPacketValues.synSize + bpsTestConfig.requestPacketValues.getSize + bpsTestConfig.requestPacketValues.finAckSize) * sessions
        BpsStatsService.compareConvertedBytes(clientBytes, expectedNoBytes)

    @classmethod
    def checkAtipTopStatsTotalClientPkts(cls, atipSession, filterConfig, timeInterval,
                                         dashboardType=DashboardType.MINIMIZED):
        dashboardPkts = TopFiltersService.getTopFiltersStats(atipSession, TopStats.TOTAL_PKTS, filterConfig.name,
                                                             timeInterval, limit=dashboardType.statsLimit)
        expectedValue = int(dashboardPkts / 2)
        TopFiltersService.checkTopFiltersStats(atipSession, TopStats.CLIENT_PKTS, filterConfig.name, expectedValue,
                                               limit=dashboardType.statsLimit)

    @classmethod
    def checkAtipTopStatsTotalServerBytes(cls, atipSession, filterConfig, bpsTestConfig, timeInterval,
                                          dashboardType=DashboardType.MINIMIZED):
        serverBytes = TopFiltersService.getTopFiltersStats(atipSession, TopStats.SERVER_BYTES, filterConfig.name,
                                                           timeInterval, limit=dashboardType.statsLimit)
        sessions = TopFiltersService.getTopFiltersStats(atipSession, TopStats.TOTAL_COUNT, filterConfig.name,
                                                        timeInterval, limit=dashboardType.statsLimit)
        expectedNoBytes = (
                          bpsTestConfig.requestPacketValues.synSize + bpsTestConfig.requestPacketValues.okSize + bpsTestConfig.requestPacketValues.finAckSize) * sessions
        BpsStatsService.compareConvertedBytes(serverBytes, expectedNoBytes)

    @classmethod
    def checkAtipTopStatsTotalServerPkts(cls, atipSession, filterConfig, timeInterval,
                                         dashboardType=DashboardType.MINIMIZED):
        dashboardPkts = TopFiltersService.getTopFiltersStats(atipSession, TopStats.TOTAL_PKTS, filterConfig.name,
                                                             timeInterval, limit=dashboardType.statsLimit)
        expectedValue = int(dashboardPkts / 2)
        TopFiltersService.checkTopFiltersStats(atipSession, TopStats.SERVER_PKTS, filterConfig.name, expectedValue,
                                               limit=dashboardType.statsLimit)

    @classmethod
    def checkAtipTopStatsTotalBytes(cls, atipSession, filterConfig, bpsSession, bpsTestConfig, timeInterval,
                                    dashboardType=DashboardType.MINIMIZED):
        dashboardBytes = TopFiltersService.getTopFiltersStats(atipSession, TopStats.TOTAL_BYTES, filterConfig.name,
                                                              timeInterval, limit=dashboardType.statsLimit)
        dashboardPkts = TopFiltersService.getTopFiltersStats(atipSession, TopStats.TOTAL_PKTS, filterConfig.name,
                                                             timeInterval, limit=dashboardType.statsLimit)
        bpsTotalBytes = BpsStatsService.getL7RealTimeStatsByName(bpsSession, bpsTestConfig.testId,
                                                                 RealTimeStats.APP_TX_FRAME_DATA)
        bpsTotalPkts = BpsStatsService.getRealTimeStatsByName(bpsSession, bpsTestConfig.testId,
                                                              RealTimeStats.ETH_TX_FRAMES)
        noACKsPkts = bpsTotalPkts - dashboardPkts
        noACKsBytes = noACKsPkts * 66
        expectedNoBytes = bpsTotalBytes - noACKsBytes
        BpsStatsService.compareConvertedBytes(dashboardBytes, expectedNoBytes)

    @classmethod
    def checkAtipTopStatsTotalPkts(cls, atipSession, filterConfig, timeInterval,
                                   dashboardType=DashboardType.MAXIMIZED):
        dashboardPkts = TopFiltersService.getTopFiltersStats(atipSession, TopStats.TOTAL_PKTS, filterConfig.name,
                                                             timeInterval, limit=dashboardType.statsLimit)
        sessions = TopFiltersService.getTopFiltersStats(atipSession, TopStats.TOTAL_COUNT, filterConfig.name,
                                                        timeInterval, limit=dashboardType.statsLimit)
        clientPkts = TopFiltersService.getTopFiltersStats(atipSession, TopStats.CLIENT_PKTS, filterConfig.name,
                                                          timeInterval, limit=dashboardType.statsLimit)
        serverPkts = TopFiltersService.getTopFiltersStats(atipSession, TopStats.SERVER_PKTS, filterConfig.name,
                                                          timeInterval, limit=dashboardType.statsLimit)
        tcpPkts = sessions * 6
        totalPkts = int(clientPkts) + int(serverPkts)
        if (dashboardPkts != tcpPkts) or (dashboardPkts != totalPkts):
            raise Exception(
                'The total number of packets {} is not correct, expected values is {}.'.format(dashboardPkts, tcpPkts))
        else:
            return dashboardPkts
