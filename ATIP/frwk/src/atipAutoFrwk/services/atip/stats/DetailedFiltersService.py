
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.webApi.atip.stats.Filters import Filters as FilterStats


class DetailedFiltersService(object):

    @classmethod
    def checkAppInFilterDetailedStats(cls, atipSession, theFilter, deviceType, expectedValue, timeInterval):
        results = {}
        i = 0
        atipDetailedStatsByFilter = FilterStats.getDetailedStats(atipSession, timeInterval=timeInterval,
                                                                     limit=DashboardType.MAXIMIZED.stringLimit, ruleID=theFilter.id,
                                                                     device=deviceType, groupId=theFilter.groupId)
        if atipDetailedStatsByFilter.getList() == []:
            raise ValueError("The detailed Filter is not populated.")
        for item in atipDetailedStatsByFilter.getList():
            results[i] = item.getAppName() == expectedValue
            i += 1
        if list(results.values()).count(True) == len(results):
            return True
        else:
            raise ValueError("Not all rows contain a valid app")
        return True