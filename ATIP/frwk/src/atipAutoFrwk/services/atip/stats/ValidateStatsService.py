from cmath import isclose
from http import HTTPStatus

import itertools
import jsonpickle
import logging

from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.atip.DashboardType import DashboardType
from atipAutoFrwk.data.atip.FilteredTrafficStats import FilteredTrafficStats
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.data.atip.StatsTimeout import StatsTimeout
from atipAutoFrwk.data.atip.StatsType import StatsType
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.TimeValues import TimeValues
from atipAutoFrwk.data.atip.TopStats import TopStats
from atipAutoFrwk.data.atip.stats.MapsStatsContainer import MapsStatsContainer
from atipAutoFrwk.data.atip.stats.MapsStatsItem import MapsStatsItem
from atipAutoFrwk.data.atip.stats.PieStatsContainer import PieStatsContainer
from atipAutoFrwk.data.atip.stats.PieStatsItem import PieStatsItem
from atipAutoFrwk.data.atip.stats.TopStatsContainer import TopStatsContainer
from atipAutoFrwk.data.traffic.stats.BpsStatsType import BpsStatsType
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.services.atip.stats.StatsService import StatsService
from atipAutoFrwk.services.traffic.BpsComponentStatsService import BpsComponentStatsService
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.webApi.atip.Status import Status
from atipAutoFrwk.webApi.atip.stats.Lists import Lists
from atipAutoFrwk.webApi.atip.stats.Filters import Filters as FilterStats
from atipAutoFrwk.webApi.atip.stats.Pie import Pie
from atipAutoFrwk.webApi.atip.stats.Ports import Ports
from atipAutoFrwk.webApi.traffic.BpsStats import BpsStats
from atipAutoFrwk.webApi.atip.stats.Pie import Pie as PieStats


class ValidateStatsService(object):
    atipStats = dict()

    def __init__(self):
        pass

    @classmethod
    def setAtipStats(cls, interval, stats, crtDict = None):
        if not crtDict:
            crtDict = dict()
        crtDict[interval] = stats
        return crtDict

    @classmethod
    def getAtipStats(cls, interval, crtDict):
        return crtDict[interval]

    @classmethod
    def getKeyValue(cls, atipStat):

        if MapsStatsItem == type(atipStat):
            return atipStat.countryName
        elif PieStatsItem == type(atipStat):
            return atipStat.name.lower()
        else:
            return atipStat.msg

    @classmethod
    def getAtipStatsValues(cls, atipStats, target, statName):
        if type(atipStats) == TopStatsContainer:
            listStats = atipStats.getList()
        elif type(atipStats) == PieStatsContainer:
            listStats = atipStats.getList()
        elif type(atipStats) == MapsStatsContainer:
            listStats = atipStats.getList()
        else:
            listStats = atipStats
        print("the items: {}, target {}".format(listStats, target))
        for atipStat in listStats:
            if cls.getKeyValue(atipStat) == target:
                stat = getattr(atipStat, statName.statType)
                return stat

    @staticmethod
    def checkShare(currentAtipApp, currentBPSStats, totalBytes):

        share = currentAtipApp.totalBytes / totalBytes
        return isclose(currentAtipApp.getShare(), share, rel_tol=0.05)

    @classmethod
    def checkAtipStatsValues(cls, atipStats, target, statName, expectedValue):
        print("atipStats: {}\ntarget: {}\n statName:{}\n".format(atipStats, target, statName))
        stat = ValidateStatsService.getAtipStatsValues(atipStats, target, statName)
        if stat == None:
            stat = 0
        if expectedValue == None:
            expectedValue = 0
        print('ATIP {}: actual {} vs expected: {}'.format(statName, stat, expectedValue))
        # valueOK = isclose(stat, expectedValue, rel_tol=0.01)
        valueOK = isclose(stat, expectedValue, rel_tol=0.0)
        if not valueOK:
            raise ValueError('ATIP {}: actual {} vs expected: {}'.format(statName, stat, expectedValue))
        return valueOK

    @classmethod
    def checkAtipListStats(cls, atipStats, target, statName, expectedValue):
        checkStatValue = lambda: ValidateStatsService.checkAtipStatsValues(atipStats, target, statName, expectedValue)
        logActualValue = lambda: logging.exception(
            'Found incorrect values for {}. Expected: {}. Actual stats: {}. '.format(
                statName,
                jsonpickle.encode(expectedValue),
                ValidateStatsService.getAtipStatsValues(atipStats, target, statName)
            ))

        TestCondition.waitUntilTrue(checkStatValue,
                                    'Found incorrect values for {}. Check logs for actual values'.format(target),
                                    onFailure=logActualValue)
        return True

    @classmethod
    def checkSpecificFieldStat(cls, atipStatsByApp, bpsTestConfig, bpsComponentsStats, atipSession, statType, stat):

        result = dict()
        finalResult = dict()
        print('STAT {}'.format(stat))
        for component in bpsTestConfig.appSimComponents.values():
            print("The appsim config name and type: {}, {}".format(component.name, component.appType.appName))
            # The following lines return the first occurence of the atip dynamic app that matches the name
            # in the bpsConfig, or None if no such app was found

            if (statType == StatsType.PROVIDER):

                print("start of for:")
                for item in atipStatsByApp.getList():
                    print("item: {}".format(item))
                    print("item: {}".format(jsonpickle.encode(item)))
                    print("First item: {}".format(item.getName().lower()))
                    print("Second item: {}".format(component.serviceProvider.fullname.lower()))

                currentAtipApp = next(
                    x for x in atipStatsByApp.getList() if
                    x.getName().lower() == component.serviceProvider.fullname.lower())
            elif statType == StatsType.GEO:
                currentAtipApp = next((x for x in atipStatsByApp.getList() if x.getCountry().lower() ==
                                       component.clientLocation.countryName.lower()))

            else:
                currentAtipApp = next((x for x in atipStatsByApp.getList() if x.getName().lower() ==
                                       component.appType.appName.lower() or
                                       x.getName() in component.appType.appName.lower() or
                                       component.appType.appName.lower() in x.getName()), None)

            if (stat == TopStats.CLIENT_PKTS):
                clientPktsOK = cls.checkAtipAppSidePktsPerItem(atipStatsByApp, currentAtipApp.getName(),
                                                               bpsComponentsStats[component.name],
                                                               TopStats.CLIENT_PKTS)
                finalResult[component.name] = clientPktsOK
            elif (stat == TopStats.SERVER_PKTS):
                serverPktsOK = cls.checkAtipAppSidePktsPerItem(atipStatsByApp, currentAtipApp.getName(),
                                                               bpsComponentsStats[component.name],
                                                               TopStats.SERVER_PKTS)
                finalResult[component.name] = serverPktsOK
            elif (stat == TopStats.TOTAL_BYTES):
                totalBytesOK = cls.checkAtipTotalBytesPerItem(currentAtipApp, bpsComponentsStats[component.name])
                finalResult[component.name] = totalBytesOK
            elif (stat == TopStats.TOTAL_PKTS):
                sessions = getattr(bpsComponentsStats[component.name], BpsStatsType.SESSIONS.statsName)
                totalPktsOK = cls.checkAtipTopStatsTotalPkts(atipStatsByApp, bpsTestConfig, currentAtipApp.getName(),
                                                             sessions, stat)
                finalResult[component.name] = totalPktsOK
            elif (stat == TopStats.TOTAL_COUNT):
                sessions = getattr(bpsComponentsStats[component.name], BpsStatsType.SESSIONS.statsName)
                totalCountOK = cls.checkAtipListStats(atipStatsByApp, currentAtipApp.getName(),
                                                      TopStats.TOTAL_COUNT, sessions)
                finalResult[component.name] = totalCountOK

            elif (stat == TopStats.CLIENT_BYTES):
                result[component.name] = dict()
                result[component.name][TopStats.CLIENT_BYTES] = cls.computeClientBytesPerItem(component)
                clientBytesOK = cls.checkTopStatsSideBytesPerItem(currentAtipApp, bpsComponentsStats[component.name],
                                                                  result,
                                                                  component, TopStats.CLIENT_BYTES)
                finalResult[component.name] = clientBytesOK

            elif (stat == TopStats.SERVER_BYTES):
                result[component.name] = dict()
                result[component.name][TopStats.SERVER_BYTES] = cls.computeServerBytesPerItem(component)
                serverBytesOK = cls.checkTopStatsSideBytesPerItem(currentAtipApp, bpsComponentsStats[component.name],
                                                                  result,
                                                                  component, TopStats.SERVER_BYTES)
                finalResult[component.name] = serverBytesOK

            elif (stat == TopStats.DISCOVERY):
                theRealDate = Status.getStatus(atipSession)

                discoveryOK = cls.checkDate(currentAtipApp, theRealDate)
                result[component.name] = discoveryOK

            elif (stat == TopStats.AS_NUMBER):
                asNumberOK = cls.checkAtipAsNumberPerItem(component, currentAtipApp)
                finalResult[component.name] = asNumberOK

            elif (stat == TopStats.SHARE):
                totalBytes = cls.getAtipTotalBytes(atipStatsByApp)
                shareOK = cls.checkShare(currentAtipApp, bpsComponentsStats[component.name], totalBytes)
                finalResult[component.name] = shareOK
            else:
                raise Exception("Stat unknown!")
        print('Final result {}'.format(finalResult.values()))

        return sum(x == True for x in finalResult.values()) == len(finalResult)

    @classmethod
    def checkAtipTopStatsTotalPkts(cls, atipStats, bpsTestConfig, target, sessions, statsType=StatsType.APPS):
        dashboardPkts = ValidateStatsService.getAtipStatsValues(atipStats, target, TopStats.TOTAL_PKTS)
        count = 3
        for component, appSimConfig in bpsTestConfig.appSimComponents.items():
            if target == appSimConfig.clientLocation.countryName and target == appSimConfig.serverLocation.countryName or statsType != StatsType.GEO:
                count = 6
        geoPkts = sessions * count
        if (dashboardPkts != geoPkts):
            raise Exception(
                'The total number of packets {} is not correct, expected values is {}.'.format(dashboardPkts, geoPkts))
        else:
            return True

    @classmethod
    def checkAtipTopStatsClientPkts(cls, atipStats, bpsTestConfig, bpsComponentsStats, target):
        bpsStatsByClients = BpsComponentStatsService.filterStatsByClientLocation(bpsTestConfig, bpsComponentsStats,
                                                                                 target)
        bpsSessions = BpsComponentStatsService.sumStatItem(bpsStatsByClients, BpsStatsType.SESSIONS)
        count = 3
        expectedValue = bpsSessions * count
        ValidateStatsService.checkAtipListStats(atipStats, target, TopStats.CLIENT_PKTS, expectedValue)

    @classmethod
    def checkAtipTopStatsServerPkts(cls, atipStats, bpsTestConfig, bpsComponentsStats, target):
        bpsStatsByServers = BpsComponentStatsService.filterStatsByServerLocation(bpsTestConfig, bpsComponentsStats,
                                                                                 target)
        bpsSessions = BpsComponentStatsService.sumStatItem(bpsStatsByServers, BpsStatsType.SESSIONS)
        count = 3
        expectedValue = bpsSessions * count
        ValidateStatsService.checkAtipListStats(atipStats, target, TopStats.SERVER_PKTS, expectedValue)

    @classmethod
    def checkAtipAppSidePktsPerItem(cls, atipStats, target, currentBPSStats, side):

        bpsSession = getattr(currentBPSStats, BpsStatsType.SESSIONS.statsName)
        count = 3
        expectedValue = bpsSession * count
        ValidateStatsService.checkAtipListStats(atipStats, target, side, expectedValue)
        return True

    @classmethod
    def checkAtipTopStatsTotalBytes(cls, atipStats, bpsTestConfig, bpsComponentsStats, target):
        bpsServerBytes = ValidateStatsService.getServerBytes(bpsTestConfig, bpsComponentsStats, target)
        bpsClientBytes = ValidateStatsService.getClientBytes(bpsTestConfig, bpsComponentsStats, target)
        bpsTotalBytes = bpsClientBytes + bpsServerBytes
        atipTotalBytes = ValidateStatsService.getAtipStatsValues(atipStats, target, TopStats.TOTAL_BYTES)
        BpsStatsService.compareConvertedBytes(atipTotalBytes, bpsTotalBytes)

    @classmethod
    def checkAtipTotalBytesPerItem(cls, currentAtipApp, currentBPSStats):
        ':type currentAtipApp TopStatsItem'
        bpsBytes = getattr(currentBPSStats, BpsStatsType.BYTES.statsName)
        bpsPackets = getattr(currentBPSStats, BpsStatsType.PACKETS.statsName)
        atipPackets = getattr(currentAtipApp, TopStats.TOTAL_PKTS.statType)
        totalBytes = getattr(currentAtipApp, TopStats.TOTAL_BYTES.statType)
        expectedBytes = StatsService.getBytesForAckPacketLoss(bpsBytes, bpsPackets, atipPackets)
        if expectedBytes != totalBytes:
            raise ValueError("Total bytes differ: {} vs. {}".format(totalBytes, expectedBytes))
        return True

    @classmethod
    def checkAtipTopStatsClientBytes(cls, atipStats, bpsTestConfig, bpsComponentsStats, target):
        atipClientBytes = ValidateStatsService.getAtipStatsValues(atipStats, target, TopStats.CLIENT_BYTES)
        bpsClientBytes = ValidateStatsService.getClientBytes(bpsTestConfig, bpsComponentsStats, target)
        BpsStatsService.compareConvertedBytes(atipClientBytes, bpsClientBytes)

    @classmethod
    def checkAtipTopStatsServerBytes(cls, atipStats, bpsTestConfig, bpsComponentsStats, target):
        atipServerBytes = ValidateStatsService.getAtipStatsValues(atipStats, target, TopStats.SERVER_BYTES)
        bpsServerBytes = ValidateStatsService.getServerBytes(bpsTestConfig, bpsComponentsStats, target)
        BpsStatsService.compareConvertedBytes(atipServerBytes, bpsServerBytes)

    @classmethod
    def computeTotalClientBytes(cls, bpsTestConfig):
        '''
        Create a new dict that contains the client bytes for each app component.
        '''
        filteredResult = dict()

        for component, appSimConfig in bpsTestConfig.appSimComponents.items():
            filteredResult[component] = ValidateStatsService.computeClientBytesPerItem(appSimConfig)
            print('component {} and bytes {}'.format(component, filteredResult[component]))

        return filteredResult

    @classmethod
    def computeClientBytesPerItem(cls, appSimConfig):

        return appSimConfig.requestPacketValues.synSize + appSimConfig.requestPacketValues.getSize + \
               appSimConfig.requestPacketValues.finAckSize

    @classmethod
    def getClientBytes(cls, bpsTestConfig, bpsComponentsStats, target):
        '''
        Return the total number of BPS client bytes.
        '''
        bpsStatsClient = BpsComponentStatsService.filterStatsByClientLocation(bpsTestConfig, bpsComponentsStats,
                                                                              target)
        bpsClientBytes = ValidateStatsService.computeTotalClientBytes(bpsTestConfig)
        statSum = 0

        for component, componentStats in bpsStatsClient.items():
            sessions = getattr(componentStats, BpsStatsType.SESSIONS.statsName)
            client = bpsClientBytes[component]
            total = sessions * client
            print('Component: {}, sessions: {}, clientBytes per session: {}, clientBytes per component {}'.format(
                component, sessions, client, total))
            statSum += total

        return statSum

    @classmethod
    def checkTopStatsSideBytesPerItem(cls, currentAtipApp, currentBPSStats, sideDict, component, side):

        sessions = getattr(currentBPSStats, BpsStatsType.SESSIONS.statsName)
        sideBytes = sideDict[component.name][side]
        expectedValue = sessions * sideBytes
        print("SS: {} SB: {}".format(sessions, sideBytes))
        atipValue = getattr(currentAtipApp, side.statType)
        if expectedValue != atipValue:
            raise ValueError("Total bytes differ: {} vs. {}".format(atipValue, expectedValue))
        return True

    @classmethod
    def computeTotalServerBytes(cls, bpsTestConfig):
        '''
        Create a new dict that contains the server bytes for each app component.
        '''
        filteredResult = dict()

        for component, appSimConfig in bpsTestConfig.appSimComponents.items():
            filteredResult[
                component] = ValidateStatsService.computeServerBytesPerItem(appSimConfig)
        return filteredResult

    @classmethod
    def computeServerBytesPerItem(cls, appSimConfig):

        return appSimConfig.requestPacketValues.synSize + appSimConfig.requestPacketValues.okSize + \
               appSimConfig.requestPacketValues.finAckSize

    @classmethod
    def getServerBytes(cls, bpsTestConfig, bpsComponentsStats, target):
        '''
        Return the total number of BPS server bytes.
        '''
        bpsStatsServer = BpsComponentStatsService.filterStatsByServerLocation(bpsTestConfig, bpsComponentsStats,
                                                                              target)
        bpsServerBytes = ValidateStatsService.computeTotalServerBytes(bpsTestConfig)
        statSum = 0
        for component, componentStats in bpsStatsServer.items():
            sessions = getattr(componentStats, BpsStatsType.SESSIONS.statsName)
            server = bpsServerBytes[component]
            total = sessions * server
            print('Component: {}, sessions: {}, serverBytes per session: {}, serverBytes per component {}'.format(
                component,
                sessions,
                server,
                total))
            statSum += total

        return statSum

    @classmethod
    def checkDate(cls, currentAtipApp, theRealDate, toleratedTimeDelta=TimeValues.TEN_MINUTES):
        ':type currentAtipApp TopStatsItem'
        ':param ToleratedTimeDelta: value given in seconds'

        theRealDate = theRealDate['status']['currentTime']
        theRealDate = theRealDate // 1000
        atipDate = int(
            currentAtipApp.ts // 1000)  # cutting the last 3 digits from timestamp as they represent miliseconds
        if not isclose(atipDate, theRealDate, abs_tol=toleratedTimeDelta.value):
            print("Dates differ: {} vs. {}".format(atipDate, theRealDate))
            return False
        return True

    @staticmethod
    def checkAtipTopStatsTotalShare(atipStats):
        myList = []
        for atipStat in atipStats.list:
            item = atipStat.share
            myList.append(float(item))
        TestCondition.computeShare(myList)

    @staticmethod
    def getAtipTotalBytes(atipStats):
        totalBytes = 0
        for app in atipStats.getList():
            totalBytes += app.totalBytes
        return totalBytes

    @staticmethod
    def getAtipTotalCount(atipStats):
        totalCount = 0
        for app in atipStats.getList():
            print("Total count now: {}".format(totalCount))
            totalCount += getattr(app, TopStats.TOTAL_COUNT.statType)
        print("Total count after for: {}".format(totalCount))
        return totalCount

    @staticmethod
    def getAtipStat(atipStats, statType):
        total = 0
        for app in atipStats.getList():
            total += getattr(app, statType.statType)
        return total

    @classmethod
    def checkTargetExistsInTopStats(cls, atipStats, target):

        if target not in str(jsonpickle.encode(atipStats)):
            raise Exception('Did not find target {}'.format(target))

        print('Top stats were: {}'.format(str(jsonpickle.encode(atipStats))))
        return True

    @classmethod
    def checkTargetDoesNotExistInTopStats(cls, atipStats, target):

        if target in str(jsonpickle.encode(atipStats)):
            raise Exception('Found target {} when it should not have been found'.format(target))

        print('Did not find target {} '.format(target))
        print('Top stats were: {}'.format(str(jsonpickle.encode(atipStats))))
        return True

    @classmethod
    def checkAtipAsNumberPerItem(cls, component, currentAtipApp):
        asNumberFromATIP = currentAtipApp.asNumber
        country = component.getKeyName(StatsType.GEO)
        return cls.checkASByIP(asNumberFromATIP, country)

    @classmethod
    def compareSessionRatePerItem(cls, atipSession, bpsContainer, bpsComponentStats, bpsTestConfig, tolerance=0.15):

        bpsTime = float(bpsContainer.time)
        for component, values in bpsTestConfig.appSimComponents.items():
            currentAppType = values.getKeyName(StatsType.APPTYPE)
            currentAtipStatsByApp = Ports.getTopStats(atipSession, StatsType.TRAFFIC, timeInterval=TimeInterval.HOUR,
                                                      limit=DashboardType.MINIMIZED.stringLimit, appType=currentAppType)
            bpsSessions = getattr(bpsComponentStats[component], BpsStatsType.SESSIONS.statsName)
            # bpsBytes = getattr(bpsComponentStats[component], BpsStatsType.BYTES.statsName)
            bpsRate = bpsSessions / bpsTime
            # bpsBitRate = bpsBytes * 8 / bpsTime
            serverBytes = ValidateStatsService.computeServerBytesPerItem(values)
            clientBytes = ValidateStatsService.computeClientBytesPerItem(values)
            bpsBitRate = (serverBytes+clientBytes) * 8 * bpsRate
            bitsData = currentAtipStatsByApp.getList()[0]
            sessionsData = currentAtipStatsByApp.getList()[1]
            sessionRateOK = cls.checkRateFromList(sessionsData, bpsRate,tolerance)
            bitsRateOK = cls.checkRateFromList(bitsData, bpsBitRate,tolerance)

            print("SessionsOK: {}\nBitsOk: {}".format(sessionRateOK, bitsRateOK))
            if (sessionRateOK, bitsRateOK) != (True, True):
                raise Exception("Different values")
        return sessionRateOK, bitsRateOK

    @classmethod
    def compareSessionRate(cls, atipStatsByApp, bpsContainer, bpsComponentStats, tolerance=0.1):

        atipData = atipStatsByApp.getList()
        bpsTime = float(bpsContainer.time)
        bpsSessions = 0
        bpsBytes = 0
        for component, values in bpsComponentStats.items():
            print("For {}: ".format(component))
            bpsSessions += getattr(values, BpsStatsType.SESSIONS.statsName)
            bpsBytes += getattr(values, BpsStatsType.BYTES.statsName)
        bpsRate = bpsSessions / bpsTime

        bpsBitRate = bpsBytes * 8 / bpsTime
        print("bpsRate: {}".format(bpsRate))
        print("bpsBitRate: {}".format(bpsBitRate))
        bitsData = atipData[0]
        sessionsData = atipData[1]
        print("bitsData: {}:".format(bitsData))
        print("sessionsData: {}".format(sessionsData))
        sessionRateOK = cls.checkRateFromList(sessionsData, bpsRate, tolerance)
        bitsRateOK = cls.checkRateFromList(bitsData, bpsBitRate, tolerance)
        print("SessionsOK: {}\nBitsOk: {}".format(sessionRateOK, bitsRateOK))
        if (sessionRateOK, bitsRateOK) != (True, True):
            raise Exception("Different values")
        return sessionRateOK, bitsRateOK

    @classmethod
    def checkPieStatsValues(cls, atipSession, statsType, bpsBytesStats, totalBytes, timeInterval):
        atipStats = Pie.getTopStats(atipSession, statsType, timeInterval)
        for atipStat in atipStats.data:
            share = bpsBytesStats[atipStat.name] / totalBytes
            return isclose(atipStat.y, share, rel_tol=0.1)

    @staticmethod
    def removeIntervalHeads(q):
        q.pop()
        q.pop(0)

    @classmethod
    def checkPieStats(cls, atipSession, statsType, bpsBytesStats, totalBytes, timeInterval):
        checkStatValue = lambda: cls.checkPieStatsValues(atipSession, statsType, bpsBytesStats, totalBytes,
                                                         timeInterval)
        logActualValue = lambda: logging.exception('Found incorrect values for {}. Actual: {} expected: {}'.format(
            statsType,
            jsonpickle.encode(Pie.getTopStats(atipSession, statsType)),
            jsonpickle.encode(bpsBytesStats)))
        TestCondition.waitUntilTrue(checkStatValue,
                                    'Found incorrect values for {}. Check logs for actual values'.format(statsType),
                                    onFailure=logActualValue)

    @classmethod
    def checkRateFromList(cls, dataValues, rate, tolerance):

        q = []
        atLeastOneItem = False
        for position, item in reversed(list(enumerate(dataValues.data))):
            # Reversed the list to avoid the isolated item appearing from BPS

            if item != 0:
                atLeastOneItem = True
                q.append(item)
        print("q: {}".format(q))

        if len(q) > 2:
            cls.removeIntervalHeads(q)

        if q != None:
            results = sum(q) / len(q)
        print("ATIP rate {}, BPS rate {}".format(results, rate))
        finalResult = isclose(results, rate, rel_tol=tolerance) and atLeastOneItem
        if not finalResult:
            print("ATIP rate {}, BPS rate {}".format(results, rate))
        return isclose(results, rate, rel_tol=tolerance) and atLeastOneItem



    @classmethod
    def checkServerPackets(cls, atipServerStatsByFilter):

        for filterItem in atipServerStatsByFilter.getList():
            currentPackets = getattr(filterItem, TopStats.SERVER_PKTS.statType)
            currentSessions = getattr(filterItem, TopStats.TOTAL_COUNT.statType)
            if currentPackets != 3 * currentSessions:
                print("Values differ at item {}: packets: {} with sessions: {}".format(filterItem.getName(),
                                                                                       currentPackets, currentSessions))

                raise Exception("Values differ at item {}: packets: {} with sessions: {}".format(filterItem.getName(), currentPackets, currentSessions))

        return True

    @classmethod
    def checkClientPackets(cls, atipClientStatsByFilter):

        for filterItem in atipClientStatsByFilter.getList():
            currentPackets = getattr(filterItem, TopStats.SERVER_PKTS.statType)
            currentSessions = getattr(filterItem, TopStats.TOTAL_COUNT.statType)
            if currentPackets != 3 * currentSessions:
                raise Exception(
                "Values differ at item {}: packets: {} with sessions: {}".format(filterItem.getName(), currentPackets,
                                                                                 currentSessions))

        return True

    @classmethod
    def checkFilterStat(cls, atipTotalCount, bpsComponentsStats, appSimList, statType):

        for appSim in appSimList:
            bpsTotalCount = getattr(bpsComponentsStats[appSim], statType.statsName)
            if not isclose(atipTotalCount, bpsTotalCount, rel_tol=0.05):
                raise ValueError("Total count differ: ATIP: {} vs. BPS: {}".format(atipTotalCount, bpsTotalCount))
        return True

    @classmethod
    def checkFilterServerBytes(cls, atipServerStatsByFilter, bpsTestConfig, appSimList):

        '''

        :param atipServerStatsByFilter: ATIP Server window filter TopStatsContainer
        :param bpsTestConfig: the test config
        :param appSimList: the list of app sims corresponding to the filter applied (e.g. if netflix and facebook were
                in the traffic filter, their appsims were in the appSimList
        :return:
        '''
        print("Server Bytes:")
        serverPacketsValue = 0
        for appSim in appSimList:
            currentAppSim = bpsTestConfig.appSimComponents[appSim]
            serverPacketsValue += ValidateStatsService.computeServerBytesPerItem(currentAppSim)

        for filterItem in atipServerStatsByFilter.getList():
            currentSessions = getattr(filterItem, TopStats.TOTAL_COUNT.statType)
            currentBytes = getattr(filterItem, TopStats.SERVER_BYTES.statType)
            expectedValue = serverPacketsValue * currentSessions

            print("Sessions: {}\nServerPackets: {}\nValue: {}".format(currentSessions, currentBytes, expectedValue))
            if not isclose(currentBytes, expectedValue, rel_tol=0.01):
                raise ValueError(
                    "Server bytes differ: current: {} vs. expected: {}".format(currentBytes, expectedValue))

        return True

    @classmethod
    def checkFilterClientBytes(cls, atipClientStatsByFilter, bpsTestConfig, appSimList):

        print("Client Bytes:")
        clientPacketsValue = 0
        for appSim in appSimList:
            currentAppSim = bpsTestConfig.appSimComponents[appSim]
            clientPacketsValue += ValidateStatsService.computeClientBytesPerItem(currentAppSim)

        for filterItem in atipClientStatsByFilter.getList():
            currentSessions = getattr(filterItem, TopStats.TOTAL_COUNT.statType)
            currentBytes = getattr(filterItem, TopStats.CLIENT_BYTES.statType)
            expectedValue = clientPacketsValue * currentSessions

            print("Sessions: {}\nServerPackets: {}\nValue: {}".format(currentSessions, currentBytes, expectedValue))
            if not isclose(currentBytes, expectedValue, rel_tol=0.01):
                raise Exception(
                    "Client bytes differ: current: {} vs. expected: {}".format(currentBytes, expectedValue))

        return True

    @classmethod
    def checkTopFilteredDevices(cls, atipDeviceStatsByFilter, appSimList, bpsTestConfig, bpsComponentsStats,
                                atipServerStatsByFilter, atipClientStatsByFilter):

        cls.checkFilterSessionsByTopDevices(appSimList, bpsComponentsStats, atipDeviceStatsByFilter)


        cls.checkFilterTotalPacketsByTopDevices(atipServerStatsByFilter, atipClientStatsByFilter,
                                                atipDeviceStatsByFilter)

        cls.checkFilterTotalBytesByTopDevices(atipServerStatsByFilter, atipClientStatsByFilter,
                                              atipDeviceStatsByFilter)


    @classmethod
    def checkFilterSessionsByTopDevices(cls, appSimList, bpsComponentsStats, atipDeviceStatsByFilter):

        for filterItem in atipDeviceStatsByFilter.getList():
            atipTotalCount = getattr(filterItem, TopStats.TOTAL_COUNT.statType)
            bpsTotalCount = 0
            for appSim in appSimList:
                bpsTotalCount += getattr(bpsComponentsStats[appSim], BpsStatsType.SESSIONS.statsName)
            if not isclose(atipTotalCount, bpsTotalCount, rel_tol=0.05):
                raise ValueError("Total count differ: ATIP: {} vs. BPS: {}".format(atipTotalCount, bpsTotalCount))
        return True

    @classmethod
    def checkFilterTotalPacketsByTopDevices(cls, atipServerStatsByFilter, atipClientStatsByFilter,
                                            atipDeviceStatsByFilter):

        totalServerPackets = cls.getAtipStat(atipServerStatsByFilter, TopStats.SERVER_PKTS)
        totalClientPackets = cls.getAtipStat(atipClientStatsByFilter, TopStats.CLIENT_PKTS)

        print("Server packets: {}\nClient packets: {}".format(totalServerPackets, totalClientPackets))
        totalPackets = cls.getAtipStat(atipDeviceStatsByFilter, TopStats.TOTAL_PKTS)
        expectedValue = totalServerPackets + totalClientPackets
        if not isclose(totalPackets, expectedValue, rel_tol=0.01):
            raise ValueError("Total packets differ in Top devices: {} vs. expected {}".format(totalPackets,
                                                                                              expectedValue))
        return True

    @classmethod
    def checkFilterTotalBytesByTopDevices(cls, atipServerStatsByFilter, atipClientStatsByFilter,
                                          atipDeviceStatsByFilter):

        totalServerBytes = cls.getAtipStat(atipServerStatsByFilter, TopStats.SERVER_BYTES)
        totalClientBytes = cls.getAtipStat(atipClientStatsByFilter, TopStats.CLIENT_BYTES)

        print("Server bytes: {}\nClient bytes: {}".format(totalServerBytes, totalClientBytes))
        totalBytes = cls.getAtipStat(atipDeviceStatsByFilter, TopStats.TOTAL_BYTES)
        expectedValue = totalServerBytes + totalClientBytes
        if not isclose(totalBytes, expectedValue, rel_tol=0.01):
            raise ValueError("Total bytes differ in Top devices: {} vs. expected {}".format(totalBytes,
                                                                                            expectedValue))
        return True

    @classmethod
    def checkDetailedFilter(cls, atipDetailedStatsByFilter, finalMatches, bpsTestConfig, theFilter, atipFullServerStats,
                            atipFullClientStats, theTime):

        '''

        :param atipDetailedStatsByFilter: the ATIP result of the detailed filter REST Api call
        :param finalMatches: a user defined dictionary with all the correct client-server matches
                        (if any atipStat from the call is not presend in any of the finalMatches, then it
                should not be there)
        :param bpsTestConfig: the BPS test configuration
        :param theFilter: the current FilterConfig item
        :return: True if all items are ok
        '''
        results = {}
        sums = {}
        expected = {}
        if atipDetailedStatsByFilter.getList() == []:
            return True
        sums[FilteredTrafficStats.SERVER_BYTES.statType] = 0
        sums[FilteredTrafficStats.CLIENT_BYTES.statType] = 0
        sums[FilteredTrafficStats.SERVER_PKTS.statType] = 0
        sums[FilteredTrafficStats.CLIENT_PKTS.statType] = 0
        sums[FilteredTrafficStats.COUNT.statType] = 0
        cls.resetBooleans(results)
        currentItem = {}
        for item in atipDetailedStatsByFilter.getList():
            cls.resetBooleans(results)
            clientIP = str(item.getClientIP())
            serverIP = str(item.getServerIP())
            print("The atipFullServerStats: {}".format(jsonpickle.encode(atipFullServerStats.getList())))
            print("The atipFullClientStats: {}".format(jsonpickle.encode(atipFullClientStats.getList())))
            print("Checking IPs: {} and {}".format(clientIP, serverIP))
            print("Detailed: {}".format(jsonpickle.encode(item)))
            currentItem[clientIP] = serverIP

            if currentItem.items() <= finalMatches.items():  # this means that the currentItem dict is
                # a subset of the one with the possible correct combinations
                results[FilteredTrafficStats.APP.statType] = item.getAppName() in theFilter.conditions[0]['apps'][0].values()
                results[FilteredTrafficStats.SERVER_IP.statType] = serverIP in [item.getName() for item in atipFullServerStats.getList()]
                results[FilteredTrafficStats.SERVER_COUNTRY.statType] = item.serverCountry == GeoLocation.CHINA.countryCode
                results[FilteredTrafficStats.CLIENT_COUNTRY.statType] = item.clientCountry == GeoLocation.CHINA.countryCode
                results[FilteredTrafficStats.CLIENT_IP.statType] = clientIP in [item.getName() for item in atipFullClientStats.getList()]
                results[FilteredTrafficStats.CLIENT_CITY.statType] = item.clientCity == GeoLocation.CHINA.city
                sums[FilteredTrafficStats.SERVER_BYTES.statType] += item.serverBytes
                sums[FilteredTrafficStats.CLIENT_BYTES.statType] += item.clientBytes
                sums[FilteredTrafficStats.SERVER_PKTS.statType] += item.serverPkts
                sums[FilteredTrafficStats.CLIENT_PKTS.statType] += item.clientPkts
                sums[FilteredTrafficStats.COUNT.statType] += item.count
                print("Detailed: {}".format(jsonpickle.encode(item)))
                expected[FilteredTrafficStats.SERVER_PKTS.statType] = [atipItem.serverPkts for atipItem in atipFullServerStats.getList() if atipItem.getName() == serverIP][0]
                expected[FilteredTrafficStats.CLIENT_PKTS.statType] = [atipItem.clientPkts for atipItem in atipFullClientStats.getList() if atipItem.getName() == clientIP][0]
                expected[FilteredTrafficStats.SERVER_BYTES.statType] = [atipItem.serverBytes for atipItem in atipFullServerStats.getList() if atipItem.getName() == serverIP][0]
                expected[FilteredTrafficStats.CLIENT_BYTES.statType] = [atipItem.clientBytes for atipItem in atipFullClientStats.getList() if atipItem.getName() == clientIP][0]
                expected[FilteredTrafficStats.COUNT.statType] = [atipItem.totalCount for atipItem in atipFullServerStats.getList() if atipItem.getName() == serverIP][0]
                print("Time: {} {}".format(item.ts, theTime))
                # results[FilteredTrafficStats.TIME.statType] = isclose(item.ts // 1000, int(theTime), abs_tol=1.5 * bpsTestConfig.testDuration)

            else:
                raise ValueError("Current item: {} "
                             "is not one of the expected ones".format(currentItem))


        results[FilteredTrafficStats.SERVER_PKTS.statType] = expected[FilteredTrafficStats.SERVER_PKTS.statType] == sums[FilteredTrafficStats.SERVER_PKTS.statType]
        results[FilteredTrafficStats.CLIENT_PKTS.statType] = expected[FilteredTrafficStats.CLIENT_PKTS.statType] == sums[FilteredTrafficStats.CLIENT_PKTS.statType]
        results[FilteredTrafficStats.SERVER_BYTES.statType] = expected[FilteredTrafficStats.SERVER_BYTES.statType] == sums[FilteredTrafficStats.SERVER_BYTES.statType]
        results[FilteredTrafficStats.CLIENT_BYTES.statType] = expected[FilteredTrafficStats.CLIENT_BYTES.statType] == sums[FilteredTrafficStats.CLIENT_BYTES.statType]
        results[FilteredTrafficStats.COUNT.statType] = expected[FilteredTrafficStats.COUNT.statType] == sums[FilteredTrafficStats.COUNT.statType]
        print("The booleans now: {}".format(results))
        if list(results.values()).count(True) == len(results):
            return True
        else:
            raise ValueError("Not all stats are True")

        return True

    @classmethod
    def checkAllDetailedFilters(cls, clientIPs, serverIPs, devices, theFilter, atipSession, bpsTestConfig, theTime, bpsComponentStats, bpsSession, timeInterval):

        '''

        :param clientIPs: the list of all the available clientIPs (as a string)
        :param serverIPs: the list of all the available serverIPs (as a string)
        :param devices: the list of all the available devices
        :param theFilter: the filter to be used
        :param atipSession: current session
        :return: True if all detailed filter items are ok
        '''

        finalMatches = dict(zip(clientIPs, serverIPs))

        atipFullServerDetailedStats = FilterStats.getTopStats(atipSession, statsType=StatsType.SERVERS, timeInterval=timeInterval,
                                                              limit=DashboardType.MAXIMIZED.stringLimit,
                                                              ruleID=theFilter.id)

        atipFullClientDetailedStats = FilterStats.getTopStats(atipSession, statsType=StatsType.CLIENTS, timeInterval=timeInterval,
                                                              limit=DashboardType.MAXIMIZED.stringLimit,
                                                              ruleID=theFilter.id)
        for clientIP, serverIP, device in itertools.product(clientIPs, serverIPs, devices):
            print("Combined item: {} {} {}".format(clientIP, serverIP, device))
            atipDetailedStatsByFilter = FilterStats.getDetailedStats(atipSession, serverIP, clientIP, timeInterval,
                                                                     DashboardType.MAXIMIZED.stringLimit, theFilter.id,
                                                                     device=device)

            cls.checkDetailedFilter(atipDetailedStatsByFilter, finalMatches, bpsTestConfig,
                                    theFilter, atipFullServerDetailedStats,
                                    atipFullClientDetailedStats, theTime)
        return True

    @classmethod
    def checkGeosInstalledApp(cls, atipSession, timeInterval, bpsTestConfig, bpsComponentsStats):

        for component, appSimConfig in bpsTestConfig.appSimComponents.items():
            crtApp = appSimConfig.appType
            crtAppName = crtApp.appName
            cls.checkListStats(timeInterval, StatsType.GEO, crtApp, appSimConfig, atipSession=atipSession,
                               bpsTestConfig=bpsTestConfig,
                               bpsComponentsStats=bpsComponentsStats)

    @classmethod
    def checkListStats(cls, timeInterval, statsType, crtApp, appSimConfig, bpsSessions=-1, bpsPackets=-1,
                       bpsBytes=-1, atipSession=None, bpsTestConfig=None, bpsComponentsStats=None):

        if statsType == StatsType.GEO:
            checkStatValue = lambda: cls.checkListGeoValues(atipSession, timeInterval, crtApp, appSimConfig,
                                                            bpsComponentsStats)
        else:
            checkStatValue = lambda: cls.checkListStatsValues(atipSession, timeInterval, statsType, crtApp,
                                                              appSimConfig, bpsSessions, bpsPackets, bpsBytes)
        logActualValue = lambda: logging.exception(
            'Found incorrect values for {}. Actual: {}. Bps sessions: {}. Bps packets: {}. Bps bytes: {}'.format(
                statsType,
                jsonpickle.encode(Lists.getTopStats(atipSession, statsType, appType=crtApp)),
                jsonpickle.encode(bpsSessions),
                jsonpickle.encode(bpsPackets),
                jsonpickle.encode(bpsBytes)))

        TestCondition.waitUntilTrue(checkStatValue,
                                    'Found incorrect values for {}. Check logs for actual values'.format(statsType),
                                    onFailure=logActualValue)

    @classmethod
    def checkSpecificInstalledApp(cls, atipSession, timeInterval, atipStatsType, bpsTestConfig, bpsComponentsStats):
        '''
        :param atipStatsType: the ATIP window stats type to be validated
        e.g. StatsType.BROWSER
        '''
        print("Check specific installed apps")
        for component, appSimConfig in bpsTestConfig.appSimComponents.items():
            crtApp = appSimConfig.appType
            crtAppName = crtApp.appName
            # compute bps values

            bpsStatsByApp = BpsComponentStatsService.filterStatsByType(bpsTestConfig, bpsComponentsStats,
                                                                       StatsType.APPS,
                                                                       crtAppName)

            bpsSessionsByStat = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp,
                                                                           BpsStatsType.SESSIONS, atipStatsType)

            bpsPacketsByStat = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp,
                                                                          BpsStatsType.PACKETS,
                                                                          atipStatsType)
            print("!{} {}".format(bpsPacketsByStat, bpsSessionsByStat))
            bpsBytesByStat = BpsComponentStatsService.sumStatItemByType(bpsTestConfig, bpsStatsByApp,
                                                                        BpsStatsType.BYTES,
                                                                        atipStatsType)
            # check the values
            cls.checkListStats(timeInterval, atipStatsType, crtApp, appSimConfig, bpsSessionsByStat, bpsPacketsByStat,
                               bpsBytesByStat, atipSession)

    @staticmethod
    def checkListStatsValues(atipSession, timeInterval, statsType, crtApp, appSimConfig, bpsSessions, bpsPackets,
                             bpsBytes):
        atipStatsByApp = Lists.getTopStats(atipSession, statsType, timeInterval, appType=crtApp)
        print("{} STATS BY APP {}: {}".format(statsType, crtApp.appName, jsonpickle.encode(atipStatsByApp)))
        print("BPS BYtes: {}".format(bpsBytes))
        for atipStat in atipStatsByApp.getList():
            print("Current country: {}".format(atipStat.msg))
            crtKeyName = appSimConfig.getKeyName(statsType)
            print("Current key name: {}".format(crtKeyName))
            print("Key: {}".format(crtKeyName))
            print('{} {} {}'.format(atipStat.msg, atipStat.totalBytes, bpsBytes[crtKeyName]))
            print('{} {} {}'.format(atipStat.msg, atipStat.totalCount, bpsSessions[crtKeyName]))
            print('{} {} {}'.format(atipStat.msg, atipStat.totalPkts, bpsPackets[crtKeyName]))

            if atipStat.totalCount == None:
                atipStat.totalCount = 0

            expectedBytes = StatsService.getBytesForAckPacketLoss(bpsBytes[crtKeyName], bpsPackets[crtKeyName],
                                                                  atipStat.totalPkts)

            sessionsOK = isclose(atipStat.totalCount, bpsSessions[crtKeyName], rel_tol=0.1)
            bytesOK = isclose(atipStat.totalBytes, expectedBytes, rel_tol=0.1)
            return sessionsOK and bytesOK

    @staticmethod
    def checkListGeoValues(atipSession, timeInterval, crtApp, appSimConfig, bpsComponentsStats):

        atipStatsByApp = Lists.getTopStats(atipSession, StatsType.GEO, timeInterval, appType=crtApp)
        clientLocation = appSimConfig.clientLocation.countryName
        serverLocation = appSimConfig.serverLocation.countryName
        currentApp = appSimConfig.appType.appName
        ValidateStatsService.checkTargetExistsInTopStats(atipStatsByApp, clientLocation)
        ValidateStatsService.checkTargetExistsInTopStats(atipStatsByApp, serverLocation)

        for country in atipStatsByApp.getList():
            atipCountryName = getattr(country, TopStats.MESSAGE.statType)

            if atipCountryName == appSimConfig.clientLocation.countryName:

                result = dict()
                result[appSimConfig.name] = dict()
                result[appSimConfig.name][TopStats.CLIENT_BYTES] = ValidateStatsService.computeClientBytesPerItem(
                    appSimConfig)

                sessions = getattr(bpsComponentsStats[appSimConfig.name], BpsStatsType.SESSIONS.statsName)
                totalCountOK = ValidateStatsService.checkAtipListStats(atipStatsByApp,
                                                                       appSimConfig.clientLocation.countryName,
                                                                       TopStats.TOTAL_COUNT,
                                                                       sessions)
                bytesOK = ValidateStatsService.checkTopStatsSideBytesPerItem(country,
                                                                             bpsComponentsStats[appSimConfig.name],
                                                                             result,
                                                                             appSimConfig, TopStats.CLIENT_BYTES)

            elif atipCountryName == appSimConfig.serverLocation.countryName:

                result = dict()
                result[appSimConfig.name] = dict()
                result[appSimConfig.name][TopStats.SERVER_BYTES] = ValidateStatsService.computeServerBytesPerItem(
                    appSimConfig)

                bytesOK = ValidateStatsService.checkTopStatsSideBytesPerItem(country,
                                                                             bpsComponentsStats[appSimConfig.name],
                                                                             result,
                                                                             appSimConfig, TopStats.SERVER_BYTES)
                totalCountOK = ValidateStatsService.checkAtipListStats(atipStatsByApp,
                                                                       appSimConfig.serverLocation.countryName,
                                                                       TopStats.TOTAL_COUNT,
                                                                       0)

        return bytesOK and totalCountOK

    @classmethod
    def checkTrafficInstalledApp(cls, atipSession, bpsSession, testId, bpsComponentsStats, bpsTestConfig, tolerance=0.15):
        bpsContainer = BpsStats.getBpsRealTimeStatsContainer(bpsSession, testId)
        print("BPS Container: {}".format(jsonpickle.encode(bpsContainer.getList(bpsContainer))))
        (isOKS, isOKB) = ValidateStatsService.compareSessionRatePerItem(atipSession, bpsContainer, bpsComponentsStats,
                                                                 bpsTestConfig, tolerance)
        print("{} {}".format(isOKS, isOKB))
        if (isOKB, isOKS) != (True, True):
            raise Exception("Different values!")

    @classmethod
    def resetBooleans(cls, results):
        results[FilteredTrafficStats.APP.statType] = False
        results[FilteredTrafficStats.SERVER_IP.statType] = False
        results[FilteredTrafficStats.SERVER_COUNTRY.statType] = False
        results[FilteredTrafficStats.CLIENT_COUNTRY.statType] = False
        results[FilteredTrafficStats.CLIENT_IP.statType] = False
        results[FilteredTrafficStats.CLIENT_CITY.statType] = False
        results[FilteredTrafficStats.SERVER_PKTS.statType] = False
        results[FilteredTrafficStats.CLIENT_PKTS.statType] = False
        results[FilteredTrafficStats.SERVER_BYTES.statType] = False
        results[FilteredTrafficStats.CLIENT_BYTES.statType] = False
        # results[FilteredTrafficStats.TIME.statType] = False
        results[FilteredTrafficStats.COUNT.statType] = False

    @classmethod
    def checkTrafficForFilterApps(cls, theFilter, bpsTestConfig, bpsComponentStats, currentAtipStatsByFilter, bpsSession, tolerance=0.15):

        bpsContainer = BpsStats.getBpsRealTimeStatsContainer(bpsSession, bpsTestConfig.testId)
        bpsTime = float(bpsContainer.time)
        theAppList = theFilter.getAppConditions()
        bpsSessions = 0
        bpsBytes = 0
        for app in theAppList:

            print("~~~~~~~~~~~~~~~~~~~~~")
            print("The app in the list: {}".format(app))
        appSimList = bpsTestConfig.getAppSimsFromAppFilter(theFilter)


        print("The appSimList: {}".format(appSimList))
        print("Checking the bps for {}".format(appSimList))
        for appSim in appSimList:
            bpsSessions += getattr(bpsComponentStats[appSim], BpsStatsType.SESSIONS.statsName)
            bpsBytes += getattr(bpsComponentStats[appSim], BpsStatsType.BYTES.statsName)
        bpsRate = bpsSessions / bpsTime
        print("The BPS Time: {}".format(bpsTime))
        print("The ATIP app stat: {}".format(currentAtipStatsByFilter))

        bpsBitRate = bpsBytes * 8 / bpsTime
        print("bpsSessionRate: {}".format(bpsRate))
        print("bpsBitRate: {}".format(bpsBitRate))
        bitsData = currentAtipStatsByFilter.getList()[0]
        sessionsData = currentAtipStatsByFilter.getList()[1]
        print("bitsData: {}:".format(bitsData))
        print("sessionsData: {}".format(sessionsData))
        sessionRateOK = cls.checkRateFromList(sessionsData, bpsRate, tolerance)
        bitsRateOK = cls.checkRateFromList(bitsData, bpsBitRate, tolerance)

        print("SessionsOK: {}\nBitsOk: {}".format(sessionRateOK, bitsRateOK))
        if (sessionRateOK, bitsRateOK) != (True, True):
            raise Exception("Different values")
        return True


    def waitTargetExistsInPieStats(cls, atipSession, statsType, timeInterval, target, groupId =0):
        checkTargetExists = lambda : target in str(jsonpickle.encode(PieStats.getTopStats(atipSession, statsType, timeInterval,groupId=groupId)))
        TestCondition.waitUntilTrue(checkTargetExists, 'Couldn\'t find target: {}'.format(target),
                                    totalTime=StatsTimeout.TOP_FILTERS.waitTime, iterationTime=StatsTimeout.ITERATION_TIME.waitTime)  # @UndefinedVariable
        print('Found target {} in dashboard'.format(target))

    @classmethod
    def checkListStatsEmpty(cls, atipSession, statsType, timeInterval=TimeInterval.HOUR, groupId =0):
        '''
        Keyword arguments:
        '''
        ':type webApiSession: WebApiSession'
        ':type timeInterval: TimeInterval'
        condition = lambda : (Lists.getTopStats(atipSession, statsType, timeInterval,groupId=groupId)).list == []
        TestCondition.waitUntilTrue(condition, 'Top stats are not empty',
                                   totalTime=StatsTimeout.TOP_FILTERS.waitTime,
                                   iterationTime=StatsTimeout.ITERATION_TIME.waitTime)

    @classmethod
    def checkPieStatsEmpty(cls, atipSession, statsType, timeInterval=TimeInterval.HOUR, groupId=0):
        '''
        Keyword arguments:
        '''
        ':type webApiSession: WebApiSession'
        ':type timeInterval: TimeInterval'
        condition = lambda: (Pie.getTopStats(atipSession, statsType, timeInterval, groupId=groupId)).data == []
        TestCondition.waitUntilTrue(condition, 'Pie stats are not empty',
                                    totalTime=StatsTimeout.TOP_FILTERS.waitTime,
                                    iterationTime=StatsTimeout.ITERATION_TIME.waitTime)

    @classmethod
    def checkTrafficStatsEmpty(cls, atipSession, statsType, timeInterval=TimeInterval.HOUR, groupId=0):
        '''
        Keyword arguments:
        '''
        ':type webApiSession: WebApiSession'
        ':type timeInterval: TimeInterval'
        condition = lambda: (sum((Ports.getTopStats(atipSession, statsType, timeInterval)).list[0].data) == 0) and (sum((Ports.getTopStats(atipSession, statsType, timeInterval)).list[1].data) == 0)
        TestCondition.waitUntilTrue(condition, 'Traffic stats are not empty',
                                    totalTime=StatsTimeout.TOP_FILTERS.waitTime,
                                    iterationTime=StatsTimeout.ITERATION_TIME.waitTime)








