import time
import warnings

from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig
from atipAutoFrwk.data.traffic.RealTimeStats import RealTimeStats

from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage
from atipAutoFrwk.services.atip.stats.TrafficService import TrafficService
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations


class PerformanceService(object):

    @classmethod
    def verifyDropPercent(cls, webSession, env, accepted_drop):

        drop, drop_pkts, drop_b = DebugPage.getDropPercentFromDebugPage(webSession, env)
        print("SW drop current bytes {} current pkts {} sw_drop percent {} accepted {}".format(drop_b, drop_pkts, drop, accepted_drop))
        if (drop > float(accepted_drop)):
            return drop
        return 0

    @classmethod
    def checkPerformance(cls, env, atipSession, bpsSession, bpsFile, duration, rateType, validateDropRateFrequency, expectedRate, percent, hwDropPercent, swDropPercent):
        '''
        Verify traffic is received at expected rate on atip during test execution; rate and hw/sw drop rate are checked at a specific interval
        :param env: Environment object
        :param atipSession: current atip session
        :param bpsSession: current BPS session
        :param bpsFile: BPS file config
        :param duration: test duration
        :param rateType: 'bits' or 'sessions'
        :param validateDropRateFrequency: interval between drop rate validations
        :param expectedRate: expected rate the traffic should reach atip
        :param percent: accepted variation of expected rate
        :param hwDropPercent: hardware drop percent (should be always 0)
        :param swDropPercent: software drop percent (accepted drop rate)
        :return:
        '''
        init_rx = DebugPage.getRxPacketsFromDebugPage(atipSession, env)

        print("start bps test {}".format(bpsFile))
        bpsTestConfig = BpsTestConfig(bpsFile, duration)
        testId = BpsOperations.startBps(bpsSession, bpsTestConfig.testName, env.bpsPortGroup)

        time_left = bpsTestConfig.testDuration + 180  # add 3 more minutes to make sure test is done
        sleep_time = int(validateDropRateFrequency)
        if sleep_time > time_left:
            sleep_time = time_left

        time_passed = 0
        progress = BpsOperations.getBpsTestProgress(bpsSession, testId)
        sw_drop = 0
        current_sw_drop = 0

        #Record highest software drop
        while (0 < time_left) and progress != 100:
            time.sleep(sleep_time)
            time_passed += sleep_time
            time_left -= sleep_time
            current_sw_drop = cls.verifyDropPercent(atipSession, env, swDropPercent)
            if current_sw_drop > sw_drop:
                sw_drop = current_sw_drop
            progress = BpsOperations.getBpsTestProgress(bpsSession, testId)
            print('Time elapsed: {}s. Progress: {}'.format(time_passed, progress))

        check = None
        if progress != 100:
            check = BpsOperations.forceStopBpsTestIfRunning(bpsSession)
        if check is not None:
            warnings.warn("BPS test has been forcefully stopped")

        current_sw_drop = cls.verifyDropPercent(atipSession, env, swDropPercent)
        if current_sw_drop > sw_drop:
            sw_drop = current_sw_drop
        exceptionInfo = ""
        if sw_drop > 0:
            exceptionInfo = "Highest software drop counted is {}!\n".format(sw_drop)

        #Calculate hardware rate
        bpsTxFramesSessions = BpsStatsService.getRealTimeStatsByName(bpsSession, testId, RealTimeStats.ETH_TX_FRAMES)
        final_rx = DebugPage.getRxPacketsFromDebugPage(atipSession, env)
        hw_current_percent = 100.0 - (final_rx - init_rx) * 100.0 / bpsTxFramesSessions
        print("BPS tx frames {} atip rx frames {} hw_drop {} drop accepted {}".format(bpsTxFramesSessions, (final_rx - init_rx), hw_current_percent, hwDropPercent))
        if (hw_current_percent > float(hwDropPercent)):
            exceptionInfo += "Current HW drop {} is exceeding the accepted value {}!\n".format(hw_current_percent, hwDropPercent)

        current_rate = 0
        if (False == TrafficService.validateTrafficValues(atipSession, rateType, expectedRate, percent, current_rate, False)):
            exceptionInfo += "Current rate is not in the accepted interval expected {} current {}!".format(expectedRate, current_rate)

        if len(exceptionInfo) > 0:
            raise ValueError(exceptionInfo)

    @classmethod
    def checkHWDrop(cls, env, atipSession, bpsSession, bpsTestConfig, initRx, hwDropPercent, indexSlotNumber= 0 ):
        '''
        Verify traffic is received at expected rate on atip during test execution; rate and hw/sw drop rate are checked at a specific interval
        :param env: Environment object
        :param init_rx: initial drop
        :param bpsSession: current BPS session
        :param atipSession: current ATIP session
        :param bpsTestConfig: bpsTestConfig object
        :param rateType: 'bits' or 'sessions'
        :param hwDropPercent: hardware drop percent (should be always 0)
        :return:
        '''

        exceptionInfo = ""
        if bpsTestConfig.testId != None:
            bpsTxFramesSessions = BpsStatsService.getRealTimeStatsByName(bpsSession, bpsTestConfig.testId,
                                                                     RealTimeStats.ETH_TX_FRAMES)
            finalRx = DebugPage.getRxPacketsFromDebugPage(atipSession, env, indexSlotNumber)
            hwCurrentPercent = 100.0 - (finalRx - initRx) * 100.0 / bpsTxFramesSessions
            print("BPS tx frames {} atip rx frames {} hw_drop {} drop accepted {}".format(bpsTxFramesSessions,
                                                                                      (finalRx - initRx),
                                                                                      hwCurrentPercent,
                                                                                      hwDropPercent))
            if (hwCurrentPercent > float(hwDropPercent)):
                exceptionInfo += "Current HW drop {} is exceeding the accepted value {}!\n".format(hwCurrentPercent,
                                                                                               hwDropPercent)
        else:
            raise Exception("No Test Run found on for {}\n".format(bpsTestConfig.testName))