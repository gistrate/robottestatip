from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.webApi.atip.Traffic import Traffic


class TrafficService(object):


    @classmethod
    def getTrafficSessionsStats(cls, webApiSession, timeInterval=TimeInterval.HOUR, strict=True):
        statsList = Traffic.getTrafficStats(webApiSession, timeInterval)
        print(statsList)
        if not statsList.get('portSeries')[0].get('series'):
            if strict:
                raise Exception('ERROR: No stats present in the Traffic window')
            else:
                return None
        else:
            if not isinstance(statsList.get('portSeries')[0].get('series'), list):
                if statsList.get('portSeries')[0].get('series').get('name') != "Sessions/s":
                    if strict:
                        raise Exception('Throughput(Sessions/s) wasn\'t found in the Traffic window.')
                    else:
                        return None 
                else:
                    return statsList.get('portSeries')[0].get('series').get("data")

            else:
                if len([element.get('name') for element in statsList.get('portSeries')[0].get('series') if
                        element.get('name') == "Sessions/s"]) == 0:
                    if strict:
                        raise Exception('Throughput(Sessions/s) wasn\'t found in the Traffic window.')
                    else:
                        return None
                else:
                    return [element.get("data") for element in statsList.get('portSeries')[0].get('series') if element.get('name') == "Sessions/s"]
    
    @classmethod
    def getTrafficThroughputStats(cls, webApiSession, timeInterval=TimeInterval.HOUR, strict=True):
        statsList = Traffic.getTrafficStats(webApiSession, timeInterval)
        print(statsList)
        if not statsList.get('portSeries')[0].get('series'):
            if strict:
                raise Exception('ERROR: No stats present in the Traffic window')
            else:
                return None
        else:
            if not isinstance(statsList.get('portSeries')[0].get('series'), list):
                if statsList.get('portSeries')[0].get('series').get('name') != "Bits/s":
                    if strict:
                        raise Exception('Throughput(Bits/s) wasn\'t found in the Traffic window.')
                    else:
                        return None 
                else:
                    return statsList.get('portSeries')[0].get('series').get("data")

            else:
                if len([element.get('data') for element in statsList.get('portSeries')[0].get('series') if
                        element.get('name') == "Bits/s"]) == 0:
                    if strict:
                        raise Exception('Throughput(Bits/s) wasn\'t found in the Traffic window.')
                    else:
                        return None
                else:
                    return [element.get("data") for element in statsList.get('portSeries')[0].get('series') if element.get('name') == "Bits/s"]
            
    @classmethod
    def validateTrafficValues(cls, webApiSession, rateType, rate, percent, current_rate, strict=True):
        current_rate = 0
        
        #returns True if current rate is at least percent of rate
        if "bits" == rateType:
            list_of_data = TrafficService.getTrafficThroughputStats(webApiSession)
        else:
            list_of_data = TrafficService.getTrafficSessionsStats(webApiSession)
        
        if None != list_of_data[0]:
            nr=0
            last=0
            start=1
            sum_data=0
            for val in list_of_data[0]:
                if 0!=val:
                    if 1==start:
                        start=0
                    else:
                        nr+=1
                        sum_data+=val
                        last=val
            if nr>0:
                sum_data-=last
                nr-=1
            if nr<=0:
                if strict:
                    raise Exception('No traffic data.')
                else:
                    return False
            calc_rate=sum_data/nr
            current_rate = calc_rate
            calc_percent=(float(rate)-calc_rate)*100.0/float(rate)
            print('rate current: {}. rate expected: {} {}'.format(current_rate, rate, calc_percent))
            if (calc_percent > ((-1)*float(percent))) and (calc_percent < float(percent)):
                return True
            else:
                return False
        else:
            if strict:
                raise Exception('No traffic data.')
            else:
                print('No traffic data.')
                return False
            
            
            
