import logging
import logging
import re
from urllib3.util import timeout

from atipAutoFrwk.data.atip.MaxItemsPerCategory import MaxItemsPerCategory
from atipAutoFrwk.data.atip.ProcStats import ProcStats
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.webApi.atip.System import System


class DebugPage(object):

    @classmethod
    def getDropPercentFromDebugPage(cls, webApiSession, envVars, indexSlotNumber = 0):
        response = System.readDebug(webApiSession)
        drops = re.findall(r'NP::{}[\S\s]+?(\d+)\s+-\s+drop_pcnt'.format(envVars.atipSlotNumber[indexSlotNumber]), response)
        drop_percent = int(drops[0][0])
        drops = re.findall(r'NP::{}[\S\s]+?(\d+)\s+(\d+)\s+rx_drop'.format(envVars.atipSlotNumber[indexSlotNumber]), response)
        drop_bytes = int(drops[0][1])
        drop_pkts = int(drops[0][0])
        return drop_percent, drop_pkts, drop_bytes

    @classmethod
    def getRxPacketsFromDebugPage(cls, webApiSession, envVars, indexSlotNumber=0):
        response = System.readDebug(webApiSession)
        hw_rx = re.findall(r'NP::{}[\S\s]+?Proc\sStats\sTotal:[\S\s]+?(\d+)\s+(\d+)\s+hw_rx'.format(envVars.atipSlotNumber[indexSlotNumber]), response)
        print('hw_rx: {}'.format(hw_rx))
        rx_pkts = int(hw_rx[0][0])
        return rx_pkts

    @classmethod
    def getRxProcPacketsFromDebugPage(cls, webApiSession, envVars, indexSlotNumber=0):
        response = System.readDebug(webApiSession)
        rx_proc = re.findall(r'NP::{}[\S\s]+?(\d+)\s+(\d+)\s+rx_proc'.format(envVars.atipSlotNumber[indexSlotNumber]), response)
        print('rx_proc: {}'.format(rx_proc))
        rx_proc_pkts = int(rx_proc[0][0])
        return rx_proc_pkts


    @classmethod
    def getNpRulesFromDebugPage(cls, webApiSession, npId):
        response = System.readDebug(webApiSession)
        respNoNewLines = response.replace('\n', '').replace('\r', '')
        rules = re.findall(r'NP::{0}.*?(\d+)\s+:\s+Rules Installed'.format(npId), respNoNewLines)
        rulesNo = 0
        if rules:
            rulesNo = int(rules[0])
        return rulesNo

    @classmethod
    def getTxStatsFromDebugPage(cls, webApiSession, envVars, statType='bits', indexSlotNumber=0):
        '''
        Get tx statistics from debug page, packets or bytes
        :param webApiSession: current session
        :param envVars: environment variables
        :param statType: type of statistics you want to retrieve: packets, bytes or bits
        :param indexSlotNumber: NP slot number
        :return: specified statistics value
        '''
        response = System.readDebug(webApiSession)
        txStats = re.findall(r'NP::{}[\S\s]+?(\d+)\s+(\d+)\s+tx\s'.format(envVars.atipSlotNumber[indexSlotNumber]), response)

        if statType == 'packets':
            respStats = int(txStats[0][0])
        elif statType == 'bytes':
            respStats = int(txStats[0][1])
        else:
            respStats = int(txStats[0][1]*8)
        return respStats


    @classmethod
    def getSSLStatsFromDebugPage(cls, webApiSession, groupId=0):
        response = System.readSslDebug(webApiSession, groupId=groupId)
        dict = {}
        result = re.findall(r'(.*?)=(\d+)', response)
        for i in result:
            dict[i[0]] = i[1]
        print('dict: {}'.format(dict))
        return dict

    @classmethod
    def getNpStaticAppsDebugPage(cls, webApiSession, npId):
        response = System.readDebug(webApiSession)
        respNoNewLines = response.replace('\n', '').replace('\r', '')
        apps = re.findall(r'NP::{0}.*?(\d+)\s+:\s+Static Apps Installed'.format(npId), respNoNewLines)
        print('Number of static apps {} for NP {}'.format(apps, npId))
        appsNo = 0
        if apps:
            appsNo = int(apps[0])
        return appsNo

    @classmethod
    def checkNpStaticApps(cls, webApiSession, npIdList, staticAppsNo=MaxItemsPerCategory.MAX_STATIC_APPS.maxNumber):
        '''
        Checks the number of static apps for all the given NPs.
        If no number for static apps is provided, the default number of static apps is used.
        Can be used for a single NP.
        '''
        if not isinstance(npIdList, list):
            npIdList = [npIdList]

        for npId in npIdList:
            checkStaticApps = lambda: cls.getNpStaticAppsDebugPage(webApiSession, npId) == staticAppsNo
            TestCondition.waitUntilTrue(checkStaticApps, 'Expected Number of Static Apps:{}'.format(staticAppsNo), 300, iterationTime=5)

    @classmethod
    def getTxPacketsFromDebugPage(cls, webApiSession, npId):
        response = System.readDebug(webApiSession)
        transmitted = re.findall(r'NP::{}[\S\s]+?(\d+)\s+(\d+)\s+tx'.format(npId), response)
        transmitted_pkts = int(transmitted[0][0])
        return transmitted_pkts

    @classmethod
    def verifyNpRules(cls, webApiSession, expRules, npId):
        '''
        :type webApiSession: WebApiSession
        :type expRules: int
        :type npId: string
        '''
        rules = DebugPage.getNpRulesFromDebugPage(webApiSession, npId)
        if rules != expRules:
            return False

        return True

    @classmethod
    def checkNpRules(cls, webApiSession, expRules, npId):
        expRules = int(expRules)
        checkValue = lambda: DebugPage.verifyNpRules(webApiSession, expRules, npId) == True
        logActualValue = lambda: logging.exception('Found incorrect number of rules {} on NP {}; expected {}'
                                                   .format(DebugPage.getNpRulesFromDebugPage(webApiSession, npId), npId, expRules))
        
        TestCondition.waitUntilTrue(checkValue, onFailure=logActualValue, totalTime=300)
        return True

    @classmethod
    def getNpDynamicAppsDebugPage(cls, webApiSession, npId):
        response = System.readDebug(webApiSession)
        respNoNewLines = response.replace('\n', '').replace('\r', '')
        apps = re.findall(r'NP::{0}.*?(\d+)\s+:\s+Dynamic Apps Installed'.format(npId), respNoNewLines)
        print('Number of dynamic apps {} for NP {}'.format(apps, npId))
        return apps[0]

    @classmethod
    def checkNumberDynamicApps(cls, webApiSession, apps, npId=0, timeout=0, iterations=0):
        checkDynApps = lambda: int(cls.getNpDynamicAppsDebugPage(webApiSession, npId)) == int(apps)
        TestCondition.waitUntilTrue(checkDynApps, 'Expected Number of Dynamic Apps:{}'.format(apps), totalTime=int(timeout), iterationTime=int(iterations))
        return None

    @classmethod
    def getDedupStatsFromDebugPage(cls, webApiSession, groupId=0):
        response = System.readDedupDebug(webApiSession, groupId)
        dict = {}
        result = re.findall(r'(.*?)=(\d+)', response)
        for i in result:
            dict[i[0]] = i[1]
        print('dict: {}'.format(dict))
        return dict

    @classmethod
    def getConnTimeoutsFromDebugPage(cls, webApiSession, npId):
        response = System.readDebug(webApiSession)
        timeouts = re.findall(r'NP::{}[\S\s]+?Proc\sStats\sTotal:[\S\s]+?(\d+)\s+:\s+Connection Timeouts'.format(
            npId), response)
        print('conn timeouts: {}'.format(timeouts))
        result = int(timeouts[0])
        return result

    @classmethod
    def getProcStatValueFromDebugPage(cls, webApiSession, npId, procStat):
        response = System.readDebug(webApiSession)
        match = re.findall(r'NP::{}[\S\s]+?Proc\sStats\sTotal:[\S\s]+?(\d+)\s+:\s+{}'.format(
            npId,procStat), response)
        print('{}: {}'.format(procStat, match))
        result = int(match[0])
        return result




