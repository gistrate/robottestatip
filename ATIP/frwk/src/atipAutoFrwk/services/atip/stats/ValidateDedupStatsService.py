from cmath import isclose
from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage



class ValidateDedupStatsService(object):

    @classmethod
    def validateDedupVariation(cls, atipSession, percentage, groupId=0):
        stats= DebugPage.getDedupStatsFromDebugPage(atipSession, groupId)
        valueOK = isclose(float(stats['PacketsDropped']), float(stats['PacketsPassed']), rel_tol=float(percentage))
        if not valueOK:
            raise ValueError('packets passed {} vs packets dropped: {}'.format(stats['PacketsPassed'], stats['PacketsDropped']))
        else:
            print('packets passed {} vs packets dropped: {}'.format(stats['PacketsPassed'], stats['PacketsDropped']))
        return valueOK


    @classmethod
    def checkUnhandledPckts(cls, atipSession, expectedValue, groupId=0):
        stats = DebugPage.getDedupStatsFromDebugPage(atipSession, groupId)
        unhandled = stats['UnhandledPackets']
        if int(unhandled) > int(expectedValue):
            raise ValueError('Unhandled Packets present: {}'.format(unhandled))
        else:
            print('Unhandled packets: {}'.format(expectedValue))
        return unhandled



