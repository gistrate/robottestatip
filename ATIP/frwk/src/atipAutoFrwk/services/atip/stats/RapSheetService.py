import re
import logging

from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.webApi.atip.System import System

class RapSheetService(object):

    @classmethod

    def verifyRapsheetDbExists(cls, webApiSession, expectedValue):
        '''
        Checks that Threat Database is present on ATIP
        :type webApiSession: WebApiSession
        :param expectedValue: should be non 0 if the threat db got downloaded
        :return: False if db was not found, True if db present.
        '''
        dbVersion = lambda: cls.checkForRapsheetDb(webApiSession, expectedValue)

        TestCondition.waitUntilTrue(dbVersion,
                                    'Rapsheet Threat Database Couldn\'t be found',totalTime=60,iterationTime=2)
        return True

    @classmethod
    def checkForRapsheetDb(cls, webApiSession, expectedValue):
        dbVersion = System.readRapSheetDebug(webApiSession)
        if dbVersion == expectedValue:
            return False
        else:
            return True