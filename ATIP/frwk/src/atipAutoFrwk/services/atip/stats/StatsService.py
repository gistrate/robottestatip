class StatsService(object):

    @classmethod
    def getBytesForAckPacketLoss(cls, bpsBytes, bpsPackets, atipPackets):
        return bpsBytes - ((bpsPackets - atipPackets) * 66)