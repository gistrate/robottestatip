from cmath import isclose
from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage



class ValidateSSLStatsService(object):

    @classmethod
    def validateSslTotalDecryptedSessionsByPercentage(cls, atipSession, expectedValue, groupId=0, percentage=0.06):
        stats= DebugPage.getSSLStatsFromDebugPage(atipSession, groupId)
        conns = float(stats['SslResumedConns'])+float(stats['SslStandardConns'])
        valueOK = isclose(conns, float(expectedValue), rel_tol=percentage)
        if not valueOK:
            raise ValueError('bpsSession {} vs ssl decrypted conn: {}'.format(expectedValue, conns))
        else:
            print('BPS sessions: {}, ATIP conns: {}'.format(expectedValue,conns))
        return valueOK

    @classmethod
    def validateSslDecryptedBytesByPercentage(cls, atipSession, expectedValue, groupId=0, percentage=0.06):
        stats = DebugPage.getSSLStatsFromDebugPage(atipSession, groupId)
        sslDecryptedBytes = float(stats['SslDecryptedBytes'])/8
        valueOK = isclose(sslDecryptedBytes, float(expectedValue), rel_tol=percentage)
        if not valueOK:
            raise ValueError('bps bytes {} vs ssl decrypted bytes: {}'.format(expectedValue, sslDecryptedBytes))
        else:
            print('BPS bytes: {}, ATIP decrypted bytes: {}'.format(expectedValue, sslDecryptedBytes))
        return valueOK

    @classmethod
    def checkSslKeyFails(cls, atipSession, groupId=0):
        stats = DebugPage.getSSLStatsFromDebugPage(atipSession,groupId)
        sslKeyFails = int(stats['SslKeyFails'])
        if sslKeyFails != 0:
            raise ValueError('SSLKeyFails present: {}'.format(sslKeyFails))
        return sslKeyFails

    @classmethod
    def checkSslResumeFails(cls, atipSession, groupId=0):
        stats = DebugPage.getSSLStatsFromDebugPage(atipSession, groupId)
        sslResumeFails = int(stats['SslResumeFails'])
        if sslResumeFails != 0:
            raise ValueError('SSLResumeFails present: {}'.format(sslResumeFails))
        return sslResumeFails



