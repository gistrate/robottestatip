from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.Status import Status
import time


class LoginService(object):

    @classmethod
    def waitUntilLoggedIn(cls, webApiSession, username=None, password=None, maxWaitTime=300):
        '''
        This does not work when clear system is called
        :param maxWaitTime: Maximum time in seconds to wait for the condition to be true. Default 5 min.
        :type webApiSession: WebApiSession
        :type env: Environment
        '''


        loginOk = lambda: cls.loginOk(webApiSession, username, password) == True
            
        errMsg = 'Couldn\'t login in {} seconds'.format(maxWaitTime)   
        TestCondition.waitUntilTrue(loginOk, errorMessage=errMsg, totalTime=maxWaitTime, iterationTime=5)


    @classmethod
    def loginOk(cls, webApiSession, username=None, password=None):
        '''
        :return True if the Login was successful; False otherwise.
        '''
        try:
            Login.login(webApiSession, username, password)
        except Exception:
            return False  
        return True