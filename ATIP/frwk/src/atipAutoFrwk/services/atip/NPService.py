from atipAutoFrwk.services.SshService import SshService
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.SshNP import SshNP
from atipAutoFrwk.services.atip.FileService import FileService
from atipAutoFrwk.webApi.atip.NP import NP


class NPService(object):

    def setSshParamsAndConnect(self, envVars):
        sshSession = SshService(envVars.atipConnectionConfig.host, 'root')
        if envVars.atipType == AtipType.HARDWARE:
            sshSession.keyFile = SshNP.ATIP_HW_PKey.filePath
        else:
            sshSession.keyFile = SshNP.ATIP_VIRT_PKey.filePath

        sshSession.connectWithPrivateKey()

        return sshSession

    def sshRemoveCoreFilesInNP(self, envVars, docker='atip-np-dpdk', indexSlotNumber=0):

        sshSession = self.setSshParamsAndConnect(envVars)

        if envVars.atipType == AtipType.VIRTUAL:
            sshSession.sendShell("docker exec {} sh -c 'rm -f {}core*'".format(docker, SshNP.CORE_PATH.filePath))
        else:
            sshSession.sendShell('ssh np{}-0 rm -f {}core*'.format(envVars.atipSlotNumber[indexSlotNumber],SshNP.CORE_PATH.filePath))

        sshSession.closeConnection()

    def sshRemoveDumpFilesInNP(self, envVars, docker='atip-np-dpdk', indexSlotNumber=0):

        sshSession = self.setSshParamsAndConnect(envVars)
        if envVars.atipType == AtipType.VIRTUAL:
            sshSession.sendShell("docker exec {} sh -c 'rm -f {}sysapp_dump*'".format(docker, SshNP.CORE_PATH.filePath))
        else:
            sshSession.sendShell('ssh np{}-0 rm -f {}sysapp_dump*'.format(envVars.atipSlotNumber[indexSlotNumber],SshNP.CORE_PATH.filePath))

        sshSession.closeConnection()

    def sshRemoveNPLogFile(self, envVars, docker='atip-np-dpdk', indexSlotNumber=0):

        sshSession = self.setSshParamsAndConnect(envVars)
        location = '{}/all'.format(SshNP.LOG_FILES_PATH_NP.filePath)

        # check target file exists
        testAvail = self.checkForFile(self, envVars, sshSession, location)
        if 'exists' in testAvail:
            print('File {} found.'.format(location))
        else:
            print('File {} not found, nothing to delete'.format(location))

        if envVars.atipType == AtipType.VIRTUAL:
            sshSession.sendShell("docker exec {} sh -c 'rm -f {}'".format(docker, location))
        else:
            sshSession.sendShell('ssh np{}-0 rm -f {}'.format(envVars.atipSlotNumber[indexSlotNumber],location))

        # check file has been deleted
        testAvail = self.checkForFile(self, envVars, sshSession, location)
        if 'exists' in testAvail:
            raise Exception('Deleting file {} failed'.format(location))
        else:
            print('File {} has been successfully deleted.'.format(location))

        sshSession.closeConnection()

    def validateNoCrashesInNPCoreFile(self, webApiSession, envVars, testCase, docker='atip-np-dpdk', indexSlotNumber=0):
        ':type webApiSession WebApiSession'

        sshSession = self.setSshParamsAndConnect(envVars)
        diagsLocation = 'diags/'

        if envVars.atipType == AtipType.VIRTUAL:
            output, exitStatus = sshSession.sendShell("docker exec {} sh -c 'find {} -maxdepth 1 -iname \'core*\''".format(docker, SshNP.CORE_PATH.filePath))
            print("Find for {}core* returned ".format(SshNP.CORE_PATH.filePath) + output)
            if 'core' in output.lower():
                sshSession.sendShell('rm -rf {}'.format(SshNP.CORE_FILES_PATH.filePath) + testCase)
                sshSession.sendShell('mkdir -p {}'.format(SshNP.CORE_FILES_PATH.filePath) + testCase)
                sshSession.sendShell('docker cp {}:{} {}'.format(docker, SshNP.CORE_PATH.filePath,SshNP.CORE_FILES_PATH.filePath) + testCase)
                # save diagnostics file
                FileService.saveFile(self, webApiSession, diagsLocation, testCase, indexSlotNumber=0)
                sshSession.closeConnection()
                raise Exception('File(s) with name core* do(es) exist, indicating that there is a crash in NP, look for these files under {}'.format(SshNP.CORE_FILES_PATH.filePath) + testCase)
            else:
                sshSession.closeConnection()
                print('There are no core* log files in {}core*, assuming there are no crashes in NP'.format(SshNP.CORE_PATH.filePath))
        else:
            output, exitStatus = sshSession.sendShell("ssh np{}-0 find {} -maxdepth 1 -iname \'core*\'".format(envVars.atipSlotNumber[indexSlotNumber],SshNP.CORE_PATH.filePath))
            print("Find for {}core* returned ".format(SshNP.CORE_PATH.filePath) + output)
            if 'core' in output.lower():
                sshSession.sendShell('rm -rf {}'.format(SshNP.CORE_FILES_PATH.filePath) + testCase)
                sshSession.sendShell('mkdir -p {}'.format(SshNP.CORE_FILES_PATH.filePath) + testCase)
                sshSession.sendShell('scp np{}-0:{}core* {}'.format(envVars.atipSlotNumber[indexSlotNumber], SshNP.CORE_PATH.filePath,SshNP.CORE_FILES_PATH.filePath) + testCase)
                # save diagnostics file
                FileService.saveFile(self, webApiSession, diagsLocation, testCase)
                sshSession.closeConnection()
                raise Exception('File(s) with name core* do(es) exist, indicating that there is a crash in NP, look for these files under {}'.format(SshNP.CORE_FILES_PATH.filePath) + testCase)
            else:
                sshSession.closeConnection()
                print('There are no core* log files in {}core*, assuming there are no crashes in NP'.format(SshNP.CORE_PATH.filePath))


    def validateNoResetsInNPDumpFile(self, webApiSession, envVars, testCase, docker='atip-np-dpdk',indexSlotNumber=0):
        ':type webApiSession WebApiSession'

        sshSession = self.setSshParamsAndConnect(envVars)

        if envVars.atipType == AtipType.VIRTUAL:
            output, exitStatus = sshSession.sendShell("docker exec {} sh -c 'find {} -maxdepth 1 -iname \'sysapp_dump\''".format(docker, SshNP.CORE_PATH.filePath))
            print("Find for {}sysapp_dump* returned ".format(SshNP.CORE_PATH.filePath) + output)
            if 'sysapp_dump' in output.lower():
                sshSession.sendShell('rm -rf {}'.format(SshNP.DUMP_FILES_PATH.filePath) + testCase)
                sshSession.sendShell('mkdir -p {}'.format(SshNP.DUMP_FILES_PATH.filePath) + testCase)
                sshSession.sendShell('docker cp {}:{} {}'.format(docker, SshNP.CORE_PATH.filePath,SshNP.DUMP_FILES_PATH.filePath) + testCase)
                sshSession.closeConnection()
                raise Exception('File(s) with name sysapp_dump* do(es) exist, indicating that there is a watchdog failure that triggered a NP reboot, look for these files under {}'.format(SshNP.DUMP_FILES_PATH.filePath) + testCase)
            else:
                sshSession.closeConnection()
                print('There are no sysapp_dump* log files in {}sysapp_dump*, assuming there are no resets of NP'.format(SshNP.CORE_PATH.filePath))
        else:
            output, exitStatus = sshSession.sendShell("ssh np{}-0 find {} -maxdepth 1 -iname \'sysapp_dump*\'".format(envVars.atipSlotNumber[indexSlotNumber], SshNP.CORE_PATH.filePath))
            print("Find for {}sysapp_dump* returned ".format(SshNP.CORE_PATH.filePath) + output)
            if 'sysapp_dump' in output.lower():
                sshSession.sendShell('rm -rf {}'.format(SshNP.DUMP_FILES_PATH.filePath) + testCase)
                sshSession.sendShell('mkdir -p {}'.format(SshNP.DUMP_FILES_PATH.filePath) + testCase)
                sshSession.sendShell('scp np{}-0:{}sysapp_dump* {}'.format(envVars.atipSlotNumber[indexSlotNumber], SshNP.CORE_PATH.filePath, SshNP.DUMP_FILES_PATH.filePath) + testCase)
                sshSession.closeConnection()
                raise Exception('File(s) with name sysapp_dump* do(es) exist, indicating that there is a watchdog failure that triggered a NP reboot, look for these files under {}'.format(SshNP.DUMP_FILES_PATH.filePath) + testCase)
            else:
                sshSession.closeConnection()
                print('There are no sysapp_dump* log files in {}sysapp_dump*, assuming there are no resets of NP'.format(SshNP.CORE_PATH.filePath))

    def validateStringInFile(self, webApiSession, envVars, fileToValidate, searchedString, dockerName=None):
        ':type webApiSession WebApiSession'

        sshSession = self.setSshParamsAndConnect(envVars)
        location = '{}/{}'.format(SshNP.LOG_FILES_PATH.filePath, fileToValidate)

        grepCheck = 'grep ' + searchedString + ' ' + location

        if envVars.atipType == AtipType.HARDWARE:
            output, exitStatus = sshSession.sendShell(grepCheck)
        else:
            sshSession.sendShell('mkdir {}'.format(SshNP.LOG_FILES_PATH.filePath))
            sshSession.sendShell('docker cp {0}:{1}/{2} {1}/atip'.format(dockerName, SshNP.LOG_FILES_PATH.filePath, fileToValidate))
            output, exitStatus = sshSession.sendShell(grepCheck)
            sshSession.sendShell('rm -r {}'.format(SshNP.LOG_FILES_PATH.filePath))

        if exitStatus != 0:
            raise Exception('The grep command: {} was not executed successfully, the string was not found in the file'.format(grepCheck))
        else:
            print('The string was found in {}/{}'.format(SshNP.LOG_FILES_PATH.filePath, fileToValidate))

        sshSession.closeConnection()

    def validateNoParseErrorNPLogFile(self, webApiSession, envVars, docker='atip-np-dpdk', indexSlotNumber=0):
        ':type webApiSession WebApiSession'

        sshSession = self.setSshParamsAndConnect(envVars)
        location = '{}/all'.format(SshNP.LOG_FILES_PATH_NP.filePath)

        # check target file exists
        testAvail = self.checkForFile(self, envVars, sshSession, location)
        if 'exists' in testAvail:
            print('File {} found.'.format(location))
        else:
            raise Exception('File {} not found, nothing to check!'.format(location))

        if envVars.atipType == AtipType.VIRTUAL:
            output, exitStatus = sshSession.sendShell("docker exec {} sh -c \"grep \'parse error\' {}\"".format(docker, location))
            print("Results for parse error returned {}".format(output))
            sshSession.closeConnection()
            if exitStatus == 0:
                raise Exception('Parse errors detected when creating filters for static apps')
            else:
                print('No parse errors were detected when creating filters for each static app')
        else:
            output, exitStatus = sshSession.sendShell("ssh np{}-0 \"grep \'parse error\' {}\"".format(envVars.atipSlotNumber[indexSlotNumber], location))
            print("Results for parse error returned {}".format(output))
            sshSession.closeConnection()
            if exitStatus == 0:
                raise Exception('Parse errors detected when creating filters for static apps')
            else:
                print('No parse errors detected when creating filters for each static app')

        sshSession.closeConnection()

    def validateNoErrorNPLogFile(self, webApiSession, envVars, docker='atip-np-dpdk', indexSlotNumber=0):
        ':type webApiSession WebApiSession'

        sshSession = self.setSshParamsAndConnect(envVars)
        location = '{}/all'.format(SshNP.LOG_FILES_PATH_NP.filePath)

        # check target file exists
        testAvail = self.checkForFile(self, envVars, sshSession, location)
        if 'exists' in testAvail:
            print('File {} found.'.format(location))
        else:
            raise Exception('File {} not found, nothing to check!'.format(location))

        if envVars.atipType == AtipType.VIRTUAL:
            output, exitStatus = sshSession.sendShell("docker exec {} sh -c \"grep \'ERROR\' {}\"".format(docker, location))
            print("Results for error returned {}".format(output))
            sshSession.closeConnection()
            if exitStatus == 0:
                raise Exception('Errors detected when creating filters for static apps')
            else:
                print('No errors were detected when creating filters for each static app')
        else:
            output, exitStatus = sshSession.sendShell("ssh np{}-0 \"grep \'ERROR\' {}\"".format(envVars.atipSlotNumber[indexSlotNumber], location))
            print("Results for error returned {}".format(output))
            sshSession.closeConnection()
            if exitStatus == 0:
                raise Exception('Errors detected when creating filters for static apps')
            else:
                print('No errors detected when creating filters for each static app')

        sshSession.closeConnection()

    @staticmethod
    def checkForFile(cls, envVars, sshSession, fileLocation, docker='atip-np-dpdk',indexSlotNumber=0):
        ':type sshSession SshSession'

        if envVars.atipType == AtipType.VIRTUAL:
            output, exitStatus = sshSession.sendShell("docker exec {} sh -c 'test -e {} && echo exists || echo not found'".format(docker, fileLocation))
        else:
            output, exitStatus = sshSession.sendShell("ssh np{}-0 'test -e {} && echo exists || echo not found'".format(envVars.atipSlotNumber[indexSlotNumber], fileLocation))

        return output

    @classmethod
    def updateNpIdVirtual(cls, webApiSession, envVars):
        if envVars.atipType == AtipType.VIRTUAL:
            npId = NP.getNpId(webApiSession)
            print("vNP id is {} ;".format(npId))
            envVars.atipSlotNumber = [npId]
        else:
            npId = envVars.atipSlotNumber[0]
            print("NP id is {} ;".format(npId))
        return npId