import pyshark
from atipAutoFrwk.services.SshService import SshService
from atipAutoFrwk.webApi.atip.FileOperations import FileOperations
from atipAutoFrwk.webApi.atip.Capture import Capture
import os.path
from pathlib import Path
import zipfile
import shutil
import warnings
import paramiko

class FileService:

    @classmethod
    def saveFile(cls, webApiSession, sourceLocation, fileName=None):
        '''
        Get file from "sourceLocation" and saves it to user's home folder; a <user_home_folder>/sourceLocation path is created
        :type webApiSession WebApiSession
        :param  webApiSession: current api session
        :param  sourceLocation: rest api endpoint to get file from; it is also used to create folder path in user's home folder to save file to; i.e. for 'diags/' endpoint, /home/atip/diags/ will be created
        :param  fileName: name of file to be downloaded; file will be saved with same name in user's home folder; i.e. result will be  /home/atip/diags/fileName
                          fileName can be None when saving diagnostics file, which do not need file name, only 'diags/' endpoint
        :return absolute path to file
        '''

        # Store files in user's home folder
        fileLocation = str(Path.home()) + '/' + sourceLocation
        saveStatus = False

        # Check if folder exists in current user's home folder, otherwise create it
        if not os.path.exists('{}'.format(fileLocation)):
            os.makedirs('{}'.format(fileLocation))

        # Create empty file
        open('{}{}'.format(fileLocation, fileName), 'w').close()

        resp = FileOperations.getFile(webApiSession, sourceLocation + fileName)

        # Save file in location
        with open('{}{}'.format(fileLocation, fileName), 'wb') as f:
            for chunk in resp.iter_content(chunk_size=2048):
                if chunk:
                    f.write(chunk)
            saveStatus = True

        if saveStatus:
            print('File successfully saved to {}{}'.format(fileLocation, fileName))
        else:
            raise Exception('Nothing to save.')

        path = fileLocation + fileName

        return path

    @classmethod
    def unzipFileToFolder(cls, zipLocation):
        '''
        Unzip file from specified location to local folder
        :param zipLocation: absolute path to file; i.e. <user_home_folder>/capture/pcap.zip
        :return: folder location of unzipped content
        '''

        # Check if folder exists in current user's home folder
        if not os.path.exists('{}'.format(zipLocation)):
            raise Exception('Location does not exist.')

        # Get folder name where zip file content will be extracted
        folderLocation = os.path.splitext(zipLocation)[0]

        # Create folder to extract zip file
        os.makedirs('{}'.format(folderLocation))

        # Create zipfile object
        zipFile = zipfile.ZipFile(zipLocation)

        # Extract file to current dir
        zipFile.extractall(folderLocation)

        # Close file
        zipFile.close()

        # Remove zip file
        os.remove(zipLocation)

        return folderLocation + '/'

    @classmethod
    def deleteFile(cls, location):
        '''
        Delete file in location
        :param location: absolute path to file
        '''
        # Check if path exists
        if not os.path.exists('{}'.format(location)):
            raise Exception('Location does not exist.')

        # Remove file
        print('\r\nRemoving file {}...'.format(location))
        os.remove(location)
        print('Done.')

    @classmethod
    def deleteFolder(cls, location):
        '''
        Delete folder in location; will remove folder no matter if empty or not
        :param location: absolute path to folder
        '''

        # Check if path exists or is not /
        if location == '/':
            raise Exception('Cannot delete root folder.')
        elif not os.path.exists('{}'.format(location)):
            raise Exception('Location does not exist.')

        folderContent = cls.getFolderFileList(location)

        if folderContent:
            shutil.rmtree(location)
        else:
            os.rmdir(location)

    @classmethod
    def removePcapAndFolder(cls, webApiSession, pcapFolder):
        '''
        Delete local folder containing PCAP and delete all capture files from ATIP
        :param webApiSession: current session
        :param pcapFolder: absolute path to folder
        :return: nothing
        '''
        # remove PCAP folder
        cls.deleteFolder(pcapFolder)

        # delete capture files from ATIP
        Capture.deleteCaptureFiles(webApiSession)

    @classmethod
    def getFolderFileList(cls, location):
        '''
        Get file names from specific folder
        :param location: absolute path to folder
        :return: a list containing all file names contained into that folder
        '''

        # Check if folder path exists
        if not os.path.exists('{}'.format(location)):
            raise Exception('Location does not exist.')

        folderFileList = os.listdir(location)

        return folderFileList

    @classmethod
    def analyzePCAP(cls, pcapFolderLocation, pcapFilter):
        '''
        Analize a PCAP file using provided filter
        :param pcapFolderLocation: location of PCAP file
        :param pcapFilter: filter used to parse PCAP
        :return: a list of packets in conformance with filter; list of packets is a FileCapture object which pyshark returns; the only downside is that Python 3 cannot calculate len(FilterCapture) directly, it has to iterate over this list
        '''
        files = cls.getFolderFileList(pcapFolderLocation)
        pcapFile = files[0]
        pcapFilteredContent = pyshark.FileCapture(pcapFolderLocation + pcapFile, display_filter=pcapFilter, use_json=True)

        return pcapFilteredContent

    @classmethod
    def getFileInfo(cls, fileLocation):
        '''
        Get size and name for the first file in a folder;
        :param fileLocation: absolute path to folder containing the file to get details
        :return: size and name of first file
        '''
        files = cls.getFolderFileList(fileLocation)
        fileItem = files[0]

        return os.path.getsize(fileLocation+fileItem), fileItem

    @classmethod
    def sftpCopyToLinuxBox(cls, envVars, localPath, remotePath, action):
        '''
        Copy, using sftp, a file between local and remote location or vice versa
        :param localPath: local filename path to be copied, including filename
        :param remotePath: remote filename path to be copied, including filename
        :param action: used to indicate if get or put file; can have values of 'to' or 'from'
        :return: nothing; corresponding operation is performed according to action
        '''
        # initialize sftp connection
        sshSession = SshService(envVars.linuxConnectionConfig.host, envVars.linuxConnectionConfig.username, envVars.linuxConnectionConfig.password)
        sshSession.transport.connect(username=envVars.linuxConnectionConfig.username, password=envVars.linuxConnectionConfig.password)
        sftp = paramiko.SFTPClient.from_transport(sshSession.transport)

        if action == 'to':
            sftp.put(localPath, remotePath)
        elif action == 'from':
            sftp.get(remotePath, localPath)
        else:
            warnings.warn('Incorrect sftp action specified: {}'.format(action))
        sftp.close()
        sshSession.transport.close()


