from atipAutoFrwk.services.atip.LoginService import LoginService
from atipAutoFrwk.services.atip.LogoutService import LogoutService
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.webApi.atip.License import License


class LicenseService(object):

    @classmethod
    def setLicenseServer(cls, webApiSession, env, force=False):
        '''
        :type webApiSession: WebApiSession
        :type env: Environment
        :param force: If it's set to False the request isn't made if the ip from env is already set  
        '''

        if not force:
            licenseServerIp = License.getLicenseServer(webApiSession)
            if licenseServerIp == env.licenseServer:
                # all is well, nothing to be done
                return
        License.configureLicenseServerIp(webApiSession, env.licenseServer)
        LogoutService.waitUntilLoggedOut(webApiSession)
        LoginService.waitUntilLoggedIn(webApiSession, env.atipConnectionConfig.username, env.atipConnectionConfig.password)
        SystemService.waitUntilSystemReady(webApiSession)
