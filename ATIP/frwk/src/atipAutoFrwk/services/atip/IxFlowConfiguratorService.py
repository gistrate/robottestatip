import jsonpickle
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig
from atipAutoFrwk.config.atip.netflow.IxFlowAllReleaseFields import IxFlowAllReleaseFields
from atipAutoFrwk.config.atip.netflow.IxFlowDefaultFields import IxFlowDefaultFields


class IxFlowConfiguratorService(object):


    def configureAllReleaseFields(self, version):
        '''

        :param version: release version e.g 154
        :return: netflowObj
        '''
        config = IxFlowAllReleaseFields()
        netflowObj = self.configureNetflowObj(config, version)
        return netflowObj

    def configureDefaultValues(self, version):
        '''

        # :param version: release version e.g 154
        :return: netflowObj
        '''
        config = IxFlowDefaultFields()
        netflowObj = self.configureNetflowObj(config,version)
        return netflowObj



    def configureNetflowObj(self, config, version):
        '''

        # :param version: release version e.g 154
        :return: netflowObj
        '''
        netflowObj = NetflowConfig()
        netflowObj.resetIxFlowFields(netflowObj)
        functionName= 'getFields_v{}'.format(version)
        func1 = getattr(config, functionName, None)
        if callable(func1):
            fields=func1()
        for field in fields:
                setattr(netflowObj, field, True)
        print("netflowObj {}".format(jsonpickle.encode(netflowObj)))
        return netflowObj




