
from  atipAutoFrwk.webApi.atip.InstalledApps import InstalledApps

class StaticApp(object):
    @classmethod
    def checkForInstalledApp(cls, webApiSession, appName, groupId=0):
        staticApp = InstalledApps.searchInstalledApps(webApiSession, appName, groupId)
        if staticApp != {}:
            appList = staticApp['apps']
        else:
            return False

        if ('list' not in str(type(appList))):
            appList = [appList]
        found = False
        for app in appList:
            if app['id'] == appName:
                found = True
                break

        if not found:
            raise Exception('App {} not in the current list'.format(appName))

        return found
