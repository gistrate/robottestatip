
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.webApi.atip.Status import Status


class LogoutService(object):

    @classmethod
    def waitUntilLoggedOut(cls, webApiSession):
        ':type webApiSession: WebApiSession'

        isLoggedOutLambda = lambda: Status.isLoggedOut(webApiSession) is True
        TestCondition.waitUntilTrue(isLoggedOutLambda, errorMessage="The user wasn't logged out!", totalTime=150, iterationTime=5)
        #  it is sometimes still logged in after the first 401 response
        TestCondition.waitUntilTrue(isLoggedOutLambda, errorMessage="The user wasn't logged out!", totalTime=150, iterationTime=5)