from http import HTTPStatus
import requests
import warnings

import jsonpickle

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType
from atipAutoFrwk.services.atip.FiltersService import FiltersService
from atipAutoFrwk.services.atip.GroupsService import GroupsService
from atipAutoFrwk.services.atip.LicenseService import LicenseService
from atipAutoFrwk.services.atip.SystemService import SystemService
from atipAutoFrwk.services.atip.stats.DebugPage import DebugPage
from atipAutoFrwk.webApi.atip.Apps import Apps
from atipAutoFrwk.webApi.atip.DataMasking import DataMasking
from atipAutoFrwk.webApi.atip.Dedup import Dedup
from atipAutoFrwk.webApi.atip.Filters import Filters
from atipAutoFrwk.webApi.atip.Groups import Groups
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.MplsParsing import MplsParsing
from atipAutoFrwk.webApi.atip.Netflow import Netflow
from atipAutoFrwk.webApi.atip.PayloadMasks import PayloadMasks
from atipAutoFrwk.webApi.atip.Update import Update


class AtipConfigService(object):
    '''
    Contains methods that can be used to configure an atip server. 
    '''

    @classmethod
    def initAtip(cls, webApiSession, env, atipConfig=None):
        '''
        Log in (and change the default password on vATIP). 
        Set the system info on the provided atipConfig.
        Set the licensing server.

        :type webApiSession: WebApiSession
        :type env: Environment
        :type atipConfig: AtipConfig
        '''

        if not atipConfig:
            atipConfig = AtipConfig()
        # Log in
        if env.atipType == AtipType.HARDWARE:
            Login.login(webApiSession, env.atipConnectionConfig.username, env.atipConnectionConfig.password)
        elif env.atipType == AtipType.VIRTUAL:
            try:
                # and change the default password
                Login.defaultLoginChangePassword(webApiSession, env.atipDefaultPassword, env.atipConnectionConfig.password, env.atipConnectionConfig.username)
            except requests.exceptions.RequestException:
                warnings.warn("Couldn't log in with the default password. Assume the password has already been changed...")
                Login.login(webApiSession, env.atipConnectionConfig.username, env.atipConnectionConfig.password)
        else:
            warnings.warn("Unknown ATIP type {}; will skip initialization...".format(env.atipType))
        # Set the system info
        atipConfig.systemInfo = Update.getSystemInfo(webApiSession)

        # Set the license manager on vATIP if it's different than what it's set to now
        if env.atipType == AtipType.VIRTUAL:
            LicenseService.setLicenseServer(webApiSession, env)

        return atipConfig

    @classmethod
    def createConfig(cls, webApiSession, atipConfig, env=None):
        '''
        Creates the atipConfig and clears the previous values for the given settings.
        Assumes the webApiSession is valid, and that the user is logged in. Env should be mandatory.
        :type webApiSession: WebApiSession
        :type atipConfig: AtipConfig
        '''

        if not env:
            warnings.warn("Please pass env param")
            env = Environment()

        cls.createGroups(webApiSession, atipConfig)

        cls.setNetflowAcceleration(webApiSession, atipConfig)

        cls.createNetflowGlobalConfig(webApiSession, atipConfig)
        cls.createNetflowCardConfig(webApiSession, atipConfig, env)
        cls.createNetflowCollectorConfig(webApiSession, atipConfig)

        cls.createDataMasking(webApiSession, atipConfig)
        cls.createHeaderMasks(webApiSession, atipConfig)
        cls.createPayloadMasks(webApiSession, atipConfig)
        cls.updateFiltersConfig(webApiSession, atipConfig)
        cls.createFilters(webApiSession, atipConfig, env)
        cls.createMplsParsing(webApiSession, atipConfig)
        cls.createDedup(webApiSession, atipConfig)

        print('Done creating ATIP config: {}'.format(jsonpickle.encode(atipConfig)))

    @classmethod
    def setNetflowAcceleration(cls, webApiSession, atipConfig):
        for netflowAcceleration in atipConfig.netflowAcceleration:
            GroupsService.updateReference(netflowAcceleration)
            path = '{}/configureStats'.format(netflowAcceleration.groupId)
            webApiSession.post(path=path, reqData=netflowAcceleration)

    @classmethod
    def createGroups(cls, webApiSession, atipConfig):
        '''
        :type webApiSession: WebApiSession
        :type atipConfig: AtipConfig
        '''
        groups = Groups.getGroups(webApiSession)
        if len(groups.get('groups')) > 1:
            GroupsService.clearGroups(webApiSession)

        for group in atipConfig.groups:
            if (group.id is None) or (group.id != 0):
                Groups.addGroup(webApiSession, group)
                group.id = Groups.getGroupId(webApiSession, group.name)

        SystemService.waitUntilSystemReady(webApiSession)

    @classmethod
    def createFilters(cls, webApiSession, atipConfig, env):
        if not atipConfig.filterList:
            # nothing to do
            return
        # clear previous config
        Filters.deleteAllFilters(webApiSession)
        # check no rules on the system (except for 'Unmatched traffic')
        for crtNp in env.atipSlotNumber:
            DebugPage.checkNpRules(webApiSession, 1, crtNp)

        # Update group reference for all filters
        for crtFilter in atipConfig.filterList:
            GroupsService.updateReference(crtFilter)

        # Create all filters
        # kind of hack-y, this will update only the default group
        FiltersService.createFilter(webApiSession, atipConfig.filterList, 0)
        # update all groups with the new rules 
        for groupId in Groups.getAllGroups(webApiSession):
            Filters.rulePush(webApiSession, groupId=groupId.id)

    @classmethod
    def createDataMasking(cls, webApiSession, atipConfig):
        ':type atipConfig: AtipConfig'

        for dataMasking in atipConfig.dataMasking:
            GroupsService.updateReference(dataMasking)
            if dataMasking.enabled == True:
                DataMasking.updateDataMasking(webApiSession, dataMasking)

    @classmethod
    def createHeaderMasks(cls, webApiSession, atipConfig):
        ':type atipConfig: AtipConfig'

        if not atipConfig.headerMasks.headerMaskList:
            # nothing to do
            return

        # clear previous config
        for group in atipConfig.groups:
            DataMasking.deleteHeaderMasks(webApiSession, groupId=group.id)

        for key, value in atipConfig.headerMasks.headerMaskList.items():
            GroupsService.updateReference(value)
            DataMasking.addHeader(webApiSession, value)

    @classmethod
    def createPayloadMasks(cls, webApiSession, atipConfig):
        ':type atipConfig: AtipConfig'

        if not atipConfig.payloadMasks.payloadMaskList:
            # nothing to do
            return
        # clear previous config
        for group in atipConfig.groups:
            PayloadMasks.deletePayloadMasks(webApiSession, groupId=group.id)

        for key, value in atipConfig.payloadMasks.payloadMaskList.items():
            GroupsService.updateReference(value)
            PayloadMasks.addPayload(webApiSession, value)

    @classmethod
    def createMplsParsing(cls, webApiSession, atipConfig):
        ':type atipConfig: AtipConfig'

        for mplsCfg in atipConfig.mplsConfig:
            GroupsService.updateReference(mplsCfg)
            if mplsCfg.enabled == True:
                MplsParsing.updateMplsParsing(webApiSession, mplsCfg)

    @classmethod
    def updateFiltersConfig(cls, webApiSession, atipConfig):
        '''
            Converts Filters MaskUUIDS from Mask name to corresponding Mask ID. MaskUUIDS were populated with Mask names instead of their ID's because Masks needed to be created
            before getting their IDs and use them to create filters. MaskUUIDS can be 'headerMaskUUIDS' or 'regexMaskUUIDS'

            maskType: type of filter mask to be converted; can get following values: 'headerMask' or 'payloadMask'
        '''

        ':type atipConfig: AtipConfig'

        maskNameToId = {}
        maskUUIDS, maskCat, maskDict = '', '', ''

        if atipConfig.headerMasks.headerMaskList:
            maskUUIDS = 'hdrMaskUUIDS'
            maskCat = 'headerMasks'
            maskDict = 'headerMaskList'
        elif atipConfig.payloadMasks.payloadMaskList:
            maskUUIDS = 'regexMaskUUIDS'
            maskCat = 'payloadMasks'
            maskDict = 'payloadMaskList'

        if atipConfig.filterList == []:
            # nothing to do
            return

        if maskCat and maskDict:
            maskList = getattr(getattr(atipConfig, maskCat), maskDict)
        else:
            return

        if not maskList:
            # nothing to do
            return

        for crtMask in maskList:
            mask = getattr(getattr(atipConfig, maskCat), maskDict)[crtMask]
            maskNameToId.setdefault(mask.groupId, {})[crtMask] = mask.id

        print('\r\nMaskUUIDS name => ID to convert: {}'.format(maskNameToId))

        for i in range(len(atipConfig.filterList)):
            if isinstance(getattr(atipConfig.filterList[i], maskUUIDS), list):
                # check every filterlist item to verify if it contains a Mask ID or Mask name
                for crtMaskUUIDS in range(len(getattr(atipConfig.filterList[i], maskUUIDS))):
                    # if existing filter has already MaskUUIDS as Mask ID, skip
                    if isinstance(getattr(atipConfig.filterList[i], maskUUIDS)[crtMaskUUIDS], int):
                        # skip to next filterList MaskUUIDS item
                        continue
                    elif getattr(atipConfig.filterList[i], maskUUIDS)[crtMaskUUIDS] in maskNameToId[atipConfig.filterList[i].groupId]:
                        # convert current Mask UUID from Mask name to Mask ID
                        getattr(atipConfig.filterList[i], maskUUIDS)[crtMaskUUIDS] = maskNameToId[atipConfig.filterList[i].groupId][getattr(atipConfig.filterList[i], maskUUIDS)[crtMaskUUIDS]]
            else:
                # filterlist does not contain a list of filters, but just one
                if isinstance(getattr(atipConfig.filterList[i], maskUUIDS), int):
                    # skip to next filterList MaskUUIDS item
                    continue
                else:
                    setattr(atipConfig.filterList[i], maskUUIDS, maskNameToId[atipConfig.filterList[i].groupId][getattr(atipConfig.filterList[i], maskUUIDS)])

        return atipConfig

    @classmethod
    def createNetflowGlobalConfig(cls, webApiSession, atipConfig):
        ':type atipConfig: AtipConfig'

        for netflowGlobal in atipConfig.netflowGlobal:
            GroupsService.updateReference(netflowGlobal)
            if netflowGlobal.enabled == True:
                Netflow.updateGlobalConfig(webApiSession, netflowGlobal, HTTPStatus.OK)

    @classmethod
    def createNetflowCardConfig(cls, webApiSession, atipConfig, env, indexSlotNumber=0):
        ':type atipConfig: AtipConfig'
        if env.atipType == AtipType.VIRTUAL:
            return
        if not env:
            env = Environment()

        for netflowCards in atipConfig.netflowCards:
            GroupsService.updateReference(netflowCards)
            cardListId = int(env.atipSlotNumber[indexSlotNumber])
            if netflowCards.cardList[cardListId].enabled != '':
                Netflow.updateCard(webApiSession, netflowCards.cardList[cardListId], groupId=netflowCards.groupId)

    @classmethod
    def createNetflowCollectorConfig(cls, webApiSession, atipConfig):
        ':type atipConfig: AtipConfig'
        for netflowCollectors in atipConfig.netflowCollectors:
            GroupsService.updateReference(netflowCollectors)
            for crtCollector in netflowCollectors.collectorList:
                if crtCollector.enabled:
                    Netflow.updateCollectorConfig(webApiSession, crtCollector, groupId=netflowCollectors.groupId)

    @classmethod
    def updateFiltersActions(cls, webApiSession, filterObj):
        '''
        Updates the filter action names to ids
        :type atipConfig: AtipConfig
        '''

        allAppsAndActions = Apps.getAppsList(webApiSession)
        for condition in filterObj.conditions:
            if condition.get('type') == FilterConditionType.APP.conditionType:
                # found conditions of this type, append the new ones
                for app in condition.get(FilterConditionType.APP.listName):
                    for action in app.get("actions"):
                        action["id"] = Apps.getActionId(webApiSession, app.get("id"), action.get("id"))

        return filterObj

    @classmethod
    def createDedup(cls, webApiSession, atipConfig):
        '''
        Creates dedup configuration
        :type atipConfig: AtipConfig
        '''
        if atipConfig.dedup.enabled == False:
            # nothing to do
            return
        Dedup.updateDedup(webApiSession, atipConfig.dedup)
        print(atipConfig.dedup)

