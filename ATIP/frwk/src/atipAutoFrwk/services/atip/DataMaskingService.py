from atipAutoFrwk.config.atip.DataMaskingConfig import DataMaskingConfig
from atipAutoFrwk.config.atip.HeaderMask import HeaderMask
from atipAutoFrwk.config.atip.HeaderMaskConfig import HeaderMaskConfig
from atipAutoFrwk.config.atip.PayloadMask import PayloadMask
from atipAutoFrwk.config.atip.PayloadMaskConfig import PayloadMaskConfig
from atipAutoFrwk.services.atip.GroupsService import GroupsService
from atipAutoFrwk.webApi.atip.DataMasking import DataMasking
from atipAutoFrwk.webApi.atip.MplsParsing import MplsParsing
from atipAutoFrwk.webApi.atip.PayloadMasks import PayloadMasks
from atipAutoFrwk.config.atip.MplsParsingConfig import MplsParsingConfig

class DataMaskingService(object):
    '''
    Contains methods that can be used to configure data masking.
    '''

    @classmethod
    def configureDataMasking(cls, webApiSession, enabled=True, maskChar='?', groupId=0):

        dataMasking = DataMaskingConfig(enabled, maskChar, groupId=groupId)
        DataMasking.updateDataMasking(webApiSession, dataMasking)

    @classmethod
    def createHeaderMask(cls, webApiSession, maskName, length, header, startOffset, groupId=0):

        headerMask = HeaderMask(startOffset, 0, maskName, header, length, groupId=groupId)
        DataMasking.addHeader(webApiSession, headerMask)

    @classmethod
    def createCustomPayloadMask(cls, webApiSession, maskName, regex, offsetStart, offsetEnd, creditCard, groupId=0):

        payloadMask = PayloadMask(regex, 0, offsetStart, maskName, offsetEnd, False, id, creditCard, groupId=groupId)
        PayloadMasks.addPayload(webApiSession, payloadMask)

    @classmethod
    def configurePayloadMask(self, webApiSession, offsetEnd, regex, maskName, offsetStart, creditCard, groupId=0):

        payloadMask = PayloadMask(regex, 0, offsetStart, maskName, offsetEnd, False, 0, creditCard, groupId=groupId)

        payloadMasks = PayloadMaskConfig()
        payloadMasks.payloadMaskList[payloadMask.name] = payloadMask

        if regex != None:

            for key, value in payloadMasks.payloadMaskList.items():
                GroupsService.updateReference(value)
                PayloadMasks.addPayload(webApiSession, value)

        else:
            PayloadMasks.updatePayloadConfig(webApiSession, payloadMask=payloadMask, groupId=groupId)

        return payloadMask

    @classmethod
    def configureHeaderMask(self, webApiSession, mplsType, maskName, length, header, startOffset, groupId=0):

        if mplsType:
            mplsParsing = MplsParsingConfig(True, mplsType, False, groupId=groupId)
            GroupsService.updateReference(mplsParsing)
            if mplsParsing.enabled == True:
                MplsParsing.updateMplsParsing(webApiSession, mplsParsing)

        headerMask = HeaderMask(startOffset, 0, maskName, header, length, -1, groupId)

        HeaderMasks = HeaderMaskConfig()
        HeaderMasks.headerMaskList[headerMask.name] = headerMask

        for key, value in HeaderMasks.headerMaskList.items():
            GroupsService.updateReference(value)
            DataMasking.addHeader(webApiSession, value)



        return headerMask