import paramiko
import time
import warnings


class SshService:

    def __init__(self, hostname,username,password=None,keyFile=None, port=22):
        print('Connecting %s@%s:%s' % (username, hostname, port))
        self.client = paramiko.client.SSHClient()
        self.transport = paramiko.Transport(hostname,port)
        self.hostname = hostname
        self.username = username
        self.password = password
        self.keyFile = keyFile
        self.port = port

    def connectWithPassword(self):
        try:
            self.client.set_missing_host_key_policy(paramiko.client.AutoAddPolicy())
            self.client.connect(hostname=self.hostname, username=self.username, password=self.password, port=self.port, look_for_keys = False, auth_timeout=10)
            print('Connected %s@%s:%s' % (self.username, self.hostname, self.port))
        except paramiko.ssh_exception.AuthenticationException as authException:
            print('Authentication failed, please verify your credentials !')
            raise authException
        except paramiko.SSHException as sshException:
            print("Unable to establish SSH connection: %s" % sshException)
            raise sshException

    def connectWithPrivateKey(self):
        try:
            self.client.set_missing_host_key_policy(paramiko.client.AutoAddPolicy())
            self.client.load_system_host_keys()
            privKey = paramiko.RSAKey.from_private_key_file(self.keyFile)
            self.client.connect(hostname=self.hostname, username=self.username, pkey=privKey, port=self.port, look_for_keys = False, auth_timeout=10)
            print('Connected %s@%s:%s' % (self.username, self.hostname, self.port))
        except paramiko.ssh_exception.BadHostKeyException as badHostKeyException:
            print("Unable to verify server's host key: {}".format(badHostKeyException))
            raise badHostKeyException
        except paramiko.SSHException as sshException:
            print("Unable to establish SSH connection: %s" % sshException)
            raise sshException

    def closeConnection(self):
        if (self.client != None):
            self.client.close()

    def openShell(self):
        self.shell = self.client.invoke_shell()

    def sendShell(self, command, sudo=False):
        if sudo:
            fullcmd = "echo " + self.password + " |   sudo -S -p '' " + command
        else:
            fullcmd = command
        sshTransport = self.client.get_transport()
        sshChanel = sshTransport.open_session()
        sshChanel.set_combine_stderr(True)

        if sudo:
            sshChanel.get_pty()

        sshChanel.exec_command(fullcmd)

        stdOut, stdErr = b"", b""
        exitStatus = None

        while not sshChanel.exit_status_ready():
            time.sleep(2)
            if sshChanel.recv_ready():
                stdOut += sshChanel.recv(10000)

            if sshChanel.recv_stderr_ready():
                stdErr += sshChanel.recv_stderr(10000)

        exitStatus = sshChanel.recv_exit_status()
        # Need to get any remaining output after command execution terminates
        while sshChanel.recv_ready():
            stdOut += sshChanel.recv(5000)

        while sshChanel.recv_stderr_ready():
            stdErr += sshChanel.recv_stderr(5000)

        if exitStatus != 0:
            warnings.warn('Command {} did not execute successfully, error: {}'.format(command, stdErr.decode('utf-8')),Warning)

        # ignore unrecognizable output using 'ignore' option for decode
        return stdOut.decode('utf-8','ignore'), exitStatus