import re

from atipAutoFrwk.data.traffic.stats.BpsRealTimeStatsContainer import BpsRealTimeStatsContainer
from atipAutoFrwk.data.traffic.stats.BpsRealTimeStatsItem import BpsRealTimeStatsItem


class ConversionService(object):
    '''
    The class contains methods that try to convert data from different formats into framework objects.
    '''

    @classmethod
    def createBasicObject(cls, values, objClass):
        '''
        Generic method that creates an object of the given type from a dictionary. 
        The members have the same types as the dictionary.
        Restrictions: works only with simple values (one key with multiple values and no lists). The class must have
        the exact field names as in the dictionary.
        :param values: the dictionary (e.g.: the ones retrieved from the WebApi responses.)
        :param objClass: the class that will be instantiated
        :return: an instance of the objClass class
        '''

        newObj = objClass()
        newObj.__dict__.update(values)
        return newObj

    @classmethod
    def createStatsContainer(cls, stats, statsContainerClass):
        '''
        Generic class that creates a statsContainer of the given type from a dictionary. 
        Tries to convert the values into floats. If the conversion isn't successful, their dictionary type is kept.
        Restrictions: works only with simple values (one key with multiple values and no lists). The class must have the exact field names as in the dictionary.
        :param stats: the dictionary containing stats (as received from webApi)
        :param statsContainerClass: the class that will be instantiated
        :return: an instance of the statsContainerClass class
        '''

        statsContainer = statsContainerClass()
        for key, val in stats.items():
            keyStat = key.replace('.', '_')

            if val:
                try:
                    val = float(val)
                except (ValueError, TypeError):
                    # couldn't convert it to float, so we're keeping it as it is
                    pass
                statsContainer.__setattr__(keyStat, val)
        return statsContainer


    @classmethod
    def createStatsList(cls, statsList, statsItemClass):
        '''
        Generic class that creates a list of statsItems of the given type from a list of dictionaries. 
        Tries to convert the values of each item into floats. If the conversion isn't successful, their dictionary type is kept.
        Restrictions: works only with simple values (one key with multiple values and no lists). The class must have the exact field names as in the dictionary.
        :param statsList: the dictionary containing stats (as received from webApi)
        :param statsItemClass: the class that will be instantiated
        :return: a list of statsItemClass objects
        '''
        print("statsList {}".format(type(statsList)))
        statsItemsList = list()
        if not statsList:
            return statsItemsList
        # webapi endpoints will not return a list if there is only one element
        if type(statsList) != list:
            statsList=[statsList]
        for statItem in statsList:
            if type(statItem) is not statsItemClass:
                print(" stats Item type {} and value {}".format(type(statItem), statItem))
                statsItemsList.append(cls.createStatsContainer(statItem, statsItemClass))
        return statsItemsList

    @staticmethod
    def createBpsRTSContainer(output):
        stats = output.get('rts')
        match = re.search('time=([^ ]*)', stats)
        time = match.group(1)
        progress = output.get('progress')
        bpsRtsContainer = BpsRealTimeStatsContainer
        bpsRtsItem = BpsRealTimeStatsItem
        bpsRtsContainer.time = time
        bpsRtsContainer.progress = progress
        startIndex = stats.find('[')
        for word in stats[startIndex:].strip('[]').split(' '):
            key, value = word.split('=')
            value = float(value.strip("'"))
            setattr(bpsRtsItem, key, value)
        bpsRtsContainer.values = bpsRtsItem
        return bpsRtsContainer
