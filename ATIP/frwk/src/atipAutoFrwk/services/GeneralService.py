from atipAutoFrwk.data.atip.AtipType import AtipType
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.webApi.atip.Login import Login
from atipAutoFrwk.webApi.atip.NP import NP
from atipAutoFrwk.webApi.atip.Status import Status
from atipAutoFrwk.webApi.traffic.BpsOperations import BpsOperations
from atipAutoFrwk.webApi.traffic.Login import Login as BpsLogin
from atipAutoFrwk.webApi.traffic.PortsOperations import PortsOperations
import time


class GeneralService(object):

    @classmethod
    def configureAtip(cls, atipSession, atipConfig, env=None):
        Login.login(atipSession)
        AtipConfigService.createConfig(atipSession, atipConfig, env)

    @classmethod
    def runBpsTest(cls, bpsSession, env, bpsTestConfig):
        BpsLogin.login(bpsSession)
        PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)
        bpsTestConfig.testId = BpsOperations.runBpsTest(bpsSession, bpsTestConfig.testName, env.bpsPortGroup, bpsTestConfig.testDuration)
        return bpsTestConfig.testId

    @classmethod
    def getCurrentTime(cls):
        theTime = time.time()
        return theTime

    @classmethod
    def getAtipTime(cls, atipSession):
        theTime =Status.getStatus(atipSession)
        return theTime

    @classmethod
    def startBpsTestAndWaitForTraffic(cls, bpsSession, env, bpsTestConfig):
        '''
        Start BPS test and wait until traffic is sent
        :param bpsSession: current BPS session
        :param env: environment parameters
        :param bpsTestConfig: bps config to use
        :return: nothing
        '''
        BpsLogin.login(bpsSession)
        PortsOperations.reserveBpsPorts(bpsSession, env.bpsSlot, env.bpsPortList, env.bpsPortGroup)
        testId = BpsOperations.startBps(bpsSession, bpsTestConfig.testName, env.bpsPortGroup)
        bpsStatus = BpsOperations.getBpsTestProgress(bpsSession, testId)
        print('bpsStatus: {}'.format(bpsStatus))
        while bpsStatus <= 1:
            print('Sleeping 1s')
            time.sleep(1)
            bpsStatus = BpsOperations.getBpsTestProgress(bpsSession, testId)

        return testId