from datetime import datetime
from time import time, sleep
import types
from cmath import isclose


class TestCondition(object):
    '''
    Contains helper methods for checking general conditions.
    '''

    @classmethod
    def waitUntilTrue(cls, condition, errorMessage = None, onFailure = None, totalTime = 30, iterationTime = 1):
        '''
        Calls the lambda function 'condition' until it resolves to true, or the timeout is reached. In the latter case, an Exception is thrown.
        
        Keyword arguments:
        condition -- lambda expression with no arguments
        errorMessage -- the message that is thrown in the Exception
        onFailure -- lambda expression with no arguments
        totalTime -- the total amount of time to check the conditon; in seconds
        iterationTime -- the amount of time between invocations of the lambda function; in seconds
        '''

        mustEnd = time() + totalTime
        while (not condition()) and (time() < mustEnd):
            sleep(iterationTime)
        
        if (not condition()) and mustEnd < time():
            if isinstance(onFailure, types.LambdaType):
                onFailure()
            raise Exception(errorMessage)


    @classmethod
    def waitUntilFalse(cls, condition, errorMessage=None, onFailure=None, totalTime=30, iterationTime=1):
        '''
        Calls the lambda function 'condition' until it resolves to False, or the timeout is reached. In the latter case, an Exception is thrown.

        Keyword arguments:
        condition -- lambda expression with no arguments
        errorMessage -- the message that is thrown in the Exception
        onFailure -- lambda expression with no arguments
        totalTime -- the total amount of time to check the conditon; in seconds
        iterationTime -- the amount of time between invocations of the lambda function; in seconds
        '''

        mustEnd = time() + totalTime
        while (condition()) and (time() < mustEnd):
            sleep(iterationTime)

        if (condition()) and mustEnd < time():
            if isinstance(onFailure, types.LambdaType):
                onFailure()
            raise Exception(errorMessage)


    @classmethod
    def computeShare(cls,myList):
        totalShare = sum(myList)
        print(totalShare)
        if not isclose(totalShare, 1.0, rel_tol=0.01):
            raise Exception('The total share {} is not correct.'.format(totalShare))
        else:
            return totalShare

    @staticmethod
    def getCurrentTime():
        return int((datetime.utcnow().timestamp() * 1000))

    @classmethod
    def compareWithTolerance(cls, actualValue, expectedValue, tolerance=0.05):
        '''
            Compares two values using a tolerance.

            Keyword arguments:
            actualValue
            expectedValue
            tolerance --  it's a relative value, and default is 0.05 meaning 5%
        '''
        if not isclose(actualValue, expectedValue, rel_tol=tolerance):
            raise Exception('The actual value {} is not correct. Expected value is {}.'.format(actualValue, expectedValue))
        else:
            return actualValue