#!/bin/bash

#BRANCH_NAME="155"
echo "Current branch to be installed $BRANCH_NAME"
#WORKSPACE

#rm $WORKSPACE/ATIP/frwk/src/atipAutoFrwk/utils/deploy/buildNumber.*
rm buildID.txt
rm *.ova

#Get the lastSuccessfulBuild ID
wget -v  http://build-master-0.ann.is.keysight.com:8080/job/vATIP-$BRANCH_NAME-OVA/lastSuccessfulBuild/buildNumber -O buildID.txt

#Error out of test if unable to lastSuccessfulBuild ID.
if [ $? -ne 0 ]; then
    echo "ERROR: lastSuccessfulBuild ID was unable to be retrieved."
    exit 1
fi

#Construct the appropriate OVA filename.
if [ "$BRANCH_NAME" == "unstable" ]; then
    LASTBUILD="$(cat buildID.txt)"
    IMAGE_NAME="CloudLens-vATIP-$BRANCH_NAME-$LASTBUILD.ova"
else
    LASTBUILD="$(cat buildID.txt)"
    BRANCH=$(echo $BRANCH_NAME | sed 's/./&./g;s/.$//')
    echo "branch is .."
    echo $BRANCH
    IMAGE_NAME="CloudLens-vATIP-r$BRANCH-$LASTBUILD.ova"
fi

if [ "$BRANCH_NAME" == "unstable" ]; then
   echo "Downloading vATIP image - $IMAGE_NAME ..."
   #Download the appropriate OVA image.
   wget http://build-filesrv-0.ann.is.keysight.com/builds/vatip/$BRANCH_NAME/latest/$IMAGE_NAME
   #Error out of test if unable to retrieve correct build.
   if [ $? -ne 0 ]; then
      echo "ERROR: Image $IMAGE_NAME was unable to be retrieved."
      exit 1
   fi
else
   echo "Downloading vATIP image - $IMAGE_NAME ..."
   #Download the appropriate OVA image.
   wget http://build-filesrv-0.ann.is.keysight.com/builds/vatip/releases/r$BRANCH/latest/$IMAGE_NAME
   #Error out of test if unable to retrieve correct build.
   if [ $? -ne 0 ]; then
      echo "ERROR: Image $IMAGE_NAME was unable to be retrieved."
      exit 1
   fi
fi

#Power off, overwrite, and install the latest vPB OVA on the ESXi host.
echo "Deploying vATIP image - $IMAGE_NAME ..."
ovftool \
--quiet \
--overwrite \
--powerOffTarget \
--name="vatip_automation" \
--net:'bridged=VM Network' \
--net:'net1=PlainAppForwarding' \
--net:'net2=HW_BPS' \
--powerOn \
--noSSLVerify \
--X:waitForIp \
$IMAGE_NAME \
vi://root:atip1234@10.38.185.47

echo "vATIP image - $IMAGE_NAME got deployed."
echo "Wating for vATIP to start ..10 minutes   "
sleep 10m
echo "vATIP Started - Enjoy!   "




