import paramiko
import os
import sys
import argparse
import urllib
import subprocess
import requests
import wget
import time


SERVER_QCOW2_FILE_NAME = 'CloudLens_vATIP_automation.qcow2'
SERVER_VM_NAME = 'CloudLens_vATIP_automation'
MGMT_BR = 'mircea'
BPS_BR='bps'
FWD_BR='mircea_net1'

def connect_to_kvm(kvm_server, user, password):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(kvm_server, username=user, password=password)
    return client
    
def getLatestSuccessfulBuild(job):
    url = 'http://build-master-0.ann.is.keysight.com:8080/job/'+job+'/lastSuccessfulBuild/buildNumber'
    r = requests.get(url)
    return r.text


def download_qcow2_to_kvm(server, dest_path, branch, build_nr, job, dest_file_name):
    last_build_nr = str(build_nr)
    url = 'http://build-filesrv-0.ann.is.keysight.com/builds/vatip/'+branch+'/'
    if 0 == build_nr:
        last_build_nr = getLatestSuccessfulBuild(job)
        url += last_build_nr+'/'
    else:
        url += build_nr + '/'
    server_file_name = 'CloudLens-vATIP-'+branch+'-'+last_build_nr+'.qcow2'
    url += server_file_name
    #download file from server
    print('wget '+url)
    filename = wget.download(url)
    print('run on server: rm -f '+dest_path+'/'+dest_file_name)
    stdin,stdout,stderr = server.exec_command('rm -f '+dest_path+'/'+dest_file_name)
    print(stdout.read())
    print(stderr.read())
    print('sftp put '+filename)
    sftp = server.open_sftp()
    sftp.put(filename, dest_path+'/'+dest_file_name)
    sftp.close()
	# removing downloaded image
    print('removing file '+filename)
    os.remove(filename)
    return (dest_file_name)
    
def deploy_qcow2_to_kvm(server, dest_path, dest_file_name, vm_name):
    print('run on server: '+'virsh list --all')
    stdin,stdout,stderr = server.exec_command('virsh list --all')
    print(stdout.read())
    print(stderr.read())
    print('run on server: '+'virsh destroy '+vm_name)
    stdin,stdout,stderr = server.exec_command('virsh destroy '+vm_name)
    print(stdout.read())
    print(stderr.read())
    print('run on server: '+'virsh undefine '+vm_name)
    stdin,stdout,stderr = server.exec_command('virsh undefine '+vm_name)
    print(stdout.read())
    print(stderr.read())
#    print('run on server: '+'virt-install --name '+vm_name+' --os-variant=ubuntutrusty --ram 16384 --vcpus 6 --cpu host --network bridge='+MGMT_BR+',mac=52:54:00:8b:1f:77,model=virtio --network bridge='+BPS_BR+',model=virtio --network network='+FWD_BR+',model=virtio,driver_queues=3 --disk path='+dest_path+'/'+dest_file_name+',size=30,format=qcow2 --import --graphics vnc --console pty,target_type=serial')
# driver_queues=3 is supported only on >=16.04
#    stdin,stdout,stderr = server.exec_command('virt-install --name '+vm_name+' --os-variant=ubuntutrusty --ram 16384 --vcpus 6 --cpu host --network bridge='+MGMT_BR+',mac=52:54:00:8b:1f:77,model=virtio --network bridge='+BPS_BR+',model=virtio --network network='+FWD_BR+',model=virtio,driver_queues=3 --disk path='+dest_path+'/'+dest_file_name+',size=30,format=qcow2 --import --graphics vnc --console pty,target_type=serial')
    print('run on server: '+'virt-install --name '+vm_name+' --os-variant=ubuntutrusty --ram 16384 --vcpus 6 --cpu host --network bridge='+MGMT_BR+',mac=52:54:00:03:d9:5e,model=virtio --network bridge='+BPS_BR+',model=virtio --network network='+FWD_BR+',model=virtio --disk path='+dest_path+'/'+dest_file_name+',size=30,format=qcow2 --import --graphics vnc --console pty,target_type=serial --noreboot')
    stdin,stdout,stderr = server.exec_command('virt-install --name '+vm_name+' --os-variant=ubuntutrusty --ram 16384 --vcpus 6 --cpu host --network bridge='+MGMT_BR+',mac=52:54:00:03:d9:5e,model=virtio --network bridge='+BPS_BR+',model=virtio --network network='+FWD_BR+',model=virtio --disk path='+dest_path+'/'+dest_file_name+',size=30,format=qcow2 --import --graphics vnc --console pty,target_type=serial --noreboot')
    print(stdout.read())
    print(stderr.read())
    #add queues to tx interface
    print('run on server: '+'virsh dumpxml '+vm_name+' > '+dest_path+'/vmxml')
    stdin,stdout,stderr = server.exec_command('virsh dumpxml '+vm_name+' > '+dest_path+'/vmxml')
    print(stdout.read())
    print(stderr.read())
    print('sftp get '+dest_path+'/vmxml')
    sftp = server.open_sftp()
    sftp.get(dest_path+'/vmxml', './vmxml')
    #add queues to xml config
    # Read in the file
    filedata=' '
    with open('./vmxml', 'r') as file :
        filedata = file.read()
    # Replace the target string
    filedata = filedata.replace('<source network=\''+FWD_BR+'\'/>', '<source network=\''+FWD_BR+'\'/>\n      <driver name=\'vhost\' queues=\'3\'/>')
    # Write the file out again
    with open('./vmxml', 'w') as file:
        file.write(filedata)
    sftp.put('./vmxml', dest_path+'/vmxml')    
    sftp.close()
    
    print('run on server: '+'virsh shutdown '+vm_name)
    stdin,stdout,stderr = server.exec_command('virsh shutdown '+vm_name)
    print(stdout.read())
    print(stderr.read())
    print('run on server: '+'virsh undefine '+vm_name)
    stdin,stdout,stderr = server.exec_command('virsh undefine '+vm_name)
    print(stdout.read())
    print(stderr.read())
    print('run on server: '+'virsh define '+dest_path+'/vmxml')
    stdin,stdout,stderr = server.exec_command('virsh define '+dest_path+'/vmxml')
    print(stdout.read())
    print(stderr.read())
    print('run on server: '+'virsh start '+vm_name)
    stdin,stdout,stderr = server.exec_command('virsh start '+vm_name)
    print(stdout.read())
    print(stderr.read())
    

parser = argparse.ArgumentParser(description='Deploy qcow2 to a kvm server.',epilog='Have a nice day!')
parser.add_argument('-b','--branch', help='branch name', dest='branch_name', default='unstable')
parser.add_argument('-n','--number', help='build number', dest='build_nr', default=0, type=int)
parser.add_argument('-k','--kvm', help='kvm server', dest='kvm_server', default='10.38.184.29')
parser.add_argument('-u','--user', help='user for kvm server', dest='user', default='mbarbu')
parser.add_argument('-p','--password', help='password for kvm server', dest='password', default='atip')
parser.add_argument('-w','--where', help='path where to copy the qcow2 file', dest='where', default='/home/mbarbu/Downloads/vATIP_Automation')
parser.add_argument('-j','--job', help='jenkins job', dest='job', default='vATIP-unstable-OVA')

args = parser.parse_args()

server_kvm = connect_to_kvm(args.kvm_server, args.user, args.password)
file_on_server = download_qcow2_to_kvm(server_kvm, args.where, args.branch_name, args.build_nr, args.job, SERVER_QCOW2_FILE_NAME)
deploy_qcow2_to_kvm(server_kvm, args.where, file_on_server, SERVER_VM_NAME)

print('Waiting 10 minutes  for vATIP_KVM_Image to start.')
time.sleep(600)
print('vATIP_KVM_Image_got_Deployed_Enjoy!!!')
