from http import HTTPStatus
import jsonpickle
import requests.packages.urllib3 
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders



requests.packages.urllib3.disable_warnings()
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'
try:
    requests.packages.urllib3.contrib.pyopenssl.DEFAULT_SSL_CIPHER_LIST += 'HIGH:!DH:!aNULL'
except AttributeError:
    # no pyopenssl support used / needed / available
    pass


class WebApiSession(object):

    def __init__(self, connConfig):
        ':type connConfig: ConnectionConfig'

        self.connConfig = connConfig

        self.url = '{}://{}:{}/{}/'.format(self.connConfig.protocol.protocolName, self.connConfig.host, self.connConfig.port, self.connConfig.basePath)
        self.headers = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        self.session = requests.Session()

        
    def checkResponse(self, response, expectedStatus, errorMessage=None):
        if errorMessage is None:
            errorMessage = 'Expected status code {}. Actual status code {}. Response text: {} on Request: {}'.format(expectedStatus, response.status_code, response.text, response.request.url)
        if response.status_code != expectedStatus:
            raise requests.exceptions.RequestException(errorMessage)


    def get(self, path, reqParams=None, reqHeaders=None, expectedStatus=HTTPStatus.OK):
        if reqHeaders is None:
            reqHeaders = self.headers

        response = self.session.get(self.url + path, params=reqParams, headers=reqHeaders, verify=self.connConfig.verifyCertificate)
        self.checkResponse(response, expectedStatus)
        return response


    def put(self, path, reqData=None, reqHeaders=None, expectedStatus=HTTPStatus.OK):
        if reqHeaders is None:
            reqHeaders = self.headers
        if reqHeaders.get(RequestHeaders.CONTENT_TYPE) == RequestHeaders.CONTENT_TYPE_JSON:
            reqData = jsonpickle.encode(reqData, unpicklable=False)

        try:
            response = self.session.put(self.url + path, data=reqData, headers=reqHeaders, verify=self.connConfig.verifyCertificate)
        except Exception as e:
            print("Received this exception: {}".format(jsonpickle.encode(e)))
            raise e
        #print("path: {}, response: {}".format(path, jsonpickle.encode(response)))
        self.checkResponse(response, expectedStatus)
        return response


    def post(self, path, reqData=None, reqHeaders=None, expectedStatus=HTTPStatus.OK):
        if reqHeaders is None:
            reqHeaders = self.headers
        if reqHeaders.get(RequestHeaders.CONTENT_TYPE) == RequestHeaders.CONTENT_TYPE_JSON:
            reqData = jsonpickle.encode(reqData, unpicklable=False)

        try:
            response = self.session.post(self.url + path, data=reqData, headers=reqHeaders, verify=self.connConfig.verifyCertificate)
        except Exception as e:
            print("Received this exception: {}".format(jsonpickle.encode(e)))
            raise e
        # print("******************************")
        # print("path: {}, response: {}".format(path, response.json()))
        # print("******************************")
        self.checkResponse(response, expectedStatus)
        return response


    def delete(self, path, reqData=None, reqHeaders=None, expectedStatus=HTTPStatus.OK, errorMessage=None):
        if reqHeaders is None:
            reqHeaders = self.headers
        if reqHeaders.get(RequestHeaders.CONTENT_TYPE) == RequestHeaders.CONTENT_TYPE_JSON:
            reqData = jsonpickle.encode(reqData, unpicklable=False)

        response = self.session.delete(self.url + path, data=reqData, headers=reqHeaders, verify=self.connConfig.verifyCertificate)
        self.checkResponse(response, expectedStatus, errorMessage)
        return response
