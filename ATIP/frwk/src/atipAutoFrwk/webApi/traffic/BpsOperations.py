from abc import ABC, abstractmethod
from cmath import isclose
from http import HTTPStatus
import re
import time
import warnings

import jsonpickle
from atipAutoFrwk.config.traffic.BpsTestConfig import BpsTestConfig

from atipAutoFrwk.data.traffic.stats.BpsRealTimeStatsContainer import BpsRealTimeStatsContainer
from atipAutoFrwk.data.traffic.stats.BpsRealTimeStatsItem import BpsRealTimeStatsItem
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.services.traffic.BpsStatsService import BpsStatsService
from atipAutoFrwk.data.traffic.RealTimeStats import RealTimeStats



class BpsOperations(object):

    _bpsTestActionsList = []

    @classmethod
    def addBpsTestAction(cls, bpsTestAction):
        cls._bpsTestActionsList.append(bpsTestAction)

    @classmethod
    def clearBpsTestAction(cls):
        cls._bpsTestActionsList = []

    @classmethod
    def startBps(cls, webApiSession, bpsTest, group, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        check = BpsOperations.forceStopBpsTestIfRunning(webApiSession)

        if check is not None:
            warnings.warn("BPS test has been forcefully stopped")
        path = 'bps/tests/operations/start'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        reqData = {
            "modelname": bpsTest,
            "group": group
        }
        response = webApiSession.post(path, reqData, reqHeaders, expectedStatus=expectedStatus)
        output = response.json()
        return output.get("testid")

    @classmethod
    def stopBps(cls, webApiSession, testid, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'bps/tests/operations/stop'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        reqData = {"testid": testid}
        response = webApiSession.post(path, reqData, reqHeaders, expectedStatus=expectedStatus)
        return response

    @classmethod
    def forceStopBpsTestIfRunning(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'bps/tests'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        output = response.json()
        runningTestInfo = output.get('runningTestInfo')
        runningTestInfoList = runningTestInfo.split(',')

        testId = None
        for runningTest in runningTestInfoList:
            if webApiSession.connConfig.username in runningTest:
                testId = re.search(r"TEST\-\d{1,10}", runningTest)
                print(testId.group(0))
                BpsOperations.stopBps(webApiSession, testId.group(0))
        return testId

    @classmethod
    def runBpsTest(cls, webApiSession, bpsTest, group, testDuration, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        testId = BpsOperations.startBps(webApiSession, bpsTest, group)
        BpsOperations.waitForBpsTestCompletion(webApiSession, testId, testDuration)

        return testId

    @classmethod
    def waitForBpsTestCompletion(cls, webApiSession, testId, testDuration, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        progress = BpsOperations.getBpsTestProgress(webApiSession, testId)
        timeout = 0
        maxTestDuration = testDuration + 180
        sleepTime = BpsOperations.getTimeoutForProgressCheck(testDuration, maxTestDuration, timeout)
        while (timeout < maxTestDuration) and progress != 100:
            time.sleep(sleepTime)
            timeout = timeout + sleepTime
            if timeout > testDuration:
                BpsOperations.getTimeoutForProgressCheck(testDuration, maxTestDuration, timeout)
            print('Time elapsed: {}s. Progress: {}%'.format(timeout, progress))
            progress = BpsOperations.getBpsTestProgress(webApiSession, testId)

        if progress != 100:
            check = BpsOperations.forceStopBpsTestIfRunning(webApiSession)
            if check is not None:
                warnings.warn("BPS test has been forcefully stopped")
        return progress

    @classmethod
    def performActionUntilBpsTestCompletion(cls, env, bpsSession, bpsTestConfig, frequencyCheck):
        ':type webApiSession: WebApiSession'

        testId = BpsOperations.startBps(bpsSession, bpsTestConfig.testName, env.bpsPortGroup)
        bpsTestConfig.testId = testId
        progress = BpsOperations.getBpsTestProgress(bpsSession, testId)
        timeout = 0
        maxTestDuration = int(bpsTestConfig.testDuration) + 180
        print('test ID:{}'.format(testId))
        sleepTime = int(frequencyCheck)
        if sleepTime > maxTestDuration:
            sleepTime = maxTestDuration
        while (timeout < maxTestDuration) and progress != 100:
            time.sleep(sleepTime)
            timeout = timeout + sleepTime
            for bpsTestAction in cls._bpsTestActionsList:
                bpsTestAction.doAction()
            progress = BpsOperations.getBpsTestProgress(bpsSession, testId)
            print('Time elapsed: {}s. Progress: {}%'.format(timeout, progress))

        if progress != 100:
            check = BpsOperations.forceStopBpsTestIfRunning(bpsSession)
            if check is not None:
                warnings.warn("BPS test has been forcefully stopped")
        return progress



    @classmethod
    def getTimeoutForProgressCheck(cls, testDuration, maxTestDuration, crtTime):
        if crtTime < testDuration:
            sleepTime = int(testDuration / 10)
        else:
            sleepTime = int(testDuration / 100)
        if sleepTime < 10:
            sleepTime = 10
        return sleepTime

    @classmethod
    def getBpsTestProgress(cls, webApiSession, testId, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'bps/tests/operations/getrts'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        reqData = {"runid": testId}
        progress = '-1'
        try:
            response = webApiSession.post(path, reqData, reqHeaders, expectedStatus=expectedStatus)
            output = response.json()
            progress = output.get('progress')
            if progress is 0:
                warnings.warn("The test is in preExecuting state, output: {}".format(output))
        except Exception as e:
            warnings.warn('BPS warning: {}'.format(e))
            
        return int(progress)

    @classmethod
    def checkBpsStability(cls, webApiSession, testId, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        stats = BpsStatsService.getAllRealTimeStats(webApiSession, testId)
        print('BpsRtsStats: {}'.format(stats))
        txFrames = RealTimeStats.ETH_TX_FRAMES.statType  # @UndefinedVariable
        rxFrames = RealTimeStats.ETH_RX_FRAMES.statType  # @UndefinedVariable
        appSuccessfull = RealTimeStats.APP_SUCCESSFUL.statType  # @UndefinedVariable
        appAttempted = RealTimeStats.APP_ATTEMPTED.statType  # @UndefinedVariable

        if not isclose(stats[txFrames], stats[rxFrames], rel_tol=RealTimeStats.ETH_TX_FRAMES.maxDiff):  # @UndefinedVariable
            raise Exception('ERROR: Discrepancy in BPS packets: ethTxFrames -> {},ethRxFrames -> {})'.format(
                stats[txFrames], stats[rxFrames]))
        if not isclose(stats[appSuccessfull], stats[appAttempted], rel_tol=RealTimeStats.ETH_RX_FRAMES.maxDiff):  # @UndefinedVariable
            raise Exception('ERROR: Discrepancy in BPS sessions: appSuccessful -> {},appAttempted -> {})'.format(stats[appSuccessfull], stats[appAttempted]))

        return None


class IBpsTestAction(ABC):

    @abstractmethod
    def doAction(self):
        pass