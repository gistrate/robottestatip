from http import HTTPStatus

from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders


class PortsOperations(object):
    @classmethod
    def reserveBpsPorts(cls, webApiSession, slot, ports, group, force=True, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'bps/ports/operations/reserve'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        reqData = {
            "slot": slot,
            "portList": ports,
            "group": group,
            "force": force
        }
        response = webApiSession.post(path, reqData, reqHeaders, expectedStatus=expectedStatus)
        return response
