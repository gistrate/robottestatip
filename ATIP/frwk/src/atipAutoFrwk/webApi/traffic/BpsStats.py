from http import HTTPStatus

from atipAutoFrwk.data.traffic.stats.BpsRealTimeStatsContainer import BpsRealTimeStatsContainer
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService


class BpsStats(object):

    @classmethod
    def getBpsRtsStatsCore(cls, webApiSession, testId, statsGroup="summary", expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'bps/tests/operations/getrts'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        reqData = {"runid": testId, "statsGroup": statsGroup}
        response = webApiSession.post(path, reqData, reqHeaders, expectedStatus=expectedStatus)
        return response

    @classmethod
    def getBpsRtsStatsJSON(cls, webApiSession, testId, statsGroup):
        ':type webApiSession: WebApiSession'

        response = cls.getBpsRtsStatsCore(webApiSession, testId, statsGroup)
        return response.json()

    @classmethod
    def getBpsRealTimeStatsContainer(cls, webApiSession, testId, statsGroup="summary"):
        ':type webApiSession: WebApiSession'

        try:
            responseJSON = cls.getBpsRtsStatsJSON(webApiSession, testId, statsGroup)
            if not responseJSON.get('rts'):
                # no stats found => return a container with an empty list
                return BpsRealTimeStatsContainer(list())
            return ConversionService.createBpsRTSContainer(responseJSON)
        except Exception as e:
            raise ValueError("Couldn't get RTS stats!")

    @classmethod
    def getBpsAggregateStats(cls, webApiSession, testId, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'bps/tests/operations/getAggregateStats'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        reqData = {"testid": testId}
        response = webApiSession.post(path, reqData, reqHeaders, expectedStatus=expectedStatus)
        return response.json()
