from http import HTTPStatus

from atipAutoFrwk.data.traffic.stats.BpsComponentStatsContainer import BpsComponentStatsContainer
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.services.TestCondition import TestCondition


class BpsComponentStats(object):

    @classmethod
    def getBpsComponents(cls, webApiSession, testName, expectedStatus=HTTPStatus.OK):
        '''
        Returns a list containing the components of the given test. 
        '''
        ':type webApiSession: WebApiSession'

        path = 'bps/tests/operations/getComponents'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        reqData = {"modelName": testName}
        response = webApiSession.post(path, reqData, reqHeaders, expectedStatus=expectedStatus)
        result = response.json().get('result')
        componentsString = result.split(':')[1].replace(' ', '')
        componentsList = componentsString[1:-1].split(',')
        return componentsList

    @classmethod
    def getComponentStats(cls, webApiSession, testId, componentId, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'bps/tests/operations/getComponentStats'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        reqData = {"testid": testId, "componentId": componentId}
        response = webApiSession.post(path, reqData, reqHeaders, expectedStatus=expectedStatus)
        stats = ConversionService.createStatsContainer(response.json(), BpsComponentStatsContainer)
        return stats

    @classmethod
    def getAllComponentsStats(cls, webApiSession, bpsTestConfig, expectedStatus=HTTPStatus.OK):
        '''
        Return a dictionary that contains as key the component name, and as value the bps stats for that component.
        '''
        ':type webApiSession: WebApiSession'
        ':type bpsTestConfig: BpsTestConfig'

        componentsList = cls.getBpsComponents(webApiSession, bpsTestConfig.testName)
        stats = dict()
        for component in componentsList:

            statsOk = lambda: cls.statsOk(webApiSession, bpsTestConfig, component) == True
            maxWaitTime = 300
            errMsg = 'Couldn\'t get stats in {} seconds'.format(maxWaitTime)
            TestCondition.waitUntilTrue(statsOk, errorMessage=errMsg, totalTime=maxWaitTime, iterationTime=5)
            stats[component] = cls.getComponentStats(webApiSession, bpsTestConfig.testId, component)
        return stats

    @classmethod
    def statsOk(cls, webApiSession, bpsTestConfig, component):
        '''
        :return True if the Login was successful; False otherwise.
        '''
        try:
            cls.getComponentStats(webApiSession, bpsTestConfig.testId, component)
        except Exception:
            return False
        return True
