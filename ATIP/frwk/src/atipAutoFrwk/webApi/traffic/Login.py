from http import HTTPStatus

from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders


class Login(object):
    @classmethod
    def login(cls, webApiSession, username=None, password=None, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'auth/session'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}

        if username is None:
            username = webApiSession.connConfig.username
        if password is None:
            password = webApiSession.connConfig.password

        reqData = {
            'username': username,
            'password': password
        }
        response = webApiSession.post(path, reqData, reqHeaders, expectedStatus=expectedStatus)
        return response

