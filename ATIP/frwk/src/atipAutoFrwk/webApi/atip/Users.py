from http import HTTPStatus
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders


class Users(object):

    @classmethod
    def changePassword(cls, webApiSession, oldPassword, newPassword, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        
        path = 'users/current'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        reqData = {
            'oldPassword': oldPassword,
            'newPassword': newPassword
            }
        response = webApiSession.put(path, reqData, reqHeaders, expectedStatus=expectedStatus)
        return response
