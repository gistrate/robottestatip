import json

from atipAutoFrwk.config.atip.DedupGlobalConfig import DedupGlobalConfig
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService



class Dedup(object):

    @classmethod
    def updateDedup(cls, webApiSession, dedupGlobalConfig, groupId=0):
        '''
            :param dedupGlobalConfig : the configuration object to be posted
            :return: nothing
        '''

        path = '{}/dedup'.format(groupId)
        response = webApiSession.post(path, reqData=dedupGlobalConfig)
        print(json.loads(response.text))

    @classmethod
    def getDedup(cls, webApiSession, groupId=0):
        ':type webApiSession: WebApiSession'
        '''
        returns: a JSON
        '''
        path = '{}/dedup'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders)
        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def getDedupGlobalConfig(cls, webApiSession, groupId=0):
        ':type webApiSession: WebApiSession'
        '''
        returns: a DedupGlobalConfig object
        '''
        theJSON = cls.getDedup(webApiSession=webApiSession, groupId=groupId)
        dedupConfig = ConversionService.createBasicObject(theJSON.get('globalConfig'), DedupGlobalConfig)
        return dedupConfig



