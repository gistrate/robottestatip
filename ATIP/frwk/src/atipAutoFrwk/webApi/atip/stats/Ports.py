from http import HTTPStatus
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.stats.TopStatsContainer import TopStatsContainer
from atipAutoFrwk.data.atip.stats.TopStatsItem import TopStatsItem
from atipAutoFrwk.data.atip.stats.TrafficContainer import TrafficContainer
from atipAutoFrwk.data.atip.stats.TrafficItem import TrafficItem
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.data.atip.StatsType import StatsType


class Ports(object):
    '''
    Methods to get the list of the top stats.   
    '''

    @classmethod
    def getTopStats(cls, webApiSession, statsType, timeInterval=TimeInterval.HOUR, limit='10', appType=None, ruleID=None, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type statsType: StatsType'
        ':type timeInterval: TimeInterval'
        ':type appType: ApplicationType'

        path = 'portStats?inv={}&lmt={}'.format(timeInterval.intervalName, limit)
        if appType:
           path += '&app=' + appType.appName
        if ruleID:
            path += '&ruleId=' + str(ruleID)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)


        if not response.json().get('portSeries')[0].get('series'):
            # no stats found => return a container with an empty list
            return TrafficContainer(list())

        stats = response.json().get('portSeries')[0].get('series')
        if isinstance(stats, dict):
            statsItemsList = list()
            statsItemsList.append(ConversionService.createStatsContainer(stats, TrafficItem))
        else:
            statsItemsList = ConversionService.createStatsList(stats, TrafficItem)
        return TrafficContainer(statsItemsList)


    @classmethod
    def getTopDevicesByAppStats(cls, webApiSession, appType, timeInterval=TimeInterval.HOUR, limit='10', expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type appType: ApplicationType'
        ':type timeInterval: TimeInterval'

        return cls.getTopStats(webApiSession, StatsType.DEVICES, timeInterval, limit, appType, expectedStatus)

    @classmethod
    def getTopBrowsersByAppStats(cls, webApiSession, appType, timeInterval=TimeInterval.HOUR, limit='10', expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type appType: ApplicationType'
        ':type timeInterval: TimeInterval'

        return cls.getTopStats(webApiSession, StatsType.BROWSERS, timeInterval, limit, appType, expectedStatus)

    @classmethod
    def getTopCountriesByAppStats(cls, webApiSession, appType, timeInterval=TimeInterval.HOUR, limit='10',
                                 expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type appType: ApplicationType'
        ':type timeInterval: TimeInterval'

        return cls.getTopStats(webApiSession, StatsType.GEO, timeInterval, limit, appType, expectedStatus)





