from http import HTTPStatus
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.stats.DetailedFilterContainer import DetailedFilterContainer
from atipAutoFrwk.data.atip.stats.DetailedFilterItem import DetailedFilterItem
from atipAutoFrwk.data.atip.stats.MapsStatsContainer import MapsStatsContainer
from atipAutoFrwk.data.atip.stats.MapsStatsItem import MapsStatsItem
from atipAutoFrwk.data.atip.stats.TopStatsContainer import TopStatsContainer
from atipAutoFrwk.data.atip.stats.TopStatsItem import TopStatsItem
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.data.atip.StatsType import StatsType


class Filters(object):
    '''
    Methods to get the list of the top stats.   
    '''

    @classmethod
    def getTopStats(cls, webApiSession, statsType=StatsType.GEO, timeInterval=TimeInterval.HOUR, limit='10', ruleID = None, expectedStatus=HTTPStatus.OK, groupId=0):
        ':type webApiSession: WebApiSession'
        ':type statsType: StatsType'
        ':type timeInterval: TimeInterval'

        path = '{}/filterStats/top10?lists={}&inv={}&lmt={}'.format(groupId, statsType.statsName, timeInterval.intervalName, limit)
        if ruleID:
            path += '&ruleId=' + str(ruleID)
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        if not response.json().get('stats')[0]:

            # no stats found => return a container with an empty list
            return TopStatsContainer(None, list())
        stats = response.json().get('stats')[0].get('list')
        if isinstance(stats, dict):
            statsItemsList = list()
            statsItemsList.append(ConversionService.createStatsContainer(stats, TopStatsItem))
        else:
            statsItemsList = ConversionService.createStatsList(stats, TopStatsItem)
        return TopStatsContainer(None, statsItemsList)

    @classmethod
    def getDetailedStats(cls, webApiSession, serverIP=None, clientIP=None, timeInterval=TimeInterval.HOUR, limit='10',
                   ruleID=None, device=None, expectedStatus=HTTPStatus.OK, groupId=0):
        ':type webApiSession: WebApiSession'
        ':type statsType: StatsType'
        ':type timeInterval: TimeInterval'
        ':type appType: ApplicationType'

        path = '{}/filterStats/detail?inv={}&lmt={}'.format(groupId, timeInterval.intervalName, limit)
        if serverIP:
            path += '&serverIp=' + str(serverIP)
        if clientIP:
            path += '&clientIp=' + str(serverIP)
        if device:
            path += '&device=' + str(device)
        if ruleID:
            path += '&ruleId=' + str(ruleID)
        response = webApiSession.get(path, expectedStatus=expectedStatus)

        if not response.json().get('stats')[0]:
            # no stats found => return a container with an empty list
            return DetailedFilterContainer(None, list())

        stats = response.json().get('stats')[0].get('list')
        if isinstance(stats, dict):
            statsItemsList = list()
            statsItemsList.append(ConversionService.createStatsContainer(stats, DetailedFilterItem))
        else:
            statsItemsList = ConversionService.createStatsList(stats, DetailedFilterItem)
        return DetailedFilterContainer(None, statsItemsList)


    @classmethod
    def getTopDevicesByAppStats(cls, webApiSession, appType, timeInterval=TimeInterval.HOUR, limit='10', expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type appType: ApplicationType'
        ':type timeInterval: TimeInterval'

        return cls.getTopStats(webApiSession, StatsType.DEVICES, timeInterval, limit, appType, expectedStatus)

    @classmethod
    def getTopBrowsersByAppStats(cls, webApiSession, appType, timeInterval=TimeInterval.HOUR, limit='10', expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type appType: ApplicationType'
        ':type timeInterval: TimeInterval'

        return cls.getTopStats(webApiSession, StatsType.BROWSERS, timeInterval, limit, appType, expectedStatus)

    @classmethod
    def getTopCountriesByAppStats(cls, webApiSession, appType, timeInterval=TimeInterval.HOUR, limit='10',
                                 expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type appType: ApplicationType'
        ':type timeInterval: TimeInterval'

        return cls.getTopStats(webApiSession, StatsType.GEO, timeInterval, limit, appType, expectedStatus)





