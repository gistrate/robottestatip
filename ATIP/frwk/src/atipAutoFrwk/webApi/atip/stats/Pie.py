from http import HTTPStatus

from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.data.atip.stats.PieStatsItem import PieStatsItem
from atipAutoFrwk.data.atip.stats.PieStatsContainer import PieStatsContainer
from atipAutoFrwk.data.atip.StatsType import StatsType

class Pie(object):

    @classmethod
    def getTopStats(cls, webApiSession, statsType, timeInterval=TimeInterval.HOUR, lmt=10, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type statsType: StatsType'
        ':type timeInterval: TimeInterval'

        path = '{}/pie/{}?lmt={}&inv={}'.format(groupId, statsType.statsName, lmt, timeInterval.intervalName)
        # reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        pieItemsList = ConversionService.createStatsList(response.json().get('data'), PieStatsItem)
        return PieStatsContainer(pieItemsList)

    @classmethod
    def getTopDevices(cls, webApiSession, timeInterval=TimeInterval.HOUR, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type timeInterval: TimeInterval'

        return cls.getTopStats(webApiSession, StatsType.DEVICES, timeInterval, expectedStatus)

    @classmethod
    def getTopBrowsers(cls, webApiSession, timeInterval=TimeInterval.HOUR, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type timeInterval: TimeInterval'

        return cls.getTopStats(webApiSession, StatsType.BROWSERS, timeInterval, expectedStatus)

    @classmethod
    def getAppDistribution(cls, webApiSession, timeInterval=TimeInterval.HOUR, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession'
        :type timeInterval: TimeInterval'
        '''
        return cls.getTopStats(webApiSession, StatsType.APPS, timeInterval, expectedStatus)

    @classmethod
    def getTopCountry(cls, webApiSession, timeInterval=TimeInterval.HOUR, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession'
        :type timeInterval: TimeInterval'
        '''
        return cls.getTopStats(webApiSession, StatsType.GEO, timeInterval, expectedStatus)


