from http import HTTPStatus
from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.stats.MapsStatsContainer import MapsStatsContainer
from atipAutoFrwk.data.atip.stats.MapsStatsItem import MapsStatsItem
from atipAutoFrwk.data.atip.stats.TopStatsContainer import TopStatsContainer
from atipAutoFrwk.data.atip.stats.TopStatsItem import TopStatsItem
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.data.atip.StatsType import StatsType


class Lists(object):
    '''
    Methods to get the list of the top stats.   
    '''

    @classmethod
    def getTopStats(cls, webApiSession, statsType=StatsType.GEO,timeInterval=TimeInterval.HOUR, limit=10, groupId=0, appType=None, ruleID = None, expectedStatus=HTTPStatus.OK, ):
        ':type webApiSession: WebApiSession'
        ':type statsType: StatsType'
        ':type timeInterval: TimeInterval'
        ':type appType: ApplicationType'

        path = '{}/lists/top10?lists={}&inv={}&lmt={}'.format(groupId, statsType.statsName, timeInterval.intervalName, limit)
        if appType:
            path += '&app=' + appType.appName
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        print(response)

        if ruleID:
            path += '&ruleId=' + str(ruleID)

        s = response.json().get('stats')[0]

        if not isinstance(s,dict):
            s = s[0]
        name = s.get('name')
        stats = s.get('list')
        if not stats:
            # no stats found => return a container with an empty list
            return TopStatsContainer(name, list())

        if isinstance(stats, dict):
            statsItemsList = list()
            statsItemsList.append(ConversionService.createStatsContainer(stats, TopStatsItem))
        else:
            statsItemsList = ConversionService.createStatsList(stats, TopStatsItem)
        return TopStatsContainer(name, statsItemsList)


    @classmethod
    def getTopDevicesByAppStats(cls, webApiSession, appType, timeInterval=TimeInterval.HOUR, limit='10', expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type appType: ApplicationType'
        ':type timeInterval: TimeInterval'

        return cls.getTopStats(webApiSession, StatsType.DEVICES, timeInterval, limit, appType, expectedStatus)

    @classmethod
    def getTopBrowsersByAppStats(cls, webApiSession, appType, timeInterval=TimeInterval.HOUR, limit='10', expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type appType: ApplicationType'
        ':type timeInterval: TimeInterval'

        return cls.getTopStats(webApiSession, StatsType.BROWSERS, timeInterval, limit, appType, expectedStatus)

    @classmethod
    def getTopCountriesByAppStats(cls, webApiSession, appType, timeInterval=TimeInterval.HOUR, limit='10',
                                 expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type appType: ApplicationType'
        ':type timeInterval: TimeInterval'

        return cls.getTopStats(webApiSession, StatsType.GEO, timeInterval, limit, appType, expectedStatus)





