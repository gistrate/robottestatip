from http import HTTPStatus

import jsonpickle

from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.atip.stats.MapsStatsItem import MapsStatsItem
from atipAutoFrwk.data.atip.stats.MapsStatsContainer import MapsStatsContainer
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService



class Maps(object):

    @classmethod
    def getTopStats(cls, webApiSession, timeInterval=TimeInterval.HOUR, limit='10', appType='None', expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type timeInterval: TimeInterval'
        path = 'mapStats?inv={}&lmt={}'.format(timeInterval.intervalName, limit)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        mapsItemsList = ConversionService.createStatsList(response.json().get('stats'), MapsStatsItem)
        return mapsItemsList




