from http import HTTPStatus
import logging
import jsonpickle

from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.TestCondition import TestCondition
from atipAutoFrwk.data.atip.ConfigTimeout import ConfigTimeout
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.config.atip.filter.FilterConfig import FilterConfig

class Filters(object):

    @classmethod
    def createFilter(cls, webApiSession, filterConfig, expectedStatus=HTTPStatus.OK):
        '''
        Creates a new filter. rulePush MUST be called after this method in order for the new filter to be sent to the NP.
        See :func: `atipAutoFrwk.services.atip.FiltersService.FiltersService.createFilter`
        :type webApiSession: WebApiSession
        :type filterConfig: FilterConfig
        '''

        path = 'rule/-1'
        reqData = filterConfig
        print('Creating filter containing {}'.format(jsonpickle.encode(filterConfig, unpicklable=False)))
        response = webApiSession.put(path, reqData, expectedStatus=expectedStatus)
        if  expectedStatus==HTTPStatus.OK:
            filterConfig.id = response.json().get('rule')[0].get('id')
            filterConfig.priority = response.json().get('rule')[0].get('priority')
            return filterConfig
        else:
            return response.json()

    @classmethod
    def updateFilter(cls, webApiSession, filterConfig, expectedStatus=HTTPStatus.OK):
        '''
        Creates a new filter. rulePush MUST be called after this method in order for the new filter to be sent to the NP.
        '''
        ':type webApiSession: WebApiSession'
        ':type filterConfig: FilterConfig'

        path = 'rule/{}'.format(filterConfig.id)
        reqData = filterConfig
        print('\r\nCreating filter containing {}'.format(jsonpickle.encode(filterConfig)))
        response = webApiSession.put(path, reqData, expectedStatus=expectedStatus)
        return response

    @classmethod
    def rulePush(cls, webApiSession, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = '{}/rulePush'.format(groupId)

        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.post(path, reqHeaders, expectedStatus=expectedStatus)
        return response

    @classmethod
    def getAllFilters(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'rule/-1'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response.json()

    @classmethod
    def checkNoFilters(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        filters = cls.getAllFilters(webApiSession, HTTPStatus.OK)
        if (filters['total'] == 0):
            return True
        return False

    @classmethod
    def getNumberOfFilters(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        filterCount = 0
        
        allFiltersList = Filters.getAllFilters(webApiSession)
        if not allFiltersList.get('rule'):
            return 0
        else:
            if not isinstance(allFiltersList.get('rule'), list):
                if isinstance(allFiltersList.get('rule').get('isUnmatched'), object) and allFiltersList.get('rule').get('isUnmatched') ==  False:
                    filterCount+=1
            else:
                for item in allFiltersList.get('rule'):
                    if isinstance(item.get('isUnmatched'), object) and item.get('isUnmatched') == False:
                        filterCount+=1
        
        return filterCount

    @classmethod
    def deleteAllFilters(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        response = None
        path = 'rule'
        allFiltersList = Filters.getAllFilters(webApiSession)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        if not allFiltersList.get('rule'):
            print('No filter config present on system...')
        else:
            if not isinstance(allFiltersList.get('rule'), list):
                if isinstance(allFiltersList.get('rule').get('isUnmatched'), object) and allFiltersList.get('rule').get('isUnmatched') ==  False:
                    print('\r\nDeleting filter {}'.format(allFiltersList.get('rule').get('id')))
                    errorMessage = 'Unable to remove filter (id: {}; name: {})'.format(
                            allFiltersList.get('rule').get('id'), allFiltersList.get('rule').get('name'))
                    response = webApiSession.delete('{}/{}'.format(path, allFiltersList.get('rule').get('id')), reqHeaders,
                                                    expectedStatus=expectedStatus, errorMessage=errorMessage)
            else:
                for item in allFiltersList.get('rule'):
                    if isinstance(item.get('isUnmatched'), object) and item.get('isUnmatched') == False:
                        print('Deleting filter {} {}'.format(item['id'], item['name']))
                        errorMessage = 'Unable to remove filter (id: {}; name: {} )'.format(
                                item['id'], item['name'])
                        response = webApiSession.delete('{}/{}'.format(path, item['id']),reqHeaders, expectedStatus=expectedStatus, errorMessage=errorMessage)
                        
        noFilters = lambda : cls.getNumberOfFilters(webApiSession) == 0
        listFilters = lambda : logging.exception('Unable to remove all the filters. Filters still present: {}'.format(cls.getAllFilters(webApiSession)))
        TestCondition.waitUntilTrue(noFilters, 'Unable to remove all the filters.', onFailure=listFilters, totalTime=ConfigTimeout.FILTERS_UPDATE.waitTime)  # @UndefinedVariable

        return response

    @classmethod
    def deleteFilterByName(cls, webApiSession, name, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        response = None
        path = 'rule'
        allFiltersList = Filters.getAllFilters(webApiSession)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        errorMessage = 'Unable to remove filter {}. '.format(name)
        found = False
        if not allFiltersList.get('rule'):
            raise Exception('No filter config present on system...')
        else:
            if not isinstance(allFiltersList.get('rule'), list):
                print('Deleting filter {}'.format(allFiltersList.get('rule').get('name')))
                if allFiltersList.get('rule').get('name') == name:

                    response = webApiSession.delete('{}/{}'.format(path, allFiltersList.get('rule').get('id')), reqHeaders,
                                                expectedStatus=expectedStatus, errorMessage=errorMessage)
                    found = True

            else:
                for item in allFiltersList.get('rule'):
                    if item.get('name') == name:
                        print('Deleting filter {} {}'.format(item['id'], item['name']))
                        response = webApiSession.delete('{}/{}'.format(path,item['id']), reqHeaders, expectedStatus=expectedStatus, errorMessage=errorMessage)
                        found = True
        if not found:
            raise Exception('Filter {} not found.'.format(name))
        return response

    @classmethod
    def updateFilterConfig(cls, webApiSession, filterName, key=None, value=None):
        '''
        Updates specified attribute for filter; must be followed by "rulePush" keyword to send it to NP;
        Note: cannot be used to change filter priority, use below procedure to do this
        :param filterName: the name of the filter to be updated
        :param key: the attribute name to be updated
        :param value: the value the attribute will be updated
        :return: the updated filter config
        '''

        index = None
        currentId = None
        filtersConfig = cls.getAllFilters(webApiSession)['rule']

        if not isinstance(filtersConfig, list):
            filtersConfig = [filtersConfig]

        for filter in filtersConfig:
            if filter['name'] == filterName:
                currentId = filter['id']
                index = filtersConfig.index(filter)
                break

        path = 'rule/' + str(currentId)

        if key != None:
            filtersConfig[index][key] = value
            response = webApiSession.put(path, reqData=filtersConfig[index])

        filtersConfig = cls.getAllFilters(webApiSession)['rule']

        return filtersConfig

    @classmethod
    def updateFilterPriority(cls, webApiSession, filterName, priority, groupId=0):
        '''
        Updates priority for filter; this is accomplished by swapping priorities of actual filter and the one that has the old priority; i.e. if having filter:priority pairs f1:1, f2:2, f3:3, assigning
        priority 2 to f1 result in swapping priorities for filters f1 and f2 and updating; result will be f2:1, f1:2, f3:3
        Note: can be used to change filter priority, but changing priorities means changing filters order by priority criteria, cannot assign random values for filter priority;
        :param filterName: the name of the filter to be updated
        :param priority: the new priority
        :return: nothing; new priority is applied to filter
        '''

        index = None
        toChangeIndex = None
        currentId = None
        currentPriority = None
        filterList = []
        filtersConfig = cls.getAllFilters(webApiSession)['rule']
        if not isinstance(filtersConfig, list):
            filtersConfig = [filtersConfig]

        for filter in filtersConfig:
            if filter['name'] == filterName:
                currentId = filter['id']
                currentPriority = filter['priority']
                # for matching filter, index in filters list will be the same with index in the new {id:priority} list
                index = filtersConfig.index(filter)
            filterList.append({'groupId':filter['groupId'],'id':filter['id'], 'priority':filter['priority']})

        for item in range(len(filterList)):
            if filterList[item]['priority'] == int(priority):
                # get index for item which currently has priority to be updated
                toChangeIndex = item


        filterList[index]['priority'] = priority
        filterList[toChangeIndex]['priority'] = currentPriority
        #delete the unmatched traffic filter from the list as it will always be the last
        for item in range(len(filterList) - 1, -1, -1):
            if 1000 in filterList[item].values():
                del (filterList[item])

        dataToPush = {'rule':filterList}
        path = 'rule/priority'
        response = webApiSession.put(path, reqData=dataToPush)

    def getFilterId(self, webApiSession, filterName):
        filters = Filters.getAllFilters(webApiSession)['rule']
        print(jsonpickle.encode(filters))

        filterId = None
        if not isinstance(filters, list):
            filters = [filters]

        for filter in filters:
            if filter['name'] == filterName:
                filterId = filter['id']

        return filterId

    @classmethod
    def getFilterByName(cls, webApiSession, name, groupId):
        ':type webApiSession: WebApiSession'
        filterConfig = cls.getAllFilters(webApiSession)['rule']
        found = False

        for item in filterConfig:
            if item['name'] == name and item['groupId'] == groupId:
                print('Updating filter {} {}'.format(item['id'], item['name']))
                filterConfig = ConversionService.createBasicObject(item, FilterConfig)
                found = True
                break


        if not found:
            raise Exception('Filter {} not found.'.format(name))
        return filterConfig
    
