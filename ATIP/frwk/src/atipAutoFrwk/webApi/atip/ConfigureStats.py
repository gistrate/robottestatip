import json
from http import HTTPStatus

from atipAutoFrwk.config.atip.NetflowAccelerationConfig import NetflowAccelerationConfig
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService


class ConfigureStats(object):
    @classmethod
    def getConfig(cls, webApiSession, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        
        path = "{0}/configureStats".format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        theJSON = json.loads(response.text)
        config = ConversionService.createBasicObject(theJSON.get('statsGlobalConfig'), NetflowAccelerationConfig)
        return config

    @classmethod
    def getConfigureStatsObj(cls, enableTurboMode):
        return {'enableTurboMode': enableTurboMode}
    
    @classmethod   
    def changeConfig(cls, webApiSession, cfg="", groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        
        path = "{}/configureStats".format(groupId)
        response = webApiSession.post(path=path, reqData=cfg, expectedStatus=expectedStatus)
        return response.json()


