from http import HTTPStatus
import json

from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders


class License(object):

    @classmethod
    def configureLicenseServerIp(cls, webApiSession, licenseServerIp, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'license'
        reqData = dict()
        reqData['serverIp'] = licenseServerIp
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.post(path, reqData, reqHeaders, expectedStatus=expectedStatus)
        return response

    @classmethod
    def getLicenseServer(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        :return: A string containing the License Manager ip.
        '''

        path = 'license'
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        return json.loads(response.text).get('serverIp')
