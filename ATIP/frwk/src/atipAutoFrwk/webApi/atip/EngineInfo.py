from atipAutoFrwk.data.atip.stats.NetflowConnItem import NetflowConnItem
from atipAutoFrwk.data.atip.stats.NetflowStatsContainer import NetflowStatsContainer
from atipAutoFrwk.data.atip.stats.NetflowStatsItem import NetflowStatsItem
from atipAutoFrwk.services.ConversionService import ConversionService
import jsonpickle

class EngineInfo(object):

    @classmethod
    def getNetflowStats(cls, webApiSession, groupId = 0):
        '''
        :param webApiSession: current session
        :return: a class item of type NetflowStatsContainer
        '''
        path = '{}/engineinfo/netflow'.format(groupId)
        response = webApiSession.get(path)
        stats = response.json().get('status')
        print("stats {}".format(stats))
        statsItemsList = ConversionService.createStatsList(stats, NetflowStatsItem)

        for netflowStatItem in statsItemsList:
            connItemsList=ConversionService.createStatsList(netflowStatItem.conn, NetflowConnItem)
            netflowStatItem.conn= connItemsList
        return NetflowStatsContainer(statsItemsList)

    def getNetflowStatValue(cls, webApiSession, statName, collectorIp, slotId = None, groupId = 0):
        statsItemsList = cls.getNetflowStats(webApiSession, groupId=groupId)
        for netflowStatItem in statsItemsList.getList():
            if netflowStatItem.enabled == False:
                continue
            if (slotId == None) or (netflowStatItem.slotNum == int(slotId)):
                for connStatItem in netflowStatItem.conn:
                    if connStatItem.collector == collectorIp:
                        return getattr(connStatItem, statName.statType)