import json
from http import HTTPStatus
from atipAutoFrwk.config.atip.TcpModeConfig import TcpModeConfig
from atipAutoFrwk.services.ConversionService import ConversionService


class TcpMode(object):


    @classmethod
    def getTcpConfig(cls, webApiSession, groupId, expectedStatus=HTTPStatus.OK):
        '''
        ':type webApiSession: WebApiSession'
        returns: a JSON
        '''
        path = '{}/tcpMode'.format(groupId)
        print(path)
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        tcpConfig = ConversionService.createBasicObject(json.loads(response.text).get("tcpMode"), TcpModeConfig)
        print(tcpConfig)
        return tcpConfig

    @classmethod
    def updateTcpConfig(cls, webApiSession, tcpConfig, groupId, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type tcpConfig: TcpModeConfig'
        '''
            :param tcpConfig : the configuration object to be posted
            :return: response
        '''

        path = '{}/tcpMode'.format(groupId)
        fullTcpConfig = {"tcpMode": tcpConfig}
        response = webApiSession.post(path, reqData=fullTcpConfig, expectedStatus=expectedStatus)
        return response




