import json
from http import HTTPStatus

from atipAutoFrwk.config.atip.ConnTableGlobalConfig import ConnTableGlobalConfig
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService


class ConnTableGlobalConfig(object):
    @classmethod
    def getConnTableGlobalConfig(cls, webApiSession, groupId = 0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = "{}/connTableGlobalConfig".format(groupId)
        response = webApiSession.get(path)
        theJSON = json.loads(response.text)
        connTableConfig = ConversionService.createBasicObject(theJSON.get('globalConfig'), ConnTableGlobalConfig)
        return connTableConfig

    @classmethod
    def updateConnTableGlobalConfig(cls, webApiSession, connTableConfig, groupId = 0):
        ':type webApiSession: WebApiSession'
        ':type connTableConfig: ConnTableGlobalConfig'
        '''
            :param connTableConfig : the configuration object to be posted
            :return: response
        '''

        path = '{}/connTableGlobalConfig'.format(groupId)
        response = webApiSession.post(path, reqData=connTableConfig)
        return response

    @classmethod
    def resetConnTableGlobalConfig(cls, webApiSession, groupId=0):
        ':type webApiSession: WebApiSession'
        ':type connTableConfig: ConnTableGlobalConfig'
        '''
            :param connTableConfig : the configuration object to be posted
            :return: response
        '''

        path = '{}/connTableGlobalConfig/reset'.format(groupId)
        response = webApiSession.post(path)
        theJSON = json.loads(response.text)
        connTableConfig = ConversionService.createBasicObject(theJSON.get('globalConfig'), ConnTableGlobalConfig)
        return connTableConfig





