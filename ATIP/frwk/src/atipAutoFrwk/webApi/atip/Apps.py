from http import HTTPStatus
import requests.exceptions
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
import json

class Apps(object):

    @classmethod
    def getAppsList(cls, webApiSession):
        ':type webApiSession: WebApiSession'

        path = 'apps/list'
        response = webApiSession.get(path)
        return json.loads(response.text)

    @classmethod
    def getActionsForApp(cls, webApiSession, expectedApp):
        '''
        :return: the header masks configuration
        '''

        theJSON = cls.getAppsList(webApiSession)
        for app in theJSON['apps']:
            if (app["id"] == expectedApp):
                return app['actions']

    @classmethod
    def getActionId(cls, webApiSession, expectedApp, expectedAction):
        '''
        :return: the header masks configuration
        '''

        theJSON = cls.getActionsForApp(webApiSession,expectedApp)
        for action in theJSON:
            if (action["name"] == expectedAction):
                print(action['id'])
                return action['id']
