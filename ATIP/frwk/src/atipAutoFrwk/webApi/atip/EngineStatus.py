from http import HTTPStatus
import requests.exceptions
import json
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders

class EngineStatus(object):

    @classmethod
    def getEngineStatus(cls, webApiSession, expectedStatus):
        ':type webApiSession: WebApiSession'

        path = 'com/engineStatus'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)

        if response.status_code != expectedStatus:
            raise requests.exceptions.RequestException(
                'Expected message {}. Actual message: {}'.format(response.text, response.text))
        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def getVersion(cls, webApiSession):
        ':type webApiSession: WebApiSession'

        try:
            theJSON = EngineStatus.getEngineStatus(webApiSession, expectedStatus=HTTPStatus.OK)
        except (ConnectionError):
            return False
        version = '1.5.2'
        if ("version" in theJSON):
            version = theJSON["version"]
            return version
        else:
            return False



