from http import HTTPStatus

from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from requests_toolbelt.multipart.encoder import MultipartEncoder
import os


class CustomIP(object):

    @classmethod
    def uploadCustomIp(cls, webApiSession,  fileName, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = '{}/custom/geo'.format(groupId)
        print(groupId)
        print(fileName)
        print(path)
        multipart = MultipartEncoder(
            fields={'file': (os.path.basename(fileName), open(fileName, 'rb'), 'application/vnd.ms-excel')})

        reqHeaders = {RequestHeaders.CONTENT_TYPE: multipart.content_type}
        response = webApiSession.post(path,multipart,reqHeaders,expectedStatus=expectedStatus)
        return response