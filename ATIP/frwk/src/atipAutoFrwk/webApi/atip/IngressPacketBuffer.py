import json
from http import HTTPStatus

from atipAutoFrwk.config.atip.IngressPacketBufferConfig import IngressPacketBufferConfig
from atipAutoFrwk.services.ConversionService import ConversionService


class IngressPacketBuffer(object):

    @classmethod
    def getIngressPacketBufferConfig(cls, webApiSession, groupId=0, expectedStatus=HTTPStatus.OK):
        '''

        :param webApiSession:
        :param groupId:
        :param expectedStatus:
        :return:
        '''
        path = "{}/networkConfig/ingressPacketBuffer".format(groupId)
        response = webApiSession.get(path)
        theJSON = json.loads(response.text)
        ingressPacketBufferConfig = ConversionService.createBasicObject(theJSON, IngressPacketBufferConfig)
        return ingressPacketBufferConfig

    @classmethod
    def updateIngressPacketBufferConfig(cls, webApiSession, ingressPacketBufferConfig, groupId=0):
        '''

        :param webApiSession:
        :param ingressPacketBufferConfig:
        :param groupId:
        :return:
        '''

        path = '{}/networkConfig/ingressPacketBuffer'.format(groupId)

        response = webApiSession.post(path, reqData=ingressPacketBufferConfig)
        theJSON = json.loads(response.text)
        ingressPacketBufferConfig = ConversionService.createBasicObject(theJSON, IngressPacketBufferConfig)
        return ingressPacketBufferConfig