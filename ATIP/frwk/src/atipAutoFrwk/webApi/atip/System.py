from http import HTTPStatus
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
import json


class System(object):
    
    @classmethod
    def resetStats(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        '''

        path = 'system/resetStats'
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        return response

    @classmethod
    def resetAtip(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        '''

        path = 'system/resetATIP'
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        return response

    @classmethod
    def readDebug(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        '''

        path = 'debug'
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        return response.text

    @classmethod
    def readVersionJson(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        '''

        path = 'update'
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        return response

    @classmethod
    def readAtipVersion(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        '''

        output = System.readVersionJson(webApiSession, expectedStatus)
        obj = json.loads(output.content.decode('utf-8'))
        return obj["softwareVersion"]

    @classmethod
    def readSslDebug(cls, webApiSession, groupId=0, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        '''

        path = '{}/sslDebugStats'.format(groupId)
        print(path)
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        return response.text

    @classmethod
    def readDedupDebug(cls, webApiSession, groupId=0, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        '''

        path = '{}/dedupDebugStats'.format(groupId)
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        return response.text

    @classmethod
    def readRapSheetDebug(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'debug/rapsheet/version'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response.text