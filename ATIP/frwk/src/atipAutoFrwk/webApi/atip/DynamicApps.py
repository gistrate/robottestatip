import os
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from http import HTTPStatus

class DynamicApps(object):

    @classmethod
    def downloadDynamicApp(cls, webApiSession, appName, sigFile, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        '''
        returns: a JSON
        '''
        path = '{}/appxml?id={}'.format(groupId, appName)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_XML}
        response = webApiSession.get(path, None, reqHeaders, expectedStatus=expectedStatus)
        pathname = os.path.abspath(sigFile)
        print('path', pathname)
        print('full path', os.path.abspath(pathname))
        with open(pathname, 'w') as f:
             f.write(response.text)
        return response


    @classmethod
    def deleteDynamicApp(cls, webApiSession, appName, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.delete('{}/apps/dynamic/{}'.format(groupId, appName), reqHeaders,
                                            expectedStatus=expectedStatus)
        return response