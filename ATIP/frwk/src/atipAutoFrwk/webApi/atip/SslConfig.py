import os
import json
from http import HTTPStatus
from pprint import pprint
from atipAutoFrwk.config.atip.SslGlobalConfig import SslGlobalConfig
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from requests_toolbelt.multipart.encoder import MultipartEncoder
from lib2to3.pgen2.tokenize import group
import jsonpickle

class SslConfig(object):

    @classmethod
    def getJSON(cls, webApiSession, location, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        '''
        returns: a JSON
        '''
        path = location
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        theJSON = json.loads(response.text)
        return theJSON
#
    @classmethod
    def updateSslSettings(cls, webApiSession, bPassiveSSLDecryption=None, groupId=0, expectedStatus=HTTPStatus.OK):
        '''
        Args:
            webApiSession (WebApiSession): the current session
            bPassiveSSLDecryption : Flase, enable/disable pssiveSSLDecryption
        '''

        path = '{}/sslGlobalConfig'.format(groupId)
        theSslSettings = cls.getSslSettings(webApiSession, groupId=groupId)
        if bPassiveSSLDecryption != None:
            theSslSettings.passiveSSLDecryption = bPassiveSSLDecryption
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.post(path, reqHeaders=reqHeaders, reqData=theSslSettings, expectedStatus=expectedStatus)

        theSslSettings = cls.getSslSettings(webApiSession, groupId=groupId)
        return theSslSettings

    @classmethod
    def getSslSettings(cls, webApiSession, groupId=0):
        '''
        :param webApiSession:
        :return: the Ssl Global configuration (no items)
        '''
        theJSON = cls.getJSON(webApiSession=webApiSession, location="{}/sslGlobalConfig".format(groupId), expectedStatus=HTTPStatus.OK)

        if ("sslGlobalConfig" in theJSON):
            sslConfig = SslGlobalConfig()
            setattr(sslConfig, "passiveSSLDecryption", theJSON['sslGlobalConfig']['passiveSSLDecryption'])
            return sslConfig
        return None

    @classmethod
    def uploadSeparateFiles(cls, webApiSession, name=None, fileCert=None, fileKey=None, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = '{}/sslCertificates'.format(groupId)

        multipart = MultipartEncoder(fields={'enabled': 'true',
                                             'name': name,
                                             'formatRadio': 'separateFiles',
                                             'certificateFile': (os.path.basename(fileCert),
                                                                 open(fileCert, 'rb'),
                                                                 'application/octet-stream'),
                                             'privateKeyFile' : (os.path.basename(fileKey),
                                                                 open(fileKey, 'rb'),
                                                                 'application/octet-stream')
                                             })

        reqHeaders = {RequestHeaders.CONTENT_TYPE: multipart.content_type}
        response = webApiSession.post(path, multipart, reqHeaders, expectedStatus=expectedStatus)
        return response.json()['certificates'][0]['id']

    @classmethod
    def uploadSingleFile(cls, webApiSession, fileCombined=None, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'sslCertificates'
        multipart = MultipartEncoder(fields={'enabled': 'true',
                                             'formatRadio': 'singleFile',
                                             'combinedFile': (os.path.basename(fileCombined),
                                                                 open(fileCombined, 'rb'),
                                                                 'application/octet-stream')
                                             })
        reqHeaders = {RequestHeaders.CONTENT_TYPE: multipart.content_type}
        response = webApiSession.post(path, multipart, reqHeaders, expectedStatus=expectedStatus)
        return response.json()

    @classmethod
    def bulkUploadSingleFile(cls, webApiSession, fileCombined=None, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = '{}/sslCertificates'.format(groupId)

        if fileCombined :
            if not isinstance(fileCombined, list):
                multipart = MultipartEncoder(fields={'enabled': 'true',
                                             'formatRadio': 'singleFile',
                                             'combinedFile': (os.path.basename(fileCombined),
                                                                 open(fileCombined, 'rb'),
                                                                 'application/octet-stream')
                                             })
            else:
                print(fileCombined)
                fields=[('enabled', 'true'), ('formatRadio', 'singleFile')]
                for filename in fileCombined:
                    fields.append(('combinedFile', (os.path.basename(filename), open(filename, 'rb'), 'application/octet-stream')))

                print(fields)
                multipart = MultipartEncoder(fields)

        reqHeaders = {RequestHeaders.CONTENT_TYPE: multipart.content_type}
        response = webApiSession.post(path, multipart, reqHeaders, expectedStatus=expectedStatus)
        return response.json()


    @classmethod
    def getSslCerts(cls, webApiSession, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = '{}/sslCertificates'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response.json()

    @classmethod
    def deleteAllCerts(cls, webApiSession, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        response = None
        path = '{}/sslCertificates'.format(groupId)
        allCertsList = cls.getSslCerts(webApiSession, groupId=groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        if not allCertsList.get('certificates'):
            print('No Certificates config present on system...')
        else:
            if not isinstance(allCertsList.get('certificates'), list):
                print('Deleting certificate {}'.format(allCertsList.get('certificates').get('id')))
                errorMessage = 'Unable to remove certificate (id: {}; name: {})'.format(
                    allCertsList.get('certificates').get('id'), allCertsList.get('certificates').get('name'))
                response = webApiSession.delete('{}/{}'.format(path, allCertsList.get('certificates').get('id')), reqHeaders,
                                                expectedStatus=expectedStatus, errorMessage=errorMessage)
            else:
                for item in allCertsList.get('certificates'):
                    print('Deleting certificate {} {}'.format(item['id'], item['name']))
                    errorMessage = 'Unable to remove certificate (id: {}; name: {} )'.format(
                        item['id'], item['name'])
                    response = webApiSession.delete('{}/{}'.format(path, item['id']), reqHeaders,
                                                    expectedStatus=expectedStatus, errorMessage=errorMessage)

        return response

    @classmethod
    def modifyCert(cls, webApiSession, groupId=0, certName=None, isEnabled=True, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type isEnabled: bool'

        allCertsList = cls.getSslCerts(webApiSession, groupId=groupId)

        response = None
        certNamesId = dict()
        for cert in allCertsList.get('certificates'):
            certNamesId[cert.get('name')] = cert.get('id')
        if certName not in certNamesId:
            print('Certificate not present in list')

        else:
            path = '{}/sslCertificates/{}/'.format(groupId, certNamesId[certName])
            reqData = {"id": certNamesId[certName],"enabled": isEnabled, "name": certName}

            response = webApiSession.put(path, reqData, expectedStatus=expectedStatus)
        return response

    @classmethod
    def bulkDeleteAllCerts(cls, webApiSession, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        response = None
        idList =[]
        path = '{}/sslCertificates'.format(groupId)
        allCertsList = cls.getSslCerts(webApiSession, groupId=groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        if not allCertsList.get('certificates'):
            print('No Certificates config present on system...')
        else:
            if not isinstance(allCertsList.get('certificates'), list):
                print('Deleting certificate {}'.format(allCertsList.get('certificates').get('id')))
                errorMessage = 'Unable to remove certificate (id: {}; name: {})'.format(
                    allCertsList.get('certificates').get('id'), allCertsList.get('certificates').get('name'))
                idList = [allCertsList.get('certificates').get('id')]
            else:
                reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
                for item in allCertsList.get('certificates'):
                    print('Deleting certificate {} {}'.format(item['id'], item['name']))
                    errorMessage = 'Unable to remove certificate (id: {}; name: {} )'.format(
                        item['id'], item['name'])
                    idList.append(item['id'])
                print(idList)

            reqData = json.dumps(idList, separators=(',',':'))
            response = webApiSession.session.delete(webApiSession.url+path,
                                                    data=reqData,
                                                    headers=reqHeaders,
                                                    verify=webApiSession.connConfig.verifyCertificate)
            webApiSession.checkResponse(response, expectedStatus, errorMessage)

        return response

    @classmethod
    def getSslPortMaps(cls, webApiSession, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = '{}/sslPortMaps'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response.json()

    @classmethod
    def addSslPortMap(cls, webApiSession, portMapObj, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = '{}/sslPortMaps'.format(portMapObj.groupId)
        response = webApiSession.post(path, reqData=portMapObj, expectedStatus=expectedStatus)
        print("Response {}".format(response.json()))
        portMapObj.id = cls.getSslPortMapId(webApiSession, portMapObj)
        return response.json()

    @classmethod
    def getSslPortMapId(cls, webApiSession, portMapObj, groupId=0):
        ':type webApiSession: WebApiSession'

        sslPortMaps = cls.getSslPortMaps(webApiSession, groupId=groupId)
        for i in sslPortMaps.get('mappings'):
            if portMapObj.description == i.get('description'):
                currentID = i.get('id')
        print('current id: {}'.format(currentID))
        return currentID

    @classmethod
    def updateSslPortMap(cls, webApiSession, updatedPortMapObj):
        ':type webApiSession: WebApiSession'
        path = '{}/sslPortMaps/'.format(updatedPortMapObj.groupId) + str(updatedPortMapObj.id)
        response = webApiSession.put(path, reqData=updatedPortMapObj)
        return response.json()

    @classmethod
    def deleteSslPortMap(cls, webApiSession, portMapObj):
        ':type webApiSession: WebApiSession'
        path = '{}/sslPortMaps/'.format(portMapObj.groupId) + str(portMapObj.id)
        response = webApiSession.delete(path)
        return response.json()
