from http import HTTPStatus
import json
import requests.exceptions
from atipAutoFrwk.webApi.atip.Login import Login


class Status(object):

    @classmethod
    def getStatus(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'status'
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        return json.loads(response.text)

    @classmethod
    def isATIPSystemReady(cls, webApiSession, password=None):
        ':type webApiSession: WebApiSession'

        ready = False

        try:
            theJSON = Status.getStatus(webApiSession)
        except ConnectionError:
            return False
        except requests.exceptions.RequestException:
            try:
                #sometimes the user gets logged out after clear system, although it managed to log in once
                Login.login(webApiSession, password=password)
            except Exception:
                pass
            ready = False

        try:
            ready = theJSON['status']['systemStatus'] == 'ready'
        except Exception:
            ready = False

        return ready


    @classmethod
    def isReady(cls, webApiSession, password=None):
        ':type webApiSession: WebApiSession'

        try:
            theJSON = Status.getStatus(webApiSession)
        except ConnectionError:
            return False
        except requests.exceptions.RequestException as e:
            try:
                #sometimes the user gets logged out after clear system, although it managed to log in once
                Login.login(webApiSession, password=password)
            except Exception:
                pass
            allReady = False

        allReady = True
        try:
            if (type(theJSON['status']['npStatus']) == dict):
                if theJSON['status']['npStatus']['status'] != 'ready':
                    allReady = False
            else:  # 7300
                for crtNp in theJSON['status']['npStatus']:
                    if crtNp['status'] != 'ready':
                        allReady = False
        except Exception:
            allReady = False

        return allReady

    @classmethod
    def isLoggedIn(cls, webApiSession):
        ':type webApiSession: WebApiSession'

        try:
            theJSON = Status.getStatus(webApiSession)
        except (ConnectionError, requests.exceptions.RequestException):
            return False
        if ("No session found." in theJSON):
            return False
        return True

    @classmethod
    def isLoggedOut(cls, webApiSession):
        '''
        Returns True only if the status request responds that there is no session.
        '''
        ':type webApiSession: WebApiSession'

        try:
            response = Status.getStatus(webApiSession)
        except ConnectionError:
            return False
        except requests.exceptions.RequestException:
            return True
        if ("No session found." in response):
            return True
        return False
    
