from http import HTTPStatus

import jsonpickle

from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService


class Groups(object):
    @classmethod
    def getGroups(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'groups'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response.json()
    
    @classmethod
    def getAllGroups(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        '''

        path = 'groups'
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        groupList = []
        for group in response.json().get('groups'):
            ConversionService.createBasicObject(group, GroupConfig)

        return groupList

    @classmethod
    def addGroup(cls, webApiSession, groupConfig, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type groupConfig: GroupConfig'

        path = 'groups'
        response = webApiSession.post(path, groupConfig.cleandict(groupConfig.__dict__), expectedStatus=expectedStatus)
        return response.json()

    @classmethod
    def getNPsInGroup(cls, webApiSession, groupId, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'groups/{}'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response.json()

    @classmethod
    def getGroupNpIdList(cls, webApiSession, groupId, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        '''

        path = 'groups/{}'.format(groupId)
        response = webApiSession.get(path, expectedStatus=expectedStatus)

        npIdList = []
        if response.json().get('nps') : 
            for crtNp in response.json().get('nps'):
                npIdList.append(crtNp.get('id'))
        return npIdList


    @classmethod
    def deleteGroup(cls, webApiSession, groupId, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'groups/{}'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.delete(path, reqHeaders, expectedStatus=expectedStatus)
        return response.json()

    @classmethod
    def updateGroup(cls, webApiSession, groupConfig, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type groupConfig: GroupConfig'

        path = 'groups'
        response = webApiSession.put(path, groupConfig.cleandict(groupConfig.__dict__), expectedStatus=expectedStatus)
        return response.json()

    @classmethod
    def getNPList(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'np'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response.json()

    @classmethod
    def changeNPGroup(cls, webApiSession, npId, groupId, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'np'
        response = webApiSession.put(path, {"id": npId, "group": groupId}, expectedStatus=expectedStatus)
        print('\r\nMoving NP{}/{}/{}'.format(path, npId, groupId))
        return response.json()

    @classmethod
    def getGroupId(self, webApiSession, name):
        groups = Groups.getGroups(webApiSession)
        for item in groups.get('groups'):
            if item.get('name') == name:
                return item.get('id')
        return None
    
    @classmethod
    def getStatus(self, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        
        path = 'groupStatus'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response.json()