import json
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from http import HTTPStatus

class Capture(object):

    @classmethod
    def getCaptureFileList(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        '''
        Get all PCAP files stored on Atip
        :param webApiSession: current session to Atip
        :param path: rest api endpoint to get stored PCAP files
        :return: a JSON containing all PCAP file names on Atip
        '''

        path = 'capture/file/'
        response = webApiSession.get(path, expectedStatus=expectedStatus)

        theJSON = json.loads(response.text)
        return theJSON, path

    @classmethod
    def deleteCaptureFiles(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        '''
        Delete all PCAP files from Atip
        :type webApiSession: WebApiSession
        :returns response from Atip
        '''

        response = None
        allCaptureFiles, path = cls.getCaptureFileList(webApiSession)

        if not allCaptureFiles:
            print('No capture file...')
        else:
            if not isinstance(allCaptureFiles['rulesFile'], list):
                print('Deleting capture file {}'.format(allCaptureFiles['rulesFile']['fileName']))
                errorMessage = 'Unable to remove capture file {})'.format(allCaptureFiles['rulesFile']['fileName'])
                response = webApiSession.delete('{}{}'.format(path, allCaptureFiles['rulesFile']['fileName']), expectedStatus=expectedStatus, errorMessage=errorMessage)
            else:
                for item in allCaptureFiles['rulesFile']:
                    print('Deleting capture file {}'.format(item['fileName']))
                    errorMessage = 'Unable to remove capture file: {}'.format(item['fileName'])
                    response = webApiSession.delete('{}{}'.format(path, item['fileName']), expectedStatus=expectedStatus, errorMessage=errorMessage)

        return response

    @classmethod
    def deleteCaptureFileByName(cls, webApiSession, fileName, expectedStatus=HTTPStatus.OK):
        '''
        Delete specific PCAP file from Atip
        :param webApiSession: current session to Atip
        :param fileName: PCAP file name to delete from Atip
        :return: response from Atip
        '''

        response = None
        found = False
        allCaptureFiles, path = cls.getCaptureFileList(webApiSession)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        if not allCaptureFiles['rulesFile']:
            print('No capture file...')
        else:
            if not isinstance(allCaptureFiles['rulesFile'], list):
                if allCaptureFiles['rulesFile']['fileName'] == fileName:
                    print('Deleting capture file {}'.format(allCaptureFiles['rulesFile']['fileName']))
                    errorMessage = 'Unable to remove capture file {})'.format(allCaptureFiles['rulesFile']['fileName'])
                    response = webApiSession.delete('{}{}'.format(path, allCaptureFiles['rulesFile']['fileName']), reqHeaders, expectedStatus=expectedStatus, errorMessage=errorMessage)
                    found = True
            else:
                for index in range(len(allCaptureFiles['rulesFile'])):
                    if allCaptureFiles['rulesFile'][index].get('fileName') == fileName:
                        print('Deleting capture file {}'.format(fileName))
                        errorMessage = 'Unable to remove capture file: {}'.format(fileName)
                        response = webApiSession.delete('{}{}'.format(path, allCaptureFiles['rulesFile'][index].get('fileName')), reqHeaders, expectedStatus=expectedStatus, errorMessage=errorMessage)
                        found = True
        if not found:
            raise Exception('Capture file {} not found.'.format(fileName))

        return response

    @classmethod
    def controlCapture(cls, webApiSession, ruleId, action, expectedStatus=HTTPStatus.OK):
        '''
        Control capture per filter.
        :param webApiSession: current session to Atip
        :param ruleId: filter ID to apply action
        :param action: can be True to start capture or False to stop capture
        :return: response from Atip
        '''

        data = {"enable":'{}'.format(action), "force":True}
        path = 'capture/{}'.format(ruleId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.put(path, reqHeaders=reqHeaders, reqData=data, expectedStatus=expectedStatus)

        return response

    @classmethod
    def editCaptureSize(cls, webApiSession, ruleId, captureSize, expectedStatus=HTTPStatus.OK):
        '''
        Set capture file size
        :param webApiSession: current session to Atip
        :param ruleId: filter ID to set capture file size
        :param captureSize: new capture file size in bits
        :return: response from Atip
        '''

        data = {"captureSize":'{}'.format(int(captureSize))}
        path = 'capture/{}/edit'.format(ruleId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.put(path, reqHeaders=reqHeaders, reqData=data, expectedStatus=expectedStatus)

        return response

    @classmethod
    def captureRunningStatus(cls, webApiSession, groupId=0, expectedStatus=HTTPStatus.OK):
        '''
        Verify capture running status
        :returns  boolean value; response JSON from Atip should contain 'runningCaptureFilterId': <filterID>  key/value pair when capture is running
        '''
        path = 'groupStatus'
        status = False
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)

        for groupStatus in json.loads(response.text)['groups']:
            print(groupStatus) 
            if groupStatus['id'] == groupId and 'runningCaptureFilterId' in groupStatus.keys():
                status = True

        return status

