from http import HTTPStatus
import json
import os
from requests_toolbelt.multipart.encoder import MultipartEncoder

from atipAutoFrwk.config.atip.SystemInfo import SystemInfo
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService


class Update(object):

    @classmethod
    def getSystemInfo(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        :rtype: SystemInfo
        '''

        path = 'update'

        response = webApiSession.get(path, expectedStatus=expectedStatus)
        systemInfo = ConversionService.createBasicObject(json.loads(response.text), SystemInfo)
        systemInfo.updateBuildAndVersion()
        return systemInfo


    @classmethod
    def updateSoftware(cls, webApiSession, buildPath, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        :type buildPath:  string containing a path to the build file that will be uploaded
        :rtype: response.content
        '''

        path = 'update/software'

        multipart = MultipartEncoder(fields={'field0': (os.path.basename(buildPath), open(buildPath, 'rb'),
                                                        'application/octet-stream')})
        reqHeaders = {RequestHeaders.CONTENT_TYPE: multipart.content_type}
        response = webApiSession.post(path, multipart, reqHeaders, expectedStatus=expectedStatus)
        return response.content

    @classmethod
    def updateAti(cls, webApiSession, buildPath, expectedStatus=HTTPStatus.OK):
        '''
        :type webApiSession: WebApiSession
        :type buildPath:  string containing a path to the build file that will be uploaded
        :rtype: response.content
        '''

        path = 'update/ati'

        multipart = MultipartEncoder(fields={'field0': (os.path.basename(buildPath), open(buildPath, 'rb'),
                                                        'application/octet-stream')})
        reqHeaders = {RequestHeaders.CONTENT_TYPE: multipart.content_type}
        response = webApiSession.post(path, multipart, reqHeaders, expectedStatus=expectedStatus)
        return response.content