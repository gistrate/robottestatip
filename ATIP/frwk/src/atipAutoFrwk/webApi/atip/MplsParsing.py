import json

from atipAutoFrwk.config.atip.MplsParsingConfig import MplsParsingConfig
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService


class MplsParsing(object):

    @classmethod
    def updateMplsParsing(cls, webApiSession, mplsParsingConfig):
        ':type webApiSession: WebApiSession'
        ':type mplsParsingConfig: MplsParsingConfig'
        '''
            :param mplsParsingConfig : the configuration object to be posted
            :return: nothing
        '''

        path = '{}/mplsGlobalConfig'.format(mplsParsingConfig.groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.post(path, reqHeaders=reqHeaders, reqData=mplsParsingConfig)

    @classmethod
    def getMplsParsing(cls, webApiSession, groupId=0):
        ':type webApiSession: WebApiSession'
        '''
        returns: a JSON
        '''
        path = '{}/mplsGlobalConfig'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders)
        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def getMplsParsingConfig(cls, webApiSession, groupId=0):
        '''
        :return: a class item of type MplsParsingConfig
        '''
        theJSON = cls.getMplsParsing(webApiSession,groupId)
        mplsParsingConfig = ConversionService.createBasicObject(theJSON.get('globalConfig'), MplsParsingConfig)
        return mplsParsingConfig
