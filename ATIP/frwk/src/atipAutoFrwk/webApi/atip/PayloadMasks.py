from http import HTTPStatus

import json
from atipAutoFrwk.config.atip.PayloadMask import PayloadMask
from atipAutoFrwk.config.atip.PayloadMaskConfig import PayloadMaskConfig
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.webApi.atip.DataMasking import DataMasking
from lib2to3.pgen2.tokenize import group


class PayloadMasks(object):

    @classmethod
    def getPayloadMasks(cls, webApiSession, groupId=0):
        '''
        :param webApiSession:
        :return: the payload masks configuration
        '''

        theJSON = DataMasking.getJSON(webApiSession=webApiSession, location="{}/regexMasks".format(groupId))
        payloadMaskConfig = PayloadMaskConfig()
        if ("masks" in theJSON):
            if (type(theJSON["masks"]) == list):
                for i in range(len(theJSON['masks'])):
                    currentPayloadMask = ConversionService.createBasicObject(theJSON['masks'][i], PayloadMask)
                    currentPayloadMask.groupId = groupId
                    payloadMaskConfig.payloadMaskList[currentPayloadMask.name] = currentPayloadMask
            else:
                currentPayloadMask = ConversionService.createBasicObject(theJSON['masks'], PayloadMask)
                currentPayloadMask.groupId = groupId
                payloadMaskConfig.payloadMaskList[currentPayloadMask.name] = currentPayloadMask
            return payloadMaskConfig
        else:
            PayloadMaskConfig()

    @classmethod
    def getPayloadConfigByName(cls, webApiSession, name, groupId=0):
        '''
        :param webApiSession: the atip webApi session
        :param name: the name of the payload to get config
        :return: your usual new payload mask config
        '''

        dmConf = DataMasking.getDataMaskingConfig(webApiSession,groupId=groupId)
        if (dmConf.enabled != True):
            raise ValueError("Datamasking must be enabled!")
        payloadMaskConfig = cls.getPayloadMasks(webApiSession, groupId=groupId)
        currentID = payloadMaskConfig.payloadMaskList[name].id
        path = '{}/regexMasks/'.format(groupId) + str(currentID)

        response = webApiSession.get(path)

        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def deletePayloadMasks(cls, webApiSession, expectedStatus=HTTPStatus.OK, groupId=0):
        '''
        :param webApiSession: the current session (that thing you need for all the operations)
        :param expectedStatus: usually HTTPStatus.OK if no catastrophe happens again
        :return: the new, empty configuration
        '''

        dmConf = DataMasking.getDataMaskingConfig(webApiSession, groupId=groupId)
        if (dmConf.enabled != True):
            raise ValueError("Datamasking must be enabled!")

        path = 'regexMasks'
        payloadMasks = cls.getPayloadMasks(webApiSession, groupId=groupId)

        for crtKey, crtMask in payloadMasks.payloadMaskList.items():
            if not crtMask.readOnly:
                cls.deletePayloadConfigByID(webApiSession, crtMask.id)

        payloadMasks = cls.getPayloadMasks(webApiSession, groupId=groupId)

        return payloadMasks

    @classmethod
    def deletePayloadConfigByID(cls, webApiSession, currentID, expectedStatus=HTTPStatus.OK,groupId=0):
        '''
        Auxiliary method for deletePayloadConfigByName
        CAREFUL! NOT TO BE USED UNLESS YOU KNOW THE ID (the ID is not based on the order in the list)
        You should get the information based on the name, which finds the ID automatically!
        ( cls.deletePayloadConfigByName )
        :param webApiSession: that thing you need for all the operations
        :param currentID: the ID of the desired header to be deleted
        :param expectedStatus: you know what this is already
        :return: the new configuration, without the header with that ID
        '''


        theJSON = DataMasking.getJSON(webApiSession=webApiSession, location="{}/regexMasks".format(groupId))

        position = theJSON['masks'][currentID]['name']

        path = '{}/regexMasks/'.format(groupId) + str(currentID)
        payloadMaskConfig = cls.getPayloadMasks(webApiSession)

        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.delete(
            path, reqHeaders=reqHeaders, reqData=payloadMaskConfig.payloadMaskList[position],
            expectedStatus=expectedStatus)

        payloadMaskConfig = cls.getPayloadMasks(webApiSession, groupId=groupId)

        return payloadMaskConfig

    @classmethod
    def addPayload(cls, atipSession, myPayload, expectedStatus = HTTPStatus.OK):
        '''

        :param atipSession: the current session
        :param myPayload: the PayloadMask object to be added
        :param expectedStatus: 200 OK if nothing bad happens (hopefully)
        :param id: if you prefer a certain card (if not, the method takes care of it for you)
        :return: the new payload configuration (if you are lucky)
        '''
        dmConf = DataMasking.getDataMaskingConfig(atipSession, myPayload.groupId)
        if (dmConf.enabled != True):
            raise ValueError("Datamasking must be enabled!")

        path = '{}/regexMasks/'.format(myPayload.groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = atipSession.post(path, reqHeaders=reqHeaders, reqData=myPayload,
                                      expectedStatus=expectedStatus)
        thePayloadMaskConfig = cls.getPayloadMasks(atipSession,myPayload.groupId)

        return thePayloadMaskConfig

    @classmethod
    def updatePayloadConfig(cls, webApiSession, payloadMask, expectedStatus = HTTPStatus.OK, groupId=0):
        '''

       :param webApiSession: the atip webApi session
       :param PayloadMask: The payload mask object
       :param expectedStatus: your usual HTTPStatus.OK (200)
       :return: the new, updated header mask
       '''
        theName = payloadMask.name

        dmConf = DataMasking.getDataMaskingConfig(webApiSession, groupId=groupId)
        if (dmConf.enabled != True):
            raise ValueError("Datamasking must be enabled!")

        thePayloadMaskConfig = cls.getPayloadMasks(webApiSession,groupId=groupId)

        theJSON = DataMasking.getJSON(webApiSession=webApiSession, location="{}/regexMasks".format(groupId))
        currentID = thePayloadMaskConfig.payloadMaskList[theName].id
        path = '{}/regexMasks/'.format(groupId) + str(currentID)
        setattr(thePayloadMaskConfig.payloadMaskList[theName], 'offsetEnd', payloadMask.offsetEnd)
        setattr(thePayloadMaskConfig.payloadMaskList[theName], 'offsetBegin', payloadMask.offsetBegin)
        setattr(thePayloadMaskConfig.payloadMaskList[theName], 'creditCard', payloadMask.creditCard)

        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.put(path,
                                     reqHeaders=reqHeaders,
                                     reqData=thePayloadMaskConfig.payloadMaskList[theName],
                                     expectedStatus=expectedStatus)

        thePayloadMaskConfig = cls.getPayloadMasks(webApiSession,groupId=groupId)

        return thePayloadMaskConfig

    @classmethod
    def deletePayloadConfigByName(cls, atipSession, name, groupId=0):
        '''

        :param atipSession: the thing you always need
        :param name: the name of the payload to be deleted
        :return: your usual new payload mask config
        '''

        dmConf = DataMasking.getDataMaskingConfig(atipSession, groupId=groupId)
        if (dmConf.enabled != True):
            raise ValueError("Datamasking must be enabled!")
        payloadMaskConfig = cls.getPayloadMasks(atipSession, groupId=groupId)
        currentID = payloadMaskConfig.payloadMaskList[name].id
        path = '{}/regexMasks/'.format(groupId) + str(currentID)

        response = atipSession.delete(
            path, reqData=payloadMaskConfig.payloadMaskList[name])
        payloadMaskConfig = cls.getPayloadMasks(atipSession, groupId=groupId)
        return payloadMaskConfig