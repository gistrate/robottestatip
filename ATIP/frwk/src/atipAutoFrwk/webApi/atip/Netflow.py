from http import HTTPStatus
import requests
import json
import jsonpickle
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig
from atipAutoFrwk.config.atip.NetflowCollectorConfig import NetflowCollectorConfig

class Netflow(object):

    @classmethod
    def getGlobalConfig(cls, webApiSession, expectedStatus = HTTPStatus.OK, groupId = 0):
        '''
        :param webApiSession: current session
        :return: a class item of type NetflowConfig
        '''
        path = '{}/netflowGlobalConfig'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)

        if response.status_code != expectedStatus:
            raise requests.exceptions.RequestException(
                'Expected message {}. Actual message: {}'.format(response.text, response.text))
        theJSON = json.loads(response.text)
        netflowConfig = ConversionService.createBasicObject(theJSON.get('globalConfig'), NetflowConfig)
        netflowConfig.groupId = int(groupId)
        return netflowConfig

    @classmethod
    def updateGlobalConfig(cls, webApiSession, netflowConfig, expectedStatus = HTTPStatus.OK):
        '''
        Args:
            webApiSession (WebApiSession): the current session
            netflowConfig (NetflowConfig): the configuration object to be posted
        '''

        path = '{:d}/netflowGlobalConfig'.format(netflowConfig.groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.post(path, reqHeaders=reqHeaders, reqData=netflowConfig, expectedStatus=expectedStatus)

        if response.status_code != expectedStatus:
            raise requests.exceptions.RequestException(
                'Expected message {}. Actual message: {}'.format(response.text, response.text))

    @classmethod
    def getCardConfig(cls, webApiSession, expectedStatus = HTTPStatus.OK, groupId = 0):
        ':type webApiSession: WebApiSession'
        path = '{}/netflowCardConfig'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)

        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def getCollectorConfig(cls, webApiSession, expectedStatus = HTTPStatus.OK, groupId = 0):
        ':type webApiSession: WebApiSession'

        path = '{}/netflowCollectorConfig'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)

        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def updateCollectorConfig(cls, webApiSession, crtCollector, groupId = 0):
        '''

        :param webApiSession: the current session
        :param crtCollector: the collector object
        :return: nothing
        '''

        path = '{}/netflowCollectorConfig/'.format(groupId) + str(crtCollector.id)
        response = webApiSession.put(path, reqData=crtCollector)
        return response.status_code

    @classmethod
    def getFirstAvailable(cls, webApiSession, groupId = 0):
        '''
        :param webApiSession: current session
        :return: the first available netflow configuration card or -1 if all of them are occupied
        '''
        theJSON = Netflow.getCollectorConfig(webApiSession, groupId=groupId)
        netflowCollectorConfig = NetflowCollectorConfig()
        for i in range(netflowCollectorConfig.MAXIMUM_COLLECTORS):
            if (not ('enabled' in theJSON['collectors'][i])):
                break
        NetflowCollectorConfig.FIRST_AVAILABLE = i
        if (i == netflowCollectorConfig.MAXIMUM_COLLECTORS):
            return -1
        return i

    @classmethod
    def updateCard(cls, webApiSession, netflowCardConfig, groupId = 0):
        '''
        Changes the key of the netflow card to the value given
        :param webApiSession: the current session
        :param netflowCardConfig: the current netflow card configuration (you can get this by calling getCollectorConfig
        :return: nothing
        '''
        path = '{}/netflowCardConfig/'.format(groupId) + str(netflowCardConfig.id)
        response = webApiSession.put(path, reqData=netflowCardConfig)
        return response.status_code

    @classmethod
    def addCollector(cls, webApiSession, netflowCollector, expectedStatus = HTTPStatus.OK, id=-1, groupId = 0):
        '''

        :param webApiSession: the current session
        :param netflowCollectorConfig: the Collector object to be added
        :param expectedStatus: 200 OK if nothing bad happens (hopefully)
        :param id: if you prefer a certain card (if not, the method takes care of it for you)
        :return: the response status code (200 OK if you are lucky)
        '''

        firstAvb = Netflow.getFirstAvailable(webApiSession, groupId=groupId)
        if (id == -1): # NO ID has been requested
            id = firstAvb
        netflowCollector.id = id
        path = '{}/netflowCollectorConfig'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.post(path, reqHeaders=reqHeaders, reqData=netflowCollector,
                                      expectedStatus=expectedStatus)
        return response.status_code







