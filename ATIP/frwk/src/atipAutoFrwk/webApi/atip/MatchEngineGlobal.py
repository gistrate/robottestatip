import json
from http import HTTPStatus

from atipAutoFrwk.config.atip.ConnTableGlobalConfig import ConnTableGlobalConfig
from atipAutoFrwk.config.atip.MatchEngineGlobalConfig import MatchEngineGlobalConfig
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService

class MatchEngineGlobal(object):
    @classmethod
    def getMatchEngineGlobalConfig(cls, webApiSession, groupId = 0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = "{}/matchEngineGlobalConfig".format(groupId)
        response = webApiSession.get(path)
        theJSON = json.loads(response.text)
        engineConfig = ConversionService.createBasicObject(theJSON.get('globalConfig'), MatchEngineGlobalConfig)
        return engineConfig

    @classmethod
    def updateMatchEngineGlobalConfig(cls, webApiSession, engineConfig, groupId = 0):
        ':type webApiSession: WebApiSession'
        ':type engineConfig: MatchEngineGlobalConfig'
        '''
            :param engineConfig : the configuration object to be posted
            :return: response
        '''

        path = '{}/matchEngineGlobalConfig'.format(groupId)
        response = webApiSession.post(path, reqData=engineConfig)
        theJSON = json.loads(response.text)
        engineConfig = ConversionService.createBasicObject(theJSON.get('globalConfig'), MatchEngineGlobalConfig)
        return engineConfig

    @classmethod
    def resetMatchEngineGlobalConfig(cls, webApiSession, groupId=0):
        ':type webApiSession: WebApiSession'
        '''
            :param webApiSession : the configuration object to be posted
            :return: response
        '''

        path = '{}/matchEngineGlobalConfig/reset'.format(groupId)
        response = webApiSession.post(path)
        theJSON = json.loads(response.text)
        engineConfig = ConversionService.createBasicObject(theJSON.get('globalConfig'), MatchEngineGlobalConfig)
        return engineConfig





