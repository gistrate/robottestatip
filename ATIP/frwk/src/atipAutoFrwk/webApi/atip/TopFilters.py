from http import HTTPStatus

from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders


class TopFilters(object):

    @classmethod
    def getTopFiltersStats(cls, webApiSession, groupId=0, timeInterval=TimeInterval.HOUR, limit= '10', expectedStatus=HTTPStatus.OK) :
        ':type webApiSession: WebApiSession'
        ':type timeInterval: TimeInterval'
        path = '{}/lists/top10?lists=rules&inv={}&lmt={}'.format(groupId,timeInterval.intervalName, limit)

        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response.json()

