from http import HTTPStatus
import requests.exceptions

from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.data.webApi.ResponseMessage import ResponseMessage
from atipAutoFrwk.webApi.atip.Users import Users


class Login(object):

    @classmethod
    def login(cls, webApiSession, username=None, password=None, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'login'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        
        if username is None:
            username = webApiSession.connConfig.username
        if password is None:
            password = webApiSession.connConfig.password
        
        reqData = {
            'userid': username,
            'password': password
            }

        response = webApiSession.post(path, reqData, reqHeaders, expectedStatus=expectedStatus)
        print("{}".format(response.text))
        if response.status_code == HTTPStatus.OK:
            if response.text != ResponseMessage.LOGIN_SUCCESSFUL:
                raise requests.exceptions.RequestException(
                'Expected message {}. Actual message: {}'.format(ResponseMessage.LOGIN_SUCCESSFUL, response.text, response.text))
                
        return response


    @classmethod
    def defaultLoginChangePassword(cls, webApiSession, oldPassword, newPassword=None, username=None, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        if username is None:
            username = webApiSession.connConfig.username
        if newPassword is None:
            newPassword = webApiSession.connConfig.password
        cls.login(webApiSession, username, oldPassword)
        response = Users.changePassword(webApiSession, oldPassword, newPassword)
        return response


