import os
from http import HTTPStatus
from pprint import pprint
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from requests_toolbelt.multipart.encoder import MultipartEncoder


class CustomAppConfig(object):
    @classmethod
    def uploadCustomAppFile(cls, webApiSession, sigFile=None, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = '{}/custom/apps'.format(groupId)
        multipart = MultipartEncoder(fields={'enabled': 'true',
                                             'formatRadio': 'singleFile',
                                             'combinedFile': (os.path.basename(sigFile),
                                                              open(sigFile, 'rb'),
                                                              'application/octet-stream')
                                             })
        reqHeaders = {RequestHeaders.CONTENT_TYPE: multipart.content_type}
        pprint("multipart=%s" % (multipart))
        response = webApiSession.post(path, multipart, reqHeaders, expectedStatus=expectedStatus)
        return response.json()

    @classmethod
    def validateCustomApp(cls, webApiSession, appName, numSigsStr, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        # Robot treats everything as a string so convert to int
        # numSigs = int(numSigsStr)
        numSigs = numSigsStr

        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get('{}/custom/apps'.format(groupId), None, reqHeaders, expectedStatus=expectedStatus)
        r = response.json()
        appList = r['apps']
        found = False
        # webapi endpoints will not return a list if there is only one element
        if ('list' not in str(type(appList))):
            appList = [appList]
        for app in appList:
            if app['name'] == appName:
                if app['signatures'] == numSigs:
                    found = True
                    break
                else:
                    raise Exception("Custom application uploaded but expected %s signatures and only found %s" % (
                    numSigs, app['signatures']))

        if found == False:
            raise Exception("Custom application (%s) not found after upload" % (appName))

        return True

    @classmethod
    def getCustomAppId(cls, webApiSession, appName, numSigsStr, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        # Robot treats everything as a string so convert to int
        # numSigs = int(numSigsStr)
        numSigs = numSigsStr

        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get('{}/custom/apps'.format(groupId), None, reqHeaders, expectedStatus=expectedStatus)
        r = response.json()
        appList = r['apps']
        appId = ""
        # webapi endpoints will not return a list if there is only one element
        if ('list' not in str(type(appList))):
            appList = [appList]
        for app in appList:
            if app['name'] == appName:
                if app['signatures'] == numSigs:
                    appId = app['ident']
                    break
                else:
                    raise Exception("Custom application uploaded but expected %s signatures and only found %s" % (
                        numSigs, app['signatures']))

        return appId


    @classmethod
    def deleteCustomApp(cls, webApiSession, appName, numSigsStr, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        foundApp = CustomAppConfig.validateCustomApp(webApiSession, appName, numSigsStr, groupId)
        appId = CustomAppConfig.getCustomAppId(webApiSession, appName, numSigsStr, groupId)

        if foundApp == False:
            raise Exception("Custom application on found")
        else:
            reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
            response = webApiSession.delete('{}/custom/apps/{}'.format(groupId, appId), reqHeaders,
                                            expectedStatus=expectedStatus)
            return response.json()
