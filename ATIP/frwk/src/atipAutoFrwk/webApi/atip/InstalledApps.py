
from http import HTTPStatus
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders


class InstalledApps(object):
    @classmethod
    def searchInstalledApps(cls, webApiSession, appName, groupId=0, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = '{}/apps/searchStrict?id={}'.format(groupId, appName)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response.json()
