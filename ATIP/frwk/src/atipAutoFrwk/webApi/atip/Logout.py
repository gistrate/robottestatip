from http import HTTPStatus


class Logout(object):

    @classmethod
    def logout(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        
        path = 'logout'

        response = webApiSession.post(path, expectedStatus=expectedStatus)
        return response
