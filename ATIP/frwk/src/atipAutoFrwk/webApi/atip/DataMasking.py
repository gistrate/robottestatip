import json

from atipAutoFrwk.config.atip.DataMaskingConfig import DataMaskingConfig
from atipAutoFrwk.config.atip.MplsParsingConfig import MplsParsingConfig
from atipAutoFrwk.config.atip.HeaderMaskConfig import HeaderMaskConfig
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.config.atip.HeaderMask import HeaderMask
from lib2to3.pgen2.tokenize import group


class DataMasking(object):

    @classmethod
    def getDataMasking(cls, webApiSession, groupId=0):
        ':type webApiSession: WebApiSession'
        '''
        returns: a JSON
        '''
        path = '{}/dataMasking'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders)
        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def getJSON(cls, webApiSession, location):
        ':type webApiSession: WebApiSession'
        '''
        returns: a JSON
        '''
        path = location
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders)

        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def getHeaderMaskByID(cls, webApiSession, ID, groupId=0):
        ':type webApiSession: WebApiSession'
        path = '{}/headerMasks/'.format(groupId) + str(ID)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders)

        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def updateDataMasking(cls, webApiSession, dataMaskingConfig):
        '''
            :param dataMaskingConfig : the configuration object to be posted
            :return: nothing
        '''

        path = '{}/dataMasking'.format(dataMaskingConfig.groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.post(path, reqHeaders=reqHeaders, reqData=dataMaskingConfig)

    @classmethod
    def updateMplsParsing(cls, webApiSession, mplsParsingConfig):
        '''
            :param mplsParsingConfig : the configuration object to be posted
            :return: nothing
        '''

        path = '{}/mplsGlobalConfig'.format(mplsParsingConfig.groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.post(path, reqHeaders=reqHeaders, reqData=mplsParsingConfig)

    @classmethod
    def getMplsParsing(cls, webApiSession, groupId=0):
        ':type webApiSession: WebApiSession'
        '''
        returns: a JSON
        '''
        path = '{}/mplsGlobalConfig'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders)
        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def getMplsParsingConfig(cls, webApiSession, groupId=0):
        '''
        :return: a class item of type MplsParsingConfig
        '''
        theJSON = DataMasking.getMplsParsing(webApiSession=webApiSession,groupId=groupId)
        mplsParsingConfig = ConversionService.createBasicObject(theJSON.get('globalConfig'), MplsParsingConfig)
        return mplsParsingConfig

    @classmethod
    def getDataMaskingConfig(cls, webApiSession, groupId=0):
        '''
        :return: a class item of type DataMaskingConfig
        '''
        theJSON = DataMasking.getDataMasking(webApiSession=webApiSession,groupId=groupId)
        dataMaskingConfig = ConversionService.createBasicObject(theJSON.get('dmGlobalConfig'), DataMaskingConfig)
        return dataMaskingConfig

    @classmethod
    def getHeaderMasks(cls, webApiSession, groupId=0):
        '''
        :return: the header masks configuration
        '''

        theJSON = DataMasking.getJSON(webApiSession=webApiSession, location="{}/headerMasks".format(groupId))
        headerMaskConfig = HeaderMaskConfig()
        if ("masks" in theJSON):
            if (type(theJSON["masks"]) != list):
                theJSON["masks"]=[theJSON["masks"]]

            for i in range(len(theJSON['masks'])):
                currentHeaderMask = ConversionService.createBasicObject(theJSON['masks'][i], HeaderMask)
                currentHeaderMask.groupId = groupId
                headerMaskConfig.headerMaskList[currentHeaderMask.name] = currentHeaderMask

            return headerMaskConfig
        else:
            return headerMaskConfig

    @classmethod
    def getHeaderMaskByName(cls, webApiSession, name, groupId=0):
        ':type webApiSession: WebApiSession'

        headerMaskConfig = cls.getHeaderMasks(webApiSession,groupId=groupId)
        currentID = headerMaskConfig.headerMaskList[name].id
        path = '{}/headerMasks/'.format(groupId) + str(currentID)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders)

        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def deleteHeaderMasks(cls, webApiSession, groupId=0):
        '''
        :param expectedStatus: usually HTTPStatus.OK if no catastrophe happens again
        :return: the new, empty configuration
        '''

        dmConf = cls.getDataMaskingConfig(webApiSession, groupId=groupId)
        if (dmConf.enabled != True):
            raise ValueError("Datamasking must be enabled!")

        theHeaderMaskConfig = cls.getHeaderMasks(webApiSession,groupId=groupId)

        if not theHeaderMaskConfig.headerMaskList:
            return

        for name in theHeaderMaskConfig.headerMaskList:
            path = '{}/headerMasks/'.format(groupId) + str(theHeaderMaskConfig.headerMaskList[name].id)
            response = webApiSession.delete(path, reqData=theHeaderMaskConfig.headerMaskList[name])

        theHeaderMaskConfig = cls.getHeaderMasks(webApiSession, groupId=groupId)
        return theHeaderMaskConfig

    @classmethod
    def deleteHeaderConfigByName(cls, webApiSession, name, groupId=0):
        '''
        :param currentID: the ID of the desired header to be deleted
        :param expectedStatus: you know what this is already
        :return: the new configuration, without the header with that ID
        '''

        dmConf = DataMasking.getDataMaskingConfig(webApiSession, groupId=groupId)
        if (dmConf.enabled != True):
            raise ValueError("Datamasking must be enabled!")

        headerMaskConfig = cls.getHeaderMasks(webApiSession, groupId=groupId)
        currentID = headerMaskConfig.headerMaskList[name].id
        path = '{}/headerMasks/'.format(groupId) + str(currentID)

        response = webApiSession.delete(path, reqData=headerMaskConfig.headerMaskList[name])
        headerMaskConfig = cls.getHeaderMasks(webApiSession,groupId=groupId)
        return headerMaskConfig

    @classmethod
    def updateHeaderConfig(cls, webApiSession, theName, key = None, value = None, groupId=0):
        '''
        :param theName: self explanatory.. the name of the header to be updated
        :param key: the key to be updated
        :param value: the value with which the key will be updated
        :param expectedStatus: your usual HTTPStatus.OK (200)
        :return: the new, updated header mask
        '''

        dmConf = cls.getDataMaskingConfig(webApiSession, groupId=groupId)
        if (dmConf.enabled != True):
            raise ValueError("Datamasking must be enabled!")

        theHeaderMaskConfig = cls.getHeaderMasks(webApiSession, groupId=groupId)
        theJSON = DataMasking.getJSON(webApiSession=webApiSession, location="{}/headerMasks".format(groupId))
        currentID = theHeaderMaskConfig.headerMaskList[theName].id
        print(currentID)
        path = '{}/headerMasks/'.format(groupId) + str(currentID)
        if(key != None):
            reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
            setattr(theHeaderMaskConfig.headerMaskList[theName], key, value)
            response = webApiSession.put( path,
                                          reqHeaders=reqHeaders,
                                          reqData=theHeaderMaskConfig.headerMaskList[theName])
        theHeaderMaskConfig = cls.getHeaderMasks(webApiSession, groupId=groupId)
        return theHeaderMaskConfig


    @classmethod
    def addHeader(cls, webApiSession, headerMask):
        '''
        :param headerMask: the Header object to be added
        :return: the new header configuration (if you are lucky)
        '''
        dmConf = cls.getDataMaskingConfig(webApiSession,groupId=headerMask.groupId)
        if (dmConf.enabled != True):
            raise ValueError("Datamasking must be enabled!")
        path = '{}/headerMasks/'.format(headerMask.groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.post(path, reqHeaders=reqHeaders, reqData=headerMask)
        theHeaderMaskConfig = cls.getHeaderMasks(webApiSession, groupId=headerMask.groupId)
        headerMask.id=theHeaderMaskConfig.headerMaskList[headerMask.name].id
        return theHeaderMaskConfig
