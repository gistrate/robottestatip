from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from http import HTTPStatus


class NP(object):
    @classmethod
    def getNPList(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'np'
        response = webApiSession.get(path, expectedStatus=expectedStatus)
        return response.json()

    @classmethod
    def getNpId(cls, webApiSession, npIndex=0):
        nps = cls.getNPList(webApiSession)
        npId = nps.get('np')[npIndex].get('id')
        return npId
