
class FileOperations(object):
    
    @staticmethod
    def getFile(webApiSession, path):

        ':type webApiSession WebApiSession'

        response = webApiSession.get(path)

        return response
