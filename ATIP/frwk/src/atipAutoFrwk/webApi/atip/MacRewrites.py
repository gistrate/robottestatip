import json
from http import HTTPStatus
from pprint import pprint
import jsonpickle
from atipAutoFrwk.config.atip.MacRewrite import MacRewrite
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.config.atip.MacRewriteConfig import MacRewriteConfig
import warnings

class MacRewrites(object):


    @classmethod
    def getJSON(cls, webApiSession, location, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        '''
        returns: a JSON
        '''
        path = location
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        theJSON = json.loads(response.text)
        return theJSON


    @classmethod
    def getMacRewriteByID(cls, webApiSession, ID, expectedStatus, groupId=0):
        ':type webApiSession: WebApiSession'
        path = '{}/macrewrite/'.format(groupId) + str(ID)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        theJSON = json.loads(response.text)
        return theJSON

    @classmethod
    def updateMacRewriteSettings(cls, webApiSession, key, expectedStatus=HTTPStatus.OK, groupId=0):
        '''
        Args:
            webApiSession (WebApiSession): the current session
            key : enable/disable the mac rewrite
        '''

        path = '{}/macrewritesettings'.format(groupId)
        theMacSettings = MacRewrites.getMacRewritesSettings(webApiSession, groupId=groupId)
        if (key == 'enable'):
            theMacSettings.enabled = True
        else:
            if (key == 'disable'):
                theMacSettings.enabled = False
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.post(path, reqHeaders=reqHeaders, reqData=theMacSettings, expectedStatus=expectedStatus)

        theMacSettings = MacRewrites.getMacRewritesSettings(webApiSession, groupId=groupId)
        return theMacSettings

    @classmethod
    def getMacRewritesSettings(cls, webApiSession, groupId=0):
        '''
        :param webApiSession:
        :return: the mac rewrite configuration (no items)
        '''
        theJSON = cls.getJSON(webApiSession=webApiSession, location="{}/macrewritesettings".format(groupId), expectedStatus=HTTPStatus.OK)

        if ("macRewriteSettings" in theJSON):
            macRewriteConfig = MacRewriteConfig()
            macRewriteConfig.groupId = groupId
            setattr(macRewriteConfig, "enabled", theJSON['macRewriteSettings']['enabled'])
            return macRewriteConfig
        return None

    @classmethod
    def getMacRewritesItems(cls, webApiSession, groupId=0):
        '''
        :param webApiSession:
        :return: the mac rewrite configuration (with items)
        '''

        theJSON = cls.getJSON(webApiSession=webApiSession, location="{}/macrewrite".format(groupId), expectedStatus=HTTPStatus.OK)
        macRewriteConfig = MacRewriteConfig()
        if ("macrewrite" in theJSON):
            if (type(theJSON["macrewrite"]) != list):
                theJSON["macrewrite"] = [theJSON["macrewrite"]]

            for i in range(len(theJSON['macrewrite'])):
                currentMacRewrite = ConversionService.createBasicObject(theJSON['macrewrite'][i], MacRewrite)
                macRewriteConfig.macRewriteList[currentMacRewrite.name] = currentMacRewrite

            return macRewriteConfig
        else:
            MacRewriteConfig()

    @classmethod
    def getMatchCountByName(cls, webApiSession, name, groupId=0):

        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path="{}/macrewrite".format(groupId), reqHeaders=reqHeaders, expectedStatus=HTTPStatus.OK)
        theJSON = json.loads(response.text)

        for mask in theJSON['macrewrite']:
            if mask['name'] == name:
                return mask['rpt_count']


    @classmethod
    def deletePayloadMasks(cls, webApiSession, groupId=0):
        '''

        :param webApiSession: the current session (that thing you need for all the operations)
        :param expectedStatus: usually HTTPStatus.OK if no catastrophe happens again
        :return: the new, empty configuration
        '''

        dmConf = cls.getDataMaskingConfig(webApiSession,groupId=groupId)
        if (dmConf.enabled != True):
            raise ValueError("Datamasking must be enabled!")

        path = '{}/regexMasks'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        thePayloadMaskConfig = cls.getPayloadMasks(webApiSession, groupId=groupId)

        for i in range(thePayloadMaskConfig.getTotalItems() - 1, -1, -1):
            cls.deleteHeaderConfigByID(webApiSession, thePayloadMaskConfig, i,groupId=groupId)
        return thePayloadMaskConfig

    @classmethod
    def getIDbyName(cls, theJSON, theName):
        '''

        :param theJSON: a JSON (oh ok)
        :param theName: the name to be searched in the JSON
        :return: the ID corresponding to the name of the JSON
        '''
        if ('macrewrite' in theJSON):
            if(type(theJSON['macrewrite']) == list):
                for i in range(len(theJSON['macrewrite'])):
                    if (str(theJSON['macrewrite'][i]['name']) == str(theName)):
                        return (i,theJSON['macrewrite'][i]['id'])
            else:
                print("{}, {}".format(theJSON['macrewrite']['name'], theName))
                if (str(theJSON['macrewrite']['name']) == str(theName)):
                    return (0, theJSON['mmacrewrite']['id'])
            return (-2, -2)
        return (-2, -2)

    @classmethod
    def addMac(cls, atipSession, field="", name="",replacement="",groupId=0,\
               regex="[\\x00-\\xFF][\\x00-\\xFF][\\x00-\\xFF][\\x00-\\xFF][\\x00-\\xFF][\\x00-\\xFF]",\
               filters="",expectedStatus = HTTPStatus.OK):
        '''

        :param atipSession: the current session
        :param field: can be Source, Destination or Both
        :param name: name
        :param replacement: the replacement of the match
        :param groupId: groupId
        :param regex: the matching regex rule
        :param filters: filters associated with the rule
        :returns: the new mac configuration
        '''
        mac = MacRewrite(regex=regex, field=field, name=name, replacement=replacement,filters=filters, groupId=groupId)

        dmConf = cls.getMacRewritesSettings(atipSession, groupId)
        if (dmConf.enabled != True):
            raise ValueError("Mac Rewrite must be enabled!")

        path = '{}/macrewrite/'.format(groupId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = atipSession.post(path, reqHeaders=reqHeaders, reqData=mac,
                                      expectedStatus=expectedStatus)
        theMacConfig = cls.getMacRewritesItems(atipSession)

        return mac

    @classmethod
    def updateMacItem(cls, webApiSession, theName, key, value, expectedStatus = HTTPStatus.OK, groupId=0):
        '''

       :param webApiSession: the thing you always need
       :param theName: self explanatory.. the name of the mac item to be updated
       :param key: the key to be updated
       :param value: the value with which the key will be updated
       :param expectedStatus: your usual HTTPStatus.OK (200)
       :return: the new, updated mac item list
       '''

        dmConf = cls.getMacRewritesSettings(webApiSession, groupId=groupId)
        if (dmConf.enabled != True):
            raise ValueError("Datamasking must be enabled!")

        theMacItems = cls.getMacRewritesItems(webApiSession, groupId=groupId)
        theJSON = cls.getJSON(webApiSession=webApiSession, location="{}/macrewrite".format(groupId), expectedStatus=HTTPStatus.OK)
        currentID = theMacItems.macRewriteList[theName].id
        path = '{}/macrewrite/'.format(groupId) + str(currentID)
        if (key != None):
            reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
            setattr(theMacItems.macRewriteList[theName], key, value)
            response = webApiSession.put(path,
                                         reqHeaders=reqHeaders,
                                         reqData=theMacItems.macRewriteList[theName],
                                         expectedStatus=expectedStatus)
        theMacItems = cls.getMacRewritesItems(webApiSession, groupId=groupId)
        return theMacItems

    @classmethod
    def deleteMacConfigByName(cls, webApiSession, theName, expectedStatus = HTTPStatus.OK, groupId=0):
        '''

        :param webApiSession: the thing you always need
        :param theName: the name of the mac item to be deleted
        :return: your usual new MacRewrite config
        '''

        dmConf = cls.getMacRewritesSettings(webApiSession, groupId=groupId)
        if (dmConf.enabled != True):
            raise ValueError("Mac Rewrite must be enabled!")

        theMacItems = cls.getMacRewritesItems(webApiSession, groupId=groupId)
        theJSON = cls.getJSON(webApiSession=webApiSession, location="{}/macrewrite".format(groupId), expectedStatus=HTTPStatus.OK)
        currentID = theMacItems.macRewriteList[theName].id
        path = '{}/macrewrite/'.format(groupId) + str(currentID)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.delete(path,
                                         reqHeaders=reqHeaders,
                                         reqData=theMacItems.macRewriteList[theName],
                                         expectedStatus=expectedStatus)
        theMacItems = cls.getMacRewritesItems(webApiSession, groupId=groupId)
        return theMacItems

    @classmethod
    def deleteAllMacItems(cls, webApiSession, groupId=0):
        '''
        Delete all configured mac rewrite items
        :param webApiSession: current session
        :return: new MacRewrite config
        '''
        dmConf = cls.getMacRewritesSettings(webApiSession, groupId=groupId)
        if (dmConf.enabled != True):
            raise ValueError("Mac Rewrite must be enabled!")

        theMacItems = cls.getMacRewritesItems(webApiSession, groupId=groupId)
        if theMacItems:
            theMacItemsDict = theMacItems.macRewriteList
        else:
            warnings.warn('No mac rewrite item to delete.')
            return

        for macItem in theMacItemsDict.keys():
            cls.deleteMacConfigByName(webApiSession, macItem)

        theMacItems = cls.getMacRewritesItems(webApiSession, groupId=groupId)
        return theMacItems

