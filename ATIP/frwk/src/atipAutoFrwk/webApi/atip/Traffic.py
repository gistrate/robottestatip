from http import HTTPStatus

from atipAutoFrwk.data.atip.TimeInterval import TimeInterval
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders


class Traffic(object):

    @classmethod
    def getTrafficStats(cls, webApiSession, timeInterval=TimeInterval.HOUR, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'
        ':type timeInterval: TimeInterval'

        path = 'portStats?inv={}'.format(timeInterval.intervalName)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response.json()

