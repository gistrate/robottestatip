import json
from http import HTTPStatus
from pprint import pprint
from atipAutoFrwk.config.atip.MatchEngineGlobalConfig import MatchEngineGlobalConfig
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.config.atip.MacRewriteConfig import MacRewriteConfig

class PacketBufferGlobalConfig(object):


    @classmethod
    def getJSON(cls, webApiSession, location, expectedStatus=HTTPStatus.OK):
        '''
        ':type webApiSession: WebApiSession'
        returns: a JSON
        '''
        path = location
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        theJSON = json.loads(response.text)
        return theJSON
#
    @classmethod
    def updatePacketBufferSettings(cls, webApiSession, bBufferPackets=None, nMaxInspectPackets=None, expectedStatus=HTTPStatus.OK):
        '''
        Args:
            webApiSession (WebApiSession): the current session
            bBufferPackets : Flase, enable/disable packet buffering
            nMaxInspectPackets: 30, Max number of packets in the packet buffer
        '''

        path = 'matchEngineGlobalConfig'
        thePBSettings = cls.getPacketBuffersSettings(webApiSession)
        if bBufferPackets :
            thePBSettings.buffer_packets = bBufferPackets
        if nMaxInspectPackets :
            thePBSettings.max_inspect_packets = nMaxInspectPackets
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.post(path, reqHeaders=reqHeaders, reqData=thePBSettings, expectedStatus=expectedStatus)

        thePBSettings = cls.getPacketBuffersSettings(webApiSession)
        return thePBSettings

    @classmethod
    def getPacketBuffersSettings(cls, webApiSession):
        '''
        :param webApiSession:
        :return: the Packet Buffer configuration (no items)
        '''
        theJSON = cls.getJSON(webApiSession=webApiSession, location="matchEngineGlobalConfig", expectedStatus=HTTPStatus.OK)

        if ("globalConfig" in theJSON):
            packetBufferConfig = MatchEngineGlobalConfig()
            setattr(packetBufferConfig, "buffer_packets", theJSON['globalConfig']['buffer_packets'])
            setattr(packetBufferConfig, "max_inspect_packets", theJSON['globalConfig']['max_inspect_packets'])
            return packetBufferConfig
        return None

