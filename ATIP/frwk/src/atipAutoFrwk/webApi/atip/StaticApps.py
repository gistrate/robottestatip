import json
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders


class StaticApps(object):

    @classmethod
    def getStaticApps(cls, webApiSession):
        '''
        :type webApiSession: WebApiSession
        :returns a JSON
        '''
        path = 'apps/list?staticOnly=true'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.get(path, reqHeaders)
        theJSON = json.loads(response.text)['apps']
        return theJSON
