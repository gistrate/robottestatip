import json
from http import HTTPStatus
from pprint import pprint
from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.services.ConversionService import ConversionService
from atipAutoFrwk.config.atip.Capture import Capture

class MacRewrites(object):


    @classmethod
    def startCapture(cls, webApiSession, filterId, expectedStatus=HTTPStatus.OK):
        '''
        Args:
            webApiSession (WebApiSession): the current session
            filterId: the filter to start PCAP capture on
        '''
        Capture.enable= True;
        path = 'capture/{}'.format(filterId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.post(path, reqHeaders=reqHeaders, expectedStatus=expectedStatus)
        return response


    @classmethod
    def stopCapture(cls, webApiSession, filterId, expectedStatus=HTTPStatus.OK):
        '''
        Args:
            webApiSession (WebApiSession): the current session
            filterId: the filter to start PCAP capture on
        '''
        Capture.enable = False;
        path = 'capture/{}'.format(filterId)
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_JSON}
        response = webApiSession.post(path, reqHeaders=reqHeaders, expectedStatus=expectedStatus)
        return response
