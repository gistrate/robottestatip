from datetime import datetime
from http import HTTPStatus
import os
from requests_toolbelt.multipart.encoder import MultipartEncoder

from atipAutoFrwk.data.webApi.RequestHeaders import RequestHeaders
from atipAutoFrwk.webApi.atip.EngineStatus import EngineStatus


class Configuration(object):


    # ATIP doesn't return 200 OK for this call even when it's successful
    @classmethod
    def clearSystem(cls, webApiSession, expectedStatus=HTTPStatus.OK):
        ':type webApiSession: WebApiSession'

        path = 'configuration/clear'
        reqHeaders = {RequestHeaders.CONTENT_TYPE: RequestHeaders.CONTENT_TYPE_URLENCODED}
        response = webApiSession.get(path, reqHeaders, expectedStatus=expectedStatus)
        return response

    @classmethod
    def getName(self, apiSession, ip='127.0.0.1'):
        thedate = str(datetime.now())
        thename = thedate.split(" ")[0]
        thename = str(EngineStatus.getVersion(apiSession)) + "_" + thename + "_" + ip + ".bin"
        return thename

    @classmethod
    def exportConfig(cls, webApiSession, expectedStatus=HTTPStatus.INTERNAL_SERVER_ERROR, host='127.0.0.1'):
        ':type webApiSession: WebApiSession'

        path = 'configuration/export'
        response = webApiSession.get(path, None, webApiSession.headers)
        newFile = open(cls.getName(webApiSession, host), "wb")
        newFileByteArray = response.content
        newFile.write(newFileByteArray)
        return response

    @classmethod
    def importConfig(cls, webApiSession, expectedStatus=HTTPStatus.INTERNAL_SERVER_ERROR, host='127.0.0.1'):
        ':type webApiSession: WebApiSession'

        path = 'configuration/import'
        file_name = cls.getName(webApiSession, host)
        multipart = MultipartEncoder(fields={'field0': (os.path.basename(file_name), open(file_name, 'rb'),
                                                        'application/octet-stream')})
        reqHeaders = {RequestHeaders.CONTENT_TYPE: multipart.content_type}
        response = webApiSession.post(path, multipart, reqHeaders, expectedStatus=expectedStatus)
        return response
