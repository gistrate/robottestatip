from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.StatsType import StatsType


class AppSimConfig(object):
    '''
    Cofiguration details of an Application Simulator Component for a BPS test.
    '''

    def __init__(self, name, appType, osType, browserType, clientLocation, serverLocation, requestPacketValues=PacketSizes(70,528,450,66), serviceProvider=None):
        ':type appType: ApplicationType'
        ':type osType: OSType'
        ':type browserType: BrowserType'

        ':type clientLocation: GeoLocation'
        ':type serverLocation: GeoLocation'

        ':type requestPacketValues: PacketSizes'
        self.name = name
        self.appType = appType
        self.osType = osType
        self.browserType = browserType
        self.clientLocation = clientLocation
        self.serverLocation = serverLocation
        self.requestPacketValues = requestPacketValues
        self.serviceProvider = serviceProvider

    def getKeyName(self, statsType):
        ':type statsType: StatsType'
        if statsType == StatsType.DEVICES:
            return self.osType.osName
        if statsType == StatsType.BROWSERS:
            return self.browserType.browserName
        if statsType == StatsType.APPS:
            return self.appType.appName
        if statsType == StatsType.APPTYPE:
            return self.appType
        if statsType == StatsType.GEO :
            return self.clientLocation.countryName