class PacketSizes(object):
    '''
    Contains a default configuration for the traffic packet sizes.
    '''

    def __init__(self, synSize=70, getSize=534, okSize=450, finAckSize=66):
        self.synSize = synSize
        self.getSize = getSize
        self.okSize = okSize
        self.finAckSize = finAckSize


