import jsonpickle

from atipAutoFrwk.config.traffic.PacketSizes import PacketSizes
from atipAutoFrwk.data.atip.StatsType import StatsType



class BpsTestConfig(object):


    def __init__(self, testName, testDuration):
        '''
        testId can be initialized to the value returned by the methods that run a test from the BpsOperations class
        '''
        self.testName = testName
        self.testDuration = int(testDuration)
        self.testId = None
        self.requestPacketValues = PacketSizes()
        self.appSimComponents = dict()

    def reinit(self, testName, testDuration):
        '''
        Used when a BpsTestConfig object needs to be re-used for another test.
        '''
        self.__init__(testName, testDuration)
        
    def addAppSim(self, appSims):
        ':type appSims: AppSimConfig'
        if isinstance(appSims, list):
            for appSim in appSims:
                self.appSimComponents[appSim.name] = appSim
        else:
            self.appSimComponents[appSims.name] = appSims


    def getAppSimsFromName(self, appName):
        '''

        :param appName: the name of the app
        :return: the first item in the list of appsims that have that app name
        '''
        item = [appSimKey for appSimKey, appSimValue in self.appSimComponents.items() if appSimValue.getKeyName(StatsType.APPS) == appName]
        return item[0]

    def getAppSimsFromAppFilter(self, filterItem):

        '''
        This function helps with the extractions of the appsim keys from the filter needed for the BpsComponentsStats
        :param filterItem: the filterItem configuration from ATIP
        :param bpsTestConfig: the test-specific BPS config
        :return: a list of all the appSim names from the filter (e.g. appsim1, appsim2 needed for the bps stats validation)
        '''
        appSimList = []
        filterConditionList = []

        print("Apps of filter {}: {}".format(filterItem.id, filterItem.conditions))
        for item in filterItem.conditions:
            if item['type'] == 'app':
                filterConditionList.extend(item['apps'])

        print(filterConditionList)
        for app in filterConditionList:
            appName = app['id']
            currentAppSim = self.getAppSimsFromName(appName)
            print("The appsimID of {} is {}".format(appName, currentAppSim))
            appSimList.append(currentAppSim)
        return appSimList

