import socket
from ssl import socket_error

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.data.atip.AtipType import AtipType


class Environment(object):

    def __init__(self):
        self.atipType = AtipType.HARDWARE
        self.atipDefaultPassword = "admin"
        self._licenseServer = '10.215.156.29' # eg.: '10.215.156.27'
        self.bpsPortList = [0,1,2,3]
        self.bpsPortGroup = '1'
        self.bpsSlot = '4'
        self.atipConnectionConfig = ConnectionConfig('admin', 'admin', '10.36.168.162', '443', 'atie/rest')
        self.bpsConnectionConfig = ConnectionConfig('admin', 'admin', '10.36.168.233', '443', 'api/v1')

    def getEnvironment(self): 
        return self
    
    @property
    def licenseServer(self, licenseServer):
        return self._licenseServer
    
    @licenseServer.getter
    def licenseServer(self):
        try: 
            socket.inet_aton(self._licenseServer)
        except socket_error:
            raise ValueError('Invalid ip set for the license manager.')
        return self._licenseServer