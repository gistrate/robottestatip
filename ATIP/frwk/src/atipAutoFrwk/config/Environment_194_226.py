import socket
from ssl import socket_error

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.data.atip.AtipType import AtipType


class Environment(object):
    def __init__(self):
        self.atipType = AtipType.VIRTUAL
        self.atipSlotNumber = [0]
        self.atipDefaultPassword = "admin"
        self._licenseServer = '10.38.194.31'
        self.buildServer = 'http://build-filesrv-0.ann.is.keysight.com/builds/'
        self.bpsPortList = [0, 1, 2, 3, 4, 5, 6, 7]
        self.bpsPortGroup = '1'
        self.bpsSlot = '1'
        self.atipConnectionConfig = ConnectionConfig('admin', 'admin1234', '10.38.194.226', '8443', 'atie/rest')
        self.bpsConnectionConfig = ConnectionConfig('adina', 'adina', '10.38.195.31', '443', 'api/v1')
        self.linuxConnectionConfig = ConnectionConfig('atip', 'atip', '10.38.195.51', 'ens224', 'telnet')
        self.netflowCollectorConnectionConfig = ConnectionConfig('atip', 'atip', '10.38.195.52', 'ens160', 'telnet')

    def getEnvironment(self):
        return self

    @property
    def licenseServer(self, licenseServer):
        return self._licenseServer

    @licenseServer.getter
    def licenseServer(self):
        try:
            socket.inet_aton(self._licenseServer)
        except socket_error:
            raise ValueError('Invalid ip set for the license manager.')
        return self._licenseServer
