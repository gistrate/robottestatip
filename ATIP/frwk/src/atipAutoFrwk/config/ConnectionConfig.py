import socket
from ssl import socket_error

from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol


class ConnectionConfig(object):

    def __init__(self, username, password, host, port, basePath, protocol=NetworkProtocol.HTTPS, verifyCertificate=False):
        self.username = username
        self.password = password
        self._host = host
        self.port = port
        self.basePath = basePath
        self.protocol = protocol
        self.verifyCertificate = verifyCertificate

    @property
    def host(self, host):
        return self._host

    @host.getter
    def host(self):
        try: 
            socket.inet_aton(self._host)
        except socket_error:
            raise ValueError('Invalid value set for "host". It must be a valid IP address.')
        return self._host