class NTOConfig(object):
    def __init__(self):
        self.trafficPorts = ['P37', 'P38', 'P39', 'P40', 'P41', 'P42', 'P43', 'P44']
        self.netflowPG = ["P48"]
        self.appForwardingPorts = ["P10", "P46"]
        self.vlans = [100, 200, 300]
