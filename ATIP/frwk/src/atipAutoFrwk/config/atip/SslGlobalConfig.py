class SslGlobalConfig(object):
    """
    Contains a default configuration for the sslGlobalConfig.
    """

    def __init__(self):
        self.passiveSSLDecryption = False
        self.groupId = 0
        
    def __getstate__(self):
        state = self.__dict__.copy()
        del state['groupId']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
