
class Collector(object):
    '''
    Contains a default configuration for a Netflow Collector.
    '''

    def __init__(self, id = None, rate = 100, unit = None, enabled = None, proto = None, port = 4739, collector = None):
        self.id = id
        self.rate = rate
        self.unit = unit
        self.enabled = enabled
        self.proto = proto
        self.port = port
        self.collector = collector

    def __str__(self):
        return 'id={},rate={},unit={},enabled={},proto={},port={},collector={}'.format(self.id, self.rate, self.unit, self.enabled, self.proto, self.port, self.collector)

