class DataMaskingConfig(object):
    '''
    Contains a default configuration for the Data Masking configuration.
    '''


    def __init__(self, enabled=False, maskChar='?', groupId=0):

        self.enabled = enabled
        self.maskChar = maskChar
        self.groupId = groupId
        
    def __getstate__(self):
        state = self.__dict__.copy()
        del state['groupId']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)