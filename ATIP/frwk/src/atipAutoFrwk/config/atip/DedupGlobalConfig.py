import jsonpickle


class DedupGlobalConfig(object):
    '''
    Contains a default configuration for Dedup global configuration.
    '''

    def __init__(self, packetWindow = 844600, enabled = False, headerToIgnore = 'NONE'):

        self.packetWindow = packetWindow
        self.enabled = enabled
        self.headerToIgnore = headerToIgnore

    def __str__(self):
        return str(jsonpickle.encode(self))