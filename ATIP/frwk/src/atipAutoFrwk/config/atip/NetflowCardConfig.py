from atipAutoFrwk.config.atip.Card import Card

class NetflowCardConfig(object):
    '''
    Contains a default configuration for the Netflow Collector configuration.
    '''
    FIRST_AVAILABLE = 0
    MAXIMUM_COLLECTORS = 6      # deprecated, please do not use this constant value
    MAXIMUM_CARDS = 7           # replacing MAXIMUM_COLLECTORS that is confusing and not correct.

    cardList = [Card(id=i) for i in range(MAXIMUM_CARDS)]
    groupId = 0

    def printConfig(self):
        for i in range(1, self.MAXIMUM_CARDS):
            print(vars(self.cardList[i]))

    def __getstate__(self):
        state = self.__dict__.copy()
        if 'groupId' in state:
            del state['groupId']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)