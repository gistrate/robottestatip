
class HeaderMask(object):
    '''
    Contains a default configuration for a Header Mask item configuration.
    '''

    def __init__(self, startOffset = None, rpt_count = 0, name = None, header = "", maskLength = None, id = -1, groupId = 0):
        self.id = id
        self.startOffset = startOffset
        self.rpt_count = rpt_count
        self.name = name
        self.header = header
        self.maskLength = maskLength
        self.groupId = groupId

    def __str__(self):
        print(vars(self))

          
    def __getstate__(self):
        state = self.__dict__.copy()
        del state['groupId']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)