
class Card(object):
    '''
    Contains a default configuration for a Netflow Collector.
    '''

    def __init__(self, id = None, gw = "", odid = "", netmask = "", ipMethod = "", dns = "", ip_addr = "",
                 enabled = "", mac = ""):
        self.id = id
        self.gw = gw
        self.odid = odid
        self.netmask = netmask
        self.ipMethod = ipMethod
        self.dns = dns
        self.ip_addr = ip_addr
        self.enabled = enabled
        self.mac = mac



