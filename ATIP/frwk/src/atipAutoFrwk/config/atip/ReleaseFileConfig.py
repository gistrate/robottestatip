class ReleaseFileConfig(object):
    '''
    Contains a default configuration for downloading latest build files.
    '''
    def __init__(self, buildServerLocation=None, fileName=None, testCase=None):
        '''
        :param buildServerLocation: root location of build files; e.g. http://build-filesrv-0.ann.is.keysight.com/builds/
        :param fileName: requested file fullname or substring of fullname which should be contained in "latest/" URL location; e.g. 'atie-signature.bin' or 'signature.bin'
        :param testCase: test case ID to use when saving files to specific test folder
        '''
        self.buildServerLocation = buildServerLocation
        self.fileName = fileName
        self.testCase = testCase





