class MatchEngineGlobalConfig(object):
    """
    Contains a default configuration for the matchEngineGlobalConfig (packet buffer).
    """

    def __init__(self):
        self.buffer_packets = False
        self.max_inspect_packets = 30
