
class PayloadMask(object):
    '''
    Contains a default configuration for a Payload configuration.
    '''

    def __init__(self, regex = None, rpt_count = 0, offsetBegin = None, name = "", offsetEnd = None, readOnly = None, id = 0, creditCard = True, groupId = 0):
        self.id = id
        self.regex = regex
        self.rpt_count = rpt_count
        self.name = name
        self.offsetBegin = offsetBegin
        self.offsetEnd = offsetEnd
        self.readOnly = readOnly
        self.creditCard = creditCard
        self.groupId = groupId

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['groupId']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)