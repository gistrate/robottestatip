from atipAutoFrwk.config.atip.Collector import Collector

class NetflowCollectorConfig(object):
    '''
    Contains a default configuration for the Netflow Collector configuration.
    '''
    FIRST_AVAILABLE = 0
    MAXIMUM_COLLECTORS = 10

    collectorList = [Collector(id=i) for i in range(MAXIMUM_COLLECTORS)]
    groupId = 0    

    def printConfig(self):
        for collector in self.collectorList:
            print(collector)


    def __getstate__(self):
        state = self.__dict__.copy()
        if 'groupId' in state:
            del state['groupId']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
