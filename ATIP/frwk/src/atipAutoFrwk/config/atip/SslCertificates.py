class SslCertificates(object):
    """
    Contains a SslCertificates
    """

    def __init__(self, name='', id='', enabled=True, groupId = 0):
        self.name = name
        self.id = id
        self.enabled = enabled
        self.groupId = groupId
        
    def __getstate__(self):
        state = self.__dict__.copy()
        del state['groupId']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)

