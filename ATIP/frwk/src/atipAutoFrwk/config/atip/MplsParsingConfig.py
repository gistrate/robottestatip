import jsonpickle


class MplsParsingConfig(object):
    '''
    Contains a default configuration for the MPLS Parsing configuration.
    '''

    def __init__(self, enabled=False, traffic_type=None, contains_pwcw=False, groupId=0):

        self.enabled = enabled
        self.traffic_type = traffic_type
        self.contains_pwcw = contains_pwcw
        self.groupId = groupId

    def __str__(self):
        return str(jsonpickle.encode(self))

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['groupId']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)