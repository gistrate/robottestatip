from atipAutoFrwk.config.atip.DataMaskingConfig import DataMaskingConfig
from atipAutoFrwk.config.atip.DedupGlobalConfig import DedupGlobalConfig
from atipAutoFrwk.config.atip.HeaderMaskConfig import HeaderMaskConfig
from atipAutoFrwk.config.atip.MplsParsingConfig import MplsParsingConfig
from atipAutoFrwk.config.atip.NetflowAccelerationConfig import NetflowAccelerationConfig
from atipAutoFrwk.config.atip.NetflowCardConfig import NetflowCardConfig
from atipAutoFrwk.config.atip.NetflowCollectorConfig import NetflowCollectorConfig
from atipAutoFrwk.config.atip.NetflowConfig import NetflowConfig
from atipAutoFrwk.config.atip.PayloadMaskConfig import PayloadMaskConfig
from atipAutoFrwk.config.atip.SystemInfo import SystemInfo
from atipAutoFrwk.config.atip.GroupConfig import GroupConfig
from atipAutoFrwk.config.atip.MacRewriteConfig import MacRewriteConfig


class AtipConfig(object):
    '''
    Contains the configuration of an ATIP server.
    '''

    def __init__(self):
        self.systemInfo = SystemInfo()
        self.filterList = []
        self.netflowAcceleration = [NetflowAccelerationConfig()]
        self.dataMasking = [DataMaskingConfig()]
        self.macRewriting = MacRewriteConfig()
        self.headerMasks = HeaderMaskConfig()
        self.payloadMasks = PayloadMaskConfig()
        self.mplsConfig = [MplsParsingConfig()]
        self.netflowGlobal = [NetflowConfig()]
        self.netflowCards = [NetflowCardConfig()]
        self.netflowCollectors = [NetflowCollectorConfig()]
        self.dedup= DedupGlobalConfig()
        self.groups = [GroupConfig(name='Default', id=0)]

    def addFilters(self, filters):
        if isinstance(filters, list):
            self.filterList.extend(filters)
        else:
            self.filterList.append(filters)
            
    def addGroup(self, groups):
        if isinstance(groups, list):
            self.groups.extend(groups)
        else:
            self.groups.append(groups)

    def addPayloadMask(self, payloadMask):
        self.payloadMasks.payloadMaskList[payloadMask.name] = payloadMask

    def addHeaderMask(self, headerMask):
        self.headerMasks.headerMaskList[headerMask.name] = headerMask

    def addMacRewrite(self,macRewrite):
        self.macRewriting.macRewriteList[macRewrite.name] = macRewrite

    def setDataMasking(self, dataMasking):
        if isinstance(dataMasking, list):
            self.dataMasking.extend(dataMasking)
        else:
            self.dataMasking.append(dataMasking)

    def setNetflowAcceleration(self, netflowAcceleration):
        if isinstance(netflowAcceleration, list):
            self.netflowAcceleration.extend(netflowAcceleration)
        else:
            self.netflowAcceleration.append(netflowAcceleration)

    def setNetflowGlobal(self, netflowGlobal):
        if isinstance(netflowGlobal, list):
            self.netflowGlobal.extend(netflowGlobal)
        else:
            self.netflowGlobal.append(netflowGlobal)

    def setMplsParsing(self, mplsConfig):
        if isinstance(mplsConfig, list):
            self.mplsConfig.extend(mplsConfig)
        else:
            self.mplsConfig.append(mplsConfig)

    def setNetflowCards(self, netflowCards):
        if isinstance(netflowCards, list):
            self.netflowCards.extend(netflowCards)
        else:
            self.netflowCards.append(netflowCards)
            
    def setNetflowCollectors(self, netflowCollectors):
        if isinstance(netflowCollectors, list):
            self.netflowCollectors.extend(netflowCollectors)
        else:
            self.netflowCollectors.append(netflowCollectors)

