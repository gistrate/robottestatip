import ipaddress

class IpRange(object):

    def __init__(self, startRange, endRange, source):
        self.startRange = startRange
        self.endRange = endRange
        self.source = source

    def toDict(self):

        ipRange = {
            'startRange' : ipaddress.IPv4Address(self.startRange),
            'endRange' : ipaddress.IPv4Address(self.endRange),
            'source' : self.source
            }
        return ipRange
