from datetime import datetime

from atipAutoFrwk.services.TestCondition import TestCondition


class FilterConfig(object):
    '''
    Contains a default configuration for a rule. Can be used as a template to configure more specific filters.
    '''

    def __init__(self, name = None, id = -1, groupId = 0):
        crtDatetime = TestCondition.getCurrentTime()

        self.id = id
        self.name = name
        self.netflow = False
        self.deny = False
        self.dropTraffic = False
        self.forward = False
        self.encrypted = False
        self.dropTraffic = False
        self.lastUpdate = crtDatetime
        self.decrypted = False
        self.createDate = crtDatetime
        self.transmitIds = []
        self.collectors = []
        self.hdrMaskUUIDS = []
        self.regexMaskUUIDS = []
        self.macrewriteUUIDS = []
        self.ingressDevice = ''
        self.conditions = []
        self.isUnmatched = False
        self.groupId = groupId

        if name is None:
            self.name = 'defaultFilter_{}'.format(crtDatetime)

    def getAppConditions(self):
        results = []
        for condition in self.conditions:
            if condition['type'] == 'app':
                for app in condition['apps']:
                    results.append(app['id'])
        return results

    def getGeoConditions(self):
        results = []
        for condition in self.conditions:
            if condition['type'] == 'geo':
                for geo in condition['geos']:
                    results.append(geo['country'])
        return results


    def addTransmitIds(self, transmitIds):
        if isinstance(transmitIds, list):
            self.transmitIds.extend(transmitIds)
        else:
            self.transmitIds.append(transmitIds)

    def addCollectors(self, collectors):
        if isinstance(collectors, list):
            self.collectors.extend(collectors)
        else:
            self.collectors.append(collectors)

    def addRegexMaskUUIDS(self, regexUuids):
        if isinstance(regexUuids, list):
            self.regexMaskUUIDS.extend(regexUuids)
        else:
            self.regexMaskUUIDS.append(regexUuids)

    def addHeaderMaskUUIDS(self, hdrUuids):
        if isinstance(hdrUuids, list):
            self.hdrMaskUUIDS.extend(hdrUuids)
        else:
            self.hdrMaskUUIDS.append(hdrUuids)

    def addMacRewriteUUIDS(self, macRwUuids):
        if isinstance(macRwUuids, list):
            self.macrewriteUUIDS.extend(macRwUuids)
        else:
            self.macrewriteUUIDS.append(macRwUuids)

