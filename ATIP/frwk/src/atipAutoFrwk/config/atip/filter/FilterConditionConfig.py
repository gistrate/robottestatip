from atipAutoFrwk.data.atip.ApplicationGroupType import AppGroupType
from atipAutoFrwk.data.atip.ApplicationType import ApplicationType
from atipAutoFrwk.data.atip.FilterConditionType import FilterConditionType
from atipAutoFrwk.data.atip.GeoLocation import GeoLocation
from atipAutoFrwk.config.atip.filter.IpRange import IpRange
from atipAutoFrwk.data.atip.NetworkProtocol import NetworkProtocol
from atipAutoFrwk.data.atip.PortRange import PortRange
from atipAutoFrwk.data.atip.IPAddressRange import IPAddressRange
from atipAutoFrwk.webApi.atip.Filters import Filters

class FilterConditionConfig(object):
    '''
    Offers support for adding conditions on a filter template.
    '''

    @classmethod
    def addCondition(cls, filterConfig, filterConditionType, conditionsList):
        ':type filterConfig: FilterConfig'
        ':type filterConditionType: FilterConditionType'
        
        found = False
        # check if there are any conditions on the filter
        for condition in filterConfig.conditions:
            if condition.get('type') == filterConditionType.conditionType:
                # found conditions of this type, append the new ones
                condition.get(filterConditionType.listName).extend(conditionsList)
                found = True

        if not found:
            # no conditions of this type found, add this category and append the new conditions
            conditionTypeList = {
                "type": filterConditionType.conditionType,
                filterConditionType.listName : conditionsList
            }
            filterConfig.conditions.append(conditionTypeList)

    @classmethod
    def resetConditions(cls, filterConfig):
        ':type filterConfig: FilterConfig'

        filterConfig.conditions= []
        return filterConfig

    @classmethod
    def addGeoCondition(cls, filterConfig, geoLocations):
        '''
        Pass a GeoLocation or a list of GeoLocation objects to add them as conditions on the given filter.
        '''
        ':type filterConfig: FilterConfig'
        
        conditionsList = []
        if isinstance(geoLocations, GeoLocation):
            conditionsList.append(GeoLocation.toDict(geoLocations))
        elif isinstance(geoLocations, list):
            for geoLocation in geoLocations: 
                if not isinstance(geoLocation, GeoLocation):
                    raise ValueError('geoLocation argument supports only GeoLocation values')
                conditionsList.append(GeoLocation.toDict(geoLocation))
        else:
            raise ValueError('geoLocation argument supports only GeoLocation values')
        
        cls.addCondition(filterConfig, FilterConditionType.GEO, conditionsList)
        
        
    @classmethod
    def addAppCondition(cls, filterConfig, applications):
        '''
        Pass a ApplicationType or a list of ApplicationType objects to add them as conditions on the given filter.
        '''
        ':type filterConfig: FilterConfig'
        
        conditionsList = []

        if isinstance(applications, ApplicationType):
            conditionsList.append(ApplicationType.toDict(applications))

        elif isinstance(applications, list):
            for application in applications: 
                if not isinstance(application, ApplicationType):
                    raise ValueError('application argument supports only ApplicationType values')
                conditionsList.append(ApplicationType.toDict(application))
        else:
            raise ValueError('application argument supports only ApplicationType values')
        
        cls.addCondition(filterConfig, FilterConditionType.APP, conditionsList)
        
        
    @classmethod
    def addProtocolCondition(cls, filterConfig, networkProtocols):
        '''
        Pass a NetworkProtocol object to add it as conditions on the given filter.
        '''
        ':type filterConfig: FilterConfig'
        ':type networkProtocol: NetworkProtocol'
        
        conditionsList = []
        if isinstance(networkProtocols, NetworkProtocol):
            conditionsList.append(NetworkProtocol.toDict(networkProtocols))
        elif isinstance(networkProtocols, list):
            for networkProtocol in networkProtocols:
                if not isinstance(networkProtocol, NetworkProtocol):
                    raise ValueError('networkProtocol argument supports only NetworkProtocol values')
                conditionsList.append(NetworkProtocol.toDict(networkProtocol))
        else:
            raise ValueError('networkProtocol argument supports only NetworkProtocol values')
        
        cls.addCondition(filterConfig, FilterConditionType.PROTOCOL, conditionsList)
        
    
    @classmethod
    def addPortRangeCondition(cls, filterConfig, portRanges):
        '''
        Pass a PortRange or a list of PortRange objects to add them as conditions on the given filter.
        '''
        ':type filterConfig: FilterConfig'
        conditionsList = []
        if isinstance(portRanges, PortRange):
            conditionsList.append(PortRange.toDict(portRanges))
        elif isinstance(portRanges, list):
            for portRange in portRanges: 
                if not isinstance(portRange, PortRange):
                    raise ValueError('portRange argument supports only PortRange values')
                conditionsList.append(portRange.toDict())
        else:
            raise ValueError('portRange argument supports only PortRange values')
        cls.addCondition(filterConfig, FilterConditionType.PORT_RANGE, conditionsList)

    @classmethod
    def addIPRangeCondition(cls, filterConfig, ipAddressRanges):
        '''
        Pass a IPAddressRange or a list of IPAddressRange objects to add them as conditions on the given filter.
        '''
        ':type filterConfig: FilterConfig'

        conditionsList = []
        if isinstance(ipAddressRanges, IPAddressRange):
            conditionsList.append(IPAddressRange.toDict(ipAddressRanges))
        elif isinstance(ipAddressRanges, list):
            for ipAddressRange in ipAddressRanges:
                if not isinstance(ipAddressRange, IPAddressRange):
                    raise ValueError('IPAddressRange argument supports only IPAddressRange values')
                conditionsList.append(ipAddressRange.toDict())
        else:
            raise ValueError('IPAddressRange argument supports only IPAddressRange values')
        cls.addCondition(filterConfig, FilterConditionType.IP_RANGE, conditionsList)

    @classmethod
    def addAppGroupCondition(cls, filterConfig, groups):
        '''
        Pass a AppGroupType or a list of AppGroupType objects to add them as conditions on the given filter.
        '''
        ':type filterConfig: FilterConfig'
        ':type group: AppGroupType'

        conditionsList = []

        if isinstance(groups, AppGroupType):
            conditionsList.append(AppGroupType.toDict(groups))

        elif isinstance(groups, list):
            for group in groups:
                if not isinstance(group, AppGroupType):
                    raise ValueError('application argument supports only AppGroupType values')
                conditionsList.append(AppGroupType.toDict(group))
        else:
            raise ValueError('application argument supports only AppGroupType values')

        cls.addCondition(filterConfig, FilterConditionType.APPGROUP, conditionsList)

    @classmethod
    def addAppActionCondition(cls, filterConfig, application, actions):
        '''
        Pass a ApplicationType or a list of ApplicationType objects to add them as conditions on the given filter.
        '''
        ':type filterConfig: FilterConfig'

        conditionsList = []

        if isinstance(application, ApplicationType):
            conditionsList.append(ApplicationType.toDict(application, actions))
        else:
            raise ValueError('application argument supports only a single ApplicationType instance')

        cls.addCondition(filterConfig, FilterConditionType.APP, conditionsList)

    @classmethod
    def getFilterIpRangeById(cls, webApiSession, ruleId):
        '''
        Extract IP range from a configured filter config designated by "ruleId"
        :param webApiSession: current session
        :param ruleId: ID of filter to get IP range from
        :return: filter IP address range
        '''
        filterIpAddressRange = []
        for filterConfig in Filters.getAllFilters(webApiSession)['rule']:
            if filterConfig['id'] == ruleId:
                for condition in filterConfig['conditions']:
                    if condition['type'] == 'ipAddressRange':
                        if not isinstance(condition['ipAddressRanges'], list):
                            condition['ipAddressRanges'] = [condition['ipAddressRanges']]
                        for ipRange in condition['ipAddressRanges']:
                            ipRange = IpRange(ipRange['startRange'], ipRange['endRange'], ipRange['source'])
                            filterIpAddressRange.append(ipRange.toDict())
        print('filterIpAddressRange: {}'.format(filterIpAddressRange))

        return filterIpAddressRange