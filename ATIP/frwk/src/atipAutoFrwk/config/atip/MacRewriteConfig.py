import jsonpickle

class MacRewriteConfig(object):
    '''
    Contains a default configuration for the Mac Rewriting configuration.
    '''

    def __init__(self, enabled=False, groupId=0):
        self.enabled = enabled
        self.groupId = groupId
        self.macRewriteList = dict()

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['groupId']
        del state['macRewriteList']
        return state

    def __str__(self):
        return str(jsonpickle.encode(self))

    def __setstate__(self, state):
        self.__dict__.update(state)

