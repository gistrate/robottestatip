
class PayloadMaskConfig(object):
    '''
    Contains a default configuration for the Payload Mask Config configuration.
    '''

    def __init__(self):
        self.payloadMaskList = dict()

    def getTotalItems(self):
        return len(self.payloadMaskList)

    def printConfig(self):
        for payload in self.payloadMaskList:
            print(payload)
