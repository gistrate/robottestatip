
class NetflowAccelerationConfig(object):
    '''
    Contains a default configuration for the netflow acceleration configuration.
    '''


    def __init__(self, enableTurboMode=False, groupId=0):
        self.enableTurboMode = enableTurboMode
        self.groupId = groupId
        
    def __getstate__(self):
        state = self.__dict__.copy()
        del state['groupId']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)