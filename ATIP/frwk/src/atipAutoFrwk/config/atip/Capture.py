import jsonpickle

class Capture(object):
    '''
    Contains a default configuration filter capture configuration.
    '''

    def __init__(self , enable= None, ruleid= None):
        self.enable = enable
        self.ruleid= ruleid

    def __str__(self):
        return str(jsonpickle.encode(self))