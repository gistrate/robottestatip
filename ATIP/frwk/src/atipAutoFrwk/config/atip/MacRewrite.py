
class MacRewrite(object):
    '''
    Contains a default configuration for the Mac Rewrite configuration.
    '''

    def __init__(self, regex ="", field="", rpt_count=None, name="", id=-1, replacement="", filters="", groupId = 0):

        self.id = id
        self.regex = regex
        self.rpt_count = rpt_count
        self.name = name
        self.field = field
        self.replacement = replacement
        self.filters = filters
        self.groupId = groupId

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['groupId']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)