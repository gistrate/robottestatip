import jsonpickle

class IngressPacketBufferConfig(object):
    """
    Contains a default configuration for the IngressPacketBuffer.
    """

    def __init__(self):
        self.softwareRxQueue = 0  # actual value of the ingress packet buffer

        # the following values are returned on GET, but should not be used in POST operations since they are internal
        self.minSoftwareRxQueue = 0
        self.maxSoftwareRxQueue = 0
        self.defaultSoftwareRxQueue = 0

    def __str__(self):
        return str(jsonpickle.encode(self))

    def __getstate__(self):
        state = self.__dict__.copy()
        # removing the following fields since they are internal only
        del state['minSoftwareRxQueue']
        del state['maxSoftwareRxQueue']
        del state['defaultSoftwareRxQueue']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
