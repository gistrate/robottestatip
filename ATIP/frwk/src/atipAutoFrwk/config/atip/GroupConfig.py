class GroupConfig(object):
    '''
    Contains a default configuration for a group
    '''

    def __init__(self, id=None, name=None, position=None, nps=None):
        self.id = id
        self.name = name
        self.position = position
        self.nps = None

    def cleandict(self, d):
        if not isinstance(d, dict):
            return d
        return dict((k, self.cleandict(v)) for k, v in d.items() if v is not None)
