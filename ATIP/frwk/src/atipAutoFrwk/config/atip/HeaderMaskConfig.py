
class HeaderMaskConfig(object):
    '''
    Contains a default configuration for the Header Mask configuration.
    '''

    def __init__(self):
        self.headerMaskList = dict()

    def getTotalItems(self):
        return len(self.headerMaskList)

    def printConfig(self):
        print(self.headerMaskList.items())


