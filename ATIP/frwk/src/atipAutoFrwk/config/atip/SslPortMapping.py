class SslPortMapping(object):
    """
    Contains a default configuration for a SSL port mapping configuration.
    """

    def __init__(self, id, enabled, portfrom, description, portto, groupId = 0):
        self.id = id
        self.enabled = enabled
        self.portfrom = portfrom
        self.description = description
        self.portto = portto
        self.groupId = groupId
        
    def __getstate__(self):
        state = self.__dict__.copy()
        del state['groupId']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
