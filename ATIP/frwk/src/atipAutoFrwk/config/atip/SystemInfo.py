class SystemInfo(object):

    def __init__(self):
        self.softwareVersion = None
        self.softwareDate = None
        self.atiDate = None
        self.atiVersion = None
        self.version = None
        self.build = None
        self.buildIdentifier = None 

    def updateBuildAndVersion(self):
        '''
        Update version, build and buildIdentifier based on the softwareVersion.
        '''
        self.version = self.softwareVersion.split(',')[0]
        self.build = self.softwareVersion.split(':')[1].strip()
        self.buildIdentifier = self.version + "-" + self.build