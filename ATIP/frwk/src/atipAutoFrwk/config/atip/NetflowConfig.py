from datetime import datetime

from atipAutoFrwk.services.TestCondition import TestCondition
import jsonpickle


class NetflowConfig(object):
    '''
    Contains a default configuration for the Netflow configuration.
    '''

    def __init__(self):

        crtDatetime = TestCondition.getCurrentTime()

        #
        self.netflowL7ImsiMapping = False
        self.netflowL7SrcLongitude = True
        self.netflowL7DNSClass = False
        self.netflowL7SrcRegionName = True
        self.netflowL7AppID = True
        self.netflowL7SrcCityName = True
        self.netflowL7DNSHostname = False
        self.timeout = 30
        self.enabled = False
        self.netflow_l7 = True
        self.netflowL7DeviceId = True
        self.netflowL7EncryptionCipher = True
        self.netflowL7SrcCountryCode = True
        self.netflowL7SrcRegionCode = True
        self.netflowL7URI = False
        self.netflowL7SrcASName = False
        self.netflowL7EncryptionKeyLen = True
        self.netflowL7DstRegionName = True
        self.netflowL7DstCityName = True
        self.netflowL7DstLatitude = True
        self.netflowL7DstCountryName = True
        self.enableVlanMaps = False
        self.netflowL7Domain = False
        self.netflowL7BrowserId = True
        self.biFlows = False
        self.netflowL7SrcLatitude = True
        self.netflowL7DstLongitude = True
        self.netflowL7DstRegionCode = True
        self.netflowL7BrowserName = True
        self.netflowL7DeviceName = True
        self.netflowL7SrcCountryName = True
        self.version = 10
        self.netflowL7DstASName = False
        self.netflowL7AppName = True
        self.netflowL7ConnEncryptType = True
        self.netflowL7Latency = False
        self.netflowL7DNSText = False
        self.netflowL7DstCountryCode = True
        self.netflowL7UserAgent = False
        self.netflowL7DNSHostnameQuery = False
        self.netflowL7DNSHostnameResponse = False
        self.groupId = 0


    def  resetIxFlowFields(self, netflowObj):
        print("attribute {}".format(netflowObj.__dict__.keys()))
        for key in netflowObj.__dict__.keys():
            if key not in ["timeout", "version", "netflow_l7", "enabled", "groupId"]:
                setattr(netflowObj, key, False)
        print("netflowObj {}".format(jsonpickle.encode(netflowObj)))
        return  netflowObj

    def  enableAllIxFlowFields(self, netflowObj):
        print("attribute {}".format(netflowObj.__dict__.keys()))
        for key in netflowObj.__dict__.keys():
            if key not in ["timeout", "version", "netflow_l7", "enabled", "groupId"]:
                setattr(netflowObj, key, True)
        print("netflowObj {}".format(jsonpickle.encode(netflowObj)))
        return  netflowObj
    
    def __getstate__(self):
        state = self.__dict__.copy()
        del state['groupId']
        return state
