class ConnTableGlobalConfig(object):
    '''
    Contains a default configuration for the global configuration.
    '''

    def __init__(self, tcpTimeout, nontcpTimeout, hashMethod="ip_port"):
        self.inactive_expire_time_sec_tcp = tcpTimeout
        self.inactive_expire_time_sec_nontcp = nontcpTimeout
        self.tuple_hash_method = hashMethod






