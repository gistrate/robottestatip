class TcpModeConfig(object):
    '''
    Contains a default configuration for the global configuration.
    '''

    def __init__(self, enabledAlways = False, forwardEmptyAcks = False, enabledBySSL=False, msg =""):
        self.enabledAlways = enabledAlways
        self.forwardEmptyAcks = forwardEmptyAcks
        self.msg = msg
        self.enabledBySSL = enabledBySSL





