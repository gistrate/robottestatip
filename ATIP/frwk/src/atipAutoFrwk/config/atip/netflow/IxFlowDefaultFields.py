
class IxFlowDefaultFields(object):
    '''
    This class should contain all enabled fields per release
    '''
    def getFields_v100(self):
        defaultFields_v100 = []
        # application specific fields
        defaultFields_v100.append('netflowL7AppID')
        defaultFields_v100.append('netflowL7AppName')
        # browser specific fields
        defaultFields_v100.append('netflowL7BrowserId')
        defaultFields_v100.append('netflowL7BrowserName')
        # source specific fields
        defaultFields_v100.append('netflowL7SrcCountryCode')
        defaultFields_v100.append('netflowL7SrcCountryName')
        defaultFields_v100.append('netflowL7SrcRegionCode')
        defaultFields_v100.append('netflowL7SrcRegionName')
        defaultFields_v100.append('netflowL7SrcCityName')
        defaultFields_v100.append('netflowL7SrcLatitude')
        defaultFields_v100.append('netflowL7SrcLongitude')
        # destination specific fields
        defaultFields_v100.append('netflowL7DstCountryCode')
        defaultFields_v100.append('netflowL7DstCountryName')
        defaultFields_v100.append('netflowL7DstRegionCode')
        defaultFields_v100.append('netflowL7DstRegionName')
        defaultFields_v100.append('netflowL7DstCityName')
        defaultFields_v100.append('netflowL7DstLatitude')
        defaultFields_v100.append('netflowL7DstLongitude')
        # device specific
        defaultFields_v100.append('netflowL7DeviceId')
        defaultFields_v100.append('netflowL7DeviceName')
        return defaultFields_v100

    def getFields_v120(self):
        defaultFields_v120 = self.getFields_v100()
        # new fields
        defaultFields_v120.append('netflowL7ConnEncryptType')
        defaultFields_v120.append('netflowL7EncryptionCipher')
        defaultFields_v120.append('netflowL7EncryptionKeyLen')
        return defaultFields_v120

    def getFields_v133(self):
        defaultFields_v133 = self.getFields_v120()
        return defaultFields_v133

    def getFields_v144(self):
        defaultFields_v144 = self.getFields_v133()
        return defaultFields_v144

    def getFields_v150(self):
        defaultFields_v150 = self.getFields_v144()
        return defaultFields_v150

    def getFields_v154(self):
        defaultFields_v154 = self.getFields_v150()
        return defaultFields_v154

