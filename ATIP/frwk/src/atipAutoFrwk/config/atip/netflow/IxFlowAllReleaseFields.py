
class IxFlowAllReleaseFields(object):
    '''
        This class should contain all existent fields per release
    '''

    def getFields_v100(self):
        allFields_v100 = []
        # application specific fields
        allFields_v100.append('netflowL7AppID')
        allFields_v100.append('netflowL7AppName')
        # browser specific fields
        allFields_v100.append('netflowL7BrowserId')
        allFields_v100.append('netflowL7BrowserName')
        # source specific fields
        allFields_v100.append('netflowL7SrcCountryCode')
        allFields_v100.append('netflowL7SrcCountryName')
        allFields_v100.append('netflowL7SrcRegionCode')
        allFields_v100.append('netflowL7SrcRegionName')
        allFields_v100.append('netflowL7SrcCityName')
        allFields_v100.append('netflowL7SrcLatitude')
        allFields_v100.append('netflowL7SrcLongitude')
        # destination specific fields
        allFields_v100.append('netflowL7DstCountryCode')
        allFields_v100.append('netflowL7DstCountryName')
        allFields_v100.append('netflowL7DstRegionCode')
        allFields_v100.append('netflowL7DstRegionName')
        allFields_v100.append('netflowL7DstCityName')
        allFields_v100.append('netflowL7DstLatitude')
        allFields_v100.append('netflowL7DstLongitude')
        # device specific
        allFields_v100.append('netflowL7DeviceId')
        allFields_v100.append('netflowL7DeviceName')
        return allFields_v100

    def getFields_v120(self):
        allFields_v120 = self.getFields_v100()
        # new fields
        allFields_v120.append('netflowL7ConnEncryptType')
        allFields_v120.append('netflowL7EncryptionCipher')
        allFields_v120.append('netflowL7EncryptionKeyLen')
        return allFields_v120

    def getFields_v133(self):
        allFields_v133 = self.getFields_v120()
        # new fields
        allFields_v133.append('netflowL7UserAgent')
        allFields_v133.append('netflowL7Domain')
        allFields_v133.append('netflowL7URI')
        allFields_v133.append('netflowL7DNSText')
        return allFields_v133

    def getFields_v144(self):
        allFields_v144 = self.getFields_v133()
        # new fields
        allFields_v144.append('netflowL7Latency')
        return allFields_v144

    def getFields_v150(self):
        allFields_v150 = self.getFields_v144()
        # new fields
        allFields_v150.append('netflowL7SrcASName')
        allFields_v150.append('netflowL7DstASName')
        return allFields_v150

    def getFields_v154(self):
        allFields_v154 = self.getFields_v150()
        # new fields
        allFields_v154.append('netflowL7DNSHostnameQuery')
        allFields_v154.append('netflowL7DNSHostnameResponse')
        allFields_v154.append('netflowL7DNSClass')
        return allFields_v154

