import socket
from ssl import socket_error

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.data.atip.AtipType import AtipType


class Environment(object):

    def __init__(self):
        self.atipType = AtipType.HARDWARE
        #self.atipHWType = 'V1'
        self.atipSlotNumber = 1
        self.atipDefaultPassword = "admin"
        self._licenseServer = '10.218.165.181' # eg.: '10.215.156.29'
        #self.bpsPortList = [0,1,2,3,4,5,6,7]
        self.bpsPortList = [0,1,2,3]
        self.bpsPortGroup = '1'
        self.bpsSlot = '11'
        self.atipConnectionConfig = ConnectionConfig('admin', 'admin', '10.38.185.226', '443', 'atie/rest')
        self.bpsConnectionConfig = ConnectionConfig('cris', 'cris', '10.38.185.144', '443', 'api/v1')
        self.linuxConnectionConfig = ConnectionConfig('atip', 'atip', '10.38.184.101', 'ens192', 'telnet')
        self.netflowCollectorConnectionConfig = ConnectionConfig('atip', 'atip', '192.168.30.100', 'ens192', 'telnet')

    def getEnvironment(self): 
        return self
    
    @property
    def licenseServer(self, licenseServer):
        return self._licenseServer
    
    @licenseServer.getter
    def licenseServer(self):
        try: 
            socket.inet_aton(self._licenseServer)
        except socket_error:
            raise ValueError('Invalid ip set for the license manager.')
        return self._licenseServer
