import socket
from ssl import socket_error

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.data.atip.AtipType import AtipType


class Environment(object):

    def __init__(self):
        self.atipType = AtipType.HARDWARE
        #self.atipHWType = 'V1'
        self.atipSlotNumber = [4, 2, 3]
        self.atipDefaultPassword = "admin"
        self._licenseServer = '10.218.165.181' # eg.: '10.215.156.29'
        self.buildServer = 'http://build-filesrv-0.ann.is.keysight.com/builds/'
        self.bpsPortList = [0,1,2,3,4,5,6,7]
        self.bpsPortGroup = '1'
        self.bpsSlot = '1'
        self.atipConnectionConfig = ConnectionConfig('admin', 'admin', '10.36.168.211', '8443', 'atie/rest')
        self.bpsConnectionConfig = ConnectionConfig('test', 'test', '10.36.169.27', '443', 'api/v1')
        self.linuxConnectionConfig = ConnectionConfig('atip', 'atip', '10.36.154.76', 'ens224', 'telnet')
        self.netflowCollectorConnectionConfig = ConnectionConfig('atip', 'atip', '192.168.30.100', 'ens192', 'telnet')
        self.ntoConnectionConfig = ConnectionConfig('admin', 'admin', '10.36.168.211', '8000', 'api')

    def getEnvironment(self): 
        return self
    
    @property
    def licenseServer(self, licenseServer):
        return self._licenseServer
    
    @licenseServer.getter
    def licenseServer(self):
        try: 
            socket.inet_aton(self._licenseServer)
        except socket_error:
            raise ValueError('Invalid ip set for the license manager.')
        return self._licenseServer