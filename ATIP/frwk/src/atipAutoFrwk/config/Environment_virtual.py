import socket
from ssl import socket_error

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.data.atip.AtipType import AtipType


class Environment(object):

    def __init__(self):
        self.atipType = AtipType.VIRTUAL
        self.atipSlotNumber = [1]
        self.atipDefaultPassword = "admin"
        self._licenseServer = '10.218.165.181' # eg.: '10.215.156.29'
        #self.bpsPortList = [0,1,2,3,4,5,6,7]
        self.bpsPortList = [0,1]
        self.bpsPortGroup = '1'
        self.bpsSlot = '6'
        self.atipConnectionConfig = ConnectionConfig('admin', 'admin1234', '10.218.165.183', '8443', 'atie/rest')
        self.bpsConnectionConfig = ConnectionConfig('syang', 'syang', '10.218.164.233', '443', 'api/v1')
        self.linuxConnectionConfig = ConnectionConfig('chewie', 'chewie', '10.218.165.142', 'eth0', 'telnet')
        self.netflowCollectorConnectionConfig = ConnectionConfig('chewie', 'chewie', '10.218.165.142', 'eth0', 'telnet')

    def getEnvironment(self): 
        return self
    
    @property
    def licenseServer(self, licenseServer):
        return self._licenseServer
    
    @licenseServer.getter
    def licenseServer(self):
        try: 
            socket.inet_aton(self._licenseServer)
        except socket_error:
            raise ValueError('Invalid ip set for the license manager.')
        return self._licenseServer
