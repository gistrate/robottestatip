import socket
from ssl import socket_error

from atipAutoFrwk.config.ConnectionConfig import ConnectionConfig
from atipAutoFrwk.data.atip.AtipType import AtipType


class Environment(object):

    def __init__(self):
        self.atipType = AtipType.HARDWARE
        #self.atipHWType = '7300'
        self.atipSlotNumber = [4]
        self.atipDefaultPassword = "admin"
        self._licenseServer = '10.218.165.181' # eg.: '10.215.156.29'
        #self.bpsPortList = [0,1,2,3,4,5,6,7]
        self.bpsPortList = [6,7]
        self.bpsPortGroup = '2'
        self.bpsSlot = '4'
        self.atipConnectionConfig = ConnectionConfig('admin', 'admin', '10.218.164.203', '443', 'atie/rest')
        self.bpsConnectionConfig = ConnectionConfig('syang', 'syang', '10.218.164.233', '443', 'api/v1')
        self.linuxConnectionConfig = ConnectionConfig('chewie', 'chewie', '10.218.165.142', 'eth2', 'telnet')
        self.netflowCollectorConnectionConfig = ConnectionConfig('chewie', 'chewie', '192.168.30.100', 'eth0', 'telnet')

    def getEnvironment(self): 
        return self
    
    @property
    def licenseServer(self, licenseServer):
        return self._licenseServer
    
    @licenseServer.getter
    def licenseServer(self):
        try: 
            socket.inet_aton(self._licenseServer)
        except socket_error:
            raise ValueError('Invalid ip set for the license manager.')
        return self._licenseServer
