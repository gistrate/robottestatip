from selenium.webdriver.common.by import By
#from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from NTOgui.frwk.Browser import Browser

class SettingsPage:

    """This library forms the base class for the ATIP settings page.

       Parent Objects: N/A

       Child Objects: N/A
    """

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'
    ROBOT_LIBRARY_DOC_FORMAT = 'reST'

    def __init__(self, b):
        self.Browser = b

    def gotoCustomApps(self):
        b = WebDriverWait(self.Browser.driver, 60).until(
            EC.element_to_be_clickable((By.ID, "customAppsBtn-btnInnerEl")))
        b.click()

    def loadCustomApp(self, appFile):
        print("Loading custom app [%s]..." % (appFile))
        self.gotoCustomApps()
        # clicking the upload new button launches a file dialog that Selenium cannot manipulate
        #b = WebDriverWait(self.Browser.driver, 60).until(
        #    EC.element_to_be_clickable((By.LINK_TEXT, "Upload New")))
        #b.click()
        
        
    def verifyCustomApp(self, appName, numSigs):
        print("Verifying custom app [%s] with [%s] signatures..." % (appName, numSigs))
        self.gotoCustomApps()
        