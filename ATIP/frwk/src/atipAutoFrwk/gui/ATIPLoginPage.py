from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

class ATIPLoginPage():

    """This library implements support for interacting with the ATIP login page.

       Parent Objects: N/A

       Child Objects: N/A
    """

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'
    ROBOT_LIBRARY_DOC_FORMAT = 'reST'

    def __init__(self, b):
        """ Description:  This is the constructor.

              Arguments: b - The Browser object running the GUI

              Return Value: N/A """

        self.Browser = b

    def setUser(self, u):
        """ Description:  Types the desired string into the user name field

              Arguments: u - the user name

              Return Value: N/A """
              
        print("Entering username [%s]..." % (u))
        
        uf = WebDriverWait(self.Browser.driver, 60).until(
            EC.element_to_be_clickable((By.ID, "username")))

        # On occassion, the field becomes clickable before it is truly ready
        # so confirm text was entered before proceeding and re-enter if necessary
        # NOTE: This redundancy is only needed on the user name field as this is the first field
        # populated after launching the page...subsequent interactions should not require this.
        for i in range(1,10):
            if type(uf) is not bool:
                uf.send_keys(u)
                val = uf.get_attribute("value")
                if val == u:
                    break
                else:
                    print("*** Exercising wait loop - making sure user name is entered correctly ***")
                    # Delete any partial text that may have been captured
                    uf.clear()
                    time.sleep(1)

    def setPassword(self, p):
        """ Description:  Types the desired string into the password field

              Arguments: p - the password

              Return Value: N/A """

        print("Entering password [%s]..." % (p))
        
        pf = WebDriverWait(self.Browser.driver, 60).until(
            EC.element_to_be_clickable((By.ID, "password")))
        for i in range(1,10):
            if type(pf) is not bool:
                pf.send_keys(p)
                val = pf.get_attribute("value")
                if val == p:
                    break
                else:
                    print("*** Exercising wait loop - making sure password is entered correctly ***")
                    # Delete any partial text that may have been captured
                    pf.clear()
                    time.sleep(1)

    def clickOK(self):
        """ Description:  Clicks on the "OK" button on the login screen

              Arguments: N/A

              Return Value: N/A """

        print("Clicking on OK button...")
        
        ok = WebDriverWait(self.Browser.driver, 60).until(
            EC.element_to_be_clickable((By.ID, "loginBtn-btnInnerEl")))
        ok.click()

    def doLogin(self, u, p):
        """ Description:  Enters the user name and password and then clicks on OK and waits
                for the diagram to render

              Arguments:
                  u - the user name
                  p - the password

              Return Value: N/A """

        self.setUser(u)
        self.setPassword(p)
        self.clickOK()
