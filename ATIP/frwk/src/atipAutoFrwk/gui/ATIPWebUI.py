from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from ATIP.frwk.src.atipAutoFrwk.config.Environment import Environment
from ATIP.frwk.src.atipAutoFrwk.gui.ATIPLoginPage import ATIPLoginPage
from ATIP.frwk.src.atipAutoFrwk.gui.Settings.SettingsPage import SettingsPage
from NTOgui.frwk.Browser import Browser

class ATIPWebUI:

    """This library forms the base class for the ATIP web GUI.

       Parent Objects: N/A

       Child Objects: N/A
    """

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LIBRARY_VERSION = '1.0'
    ROBOT_LIBRARY_DOC_FORMAT = 'reST'

    def __init__(self):
        self.Browser = None
        self.browserType = "chrome"
        self.user = "admin"
        self.password = "admin"
        self.isLoggedIn = False
        self.Settings = None
        self.env = Environment()

    def Login(self, u=None, p=None):
        # Cannot set these as defaults for the parameters since "self" is not defined at that point
        if (u==None):
            u = self.env.atipConnectionConfig.username
        if (p==None):
            p = self.env.atipConnectionConfig.password
            
        self.Browser = Browser(self.browserType)
        self.Browser.open("https://%s/atie" % (self.env.atipConnectionConfig.host))
        self.Browser.maximize()
        loginPage = ATIPLoginPage(self.Browser)
        loginPage.doLogin(u, p)
        self.isLoggedIn = True
        
        # Initialize other GUI objects
        self.Settings = SettingsPage(self.Browser)
        
    def SelectNavMenuItem(self, m):
        # This is a helper method and should not be called directly
        # m should be either "Settings" or "Logout"
        if (m == "Settings"):
            idText = "settingsBtn-btnInnerEl"
        else:
            idText = "logoutBtn-textEl"
            
        print("Looking for navburger icon...")
        m = WebDriverWait(self.Browser.driver, 60).until(
            EC.element_to_be_clickable((By.ID, "navButton-btnEl")))
        print("Moving mouse to navburger icon...")
        ActionChains(self.Browser.driver).move_to_element(m).perform()
        print("Looking for [%s] menuitem..." % (idText))
        l = WebDriverWait(self.Browser.driver, 60).until(
            EC.element_to_be_clickable((By.ID, idText)))
        print("Clicking on [%s] menuitem..." % (idText))
        l.click()

    def Logout(self):
        print("Logging out...")
        self.SelectNavMenuItem("Logout")

    def ExitBrowser(self):
        # CloseBrowser is ambiguous with a built-in Selenium method so chose ExitBrowser
        print("Closing browser...")
        self.Browser.close()
        
    def gotoSettingsPage(self):
        print("Navigating to Settings page...")
        self.SelectNavMenuItem("Settings")

    def loadCustomApp(self, appFile):
        self.Settings.loadCustomApp(appFile)
        
    def verifyCustomApp(self, appName, numSigs):
        self.Settings.verifyCustomApp(appName, numSigs)
        