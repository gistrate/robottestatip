import argparse
import ftplib
import logging
import os
import sys
import warnings
import json
import requests

from atipAutoFrwk.config.Environment import Environment
from atipAutoFrwk.config.atip.AtipConfig import AtipConfig
from atipAutoFrwk.services.atip.AtipConfigService import AtipConfigService
from atipAutoFrwk.webApi.WebApiSession import WebApiSession
from atipAutoFrwk.webApi.atip.Logout import Logout


def get_build_identifier(device_ip):

    job = "https://" + device_ip + ":8000/api/system?properties=software_version"
    try:
        ret = requests.get(job, auth=("admin", "admin"), verify=False, timeout=120)
        if ret.status_code != 200:
            print("ERROR: get_build_identifier Bad Return status: " + job + " " + str(ret.status_code))
            quitJob()
        json_data = json.loads(ret.text)
        software_version = json_data['software_version']
        return software_version
    except:
        print("ERROR: get_build_identifier: exception: " + job)
        quitJob()


def get_branch_name(device_ip):

    job = "https://" + device_ip + ":8000/api/system?properties=software_version"
    try:
        ret = requests.get(job, auth=("admin", "admin"), verify=False, timeout=120)
        if ret.status_code != 200:
            print("ERROR: get_branch_name Bad Return status: " + job + " " + str(ret.status_code))
            quitJob()
        json_data = json.loads(ret.text)
        software_version = json_data['software_version']
        branch_name = software_version.split("-")[0]

        #
        # get the number of dots. If there are 3 dots then this
        # is a RC build and the RC number needs to be stripped out
        #
        bc = branch_name.count('.')
        if bc == 3:
            rf = branch_name.rfind('.')
            branch_name = branch_name[:rf]
        return branch_name
    except:
        print("ERROR: get_branch_name: exception: " + job)
        quitJob()


def save_test_run():
    global REPORT_TYPE
    global OUTPUT_DIRECTORY
    global REMOTE_FILE_SYSTEM

    os.chdir(OUTPUT_DIRECTORY)

    ftp = ftplib.FTP(REMOTE_FILE_SYSTEM)
    ftp.login("ftpuser", "ftpuser")

    ftp.cwd("testRuns")

    try:
        ftp.mkd(REPORT_TYPE)
    except:
        pass
    ftp.cwd(REPORT_TYPE)

    try:
        ftp.mkd(BRANCH_NAME)
    except:
        pass
    ftp.cwd(BRANCH_NAME)

    try:
        ftp.mkd(TEST_RUN_TYPE)
    except:
        pass
    ftp.cwd(TEST_RUN_TYPE)

    try:
        ftp.mkd(BUILD_NUM)
    except:
        pass
    ftp.cwd(BUILD_NUM)

    ftp.storbinary('STOR ' + "output.xml", open("output.xml", 'rb'))
    ftp.storbinary('STOR ' + "log.html", open("log.html", 'rb'))
    ftp.storbinary('STOR ' + "report.html", open("report.html", 'rb'))
    ftp.storbinary('STOR ' + "passed_count.txt", open("passed_count.txt", 'rb'))
    ftp.storbinary('STOR ' + "failed_count.txt", open("failed_count.txt", 'rb'))

    ftp.quit()


def save_test_report():
    global REPORT_TYPE
    global OUTPUT_DIRECTORY
    global REMOTE_FILE_SYSTEM

    os.chdir(OUTPUT_DIRECTORY)

    ftp = ftplib.FTP(REMOTE_FILE_SYSTEM)
    ftp.login("ftpuser", "ftpuser")

    ftp.cwd("reports")

    try:
        ftp.mkd(REPORT_TYPE)
    except:
        pass
    ftp.cwd(REPORT_TYPE)

    try:
        ftp.mkd(BRANCH_NAME)
    except:
        pass
    ftp.cwd(BRANCH_NAME)

    try:
        ftp.mkd(TEST_RUN_TYPE)
    except:
        pass
    ftp.cwd(TEST_RUN_TYPE)

    ftp.storbinary('STOR ' + "output.xml", open("output.xml", 'rb'))
    ftp.storbinary('STOR ' + "log.html", open("log.html", 'rb'))
    ftp.storbinary('STOR ' + "report.html", open("report.html", 'rb'))
    ftp.storbinary('STOR ' + "passed_count.txt", open("passed_count.txt", 'rb'))
    ftp.storbinary('STOR ' + "failed_count.txt", open("failed_count.txt", 'rb'))

    ftp.quit()


#
# exit with error is manner that Jenkins realizes the job was a failure
#
def quitJob(reason=None):
    if reason:
        logging.error(reason)
    sys.exit(1)


#
# insure that all the required environment variables need to perform the
# update have been set.
#
def validateEnvironment():
    global CONFIG_DIR
    global ROBOT_CONFIG
    global TEST_TYPE
    global REPORT_TYPE
    global TEST_RUN_TYPE
    global WORKSPACE
    global DEVICE_IP
    global DEVICE
    global REMOTE_FILE_SYSTEM
    global BUILD_ID
    global BUILD_URL
    global JOB_URL
    global OTHER_PYBOT_PARAMS

    validEnviron = True

    if os.getenv("CONFIG_DIR") is None:
        print("ERROR: Environment variable CONFIG_DIR not set")
        validEnviron = False
    else:
        CONFIG_DIR = os.getenv("CONFIG_DIR")

    if os.getenv("ROBOT_CONFIG") is None:
        print("ERROR: Environment variable ROBOT_CONFIG not set")
        validEnviron = False
    else:
        ROBOT_CONFIG = os.getenv("ROBOT_CONFIG")

    if os.getenv("WORKSPACE") is None:
        print("ERROR: Environment variable WORKSPACE not set")
        validEnviron = False
    else:
        WORKSPACE = os.getenv("WORKSPACE")

    if os.getenv("DEVICE_IP") is None:
        print("ERROR: Environment variable DEVICE_IP not set")
        validEnviron = False
    else:
        DEVICE_IP = os.getenv("DEVICE_IP")

    if os.getenv("DEVICE") is None:
        print("ERROR: Environment variable DEVICE not set")
        validEnviron = False
    else:
        DEVICE = os.getenv("DEVICE")

    if os.getenv("TEST_TYPE") is None:
        print("ERROR: Environment variable TEST_TYPE not set")
        validEnviron = False
    else:
        TEST_TYPE = os.getenv("TEST_TYPE")

    if os.getenv("REPORT_TYPE") is None:
        print("ERROR: Environment variable REPORT_TYPE not set")
        validEnviron = False
    else:
        REPORT_TYPE = os.getenv("REPORT_TYPE")

    if os.getenv("TEST_RUN_TYPE") is None:
        print("ERROR: Environment variable TEST_RUN_TYPE not set")
        validEnviron = False
    else:
        TEST_RUN_TYPE = os.getenv("TEST_RUN_TYPE")

    if os.getenv("REMOTE_FILE_SYSTEM") is None:
        print("ERROR: Environment variable REMOTE_FILE_SYSTEM not set")
        validEnviron = False
    else:
        REMOTE_FILE_SYSTEM = os.getenv("REMOTE_FILE_SYSTEM")

    if os.getenv("BUILD_ID") is None:
        print("ERROR: Environment variable BUILD_ID not set")
        validEnviron = False
    else:
        BUILD_ID = os.getenv("BUILD_ID")

    if os.getenv("BUILD_URL") is None:
        print("ERROR: Environment variable BUILD_URL not set")
        validEnviron = False
    else:
        BUILD_URL = os.getenv("BUILD_URL")

    # This is an optional value
    if os.getenv("OTHER_PYBOT_PARAMS") is None:
        print("INFO: Environment variable OTHER_PYBOT_PARAMS not set")
        OTHER_PYBOT_PARAMS = ""
    else:
        OTHER_PYBOT_PARAMS = os.getenv("OTHER_PYBOT_PARAMS")

    if os.getenv("JOB_URL") is None:
        print("ERROR: Environment variable JOB_URL not set")
        validEnviron = False
    else:
        JOB_URL = os.getenv("JOB_URL")

    if not validEnviron:
        print("ERROR: Environment variables not set. Terminating Install")
        quitJob("Environment variables not set")


def generateEmail():
    subjectText = []

    SUBJECT = DEVICE + " Completed Robot ATIP Test Automation Run  - " + BUILD_IDENTIFIER

    subjectText.append(SUBJECT + "<BR>")
    subjectText.append("<BR>")
    subjectText.append("PASSED: " + passedCount + "  FAILED: " + failedCount + "<BR>")
    subjectText.append("<BR>")
    subjectText.append(DEVICE + " Robot ATIP Test Automation Run: " + BUILD_URL + "<BR>")
    subjectText.append(DEVICE + " Robot System Test Automation Run Results: " + JOB_URL + "ws/output/report.html<BR>")
    subjectText.append(DEVICE + " Robot System Test Job Summary: " + JOB_URL + "<BR>")

    file = os.path.join(MAIL_DIRECTORY, "header.txt")
    f = open(file, 'w')
    f.write(SUBJECT + '\n')
    f.close()

    file = os.path.join(MAIL_DIRECTORY, "body.txt")
    f = open(file, 'w')
    for line in subjectText:
        f.write(line + '\n')
    f.close()


def setDescription():
    global failedCount
    global passedCount

    file = os.path.join(OUTPUT_DIRECTORY, "output.xml")
    try:
        f = open(file, 'r')
    except IOError as e:
        print("Could not open file", file)
        quitJob(e)
    for line in f:
        line = line.rstrip()
        if line.strip():
            if "All Tests" in line:
                words = line.split()
                for word in words:
                    if "fail" in word:
                        failedCount = word.split("\"")[1]
                    if "pass" in word:
                        passedCount = word.split("\"")[1]
    f.close()

    file = os.path.join(OUTPUT_DIRECTORY, "failed_count.txt")
    f = open(file, 'w')
    f.write(failedCount + '\n')
    f.close()

    file = os.path.join(OUTPUT_DIRECTORY, "passed_count.txt")
    f = open(file, 'w')
    f.write(passedCount + '\n')
    f.close()

    print("[DESCRIPTION] PASSED: " + passedCount + "  FAILED: " + failedCount)


#
# read schedule config file to get list of jobs and there associated resource pool and options
#
def readConfigFile():
    file = os.path.join(CONFIG_DIR, ROBOT_CONFIG)

    try:
        f = open(file, 'r')
    except IOError:
        print("Could not open file", file)
        quitJob()
    for line in f:
        line = line.rstrip()
        if line.strip():
            if not line.startswith('#'):
                x = (line.split(';'))
                jobs.append(x)
    f.close()

#
# Get the output dir based on the robot args
#
def getCrtOutputDir(line):
    outDir = os.path.join("$WORKSPACE", TEST_TYPE, "output")
    if len(line) > 2:
        robotVars = getRobotValiables(line[2])
        if robotVars.get(OUTPUT_DIR_VAR_NAME):
            outDir = os.path.join(outDir, robotVars.get(OUTPUT_DIR_VAR_NAME))
    outDir = os.path.join(outDir, line[0], line[1])
    return outDir


#
# Get a list of all output dirs relative to $WORKSPACE sorted in reverse order
#
def getCustomSuitesOutputDirs(jobs):
    suitesOutDirs = set()
    for line in jobs:
        if len(line) > 2:
            robotVars = getRobotValiables(line[2])
            if robotVars.get(OUTPUT_DIR_VAR_NAME):
                outDir = os.path.join(robotVars.get(OUTPUT_DIR_VAR_NAME))
                suitesOutDirs.add(outDir)
    return sorted(suitesOutDirs, reverse=True)


#
# Get the robot variables as a dictionary
#
def getRobotValiables(robotArgs):
    argParser = argparse.ArgumentParser()
    argParser.add_argument("--variable", action='append', type=lambda kv: kv.split(":"), dest='keyvalues')
    args, _ = argParser.parse_known_args(robotArgs.split())
    if not args.keyvalues:
        return dict()
    return dict(args.keyvalues)


########################
#####     MAIN    ######
########################

jobs = []

validateEnvironment()

# Initialize ATIP and get the system info
env = Environment()
atipSession = WebApiSession(env.atipConnectionConfig)
atipConfig = AtipConfig()
try:
    atipConfig = AtipConfigService.initAtip(atipSession, env, atipConfig)
    Logout.logout(atipSession)
except Exception as e:
    warnings.warn("Could not initialize ATIP: {}".format(e))

BRANCH_NAME = atipConfig.systemInfo.version
BUILD_NUM = atipConfig.systemInfo.build
BUILD_IDENTIFIER = atipConfig.systemInfo.buildIdentifier

# the name of the robot variable that is used to set the output dir
OUTPUT_DIR_VAR_NAME = "testType"

NTO_BRANCH_NAME = ""
NTO_BUILD_IDENTIFIER = ""
if REPORT_TYPE == "ATIP":
    NTO_BRANCH_NAME = get_branch_name(DEVICE_IP)
    NTO_BUILD_IDENTIFIER = get_build_identifier(DEVICE_IP)

print("")
print("####################################")
if REPORT_TYPE == "ATIP":
    print("INFO NTO BUILD IDENTIFIER:", NTO_BUILD_IDENTIFIER)
    print("INFO NTO BRANCH NAME:", NTO_BRANCH_NAME)
print("INFO ATIP BUILD IDENTIFIER:", BUILD_IDENTIFIER)
print("INFO ATIP BRANCH NAME:", BRANCH_NAME)
print("INFO DEVICE IP ADDRESS:", DEVICE_IP)
print("INFO DEVICE MODEL:", DEVICE)
print("INFO TEST TYPE:", TEST_TYPE)
print("INFO CONFIG FILE:", ROBOT_CONFIG)
print("####################################")
print("")

readConfigFile()

for line in jobs:
    parms = ""
    outputDir = getCrtOutputDir(line)
    if len(line) > 2:
        parms = line[2]
    cmd = "pybot --exclude incomplete --exclude skip --exclude obsolete " + parms + " " + OTHER_PYBOT_PARAMS + " --outputdir " + outputDir + " $WORKSPACE/" + TEST_TYPE + "/scripts/" + \
          line[0] + "/" + line[1] + ".robot"
    print(cmd)
    os.system(cmd)

OUTPUT_DIRECTORY = os.path.join(WORKSPACE, "output")
MAIL_DIRECTORY = os.path.join(WORKSPACE, "mail")

os.mkdir(OUTPUT_DIRECTORY)
os.mkdir(MAIL_DIRECTORY)

os.chdir(WORKSPACE)
# Create partial outputs for custom dirs
allOutDirs = getCustomSuitesOutputDirs(jobs)

for customOutDir in allOutDirs:
    outDir = os.path.join("$WORKSPACE", TEST_TYPE, "output", customOutDir)

    cmd = "rebot --output output.xml -d " + outDir + " -N \"" + customOutDir + "\" " + outDir + "/*/*/*/*/output.xml"
    print(cmd)
    os.system(cmd)

# Create the general output dir
cmd = "rebot --output output.xml -d " + TEST_TYPE + "/output/dir1/dir2/dir3/performance -N \"performance\" " + WORKSPACE + "/" + TEST_TYPE + "/output/functional/NP_Group/performance/*/*/output.xml"
os.system(cmd)
print(cmd)
cmd = "rebot --output output.xml -d " + TEST_TYPE + "/output/dir1/dir2/dir3 -N \"NP_Group | Sigs\" " + WORKSPACE + "/" + TEST_TYPE + "/output/*/*/*/*/output.xml"
os.system(cmd)
print(cmd)
cmd = "rebot --output output.xml -d " + TEST_TYPE + "/output/dir1/ -N \"Legacy | NP_Groups | Sigs\" " + WORKSPACE + "/" + TEST_TYPE + "/output/*/*/*/output.xml"
os.system(cmd)
print(cmd)

testReportName = DEVICE + " " + BUILD_IDENTIFIER
if REPORT_TYPE == "ATIP":
    testReportName = NTO_BUILD_IDENTIFIER + " " + BUILD_IDENTIFIER
cmd = "rebot --output output.xml -d output -N \"" + testReportName + "\" " + WORKSPACE + "/" + TEST_TYPE + "/output/*/output.xml"
os.system(cmd)
print(cmd)

setDescription()

save_test_run()

save_test_report()

generateEmail()