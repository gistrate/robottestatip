#
# continuous test run configuration file
#

#
# SVT test run option
#
#!SVT Gibson_25G_AFMR@SVTTestRun.config

Gibson_25G_AFMR@ParallelTestRun.config/Gibson_50G_AFMR@ParallelTestRun.config
Gibson_25G@SerialTestRun.config
Gibson_50G@SerialTestRun.config
Gibson_25G@ParallelTestRun.config/Gibson_50G@ParallelTestRun.config
Gibson_50G_AFMR@SerialTestRun.config
Gibson_25G_AFMR@SerialTestRun.config


#Gibson_25G@SpecialTestRun.config
#Gibson_25G_AFMR@ParallelTestRun.config
#Gibson_25G_AFMR@SerialTestRun.config
