#!/bin/bash

test -z "${WORKSPACE}"                && { echo "$0: Please set WORKSPACE"; exit 1; }
test -z "${ATIE_FABRIC_IP_PARAMETER}" && { echo "$0: Please set ATIE_FABRIC_IP_PARAMETER"; exit 1; }

rm atip-upgrade.bin 2>/dev/null 1>&2


FTP_FILE=/tmp/ftpFile
rm -f $FTP_FILE

echo bin > $FTP_FILE
echo prompt off >> $FTP_FILE

echo lcd $WORKSPACE >> $FTP_FILE
#
# get distribution files needed for upgrade
#
echo cd builds/atip/${ATIE_BRANCH_NAME}/latest >> $FTP_FILE
echo get P4_REVISION.txt >> $FTP_FILE
echo get atip-upgrade.bin >> $FTP_FILE
echo get setup_db.sql >> $FTP_FILE

ftp $REMOTE_FILE_SYSTEM < $FTP_FILE

rm -f $FTP_FILE

chmod 400 anue_id_rsa.ppk

echo
echo "INFO: copy atip-upgrade.bin to device: $ATIE_FABRIC_IP_PARAMETER"
echo

echo scp -i anue_id_rsa.ppk $WORKSPACE/atip-upgrade.bin root@$ATIE_FABRIC_IP_PARAMETER:/var/ipc
scp -i anue_id_rsa.ppk $WORKSPACE/atip-upgrade.bin root@$ATIE_FABRIC_IP_PARAMETER:/var/ipc
if [ $? -ne 0 ]; then
    echo "ERROR: Failure to copy atip-upgrade.bin to device: $ATIE_FABRIC_IP_PARAMETER"
    exit 1
fi

echo scp -i anue_id_rsa.ppk $WORKSPACE/setup_db.sql root@$ATIE_FABRIC_IP_PARAMETER:/opt/atie/control
scp -i anue_id_rsa.ppk $WORKSPACE/setup_db.sql root@$ATIE_FABRIC_IP_PARAMETER:/opt/atie/control
if [ $? -ne 0 ]; then
    echo "ERROR: Failure to copy setup_db.sql to device: $ATIE_FABRIC_IP_PARAMETER"
    exit 1
fi

echo
echo "INFO: use ssh to run script to update ATIE on device: $ATIE_FABRIC_IP_PARAMETER"
echo
echo ssh -i anue_id_rsa.ppk root@$ATIE_FABRIC_IP_PARAMETER bash -c -l "/opt/atie/scripts/atie_unpack.sh /var/ipc/atip-upgrade.bin"
ssh -i anue_id_rsa.ppk root@$ATIE_FABRIC_IP_PARAMETER bash -c -l \"/opt/atie/scripts/atie_unpack.sh /var/ipc/atip-upgrade.bin\"
if [ $? -ne 0 ]; then
    echo "ERROR: Failure to run script to update atip-upgrade.bin on device: $ATIE_FABRIC_IP_PARAMETER"
    exit 1
fi

#
# sleep to allow device to restart properly
#
echo INFO: Device updated wait 10 minutes for device to restart.
date
sleep 600
date

echo scp -i anue_id_rsa.ppk root@$ATIE_FABRIC_IP_PARAMETER:/var/volatile/log/atie-update.log $WORKSPACE/
scp -i anue_id_rsa.ppk root@$ATIE_FABRIC_IP_PARAMETER:/var/volatile/log/atie-update.log $WORKSPACE/

rm $WORKSPACE/messages 2>/dev/null 1>&2
echo scp -i anue_id_rsa.ppk root@$ATIE_FABRIC_IP_PARAMETER:/var/volatile/log/messages $WORKSPACE/messages
scp -i anue_id_rsa.ppk root@$ATIE_FABRIC_IP_PARAMETER:/var/volatile/log/messages $WORKSPACE/messages

echo ssh -i anue_id_rsa.ppk root@$ATIE_FABRIC_IP_PARAMETER ls -l /mnt/safe
ssh -i anue_id_rsa.ppk root@$ATIE_FABRIC_IP_PARAMETER ls -l /mnt/safe > $WORKSPACE/safe_listing

grep -q "A fatal error has been detected by the Java Runtime Environment" $WORKSPACE/messages
if [ $? -eq 0 ]; then
    echo ""
    echo "ERROR: A fatal error has been detected by the Java Runtime Environment"
    echo ""
    exit 1
else
    echo ""
    echo "INFO: No errors detected by the Java Runtime Environment"
    echo ""
fi

grep -q "core" $WORKSPACE/safe_listing
if [ $? -eq 0 ]; then
    echo ""
    echo "ERROR: Core file exists in /mnt/safe"
    echo ""
    exit 1
else
    echo ""
    echo "INFO: No Core files in /mnt/safe"
    echo ""
fi

echo INFO: Device update completed


