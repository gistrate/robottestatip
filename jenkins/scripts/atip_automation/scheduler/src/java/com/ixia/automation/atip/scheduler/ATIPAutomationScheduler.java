package com.ixia.automation.atip.scheduler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ATIPAutomationScheduler {

	private static String CONFIG_FILE = "automation_configuration.xml";
	private static String MAILING_DIR = "MailingLists";
	
	public class BranchInfo {
		String branch;
	}
	
	public class ConfigFileSetup {
		String branchName;
		String suiteType;
		String upgradeATIP;
		String outputFile;
		String atipMailingList;
	}
	
	String dotw = "";
	
	String envRunAutomation = "";
	String envRCBuild = "";
	
	private ArrayList<BranchInfo> releaseInfo = new ArrayList<BranchInfo>();
	
	private void setup() {
		Date date = new Date();
	    DateFormat df = new SimpleDateFormat("u");
	    dotw = df.format(date);
	    
		envRunAutomation = System.getenv("RUNAUTOMATION");
		envRCBuild = System.getenv("RC_BUILD");
	}
	
	private boolean checkBuildCause(String buildCause) {
		boolean retValue = false;
		
		if (buildCause.equals("any"))
			return true;
		
		if (buildCause.equals("RUNAUTOMATION")) {
			if ((envRunAutomation != null) && (envRunAutomation.equals("true"))) {
				retValue = true;
			}
		}
		
		if (buildCause.equals("RC")) {
			if ((envRCBuild != null) && (envRCBuild.equals("true"))) {
				retValue = true;
			}
		}

		return retValue;
	}
	
	private boolean isBranchEnabled(String branch) {
		boolean status = false;

		for (int i = 0; i < releaseInfo.size(); i++) {
			BranchInfo branchInfo = releaseInfo.get(i);
			if (branchInfo.branch.equals(branch)) {
				status = true;
				break;
			}
		}
		return status;
	}
	
	private void ReadBranches(File xmlFile) {
				
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("BRANCHES");
			
			for (int i = 0; i < nList.getLength(); i++) {

				Node nNode = nList.item(i);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					
					NodeList fList = eElement.getElementsByTagName("BRANCH");
					for (int j = 0; j < fList.getLength(); j++) {
						Node fNode = fList.item(j);
						if (fNode.getNodeType() == Node.ELEMENT_NODE) {

							Element fElement = (Element) fNode;
	
							String branchName = fElement.getAttribute("BRANCH_NAME");
							String buildCause = fElement.getAttribute("BUILD_CAUSE");	
							String branchStatus = fElement.getAttribute("STATUS");

							if (checkBuildCause(buildCause)) {
								if (branchStatus.equals("enabled")) {
									BranchInfo branchInfo = new BranchInfo();
									branchInfo.branch = branchName;
									releaseInfo.add(branchInfo);
								}
							}									
						}
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	private String getAutomationMailingList(String mailingListFile, String configDir) {
		String mailingList = "";
        String line = "";
        String mailFile = configDir + File.separator + MAILING_DIR + File.separator + mailingListFile;
        
        try {
            BufferedReader br = new BufferedReader(new FileReader(mailFile));
            while ((line = br.readLine()) != null) {
 
                if (line.length() == 0)
                	continue;
                if (line.startsWith("#")) 
                    continue;
                
                mailingList = mailingList + line + ",";
            }

            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(mailingList);
		return mailingList;
	}
	
	private void writeConfigFile(ConfigFileSetup cfs, String configDir, String outputFile) {
	       try {
	            FileWriter fw = new FileWriter(outputFile, false);
	            fw.write("ATIE_BRANCH_NAME=" + cfs.branchName +"\n");
	            fw.write("SUITE_TYPE=" + cfs.suiteType + "\n");
	            
	            if (cfs.upgradeATIP.equals("true")) {
	            	fw.write("UPGRADE_RUN_FLAG=true\n");
	            }
	            else {
	            	fw.write("UPGRADE_RUN_FLAG=false\n");	
	            }
	            
	            if (cfs.atipMailingList.equals("")) {
	            	cfs.atipMailingList = "default";
	            }

            	String mailingList = getAutomationMailingList(cfs.atipMailingList, configDir);
	            fw.write("ATIP_MAILING_LIST=" + mailingList + "\n");
	            
	            fw.close();
	       } catch (Exception e) {
	    	   e.printStackTrace();
	       }    
	}
	
	private boolean ReadAutomationConfig(File xmlFile, String configDir, String device, String branchName, String outputFile) {
					
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("AUTOMATION_CONFIG");
			
			for (int i = 0; i < nList.getLength(); i++) {

				Node nNode = nList.item(i);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					
					NodeList fList = eElement.getElementsByTagName("DEVICE");
					for (int j = 0; j < fList.getLength(); j++) {
						Node fNode = fList.item(j);
						if (fNode.getNodeType() == Node.ELEMENT_NODE) {

							Element fElement = (Element) fNode;
	
							String xmlIP = fElement.getAttribute("IP");
							if (!xmlIP.equals(device))
								continue;
							
							String xmlBranchName = fElement.getAttribute("BRANCH_NAME");
							if (!xmlBranchName.equals(branchName))
								continue;
							
							String xmlStatus = fElement.getAttribute("STATUS");
							if (!xmlStatus.equals("enabled"))
								continue;
							
							String xmlSchedule = fElement.getAttribute("SCHEDULE");
							if (!xmlSchedule.contains(dotw))
								continue;
							
							if (!isBranchEnabled(xmlBranchName)) {
								continue;
							}
							
							String xmlSuiteType = fElement.getAttribute("SUITE_TYPE");

							ConfigFileSetup cfs = new ConfigFileSetup();
							
							cfs.branchName = xmlBranchName;
							cfs.suiteType = xmlSuiteType; 
							cfs.upgradeATIP = fElement.getAttribute("UPGRADE_ATIP"); 
							cfs.atipMailingList = fElement.getAttribute("MAILING_LIST"); 

							writeConfigFile(cfs, configDir, outputFile);
							
							System.out.println("");
							System.out.println("Starting ATIP Automation Test Run");
							System.out.println("DEVICE: " + device);
							System.out.println("BRANCH: " + branchName);
							System.out.println("SUITE TYPE: " + cfs.suiteType);
							System.out.println("");

							return true;
						}
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("");
		System.out.println("NOT Starting ATIP Automation Test Run");
		System.out.println("DEVICE: " + device);
		System.out.println("BRANCH: " + branchName);
		System.out.println("");

		return false;
	}
	

	public ATIPAutomationScheduler(String configDir, String device, String branchName, String outputFile) {
		File f = new File(configDir + File.separator + CONFIG_FILE);
		if (!f.exists()) {
			System.out.println("configuration file: " + f.getName() + " does not exist");
			System.exit(1);
		}
		
		setup();
		
		ReadBranches(f);

		if (!ReadAutomationConfig(f, configDir, device, branchName, outputFile)) {
			System.exit(1);
		}
		System.exit(0);
	}
	
    public static void main(String[] argv) {
		int i = 0;
		String parameter;
		String value;
		
		String configDir = null;
		String branchName = null;
		String device = null;
		String outputFile = null;
		
		// Ixia_Web_Automation/Ixia_Web_Automation.rxtst

		while (i < argv.length) {
			parameter = argv[i++];
            value = argv[i++];
            
            if (parameter.equals("--configDir")) {
            	configDir = value;
            	continue;
            }
            
            if (parameter.equals("--branchName")) {
            	branchName = value;
            	continue;
            }
                
            if (parameter.equals("--device")) {
            	device = value;
            	continue;
            }
            
            if (parameter.equals("--outputFile")) {
            	outputFile = value;
            	continue;
            }
            
            System.out.println("ERROR: Invalid parameter specified: " + parameter);
            System.exit(1);
		}
		
		if (configDir == null) {
			System.out.println("ERROR: configDir not specified");
			System.exit(1);
		}
		
		if (branchName == null) {
			System.out.println("ERROR: branchName not specified");
			System.exit(1);
		}
		
		if (device == null) {
			System.out.println("ERROR: device not specified");
			System.exit(1);
		}
		
		if (outputFile == null) {
			System.out.println("ERROR: outputFile not specified");
			System.exit(1);
		}
	
        new ATIPAutomationScheduler(configDir, device, branchName, outputFile);
    }
}
