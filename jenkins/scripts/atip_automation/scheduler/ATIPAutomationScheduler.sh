#!/bin/bash

test -z "${WORKSPACE}"    && { echo "ERROR: WORKSPACE not set"; exit 1; }
test -z "${BRANCH_NAME}"  && { echo "ERROR: BRANCH_NAME not set"; exit 1; }
test -z "${DEVICE}"       && { echo "ERROR: DEVICE not set"; exit 1; }
test -z "${CONFIG_DIR}"   && { echo "ERROR: CONFIG_DIR not set"; exit 1; }
test -z "${OUTPUT_FILE}"  && { echo "ERROR: OUTPUT_FILE not set"; exit 1; }

JENKINS_JAR=$WORKSPACE/jenkins/scripts/atip_automation/scheduler/build/jar/AutomationScheduler.jar

echo java -cp $JENKINS_JAR com.ixia.automation.atip.scheduler.ATIPAutomationScheduler --configDir ${CONFIG_DIR} --device ${DEVICE} --branchName ${BRANCH_NAME} --outputFile ${OUTPUT_FILE}
java -cp $JENKINS_JAR com.ixia.automation.atip.scheduler.ATIPAutomationScheduler --configDir ${CONFIG_DIR} --device ${DEVICE} --branchName ${BRANCH_NAME} --outputFile ${OUTPUT_FILE}
