import os
import sys
import time
import requests
import json
import urllib3
import platform
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


#
# exit with error is manner that Jenkins realizes the job was a failure
#
def quit_job(reason):
    global DEVICE_TYPE
    global DEVICE

    print("")
    print(reason)
    print("")
    print("[DESCRIPTION] Installation Failure")

    subject = "NTO: " + DEVICE_TYPE + " " + DEVICE + " Installation Failure"
    body = "Reason: " + reason + "<BR><BR>"
    body = body + "Device: http://" + DEVICE + "<BR><BR>"
    body = body + "Jenkins Job: " + os.getenv("BUILD_URL") + "<BR>"

    build_master = "nto-automation@keysight.com"
    mail_list = os.getenv("EMAIL_AUTOMATION_SUMMARY")
    mail_list = mail_list.split(",")

    mail_server = "smtp.cos.is.keysight.com"

    msg = MIMEMultipart()

    msg['Subject'] = subject
    msg['From'] = build_master
    msg['To'] = ", ".join(mail_list)
    msg.attach(MIMEText(body, 'html'))

    s = smtplib.SMTP(mail_server)
    s.sendmail(build_master, mail_list, msg.as_string())
    s.quit()

    sys.exit(1)


#
# insure that all the required environment variables need to perform the
# update have been set.
#
def validate_environment():
    valid_environ = True

    if os.getenv("WORKSPACE") is None:
        print("ERROR: Environment variable WORKSPACE not set")
        valid_environ = False

    if os.getenv("DEVICE") is None:
        print("ERROR: Environment variable DEVICE not set")
        valid_environ = False

    if os.getenv("DEVICE_TYPE") is None:
        print("ERROR: Environment variable DEVICE_TYPE not set")
        valid_environ = False

    if os.getenv("BUILD_TYPE") is None:
        print("ERROR: Environment variable BUILD_TYPE not set")
        valid_environ = False

    if os.getenv("BRANCH_NAME") is None:
        print("ERROR: Environment variable BRANCH_NAME not set")
        valid_environ = False

    if os.getenv("SLEEP_TIME") is None:
        print("ERROR: Environment variable SLEEP_TIME not set")
        valid_environ = False

    if os.getenv("REMOTE_FILE_SYSTEM") is None:
        print("ERROR: Environment variable REMOTE_FILE_SYSTEM not set")
        valid_environ = False

    return valid_environ


def get_line_card_info():
    global DEVICE
    global PORT_WEBAPI
    global USER
    global PASSWD

    job = "https://" + DEVICE + ":" + PORT_WEBAPI + "/api/line_boards"
    try:
        ret = requests.get(job, auth=(USER, PASSWD), verify=False, timeout=120)
        if ret.status_code != 200:
            print(job, "return status", ret.status_code)

        json_data = json.loads(ret.text)
        line_cards = []
        for item in json_data:
            lbs = item['line_board_status']
            name = item['name']
            line_cards.append(name + " has status of " + lbs)

            line_cards.sort()
        for item in line_cards:
            print(item)

    except:
        print("ERROR: getLineCardInfo Could not retrieve line card information")
        quit_job("Could not retrieve line card information")


#
# NTO has been updated get the software version to insure that the running version
# is the requested update. if the request times out or the version does not match
# the expected version then exit with failure
#
def get_current_version_info(expected_version):
    global DEVICE
    global PORT_WEBAPI
    global USER
    global PASSWD

    job = "https://" + DEVICE + ":" + PORT_WEBAPI + "/api/system?properties=software_version"
    try:
        ret = requests.get(job, auth=(USER, PASSWD), verify=False, timeout=120)
        if ret.status_code != 200:
            print(job, "return status", ret.status_code)
            quit_job("Device not running expected version")

        json_data = json.loads(ret.text)
        current_software_version = json_data['software_version']
        print("")
        print("NTO " + DEVICE + " is running " + current_software_version)
        print("")

        if current_software_version != expected_version:
            print("ERROR: getCurrentVersionInfo Software Not Upgraded on " + DEVICE)
            print("ERROR: getCurrentVersionInfo Device currently running " + current_software_version)
            print("ERROR: getCurrentVersionInfo Expected version is " + expected_version)
            quit_job("Software Not Upgraded. Expected version: " + expected_version + "  Actual version: " +
                     current_software_version)
        else:
            print("[DESCRIPTION] " + expected_version)

    except:
        print("ERROR: getCurrentVersionInfo Could not retrieve version information")
        quit_job("Device not running expected version")


#
# get the software version of the NTO prior to trying to install
# if the request times out then the NTO is not accessible and exit with
# failure without trying to update
#
def get_release_version_info():
    global DEVICE
    global PORT_WEBAPI
    global USER
    global PASSWD

    job = "https://" + DEVICE + ":" + PORT_WEBAPI + "/api/system?properties=software_version"
    try:
        ret = requests.get(job, auth=(USER, PASSWD), verify=False, timeout=120)
        if ret.status_code != 200:
            print(job, "return status", ret.status_code)
        else:
            json_data = json.loads(ret.text)
            software_version = json_data['software_version']
            print("NTO " + DEVICE + " is currently running " + software_version)
            print("")
    except:
        print("ERROR: getReleaseVersionInfo Could not retrieve version information")
        quit_job("Can not get current Build Identifier from device")


#
# retrieve and return the text file that contains the name of the image zip file for the specified
# BUILD_TYPE, BRANCH_NAME and BUILD_IDENTIFIER
#
def get_image_file_name():
    global REMOTE_FILE_SYSTEM
    global BRANCH_NAME
    global BUILD_IDENTIFIER

    job = "http://" + REMOTE_FILE_SYSTEM + "/builds/" + BRANCH_NAME + "/" + BUILD_IDENTIFIER + "/"

    if BUILD_TYPE == "X86":
        url = job + "upgrade_73xx-62xx_file_name.txt"

    if BUILD_TYPE == "PPC":
        url = job + "upgrade_52xx_file_name.txt"

    if BUILD_TYPE == "OEM":
        url = job + "upgrade_VisionEdge_file_name.txt"

    try:
        ret = requests.get(url)
        file_name = ret.text.rstrip()
        return file_name

    except:
        print("ERROR getFileName: Could not retrieve file: " + url)
        quit_job("Can not get obtain image name file from file server for build " + BUILD_IDENTIFIER)


#
# retrieve and return the text file that contains the release name for the specified
# BUILD_TYPE, BRANCH_NAME and BUILD_IDENTIFIER
#
def get_release_name():
    global REMOTE_FILE_SYSTEM
    global BRANCH_NAME
    global BUILD_IDENTIFIER
    global BUILD_TYPE

    job = "http://" + REMOTE_FILE_SYSTEM + "/builds/" + BRANCH_NAME + "/" + BUILD_IDENTIFIER + "/"

    if BUILD_TYPE == "X86":
        url = job + "upgrade_73xx-62xx_releaseName.txt"
    elif BUILD_TYPE == "PPC":
        url = job + "upgrade_52xx_releaseName.txt"
    elif BUILD_TYPE == "OEM":
        url = job + "upgrade_VisionEdge_releaseName.txt"
    else:
        print("ERROR get_release_name: Invalid BUILD_TYPE environment variable " + BUILD_TYPE)
        quit_job("Invalid BUILD_TYPE environment variable " + BUILD_TYPE)

    try:
        print("getReleaseName: " + url)
        ret = requests.get(url)

    except:
        print("ERROR get_release_name: Could not retrieve: " + url)
        quit_job("Can not get obtain release name file from file server for build " + BUILD_IDENTIFIER)

    release_name = ret.text.rstrip()
    print("INFO: version to be installed is: " + release_name)
    return release_name


def download_image_file(file_name):
    global REMOTE_FILE_SYSTEM
    global BRANCH_NAME
    global BUILD_IDENTIFIER
    global UPGRADE_DIRECTORY

    job = "http://" + REMOTE_FILE_SYSTEM + "/builds/" + BRANCH_NAME + "/" + BUILD_IDENTIFIER + "/"

    print("Downloading", file_name + ".md5")
    try:
        url = job + file_name + ".md5"
        print("downloadImage: " + url)
        ret = requests.get(url)
        md5_file = os.path.join(UPGRADE_DIRECTORY, file_name + ".md5")
        with open(md5_file, 'wb') as f:
            f.write(ret.content)
    except:
        print("ERROR downloadImage: Could not retrieve: " + url)
        quit_job("Could not retrieve: " + url)

    print("Downloading " + file_name)
    try:
        url = job + file_name
        print("downloadImage: " + url)
        ret = requests.get(url)
        image_file = os.path.join(UPGRADE_DIRECTORY, file_name)
        with open(image_file, 'wb') as f:
            f.write(ret.content)
    except:
        print("ERROR downloadImage: Could not retrieve: " + url)
        quit_job("Can not get obtain install image " + BUILD_IDENTIFIER + " from file server")

    return image_file


#
# unfortunately requests.post does not work for installing NTO images
# OverflowError: string longer than 2147483647 bytes
#
def update_nto(image_file):
    global USER
    global PASSWD
    global DEVICE
    global PORT_WEBAPI

    #    downloadFile = [
    #       ('name', 'syy;type=text/plain'),
    #        ('description', 'syy;type;type=text/plain'),
    #        ('file', open(IMAGE_FILE, 'rb'), type=application/zip),
    #    ]
    #
    #    job = "https://" + DEVICE + ":" + PORT_WEBAPI + "/api/actions/install_software"
    #
    #    try:
    #        print("Update NTO with " + IMAGE_FILE)
    #        ret = requests.post(job, data=downloadFile, verify=False, auth=(USER, PASSWD), timeout=3600)
    #    except:
    #        print("ERROR updateNTO: Could not retrieve: " + job + " except status: " + sys.exc_info()[0])
    #        quit_job()
    #
    print("")
    print("Start device update: " + time.strftime("%H:%M:%S"))
    print("")
    curl_cmd = "curl"
    if platform.system() == "Windows":
        curl_cmd = "\\cygwin\\bin\\curl.exe"
    cmd = curl_cmd + " -k -m 1800 -u " + USER + ":" + PASSWD + " https://" + DEVICE + ":" + PORT_WEBAPI + \
          "/api/actions/install_software -F \"name=syy;type=text/plain\" -F \"description=syy;type=text/plain\" -F \"file=@" + \
          image_file + ";type=application/zip\""
    print(cmd)
    os.system(cmd)
    print("")
    print("")
    print("device update complete: " + time.strftime("%H:%M:%S"))
    print("")


#
# start of main routine
#
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

USER = "admin"
PASSWD = "admin"
PORT_WEBAPI = "8000"

if not validate_environment():
    print("ERROR: Environment variables not set. Terminating Install")
    quit_job("Environment variables not set")

WORKSPACE = os.getenv("WORKSPACE")
DEVICE = os.getenv("DEVICE")
DEVICE_TYPE = os.getenv("DEVICE_TYPE")
BUILD_TYPE = os.getenv("BUILD_TYPE")
BRANCH_NAME = os.getenv("BRANCH_NAME")
SLEEP_TIME = os.getenv("SLEEP_TIME")
REMOTE_FILE_SYSTEM = os.getenv("REMOTE_FILE_SYSTEM")
if os.getenv("BUILD_IDENTIFIER") is None:
    BUILD_IDENTIFIER = "latest"
else:
    BUILD_IDENTIFIER = os.getenv("BUILD_IDENTIFIER")

print("")
print("####################################")
print("INFO BUILD IDENTIFIER:", BUILD_IDENTIFIER)
print("INFO BRANCH NAME:", BRANCH_NAME)
print("INFO DEVICE:", DEVICE)
print("INFO DEVICE TYPE:", DEVICE_TYPE)
print("INFO BUILD TYPE:", BUILD_TYPE)
print("INFO SLEEP TIME:", SLEEP_TIME, "seconds")
print("####################################")
print("")

start_time = time.time()

UPGRADE_DIRECTORY = os.path.join(WORKSPACE, "upgrade")
os.mkdir(UPGRADE_DIRECTORY)

get_release_version_info()

imageFileName = get_image_file_name()

updateReleaseName = get_release_name()

imageFile = download_image_file(imageFileName)

update_nto(imageFile)

print("INFO: Wait " + SLEEP_TIME + " seconds for install to complete")

time.sleep(float(SLEEP_TIME))

get_current_version_info(updateReleaseName)

print("")
if DEVICE_TYPE == "7300":
    get_line_card_info()

elapsed_time = time.time() - start_time

print("")
print("Job run time: " + time.strftime("%H:%M:%S", time.gmtime(elapsed_time)))
