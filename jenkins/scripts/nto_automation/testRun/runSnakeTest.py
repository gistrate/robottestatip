import sys
import os
import shutil
import time
import platform
import ftplib
import settings

from deviceSetup import *
from configFile import *
from generateReports import *

#
# exit with error is manner that Jenkins realizes the job was a failure
#
def quit_job():
    sys.exit(1)


#
# insure that all the required environment variables need to perform the
# test run have been set.
#
def validate_environment():
    validEnviron = True

    if os.getenv("WORKSPACE") is None:
        print("ERROR runSnakeTest: Environment variable WORKSPACE not set")
        validEnviron = False

    if os.getenv("DEVICES_DIR") is None:
        print("ERROR runSnakeTest: Environment variable DEVICES_DIR not set")
        validEnviron = False

    if os.getenv("IXTESTER_DIR") is None:
        print("ERROR runSnakeTest: Environment variable IXTESTER_DIR not set")
        validEnviron = False

    if os.getenv("DEVICE") is None:
        print("ERROR runSnakeTest: Environment variable DEVICE not set")
        validEnviron = False

    if os.getenv("DEVICE_TYPE") is None:
        print("ERROR runSnakeTest: Environment variable DEVICE_TYPE not set")
        validEnviron = False

    if os.getenv("SNAKE_TEST_RUN_LENGTH") is None:
        print("ERROR runSnakeTest: Environment variable SNAKE_TEST_RUN_LENGTH not set")
        validEnviron = False

    if not validEnviron:
        print("ERROR runSnakeTest: Environment variables not set. Terminating Test Run")
        quit_job()


#
# transfer zip file to file server is remote file server has been specified
#
def transfer_files(workspace_dir, file_server, reportDir, branch_name, device_type, zip_file):
    os.chdir(workspace_dir)

    ftp = ftplib.FTP(file_server)
    ftp.login("ftpuser", "ftpuser")

    try:
        ftp.mkd("reports")
    except:
        pass
    ftp.cwd("reports")

    try:
        ftp.mkd(reportDir)
    except:
        pass
    ftp.cwd(reportDir)

    try:
        ftp.mkd(branch_name)
    except:
        pass
    ftp.cwd(branch_name)

    try:
        ftp.mkd(device_type)
    except:
        pass
    ftp.cwd(device_type)

    ftp.storbinary('STOR ' + zip_file, open(zip_file, 'rb'))

    ftp.quit()


def execute_snake_test(test_type, test_bucket):

    settings.init()

    print("INFO runSnakeTest: Validate Environment")

    validate_environment()

    DEVICE_IP = os.getenv("DEVICE")
    DEVICE_TYPE = os.getenv("DEVICE_TYPE")
    WORKSPACE_DIR = os.getenv("WORKSPACE")
    DEVICES_DIR = os.getenv("DEVICES_DIR")
    IXTESTER_DIR = os.getenv("IXTESTER_DIR")
    SNAKE_TEST_RUN_LENGTH = os.getenv("SNAKE_TEST_RUN_LENGTH")

    remoteFileSystem = "noTransfer"
    if os.getenv("REMOTE_FILE_SYSTEM") is not None:
        remoteFileSystem = os.getenv("REMOTE_FILE_SYSTEM")

    print("INFO runSnakeTest: Get Build Identifier")
    build_identifier = getBuildIdentifier(DEVICE_IP)
    branch_name = getBranchName(DEVICE_IP)

    currentRunDir = os.path.join(WORKSPACE_DIR, "currentRun", test_type, "testHistory", test_bucket)
    shutil.rmtree(currentRunDir, ignore_errors=True)

    print("INFO runSnakeTest: Creating directory: " + currentRunDir)
    os.makedirs(currentRunDir)

    print("")
    print("INFO runSnakeTest: Device " + DEVICE_IP + " is running " + build_identifier)

    print("")
    print("INFO runSnakeTest: Clear Log Files")
    print("")

    #
    # clean out relevant BRDebug directory
    #
    brpath = os.path.join("C:\\Users", os.getenv("USERNAME"), "BRDebug", platform.node(), test_type, test_bucket)
    print("INFO runSnakeTest: BRDebug directory: " + brpath)
    shutil.rmtree(brpath, ignore_errors=True)

    #
    # clean out the relevant IxTester log directory
    #
    shutil.rmtree(os.path.join(IXTESTER_DIR, "logs", test_type), ignore_errors=True)

    testRunID = test_type + "/" + test_bucket
    print("INFO runSnakeTest: Test: " + testRunID)
    print("")
    print("INFO runSnakeTest: Job Name: " + testRunID + " Started at " + time.strftime("%d/%m/%Y %H:%M:%S"))
    print("")

    IxConfigFile = os.path.join(DEVICES_DIR, DEVICE_IP, test_type, "IxTester.cfg")
    if not os.path.exists(IxConfigFile):
        print("")
        print(" ERROR runSnakeTest: No " + test_type + " config file for " + DEVICE_IP)
        print("")
        quit_job()

    os.makedirs(os.path.join(currentRunDir, "files"))

    print("INFO runSnakeTest: Create test run configuration file")
    print("")

    outputConfigFile = os.path.join(currentRunDir, "files", "AutoSysTests.cfg")

    snake_run = "DAILY"
    if SNAKE_TEST_RUN_LENGTH == "DAILY":
        print("INFO runSnakeTest: " + settings.SNAKE_TEST_DAILY_DESCRIPTION)
        snake_run = settings.SNAKE_TEST_DAILY
    if SNAKE_TEST_RUN_LENGTH == "WEEKEND":
        print("INFO runSnakeTest: " + settings.SNAKE_TEST_WEEKEND_DESCRIPTION)
        snake_run = settings.SNAKE_TEST_WEEKEND
    if SNAKE_TEST_RUN_LENGTH == "EXTENDED":
        print("INFO runSnakeTest: " + settings.SNAKE_TEST_EXTENDED_DESCRIPTION)
        snake_run = settings.SNAKE_TEST_EXTENDED
    if SNAKE_TEST_RUN_LENGTH == "SHORT":
        print("INFO runSnakeTest: " + settings.SNAKE_TEST_SHORT_DESCRIPTION)
        snake_run = settings.SNAKE_TEST_SHORT

    print("")

    #
    # 1. add IxTester.cfg file for test suite to test run config file
    #
    createSnakeConfigFile(IxConfigFile, outputConfigFile, snake_run)

    wish_cmd = IXTESTER_DIR + "\\utilities\\8.5.17.0\\bin\\wish85.exe " + IXTESTER_DIR + \
        "\\system\\RunTests.tcl nto " + \
        outputConfigFile + " " + testRunID
    print(wish_cmd)

    #
    # save current working directory
    #
    currentDir = os.getcwd()

    #
    # change to IxTester directory - wish console can only be run from IxTester directory
    #
    os.chdir(IXTESTER_DIR)

    #
    # run wish console to actually run tests
    #
    os.system(wish_cmd)

    #
    # revert working directory to previous directory
    #
    os.chdir(currentDir)

    clearConfig(DEVICE_IP)
    clearFilterAndPorts(DEVICE_IP)
    clearFilterMemory(DEVICE_IP)
    tokenTimeout(DEVICE_IP)
    pollingInterval(DEVICE_IP)
    loadBalance(DEVICE_IP)

    print("")
    print("INFO runSnakeTest: Complete Current Time " + time.strftime("%d/%m/%Y %H:%M:%S"))
    print("")

    print("INFO runSnakeTest: save log files and BRDebug files to zip files")
    print("")

    logsDir = os.path.join(IXTESTER_DIR, "logs", testRunID)
    shutil.make_archive(os.path.join(currentRunDir, "files", "logFiles"), "zip", root_dir=logsDir, base_dir=".")
    shutil.make_archive(os.path.join(currentRunDir, "files", "BRDebugFiles"), "zip", root_dir=brpath, base_dir=".")

    arr = os.listdir(logsDir)
    useless_directory_name = arr[0]

    print("INFO runSnakeTest: Useless Directory Name: " + useless_directory_name)

    #
    # convert IxTester report IxTester-Report.xml to usable junit report Report.xml
    #
    xml_file = os.path.join(logsDir, useless_directory_name, "IxTester-Report.xml")
    print("INFO runSnakeTest: XML Test Results File: " + xml_file)
    convert_test_bucket(xml_file, DEVICE_IP, build_identifier, test_bucket,
                                      os.path.join(currentRunDir, "IxTester-Report.xml"))

    current_run_name = "currentRun_" + test_type + "_" + test_bucket
    print("")
    print("INFO runSnakeTest: Save current Run results " + current_run_name)
    print("")

    #
    # create zip file containing test results, xml results file and log files
    #
    shutil.make_archive(os.path.join(WORKSPACE_DIR, current_run_name), "zip",
                        root_dir=os.path.join(WORKSPACE_DIR, "currentRun"), base_dir=".")

    print("INFO runSnakeTest: Transfer current Run results " + current_run_name)
    print("")

    if remoteFileSystem is not "noTransfer":
        transfer_files(WORKSPACE_DIR, remoteFileSystem, "systemTest", branch_name, DEVICE_TYPE, current_run_name + ".zip")
        transfer_files(WORKSPACE_DIR, remoteFileSystem, "dailyStatus", branch_name, DEVICE_TYPE, current_run_name + ".zip")


if __name__ == "__main__":
    print("INFO runSnakeTest: Start Run")
    execute_snake_test("10G", "SnakeTest")