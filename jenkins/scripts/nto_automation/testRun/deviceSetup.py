import sys
import requests
import urllib3
import json

def clearConfig(deviceIP):
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    USER = "admin"
    PASSWD = "admin"
    PORT_WEBAPI = "8000"

    print("INFO: Clear Config on NTO: " + deviceIP)

    job = "https://" + deviceIP  + ":" + PORT_WEBAPI + "/api/actions/clear_config"

    try:
        ret = requests.post(job, verify=False, auth=(USER, PASSWD), timeout=300)
        jsonData = json.loads(ret.text)
        print("Return status: " + jsonData['message'])
    except:
        print("ERROR clearConfig: except status: " + job)


def clearFilterAndPorts(deviceIP):
     urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

     USER = "admin"
     PASSWD = "admin"
     PORT_WEBAPI = "8000"

     print("INFO: Clear Filter and Ports on NTO: " + deviceIP)

     job = "https://" + deviceIP + ":" + PORT_WEBAPI + "/api/actions/clear_filters_and_ports"

     try:
         ret = requests.post(job, verify=False, auth=(USER, PASSWD), timeout=300)
         jsonData = json.loads(ret.text)
         print("Return status: " + jsonData['message'])
     except:
         print("ERROR clearFilterAndPorts: except status: " + job)


def clearFilterMemory(deviceIP):
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    USER = "admin"
    PASSWD = "admin"
    PORT_WEBAPI = "8000"

    print("INFO: Clear Filter Memory on NTO: " + deviceIP)

    data = {"custom": "CUSTOM_NONE",
            "dynamic": "L2L3L4_100",
            "dynamic_sip": "IPV4_050_IPV6_050",
            "network": "L2L3L4_50_IPV6_50_VLAN_000_L4_100",
            "network_dynamic_sip_allocation_mix": "NP_050_DSIP_050",
            "tool": "L2L3L4_050_L3V6_050"}

    job = "https://" + deviceIP + ":" + PORT_WEBAPI + "/api/system"

    try:
        ret = requests.put(job, verify=False, auth=(USER, PASSWD), timeout=300, json={'memory_allocation': data})
        print("Return status: " + (str)(ret.status_code))
    except:
        print("ERROR clearFilterMemory: except status: " + job)


def tokenTimeout(deviceIP):
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    USER = "admin"
    PASSWD = "admin"
    PORT_WEBAPI = "8000"

    print("INFO: Set Token Timeout on NTO: " + deviceIP)

    data = {"enabled": True,
            "port": 8000,
            "token_timeout": {"unit": "HOUR", "value": 1}}

    job = "https://" + deviceIP + ":" + PORT_WEBAPI + "/api/system"

    try:
        ret = requests.put(job, verify=False, auth=(USER, PASSWD), timeout=300, json={'web_api_config': data})
        print("Return status: " + (str)(ret.status_code))
    except:
        print("ERROR tokenTimeout: except status: " + job)

def pollingInterval(deviceIP):
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    USER = "admin"
    PASSWD = "admin"
    PORT_WEBAPI = "8000"

    print("INFO: Set Polling Interval on NTO: " + deviceIP)

    job = "https://" + deviceIP + ":" + PORT_WEBAPI + "/api/system"

    try:
        ret = requests.put(job, verify=False, auth=(USER, PASSWD), timeout=300, json={"stats_polling_interval": 1})
        print("Return status: " + (str)(ret.status_code))
    except:
        print("ERROR pollingInterval: except status: " + job)

def loadBalance(deviceIP):
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    USER = "admin"
    PASSWD = "admin"
    PORT_WEBAPI = "8000"

    print("INFO: Set Load Balance on NTO: " + deviceIP)

    data = {"ipv4": "ADDR_PROTOCOL_PORT",
            "ipv6": "ADDR_PROTOCOL_PORT",
            "l2": "MAC",
            "mpls": "TUNNELED_IP",
            "use_l2_for_all": False}

    job = "https://" + deviceIP + ":" + PORT_WEBAPI + "/api/system"

    try:
        ret = requests.put(job, verify=False, auth=(USER, PASSWD), timeout=300, json={'load_balance_settings': data})
        print("Return status: " + (str)(ret.status_code))
    except:
        print("ERROR loadBalance: except status: " + job)

def clearSystem(deviceIP):
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    USER = "admin"
    PASSWD = "admin"
    PORT_WEBAPI = "8000"

    print("INFO: Clear System on NTO: " + deviceIP)

    job = "https://" + deviceIP + ":" + PORT_WEBAPI + "/api/actions/clear_system"

    try:
        ret = requests.post(job, verify=False, auth=(USER, PASSWD), timeout=300)
        jsonData = json.loads(ret.text)
        print("Return status: " + jsonData['message'])
    except:
        print("ERROR clearSystem: except status: " + job)


def getBuildIdentifier(deviceIP):

    PORT_WEBAPI = "8000"
    USER = "admin"
    PASSWD = "admin"

    job = "https://" + deviceIP + ":" + PORT_WEBAPI + "/api/system?properties=software_version"
    try:
        ret = requests.get(job, auth=(USER, PASSWD), verify=False, timeout=120)
        if ret.status_code == 200:
            jsonData = json.loads(ret.text)
            software_version = jsonData['software_version']
            return software_version
        print("ERROR: getBuildIdentifier " + job, " return status ", ret.status_code)
    except:
        print("ERROR: getBuildIdentifier try 1: except status: " + job)

    #
    # 1st failed try again before giving up
    #
    try:
        ret = requests.get(job, auth=(USER, PASSWD), verify=False, timeout=120)
        if ret.status_code == 200:
            jsonData = json.loads(ret.text)
            software_version = jsonData['software_version']
            return software_version
        print("ERROR: getBuildIdentifier " + job, " return status ", ret.status_code)
    except:
        print("ERROR: getBuildIdentifier try 1: except status: " + job)


def getBranchName(deviceIP):

    PORT_WEBAPI = "8000"
    USER = "admin"
    PASSWD = "admin"

    job = "https://" + deviceIP + ":" + PORT_WEBAPI + "/api/system?properties=software_version"
    try:
        ret = requests.get(job, auth=(USER, PASSWD), verify=False, timeout=120)
        if ret.status_code != 200:
            print(job, "return status", ret.status_code)
        jsonData = json.loads(ret.text)
        software_version = jsonData['software_version']
        branch_name = software_version.split("-")[0]

        #
        # get the number of dots. If there are 3 dots then this
        # is a RC build and the RC number needs to be stripped out
        #
        bc = branch_name.count('.')
        if (bc == 3):
            rf = branch_name.rfind('.')
            branch_name = branch_name[:rf]

        return (branch_name)
    except:
        print("ERROR getBranchName: except status: " + job)





