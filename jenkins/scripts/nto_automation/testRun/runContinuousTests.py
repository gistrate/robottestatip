import sys
import os
import argparse
import ftplib
import settings
from runSimultaneousTestSuite import execute_simultaneous_test_suite


#
# exit with error is manner that Jenkins realizes the job was a failure
#
def quit_job():
    sys.exit(1)


#
# insure that all the required environment variables need to perform the
# test run have been set.
#
def validate_environment():
    validEnviron = True

    if os.getenv("DEVICE") is None:
        print("ERROR runContinuousTests: Environment variable DEVICE not set")
        validEnviron = False

    if os.getenv("DEVICES_DIR") is None:
        print("ERROR runContinuousTests: Environment variable DEVICES_DIR not set")
        validEnviron = False

    if os.getenv("REMOTE_FILE_SYSTEM") is None:
        print("ERROR runContinuousTests: Environment variable REMOTE_FILE_SYSTEM not set")
        validEnviron = False

    if os.getenv("WORKSPACE") is None:
        print("ERROR runContinuousTests: Environment variable WORKSPACE not set")
        validEnviron = False

    if not validEnviron:
        print("ERROR runContinuousTests: Environment variables not set. Terminating Test Run")
        quit_job()


#
# write the current simultaneous test suite to the status directory and to the
# file server.
# status directory is used to easily observe the current state of the test run
# file server is used to control the simultaneous test run across multiple Jenkins
# test runs
#
def write_current_state(simultaneous_test_suite):
    global DEVICE_IP
    global REMOTE_FILE_SYSTEM

    status_file = os.path.join(settings.LOCAL_STATUS_DIRECTORY, settings.CURRENT_RUN_FILE)
    write_file = open(status_file, 'w')
    write_file.write(simultaneous_test_suite + "\n")
    write_file.close()

    ftp = ftplib.FTP(REMOTE_FILE_SYSTEM)
    ftp.login(settings.FTP_USER, settings.FTP_PASSWORD)

    try:
        ftp.mkd(settings.FTP_TEST_STATUS_DIRECTORY)
    except:
        pass
    ftp.cwd(settings.FTP_TEST_STATUS_DIRECTORY)

    try:
        ftp.mkd(DEVICE_IP)
    except:
        pass

    ftp.cwd(DEVICE_IP)
    ftp.storbinary('STOR ' + settings.CURRENT_RUN_FILE,
                   open(os.path.join(settings.LOCAL_STATUS_DIRECTORY, settings.CURRENT_RUN_FILE), 'rb'))
    ftp.quit()


#
# delete the current status file from the status directory and from the file server
# this is only done when the Jenkins job completes normally after a full test run
#
def delete_status_directory():
    global DEVICE_IP
    global REMOTE_FILE_SYSTEM

    try:
        os.remove(os.path.join(settings.LOCAL_STATUS_DIRECTORY, settings.CURRENT_RUN_FILE))
    except:
        pass

    ftp = ftplib.FTP(REMOTE_FILE_SYSTEM)
    ftp.login(settings.FTP_USER, settings.FTP_PASSWORD)

    try:
        ftp.mkd(settings.FTP_TEST_STATUS_DIRECTORY)
    except:
        pass
    ftp.cwd(settings.FTP_TEST_STATUS_DIRECTORY)

    try:
        ftp.mkd(DEVICE_IP)
    except:
        pass
    ftp.cwd(DEVICE_IP)

    try:
        ftp.delete(settings.CURRENT_RUN_FILE)
    except:
        pass

    ftp.quit()


def get_current_state():
    global DEVICE_IP
    global REMOTE_FILE_SYSTEM

    status_file = os.path.join(settings.LOCAL_STATUS_DIRECTORY, settings.CURRENT_RUN_FILE)

    ftp = ftplib.FTP(REMOTE_FILE_SYSTEM)
    ftp.login(settings.FTP_USER, settings.FTP_PASSWORD)

    try:
        ftp.mkd(settings.FTP_TEST_STATUS_DIRECTORY)
    except:
        pass
    ftp.cwd(settings.FTP_TEST_STATUS_DIRECTORY)

    try:
        ftp.mkd(DEVICE_IP)
    except:
        pass
    ftp.cwd(DEVICE_IP)

    transfer_status_flag = False
    try:
        ftp.retrbinary("RETR " + settings.CURRENT_RUN_FILE,
                       open(os.path.join(settings.LOCAL_STATUS_DIRECTORY, settings.CURRENT_RUN_FILE), 'wb').write)
        print("INFO runContinuousTests: " + settings.CURRENT_RUN_FILE + " retrieved from file server")
        print("")
        transfer_status_flag = True

    except:
        print("ERROR runContinuousTests: " +  settings.CURRENT_RUN_FILE + " not found on file server")
        print("")
    ftp.quit()

    if os.path.exists(status_file) and not transfer_status_flag:
        print("ERROR runContinuousTests: currentContinuousTestRun not properly retrieved")
        os.remove(status_file)
        print("")


def get_current_test_run(test_type, test_suite_name):
    global DEVICE_IP
    global REMOTE_FILE_SYSTEM

    ftp = ftplib.FTP(REMOTE_FILE_SYSTEM)
    ftp.login(settings.FTP_USER, settings.FTP_PASSWORD)

    try:
        ftp.mkd(settings.FTP_TEST_STATUS_DIRECTORY)
    except:
        pass
    ftp.cwd(settings.FTP_TEST_STATUS_DIRECTORY)

    try:
        ftp.mkd(DEVICE_IP)
    except:
        pass
    ftp.cwd(DEVICE_IP)

    try:
        ftp.mkd(test_type)
    except:
        pass
    ftp.cwd(test_type)

    try:
        ftp.mkd(test_suite_name)
    except:
        pass
    ftp.cwd(test_suite_name)

    try:
        ftp.retrbinary("RETR " + settings.CURRENT_JOB_FILE,
                       open(os.path.join(settings.LOCAL_STATUS_DIRECTORY, test_type, test_suite_name,
                       settings.CURRENT_JOB_FILE), 'wb').write)
        print("INFO runContinuousTests: " + settings.CURRENT_JOB_FILE + " file retrieved from file server")
        print("")
    except:
        print("ERROR runContinuousTests: " + settings.CURRENT_JOB_FILE + " file not found on file server")
        print("")

    ftp.quit()


def munge_current_test_run(job):
    global DEVICE_IP
    global DEVICES_DIR

    test_type = job.split("@")[0]
    test_suite_name = job.split("@")[1].split(".")[0]

    print("INFO runContinuousTests: Test Type: " + test_type)
    print("INFO runContinuousTests: Suite Name: " + test_suite_name)

    os.makedirs(os.path.join(settings.LOCAL_STATUS_DIRECTORY, test_type, test_suite_name))

    get_current_test_run(test_type, test_suite_name)
    test_suite_status_file = os.path.join(settings.LOCAL_STATUS_DIRECTORY, test_type, test_suite_name,
                                          settings.CURRENT_JOB_FILE)
    if not os.path.exists(test_suite_status_file):
        print("")
        print("ERROR runContinuousTests: Current Continuous Test Run status file does not exist " +
              test_suite_status_file)
        print("ERROR runContinuousTests: Starting test run from beginning")
        print("")
        delete_status_directory()
        return False

    test_suite = os.path.join(DEVICES_DIR, DEVICE_IP, test_type, test_suite_name + ".config")
    if not os.path.exists(test_suite):
        print("")
        print("ERROR runContinuousTests: Invalid Current Test Run status File " + test_suite)
        print("ERROR runContinuousTests: Starting test run from beginning")
        print("")
        return False

    read_file = open(test_suite_status_file)
    current_job = read_file.readline()
    current_job = current_job.rstrip()
    read_file.close()

    #
    # create config file of jobs not run that will be run first
    #
    read_config_file = open(test_suite)
    found_flag = False
    lines = []
    for line in read_config_file.readlines():
        line = line.rstrip()
        if line.strip():
            if line.startswith("#"):
                continue
            if line.startswith("@"):
                lines.append(line)
                continue
            if current_job in line:
                found_flag = True
            if found_flag:
                lines.append(line)
    read_config_file.close()


    test_suite_1 = os.path.join(DEVICES_DIR, DEVICE_IP, test_type, test_suite_name + "_1.config")
    write_file = open(test_suite_1, "w")
    for line in lines:
       write_file.write(line + "\n")
    write_file.close()

    #
    # create config file of jobs already run - but will be re-run last
    #
    read_config_file = open(test_suite)
    found_flag = True
    lines = []
    for line in read_config_file.readlines():
        line = line.rstrip()
        if line.strip():
            if line.startswith("#"):
                continue
            if line.startswith("@"):
                lines.append(line)
                continue
            if current_job in line:
                found_flag = False
            if found_flag:
                lines.append(line)
    read_config_file.close()


    test_suite_2 = os.path.join(DEVICES_DIR, DEVICE_IP, test_type, test_suite_name + "_2.config")
    write_file = open(test_suite_2, "w")
    for line in lines:
       write_file.write(line + "\n")
    write_file.close()

    return True


def munge_test_run_list():
    global DEVICE_IP
    global testRunList

    get_current_state()

    status_file = os.path.join(settings.LOCAL_STATUS_DIRECTORY, settings.CURRENT_RUN_FILE)
    if not os.path.exists(status_file):
        print("")
        print("ERROR runContinuousTests: Current Continuous Test Run configuration file does not exist " + status_file)
        print("ERROR runContinuousTests: Starting test run from beginning")
        print("")
        delete_status_directory()
        return

    read_file = open(status_file)
    current_job = read_file.readline()
    current_job = current_job.rstrip()
    read_file.close()

    current_jobs = current_job.split("/")
    for x in current_jobs:
        if not munge_current_test_run(x):
            delete_status_directory()
            return

    print("")
    print("INFO runContinuousTests: Last Continuous Test Run was " + current_job)
    print("")

    new_munge_list = []
    found_flag = False
    new_munge_list.append(current_job.replace(".", "_1."))
    for x in testRunList:
        if x == current_job:
            found_flag = True
            continue
        if found_flag:
            new_munge_list.append(x)

    found_flag = True
    for x in testRunList:
        if x == current_job:
            found_flag = False
        if found_flag:
            new_munge_list.append(x)

    new_munge_list.append(current_job.replace(".", "_2."))

    testRunList = new_munge_list

    delete_status_directory()

def print_munged_simultaneousList():
    global testRunList

    WORKSPACE_DIR = os.getenv("WORKSPACE")
    file = os.path.join(WORKSPACE_DIR, "continuousTestRun.txt")
    write_file = open(file, 'w')
    for x in testRunList:
        write_file.write(x + "\n")
    write_file.close()


if __name__ == "__main__":
    settings.init()

    parser = argparse.ArgumentParser()
    parser.add_argument('--testRunFile', required=True, action='store', dest='testRunFile',
                        help='Continuous Test Run configuration file')
    results = parser.parse_args()

    validate_environment()

    #
    # convert environment variables to global variables
    #
    DEVICE_IP = os.getenv("DEVICE")
    DEVICES_DIR = os.getenv("DEVICES_DIR")
    REMOTE_FILE_SYSTEM = os.getenv("REMOTE_FILE_SYSTEM")

    configFile = os.path.join(DEVICES_DIR, DEVICE_IP,  results.testRunFile)
    if not os.path.exists(configFile):
        print("")
        print(" ERROR runContinuousTests: Test Run config file does not exist " + configFile)
        print("")
        quit_job()

    testRunList = []
    SVT = ""
    read_file = open(configFile)
    print("")
    print("INFO runContinuousTests: Test Run config file: " + configFile)
    print("")
    for line in read_file.readlines():
            line = line.rstrip()
            if not line.strip():
                continue

            #
            # ignore comments
            #
            if line.startswith("#"):
                continue

            #
            # If SVT is to be run when continuous mode is enabled
            #  it is configured by !SVT in the config file
            # 2nd part of the line is the test suite to be run for SVT
            #
            # example
            # !SVT 10G@SVTTestRun.config
            #
            # if !SVT is not present in file SVT will not be run in
            # continuous mode. If continuous mode is not enabled
            # SVT will not be run
            #
            if line.startswith("!SVT"):
                SVT = line.strip("!SVT ")
                print("")
                print("INFO runContinuousTests: SVT Test Run: " + SVT)
                continue

            testRunList.append(line)
    read_file.close()

    if SVT:
        print("")
        print("INFO runContinuousTests:  running SVT " + SVT)
        execute_simultaneous_test_suite(SVT, False)

    #
    # create status directory to continuous mode information on what
    # line of the ContinuousTestRun file is running along with which
    # test bucket is running for the specified suite
    #
    if not os.path.exists(os.path.join(settings.LOCAL_STATUS_DIRECTORY)):
        os.makedirs(os.path.join(settings.LOCAL_STATUS_DIRECTORY))

    #
    # Update the test suite order to reflect the last known state of the
    # continuous test run
    #
    munge_test_run_list()
    print_munged_simultaneousList()

    for simultaneousTestSuite in testRunList:
        write_current_state(simultaneousTestSuite)
        print("")
        print("INFO runContinuousTests: running " + simultaneousTestSuite)
        execute_simultaneous_test_suite(simultaneousTestSuite, True)

    delete_status_directory()



