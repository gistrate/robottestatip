import sys
import os
import shutil
import time
import platform
import ftplib
import argparse

from deviceSetup import *
from configFile import *
from generateReports import *

#
# exit with error is manner that Jenkins realizes the job was a failure
#
def quit_job():
    sys.exit(1)


#
# insure that all the required environment variables need to perform the
# test run have been set.
#
def validate_environment():
    validEnviron = True

    if os.getenv("WORKSPACE") is None:
        print("ERROR runTestBucket: Environment variable WORKSPACE not set")
        validEnviron = False

    if os.getenv("DEVICES_DIR") is None:
        print("ERROR runTestBucket: Environment variable DEVICES_DIR not set")
        validEnviron = False

    if os.getenv("IXTESTER_DIR") is None:
        print("ERROR runTestBucket: Environment variable IXTESTER_DIR not set")
        validEnviron = False

    if os.getenv("DEVICE") is None:
        print("ERROR runTestBucket: Environment variable DEVICE not set")
        validEnviron = False

    if os.getenv("DEVICE_TYPE") is None:
        print("ERROR runTestBucket: Environment variable DEVICE_TYPE not set")
        validEnviron = False

    if not validEnviron:
        print("ERROR runTestBucket: Environment variables not set. Terminating Test Run")
        quit_job()


#
# transfer zip file to file server is remote file server has been specified
#
def transfer_files(workspace_dir, file_server, reportDir, branch_name, device_type, zip_file):
    os.chdir(workspace_dir)

    ftp = ftplib.FTP(file_server)
    ftp.login("ftpuser", "ftpuser")

    try:
        ftp.mkd("reports")
    except:
        pass
    ftp.cwd("reports")

    try:
        ftp.mkd(reportDir)
    except:
        pass
    ftp.cwd(reportDir)

    try:
        ftp.mkd(branch_name)
    except:
        pass
    ftp.cwd(branch_name)

    try:
        ftp.mkd(device_type)
    except:
        pass
    ftp.cwd(device_type)

    ftp.storbinary('STOR ' + zip_file, open(zip_file, 'rb'))

    ftp.quit()


def execute_test_bucket(test_type, test_bucket, parallel_mode, overrides):

    validate_environment()

    DEVICE_IP = os.getenv("DEVICE")
    DEVICE_TYPE = os.getenv("DEVICE_TYPE")
    WORKSPACE_DIR = os.getenv("WORKSPACE")
    DEVICES_DIR = os.getenv("DEVICES_DIR")
    IXTESTER_DIR = os.getenv("IXTESTER_DIR")

    remoteFileSystem = "noTransfer"
    if os.getenv("REMOTE_FILE_SYSTEM") is not None:
        remoteFileSystem = os.getenv("REMOTE_FILE_SYSTEM")

    build_identifier = getBuildIdentifier(DEVICE_IP)
    branch_name = getBranchName(DEVICE_IP)

    currentRunDir = os.path.join(WORKSPACE_DIR, "currentRun", test_type, "testHistory", test_bucket)
    shutil.rmtree(currentRunDir, ignore_errors=True)

    print("INFO runTestBucket: Creating directory: " + currentRunDir)
    os.makedirs(currentRunDir)

    print("")
    print("INFO runTestBucket: Device " + DEVICE_IP + " is running " + build_identifier)

    print("")
    print("INFO runTestBucket: Clear Log Files")
    print("")

    #
    # clean out relevant BRDebug directory
    #
    brpath = os.path.join("C:\\Users", os.getenv("USERNAME"), "BRDebug", platform.node(), test_type, test_bucket)
    print("INFO runTestBucket: BRDebug directory: " + brpath)
    shutil.rmtree(brpath, ignore_errors=True)

    #
    # clean out the relevant IxTester log directory
    #
    shutil.rmtree(os.path.join(IXTESTER_DIR, "logs", test_type), ignore_errors=True)

    testRunID = test_type + "/" + test_bucket
    print("INFO runTestBucket: Test: " + testRunID)
    print("")
    print("INFO runTestBucket: Job Name: " + testRunID + " Started at " + time.strftime("%d/%m/%Y %H:%M:%S"))
    print("")

    IxConfigFile = os.path.join(DEVICES_DIR, DEVICE_IP, test_type, "IxTester.cfg")
    if not os.path.exists(IxConfigFile):
        print("")
        print(" ERROR runTestBucket: No " + test_type + " config file for " + DEVICE_IP)
        print("")
        quit_job()

    os.makedirs(os.path.join(currentRunDir, "files"))

    print("INFO runTestBucket: Create test run configuration file")
    print("")

    outputConfigFile = os.path.join(currentRunDir, "files", "AutoSysTests.cfg")

    #
    # 1. write standard Template.cfg file to test run config file
    #
    writeTemplateFile(DEVICES_DIR, outputConfigFile)

    #
    # 2. add IxTester.cfg file for test suite to test run config file
    #
    addIxConfigFile(IxConfigFile, outputConfigFile)

    #
    # 3. add parallel or serial configuration parameters to test run config file
    #
    addConfigParms(DEVICES_DIR, parallel_mode, outputConfigFile)

    #
    # 3. add override configuration parameters to test run config file
    #
    add_override_parameters(overrides, outputConfigFile)

    #
    # 5. add tests to run from test_bucket to test run config file
    #
    if test_bucket == "SVT":
        parseSVTTestFile(IXTESTER_DIR, test_bucket, outputConfigFile)
    else:
        if test_bucket == "DynamicFiltersIPv4_8184_ADDRESSES":
            add_8K_overrides(DEVICES_DIR, outputConfigFile)
            parseNTOTestFile(IXTESTER_DIR, "8K_Filter", test_bucket, outputConfigFile)
        else:
            parseNTOTestFile(IXTESTER_DIR, "", test_bucket, outputConfigFile)

    print("INFO runNTOTest: Start Automation Tests " + test_bucket)
    print("")

    wish_cmd = IXTESTER_DIR + "\\utilities\\8.5.17.0\\bin\\wish85.exe " + IXTESTER_DIR + \
        "\\system\\RunTests.tcl nto " + \
        outputConfigFile + " " + testRunID
    print(wish_cmd)

    #
    # save current working directory
    #
    currentDir = os.getcwd()

    #
    # change to IxTester directory - wish console can only be run from IxTester directory
    #
    os.chdir(IXTESTER_DIR)

    #
    # run wish console to actually run tests
    #
    os.system(wish_cmd)

    #
    # revert working directory to previous directory
    #
    os.chdir(currentDir)

    if not parallel_mode:
        clearConfig(DEVICE_IP)
        clearFilterAndPorts(DEVICE_IP)
        clearFilterMemory(DEVICE_IP)
        tokenTimeout(DEVICE_IP)
        pollingInterval(DEVICE_IP)
        loadBalance(DEVICE_IP)

    print("")
    print("INFO runTestBucket: Complete Current Time " + time.strftime("%d/%m/%Y %H:%M:%S"))
    print("")

    print("INFO runTestBucket: save log files and BRDebug files to zip files")
    print("")

    logsDir = os.path.join(IXTESTER_DIR, "logs", testRunID)
    shutil.make_archive(os.path.join(currentRunDir, "files", "logFiles"), "zip", root_dir=logsDir, base_dir=".")
    shutil.make_archive(os.path.join(currentRunDir, "files", "BRDebugFiles"), "zip", root_dir=brpath, base_dir=".")

    arr = os.listdir(logsDir)
    useless_directory_name = arr[0]

    print("INFO runTestBucket: Useless Directory Name: " + useless_directory_name)

    #
    # convert IxTester report IxTester-Report.xml to usable junit report Report.xml
    #
    xml_file = os.path.join(logsDir, useless_directory_name, "IxTester-Report.xml")
    print("INFO runTestBucket: XML Test Results File: " + xml_file)
    convert_test_bucket(xml_file, DEVICE_IP, build_identifier, test_bucket,
                      os.path.join(currentRunDir, "IxTester-Report.xml"))

    current_run_name = "testRun_" + test_type + "_" + test_bucket
    print("")
    print("INFO runTestBucket: Save current Run results " + current_run_name)
    print("")

    #
    # create zip file containing test results, xml results file and log files
    #
    base = os.path.join(test_type, "testHistory", test_bucket)
    shutil.make_archive(os.path.join(WORKSPACE_DIR, current_run_name), "zip",
                        root_dir=os.path.join(WORKSPACE_DIR, "currentRun"), base_dir=base)

    print("INFO runTestBucket: Transfer current Run results " + current_run_name)
    print("")

    if remoteFileSystem is not "noTransfer":
        transfer_files(WORKSPACE_DIR, remoteFileSystem, "systemTest", branch_name, DEVICE_TYPE, current_run_name + ".zip")
        transfer_files(WORKSPACE_DIR, remoteFileSystem, "dailyStatus", branch_name, DEVICE_TYPE, current_run_name + ".zip")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--testType', required=True, action='store', dest='testType',
                        help='Test type to run')
    parser.add_argument('--testBucket', required=True, action='store', dest='testBucket',
                        help='Test Bucket to run')
    parser.add_argument('--parallelMode', dest="parallelMode",
                        help='Test Bucket run in parallel mode', action='store_true')
    results = parser.parse_args()
    execute_test_bucket(results.testType, results.testBucket, results.parallelMode, "")