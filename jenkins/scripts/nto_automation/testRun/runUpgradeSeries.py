import os
import sys
import argparse
from runUpgradeTest import execute_upgrade_test


#
# exit with error is manner that Jenkins realizes the job was a failure
#
def quit_job():
    sys.exit(1)


#
# insure that all the required environment variables need to perform the
# test run have been set.
#
def validate_environment():
    valid_environ = True

    if os.getenv("DEVICES_DIR") is None:
        print("ERROR runUpgradeSeries: Environment variable DEVICES_DIR not set")
        valid_environ = False

    if os.getenv("DEVICE") is None:
        print("ERROR runUpgradeSeries: Environment variable DEVICE not set")
        valid_environ = False

    if os.getenv("TEST_TYPE") is None:
        print("ERROR runUpgradeSeries: Environment variable TEST_TYPE not set")
        valid_environ = False

    if not valid_environ:
        print("ERROR runUpgradeSeries: Environment variables not set. Terminating Test Run")
        quit_job()


def run_upgrade_series(config_file):
    validate_environment()

    DEVICE_IP = os.getenv("DEVICE")
    DEVICES_DIR = os.getenv("DEVICES_DIR")
    TEST_TYPE = os.getenv("TEST_TYPE")

    test_run_file = os.path.join(DEVICES_DIR, DEVICE_IP, TEST_TYPE, "testRuns", config_file)
    if not os.path.exists(test_run_file):
        print("")
        print(" ERROR runUpgradeSeries: No " + config_file + " config file for " + DEVICE_IP + " for " + TEST_TYPE)
        print("")
        quit_job()

    config_fd = open(test_run_file)
    run_index = 1
    release_to_test = None
    for line in config_fd.readlines():
        line = line.rstrip()
        if not line:
            continue
        if line.startswith("#"):
            continue
        if "!release" in line:
            release_to_test = line.strip("!release ")
            print("INFO runUpgradeSeries: Release to Test: " + release_to_test)
            continue
        print("INFO runUpgradeSeries: Upgrade Test:" + line)
        if release_to_test:
            execute_upgrade_test(TEST_TYPE, "SystemLevelTests", str(run_index), release_to_test, line)
            run_index = run_index + 1
        else:
            print("ERROR runUpgradeSeries: No Release to Test specified. Terminating Test Run")
            quit_job()
    config_fd.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--configFile', required=True, action='store', dest='configFile',
                        help='Configuration File')
    results = parser.parse_args()

    run_upgrade_series(results.configFile)

