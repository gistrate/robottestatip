import sys
import os
import shutil
import time
import platform
import ftplib
import argparse

from deviceSetup import *
from configFile import *
from generateReports import *


#
# exit with error is manner that Jenkins realizes the job was a failure
#
def quit_job():
    sys.exit(1)


#
# insure that all the required environment variables need to perform the
# test run have been set.
#
def validate_environment():
    valid_environ = True

    if os.getenv("WORKSPACE") is None:
        print("ERROR runUpgradeTest: Environment variable WORKSPACE not set")
        valid_environ = False

    if os.getenv("DEVICES_DIR") is None:
        print("ERROR runUpgradeTest: Environment variable DEVICES_DIR not set")
        valid_environ = False

    if os.getenv("IXTESTER_DIR") is None:
        print("ERROR runUpgradeTest: Environment variable IXTESTER_DIR not set")
        valid_environ = False

    if os.getenv("DEVICE") is None:
        print("ERROR runUpgradeTest: Environment variable DEVICE not set")
        valid_environ = False

    if os.getenv("DEVICE_TYPE") is None:
        print("ERROR runUpgradeTest: Environment variable DEVICE_TYPE not set")
        valid_environ = False

    if not valid_environ:
        print("ERROR runUpgradeTest: Environment variables not set. Terminating Test Run")
        quit_job()


#
# transfer zip file to file server is remote file server has been specified
#
def transfer_files(workspace_dir, file_server, report_dir, branch_name, device_type, zip_file):
    os.chdir(workspace_dir)

    ftp = ftplib.FTP(file_server)
    ftp.login("ftpuser", "ftpuser")

    try:
        ftp.mkd("reports")
    except:
        pass
    ftp.cwd("reports")

    try:
        ftp.mkd(report_dir)
    except:
        pass
    ftp.cwd(report_dir)

    try:
        ftp.mkd(branch_name)
    except:
        pass
    ftp.cwd(branch_name)

    try:
        ftp.mkd(device_type)
    except:
        pass
    ftp.cwd(device_type)

    ftp.storbinary('STOR ' + zip_file, open(zip_file, 'rb'))

    ftp.quit()


def execute_upgrade_test(test_type, test_bucket, run_index, release_to_test, upgrade_path_to_test):

    validate_environment()

    DEVICE_IP = os.getenv("DEVICE")
    DEVICE_TYPE = os.getenv("DEVICE_TYPE")
    WORKSPACE_DIR = os.getenv("WORKSPACE")
    DEVICES_DIR = os.getenv("DEVICES_DIR")
    IXTESTER_DIR = os.getenv("IXTESTER_DIR")

    remote_file_system = "noTransfer"
    if os.getenv("REMOTE_FILE_SYSTEM") is not None:
        remote_file_system = os.getenv("REMOTE_FILE_SYSTEM")

    current_run_directory = os.path.join(WORKSPACE_DIR, "currentRun", test_type, "testHistory", test_bucket)
    shutil.rmtree(current_run_directory, ignore_errors=True)

    print("INFO runUpgradeTest: Creating directory: " + current_run_directory)
    os.makedirs(current_run_directory)

    print("")
    print("INFO runUpgradeTest: Clear Log Files")
    print("")

    #
    # clean out relevant BRDebug directory
    #
    brdebug_directory = os.path.join("C:\\Users", os.getenv("USERNAME"), "BRDebug", platform.node(),
                                     test_type, test_bucket)
    print("INFO runUpgradeTest: BRDebug directory: " + brdebug_directory)
    shutil.rmtree(brdebug_directory, ignore_errors=True)

    #
    # clean out the relevant IxTester log directory
    #
    shutil.rmtree(os.path.join(IXTESTER_DIR, "logs", test_type), ignore_errors=True)

    test_run_id = test_type + "/" + test_bucket
    print("INFO runUpgradeTest: Test: " + test_run_id)
    print("")
    print("INFO runUpgradeTest: Job Name: " + test_run_id + " Started at " + time.strftime("%d/%m/%Y %H:%M:%S"))
    print("")

    ix_tester_config_file = os.path.join(DEVICES_DIR, DEVICE_IP, test_type, "IxTester.cfg")
    if not os.path.exists(ix_tester_config_file):
        print("")
        print(" ERROR runUpgradeTest: No " + test_type + " config file for " + DEVICE_IP)
        print("")
        quit_job()

    os.makedirs(os.path.join(current_run_directory, "files"))

    print("INFO runUpgradeTest: Create test run configuration file")
    print("")

    test_run_config_file = os.path.join(current_run_directory, "files", "AutoSysTests_" + run_index + ".cfg")

    #
    # 1. add IxTester.cfg file for test suite to test run config file
    #
    addIxConfigFile(ix_tester_config_file, test_run_config_file)

    #
    # 2. add upgrade paths to test run config file
    #
    addUpgradePaths(upgrade_path_to_test, test_run_config_file)

    print("INFO runNTOTest: Start Automation Tests " + test_bucket)
    print("")

    wish_cmd = IXTESTER_DIR + "\\utilities\\8.5.17.0\\bin\\wish85.exe " + IXTESTER_DIR + \
               "\\system\\RunTests.tcl nto " + \
               test_run_config_file + " " + test_run_id
    print(wish_cmd)

    #
    # save current working directory
    #
    current_directory = os.getcwd()

    #
    # change to IxTester directory - wish console can only be run from IxTester directory
    #
    os.chdir(IXTESTER_DIR)

    #
    # run wish console to actually run tests
    #
    os.system(wish_cmd)

    #
    # revert working directory to previous directory
    #
    os.chdir(current_directory)

    print("")
    print("INFO runUpgradeTest: Complete Current Time " + time.strftime("%d/%m/%Y %H:%M:%S"))
    print("")

    print("INFO runUpgradeTest: save log files and BRDebug files to zip files")
    print("")

    logs_directory = os.path.join(IXTESTER_DIR, "logs", test_run_id)
    shutil.make_archive(os.path.join(current_run_directory, "files", "logFiles_" + run_index), "zip",
                        root_dir=logs_directory, base_dir=".")
    shutil.make_archive(os.path.join(current_run_directory, "files", "BRDebugFiles_" + run_index), "zip",
                        root_dir=brdebug_directory, base_dir=".")

    arr = os.listdir(logs_directory)
    useless_directory_name = arr[0]

    print("INFO runUpgradeTest: Useless Directory Name: " + useless_directory_name)

    #
    # convert IxTester report IxTester-Report.xml to usable junit report Report.xml
    #
    xml_file = os.path.join(logs_directory, useless_directory_name, "IxTester-Report.xml")
    print("INFO runUpgradeTest: XML Test Results File: " + xml_file)
    if os.path.exists(xml_file):
        convert_system_level_bucket(xml_file, DEVICE_IP, release_to_test, test_bucket, upgrade_path_to_test,
                                    os.path.join(current_run_directory, "IxTester-Report.xml"))
    else:
        print("ERROR runUpgradeTest: XML Test Results File: " + xml_file + " does not exist")

    csv_file = os.path.join(logs_directory, useless_directory_name, "UPGRADE-FROM-TO-BUILD-IMP-EXP-Report.csv")
    if os.path.exists(csv_file):
        shutil.move(csv_file, os.path.join(current_run_directory, "Report_" + run_index + ".csv"))
    else:
        print("ERROR runUpgradeTest: CSV Test Results File: " + csv_file + " does not exist")

    xml_file = os.path.join(current_run_directory, "IxTester-Report.xml")
    if os.path.exists(xml_file):
        shutil.move(xml_file, os.path.join(current_run_directory, "IxTester-Report_" + run_index + ".xml"))
    else:
        print("ERROR runUpgradeTest: XML Test Results File: " + xml_file + " does not exist")

    current_run_name = "currentRun_" + test_type + "_" + test_bucket + "_" + run_index
    print("")
    print("INFO runUpgradeTest: Save current Run results " + current_run_name)
    print("")

    #
    # create zip file containing test results, xml results file and log files
    #
    shutil.make_archive(os.path.join(WORKSPACE_DIR, current_run_name), "zip",
                        root_dir=os.path.join(WORKSPACE_DIR, "currentRun"), base_dir=".")

    print("INFO runUpgradeTest: Transfer current Run results " + current_run_name)
    print("")

    if remote_file_system is not "noTransfer":
        transfer_files(WORKSPACE_DIR, remote_file_system, "systemTest", release_to_test, DEVICE_TYPE,
                       current_run_name + ".zip")
        transfer_files(WORKSPACE_DIR, remote_file_system, "dailyStatus", release_to_test, DEVICE_TYPE,
                       current_run_name + ".zip")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--releaseToTest', required=True, action='store', dest='releaseToTest',
                        help='Release to Test')
    parser.add_argument('--upgradeTest', required=True, action='store', dest='upgradeTest',
                        help='Upgrade Tests to run')
    parser.add_argument('--testType', required=True, action='store', dest='testType',
                        help='Test Type to run')
    results = parser.parse_args()

    execute_upgrade_test(results.testType, "SystemLevelTests", "1", results.releaseToTest, results.upgradeTest)

