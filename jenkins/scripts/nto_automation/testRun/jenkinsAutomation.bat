ECHO OFF

set PATH=%PATH%;c:\cygwin\bin;C:\Python34

SET PYTHONPATH=%PYTHONPATH%;C:\Python34\Lib;%WORKSPACE%\jenkins;%WORKSPACE%\jenkins\scripts\nto_automation\testRun

SET DEVICES_DIR=%WORKSPACE%\jenkins\automationScheduler\configuration\devices
SET IXTESTER_DIR=%WORKSPACE%\IxTester
SET STATUS_DIR=%WORKSPACE%\status

ECHO svn checkout %SVN_RODAN%/test/system/Automation/IxTester/trunk IxTester
svn checkout --username %SVN_USER% --password %SVN_PASSWORD% %SVN_RODAN%/test/system/Automation/IxTester/%IXTESTER_SVN_BRANCH% IxTester 1>NUL

cd jenkins\scripts\nto_automation\testRun

IF "%CONTINUOUS_RUN_FLAG%" == "true" (
    ECHO python -u runContinuousTests.py --testRunFile %CONTINUOUS_TEST_RUN_FILE%
    python -u runContinuousTests.py --testRunFile %CONTINUOUS_TEST_RUN_FILE% | C:\cygwin\bin\tee %WORKSPACE%\testRunLog.txt
) ELSE (
    ECHO python -u runSimultaneousTestSuite.py --testSuiteCombo %TEST_RUNS%
    python -u runSimultaneousTestSuite.py --testSuiteCombo %TEST_RUNS% | C:\cygwin\bin\tee %WORKSPACE%\testRunLog.txt
)
