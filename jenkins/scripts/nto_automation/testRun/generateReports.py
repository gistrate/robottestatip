import xml.etree.ElementTree as ET
import time
import argparse

#
# convert the XML report file generated by IxTester to a usable junit report
# 1. remove testsuites line
# 2. add buildIdentifier, testBucket Name and overall test run time to testsuite line
# 3. on each testcase convert time in seconds to hh:mm;ss format
# 4, replace testcase name with specified name
#

def convert_system_level_bucket(xml_file, device_ip, build_identifier, test_bucket, test_name, output_file):

    itime = 0
    buffer = []

    tree = ET.parse(xml_file)
    root = tree.getroot()

    for child in root:
        for children in child:
            classname = children.attrib["classname"]
            itime = int(children.attrib["time"]) + itime
            classname = classname.split(".")[1]
            buffer.append("  <testcase name=\"" + test_name +
                          "\" time=\"" + time.strftime('%H:%M:%S', time.gmtime(int(children.attrib["time"]))) +
                          "\" classname=\"" + classname + "\">")
            skipped = children.find('skipped')
            if skipped is not None:
                buffer.append("    <skipped/>")

            error_msg = children.find('error')
            if error_msg is not None:
                buffer.append("    <error message=\"" + error_msg.attrib["message"] + "\">")
                buffer.append("<![CDATA[")
                buffer.append(error_msg.text)
                buffer.append("]]>")
                buffer.append("    </error>")
            buffer.append("  </testcase>")

    try:
        write_fd = open(output_file, 'w')
        write_fd.write("<?xml version=\"1.0\"?>\n")
        write_fd.write("<testsuite name=\"" + test_bucket + "\" hostname=\"" + build_identifier +
                       "\" device_ip=\"" + device_ip +
                       "\" tests=\"" + child.attrib["tests"] +
                       "\" failures=\"" + child.attrib["failures"] +
                       "\" skipped=\"" + child.attrib["skipped"] +
                       "\" date=\"" + time.strftime("%H:%M:%S %m/%d/%Y") +
                       "\" time=\"" + time.strftime('%H:%M:%S', time.gmtime(itime)) + "\">\n")
        for line in buffer:
            write_fd.write(str(line) + "\n")
        write_fd.write("</testsuite>\n")
        write_fd.close()
    except:
        print("Write XML File Failure")


#
# convert the XML report file generated by IxTester to a usable junit report
# 1. remove testsuites line
# 2. add buildIdentifier, testBucket Name and overall test run time to testsuite line
# 3. on each testcase convert time in seconds to hh:mm;ss format
#

def convert_test_bucket(xml_file, device_ip, build_identifier, test_bucket, output_file):

    itime = 0
    buffer = []

    tree = ET.parse(xml_file)
    root = tree.getroot()

    for child in root:
        for children in child:
            classname = children.attrib["classname"]
            itime = int(children.attrib["time"]) + itime
            classname = classname.split(".")[1]
            buffer.append("  <testcase name=\"" + children.attrib["name"] +
                          "\" time=\"" + time.strftime('%H:%M:%S', time.gmtime(int(children.attrib["time"]))) +
                          "\" classname=\"" + classname + "\">")
            skipped = children.find('skipped')
            if skipped is not None:
                buffer.append("    <skipped/>")

            error_msg = children.find('error')
            if error_msg is not None:
                buffer.append("    <error message=\"" + error_msg.attrib["message"] + "\">")
                buffer.append("<![CDATA[")
                buffer.append(error_msg.text)
                buffer.append("]]>")
                buffer.append("    </error>")
            buffer.append("  </testcase>")

    try:
        write_fd = open(output_file, 'w')
        write_fd.write("<?xml version=\"1.0\"?>\n")
        write_fd.write("<testsuite name=\"" + test_bucket + "\" hostname=\"" + build_identifier +
                       "\" device_ip=\"" + device_ip +
                       "\" tests=\"" + child.attrib["tests"] +
                       "\" failures=\"" + child.attrib["failures"] +
                       "\" skipped=\"" + child.attrib["skipped"] +
                       "\" date=\"" + time.strftime("%H:%M:%S %m/%d/%Y") +
                       "\" time=\"" + time.strftime('%H:%M:%S', time.gmtime(itime)) + "\">\n")
        for line in buffer:
            write_fd.write(str(line) + "\n")
        write_fd.write("</testsuite>\n")
        write_fd.close()
    except:
        print("Write XML File Failure")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--xmlFile', required=True, action='store', dest='xmlFile',
                        help='XML Input File')
    parser.add_argument('--buildIdentifier', required=True, action='store', dest='buildIdentifier',
                        help='Build Identifier')
    parser.add_argument('--testBucket', required=True, action='store', dest="testBucket",
                        help='Name of Test Bucket')
    parser.add_argument('--deviceIP', required=True, action='store', dest="deviceIP",
                        help='Device IP Address')
    parser.add_argument('--outputFile', required=True, action='store', dest="outputFile",
                        help='Xml Input File')

    results = parser.parse_args()
    xmlFile = results.xmlFile
    deviceIP = results.deviceIP
    buildIdentifier = results.buildIdentifier
    testBucket = results.testBucket
    outputFile = results.outputFile

    convert_test_bucket(xmlFile, deviceIP, buildIdentifier, testBucket, outputFile)
