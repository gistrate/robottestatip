import sys
import os
import ftplib
import settings
import argparse
from runTestBucket import execute_test_bucket


#
# exit with error is manner that Jenkins realizes the job was a failure
#
def quit_job():
    sys.exit(1)


def delete_current_job_file(current_job_file, testType, testName):
    settings.init()

    DEVICE_IP = os.getenv("DEVICE")
    REMOTE_FILE_SYSTEM = os.getenv("REMOTE_FILE_SYSTEM")

    os.remove(current_job_file)

    ftp = ftplib.FTP(REMOTE_FILE_SYSTEM)
    ftp.login(settings.FTP_USER, settings.FTP_PASSWORD)

    try:
        ftp.mkd(settings.FTP_TEST_STATUS_DIRECTORY)
    except:
        pass
    ftp.cwd(settings.FTP_TEST_STATUS_DIRECTORY)

    try:
        ftp.mkd(DEVICE_IP)
    except:
        pass
    ftp.cwd(DEVICE_IP)

    try:
        ftp.mkd(testType)
    except:
        pass
    ftp.cwd(testType)

    try:
        ftp.mkd(testName)
    except:
        pass
    ftp.cwd(testName)

    ftp.delete(settings.CURRENT_JOB_FILE)

    ftp.quit()


def write_current_job_file(current_job_file, test_bucket, testType, testName):
    settings.init()

    DEVICE_IP = os.getenv("DEVICE")
    REMOTE_FILE_SYSTEM = os.getenv("REMOTE_FILE_SYSTEM")

    write_file = open(current_job_file, 'w')
    write_file.write(test_bucket + "\n")
    write_file.close()

    ftp = ftplib.FTP(REMOTE_FILE_SYSTEM)
    ftp.login(settings.FTP_USER, settings.FTP_PASSWORD)

    try:
        ftp.mkd(settings.FTP_TEST_STATUS_DIRECTORY)
    except:
        pass
    ftp.cwd(settings.FTP_TEST_STATUS_DIRECTORY)

    try:
        ftp.mkd(DEVICE_IP)
    except:
        pass
    ftp.cwd(DEVICE_IP)

    try:
        ftp.mkd(testType)
    except:
        pass
    ftp.cwd(testType)

    try:
        ftp.mkd(testName)
    except:
        pass
    ftp.cwd(testName)

    ftp.storbinary('STOR ' + settings.CURRENT_JOB_FILE,
                   open(current_job_file, 'rb'))
    ftp.quit()


#
# insure that all the required environment variables need to perform the
# test run have been set.
#
def validate_environment():
    validEnviron = True

    if os.getenv("DEVICE") is None:
        print("ERROR runTestSuite: Environment variable DEVICE not set")
        validEnviron = False

    if os.getenv("DEVICES_DIR") is None:
        print("ERROR runTestSuite: Environment variable DEVICES_DIR not set")
        validEnviron = False

    if os.getenv("STATUS_DIR") is None:
        print("ERROR runTestSuite: Environment variable STATUS_DIR not set")
        validEnviron = False

    if os.getenv("REMOTE_FILE_SYSTEM") is None:
        print("ERROR runTestSuite: Environment variable REMOTE_FILE_SYSTEM not set")
        validEnviron = False

    if not validEnviron:
        print("ERROR runTestSuite: Environment variables not set. Terminating Test Run")
        quit_job()


def execute_test_suite(testType, testSuite, continuousMode):
    settings.init()

    validate_environment()

    DEVICE_IP = os.getenv("DEVICE")
    DEVICES_DIR = os.getenv("DEVICES_DIR")
    STATUS_DIR = os.getenv("STATUS_DIR")

    print("")
    print("INFO runTestSuite: Start Test Type " + testType + "  Test Suite " + testSuite)
    print("")

    testName = testSuite

    configFile = os.path.join(DEVICES_DIR, DEVICE_IP, testType, testSuite)
    if not os.path.exists(configFile):
        print("")
        print("ERROR runTestSuite: Test Suite config file not found: " + configFile)
        print("")
        quit_job()

    parallelMode = False
    readFile = open(configFile)
    currentJobFile = ""
    for line in readFile.readlines():
        line = line.rstrip()
        if line.strip():
            #
            # comment line - ignore
            #
            if line.startswith("#"):
                continue

            #
            # test suite option
            #
            if line.startswith("@"):
                if line.startswith("@parallel"):
                    print("INFO runTestSuite: Start Test Type " + testType + "  Test Suite " + testSuite
                          + " parallel mode")
                    parallelMode = True
                if line.startswith("@name"):
                    testName = line.split(" ")[1]
                    currentStatusDir = os.path.join(STATUS_DIR, testType, testName)
                    currentJobFile = os.path.join(currentStatusDir, settings.CURRENT_JOB_FILE)
                continue
            #
            # test bucket line
            # split into parameters separated by semi-colon
            # first parameter is the name of the test bucket
            # any additional parameters are options used when
            # running the test bucket
            #
            options = ""

            parsed_line = line.split(";")
            test_bucket = parsed_line[0]

            if len(parsed_line) > 1:
                for i in parsed_line[1::]:
                    options = options + i + ";"
            if continuousMode:
                if not os.path.exists(currentStatusDir):
                    os.makedirs(currentStatusDir)
                write_current_job_file(currentJobFile, test_bucket, testType, testName)

            execute_test_bucket(testType, test_bucket, parallelMode, options)

    readFile.close()

    if continuousMode:
        delete_current_job_file(currentJobFile, testType, testName)

    print("")
    print("INFO runTestSuite: Completed Test Type " + testType + "  Test Suite " + testSuite)
    print("")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--testType', required=True, action='store', dest='testType',
                        help='Test Suites to run in parallel')
    parser.add_argument('--testSuite', required=True, action='store', dest='testSuite',
                        help='Test Suites to run in parallel')
    parser.add_argument('--continuousMode', dest="continuousMode",
                        help='Test Suite run in continuous mode', action='store_true')
    results = parser.parse_args()

    execute_test_suite(results.testType, results.testSuite, results.continuousMode)

