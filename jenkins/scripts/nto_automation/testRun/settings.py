
def init():
    global FTP_USER
    global FTP_PASSWORD
    global CURRENT_RUN_FILE
    global CURRENT_JOB_FILE
    global LOCAL_STATUS_DIRECTORY
    global FTP_TEST_STATUS_DIRECTORY
    global SNAKE_TEST_SHORT
    global SNAKE_TEST_DAILY
    global SNAKE_TEST_WEEKEND
    global SNAKE_TEST_EXTENDED
    global SNAKE_TEST_SHORT_DESCRIPTION
    global SNAKE_TEST_DAILY_DESCRIPTION
    global SNAKE_TEST_WEEKEND_DESCRIPTION
    global SNAKE_TEST_EXTENDED_DESCRIPTION

    FTP_USER = "ftpuser"
    FTP_PASSWORD = "ftpuser"
    CURRENT_RUN_FILE = "currentContinuousTestRun.txt"
    CURRENT_JOB_FILE = "current_job.txt"
    LOCAL_STATUS_DIRECTORY = "status"
    FTP_TEST_STATUS_DIRECTORY = "testRunStatus"

    SNAKE_TEST_SHORT = "00:01:00:00 noOfTimesToVerify 1"
    SNAKE_TEST_DAILY = "00:06:00:00 noOfTimesToVerify 1"
    SNAKE_TEST_WEEKEND = "00:60:00:00 noOfTimesToVerify 5"
    SNAKE_TEST_EXTENDED = "07:00:00:00 noOfTimesToVerify 14"

    SNAKE_TEST_SHORT_DESCRIPTION = "Test Run Length is 1 Hour Verifying 1 time"
    SNAKE_TEST_DAILY_DESCRIPTION = "Test Run Length is 6 Hours Verifying 1 times"
    SNAKE_TEST_WEEKEND_DESCRIPTION = "Test Run Length is 60 Hours Verifying 5 times"
    SNAKE_TEST_EXTENDED_DESCRIPTION = "Test Run Length is 7 Days Verifying 14 times"


