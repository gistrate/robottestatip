import sys
import os


outputBuffer = []


#
# exit with error is manner that Jenkins realizes the job was a failure
#
def quit_job():
    sys.exit(1)


def writeHeader(test_bucket):
    outputBuffer.append("")
    outputBuffer.append("")
    outputBuffer.append("#*************************************************************")

    head, tail = os.path.split(test_bucket)

    outputBuffer.append("# Test Specific Information: " + tail)
    outputBuffer.append("#*************************************************************")
    outputBuffer.append("")


def writeTest(current_test_id, parameters):
    global outputBuffer

    s = current_test_id.strip()

    outputBuffer.append("set runTest(" + s + ") \"yes\"")
    outputBuffer.append("set breakOnFailure(" + s + ") 0")
    if parameters.strip():
        outputBuffer.append("set testOptions(" + s + ") \"" + parameters + "\"")
    outputBuffer.append("")


def parse_svt(ixtester_directory):
    TEST_NAME_ID = "# ID: "
    TEST_PARAMETERS = "# PARAMETERS:"
    TEST_PARAMETERS_NONE = "# PARAMETERS: none"
    TEST_TAGS = "# TAGS: "

    test_count = 0

    testscripts_path = os.path.join(ixtester_directory, "testscripts", "nto")
    test_buckets = os.listdir(testscripts_path)
    for file in os.listdir(testscripts_path):
        if not file.endswith(".txt"):
            continue
        test_bucket = file.strip(".txt")
        file = os.path.join(ixtester_directory, "testscripts", "nto", file)

        in_test_definition_flag = False
        header_flag = True
        current_test_id = ""
        parameters = ""

        try:
            f = open(file, 'r')
            for line in f:
                line = line.rstrip()
                if in_test_definition_flag:
                    if TEST_NAME_ID in line:
                        current_test_id = line.replace(TEST_NAME_ID, "")
                    if TEST_TAGS in line:
                        if "SVT" not in line:
                            in_test_definition_flag = False
                            continue
                    if TEST_PARAMETERS in line:
                        parameters = line.replace(TEST_PARAMETERS_NONE, "")
                if "START TEST" in line:
                    in_test_definition_flag = True
                    parameters = ""
                if "END TEST" in line:
                    if in_test_definition_flag:
                        if header_flag:
                            writeHeader(test_bucket)
                            header_flag = False
                        writeTest(current_test_id, parameters)
                        test_count += 1
                        in_test_definition_flag = False
            f.close()
        except IOError:
            print("Could not open file: ", file)
    return test_count


def parse_nto(ixtester_directory, sub_dir, test_bucket):
    TEST_NAME_ID = "# ID: "
    TEST_PARAMETERS = "# PARAMETERS:"
    TEST_PARAMETERS_NONE = "# PARAMETERS: none"

    test_count = 0

    file = os.path.join(ixtester_directory, "testscripts", "nto", sub_dir, test_bucket + ".txt")
    print(file)
    if not os.path.exists(file):
        print("Test Bucket file " + file + " does not exist")
        quit_job()

    in_test_definition_flag = False
    header_flag = True
    current_test_id = ""
    parameters = ""

    try:
        f = open(file, 'r')
        for line in f:
            line = line.rstrip()
            if in_test_definition_flag:
                if TEST_NAME_ID in line:
                    current_test_id = line.replace(TEST_NAME_ID, "")
                if TEST_PARAMETERS in line:
                    parameters = line.replace(TEST_PARAMETERS_NONE, "")
            if "START TEST" in line:
                in_test_definition_flag = True
                parameters = ""
            if "END TEST" in line:
                if header_flag:
                    writeHeader(test_bucket)
                    header_flag = False
                writeTest(current_test_id, parameters)
                test_count += 1
                in_test_definition_flag = False
        f.close()
        return test_count
    except IOError:
        print("Could not open file: ", file)
        quit_job()


def parseSVTTestFile(ixtester_directory, test_bucket, output_file):
    global outputBuffer
    outputBuffer = []

    print("*************************************************************")

    print("INFO: Test Bucket: " + test_bucket)

    print("INFO: Output File: " + output_file)

    test_count = parse_svt(ixtester_directory)

    print("INFO: Number of Tests to be executed: " + str(test_count))
    print("*************************************************************")

    write_file = open(output_file, 'a')
    for line in outputBuffer:
        write_file.write(line + "\n")
    write_file.close()


def parseNTOTestFile(ixtester_directory, sub_dir, test_bucket, output_file):
    global outputBuffer
    outputBuffer = []

    print("*************************************************************")

    print("INFO: Test Bucket: " + test_bucket)

    print("INFO: Output File: " + output_file)

    test_count = parse_nto(ixtester_directory, sub_dir, test_bucket)

    print("INFO: Number of Tests to be executed: " + str(test_count))
    print("*************************************************************")

    write_file = open(output_file, 'a')
    for line in outputBuffer:
        write_file.write(line + "\n")
    write_file.close()


def writeTemplateFile(devices_directory, output_file):
    template_file = os.path.join(devices_directory, "Template.cfg")
    if not os.path.exists(template_file):
        print("Template config file " + template_file + " does not exist")
        quit_job()

    read_file = open(template_file)
    write_file = open(output_file, 'w')
    for line in read_file.readlines():
        write_file.write(line)
    read_file.close()
    write_file.close()


def addIxConfigFile(ix_config_file, output_file):
    read_file = open(ix_config_file)
    write_file = open(output_file, 'a')
    for line in read_file.readlines():
        write_file.write(line)
    read_file.close()
    write_file.close()


def createSnakeConfigFile(ix_config_file, output_file, snake_run):
    read_file = open(ix_config_file)
    write_file = open(output_file, 'w')
    for line in read_file.readlines():
        if "TEST_RUN_LENGTH" in line:
            line = line.replace("TEST_RUN_LENGTH", snake_run)
        write_file.write(line)
    read_file.close()
    write_file.close()


def addConfigParms(devices_directory, parallel_mode, output_file):
    setup_file = ""
    if parallel_mode:
        setup_file = os.path.join(devices_directory, "ParallelSetup.cfg")
    else:
        setup_file = os.path.join(devices_directory, "SequentialSetup.cfg")

    if not os.path.exists(setup_file):
        print("Setup config file " + setup_file + " does not exist")
        quit_job()

    read_file = open(setup_file)
    write_file = open(output_file, 'a')
    for line in read_file.readlines():
        write_file.write(line)
        read_file.close()
    write_file.close()


def add_override_parameters(overrides, output_file):
    if overrides:
        write_file = open(output_file, 'a')
        write_file.write(overrides.replace(";", "\n"))
        write_file.close()


def add_8K_overrides(devices_directory, output_file):
    setup_file = os.path.join(devices_directory, "8KSetup.cfg")
    if not os.path.exists(setup_file):
        print("Setup config file " + setup_file + " does not exist")
        quit_job()

    read_file = open(setup_file)
    write_file = open(output_file, 'a')
    for line in read_file.readlines():
        write_file.write(line)
        read_file.close()
    write_file.close()


def addUpgradePaths(upgrade_test, config_file):
    buffer = []
    config_fd = open(config_file)
    for line in config_fd.readlines():
        if "TESTRUNS_VARIABLE" in line:
            line = line.replace("TESTRUNS_VARIABLE", upgrade_test)
        buffer.append(line)
    config_fd.close()

    config_fd = open(config_file, 'w')
    for line in buffer:
        config_fd.write(line)
    config_fd.close()





