import threading
import os
import time
import argparse
from runTestSuite import execute_test_suite


class myThread(threading.Thread):
    def __init__(self, name, mode):
        threading.Thread.__init__(self)
        self.name = name
        self.mode = mode
    def run(self):
        run_parallel_job(self.name, self.mode)


def run_parallel_job(threadName, mode):
    test_type = threadName.split("@")[0]
    test_suite = threadName.split("@")[1]

    print("%s %s: %s" % ("INFO runSimultaneousTestSuite: Start", threadName, time.ctime(time.time())))
    execute_test_suite(test_type, test_suite, mode)
    print("%s %s: %s" % ("INFO runSimultaneousTestSuite: End", threadName, time.ctime(time.time())))


def execute_simultaneous_test_suite(parallelTestSuites, continuousMode):
    threads = []
    if continuousMode:
        print("INFO runSimultaneousTestSuite: ContinuousMode enabled")
    else:
        print("INFO runSimultaneousTestSuite: ContinuousMode disbled")

    testSuites = parallelTestSuites.split("/")
    # Create new threads
    for testSuite in testSuites:
        threads.append(myThread(testSuite, continuousMode))

    # Start new Threads
    for x in range(0, len(testSuites)):
        threads[x].start()

    # Wait for all threads to complete
    print("INFO runSimultaneousTestSuite: Wait for test suites to complete")
    for t in threads:
        t.join()
    print("INFO runSimultaneousTestSuite: test suites have completed")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--testSuiteCombo', required=True, action='store', dest='testSuiteCombo',
                        help='Test Suites to run simultaneously')
    results = parser.parse_args()

    execute_simultaneous_test_suite(results.testSuiteCombo, False)

