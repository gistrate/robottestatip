import os
import json
import requests
import time
import sys
import zipfile
import tarfile
import smtplib
import shutil
import xml.etree.ElementTree as ET

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def get_release_version_info(device):
    USER = "admin"
    PASSWD = "admin"
    PORT_WEBAPI = "8000"
    software_version = ""
    job = "https://" + device + ":" + PORT_WEBAPI + "/api/system?properties=software_version"
    try:
        ret = requests.get(job, auth=(USER, PASSWD), verify=False, timeout=120)
        if ret.status_code != 200:
            print(job, "return status", ret.status_code)
        else:
            jsonData = json.loads(ret.text)
            software_version = jsonData['software_version']
    except:
        print("ERROR: get_release_version_info Could not retrieve version information")

    return software_version


def create_dictionary_item(mac, type):
    global machineStatusList
    statusItem = {}

    statusItem['machine'] = mac
    statusItem['type'] = type
    statusItem["status"] = ""
    statusItem["revision"] = ""
    statusItem['testsRun'] = 0
    statusItem['testsFailed'] = 0
    statusItem['robotTestsRun'] = 0
    statusItem['robotTestsFailed'] = 0
    statusItem['robotGUITestsRun'] = 0
    statusItem['robotGUITestsFailed'] = 0

    machineStatusList.append(statusItem)


def get_test_results(zip_file):
    global reportBuffer
    global machineStatusList

    tmp_dir = "/tmp/nto_reports"
    shutil.rmtree(os.path.join(tmp_dir), ignore_errors=True)

    zip_ref = zipfile.ZipFile(zip_file, 'r')
    zip_ref.extractall(tmp_dir)
    zip_ref.close()

    #
    # test_type, ie 10G, 100G, Gibson_100G_AFMR is the name of the first directory
    # after the test_type is hardcode  directory "testHistory"
    # after "testHistory" is the name of the test bucket
    # 10G/testHistory/SVT
    #
    # os.listdir returns a list. the test_type is always the first entry in that list
    #
    test_type = os.listdir(tmp_dir)
    history_dir = os.path.join(tmp_dir, test_type[0], "testHistory")
    test_bucket = os.listdir(history_dir)
    test_bucket_dir = os.path.join(history_dir, test_bucket[0])
    xml_file = os.path.join(test_bucket_dir, "IxTester-Report.xml")
    if not os.path.exists(xml_file):
        print("ERROR: xml file: " + xml_file + " does not exists")
        shutil.rmtree(tmp_dir, ignore_errors=True)
        return

    tree = ET.parse(xml_file)
    root = tree.getroot()
    device_ip = root.attrib['device_ip']
    tests = int(root.attrib['tests'])
    failures = int(root.attrib['failures'])
    skipped = int(root.attrib['skipped'])
    test_bucket = root.attrib['name']
    build_identifier = root.attrib['hostname']
    passed = tests - (skipped + failures)
    device = "9999"

    for status_item in machineStatusList:
        machine = status_item['machine']
        if machine == device_ip:
            status_item['testsRun'] = status_item['testsRun'] + tests
            status_item['testsFailed'] = status_item['testsFailed'] + failures
            device = status_item['type']

    if failures > 0 and device != "9999":
        reportBuffer.append("<TR>")
        reportBuffer.append("<TD>" + test_bucket + "</TD>")
        reportBuffer.append("<TD>" + str(failures) + "</TD>")
        reportBuffer.append("<TD>" + device + "</TD>")
        reportBuffer.append("<TD>" + test_type[0] + "</TD>")
        reportBuffer.append("<TD>" + device_ip + "</TD>")
        reportBuffer.append("<TD>" + build_identifier + "</TD>")
        reportBuffer.append("</TR>")
    shutil.rmtree(tmp_dir, ignore_errors=True)


def get_robot_results(tar_file):
    global reportBuffer
    global machineStatusList

    tmp_dir = "/tmp/nto_reports"
    shutil.rmtree(tmp_dir, ignore_errors=True)

    tar_ref = tarfile.open(tar_file)
    tar_ref.extractall(tmp_dir)
    tar_ref.close()

    test_type = os.listdir(tmp_dir)[0]
    print(test_type)
    robot_dir = os.path.join(tmp_dir, test_type)
    robot_type = os.listdir(robot_dir)[0]
    print(robot_type)

    read_file = open(os.path.join(robot_dir, robot_type, "failed_count.txt"))
    for line in read_file.readlines():
        failed_count = line
    read_file.close()

    read_file = open(os.path.join(robot_dir, robot_type, "passed_count.txt"))
    for line in read_file.readlines():
        passed_count = line
    read_file.close()


def send_email():
    global htmlList
    global currentTime

    build_master = "build-master-0@keysight.com"
    mail_list = os.getenv("EMAIL_AUTOMATION_SUMMARY")
    mail_list = mail_list.split(",")

    mail_server = "smtp.cos.is.keysight.com"

    mail_msg = MIMEMultipart()

    mail_msg['Subject'] = "Status report for " + currentTime

    mail_msg['From'] = build_master
    mail_msg['To'] = ", ".join(mail_list)

    mail_msg.attach(MIMEText(''.join(htmlList), 'html'))

    s = smtplib.SMTP(mail_server)
    s.sendmail(build_master, mail_list, mail_msg.as_string())

    s.quit()


def create_summary_report():
    global htmlList
    global currentTime

    htmlList.append("<STYLE>")
    htmlList.append("  a {TEXT-DECORATION: none;}")
    htmlList.append("  table, th, td {")
    htmlList.append("     border: 1px solid black;")
    htmlList.append("     border-collapse: collapse;")
    htmlList.append("     padding: 3px;")
    htmlList.append("  }")
    htmlList.append("</STYLE>")
    htmlList.append("<H3 ALIGN=LEFT>Daily Automation Summary Report for: " + currentTime + "</H3>")
    htmlList.append("<TABLE>")

    htmlList.append("<TR>")
    htmlList.append("<TH ROWSPAN=2>Device<BR>Type</TH>")
    htmlList.append("<TH ROWSPAN=2>IP<BR>Address</TH>")
    htmlList.append("<TH COLSPAN=2>Automation</TH>")
    htmlList.append("<TH COLSPAN=2>ROBOT</TH>")
    htmlList.append("<TH COLSPAN=2>ROBOT GUI</TH>")
    htmlList.append("<TH ROWSPAN=2>Current Status</TH>")
    htmlList.append("</TR>")

    htmlList.append("<TR>")
    htmlList.append("<TH>Tests Run</TH>")
    htmlList.append("<TH>Failures</TH>")
    htmlList.append("<TH>Tests Run</TH>")
    htmlList.append("<TH>Failures</TH>")
    htmlList.append("<TH>Tests Run</TH>")
    htmlList.append("<TH>Failures</TH>")
    htmlList.append("</TR>")
    for machine in machineStatusList:
        htmlList.append("<TR>")
        htmlList.append("<TD>" + machine['type'] + "</TD>")
        htmlList.append("<TD>" + machine['machine'] + "</TD>")

        htmlList.append("<TD ALIGN=CENTER>" + str(machine['testsRun']) + "</TD>")
        htmlList.append("<TD ALIGN=CENTER>" + str(machine['testsFailed']) + "</TD>")
        htmlList.append("<TD ALIGN=CENTER>" + str(machine['robotTestsRun']) + "</TD>")
        htmlList.append("<TD ALIGN=CENTER>" + str(machine['robotTestsFailed']) + "</TD>")
        htmlList.append("<TD ALIGN=CENTER>" + str(machine['robotGUITestsRun']) + "</TD>")
        htmlList.append("<TD ALIGN=CENTER>" + str(machine['robotGUITestsFailed']) + "</TD>")

        if machine['status'] != "active":
            htmlList.append("<TD>" + machine['status'] + "</TD>")
        else:
            htmlList.append("<TD>" + machine['revision'] + "</TD>")
        htmlList.append("</TR>")

    htmlList.append("</TABLE><BR><BR>")


##########################################################################################
#
# main routine
#
##########################################################################################

machineStatusList = []
htmlList = []
reportBuffer = []

dailyStatusDirectory = "/home/ftpuser/reports/dailyStatus"
currentTime = time.strftime("%Y-%m-%d %H:%M")

create_dictionary_item("10.36.168.118", "Control Tower")
create_dictionary_item("10.36.168.81", "5288")
create_dictionary_item("10.36.168.66", "5812")
create_dictionary_item("10.36.169.83", "6212")
create_dictionary_item("10.36.168.161", "6322")
create_dictionary_item("10.36.168.169", "6322")
create_dictionary_item("10.36.148.16", "6323")
create_dictionary_item("10.36.168.226", "7300")
create_dictionary_item("10.36.168.65", "7300")
create_dictionary_item("10.36.168.201", "7300")
create_dictionary_item("10.36.168.212", "7300")
create_dictionary_item("10.36.168.49", "7712")
create_dictionary_item("10.36.168.50", "E40")
create_dictionary_item("10.36.168.56", "E100")
create_dictionary_item("10.36.168.69", "E100")
create_dictionary_item("10.36.168.86", "E100")

for machine in machineStatusList:
    ping_command = "ping -n 1 -w 2000 " + machine['machine'] + " 2>NUL 1>&2"
    ping_command = "ping -c 1 -W 2 " + machine['machine'] + " 2>/dev/null 1>&2"

    ret_status = os.system(ping_command)
    if ret_status == 0:
        machine['status'] = "pingable"
    else:
        machine['status'] = "not pingable"

for machine in machineStatusList:
    if machine['status'] == "pingable":
        release = get_release_version_info(machine['machine'])
        if release:
            machine['status'] = "active"
            machine['revision'] = release
        else:
            machine['status'] = "pingable/no webapi response"

#
# check all releases directory under the daily status directory for zip/tar files
#
for release in os.listdir(dailyStatusDirectory):
    releaseDir = os.path.join(dailyStatusDirectory, release)
    if os.path.isdir(releaseDir):

        #
        # check all device type directories under the release directory
        #
        for device in sorted(os.listdir(releaseDir)):
            deviceDirectory = os.path.join(releaseDir, device)
            if os.path.isdir(deviceDirectory):

                #
                # check device type directory for zip files containing new test results
                #
                for archiveFile in sorted(os.listdir(deviceDirectory)):
                    if archiveFile.endswith('.zip'):
                        zipFile = os.path.join(deviceDirectory, archiveFile)
                        get_test_results(zipFile)
                        if os.path.exists(zipFile):
                            os.remove(zipFile)
                    if archiveFile.endswith('.tar'):
                        tarFile = os.path.join(deviceDirectory, archiveFile)
                        get_robot_results(tarFile)
                        if os.path.exists(tarFile):
                            os.remove(tarFile)


create_summary_report()

htmlList.append("<H3 ALIGN=LEFT>Test Failure Listing for: " + currentTime + "</H3>")
htmlList.append("<TABLE>")
htmlList.append("<TR>")
htmlList.append("<TH>Test Bucket</TH>")
htmlList.append("<TH>Failure Count</TH>")
htmlList.append("<TH>Device</TH>")
htmlList.append("<TH>Test Type</TH>")
htmlList.append("<TH>Device IP</TH>")
htmlList.append("<TH>Build ID</TH>")
htmlList.append("</TR>")

for line in reportBuffer:
    htmlList.append(line)
htmlList.append("</TABLE>")

send_email()
