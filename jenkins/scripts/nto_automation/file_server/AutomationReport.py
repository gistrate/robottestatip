import shutil
import os
import zipfile
import tarfile
import smtplib
import argparse
import xml.etree.ElementTree as ET

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


#
# create  HH:MM:SS time string
#
def display_time(time_in_seconds):

    #
    # get number of hours in time_in_seconds
    #
    hours = time_in_seconds // 3600

    minutes_remainder_in_seconds = time_in_seconds - (hours * 3600)
    minutes = minutes_remainder_in_seconds // 60

    seconds = minutes_remainder_in_seconds - (minutes * 60)

    value = "%#02d" % hours + ":" + "%#02d" % minutes + ":" + "%#02d" % seconds
    return value


#
# convert HH:MM:SS to total seconds
#
def convert_time(overall_time, time_value):
    times = time_value.split(":")
    hours = int(times[0])
    minutes = int(times[1])
    seconds = int(times[2])
    value = (hours * 3600) + (minutes * 60) + seconds
    new_time = overall_time + value
    return new_time


#
# create an html link to the individual test bucket test results
#
def create_bucket_link(release, device, test_type, test_bucket):
    link = "<a href=\"" + http_server_system_test + "/" + release + "/" + device + "/" + \
           test_type + "/testHistory/" + test_bucket + "/index.html\">" + test_bucket + "</a>"
    return link


def create_header(test_name):
    global report_buffer

    report_buffer.append("<html>")
    report_buffer.append("<head>")
    report_buffer.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">")
    report_buffer.append("<title>" + test_name + "</title>")
    report_buffer.append("<style type=\"text/css\">")
    report_buffer.append("  body { font:normal 68% verdana,arial,helvetica; color:#000000; }")
    report_buffer.append("  table tr td, table tr th { font-size: 68%; }")
    report_buffer.append("  table.details tr th { font-weight: bold; text-align:left; background:#a6caf0; }")
    report_buffer.append("  table.details tr td{ background:#eeeee0; }")
    report_buffer.append("  p { line-height:1.5em; margin-top:0.5em; margin-bottom:1.0em; }")
    report_buffer.append("  h1 { margin: 0px 0px 5px; font: 165% verdana,arial,helvetica }")
    report_buffer.append("  h2 { margin-top: 1em; margin-bottom: 0.5em; font: bold 125% verdana,arial,helvetica }")
    report_buffer.append("  h3 { margin-bottom: 0.5em; font: bold 115% verdana,arial,helvetica }")
    report_buffer.append("  .Error { font-weight:bold; color:red; }")
    report_buffer.append("</style>")
    report_buffer.append("</head>")
    report_buffer.append("<body>")


def create_test_summary(tests, failures, skipped, time):
    global report_buffer

    passed = tests - (skipped + failures)
    try:
        rate = (passed / (tests - skipped)) * 100
    except:
        rate = 0

    report_buffer.append("<hr size=\"1\">")
    report_buffer.append("<h2>Summary</h2>")
    report_buffer.append("<table class=\"details\" width=\"95%\" cellspacing=\"2\" cellpadding=\"5\" border=\"0\"><tbody>")
    report_buffer.append("<tr valign=\"top\">")
    report_buffer.append("<th>Tests</th>")
    report_buffer.append("<th>Success</th>")
    report_buffer.append("<th>Failures</th>")
    report_buffer.append("<th>Skipped</th>")
    report_buffer.append("<th>Success rate</th>")
    report_buffer.append("<th>Time</th>")
    report_buffer.append("</tr>")
    report_buffer.append("<tr valign=\"top\">")
    report_buffer.append("<td>" + str(tests) + "</td>")
    report_buffer.append("<td>" + str(passed) + "</td>")
    report_buffer.append("<td>" + str(failures) + "</td>")
    report_buffer.append("<td>" + str(skipped) + "</td>")
    report_buffer.append("<td>" + "%.2f" % rate + "%</td>")
    report_buffer.append("<td>" + time + "</td>")
    report_buffer.append("</tr>")
    report_buffer.append("</tbody></table>")


def create_test_run_info(release, device, test_type):
    global report_buffer
    global http_server_system_test

    report_buffer.append("<h1 align=center>Device: " + device +
                         "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Release: " +
                         release +
                         "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Test Type: " +
                         test_type + "</h1>")


def create_test_type_report(test_type_directory):
    global report_buffer

    print("Create Test Type Report " + test_type_directory)
    overall_tests = 0
    overall_passed = 0
    overall_failures = 0
    overall_skipped = 0
    time = 0

    report_buffer = []
    detail_buffer = []
    xml_buffer = []

    path = os.path.normpath(test_type_directory)
    dirs = path.split(os.sep)
    test_type = dirs[len(dirs) - 1]
    device = dirs[len(dirs) - 2]
    release = dirs[len(dirs) - 3]

    detail_buffer.append("<hr size=\"1\">")
    detail_buffer.append("<h3>Test Runs</h3>")
    detail_buffer.append("<table class=\"details\" width=\"95%\" cellspacing=\"2\" cellpadding=\"5\" border=\"0\"><tbody>")
    detail_buffer.append("<tr valign=\"top\">")
    detail_buffer.append("<th  width=\"60%\">Name</th>")
    detail_buffer.append("<th>Tests</th>")
    detail_buffer.append("<th>Success</th>")
    detail_buffer.append("<th>Failures</th>")
    detail_buffer.append("<th>Skipped</th>")
    detail_buffer.append("<th>Build Identifier</th>")
    detail_buffer.append("<th>Time</th>")
    detail_buffer.append("</tr>")

    if not os.path.exists(os.path.join(test_type_directory, "testHistory")):
        return
    for test_runs in sorted(os.listdir(os.path.join(test_type_directory, "testHistory"))):
        test_run = os.path.join(test_type_directory, "testHistory", test_runs)
        xml_file = os.path.join(test_run, "IxTester-Report.xml")
        if not os.path.exists(xml_file):
            continue
        tree = ET.parse(xml_file)
        root = tree.getroot()
        test_bucket = root.attrib['name']
        tests = int(root.attrib['tests'])
        failures = int(root.attrib['failures'])
        skipped = int(root.attrib['skipped'])
        passed = tests - (skipped + failures)
        time = convert_time(time, root.attrib['time'])
        build_id = root.attrib['hostname']

        overall_tests = overall_tests + tests
        overall_passed = overall_passed + passed
        overall_failures = overall_failures + failures
        overall_skipped = overall_skipped + skipped
        detail_buffer.append("<tr>")
        detail_buffer.append("<td>" + create_bucket_link(release, device, test_type, test_bucket) + "</td>")
        detail_buffer.append("<td>" + str(tests) + "</td>")
        detail_buffer.append("<td>" + str(passed) + "</td>")
        detail_buffer.append("<td>" + str(failures) + "</td>")
        detail_buffer.append("<td>" + str(skipped) + "</td>")
        detail_buffer.append("<td>" + build_id + "</td>")
        detail_buffer.append("<td>" + root.attrib['time'] + "</td>")

        xml_buffer.append("<testsuite tests=\"" + str(tests) + "\" failures=\"" + str(failures) +
                          "\" skipped=\"" + str(skipped) +
                          "\" name=\"" + test_bucket + "\">")
        xml_buffer.append("</testsuite>\n")

    if overall_tests > 0:
        create_header(test_type_directory)
        create_test_run_info(release, device, test_type)
        create_test_summary(overall_tests, overall_failures, overall_skipped, display_time(time))

        tmp_index_file = os.path.join(test_type_directory, "testHistory", "index.html")
        tmp_xml_file = os.path.join(test_type_directory, "testHistory", "TESTS-TestSuites.xml")

        try:
            write_file = open(tmp_index_file, 'w')
            for line in report_buffer:
                write_file.write(line + "\n")
            for line in detail_buffer:
                write_file.write(line + "\n")
            write_file.close()
        except:
            print("Failure to create: " + tmp_index_file)

        try:
            write_file = open(tmp_xml_file, "w")
            write_file.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
            write_file.write("<testsuites>\n")
            for line in xml_buffer:
                write_file.write(line + "\n")
            write_file.write("</testsuites>\n")
            write_file.close()
        except:
            print("Failure to create: " + tmp_xml_file)


def extract_zip_file(device_directory, zip_file):
    print("Create Test Bucket Report from zip file: " + zip_file)
    zip_ref = zipfile.ZipFile(zip_file, 'r')
    zip_ref.extractall(device_directory)
    zip_ref.close()


def extract_tar_file(device_directory, tar_file):
    print("Create Test Bucket Report from tar file: " + tar_file)
    tar_ref = tarfile.open(tar_file)
    tar_ref.extractall(device_directory)
    tar_ref.close()


def print_test_bucket_info(test_name, release, device, test_type, run_date, build_identifier):
    global report_buffer
    global http_server_system_test

    report_buffer.append("<h1 align=center>" + test_name + "</h1>")
    report_buffer.append("<hr size=\"1\">")
    report_buffer.append("<table width=\"100%\"><tbody>")
    report_buffer.append("<tr>")
    report_buffer.append("<td width=\"10%\"><h2>Device: " + device + "</h2></td>")
    report_buffer.append("<td width=\"15%\"><h2>Test Run: " + test_type + "</h2></td>")
    report_buffer.append("<td width=\"20%\"><h2>Run Date: " + run_date + "</h2></td>")
    report_buffer.append("<td width=\"25%\"><h2>Build Identifier: " + build_identifier + "</h2></td>")
    report_buffer.append("<td width=\"10%\"><h2><a href=\"" + http_server_system_test + "/" + release + "/"
                              + device + "/" + test_type + "/testHistory/" + test_name + "/files/logFiles.zip"
                              + "\"target=\"_blank\">Log Files</a></h2></td>")
    report_buffer.append("<td width=\"10%\"><h2><a href=\"" + http_server_system_test + "/" + release + "/"
                              + device + "/" + test_type + "/testHistory/" + test_name + "/files/BRDebugFiles.zip"
                              + "\"target=\"_blank\">BRDebug Files</a></h2></td>")
    report_buffer.append("<td width=\"10%\"><h2><a href=\"" + http_server_system_test + "/" + release + "/"
                              + device + "/" + test_type + "/testHistory/" + test_name + "/files/AutoSysTests.cfg"
                              + "\"target=\"_blank\">Config File</a></h2></td>")
    report_buffer.append("</tr>")
    report_buffer.append("</tbody></table>")


def print_test_bucket_test_cases(root):
    global report_buffer

    report_buffer.append("<h3>Test Cases</h3>")
    report_buffer.append("<table class=\"details\" width=\"95%\" cellspacing=\"2\" cellpadding=\"5\" border=\"0\"><tbody>")
    report_buffer.append("<tr valign=\"top\">")
    report_buffer.append("<th>Name</th>")
    report_buffer.append("<th>Status</th>")
    report_buffer.append("<th width=\"60%\">Message</th>")
    report_buffer.append("<th>Comments</th>")
    report_buffer.append("<th nowrap=\"nowrap\">Time(s)</th>")
    report_buffer.append("</tr>")

    for child in root:
        error_msg = child.find('error')
        if error_msg is not None:
            report_buffer.append("<tr valign=\"top\"  class=\"Error\">")
            report_buffer.append("<td>" + child.attrib["name"] + "</td>")
            report_buffer.append("<td>Error</td>")
            report_buffer.append("<td>" + error_msg.text + "</td>")
        else:
            report_buffer.append("<tr valign=\"top\">")
            report_buffer.append("<td>" + child.attrib["name"] + "</td>")
            report_buffer.append("<td>Success</td>")
            report_buffer.append("<td></td>")
        report_buffer.append("<td></td>")
        report_buffer.append("<td>" + child.attrib["time"] + "</td>")
        report_buffer.append("</tr>")

    report_buffer.append("</tbody></table>")
    report_buffer.append("</body>")
    report_buffer.append("</html>")


def test_bucket_error_check_and_report(release, device, device_directory, zip_file):
    global report_buffer

    report_buffer = []

    tmp_dir = "/tmp/nto_reports"
    shutil.rmtree(os.path.join(tmp_dir), ignore_errors=True)

    zip_ref = zipfile.ZipFile(zip_file, 'r')
    zip_ref.extractall(tmp_dir)
    zip_ref.close()

    test_type = os.listdir(tmp_dir)
    history_dir = os.path.join(tmp_dir, test_type[0], "testHistory")
    test_bucket = os.listdir(history_dir)
    test_bucket_dir = os.path.join(history_dir, test_bucket[0])
    xml_file = os.path.join(test_bucket_dir, "IxTester-Report.xml")
    if not os.path.exists(xml_file):
        print("ERROR: xml file: " + xml_file + " does not exists")
        shutil.rmtree(os.path.join(tmp_dir), ignore_errors=True)
        os.remove(zip_file)
        return

    tree = ET.parse(xml_file)
    root = tree.getroot()
    tests = int(root.attrib['tests'])
    failures = int(root.attrib['failures'])
    skipped = int(root.attrib['skipped'])
    passed = tests - (skipped + failures)
    test_bucket = root.attrib['name']
    run_date = root.attrib['date']
    build_identifier = root.attrib['hostname']

    time = root.attrib['time']
    times = time.split(":")
    time = times[0] + "h:" + times[1] + "m:" + times[2] + "s"
    if failures > 0:
        subject = "Test failures running " + test_bucket + " on Device " + device + " against build " + build_identifier
        body = "Test failures running <B>" + test_bucket + "</B> on Device " + device + " against build " + build_identifier
        body = body + "<BR><BR>"
        body = body + "Test Bucket: <B>" + test_bucket + "</B>&nbsp;&nbsp;&nbsp;&nbsp;Device: <B>" + device + "</B>&nbsp;&nbsp;&nbsp;&nbsp;Test Type: <B>" + test_type[0] + "</B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Release: <B>" + release + "</B><BR><BR>"

        body = body + "Tests: " + str(tests) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Passed: " + str(passed) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Failures: " + str(failures) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Skipped: " + str(skipped) + "<BR><BR>"
        body = body + "Test Results: " + http_server_system_test + "/" + release + "/" + device + "/" + test_type[0] + "/testHistory/" + test_bucket + "/index.html<BR>"

        build_master = "nto-automation@keysight.com"
        mail_list = os.getenv("EMAIL_NTO_4X_AUTOMATION")
        mail_list = mail_list.split(",")

        mail_server = "smtp.cos.is.keysight.com"

        msg = MIMEMultipart()

        msg['Subject'] = subject
        msg['From'] = build_master
        msg['To'] = ", ".join(mail_list)
        msg.attach(MIMEText(body, 'html'))

        s = smtplib.SMTP(mail_server)
        s.sendmail(build_master, mail_list, msg.as_string())
        s.quit()

    create_header(test_bucket)
    print_test_bucket_info(test_bucket, release, device, test_type[0], run_date, build_identifier)
    create_test_summary(tests, failures, skipped, time)

    print_test_bucket_test_cases(root)
    tmp_index_file = os.path.join(test_bucket_dir, "index.html")
    save_index_file = os.path.join(device_directory, test_type[0], "testHistory", test_bucket, "index.html")

    try:
        write_file = open(tmp_index_file, 'w')
        for line in report_buffer:
            write_file.write(line + "\n")
        write_file.close()
    except:
        None

    shutil.copy(tmp_index_file, save_index_file)
    shutil.rmtree(os.path.join(tmp_dir), ignore_errors=True)

    if os.path.exists(zip_file):
        os.remove(zip_file)


###########################################################################
#
# main routine
#
###########################################################################

parser = argparse.ArgumentParser()
parser.add_argument('--rebuildAll', dest="rebuildAll",
                    help='rebuild all reports', action='store_true')
results = parser.parse_args()

SVN_USER = os.getenv("SVN_USER")
SVN_PASSWORD = os.getenv("SVN_PASSWORD")
svn_command = "svn update --username " + SVN_USER + " --password " + SVN_PASSWORD + " /home/ftpuser/agentSetup/svn/IxTester/nto"
print("svn update /home/ftpuser/agentSetup/svn/IxTester/nto")
os.system(svn_command)

systemReportsDir = "/home/ftpuser/reports/systemTest"

FILESERVER_SYSTEM = os.getenv("FILESERVER_SYSTEM")

report_buffer = []
http_server_system_test = "http://" + FILESERVER_SYSTEM + "/reports/systemTest"

#
# check all releases directories under the system test directory looking for zip/tar files that contain test results
#
for release in os.listdir(systemReportsDir):
    release_archive_file_found = False
    releaseDirectory = os.path.join(systemReportsDir, release)
    if os.path.isdir(releaseDirectory):

        #
        # check all device type directories under the release directory
        #
        for device in os.listdir(releaseDirectory):
            if results.rebuildAll:
                device_zip_file_found = True
            else:
                device_zip_file_found = False
            deviceDirectory = os.path.join(releaseDirectory, device)
            if os.path.isdir(deviceDirectory):

                #
                # check device type directory for zip files containing new test results
                #
                for archiveFile in os.listdir(deviceDirectory):
                    if archiveFile.endswith('.zip'):
                        device_zip_file_found = True
                        release_archive_file_found = True
                        zipFile = os.path.join(deviceDirectory, archiveFile)
                        extract_zip_file(deviceDirectory, zipFile)
                        test_bucket_error_check_and_report(release, device, deviceDirectory, zipFile)
                    if archiveFile.endswith('.tar'):
                        device_tar_file_found = True
                        release_archive_file_found = True
                        tarFile = os.path.join(deviceDirectory, archiveFile)
                        extract_tar_file(deviceDirectory, tarFile)

                #
                # If there was a zip file found - recreate all automation test_type reports for that device type
                #
                if device_zip_file_found:
                    for testType in os.listdir(deviceDirectory):
                        testTypeDirectory = os.path.join(deviceDirectory, testType)
                        if os.path.isdir(testTypeDirectory):
                            create_test_type_report(testTypeDirectory)
