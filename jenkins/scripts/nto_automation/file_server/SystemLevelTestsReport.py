import os
import argparse

####################################################################################
#
# create index.html file from multiple csv files for the SystemLevelTests
#
# each csv file represents one SystemLevelTest release-to-release
#
# index.html is a overall report all the release-to-release tests
#
#####################################################################################


def fix_up_description():
    global csv_buffer

    for i in range(len(csv_buffer[0])):
        if i == 0:
            csv_buffer[0][0] = "<TD BGCOLOR=#999990>&nbsp;</TD>"
        else:
            temp = csv_buffer[0][i]
            csv_buffer[0][i] = "<TD BGCOLOR=#eeeee0>" + str(i) + " - " + temp + "</TD>"


def fix_up_header():
    global csv_buffer
    global test_count

    for i in range(len(csv_buffer)):
        if i > 0:
            buffer = csv_buffer[i][0]
            buffer = buffer.replace("fromBuild:", "<TD BGCOLOR=#999990 ALIGN=CENTER>From: ")
            buffer = buffer.replace("toBuild:", "<BR>To: ")
            buffer = buffer + "</TD>"
            csv_buffer[i][0] = buffer


def fix_up_tests(release, device, test_type, index, buffer):
    new_buffer = "<TD ALIGN=CENTER>&nbsp;<FONT COLOR=BLACK>" + buffer + "</FONT></TD>"

    if buffer == "PASSED":
        new_buffer = "<TD ALIGN=CENTER><FONT COLOR=#3CBC3C>PASSED</FONT></TD>"
    if buffer == "SKIPPED":
        new_buffer = "<TD ALIGN=CENTER><FONT COLOR=BLUE>SKIPPED</FONT></TD>"
    if buffer == "FAILED":
        URL = "http://build-filesrv-0.ann.is.keysight.com/reports/systemTest/" + release + "/" + device + "/" + \
              test_type + "/testHistory/SystemLevelTests/IxTester-Report_" + str(index) + ".xml"
        new_buffer = "<TD ALIGN=CENTER><A HREF=\"" + URL + "\"><FONT COLOR=RED><B>FAILED</B></FONT></A></TD>"
    return new_buffer


def read_csv_file(file_name):
    global csv_buffer
    global description_found
    global test_count;

    try:
        f = open(file_name, 'r')
        for line in f:
            line = line.rstrip()
            if line.strip():
                if line.startswith("upgradePaths"):
                    if not description_found:
                        description_found = True
                        x = line.split(",")
                        test_count = len(x)
                        csv_buffer.append(x)
                else:
                    x = line[:-1].split(",")
                    if len(x) < test_count:
                        for i in range(test_count - len(x)):
                            x.append("")
                    csv_buffer.append(x)
        f.close()
    except IOError:
        print("Could not open file: ", file_name)


def write_html_file(csv_directory, html_list):

    report_file = os.path.join(csv_directory, "index.html")

    try:
        write_file = open(report_file, 'w')
        for line in html_list:
            write_file.write(line + "\n")
        write_file.close()
    except:
        None


def system_test_report(release, device, test_type, report_directory):
    global test_count
    global csv_buffer
    global html_buffer

    if not os.path.exists(report_directory):
        print("No SystemLevelTests report directory")
        return

    csv_file_found = False
    for csv_file in sorted(os.listdir(report_directory)):
        if csv_file.endswith('.csv'):
            read_csv_file(os.path.join(report_directory, csv_file))
            csv_file_found = True

    if not csv_file_found:
        print("No csv input files")
        return

    fix_up_description()

    fix_up_header()

    html_buffer.append("<HTML>")
    html_buffer.append("<HEAD>")
    html_buffer.append("<STYLE>")
    html_buffer.append("  a {TEXT-DECORATION: none;}")
    html_buffer.append("  table, th, td {")
    html_buffer.append("     border: 1px solid black;")
    html_buffer.append("     border-collapse: collapse;")
    html_buffer.append("  }")
    html_buffer.append("</STYLE>")
    html_buffer.append("</HEAD>")
    html_buffer.append("<BODY>")
    html_buffer.append("<H2 ALIGN=CENTER>System Level Test Report</H2>")
    html_buffer.append("<TABLE CELLSPACING=2 CELLPADDING=2 ALIGN=CENTER WIDTH=90%>")
    for i in range(test_count):
        html_buffer.append("<TR>")
        for j in range(len(csv_buffer)):
            buffer = csv_buffer[j][i]
            if buffer.startswith("<TD"):
                html_buffer.append(buffer)
            else:
                html_buffer.append(fix_up_tests(release, device, test_type, j, buffer))
        html_buffer.append("</TR>")
    html_buffer.append("</TABLE>")

    write_html_file(report_directory, html_buffer)


test_count = 0;
csv_buffer = []
description_found = False
html_buffer = []

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--reportDirectory', required=True, action='store', dest='reportDirectory',
                        help='Report Directory containing csv files')
    results = parser.parse_args()

    #
    # check all releases directories under the system test directory looking for zip/tar files that contain test results
    #
    for release in os.listdir(results.reportDirectory):
        releaseDirectory = os.path.join(results.reportDirectory, release)
        if os.path.isdir(releaseDirectory):

            #
            # check all device type directories under the release directory
            #
            for device in os.listdir(releaseDirectory):
                deviceDirectory = os.path.join(releaseDirectory, device)
                if os.path.isdir(deviceDirectory):
                    for testType in os.listdir(deviceDirectory):
                        systemLevelTestsDirectory = os.path.join(deviceDirectory, testType, "testHistory", "SystemLevelTests")
                        if os.path.isdir(systemLevelTestsDirectory):
                            for files in os.listdir(systemLevelTestsDirectory):
                                if files.endswith('.csv'):
                                    test_count = 0;
                                    html_buffer = []
                                    csv_buffer = []
                                    description_found = False
                                    system_test_report(release, device, testType, systemLevelTestsDirectory)
                                    break

