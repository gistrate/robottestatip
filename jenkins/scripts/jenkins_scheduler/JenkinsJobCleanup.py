import requests
import json
import sys
import argparse
import os


############################################################################################################
#
# FUNCTION exit with error is manner that Jenkins realizes the job was a failure
#
############################################################################################################

def quit_job():
    sys.exit(1)


############################################################################################################
#
# FUNCTION read file containing Jenkins user
#
############################################################################################################

def read_jenkins_user():
    try:
        f = open(userFile, 'r')
        line = f.readline()
        user = line.rstrip()
        f.close()
        return user
    except IOError:
        print("ERROR: Could not open user file: " + userFile)
        quit_job()


############################################################################################################
#
# FUNCTION read file containing Jenkins password
#
############################################################################################################

def read_jenkins_password():
    try:
        f = open(passwordFile, 'r')
        line = f.readline()
        password = line.rstrip()
        f.close()
        return password
    except IOError:
        print("ERROR: Could not open password file: " + passwordFile)
        quit_job()


############################################################################################################
#
# FUNCTION read list of related Jenkins jobs
#
############################################################################################################

def read_jenkins_jobs_file(job_list_directory, device):
    #
    # read in list of Jenkins jobs associated with this NTO
    # and place the list into a string array
    #
    filename = os.path.join(job_list_directory, device + ".txt")
    try:
        f = open(filename, 'r')
        for line in f:
            line = line.rstrip()
            if line.strip():
                lines.append(line)
        f.close()
    except:
        print("ERROR: read_jenkins_jobs_file: Could read file: " + filename)
        quit_job()


############################################################################################################
#
# FUNCTION cancel all related Jenkins jobs that are in the job queue
#
############################################################################################################

def cancel_jobs_in_queue(jenkins_user, jenkins_password):

    print("INFO: cancel jobs in job queue")

    #
    # Call Jenkins webapi to get job queue
    #
    ret = requests.get(jobQueue)
    if ret.status_code != 200:
        print("ERROR: cancel_jobs_in_queue " + jobQueue + " return status" + ret.status_code)
        quit_job()

    #
    # load the JSON data from the webapi call
    #
    jsonData = json.loads(ret.text)

    #
    # determine if a NTO job is the queue
    #
    print("INFO: cancel jobs in queue")
    for item in jsonData['items']:
        job = item['task']['name']
        jobID = item['id']
        if job in lines:
            #
            # job in queue - cancel
            #
            print(job, "in Queue")
            cancelItem = jenkinsURL + cancelJob + str(jobID)
            print(cancelItem)
            status = requests.post(cancelItem, auth=(jenkins_user, jenkins_password))
            print(status)


############################################################################################################
#
# FUNCTION terminate related Jenkins jobs that are currently running
#
############################################################################################################

def terminate_running_jobs(jenkins_user, jenkins_password):

    print("INFO: terminate running jobs")
    for line in lines:
        #
        # Call Jenkins webapi to get last build of the job
        #
        runningJob = runningJobHeader + line + runningJobTrailer
        ret = requests.get(runningJob)
        if ret.status_code != 200:
            print(runningJob, "return status", ret.status_code)
            quit_job()

        #
        # load the JSON data from the webapi call
        #
        jsonData = json.loads(ret.text)

        #
        # query the building element to see if the job is running
        #
        running = jsonData['building']
        if running:
            #
            # job is currently running - terminate
            #
            jobID = jsonData['id']
            print(line, jobID, "is currently running")
            stopItem = jenkinsURL + "job/" + line + "/" + jobID + stopJob
            print(stopItem)
            status = requests.post(stopItem, auth=(jenkins_user, jenkins_password))
            print(status)


userFile = "/home/jenkins/.user.txt"
passwordFile = "/home/jenkins/.password.txt"

jenkinsURL = "http://build-master-0.ann.is.keysight.com:8080/"

jobQueue = jenkinsURL + "queue/api/json"

runningJobHeader = jenkinsURL + "job/"
runningJobTrailer = "/lastBuild/api/json?depth=1"

cancelJob = "queue/cancelItem?id="
stopJob = "/stop"

lines = []

parser = argparse.ArgumentParser()
parser.add_argument('--jobListDir', required=True, action='store', dest='jobListDir',
                    help='Jenkins job list configuration directory')
parser.add_argument('--device', required=True, action='store', dest='device', help='Device IP address')
results = parser.parse_args()

jenkinsUser = read_jenkins_user()
jenkinsPassword = read_jenkins_password()

read_jenkins_jobs_file(results.jobListDir, results.device)

cancel_jobs_in_queue(jenkinsUser, jenkinsPassword)

terminate_running_jobs(jenkinsUser, jenkinsPassword)

