import requests
import json
import sys
import os
import argparse

JENKINS_GET_URL = "http://build-master-0.ann.is.keysight.com:8080/"

jobQueue = JENKINS_GET_URL + "queue/api/json"

JENKINS_RUNNING_JOB_HEADER = JENKINS_GET_URL + "job/"
JENKINS_RUNNING_JOB_TRAILER = "/lastBuild/api/json?depth=1"

lines = []

############################################################################################################
#
# FUNCTION get and print job description
#
############################################################################################################
def jobName(name, description):
    job = JENKINS_GET_URL + "job/" +name + "/api/json"
    ret = requests.get(job)
    if ret.status_code != 200:
        print(job, "return status", ret.status_code)
        quit()
    jsonData = json.loads(ret.text)
    print(jsonData['displayName'], description)

############################################################################################################
#
# FUNCTION determine if jobs are in the job queue
#
############################################################################################################

def jenkins_job_queue():
    #
    # Call Jenkins webapi to get job queue
    #
    ret = requests.get(jobQueue)
    if ret.status_code != 200:
        print(jobQueue, "return status", ret.status_code)
        quit()

    #
    # load the JSON data from the webapi call
    #
    jsonData = json.loads(ret.text)

    #
    # determine if a NTO job is the queue
    #

    for item in jsonData['items']:
        job = item['task']['name']
        jobID = item['id']
        if job in lines:
            #
            # job in queue - cancel
            #
            jobName(job, "in Queue")


############################################################################################################
#
# FUNCTION determine if jobs are currently running
#
############################################################################################################

def jenkins_running_jobs():
    #
    # determine if NTO job is currently running
    #
    for line in lines:
        #
        # Call Jenkins webapi to get last build of the job
        #
        running_job = JENKINS_RUNNING_JOB_HEADER + line + JENKINS_RUNNING_JOB_TRAILER
        ret = requests.get(running_job)
        if ret.status_code != 200:
            print(running_job, "return status", ret.status_code)
            quit()

        #
        # load the JSON data from the webapi call
        #
        json_data = json.loads(ret.text)

        #
        # query the building element to see if the job is running
        #
        running = json_data['building']
        if running:
            #
            # job is currently running - terminate
            #
            displayName = json_data['fullDisplayName']
            jobName(line, "is currently running")


############################################################################################################
#
# read in list of Jenkins jobs associated with this NTO
# and place the list into a string array
#
############################################################################################################

parser = argparse.ArgumentParser()
parser.add_argument('--jobListDir', required=True, action='store', dest='jobListDir', help='Jenkins job list configuration directory')
parser.add_argument('--device', required=True, action='store', dest='device', help='Device IP address')
results = parser.parse_args()

filename = os.path.join(results.jobListDir, results.device + ".txt")
if (os.path.isfile(filename)):
    try:
        f = open(filename,'r')
        lines.clear()

        for line in f:
            line = line.rstrip()
            if line.strip():
                lines.append(line)
        f.close()

        jenkins_running_jobs()

        jenkins_job_queue()
    except IOError:
        print ("ERROR: Could not open file " + filename)
        quit()


