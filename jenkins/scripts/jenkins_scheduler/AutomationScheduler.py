import argparse
import os
import sys
import datetime
import json
import requests


############################################################################################################
#
# exit with error is manner that Jenkins realizes the job was a failure
#
############################################################################################################
def quit_job(reason=None):
    if reason:
        print(reason)
    sys.exit(1)


############################################################################################################
#
# FUNCTION read environment variable for Jenkins user
#
############################################################################################################
def read_jenkins_user():
    return os.getenv("JENKINS_USER")


############################################################################################################
#
# FUNCTION read environment variable for Jenkins password
#
############################################################################################################
def read_jenkins_password():
    return os.getenv("JENKINS_PASSWORD")


############################################################################################################
#
# FUNCTION terminate related Jenkins jobs that are currently running
#
############################################################################################################
def terminate_running_job(job_name, job_id):
    global JENKINS_URL
    global JENKINS_STOP_JOB
    global JENKINS_USER
    global JENKINS_PASSWORD

    stop_item = JENKINS_URL + "job/" + job_name + "/" + job_id + JENKINS_STOP_JOB
    print("Terminating Running Job: " + stop_item)
    status = requests.post(stop_item, auth=(JENKINS_USER, JENKINS_PASSWORD))
    print("Terminate Running Job Status: " + str(status))


############################################################################################################
#
# FUNCTION cancel all related Jenkins jobs that are in the job queue
#
############################################################################################################
def cancel_job_in_queue(job_id):
    global JENKINS_USER
    global JENKINS_PASSWORD
    global JENKINS_CANCEL_QUEUE_JOB

    cancel_item = JENKINS_URL + JENKINS_CANCEL_QUEUE_JOB + str(job_id)
    print("Canceling Job from Queue: " + cancel_item)
    status = requests.post(cancel_item, auth=(JENKINS_USER, JENKINS_PASSWORD))
    print("Cancel Job from Queue Status: " + str(status))


############################################################################################################
#
# FUNCTION get and print job description
#
############################################################################################################
def query_jenkins_job(name):
    """ query Jenkins to determine if specified job actually exists """
    global JENKINS_GET_URL

    job = JENKINS_GET_URL + "job/" + name + "/api/json"
    ret = requests.get(job)
    if ret.status_code != 200:
        return False
    return True


############################################################################################################
#
# print job description
#
############################################################################################################
def print_jenkins_job_description(name, description):
    global JENKINS_GET_URL

    job = JENKINS_GET_URL + "job/" + name + "/api/json"
    ret = requests.get(job)
    if ret.status_code != 200:
        print("ERROR: print_jenkins_job_description " + job, "return status", ret.status_code)
        quit_job()
    json_data = json.loads(ret.text)
    print(json_data['displayName'], description)


############################################################################################################
#
# determine if job is in the job queue. Terminates multiple instances of job in queueS
#
############################################################################################################
def jenkins_job_queue(job_name):
    """ determine if specified Jenkins job is the run queue and cancel job """
    global JENKINS_JOB_QUEUE

    #
    # Call Jenkins webapi to get job queue
    #
    ret = requests.get(JENKINS_JOB_QUEUE)
    if ret.status_code != 200:
        print("ERROR: jenkins_job_queue " + JENKINS_JOB_QUEUE, "return status", ret.status_code)
        quit_job()

    #
    # load the JSON data from the webapi call
    #
    json_data = json.loads(ret.text)

    #
    # determine if a NTO job is the queue
    #

    for item in json_data['items']:
        job = item['task']['name']
        job_id = item['id']
        if job == job_name:
            print_jenkins_job_description(job, "in Queue")
            cancel_job_in_queue(job_id)


############################################################################################################
#
# determine if job is currently running
#
############################################################################################################
def jenkins_running_jobs(job_name):
    """ determine if specified Jenkins job is running and terminate """
    global JENKINS_RUNNING_JOB_HEADER

    #
    # Call Jenkins webapi to get last build of the job
    #
    running_job = JENKINS_RUNNING_JOB_HEADER + job_name + JENKINS_RUNNING_JOB_TRAILER
    ret = requests.get(running_job)
    if ret.status_code != 200:
        return

    #
    # load the JSON data from the webapi call
    #
    json_data = json.loads(ret.text)

    #
    # query the building element to see if the job is running
    #
    running = json_data['building']
    if running:
        #
        # job is currently running - terminate
        #
        job_id = json_data['id']
        print_jenkins_job_description(job_name, "is currently running")
        terminate_running_job(job_name, job_id)


#############################################################################################################
#
# search config file for jobs that are scheduled to be updated. If scheduled to be updated terminate all
# related jenkins jobs in the queue and jobs actually running
# jobs names are determined by the following format
# NTOAutomationNightly
# Robot
# RobotWeb
#
# with specific information  device type "-" last octal of IP address
# for example
# NTOAutomationNightly5288-83
#
############################################################################################################
def cleanup_running_jobs(config_directory, branch_name, device_type):
    global UPDATE_CONFIG_FILE

    update_config_file = os.path.join(config_directory, "branches", branch_name, UPDATE_CONFIG_FILE)
    if not os.path.isfile(update_config_file):
        print("ERROR: update configuration file: " + update_config_file + " does not exist")
        quit_job()

    with open(update_config_file) as json_file:
        json_data = json.load(json_file)
        for json_item in json_data['DEVICE']:
            if device_type != json_item['DEVICE_TYPE']:
                continue
            if json_item['STATUS'] != "enabled":
                continue
            octets = json_item['IP'].split(".")
            identifier = json_item["MODEL"] + "-" + octets[3]

            #
            # check for jobs in the Jenkins run queue
            #
            job = "NTOAutomationNightly" + identifier
            if query_jenkins_job(job):
                jenkins_job_queue(job)
            job = "Robot" + identifier
            if query_jenkins_job(job):
                jenkins_job_queue(job)
            job = "RobotWeb" + identifier
            if query_jenkins_job(job):
                jenkins_job_queue(job)

            #
            # check for jobs currently running
            #
            job = "NTOAutomationNightly" + identifier
            if query_jenkins_job(job):
                jenkins_running_jobs(job)
            job = "Robot" + identifier
            if query_jenkins_job(job):
                jenkins_running_jobs(job)
            job = "RobotWeb" + identifier
            if query_jenkins_job(job):
                jenkins_running_jobs(job)


############################################################################################################
#
# start all image upgrade Jenkins jobs depending if the job is enabled for that specified branch
#
############################################################################################################
def start_install_jobs(config_directory, branch_name, device_type, build_identifier):
    global UPDATE_CONFIG_FILE
    global JENKINS_URL
    global JENKINS_USER
    global JENKINS_PASSWORD
    global JENKINS_BRANCH_PARAM
    global JENKINS_BUILD_ID_PARAM

    update_config_file = os.path.join(config_directory, "branches", branch_name, UPDATE_CONFIG_FILE)
    if not os.path.isfile(update_config_file):
        print("ERROR: update configuration file: " + update_config_file + " does not exist")
        quit_job()

    with open(update_config_file) as json_file:
        json_data = json.load(json_file)
        for json_item in json_data['DEVICE']:
            if device_type != json_item['DEVICE_TYPE']:
                continue
            if json_item['STATUS'] != "enabled":
                continue
            octets = json_item['IP'].split(".")
            identifier = json_item["MODEL"] + "-" + octets[3]

            #
            # check for jobs in the Jenkins run queue
            #
            job = "NTOAutomationUpdateDevice" + identifier
            install_job = JENKINS_URL + "job/" + job + JENKINS_BRANCH_PARAM + branch_name + JENKINS_BUILD_ID_PARAM + \
                          build_identifier
            print(install_job)
            print("Start Job: " + install_job)
            status = requests.post(install_job, auth=(JENKINS_USER, JENKINS_PASSWORD))
            print("Start Job Status: " + str(status))


############################################################################################################
#
# start all upgrade test run Jenkins jobs depending if the job is enabled for that specified branch
#
############################################################################################################
def start_system_level_jobs(config_directory, branch_name, device_type):
    global SYSTEM_LEVEL_CONFIG_FILE
    global JENKINS_URL
    global JENKINS_USER
    global JENKINS_PASSWORD
    global JENKINS_TEST_RUN_PARAM

    update_config_file = os.path.join(config_directory, "branches", branch_name, SYSTEM_LEVEL_CONFIG_FILE)
    if not os.path.isfile(update_config_file):
        print("ERROR: update configuration file: " + update_config_file + " does not exist")
        quit_job()

    with open(update_config_file) as json_file:
        json_data = json.load(json_file)
        for json_item in json_data['DEVICE']:
            if device_type != json_item['DEVICE_TYPE']:
                continue
            if json_item['STATUS'] != "enabled":
                continue
            octets = json_item['IP'].split(".")
            identifier = json_item["MODEL"] + "-" + octets[3]
            test_run_series = json_item["TEST_RUN_SERIES"]

            #
            # check for jobs in the Jenkins run queue
            #
            job = "NTOAutomationSystemLevelTest" + identifier
            install_job = JENKINS_URL + "job/" + job + JENKINS_TEST_RUN_PARAM + test_run_series
            print(install_job)
            print("Start Job: " + install_job)
            status = requests.post(install_job, auth=(JENKINS_USER, JENKINS_PASSWORD))
            print("Start Job Status: " + str(status))


if __name__ == "__main__":
    CONFIG_FILE = "automation_configuration.json"
    UPDATE_CONFIG_FILE = "update.json"
    AUTOMATION_CONFIG_FILE = "automation.json"
    ROBOT_SYSTEM_CONFIG_FILE = "robot_system.json"
    ROBOT_WEB_CONFIG_FILE = "robot_web.json"
    SNAKE_CONFIG_FILE = "snake.json"
    SYSTEM_LEVEL_CONFIG_FILE = "syslevel.json"

    JENKINS_URL = "http://build-master-0.ann.is.keysight.com:8080/"
    JENKINS_GET_URL = "http://build-master-0.ann.is.keysight.com:8080/"
    JENKINS_JOB_QUEUE = JENKINS_GET_URL + "queue/api/json"
    JENKINS_RUNNING_JOB_HEADER = JENKINS_GET_URL + "job/"
    JENKINS_RUNNING_JOB_TRAILER = "/lastBuild/api/json?depth=1"
    JENKINS_CANCEL_QUEUE_JOB = "queue/cancelItem?id="
    JENKINS_STOP_JOB = "/stop"
    JENKINS_BRANCH_PARAM = "/buildWithParameters?SCHEDULED_RUN=true&BRANCH_NAME="
    JENKINS_BUILD_ID_PARAM = "&BUILD_IDENTIFIER="
    JENKINS_TEST_RUN_PARAM = "/buildWithParameters?SCHEDULED_RUN=true&TEST_RUN_SERIES="

    JENKINS_USER = read_jenkins_user()
    JENKINS_PASSWORD = read_jenkins_password()

    parser = argparse.ArgumentParser()
    parser.add_argument('--configDir', required=True, action='store', dest='configDir',
                        help='Configuration File directory')
    parser.add_argument('--branchName', required=True, action='store', dest='branchName', help='NTO Branch Name')
    parser.add_argument('--deviceType', required=True, action='store', dest='deviceType', help='Device Type')
    results = parser.parse_args()

    if not os.path.isdir(results.configDir):
        print("ERROR: configuration directory: " + results.configDir + " does not exist")
        quit_job()

    cf = os.path.join(results.configDir, CONFIG_FILE)
    if not os.path.isfile(cf):
        print("ERROR: automation configuration file: " + cf + " does not exist")
        quit_job()

    if os.getenv("RUNAUTOMATION") is None:
        runAutomationFlag = "false"
    else:
        runAutomationFlag = os.getenv("RUNAUTOMATION")

    if os.getenv("BUILD_IDENTIFIER") is None:
        print("ERROR: Environment variable BUILD_IDENTIFIER not set")
        quit_job()
    buildIdentifier = os.getenv("BUILD_IDENTIFIER")

    if runAutomationFlag == "false":
        print("Environment variable RUNAUTOMATION not set to TRUE for device type " + results.deviceType)
        print("Automation test run not scheduled for device type " + results.deviceType + " on branch " +
              results.branchName)
        quit()

    dotw = datetime.datetime.today().weekday()
    dotw = dotw + 1

    #
    # parse automation scheduler config file to see if branch is to be tested
    #
    with open(cf) as jsonFile:
        jsonData = json.load(jsonFile)
        for jsonItem in jsonData['BRANCH']:
            bn = jsonItem['BRANCH_NAME']
            stat = jsonItem['STATUS']
            if results.branchName != bn:
                continue
            if stat != "enabled":
                continue

            ixBranch = jsonItem['IXTESTER_BRANCH']
            cleanup_running_jobs(results.configDir, results.branchName, results.deviceType)
            start_install_jobs(results.configDir, results.branchName, results.deviceType, buildIdentifier)
            start_system_level_jobs(results.configDir, results.branchName, results.deviceType)


