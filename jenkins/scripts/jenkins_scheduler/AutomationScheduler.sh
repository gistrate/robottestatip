#!/bin/bash


SCRIPT_DIR=$WORKSPACE/jenkins/scripts/jenkins_scheduler
CONFIG_DIR=$WORKSPACE/jenkins/automationScheduler/configuration/scheduler
JOB_LIST_DIR=$WORKSPACE/jenkins/automationScheduler/configuration/scheduler/JenkinsJobs

echo ""
echo ""
echo INFO: examine automation configuration file for branch $BRANCH_NAME
echo ""
echo python3 $SCRIPT_DIR/JenkinsAutomationScheduler.py --branchName $BRANCH_NAME --configDir $CONFIG_DIR --deviceType $DEVICE_TYPE --outputDir $WORKSPACE
python3 $SCRIPT_DIR/JenkinsAutomationScheduler.py --branchName $BRANCH_NAME --configDir $CONFIG_DIR --deviceType $DEVICE_TYPE --outputDir $WORKSPACE

for DEVICE in $MACHINE_LIST
do
  echo ""
  echo ""
  echo INFO: Check running jobs for $DEVICE
  echo python3 $SCRIPT_DIR/JenkinsAutomationJobStatus.py --device $DEVICE --jobListDir $JOB_LIST_DIR
  python3 $SCRIPT_DIR/JenkinsAutomationJobStatus.py --device $DEVICE --jobListDir $JOB_LIST_DIR
  if [ -e $WORKSPACE/${DEVICE}.txt ]; then
	echo python3 $SCRIPT_DIR/JenkinsJobCleanup.py --device $DEVICE --jobListDir $JOB_LIST_DIR
	python3 $SCRIPT_DIR/JenkinsJobCleanup.py --device $DEVICE --jobListDir $JOB_LIST_DIR
  fi
done

echo DONE