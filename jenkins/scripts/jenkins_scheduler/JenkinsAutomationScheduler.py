import os
import sys
import argparse
import json
import datetime


############################################################################################################
#
# FUNCTION exit with error is manner that Jenkins realizes the job was a failure
#
############################################################################################################

def quit_job():
    sys.exit(1)


def create_file_name(config_dir, branch_name, json_config_file):
    branch_dir = os.path.join(config_dir, 'branches',  branch_name)
    file_name = os.path.join(branch_dir, json_config_file)
    return file_name


def parse_automation_tests(json_config_file):
    try:
        if os.path.isfile(json_config_file):
            with open(json_config_file) as json_file:
                json_data = json.load(json_file)
                for json_item in json_data['DEVICE']:
                    config_file_device_type = json_item['DEVICE_TYPE']
                    config_file_device = json_item['IP']
                    config_file_status = json_item['STATUS']
                    if results.deviceType == config_file_device_type and config_file_status == "enabled":
                        if outputDictionary.get(config_file_device) is None:
                            outputDictionary[config_file_device] = []
                        outputDictionary[config_file_device].append("AUTOMATION_RUN_FLAG=true")
                        print("INFO: JenkinsAutomationScheduler: Device " + config_file_device +
                              " Automation Scheduled")
    except:
        print("ERROR: JenkinsAutomationScheduler parse_automation_tests parse configuration failure: " +
              json_config_file)


def parse_robot_system_tests(json_config_file):
    try:
        if os.path.isfile(json_config_file):
            with open(json_config_file) as json_file:
                json_data = json.load(json_file)
                for json_item in json_data['DEVICE']:
                    config_file_device_type = json_item['DEVICE_TYPE']
                    config_file_device = json_item['IP']
                    config_file_status = json_item['STATUS']
                    if results.deviceType == config_file_device_type and config_file_status == "enabled":
                        config = json_item['ROBOT_SYSTEM_CONFIG']
                        if outputDictionary.get(config_file_device) is None:
                            outputDictionary[config_file_device] = []
                        outputDictionary[config_file_device].append("ROBOT_SYSTEM_RUN_FLAG=true")
                        outputDictionary[config_file_device].append("ROBOT_SYSTEM_CONFIG_FILE=" + config)
                        print("INFO: JenkinsAutomationScheduler: Device " + config_file_device +
                              " Robot Scheduled")
    except:
        print("ERROR: JenkinsAutomationScheduler parse_robot_system_tests parse configuration failure: " +
              json_config_file)


def parse_robot_web_tests(json_config_file):
    try:
        if os.path.isfile(json_config_file):
            with open(json_config_file) as json_file:
                json_data = json.load(json_file)
                for json_item in json_data['DEVICE']:
                    config_file_device_type = json_item['DEVICE_TYPE']
                    config_file_device = json_item['IP']
                    config_file_status = json_item['STATUS']
                    if results.deviceType == config_file_device_type and config_file_status == "enabled":
                        config = json_item['ROBOT_WEB_CONFIG']
                        if outputDictionary.get(config_file_device) is None:
                            outputDictionary[config_file_device] = []
                        outputDictionary[config_file_device].append("ROBOT_WEB_RUN_FLAG=true")
                        outputDictionary[config_file_device].append("ROBOT_WEB_CONFIG_FILE=" + config)
                        print("INFO: JenkinsAutomationScheduler: Device " + config_file_device +
                              " Robot Web Scheduled")
    except:
        print("ERROR: JenkinsAutomationScheduler parse_robot_web_tests parse configuration failure: " +
              json_config_file)


def parse_snake_tests(json_config_file):
    try:
        if os.path.isfile(json_config_file):
            with open(json_config_file) as json_file:
                json_data = json.load(json_file)
                for json_item in json_data['DEVICE']:
                    config_file_device_type = json_item['DEVICE_TYPE']
                    config_file_device = json_item['IP']
                    config_file_status = json_item['STATUS']
                    if results.deviceType == config_file_device_type and config_file_status == "enabled":
                        schedule = json_item['SCHEDULE']
                        if str(dotw) in schedule:
                            if outputDictionary.get(config_file_device) is None:
                                outputDictionary[config_file_device] = []
                            outputDictionary[config_file_device].append("SNAKE_TEST_RUN_FLAG=true")
                            outputDictionary[config_file_device].append("SNAKE_TESTRUN=" + json_item['SNAKE_TESTRUN'])
                            print("INFO: JenkinsAutomationScheduler: Device " + config_file_device +
                                  " Snake Test Scheduled")
    except:
        print("ERROR: JenkinsAutomationScheduler parse_snake_tests parse configuration failure: " + json_config_file)


def parse_system_level_tests(json_config_file):
    try:
        if os.path.isfile(json_config_file):
            with open(json_config_file) as json_file:
                json_data = json.load(json_file)
                for json_item in json_data['DEVICE']:
                    config_file_device_type = json_item['DEVICE_TYPE']
                    config_file_device = json_item['IP']
                    config_file_status = json_item['STATUS']
                    if results.deviceType == config_file_device_type and config_file_status == "enabled":
                        schedule = json_item['SCHEDULE']
                        if str(dotw) in schedule:
                            if outputDictionary.get(config_file_device) is None:
                                outputDictionary[config_file_device] = []
                            outputDictionary[config_file_device].append("SYSTEM_LEVEL_TEST_RUN_FLAG=true")
                            outputDictionary[config_file_device].append("TEST_RUN_SERIES=" +
                                                                        json_item['TEST_RUN_SERIES'])
                            print("INFO: JenkinsAutomationScheduler: Device " + config_file_device +
                                  " Upgrade Test Scheduled")

    except:
        print("ERROR: JenkinsAutomationScheduler parse_system_level_tests parse configuration failure: " +
              json_config_file)


parser = argparse.ArgumentParser()
parser.add_argument('--configDir', required=True, action='store', dest='configDir', help='Configuration File directory')
parser.add_argument('--branchName', required=True, action='store', dest='branchName', help='NTO Branch Name')
parser.add_argument('--deviceType', required=True, action='store', dest='deviceType', help='Device Type')
parser.add_argument('--outputDir', required=True, action='store', dest='outputDir', help='Output Directory')
results = parser.parse_args()

CONFIG_FILE = "automation_configuration.json"
AUTOMATION_CONFIG_FILE = "automation.json"
ROBOT_SYSTEM_CONFIG_FILE = "robot_system.json"
ROBOT_WEB_CONFIG_FILE = "robot_web.json"
SNAKE_CONFIG_FILE = "snake.json"
SYSTEM_LEVEL_CONFIG_FILE = "syslevel.json"

#
# string array of environment variables used to run various automation test runs
#
outputDictionary = {}

cf = os.path.join(results.configDir, CONFIG_FILE)
print(cf)
if not os.path.isfile(cf):
    print("ERROR: automation configuration file: " + cf + " does not exist")
    quit_job()

if os.getenv("RUNAUTOMATION") is None:
    runAutomationFlag = "false"
else:
    runAutomationFlag = os.getenv("RUNAUTOMATION")

if os.getenv("BUILD_IDENTIFIER") is None:
    print("ERROR: Environment variable BUILD_IDENTIFIER not set")
    quit_job()

buildIdentifier = os.getenv("BUILD_IDENTIFIER")

if runAutomationFlag == "false":
    print("Environment variable RUNAUTOMATION not set to TRUE for deviceType " + results.deviceType)
    print("Automation test run not scheduled for deviceType " + results.deviceType + " on branch " + results.branchName)
    quit()

dotw = datetime.datetime.today().weekday()
dotw = dotw + 1

with open(cf) as jsonFile:
    jsonData = json.load(jsonFile)
    for jsonItem in jsonData['BRANCH']:
        bn = jsonItem['BRANCH_NAME']
        stat = jsonItem['STATUS']
        if results.branchName == bn and stat == "enabled":
            ixBranch = jsonItem['IXTESTER_BRANCH']
            mailList = jsonItem['AUTOMATION_MAILING_LIST']

            parse_automation_tests(create_file_name(results.configDir, results.branchName, AUTOMATION_CONFIG_FILE))
            parse_robot_system_tests(create_file_name(results.configDir, results.branchName, ROBOT_SYSTEM_CONFIG_FILE))
            parse_robot_web_tests(create_file_name(results.configDir, results.branchName, ROBOT_WEB_CONFIG_FILE))
            parse_snake_tests(create_file_name(results.configDir, results.branchName, SNAKE_CONFIG_FILE))
            parse_system_level_tests(create_file_name(results.configDir, results.branchName, SYSTEM_LEVEL_CONFIG_FILE))

            for key, value in outputDictionary.items():
                cf = os.path.join(results.outputDir, key + ".txt")
                with open(cf, "w") as fp:
                        fp.write("BRANCH_NAME=" + results.branchName + "\n")
                        fp.write("BUILD_IDENTIFIER=" + buildIdentifier + "\n")
                        fp.write("IXTESTER_SVN_BRANCH=" + ixBranch + "\n")
                        fp.write("AUTOMATION_MAILING_LIST=" + mailList + "\n")
                        for x in value:
                            fp.write(x + "\n")
                        fp.close()
