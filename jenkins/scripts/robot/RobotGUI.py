import os
import sys
import requests
import json
import ftplib
import smtplib
import shutil
import logging
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def transfer_files(results_directory, report_dir, tar_file):
    global BRANCH_NAME
    global DEVICE

    os.chdir(results_directory)

    ftp = ftplib.FTP(os.getenv("REMOTE_FILE_SYSTEM"))
    ftp.login("ftpuser", "ftpuser")

    ftp.cwd("reports")

    try:
        ftp.mkd(report_dir)
    except:
        pass
    ftp.cwd(report_dir)

    try:
        ftp.mkd(BRANCH_NAME)
    except:
        pass
    ftp.cwd(BRANCH_NAME)

    try:
        ftp.mkd(DEVICE)
    except:
        pass
    ftp.cwd(DEVICE)

    ftp.storbinary('STOR ' + tar_file, open(tar_file, 'rb'))

    ftp.quit()


def get_build_identifier(device_ip):

    webapi_port = "8000"
    webapi_user = "admin"
    webapi_password = "admin"

    job = "https://" + device_ip + ":" + webapi_port + "/api/system?properties=software_version"
    try:
        ret = requests.get(job, auth=(webapi_user, webapi_password), verify=False, timeout=120)
        if ret.status_code != 200:
            print("ERROR: get_build_identifier Bad Return status: " + job + " " + str(ret.status_code))
            quit_job()

    except:
        print("ERROR: get_build_identifier: exception: " + job)
        quit_job()

    json_data = json.loads(ret.text)
    software_version = json_data['software_version']
    return software_version


def get_branch_name(device_ip):

    webapi_port = "8000"
    webapi_user = "admin"
    webapi_password = "admin"

    job = "https://" + device_ip + ":" + webapi_port + "/api/system?properties=software_version"
    try:
        ret = requests.get(job, auth=(webapi_user, webapi_password), verify=False, timeout=120)
        if ret.status_code != 200:
            print("ERROR: get_branch_name Bad Return status: " + job + " " + str(ret.status_code))
            quit_job()

    except:
        print("ERROR: get_branch_name: exception: " + job)
        quit_job()

    json_data = json.loads(ret.text)
    software_version = json_data['software_version']
    branch_name = software_version.split("-")[0]

    #
    # get the number of dots. If there are 3 dots then this
    # is a RC build and the RC number needs to be stripped out
    #
    bc = branch_name.count('.')
    if bc == 3:
        rf = branch_name.rfind('.')
        branch_name = branch_name[:rf]

    return branch_name


#
# exit with error is manner that Jenkins realizes the job was a failure
#
def quit_job(reason=None):
    if reason:
        logging.error(reason)
    sys.exit(1)


#
# insure that all the required environment variables need to perform the
# update have been set.
#
def validate_environment():
    global CONFIG_DIR
    global ROBOT_CONFIG
    global TEST_TYPE
    global WORKSPACE
    global DEVICE_IP
    global DEVICE
    global BUILD_ID
    global BUILD_URL
    global JOB_URL

    validEnviron = True

    if os.getenv("CONFIG_DIR") is None:
        print("ERROR: Environment variable CONFIG_DIR not set")
        validEnviron = False
    else:
        CONFIG_DIR = os.getenv("CONFIG_DIR")

    if os.getenv("ROBOT_CONFIG") is None:
        print("ERROR: Environment variable ROBOT_CONFIG not set")
        validEnviron = False
    else:
        ROBOT_CONFIG = os.getenv("ROBOT_CONFIG")

    if os.getenv("WORKSPACE") is None:
        print("ERROR: Environment variable WORKSPACE not set")
        validEnviron = False
    else:
        WORKSPACE = os.getenv("WORKSPACE")

    if os.getenv("DEVICE_IP") is None:
        print("ERROR: Environment variable DEVICE_IP not set")
        validEnviron = False
    else:
        DEVICE_IP = os.getenv("DEVICE_IP")

    if os.getenv("DEVICE") is None:
        print("ERROR: Environment variable DEVICE not set")
        validEnviron = False

    if os.getenv("TEST_TYPE") is None:
        print("ERROR: Environment variable TEST_TYPE not set")
        validEnviron = False
    else:
        TEST_TYPE = os.getenv("TEST_TYPE")

    if os.getenv("REMOTE_FILE_SYSTEM") is None:
        print("ERROR: Environment variable REMOTE_FILE_SYSTEM not set")
        validEnviron = False

    if os.getenv("BUILD_ID") is None:
        print("ERROR: Environment variable BUILD_ID not set")
        validEnviron = False
    else:
        BUILD_ID = os.getenv("BUILD_ID")

    if os.getenv("BUILD_URL") is None:
        print("ERROR: Environment variable BUILD_URL not set")
        validEnviron = False
    else:
        BUILD_URL = os.getenv("BUILD_URL")

    if os.getenv("JOB_URL") is None:
        print("ERROR: Environment variable JOB_URL not set")
        validEnviron = False
    else:
        JOB_URL = os.getenv("JOB_URL")

    if not validEnviron:
        print("ERROR: Environment variables not set. Terminating Install")
        quit_job()


def generate_email(test_status):
    global DEVICE
    global BUILD_IDENTIFIER
    global BUILD_URL
    global JOB_URL

    subject = DEVICE + " Completed Robot NTOgui Test Automation Run  - " + BUILD_IDENTIFIER

    body = subject + "<BR><BR>"
    if test_status['passed'] == 0 and test_status['failed'] == 0:
        body = body + "ERROR: Test Run Failure<BR><BR>"
    else:
        body = body + "PASSED: " + test_status['passed'] + "  FAILED: " + test_status['failed'] + "<BR><BR>"
    body = body + DEVICE + " Robot NTOgui Test Automation Run: " + BUILD_URL + "<BR>"
    body = body + DEVICE + " Robot NTOgui Test Automation Run Results: " + JOB_URL + "ws/output/report.html<BR>"
    body = body + DEVICE + " Robot NTOgui Test Job Summary: " + JOB_URL + "<BR>"

    build_master = "build-master-0@keysight.com"
    mail_list = os.getenv("EMAIL_ROBOT_WEBAPI_TEST")
    mail_list = mail_list.split(",")

    mail_server = os.getenv("MAIL_SERVER")

    msg = MIMEMultipart()

    msg['Subject'] = subject
    msg['From'] = build_master
    msg['To'] = ", ".join(mail_list)
    msg.attach(MIMEText(body, 'html'))

    s = smtplib.SMTP(mail_server)
    s.sendmail(build_master, mail_list, msg.as_string())
    s.quit()


def determine_test_results(results_directory, set_description):
    global DEVICE_IP

    test_status = {}
    test_status['passed'] = 0
    test_status['failed'] = 0

    file = os.path.join(results_directory, "output.xml")
    try:
        f = open(file, 'r')

        for line in f:
            line = line.rstrip()
            if not line.strip():
                continue
            if "All Tests" in line:
                words = line.split()
                for word in words:
                    if "fail" in word:
                        test_status['failed'] = word.split("\"")[1]
                    if "pass" in word:
                        test_status['passed'] = word.split("\"")[1]
        f.close()

        file = os.path.join(results_directory, "failed_count.txt")
        f = open(file, 'w')
        f.write(test_status['failed'] + '\n')
        f.close()

        file = os.path.join(results_directory, "passed_count.txt")
        f = open(file, 'w')
        f.write(test_status['passed'] + '\n')
        f.close()

        file = os.path.join(results_directory, "device_ip.txt")
        f = open(file, 'w')
        f.write(DEVICE_IP + "\n")
        f.close()

        if set_description:
            print("[DESCRIPTION] PASSED: " + test_status['passed'] + "  FAILED: " + test_status['failed'])

    except IOError:
        print("ERROR: determine_test_results: Could not open output results file: " + file)
        if set_description:
            print("[DESCRIPTION] No Test Results")

    return test_status


#
# read schedule config file to get list of jobs and there associated resource pool and options
#
def read_config_file():
    global jobs

    file = os.path.join(CONFIG_DIR, ROBOT_CONFIG)

    try:
        f = open(file,'r')
        for line in f:
            line = line.rstrip()
            #
            # skip blank lines
            #
            if not line.strip():
                continue
            #
            # skip comment lines
            #
            if line.startswith('#'):
                continue

            x = (line.split(';'))
            jobs.append(x)
        f.close()
    except IOError:
        print ("ERROR: read_config_file: Could not open file" + file)
        quit_job()


def determine_run_list(nto_output_directory):
    run_list = ""
    for root, dirs, files in os.walk(nto_output_directory):
        if "output.xml" in files:
            fullPath = os.path.join(root, "output.xml")
            run_list = run_list + " " + fullPath[WORKSPACE_DIR_LENGTH:]
    return run_list


###################################################################
#
# main routine
#
####################################################################

jobs = []

validate_environment()

DEVICE = os.getenv("DEVICE")

BRANCH_NAME = get_branch_name(DEVICE_IP)

BUILD_IDENTIFIER = get_build_identifier(DEVICE_IP)

WORKSPACE_DIR_LENGTH = len(WORKSPACE) + 1

print("")
print("####################################")
print("INFO BUILD IDENTIFIER:", BUILD_IDENTIFIER)
print("INFO BRANCH NAME:", BRANCH_NAME)
print("INFO DEVICE IP ADDRESS:", DEVICE_IP)
print("INFO DEVICE MODEL:", DEVICE)
print("INFO TEST TYPE:", TEST_TYPE)
print("INFO CONFIG FILE:", ROBOT_CONFIG)
print("####################################")
print("")

read_config_file()

PORT_SPEED_INDEX = 0
TEST_SUITE_INDEX = 1
RESOURCE_POOL_INDEX = 2
PARAMETERS_INDEX = 3

portSpeeds = []

for line in jobs:
    parms = ""

    #
    # does test run contain optional parameters
    #
    if len(line) > PARAMETERS_INDEX:
        parms = line[PARAMETERS_INDEX]
    portSpeed = line[PORT_SPEED_INDEX]
    if portSpeed not in portSpeeds:
        portSpeeds.append(portSpeed)
    testSuite = line[TEST_SUITE_INDEX]
    resourcePool = line[RESOURCE_POOL_INDEX]
    cmd = "pybot --pythonpath $WORKSPACE --variable RESOURCEPOOL:" + resourcePool + \
          " --exclude incomplete --exclude skip --exclude obsolete " + parms + \
          " --outputdir $WORKSPACE/results/" + portSpeed + "/robotWeb/" + testSuite + \
          " $WORKSPACE/" + TEST_TYPE + "/scripts/robot_tests/" + testSuite + ".robot"
    print(cmd)
    os.system(cmd)

print("")
print("INFO: RobotGUI: Create test run summary with rebot")
print("")
os.mkdir("output")

OUTPUT_DIRECTORY = os.path.join(WORKSPACE, "output")

os.chdir(WORKSPACE)

description = DEVICE + " " + BUILD_IDENTIFIER
resultsDirectory = os.path.join(WORKSPACE, "results")
runList = determine_run_list(resultsDirectory)
print(runList)
cmd = "rebot --output output.xml -d output -N \"" + description + "\" " + runList
print(cmd)
os.system(cmd)

testStatus = determine_test_results(OUTPUT_DIRECTORY, True)

generate_email(testStatus)

if testStatus['passed'] == 0 and testStatus['failed'] == 0:
    #
    # total test run failure - do not attempt to transfer files
    #
    pass
else:
    #
    # create zip file containing all test run output
    #
    # parameter 1: full filename of the zip file to create (less archive type extension)
    # parameter 2: type of archive (zip file)
    # parameter 3: root_dir: full directory path of the files to archive
    # parameter 3: base_dir: file directory path of the files to archive
    #
    for deviceSpeed in portSpeeds:
        ntoOutputDirectory = os.path.join(resultsDirectory, deviceSpeed, "robotWeb")

        runList = determine_run_list(ntoOutputDirectory)
        print(runList)
        description = DEVICE + " " + deviceSpeed + " " + BUILD_IDENTIFIER
        cmd = "rebot --output output.xml -d " + ntoOutputDirectory + " -N \"" + description + "\" " + runList
        print(cmd)
        os.system(cmd)
        determine_test_results(ntoOutputDirectory, True)

    shutil.make_archive(os.path.join(OUTPUT_DIRECTORY, "robotWeb"), "tar", root_dir=resultsDirectory, base_dir=".")

    transfer_files(resultsDirectory, "systemTest", "robotWeb.tar")
    transfer_files(resultsDirectory, "dailyStatus", "robotWeb.tar")


